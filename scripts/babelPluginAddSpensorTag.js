const whitelistTagName = ["ScopedLoadingBoundary", "Spensor", "SpensorR"]

function addSpensorTag({ types: t }) {
  return {
    name: "babel-plugin-add-spensor-tag",
    visitor: {
      Program(path, state) {
        const property = state.opts.property || "spensorTag"

        path.traverse({
          JSXElement(path2) {
            if (
              !whitelistTagName.includes(path2.node.openingElement.name.name)
            ) {
              return
            }
            const filename = state.file.opts.filename || "" // filename missing in test env, see tests
            const rootDir = state.file.opts.root
            const relativePath = filename.slice(rootDir.length)
            const spensorTag = relativePath + ":" + path2.node.loc.start.line
            path2.node.openingElement.attributes.push(
              t.jSXAttribute(
                t.jSXIdentifier(property),
                t.stringLiteral(spensorTag),
              ),
            )
          },
        })
      },
    },
  }
}

module.exports = addSpensorTag
