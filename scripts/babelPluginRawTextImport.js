const sysPath = require("path")
const _ = require("lodash")

const getContentFetcher = (path, state) => {
  const { fetchers } = state.opts
  const node = path.node

  if (node.source == null) return

  const entry = _.entries(fetchers).find(([suffix]) =>
    node.source.value.endsWith(`?${suffix}`),
  )
  if (entry == null) return

  const [suffix, fetcher] = entry
  const sourceFilePath = sysPath.resolve(state.file.opts.filename)
  const targetFilePath = sysPath.resolve(
    sysPath.dirname(sourceFilePath),
    path.node.source.value.replace(new RegExp(`\\?${suffix}$`), ""),
  )

  return () => fetcher(targetFilePath, sourceFilePath)
}

/**
 * Options:
 *
 * @example
 *
 * ```js
 * ['rawTextImport', {
 *   fetchers: {
 *     "raw": (targetFilePath: string, sourceFilePath: string) => string
 *     "html": (targetFilePath: string) => string
 *   }
 * }]
 * ```
 */
module.exports = function ({ types: t }) {
  return {
    name: "babel-plugin-raw-text-import",
    visitor: {
      ExportDeclaration: {
        exit: function (path, state) {
          const node = path.node
          const fetcher = getContentFetcher(path, state)

          if (fetcher != null) {
            path.replaceWithMultiple([
              t.variableDeclaration("const", [
                t.variableDeclarator(
                  t.identifier(node.specifiers[0].exported.name),
                  t.stringLiteral(fetcher()),
                ),
              ]),
              t.exportNamedDeclaration(null, [
                t.exportSpecifier(
                  t.identifier(node.specifiers[0].exported.name),
                  t.identifier(node.specifiers[0].exported.name),
                ),
              ]),
            ])
          }
        },
      },
      ImportDeclaration: {
        exit: function (path, state) {
          const node = path.node
          const fetcher = getContentFetcher(path, state)

          if (fetcher != null) {
            path.replaceWith(
              t.variableDeclaration("const", [
                t.variableDeclarator(
                  t.identifier(node.specifiers[0].local.name),
                  t.stringLiteral(fetcher()),
                ),
              ]),
            )
          }
        },
      },
    },
  }
}
