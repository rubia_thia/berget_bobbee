const fs = require("fs")
const path = require("path")

module.exports = transformer

/**
 * @param options.readScript {(filePath: string) => string}
 */
function transformer(options) {
  const { filePath } = options
  if (filePath == null) {
    throw new Error(
      "[PostHtmlPluginInlineLocalScript] `filePath` option is required",
    )
  }

  return function transform(tree) {
    tree.walk(function (node) {
      const { attrs } = node
      if (attrs == null) return node

      if (
        node.tag === "script" &&
        attrs.src != null &&
        (attrs.src.endsWith(".js") || attrs.src.endsWith(".ts"))
      ) {
        const readScript = options.readScript ?? defaultReadScript
        const content = readScript(path.resolve(filePath, "../", attrs.src))

        return {
          ...node,
          attrs: { ...attrs, src: undefined, type: undefined },
          content,
        }
      }

      return node
    })

    return tree
  }
}

function defaultReadScript(filePath) {
  return fs.readFileSync(filePath, "utf8")
}
