#!/usr/bin/env bash

set -euo pipefail

pnpm formatjs compile 'src/generated/i18n/source.json' --out-file='src/generated/i18n/compiled/en-US.json'
for f in `ls -1 "src/generated/i18n/downloaded"/*.json`; do
  pnpm formatjs compile "$f" --out-file="src/generated/i18n/compiled/$(basename "$f")"
done
pnpm prettier --write 'src/generated/i18n/compiled/*.json'
