#!/usr/bin/env bash

set -euo pipefail

BACKEND_PROJ_PATH="${1:-}"
FRONTEND_PROJ_PATH="${2:-}"

if [ -z "$BACKEND_PROJ_PATH" ] || [ -z "$FRONTEND_PROJ_PATH" ]; then
  echo "Usage: $0 <backend-project-path> <frontend-project-path>"
  exit 1
fi

CONTRACTS_PATH="$BACKEND_PROJ_PATH/packages/contracts/core-v1"
ARTIFACTS_PATH="$CONTRACTS_PATH/artifacts"

pushd "$BACKEND_PROJ_PATH"
eval "$(direnv export bash)"
popd

rm -rf "$ARTIFACTS_PATH"
pushd "$CONTRACTS_PATH"
hardhat clean
hardhat compile
popd

files=(
  "contracts/TradingCore.sol/TradingCore.json"
  "contracts/TraderFarm.sol/TraderFarm.json"
  "contracts/Fees.sol/Fees.json"
  "contracts/SwapRouter.sol/SwapRouter.json"
  "contracts/RegistryCore.sol/RegistryCore.json"
  "contracts/LiquidityPool.sol/LiquidityPool.json"
  "contracts/OracleAggregator.sol/OracleAggregator.json"
  "contracts/Referrals.sol/Referrals.json"
  "contracts/tests/TestToken.sol/TestToken.json"
  "contracts/tokens/esUniwhaleToken.sol/esUniwhaleToken.json"
  "contracts/tokens/UniwhaleToken.sol/UniwhaleToken.json"
  "contracts/tokens/UniwhalePass.sol/UniwhalePass.json"
  "contracts/extensions/LimitBook.sol/LimitBook.json"
  "contracts/extensions/TradingCoreWithRouter.sol/TradingCoreWithRouter.json"
  "contracts/extensions/RegistryReader.sol/RegistryReader.json"
  "contracts/utils/pyth/MockPyth.sol/MockPyth.json"
  "contracts/utils/config/UniwhaleConfig.sol/UniwhaleConfig.json"
  "contracts/libs/TradingCoreLib.sol/TradingCoreLib.json"
  "contracts/utils/helpers/StakingHelper.sol/StakingHelper.json"
  "contracts/RevenuePool.sol/RevenuePool.json"
  "contracts/utils/helpers/AirdropHelper.sol/AirdropHelper.json"
  "contracts/utils/helpers/MultiCall.sol/MultiCall.json"
  "@openzeppelin/contracts/token/ERC20/ERC20.sol/ERC20.json"
  "@openzeppelin/contracts/token/ERC721/IERC721.sol/IERC721.json"
)
for i in "${files[@]}"; do
  cp "$ARTIFACTS_PATH/$i" "$FRONTEND_PROJ_PATH/src/stores/contracts/abi"
done

cp "$BACKEND_PROJ_PATH/packages/libs/env/src/lib/ONCHAIN_CONFIG_KEY.ts" "$FRONTEND_PROJ_PATH/src/stores/AppEnvStore/ONCHAIN_CONFIG_KEY.ts"

pnpm gen:contract
