module.exports = {
  schema: "./src/generated/graphql/schema.graphql",
  documents: "./src/**/*.{ts,tsx}",
  generates: {
    "src/generated/graphql/graphql.generated.ts": {
      plugins: ["typescript", "typescript-operations"],
      config: {
        scalars: {
          bigint: "number",
          float8: "number",
          jsonb: "any",
          numeric: "number",
          timestamptz: "string",
          bytea: "string",
        },
      },
    },
  },
}
