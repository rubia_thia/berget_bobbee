import { NavigatorScreenParams } from "@react-navigation/native"
import { StackScreenProps } from "@react-navigation/stack"
import { DebugNavigatorParamList } from "./DebugScreenNavigatorTypes"

export type TopLevelNavigatorParamList = {
  Trade: undefined | { symbol?: string }
  Liquidity: undefined
  Earn: undefined
  Airdrop: undefined | { modal?: string & "ugp-checker" }
  Referral: undefined | { referralCode?: string }
  Faucet: undefined
  Modal: undefined
  NotFound: undefined
  Debug: NavigatorScreenParams<DebugNavigatorParamList>
}

export type TopLevelNavigatorScreenProps<
  Screen extends keyof TopLevelNavigatorParamList,
> = StackScreenProps<TopLevelNavigatorParamList, Screen>
