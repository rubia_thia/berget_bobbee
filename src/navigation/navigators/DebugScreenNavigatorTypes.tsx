import { CompositeScreenProps } from "@react-navigation/native"
import { StackScreenProps } from "@react-navigation/stack"
import { TopLevelNavigatorParamList } from "./TopLevelNavigatorTypes"

export type DebugNavigatorParamList = {
  Index: undefined
  Latest: undefined
  Wallet: { address: string }
  OrderDetail: { orderId: string }
}

export type DebugNavigatorScreenProps<
  Screen extends keyof DebugNavigatorParamList,
> = CompositeScreenProps<
  StackScreenProps<DebugNavigatorParamList, Screen>,
  StackScreenProps<TopLevelNavigatorParamList>
>
