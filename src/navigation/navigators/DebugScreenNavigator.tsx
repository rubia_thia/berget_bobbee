import { createNativeStackNavigator } from "@react-navigation/native-stack"
import { FC } from "react"
import DebugIndexScreen from "../../screens/Debug/DebugIndexScreen"
import { DebugOrderIdScreen } from "../../screens/Debug/DebugOrderIdScreen"
import { DebugTxScreen } from "../../screens/Debug/DebugWalletScreen"
import { LatestTxScreen } from "../../screens/Debug/LatestTxScreen"
import { DebugStoreProvider } from "../../stores/DebugStore/useDebugStore"
import { DebugNavigatorParamList } from "./DebugScreenNavigatorTypes"
import { TopLevelNavigatorScreenProps } from "./TopLevelNavigatorTypes"

const DebugScreen = createNativeStackNavigator<DebugNavigatorParamList>()

export const DebugScreenNavigator: FC<
  TopLevelNavigatorScreenProps<"Debug">
> = () => {
  return (
    <DebugStoreProvider>
      <DebugScreen.Navigator>
        <DebugScreen.Screen name="Index" component={DebugIndexScreen} />
        <DebugScreen.Screen name="Latest" component={LatestTxScreen} />
        <DebugScreen.Screen name="Wallet" component={DebugTxScreen} />
        <DebugScreen.Screen name="OrderDetail" component={DebugOrderIdScreen} />
      </DebugScreen.Navigator>
    </DebugStoreProvider>
  )
}
