import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs"
import { NavigationProp, useNavigation } from "@react-navigation/native"
import React, { FC } from "react"
import { View } from "react-native"
import { WiredBottomBar } from "../../components/BottomBar/WiredBottomBar"
import { TopLevelNavigatorHelpersProvider } from "../../components/NavigatorHelpersProvider"
import { WiredTopNavBar } from "../../components/TopNavBar/WiredTopNavBar"
import { AirdropScreen } from "../../screens/AirdropScreen/AirdropScreen"
import { EarnScreen } from "../../screens/EarnScreen/EarnScreen"
import { FaucetScreen } from "../../screens/FaucentScreen/FaucetScreen"
import { LiquidityScreen } from "../../screens/LiquidityScreen/LiquidityScreen"
import { ModalScreen } from "../../screens/ModalScreen"
import { NotFoundScreen } from "../../screens/NotFoundScreen"
import { ReferralScreen } from "../../screens/ReferralScreen/ReferralScreen"
import { TradeScreen } from "../../screens/TradeScreen/TradeScreen"
import { useWiredAutoLoginConnectedWalletProvider } from "../../utils/useWiredAutoLoginConnectedWalletProvider"
import { DebugScreenNavigator } from "./DebugScreenNavigator"
import { TopLevelNavigatorParamList } from "./TopLevelNavigatorTypes"

const Nav = createMaterialTopTabNavigator<TopLevelNavigatorParamList>()

export const TopLevelNavigator: FC = () => {
  const topLevelNavigation =
    useNavigation<NavigationProp<TopLevelNavigatorParamList>>()

  useWiredAutoLoginConnectedWalletProvider()

  return (
    <TopLevelNavigatorHelpersProvider topLevelNavigation={topLevelNavigation}>
      <View className="flex-grow">
        <Nav.Navigator
          tabBar={() => <WiredTopNavBar navigation={topLevelNavigation} />}
          screenOptions={{
            swipeEnabled: false,
            lazy: true,
            animationEnabled: false,
          }}
        >
          <Nav.Screen name="Trade" component={TradeScreen} />
          <Nav.Screen name="Liquidity" component={LiquidityScreen} />
          <Nav.Screen name="Earn" component={EarnScreen} />
          <Nav.Screen name="Airdrop" component={AirdropScreen} />
          <Nav.Screen name="Referral" component={ReferralScreen} />
          <Nav.Screen name="Faucet" component={FaucetScreen} />
          <Nav.Screen
            name="Debug"
            component={DebugScreenNavigator}
            options={{ title: "Debug Screen" }}
          />
          <Nav.Screen
            name="NotFound"
            component={NotFoundScreen}
            options={{ title: "Oops!" }}
          />
          <Nav.Group screenOptions={{}}>
            <Nav.Screen name="Modal" component={ModalScreen} />
          </Nav.Group>
        </Nav.Navigator>
        <WiredBottomBar className="mt-auto" />
      </View>
    </TopLevelNavigatorHelpersProvider>
  )
}
