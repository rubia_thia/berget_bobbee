/**
 * Learn more about deep linking with React Navigation
 * https://reactnavigation.org/docs/deep-linking
 * https://reactnavigation.org/docs/configuring-links
 */

import { LinkingOptions } from "@react-navigation/native"
import * as Linking from "expo-linking"
import { TopLevelNavigatorParamList } from "./navigators/TopLevelNavigatorTypes"

export const linking: LinkingOptions<TopLevelNavigatorParamList> = {
  prefixes: [Linking.createURL("/")],
  config: {
    initialRouteName: "Trade",
    screens: {
      Trade: {
        path: "",
      },
      Liquidity: {
        path: "liquidity",
      },
      Earn: {
        path: "earn",
      },
      Airdrop: {
        path: "airdrop",
      },
      Referral: {
        path: "referral/:referralCode?",
      },
      Modal: {},
      Debug: {
        path: "debug",
        screens: {
          Index: {
            path: "",
          },
          Latest: {
            path: "/latest",
          },
          Wallet: {
            path: "/wallet/:address",
          },
          OrderDetail: {
            path: "/order/:orderId",
          },
        },
      },
      NotFound: "*",
      Faucet: {
        path: "faucet",
      },
    },
  },
}
