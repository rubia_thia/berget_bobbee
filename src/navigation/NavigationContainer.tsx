import {
  DefaultTheme,
  NavigationContainer as _NavigationContainer,
} from "@react-navigation/native"
import * as React from "react"
import { ColorSchemeName } from "react-native"
import { FCC } from "../utils/reactHelpers/types"
import { linking } from "./LinkingConfiguration"

export const NavigationContainer: FCC<{
  colorScheme: ColorSchemeName
}> = props => {
  return (
    <_NavigationContainer
      linking={linking}
      theme={{
        ...DefaultTheme,
        colors: {
          ...DefaultTheme.colors,
          background: "#E9EFF1",
        },
      }}
      // theme={props.colorScheme === "dark" ? DarkTheme : DefaultTheme}
    >
      {props.children}
    </_NavigationContainer>
  )
}
