declare module "*.svg" {
  import { ComponentType } from "react"
  import { SvgProps } from "react-native-svg"

  const Comp: ComponentType<SvgProps>

  export default Comp
}

declare module "*.png" {
  import { ImageRequireSource } from "react-native"

  const res: ImageRequireSource

  export default res
}

declare module "*?raw" {
  const result: string
  export default result
}
declare module "*?inlineScript" {
  const result: string
  export default result
}
declare module "*?inlineHtml" {
  const result: string
  export default result
}

declare module "css-gradient-angle-to-svg-gradient-coordinates" {
  function angleToCoordinates(
    angle: number,
    sizeOfSquare?: number,
  ): { x1: number; y1: number; x2: number; y2: number }

  export = angleToCoordinates
}
