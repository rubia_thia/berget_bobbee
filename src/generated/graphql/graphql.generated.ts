export type Maybe<T> = T | null
export type InputMaybe<T> = Maybe<T>
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K]
}
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]?: Maybe<T[SubKey]>
}
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]: Maybe<T[SubKey]>
}
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string
  String: string
  Boolean: boolean
  Int: number
  Float: number
  bigint: number
  bytea: string
  jsonb: any
  numeric: number
  timestamptz: string
}

/** Boolean expression to compare columns of type "Boolean". All fields are combined with logical 'AND'. */
export type Boolean_Comparison_Exp = {
  _eq?: InputMaybe<Scalars["Boolean"]>
  _gt?: InputMaybe<Scalars["Boolean"]>
  _gte?: InputMaybe<Scalars["Boolean"]>
  _in?: InputMaybe<Array<Scalars["Boolean"]>>
  _is_null?: InputMaybe<Scalars["Boolean"]>
  _lt?: InputMaybe<Scalars["Boolean"]>
  _lte?: InputMaybe<Scalars["Boolean"]>
  _neq?: InputMaybe<Scalars["Boolean"]>
  _nin?: InputMaybe<Array<Scalars["Boolean"]>>
}

/** Boolean expression to compare columns of type "Int". All fields are combined with logical 'AND'. */
export type Int_Comparison_Exp = {
  _eq?: InputMaybe<Scalars["Int"]>
  _gt?: InputMaybe<Scalars["Int"]>
  _gte?: InputMaybe<Scalars["Int"]>
  _in?: InputMaybe<Array<Scalars["Int"]>>
  _is_null?: InputMaybe<Scalars["Boolean"]>
  _lt?: InputMaybe<Scalars["Int"]>
  _lte?: InputMaybe<Scalars["Int"]>
  _neq?: InputMaybe<Scalars["Int"]>
  _nin?: InputMaybe<Array<Scalars["Int"]>>
}

/** Boolean expression to compare columns of type "String". All fields are combined with logical 'AND'. */
export type String_Comparison_Exp = {
  _eq?: InputMaybe<Scalars["String"]>
  _gt?: InputMaybe<Scalars["String"]>
  _gte?: InputMaybe<Scalars["String"]>
  /** does the column match the given case-insensitive pattern */
  _ilike?: InputMaybe<Scalars["String"]>
  _in?: InputMaybe<Array<Scalars["String"]>>
  /** does the column match the given POSIX regular expression, case insensitive */
  _iregex?: InputMaybe<Scalars["String"]>
  _is_null?: InputMaybe<Scalars["Boolean"]>
  /** does the column match the given pattern */
  _like?: InputMaybe<Scalars["String"]>
  _lt?: InputMaybe<Scalars["String"]>
  _lte?: InputMaybe<Scalars["String"]>
  _neq?: InputMaybe<Scalars["String"]>
  /** does the column NOT match the given case-insensitive pattern */
  _nilike?: InputMaybe<Scalars["String"]>
  _nin?: InputMaybe<Array<Scalars["String"]>>
  /** does the column NOT match the given POSIX regular expression, case insensitive */
  _niregex?: InputMaybe<Scalars["String"]>
  /** does the column NOT match the given pattern */
  _nlike?: InputMaybe<Scalars["String"]>
  /** does the column NOT match the given POSIX regular expression, case sensitive */
  _nregex?: InputMaybe<Scalars["String"]>
  /** does the column NOT match the given SQL regular expression */
  _nsimilar?: InputMaybe<Scalars["String"]>
  /** does the column match the given POSIX regular expression, case sensitive */
  _regex?: InputMaybe<Scalars["String"]>
  /** does the column match the given SQL regular expression */
  _similar?: InputMaybe<Scalars["String"]>
}

/** columns and relationships of "ad_ranked_holders" */
export type Ad_Ranked_Holders = {
  __typename?: "ad_ranked_holders"
  address?: Maybe<Scalars["bytea"]>
  genesis_pass_token_scores?: Maybe<Scalars["numeric"]>
  min_block_number?: Maybe<Scalars["bigint"]>
  rank?: Maybe<Scalars["bigint"]>
  testnet_contributoers_scores?: Maybe<Scalars["numeric"]>
  testnet_poap_token_scores?: Maybe<Scalars["bigint"]>
  total_scores?: Maybe<Scalars["numeric"]>
}

/** Boolean expression to filter rows from the table "ad_ranked_holders". All fields are combined with a logical 'AND'. */
export type Ad_Ranked_Holders_Bool_Exp = {
  _and?: InputMaybe<Array<Ad_Ranked_Holders_Bool_Exp>>
  _not?: InputMaybe<Ad_Ranked_Holders_Bool_Exp>
  _or?: InputMaybe<Array<Ad_Ranked_Holders_Bool_Exp>>
  address?: InputMaybe<Bytea_Comparison_Exp>
  genesis_pass_token_scores?: InputMaybe<Numeric_Comparison_Exp>
  min_block_number?: InputMaybe<Bigint_Comparison_Exp>
  rank?: InputMaybe<Bigint_Comparison_Exp>
  testnet_contributoers_scores?: InputMaybe<Numeric_Comparison_Exp>
  testnet_poap_token_scores?: InputMaybe<Bigint_Comparison_Exp>
  total_scores?: InputMaybe<Numeric_Comparison_Exp>
}

/** Ordering options when selecting data from "ad_ranked_holders". */
export type Ad_Ranked_Holders_Order_By = {
  address?: InputMaybe<Order_By>
  genesis_pass_token_scores?: InputMaybe<Order_By>
  min_block_number?: InputMaybe<Order_By>
  rank?: InputMaybe<Order_By>
  testnet_contributoers_scores?: InputMaybe<Order_By>
  testnet_poap_token_scores?: InputMaybe<Order_By>
  total_scores?: InputMaybe<Order_By>
}

/** select columns of table "ad_ranked_holders" */
export enum Ad_Ranked_Holders_Select_Column {
  /** column name */
  Address = "address",
  /** column name */
  GenesisPassTokenScores = "genesis_pass_token_scores",
  /** column name */
  MinBlockNumber = "min_block_number",
  /** column name */
  Rank = "rank",
  /** column name */
  TestnetContributoersScores = "testnet_contributoers_scores",
  /** column name */
  TestnetPoapTokenScores = "testnet_poap_token_scores",
  /** column name */
  TotalScores = "total_scores",
}

/** Streaming cursor of the table "ad_ranked_holders" */
export type Ad_Ranked_Holders_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Ad_Ranked_Holders_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Ad_Ranked_Holders_Stream_Cursor_Value_Input = {
  address?: InputMaybe<Scalars["bytea"]>
  genesis_pass_token_scores?: InputMaybe<Scalars["numeric"]>
  min_block_number?: InputMaybe<Scalars["bigint"]>
  rank?: InputMaybe<Scalars["bigint"]>
  testnet_contributoers_scores?: InputMaybe<Scalars["numeric"]>
  testnet_poap_token_scores?: InputMaybe<Scalars["bigint"]>
  total_scores?: InputMaybe<Scalars["numeric"]>
}

/** columns and relationships of "ad_summary" */
export type Ad_Summary = {
  __typename?: "ad_summary"
  total_holders?: Maybe<Scalars["bigint"]>
}

/** Boolean expression to filter rows from the table "ad_summary". All fields are combined with a logical 'AND'. */
export type Ad_Summary_Bool_Exp = {
  _and?: InputMaybe<Array<Ad_Summary_Bool_Exp>>
  _not?: InputMaybe<Ad_Summary_Bool_Exp>
  _or?: InputMaybe<Array<Ad_Summary_Bool_Exp>>
  total_holders?: InputMaybe<Bigint_Comparison_Exp>
}

/** Ordering options when selecting data from "ad_summary". */
export type Ad_Summary_Order_By = {
  total_holders?: InputMaybe<Order_By>
}

/** select columns of table "ad_summary" */
export enum Ad_Summary_Select_Column {
  /** column name */
  TotalHolders = "total_holders",
}

/** Streaming cursor of the table "ad_summary" */
export type Ad_Summary_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Ad_Summary_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Ad_Summary_Stream_Cursor_Value_Input = {
  total_holders?: InputMaybe<Scalars["bigint"]>
}

/** Boolean expression to compare columns of type "bigint". All fields are combined with logical 'AND'. */
export type Bigint_Comparison_Exp = {
  _eq?: InputMaybe<Scalars["bigint"]>
  _gt?: InputMaybe<Scalars["bigint"]>
  _gte?: InputMaybe<Scalars["bigint"]>
  _in?: InputMaybe<Array<Scalars["bigint"]>>
  _is_null?: InputMaybe<Scalars["Boolean"]>
  _lt?: InputMaybe<Scalars["bigint"]>
  _lte?: InputMaybe<Scalars["bigint"]>
  _neq?: InputMaybe<Scalars["bigint"]>
  _nin?: InputMaybe<Array<Scalars["bigint"]>>
}

/** columns and relationships of "bsc.blocks" */
export type Bsc_Blocks = {
  __typename?: "bsc_blocks"
  base_fee_per_gas?: Maybe<Scalars["numeric"]>
  difficulty: Scalars["numeric"]
  gas_limit: Scalars["numeric"]
  gas_used: Scalars["numeric"]
  hash: Scalars["bytea"]
  miner: Scalars["bytea"]
  nonce: Scalars["bytea"]
  number: Scalars["bigint"]
  parent_hash: Scalars["bytea"]
  size: Scalars["numeric"]
  time: Scalars["timestamptz"]
  total_difficulty: Scalars["numeric"]
}

/** Boolean expression to filter rows from the table "bsc.blocks". All fields are combined with a logical 'AND'. */
export type Bsc_Blocks_Bool_Exp = {
  _and?: InputMaybe<Array<Bsc_Blocks_Bool_Exp>>
  _not?: InputMaybe<Bsc_Blocks_Bool_Exp>
  _or?: InputMaybe<Array<Bsc_Blocks_Bool_Exp>>
  base_fee_per_gas?: InputMaybe<Numeric_Comparison_Exp>
  difficulty?: InputMaybe<Numeric_Comparison_Exp>
  gas_limit?: InputMaybe<Numeric_Comparison_Exp>
  gas_used?: InputMaybe<Numeric_Comparison_Exp>
  hash?: InputMaybe<Bytea_Comparison_Exp>
  miner?: InputMaybe<Bytea_Comparison_Exp>
  nonce?: InputMaybe<Bytea_Comparison_Exp>
  number?: InputMaybe<Bigint_Comparison_Exp>
  parent_hash?: InputMaybe<Bytea_Comparison_Exp>
  size?: InputMaybe<Numeric_Comparison_Exp>
  time?: InputMaybe<Timestamptz_Comparison_Exp>
  total_difficulty?: InputMaybe<Numeric_Comparison_Exp>
}

/** Ordering options when selecting data from "bsc.blocks". */
export type Bsc_Blocks_Order_By = {
  base_fee_per_gas?: InputMaybe<Order_By>
  difficulty?: InputMaybe<Order_By>
  gas_limit?: InputMaybe<Order_By>
  gas_used?: InputMaybe<Order_By>
  hash?: InputMaybe<Order_By>
  miner?: InputMaybe<Order_By>
  nonce?: InputMaybe<Order_By>
  number?: InputMaybe<Order_By>
  parent_hash?: InputMaybe<Order_By>
  size?: InputMaybe<Order_By>
  time?: InputMaybe<Order_By>
  total_difficulty?: InputMaybe<Order_By>
}

/** select columns of table "bsc.blocks" */
export enum Bsc_Blocks_Select_Column {
  /** column name */
  BaseFeePerGas = "base_fee_per_gas",
  /** column name */
  Difficulty = "difficulty",
  /** column name */
  GasLimit = "gas_limit",
  /** column name */
  GasUsed = "gas_used",
  /** column name */
  Hash = "hash",
  /** column name */
  Miner = "miner",
  /** column name */
  Nonce = "nonce",
  /** column name */
  Number = "number",
  /** column name */
  ParentHash = "parent_hash",
  /** column name */
  Size = "size",
  /** column name */
  Time = "time",
  /** column name */
  TotalDifficulty = "total_difficulty",
}

/** Streaming cursor of the table "bsc_blocks" */
export type Bsc_Blocks_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Bsc_Blocks_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Bsc_Blocks_Stream_Cursor_Value_Input = {
  base_fee_per_gas?: InputMaybe<Scalars["numeric"]>
  difficulty?: InputMaybe<Scalars["numeric"]>
  gas_limit?: InputMaybe<Scalars["numeric"]>
  gas_used?: InputMaybe<Scalars["numeric"]>
  hash?: InputMaybe<Scalars["bytea"]>
  miner?: InputMaybe<Scalars["bytea"]>
  nonce?: InputMaybe<Scalars["bytea"]>
  number?: InputMaybe<Scalars["bigint"]>
  parent_hash?: InputMaybe<Scalars["bytea"]>
  size?: InputMaybe<Scalars["numeric"]>
  time?: InputMaybe<Scalars["timestamptz"]>
  total_difficulty?: InputMaybe<Scalars["numeric"]>
}

/** Boolean expression to compare columns of type "bytea". All fields are combined with logical 'AND'. */
export type Bytea_Comparison_Exp = {
  _eq?: InputMaybe<Scalars["bytea"]>
  _gt?: InputMaybe<Scalars["bytea"]>
  _gte?: InputMaybe<Scalars["bytea"]>
  _in?: InputMaybe<Array<Scalars["bytea"]>>
  _is_null?: InputMaybe<Scalars["Boolean"]>
  _lt?: InputMaybe<Scalars["bytea"]>
  _lte?: InputMaybe<Scalars["bytea"]>
  _neq?: InputMaybe<Scalars["bytea"]>
  _nin?: InputMaybe<Array<Scalars["bytea"]>>
}

/** columns and relationships of "closed_market_orders" */
export type Closed_Market_Orders = {
  __typename?: "closed_market_orders"
  close_fee: Scalars["numeric"]
  closed_block: Scalars["bigint"]
  closed_time: Scalars["bigint"]
  deposited_margin: Scalars["numeric"]
  funding_fee: Scalars["numeric"]
  is_buy: Scalars["Boolean"]
  liquidation_fee: Scalars["numeric"]
  logs: Scalars["jsonb"]
  open_block: Scalars["bigint"]
  open_fee: Scalars["numeric"]
  open_leverage: Scalars["numeric"]
  open_margin: Scalars["numeric"]
  open_price: Scalars["numeric"]
  open_referral_code: Scalars["String"]
  open_referral_fee: Scalars["numeric"]
  open_referrer: Scalars["bytea"]
  open_time: Scalars["bigint"]
  order_hash: Scalars["bytea"]
  pnl: Scalars["numeric"]
  pnl_net_fee: Scalars["numeric"]
  price_id: Scalars["bytea"]
  rollover_fee: Scalars["numeric"]
  settled: Scalars["numeric"]
  user: Scalars["bytea"]
  withdrawn_margin: Scalars["numeric"]
}

/** columns and relationships of "closed_market_orders" */
export type Closed_Market_OrdersLogsArgs = {
  path?: InputMaybe<Scalars["String"]>
}

/** Boolean expression to filter rows from the table "closed_market_orders". All fields are combined with a logical 'AND'. */
export type Closed_Market_Orders_Bool_Exp = {
  _and?: InputMaybe<Array<Closed_Market_Orders_Bool_Exp>>
  _not?: InputMaybe<Closed_Market_Orders_Bool_Exp>
  _or?: InputMaybe<Array<Closed_Market_Orders_Bool_Exp>>
  close_fee?: InputMaybe<Numeric_Comparison_Exp>
  closed_block?: InputMaybe<Bigint_Comparison_Exp>
  closed_time?: InputMaybe<Bigint_Comparison_Exp>
  deposited_margin?: InputMaybe<Numeric_Comparison_Exp>
  funding_fee?: InputMaybe<Numeric_Comparison_Exp>
  is_buy?: InputMaybe<Boolean_Comparison_Exp>
  liquidation_fee?: InputMaybe<Numeric_Comparison_Exp>
  logs?: InputMaybe<Jsonb_Comparison_Exp>
  open_block?: InputMaybe<Bigint_Comparison_Exp>
  open_fee?: InputMaybe<Numeric_Comparison_Exp>
  open_leverage?: InputMaybe<Numeric_Comparison_Exp>
  open_margin?: InputMaybe<Numeric_Comparison_Exp>
  open_price?: InputMaybe<Numeric_Comparison_Exp>
  open_referral_code?: InputMaybe<String_Comparison_Exp>
  open_referral_fee?: InputMaybe<Numeric_Comparison_Exp>
  open_referrer?: InputMaybe<Bytea_Comparison_Exp>
  open_time?: InputMaybe<Bigint_Comparison_Exp>
  order_hash?: InputMaybe<Bytea_Comparison_Exp>
  pnl?: InputMaybe<Numeric_Comparison_Exp>
  pnl_net_fee?: InputMaybe<Numeric_Comparison_Exp>
  price_id?: InputMaybe<Bytea_Comparison_Exp>
  rollover_fee?: InputMaybe<Numeric_Comparison_Exp>
  settled?: InputMaybe<Numeric_Comparison_Exp>
  user?: InputMaybe<Bytea_Comparison_Exp>
  withdrawn_margin?: InputMaybe<Numeric_Comparison_Exp>
}

/** Ordering options when selecting data from "closed_market_orders". */
export type Closed_Market_Orders_Order_By = {
  close_fee?: InputMaybe<Order_By>
  closed_block?: InputMaybe<Order_By>
  closed_time?: InputMaybe<Order_By>
  deposited_margin?: InputMaybe<Order_By>
  funding_fee?: InputMaybe<Order_By>
  is_buy?: InputMaybe<Order_By>
  liquidation_fee?: InputMaybe<Order_By>
  logs?: InputMaybe<Order_By>
  open_block?: InputMaybe<Order_By>
  open_fee?: InputMaybe<Order_By>
  open_leverage?: InputMaybe<Order_By>
  open_margin?: InputMaybe<Order_By>
  open_price?: InputMaybe<Order_By>
  open_referral_code?: InputMaybe<Order_By>
  open_referral_fee?: InputMaybe<Order_By>
  open_referrer?: InputMaybe<Order_By>
  open_time?: InputMaybe<Order_By>
  order_hash?: InputMaybe<Order_By>
  pnl?: InputMaybe<Order_By>
  pnl_net_fee?: InputMaybe<Order_By>
  price_id?: InputMaybe<Order_By>
  rollover_fee?: InputMaybe<Order_By>
  settled?: InputMaybe<Order_By>
  user?: InputMaybe<Order_By>
  withdrawn_margin?: InputMaybe<Order_By>
}

/** select columns of table "closed_market_orders" */
export enum Closed_Market_Orders_Select_Column {
  /** column name */
  CloseFee = "close_fee",
  /** column name */
  ClosedBlock = "closed_block",
  /** column name */
  ClosedTime = "closed_time",
  /** column name */
  DepositedMargin = "deposited_margin",
  /** column name */
  FundingFee = "funding_fee",
  /** column name */
  IsBuy = "is_buy",
  /** column name */
  LiquidationFee = "liquidation_fee",
  /** column name */
  Logs = "logs",
  /** column name */
  OpenBlock = "open_block",
  /** column name */
  OpenFee = "open_fee",
  /** column name */
  OpenLeverage = "open_leverage",
  /** column name */
  OpenMargin = "open_margin",
  /** column name */
  OpenPrice = "open_price",
  /** column name */
  OpenReferralCode = "open_referral_code",
  /** column name */
  OpenReferralFee = "open_referral_fee",
  /** column name */
  OpenReferrer = "open_referrer",
  /** column name */
  OpenTime = "open_time",
  /** column name */
  OrderHash = "order_hash",
  /** column name */
  Pnl = "pnl",
  /** column name */
  PnlNetFee = "pnl_net_fee",
  /** column name */
  PriceId = "price_id",
  /** column name */
  RolloverFee = "rollover_fee",
  /** column name */
  Settled = "settled",
  /** column name */
  User = "user",
  /** column name */
  WithdrawnMargin = "withdrawn_margin",
}

/** Streaming cursor of the table "closed_market_orders" */
export type Closed_Market_Orders_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Closed_Market_Orders_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Closed_Market_Orders_Stream_Cursor_Value_Input = {
  close_fee?: InputMaybe<Scalars["numeric"]>
  closed_block?: InputMaybe<Scalars["bigint"]>
  closed_time?: InputMaybe<Scalars["bigint"]>
  deposited_margin?: InputMaybe<Scalars["numeric"]>
  funding_fee?: InputMaybe<Scalars["numeric"]>
  is_buy?: InputMaybe<Scalars["Boolean"]>
  liquidation_fee?: InputMaybe<Scalars["numeric"]>
  logs?: InputMaybe<Scalars["jsonb"]>
  open_block?: InputMaybe<Scalars["bigint"]>
  open_fee?: InputMaybe<Scalars["numeric"]>
  open_leverage?: InputMaybe<Scalars["numeric"]>
  open_margin?: InputMaybe<Scalars["numeric"]>
  open_price?: InputMaybe<Scalars["numeric"]>
  open_referral_code?: InputMaybe<Scalars["String"]>
  open_referral_fee?: InputMaybe<Scalars["numeric"]>
  open_referrer?: InputMaybe<Scalars["bytea"]>
  open_time?: InputMaybe<Scalars["bigint"]>
  order_hash?: InputMaybe<Scalars["bytea"]>
  pnl?: InputMaybe<Scalars["numeric"]>
  pnl_net_fee?: InputMaybe<Scalars["numeric"]>
  price_id?: InputMaybe<Scalars["bytea"]>
  rollover_fee?: InputMaybe<Scalars["numeric"]>
  settled?: InputMaybe<Scalars["numeric"]>
  user?: InputMaybe<Scalars["bytea"]>
  withdrawn_margin?: InputMaybe<Scalars["numeric"]>
}

/** columns and relationships of "cp_referee_summary" */
export type Cp_Referee_Summary = {
  __typename?: "cp_referee_summary"
  referral_fee?: Maybe<Scalars["numeric"]>
  referred_fee?: Maybe<Scalars["numeric"]>
  trade_user?: Maybe<Scalars["bytea"]>
  trade_volume?: Maybe<Scalars["numeric"]>
}

/** Boolean expression to filter rows from the table "cp_referee_summary". All fields are combined with a logical 'AND'. */
export type Cp_Referee_Summary_Bool_Exp = {
  _and?: InputMaybe<Array<Cp_Referee_Summary_Bool_Exp>>
  _not?: InputMaybe<Cp_Referee_Summary_Bool_Exp>
  _or?: InputMaybe<Array<Cp_Referee_Summary_Bool_Exp>>
  referral_fee?: InputMaybe<Numeric_Comparison_Exp>
  referred_fee?: InputMaybe<Numeric_Comparison_Exp>
  trade_user?: InputMaybe<Bytea_Comparison_Exp>
  trade_volume?: InputMaybe<Numeric_Comparison_Exp>
}

/** Ordering options when selecting data from "cp_referee_summary". */
export type Cp_Referee_Summary_Order_By = {
  referral_fee?: InputMaybe<Order_By>
  referred_fee?: InputMaybe<Order_By>
  trade_user?: InputMaybe<Order_By>
  trade_volume?: InputMaybe<Order_By>
}

/** select columns of table "cp_referee_summary" */
export enum Cp_Referee_Summary_Select_Column {
  /** column name */
  ReferralFee = "referral_fee",
  /** column name */
  ReferredFee = "referred_fee",
  /** column name */
  TradeUser = "trade_user",
  /** column name */
  TradeVolume = "trade_volume",
}

/** Streaming cursor of the table "cp_referee_summary" */
export type Cp_Referee_Summary_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Cp_Referee_Summary_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Cp_Referee_Summary_Stream_Cursor_Value_Input = {
  referral_fee?: InputMaybe<Scalars["numeric"]>
  referred_fee?: InputMaybe<Scalars["numeric"]>
  trade_user?: InputMaybe<Scalars["bytea"]>
  trade_volume?: InputMaybe<Scalars["numeric"]>
}

/** columns and relationships of "cp_referral_counter" */
export type Cp_Referral_Counter = {
  __typename?: "cp_referral_counter"
  count?: Maybe<Scalars["bigint"]>
  referralCode?: Maybe<Scalars["bytea"]>
  referrer?: Maybe<Scalars["bytea"]>
}

/** Boolean expression to filter rows from the table "cp_referral_counter". All fields are combined with a logical 'AND'. */
export type Cp_Referral_Counter_Bool_Exp = {
  _and?: InputMaybe<Array<Cp_Referral_Counter_Bool_Exp>>
  _not?: InputMaybe<Cp_Referral_Counter_Bool_Exp>
  _or?: InputMaybe<Array<Cp_Referral_Counter_Bool_Exp>>
  count?: InputMaybe<Bigint_Comparison_Exp>
  referralCode?: InputMaybe<Bytea_Comparison_Exp>
  referrer?: InputMaybe<Bytea_Comparison_Exp>
}

/** Ordering options when selecting data from "cp_referral_counter". */
export type Cp_Referral_Counter_Order_By = {
  count?: InputMaybe<Order_By>
  referralCode?: InputMaybe<Order_By>
  referrer?: InputMaybe<Order_By>
}

/** select columns of table "cp_referral_counter" */
export enum Cp_Referral_Counter_Select_Column {
  /** column name */
  Count = "count",
  /** column name */
  ReferralCode = "referralCode",
  /** column name */
  Referrer = "referrer",
}

/** Streaming cursor of the table "cp_referral_counter" */
export type Cp_Referral_Counter_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Cp_Referral_Counter_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Cp_Referral_Counter_Stream_Cursor_Value_Input = {
  count?: InputMaybe<Scalars["bigint"]>
  referralCode?: InputMaybe<Scalars["bytea"]>
  referrer?: InputMaybe<Scalars["bytea"]>
}

/** columns and relationships of "cp_referrer_summary" */
export type Cp_Referrer_Summary = {
  __typename?: "cp_referrer_summary"
  fee_referralCode?: Maybe<Scalars["bytea"]>
  fee_referrer?: Maybe<Scalars["bytea"]>
  referral_fee?: Maybe<Scalars["numeric"]>
  referred_fee?: Maybe<Scalars["numeric"]>
  trade_volume?: Maybe<Scalars["numeric"]>
}

/** aggregated selection of "cp_referrer_summary" */
export type Cp_Referrer_Summary_Aggregate = {
  __typename?: "cp_referrer_summary_aggregate"
  aggregate?: Maybe<Cp_Referrer_Summary_Aggregate_Fields>
  nodes: Array<Cp_Referrer_Summary>
}

/** aggregate fields of "cp_referrer_summary" */
export type Cp_Referrer_Summary_Aggregate_Fields = {
  __typename?: "cp_referrer_summary_aggregate_fields"
  avg?: Maybe<Cp_Referrer_Summary_Avg_Fields>
  count: Scalars["Int"]
  max?: Maybe<Cp_Referrer_Summary_Max_Fields>
  min?: Maybe<Cp_Referrer_Summary_Min_Fields>
  stddev?: Maybe<Cp_Referrer_Summary_Stddev_Fields>
  stddev_pop?: Maybe<Cp_Referrer_Summary_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Cp_Referrer_Summary_Stddev_Samp_Fields>
  sum?: Maybe<Cp_Referrer_Summary_Sum_Fields>
  var_pop?: Maybe<Cp_Referrer_Summary_Var_Pop_Fields>
  var_samp?: Maybe<Cp_Referrer_Summary_Var_Samp_Fields>
  variance?: Maybe<Cp_Referrer_Summary_Variance_Fields>
}

/** aggregate fields of "cp_referrer_summary" */
export type Cp_Referrer_Summary_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Cp_Referrer_Summary_Select_Column>>
  distinct?: InputMaybe<Scalars["Boolean"]>
}

/** aggregate avg on columns */
export type Cp_Referrer_Summary_Avg_Fields = {
  __typename?: "cp_referrer_summary_avg_fields"
  referral_fee?: Maybe<Scalars["Float"]>
  referred_fee?: Maybe<Scalars["Float"]>
  trade_volume?: Maybe<Scalars["Float"]>
}

/** Boolean expression to filter rows from the table "cp_referrer_summary". All fields are combined with a logical 'AND'. */
export type Cp_Referrer_Summary_Bool_Exp = {
  _and?: InputMaybe<Array<Cp_Referrer_Summary_Bool_Exp>>
  _not?: InputMaybe<Cp_Referrer_Summary_Bool_Exp>
  _or?: InputMaybe<Array<Cp_Referrer_Summary_Bool_Exp>>
  fee_referralCode?: InputMaybe<Bytea_Comparison_Exp>
  fee_referrer?: InputMaybe<Bytea_Comparison_Exp>
  referral_fee?: InputMaybe<Numeric_Comparison_Exp>
  referred_fee?: InputMaybe<Numeric_Comparison_Exp>
  trade_volume?: InputMaybe<Numeric_Comparison_Exp>
}

/** aggregate max on columns */
export type Cp_Referrer_Summary_Max_Fields = {
  __typename?: "cp_referrer_summary_max_fields"
  referral_fee?: Maybe<Scalars["numeric"]>
  referred_fee?: Maybe<Scalars["numeric"]>
  trade_volume?: Maybe<Scalars["numeric"]>
}

/** aggregate min on columns */
export type Cp_Referrer_Summary_Min_Fields = {
  __typename?: "cp_referrer_summary_min_fields"
  referral_fee?: Maybe<Scalars["numeric"]>
  referred_fee?: Maybe<Scalars["numeric"]>
  trade_volume?: Maybe<Scalars["numeric"]>
}

/** Ordering options when selecting data from "cp_referrer_summary". */
export type Cp_Referrer_Summary_Order_By = {
  fee_referralCode?: InputMaybe<Order_By>
  fee_referrer?: InputMaybe<Order_By>
  referral_fee?: InputMaybe<Order_By>
  referred_fee?: InputMaybe<Order_By>
  trade_volume?: InputMaybe<Order_By>
}

/** select columns of table "cp_referrer_summary" */
export enum Cp_Referrer_Summary_Select_Column {
  /** column name */
  FeeReferralCode = "fee_referralCode",
  /** column name */
  FeeReferrer = "fee_referrer",
  /** column name */
  ReferralFee = "referral_fee",
  /** column name */
  ReferredFee = "referred_fee",
  /** column name */
  TradeVolume = "trade_volume",
}

/** aggregate stddev on columns */
export type Cp_Referrer_Summary_Stddev_Fields = {
  __typename?: "cp_referrer_summary_stddev_fields"
  referral_fee?: Maybe<Scalars["Float"]>
  referred_fee?: Maybe<Scalars["Float"]>
  trade_volume?: Maybe<Scalars["Float"]>
}

/** aggregate stddev_pop on columns */
export type Cp_Referrer_Summary_Stddev_Pop_Fields = {
  __typename?: "cp_referrer_summary_stddev_pop_fields"
  referral_fee?: Maybe<Scalars["Float"]>
  referred_fee?: Maybe<Scalars["Float"]>
  trade_volume?: Maybe<Scalars["Float"]>
}

/** aggregate stddev_samp on columns */
export type Cp_Referrer_Summary_Stddev_Samp_Fields = {
  __typename?: "cp_referrer_summary_stddev_samp_fields"
  referral_fee?: Maybe<Scalars["Float"]>
  referred_fee?: Maybe<Scalars["Float"]>
  trade_volume?: Maybe<Scalars["Float"]>
}

/** Streaming cursor of the table "cp_referrer_summary" */
export type Cp_Referrer_Summary_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Cp_Referrer_Summary_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Cp_Referrer_Summary_Stream_Cursor_Value_Input = {
  fee_referralCode?: InputMaybe<Scalars["bytea"]>
  fee_referrer?: InputMaybe<Scalars["bytea"]>
  referral_fee?: InputMaybe<Scalars["numeric"]>
  referred_fee?: InputMaybe<Scalars["numeric"]>
  trade_volume?: InputMaybe<Scalars["numeric"]>
}

/** aggregate sum on columns */
export type Cp_Referrer_Summary_Sum_Fields = {
  __typename?: "cp_referrer_summary_sum_fields"
  referral_fee?: Maybe<Scalars["numeric"]>
  referred_fee?: Maybe<Scalars["numeric"]>
  trade_volume?: Maybe<Scalars["numeric"]>
}

/** aggregate var_pop on columns */
export type Cp_Referrer_Summary_Var_Pop_Fields = {
  __typename?: "cp_referrer_summary_var_pop_fields"
  referral_fee?: Maybe<Scalars["Float"]>
  referred_fee?: Maybe<Scalars["Float"]>
  trade_volume?: Maybe<Scalars["Float"]>
}

/** aggregate var_samp on columns */
export type Cp_Referrer_Summary_Var_Samp_Fields = {
  __typename?: "cp_referrer_summary_var_samp_fields"
  referral_fee?: Maybe<Scalars["Float"]>
  referred_fee?: Maybe<Scalars["Float"]>
  trade_volume?: Maybe<Scalars["Float"]>
}

/** aggregate variance on columns */
export type Cp_Referrer_Summary_Variance_Fields = {
  __typename?: "cp_referrer_summary_variance_fields"
  referral_fee?: Maybe<Scalars["Float"]>
  referred_fee?: Maybe<Scalars["Float"]>
  trade_volume?: Maybe<Scalars["Float"]>
}

/** ordering argument of a cursor */
export enum Cursor_Ordering {
  /** ascending ordering of the cursor */
  Asc = "ASC",
  /** descending ordering of the cursor */
  Desc = "DESC",
}

/** columns and relationships of "genesis_pass_holders" */
export type Genesis_Pass_Holders = {
  __typename?: "genesis_pass_holders"
  contract_address?: Maybe<Scalars["bytea"]>
  evt_block_number?: Maybe<Scalars["bigint"]>
  evt_index?: Maybe<Scalars["numeric"]>
  evt_tx_hash?: Maybe<Scalars["bytea"]>
  evt_tx_index?: Maybe<Scalars["numeric"]>
  from?: Maybe<Scalars["bytea"]>
  to?: Maybe<Scalars["bytea"]>
  tokenId?: Maybe<Scalars["numeric"]>
}

/** Boolean expression to filter rows from the table "genesis_pass_holders". All fields are combined with a logical 'AND'. */
export type Genesis_Pass_Holders_Bool_Exp = {
  _and?: InputMaybe<Array<Genesis_Pass_Holders_Bool_Exp>>
  _not?: InputMaybe<Genesis_Pass_Holders_Bool_Exp>
  _or?: InputMaybe<Array<Genesis_Pass_Holders_Bool_Exp>>
  contract_address?: InputMaybe<Bytea_Comparison_Exp>
  evt_block_number?: InputMaybe<Bigint_Comparison_Exp>
  evt_index?: InputMaybe<Numeric_Comparison_Exp>
  evt_tx_hash?: InputMaybe<Bytea_Comparison_Exp>
  evt_tx_index?: InputMaybe<Numeric_Comparison_Exp>
  from?: InputMaybe<Bytea_Comparison_Exp>
  to?: InputMaybe<Bytea_Comparison_Exp>
  tokenId?: InputMaybe<Numeric_Comparison_Exp>
}

/** Ordering options when selecting data from "genesis_pass_holders". */
export type Genesis_Pass_Holders_Order_By = {
  contract_address?: InputMaybe<Order_By>
  evt_block_number?: InputMaybe<Order_By>
  evt_index?: InputMaybe<Order_By>
  evt_tx_hash?: InputMaybe<Order_By>
  evt_tx_index?: InputMaybe<Order_By>
  from?: InputMaybe<Order_By>
  to?: InputMaybe<Order_By>
  tokenId?: InputMaybe<Order_By>
}

/** select columns of table "genesis_pass_holders" */
export enum Genesis_Pass_Holders_Select_Column {
  /** column name */
  ContractAddress = "contract_address",
  /** column name */
  EvtBlockNumber = "evt_block_number",
  /** column name */
  EvtIndex = "evt_index",
  /** column name */
  EvtTxHash = "evt_tx_hash",
  /** column name */
  EvtTxIndex = "evt_tx_index",
  /** column name */
  From = "from",
  /** column name */
  To = "to",
  /** column name */
  TokenId = "tokenId",
}

/** Streaming cursor of the table "genesis_pass_holders" */
export type Genesis_Pass_Holders_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Genesis_Pass_Holders_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Genesis_Pass_Holders_Stream_Cursor_Value_Input = {
  contract_address?: InputMaybe<Scalars["bytea"]>
  evt_block_number?: InputMaybe<Scalars["bigint"]>
  evt_index?: InputMaybe<Scalars["numeric"]>
  evt_tx_hash?: InputMaybe<Scalars["bytea"]>
  evt_tx_index?: InputMaybe<Scalars["numeric"]>
  from?: InputMaybe<Scalars["bytea"]>
  to?: InputMaybe<Scalars["bytea"]>
  tokenId?: InputMaybe<Scalars["numeric"]>
}

export type Jsonb_Cast_Exp = {
  String?: InputMaybe<String_Comparison_Exp>
}

/** Boolean expression to compare columns of type "jsonb". All fields are combined with logical 'AND'. */
export type Jsonb_Comparison_Exp = {
  _cast?: InputMaybe<Jsonb_Cast_Exp>
  /** is the column contained in the given json value */
  _contained_in?: InputMaybe<Scalars["jsonb"]>
  /** does the column contain the given json value at the top level */
  _contains?: InputMaybe<Scalars["jsonb"]>
  _eq?: InputMaybe<Scalars["jsonb"]>
  _gt?: InputMaybe<Scalars["jsonb"]>
  _gte?: InputMaybe<Scalars["jsonb"]>
  /** does the string exist as a top-level key in the column */
  _has_key?: InputMaybe<Scalars["String"]>
  /** do all of these strings exist as top-level keys in the column */
  _has_keys_all?: InputMaybe<Array<Scalars["String"]>>
  /** do any of these strings exist as top-level keys in the column */
  _has_keys_any?: InputMaybe<Array<Scalars["String"]>>
  _in?: InputMaybe<Array<Scalars["jsonb"]>>
  _is_null?: InputMaybe<Scalars["Boolean"]>
  _lt?: InputMaybe<Scalars["jsonb"]>
  _lte?: InputMaybe<Scalars["jsonb"]>
  _neq?: InputMaybe<Scalars["jsonb"]>
  _nin?: InputMaybe<Array<Scalars["jsonb"]>>
}

/** columns and relationships of "latest_pyth_prices" */
export type Latest_Pyth_Prices = {
  __typename?: "latest_pyth_prices"
  conf: Scalars["numeric"]
  ema_conf: Scalars["numeric"]
  ema_price: Scalars["numeric"]
  env: Scalars["String"]
  expo: Scalars["Int"]
  price: Scalars["numeric"]
  price_data?: Maybe<Scalars["bytea"]>
  price_id: Scalars["bytea"]
  publish_time: Scalars["timestamptz"]
  symbol: Scalars["String"]
}

/** aggregated selection of "latest_pyth_prices" */
export type Latest_Pyth_Prices_Aggregate = {
  __typename?: "latest_pyth_prices_aggregate"
  aggregate?: Maybe<Latest_Pyth_Prices_Aggregate_Fields>
  nodes: Array<Latest_Pyth_Prices>
}

/** aggregate fields of "latest_pyth_prices" */
export type Latest_Pyth_Prices_Aggregate_Fields = {
  __typename?: "latest_pyth_prices_aggregate_fields"
  avg?: Maybe<Latest_Pyth_Prices_Avg_Fields>
  count: Scalars["Int"]
  max?: Maybe<Latest_Pyth_Prices_Max_Fields>
  min?: Maybe<Latest_Pyth_Prices_Min_Fields>
  stddev?: Maybe<Latest_Pyth_Prices_Stddev_Fields>
  stddev_pop?: Maybe<Latest_Pyth_Prices_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Latest_Pyth_Prices_Stddev_Samp_Fields>
  sum?: Maybe<Latest_Pyth_Prices_Sum_Fields>
  var_pop?: Maybe<Latest_Pyth_Prices_Var_Pop_Fields>
  var_samp?: Maybe<Latest_Pyth_Prices_Var_Samp_Fields>
  variance?: Maybe<Latest_Pyth_Prices_Variance_Fields>
}

/** aggregate fields of "latest_pyth_prices" */
export type Latest_Pyth_Prices_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Latest_Pyth_Prices_Select_Column>>
  distinct?: InputMaybe<Scalars["Boolean"]>
}

/** aggregate avg on columns */
export type Latest_Pyth_Prices_Avg_Fields = {
  __typename?: "latest_pyth_prices_avg_fields"
  conf?: Maybe<Scalars["Float"]>
  ema_conf?: Maybe<Scalars["Float"]>
  ema_price?: Maybe<Scalars["Float"]>
  expo?: Maybe<Scalars["Float"]>
  price?: Maybe<Scalars["Float"]>
}

/** Boolean expression to filter rows from the table "latest_pyth_prices". All fields are combined with a logical 'AND'. */
export type Latest_Pyth_Prices_Bool_Exp = {
  _and?: InputMaybe<Array<Latest_Pyth_Prices_Bool_Exp>>
  _not?: InputMaybe<Latest_Pyth_Prices_Bool_Exp>
  _or?: InputMaybe<Array<Latest_Pyth_Prices_Bool_Exp>>
  conf?: InputMaybe<Numeric_Comparison_Exp>
  ema_conf?: InputMaybe<Numeric_Comparison_Exp>
  ema_price?: InputMaybe<Numeric_Comparison_Exp>
  env?: InputMaybe<String_Comparison_Exp>
  expo?: InputMaybe<Int_Comparison_Exp>
  price?: InputMaybe<Numeric_Comparison_Exp>
  price_data?: InputMaybe<Bytea_Comparison_Exp>
  price_id?: InputMaybe<Bytea_Comparison_Exp>
  publish_time?: InputMaybe<Timestamptz_Comparison_Exp>
  symbol?: InputMaybe<String_Comparison_Exp>
}

/** aggregate max on columns */
export type Latest_Pyth_Prices_Max_Fields = {
  __typename?: "latest_pyth_prices_max_fields"
  conf?: Maybe<Scalars["numeric"]>
  ema_conf?: Maybe<Scalars["numeric"]>
  ema_price?: Maybe<Scalars["numeric"]>
  env?: Maybe<Scalars["String"]>
  expo?: Maybe<Scalars["Int"]>
  price?: Maybe<Scalars["numeric"]>
  publish_time?: Maybe<Scalars["timestamptz"]>
  symbol?: Maybe<Scalars["String"]>
}

/** aggregate min on columns */
export type Latest_Pyth_Prices_Min_Fields = {
  __typename?: "latest_pyth_prices_min_fields"
  conf?: Maybe<Scalars["numeric"]>
  ema_conf?: Maybe<Scalars["numeric"]>
  ema_price?: Maybe<Scalars["numeric"]>
  env?: Maybe<Scalars["String"]>
  expo?: Maybe<Scalars["Int"]>
  price?: Maybe<Scalars["numeric"]>
  publish_time?: Maybe<Scalars["timestamptz"]>
  symbol?: Maybe<Scalars["String"]>
}

/** Ordering options when selecting data from "latest_pyth_prices". */
export type Latest_Pyth_Prices_Order_By = {
  conf?: InputMaybe<Order_By>
  ema_conf?: InputMaybe<Order_By>
  ema_price?: InputMaybe<Order_By>
  env?: InputMaybe<Order_By>
  expo?: InputMaybe<Order_By>
  price?: InputMaybe<Order_By>
  price_data?: InputMaybe<Order_By>
  price_id?: InputMaybe<Order_By>
  publish_time?: InputMaybe<Order_By>
  symbol?: InputMaybe<Order_By>
}

/** select columns of table "latest_pyth_prices" */
export enum Latest_Pyth_Prices_Select_Column {
  /** column name */
  Conf = "conf",
  /** column name */
  EmaConf = "ema_conf",
  /** column name */
  EmaPrice = "ema_price",
  /** column name */
  Env = "env",
  /** column name */
  Expo = "expo",
  /** column name */
  Price = "price",
  /** column name */
  PriceData = "price_data",
  /** column name */
  PriceId = "price_id",
  /** column name */
  PublishTime = "publish_time",
  /** column name */
  Symbol = "symbol",
}

/** aggregate stddev on columns */
export type Latest_Pyth_Prices_Stddev_Fields = {
  __typename?: "latest_pyth_prices_stddev_fields"
  conf?: Maybe<Scalars["Float"]>
  ema_conf?: Maybe<Scalars["Float"]>
  ema_price?: Maybe<Scalars["Float"]>
  expo?: Maybe<Scalars["Float"]>
  price?: Maybe<Scalars["Float"]>
}

/** aggregate stddev_pop on columns */
export type Latest_Pyth_Prices_Stddev_Pop_Fields = {
  __typename?: "latest_pyth_prices_stddev_pop_fields"
  conf?: Maybe<Scalars["Float"]>
  ema_conf?: Maybe<Scalars["Float"]>
  ema_price?: Maybe<Scalars["Float"]>
  expo?: Maybe<Scalars["Float"]>
  price?: Maybe<Scalars["Float"]>
}

/** aggregate stddev_samp on columns */
export type Latest_Pyth_Prices_Stddev_Samp_Fields = {
  __typename?: "latest_pyth_prices_stddev_samp_fields"
  conf?: Maybe<Scalars["Float"]>
  ema_conf?: Maybe<Scalars["Float"]>
  ema_price?: Maybe<Scalars["Float"]>
  expo?: Maybe<Scalars["Float"]>
  price?: Maybe<Scalars["Float"]>
}

/** Streaming cursor of the table "latest_pyth_prices" */
export type Latest_Pyth_Prices_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Latest_Pyth_Prices_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Latest_Pyth_Prices_Stream_Cursor_Value_Input = {
  conf?: InputMaybe<Scalars["numeric"]>
  ema_conf?: InputMaybe<Scalars["numeric"]>
  ema_price?: InputMaybe<Scalars["numeric"]>
  env?: InputMaybe<Scalars["String"]>
  expo?: InputMaybe<Scalars["Int"]>
  price?: InputMaybe<Scalars["numeric"]>
  price_data?: InputMaybe<Scalars["bytea"]>
  price_id?: InputMaybe<Scalars["bytea"]>
  publish_time?: InputMaybe<Scalars["timestamptz"]>
  symbol?: InputMaybe<Scalars["String"]>
}

/** aggregate sum on columns */
export type Latest_Pyth_Prices_Sum_Fields = {
  __typename?: "latest_pyth_prices_sum_fields"
  conf?: Maybe<Scalars["numeric"]>
  ema_conf?: Maybe<Scalars["numeric"]>
  ema_price?: Maybe<Scalars["numeric"]>
  expo?: Maybe<Scalars["Int"]>
  price?: Maybe<Scalars["numeric"]>
}

/** aggregate var_pop on columns */
export type Latest_Pyth_Prices_Var_Pop_Fields = {
  __typename?: "latest_pyth_prices_var_pop_fields"
  conf?: Maybe<Scalars["Float"]>
  ema_conf?: Maybe<Scalars["Float"]>
  ema_price?: Maybe<Scalars["Float"]>
  expo?: Maybe<Scalars["Float"]>
  price?: Maybe<Scalars["Float"]>
}

/** aggregate var_samp on columns */
export type Latest_Pyth_Prices_Var_Samp_Fields = {
  __typename?: "latest_pyth_prices_var_samp_fields"
  conf?: Maybe<Scalars["Float"]>
  ema_conf?: Maybe<Scalars["Float"]>
  ema_price?: Maybe<Scalars["Float"]>
  expo?: Maybe<Scalars["Float"]>
  price?: Maybe<Scalars["Float"]>
}

/** aggregate variance on columns */
export type Latest_Pyth_Prices_Variance_Fields = {
  __typename?: "latest_pyth_prices_variance_fields"
  conf?: Maybe<Scalars["Float"]>
  ema_conf?: Maybe<Scalars["Float"]>
  ema_price?: Maybe<Scalars["Float"]>
  expo?: Maybe<Scalars["Float"]>
  price?: Maybe<Scalars["Float"]>
}

/** columns and relationships of "latest_trade_stats" */
export type Latest_Trade_Stats = {
  __typename?: "latest_trade_stats"
  apr_24hr?: Maybe<Scalars["numeric"]>
  block_number?: Maybe<Scalars["Int"]>
  blocks_24hr?: Maybe<Scalars["Int"]>
  es_unw_apr_24hr?: Maybe<Scalars["numeric"]>
  es_unw_emission_apr_24hr?: Maybe<Scalars["numeric"]>
  fee_24hr?: Maybe<Scalars["numeric"]>
  lp_value?: Maybe<Scalars["numeric"]>
  timestamp?: Maybe<Scalars["Int"]>
  traders_24hr?: Maybe<Scalars["numeric"]>
  trades_24hr?: Maybe<Scalars["numeric"]>
  ulp_emission_apr_24hr?: Maybe<Scalars["numeric"]>
  unw_apr_24hr?: Maybe<Scalars["numeric"]>
  unw_emission_apr_24hr?: Maybe<Scalars["numeric"]>
  volume_24hr?: Maybe<Scalars["numeric"]>
}

/** columns and relationships of "latest_trade_stats_7d" */
export type Latest_Trade_Stats_7d = {
  __typename?: "latest_trade_stats_7d"
  accumulated_fee?: Maybe<Scalars["numeric"]>
  apr_7d?: Maybe<Scalars["numeric"]>
  block_number?: Maybe<Scalars["Int"]>
  blocks_7d?: Maybe<Scalars["Int"]>
  es_unw_apr_7d?: Maybe<Scalars["numeric"]>
  es_unw_emission_apr_7d?: Maybe<Scalars["numeric"]>
  fee_7d?: Maybe<Scalars["numeric"]>
  lp_value?: Maybe<Scalars["numeric"]>
  timestamp?: Maybe<Scalars["Int"]>
  traders_7d?: Maybe<Scalars["numeric"]>
  trades_7d?: Maybe<Scalars["numeric"]>
  ulp_emission_apr_7d?: Maybe<Scalars["numeric"]>
  unw_apr_7d?: Maybe<Scalars["numeric"]>
  unw_emission_apr_7d?: Maybe<Scalars["numeric"]>
  volume_7d?: Maybe<Scalars["numeric"]>
}

/** Boolean expression to filter rows from the table "latest_trade_stats_7d". All fields are combined with a logical 'AND'. */
export type Latest_Trade_Stats_7d_Bool_Exp = {
  _and?: InputMaybe<Array<Latest_Trade_Stats_7d_Bool_Exp>>
  _not?: InputMaybe<Latest_Trade_Stats_7d_Bool_Exp>
  _or?: InputMaybe<Array<Latest_Trade_Stats_7d_Bool_Exp>>
  accumulated_fee?: InputMaybe<Numeric_Comparison_Exp>
  apr_7d?: InputMaybe<Numeric_Comparison_Exp>
  block_number?: InputMaybe<Int_Comparison_Exp>
  blocks_7d?: InputMaybe<Int_Comparison_Exp>
  es_unw_apr_7d?: InputMaybe<Numeric_Comparison_Exp>
  es_unw_emission_apr_7d?: InputMaybe<Numeric_Comparison_Exp>
  fee_7d?: InputMaybe<Numeric_Comparison_Exp>
  lp_value?: InputMaybe<Numeric_Comparison_Exp>
  timestamp?: InputMaybe<Int_Comparison_Exp>
  traders_7d?: InputMaybe<Numeric_Comparison_Exp>
  trades_7d?: InputMaybe<Numeric_Comparison_Exp>
  ulp_emission_apr_7d?: InputMaybe<Numeric_Comparison_Exp>
  unw_apr_7d?: InputMaybe<Numeric_Comparison_Exp>
  unw_emission_apr_7d?: InputMaybe<Numeric_Comparison_Exp>
  volume_7d?: InputMaybe<Numeric_Comparison_Exp>
}

/** Ordering options when selecting data from "latest_trade_stats_7d". */
export type Latest_Trade_Stats_7d_Order_By = {
  accumulated_fee?: InputMaybe<Order_By>
  apr_7d?: InputMaybe<Order_By>
  block_number?: InputMaybe<Order_By>
  blocks_7d?: InputMaybe<Order_By>
  es_unw_apr_7d?: InputMaybe<Order_By>
  es_unw_emission_apr_7d?: InputMaybe<Order_By>
  fee_7d?: InputMaybe<Order_By>
  lp_value?: InputMaybe<Order_By>
  timestamp?: InputMaybe<Order_By>
  traders_7d?: InputMaybe<Order_By>
  trades_7d?: InputMaybe<Order_By>
  ulp_emission_apr_7d?: InputMaybe<Order_By>
  unw_apr_7d?: InputMaybe<Order_By>
  unw_emission_apr_7d?: InputMaybe<Order_By>
  volume_7d?: InputMaybe<Order_By>
}

/** select columns of table "latest_trade_stats_7d" */
export enum Latest_Trade_Stats_7d_Select_Column {
  /** column name */
  AccumulatedFee = "accumulated_fee",
  /** column name */
  Apr_7d = "apr_7d",
  /** column name */
  BlockNumber = "block_number",
  /** column name */
  Blocks_7d = "blocks_7d",
  /** column name */
  EsUnwApr_7d = "es_unw_apr_7d",
  /** column name */
  EsUnwEmissionApr_7d = "es_unw_emission_apr_7d",
  /** column name */
  Fee_7d = "fee_7d",
  /** column name */
  LpValue = "lp_value",
  /** column name */
  Timestamp = "timestamp",
  /** column name */
  Traders_7d = "traders_7d",
  /** column name */
  Trades_7d = "trades_7d",
  /** column name */
  UlpEmissionApr_7d = "ulp_emission_apr_7d",
  /** column name */
  UnwApr_7d = "unw_apr_7d",
  /** column name */
  UnwEmissionApr_7d = "unw_emission_apr_7d",
  /** column name */
  Volume_7d = "volume_7d",
}

/** Streaming cursor of the table "latest_trade_stats_7d" */
export type Latest_Trade_Stats_7d_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Latest_Trade_Stats_7d_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Latest_Trade_Stats_7d_Stream_Cursor_Value_Input = {
  accumulated_fee?: InputMaybe<Scalars["numeric"]>
  apr_7d?: InputMaybe<Scalars["numeric"]>
  block_number?: InputMaybe<Scalars["Int"]>
  blocks_7d?: InputMaybe<Scalars["Int"]>
  es_unw_apr_7d?: InputMaybe<Scalars["numeric"]>
  es_unw_emission_apr_7d?: InputMaybe<Scalars["numeric"]>
  fee_7d?: InputMaybe<Scalars["numeric"]>
  lp_value?: InputMaybe<Scalars["numeric"]>
  timestamp?: InputMaybe<Scalars["Int"]>
  traders_7d?: InputMaybe<Scalars["numeric"]>
  trades_7d?: InputMaybe<Scalars["numeric"]>
  ulp_emission_apr_7d?: InputMaybe<Scalars["numeric"]>
  unw_apr_7d?: InputMaybe<Scalars["numeric"]>
  unw_emission_apr_7d?: InputMaybe<Scalars["numeric"]>
  volume_7d?: InputMaybe<Scalars["numeric"]>
}

/** Boolean expression to filter rows from the table "latest_trade_stats". All fields are combined with a logical 'AND'. */
export type Latest_Trade_Stats_Bool_Exp = {
  _and?: InputMaybe<Array<Latest_Trade_Stats_Bool_Exp>>
  _not?: InputMaybe<Latest_Trade_Stats_Bool_Exp>
  _or?: InputMaybe<Array<Latest_Trade_Stats_Bool_Exp>>
  apr_24hr?: InputMaybe<Numeric_Comparison_Exp>
  block_number?: InputMaybe<Int_Comparison_Exp>
  blocks_24hr?: InputMaybe<Int_Comparison_Exp>
  es_unw_apr_24hr?: InputMaybe<Numeric_Comparison_Exp>
  es_unw_emission_apr_24hr?: InputMaybe<Numeric_Comparison_Exp>
  fee_24hr?: InputMaybe<Numeric_Comparison_Exp>
  lp_value?: InputMaybe<Numeric_Comparison_Exp>
  timestamp?: InputMaybe<Int_Comparison_Exp>
  traders_24hr?: InputMaybe<Numeric_Comparison_Exp>
  trades_24hr?: InputMaybe<Numeric_Comparison_Exp>
  ulp_emission_apr_24hr?: InputMaybe<Numeric_Comparison_Exp>
  unw_apr_24hr?: InputMaybe<Numeric_Comparison_Exp>
  unw_emission_apr_24hr?: InputMaybe<Numeric_Comparison_Exp>
  volume_24hr?: InputMaybe<Numeric_Comparison_Exp>
}

/** Ordering options when selecting data from "latest_trade_stats". */
export type Latest_Trade_Stats_Order_By = {
  apr_24hr?: InputMaybe<Order_By>
  block_number?: InputMaybe<Order_By>
  blocks_24hr?: InputMaybe<Order_By>
  es_unw_apr_24hr?: InputMaybe<Order_By>
  es_unw_emission_apr_24hr?: InputMaybe<Order_By>
  fee_24hr?: InputMaybe<Order_By>
  lp_value?: InputMaybe<Order_By>
  timestamp?: InputMaybe<Order_By>
  traders_24hr?: InputMaybe<Order_By>
  trades_24hr?: InputMaybe<Order_By>
  ulp_emission_apr_24hr?: InputMaybe<Order_By>
  unw_apr_24hr?: InputMaybe<Order_By>
  unw_emission_apr_24hr?: InputMaybe<Order_By>
  volume_24hr?: InputMaybe<Order_By>
}

/** select columns of table "latest_trade_stats" */
export enum Latest_Trade_Stats_Select_Column {
  /** column name */
  Apr_24hr = "apr_24hr",
  /** column name */
  BlockNumber = "block_number",
  /** column name */
  Blocks_24hr = "blocks_24hr",
  /** column name */
  EsUnwApr_24hr = "es_unw_apr_24hr",
  /** column name */
  EsUnwEmissionApr_24hr = "es_unw_emission_apr_24hr",
  /** column name */
  Fee_24hr = "fee_24hr",
  /** column name */
  LpValue = "lp_value",
  /** column name */
  Timestamp = "timestamp",
  /** column name */
  Traders_24hr = "traders_24hr",
  /** column name */
  Trades_24hr = "trades_24hr",
  /** column name */
  UlpEmissionApr_24hr = "ulp_emission_apr_24hr",
  /** column name */
  UnwApr_24hr = "unw_apr_24hr",
  /** column name */
  UnwEmissionApr_24hr = "unw_emission_apr_24hr",
  /** column name */
  Volume_24hr = "volume_24hr",
}

/** Streaming cursor of the table "latest_trade_stats" */
export type Latest_Trade_Stats_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Latest_Trade_Stats_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Latest_Trade_Stats_Stream_Cursor_Value_Input = {
  apr_24hr?: InputMaybe<Scalars["numeric"]>
  block_number?: InputMaybe<Scalars["Int"]>
  blocks_24hr?: InputMaybe<Scalars["Int"]>
  es_unw_apr_24hr?: InputMaybe<Scalars["numeric"]>
  es_unw_emission_apr_24hr?: InputMaybe<Scalars["numeric"]>
  fee_24hr?: InputMaybe<Scalars["numeric"]>
  lp_value?: InputMaybe<Scalars["numeric"]>
  timestamp?: InputMaybe<Scalars["Int"]>
  traders_24hr?: InputMaybe<Scalars["numeric"]>
  trades_24hr?: InputMaybe<Scalars["numeric"]>
  ulp_emission_apr_24hr?: InputMaybe<Scalars["numeric"]>
  unw_apr_24hr?: InputMaybe<Scalars["numeric"]>
  unw_emission_apr_24hr?: InputMaybe<Scalars["numeric"]>
  volume_24hr?: InputMaybe<Scalars["numeric"]>
}

/** columns and relationships of "liquidity_bootstrap_stage_1" */
export type Liquidity_Bootstrap_Stage_1 = {
  __typename?: "liquidity_bootstrap_stage_1"
  address?: Maybe<Scalars["bytea"]>
  balance?: Maybe<Scalars["numeric"]>
  onchain_balance?: Maybe<Scalars["numeric"]>
  scores?: Maybe<Scalars["numeric"]>
  staked_amount?: Maybe<Scalars["numeric"]>
  total_balance?: Maybe<Scalars["numeric"]>
  unqualify_block_number?: Maybe<Scalars["bigint"]>
  unstaked_amount?: Maybe<Scalars["numeric"]>
}

/** Boolean expression to filter rows from the table "liquidity_bootstrap_stage_1". All fields are combined with a logical 'AND'. */
export type Liquidity_Bootstrap_Stage_1_Bool_Exp = {
  _and?: InputMaybe<Array<Liquidity_Bootstrap_Stage_1_Bool_Exp>>
  _not?: InputMaybe<Liquidity_Bootstrap_Stage_1_Bool_Exp>
  _or?: InputMaybe<Array<Liquidity_Bootstrap_Stage_1_Bool_Exp>>
  address?: InputMaybe<Bytea_Comparison_Exp>
  balance?: InputMaybe<Numeric_Comparison_Exp>
  onchain_balance?: InputMaybe<Numeric_Comparison_Exp>
  scores?: InputMaybe<Numeric_Comparison_Exp>
  staked_amount?: InputMaybe<Numeric_Comparison_Exp>
  total_balance?: InputMaybe<Numeric_Comparison_Exp>
  unqualify_block_number?: InputMaybe<Bigint_Comparison_Exp>
  unstaked_amount?: InputMaybe<Numeric_Comparison_Exp>
}

/** Ordering options when selecting data from "liquidity_bootstrap_stage_1". */
export type Liquidity_Bootstrap_Stage_1_Order_By = {
  address?: InputMaybe<Order_By>
  balance?: InputMaybe<Order_By>
  onchain_balance?: InputMaybe<Order_By>
  scores?: InputMaybe<Order_By>
  staked_amount?: InputMaybe<Order_By>
  total_balance?: InputMaybe<Order_By>
  unqualify_block_number?: InputMaybe<Order_By>
  unstaked_amount?: InputMaybe<Order_By>
}

/** select columns of table "liquidity_bootstrap_stage_1" */
export enum Liquidity_Bootstrap_Stage_1_Select_Column {
  /** column name */
  Address = "address",
  /** column name */
  Balance = "balance",
  /** column name */
  OnchainBalance = "onchain_balance",
  /** column name */
  Scores = "scores",
  /** column name */
  StakedAmount = "staked_amount",
  /** column name */
  TotalBalance = "total_balance",
  /** column name */
  UnqualifyBlockNumber = "unqualify_block_number",
  /** column name */
  UnstakedAmount = "unstaked_amount",
}

/** Streaming cursor of the table "liquidity_bootstrap_stage_1" */
export type Liquidity_Bootstrap_Stage_1_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Liquidity_Bootstrap_Stage_1_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Liquidity_Bootstrap_Stage_1_Stream_Cursor_Value_Input = {
  address?: InputMaybe<Scalars["bytea"]>
  balance?: InputMaybe<Scalars["numeric"]>
  onchain_balance?: InputMaybe<Scalars["numeric"]>
  scores?: InputMaybe<Scalars["numeric"]>
  staked_amount?: InputMaybe<Scalars["numeric"]>
  total_balance?: InputMaybe<Scalars["numeric"]>
  unqualify_block_number?: InputMaybe<Scalars["bigint"]>
  unstaked_amount?: InputMaybe<Scalars["numeric"]>
}

/** columns and relationships of "liquidity_bootstrap_stage_2" */
export type Liquidity_Bootstrap_Stage_2 = {
  __typename?: "liquidity_bootstrap_stage_2"
  address?: Maybe<Scalars["bytea"]>
  balance?: Maybe<Scalars["numeric"]>
  onchain_balance?: Maybe<Scalars["numeric"]>
  scores?: Maybe<Scalars["numeric"]>
  staked_amount?: Maybe<Scalars["numeric"]>
  total_balance?: Maybe<Scalars["numeric"]>
  unqualify_block_number?: Maybe<Scalars["bigint"]>
  unstaked_amount?: Maybe<Scalars["numeric"]>
}

/** Boolean expression to filter rows from the table "liquidity_bootstrap_stage_2". All fields are combined with a logical 'AND'. */
export type Liquidity_Bootstrap_Stage_2_Bool_Exp = {
  _and?: InputMaybe<Array<Liquidity_Bootstrap_Stage_2_Bool_Exp>>
  _not?: InputMaybe<Liquidity_Bootstrap_Stage_2_Bool_Exp>
  _or?: InputMaybe<Array<Liquidity_Bootstrap_Stage_2_Bool_Exp>>
  address?: InputMaybe<Bytea_Comparison_Exp>
  balance?: InputMaybe<Numeric_Comparison_Exp>
  onchain_balance?: InputMaybe<Numeric_Comparison_Exp>
  scores?: InputMaybe<Numeric_Comparison_Exp>
  staked_amount?: InputMaybe<Numeric_Comparison_Exp>
  total_balance?: InputMaybe<Numeric_Comparison_Exp>
  unqualify_block_number?: InputMaybe<Bigint_Comparison_Exp>
  unstaked_amount?: InputMaybe<Numeric_Comparison_Exp>
}

/** Ordering options when selecting data from "liquidity_bootstrap_stage_2". */
export type Liquidity_Bootstrap_Stage_2_Order_By = {
  address?: InputMaybe<Order_By>
  balance?: InputMaybe<Order_By>
  onchain_balance?: InputMaybe<Order_By>
  scores?: InputMaybe<Order_By>
  staked_amount?: InputMaybe<Order_By>
  total_balance?: InputMaybe<Order_By>
  unqualify_block_number?: InputMaybe<Order_By>
  unstaked_amount?: InputMaybe<Order_By>
}

/** select columns of table "liquidity_bootstrap_stage_2" */
export enum Liquidity_Bootstrap_Stage_2_Select_Column {
  /** column name */
  Address = "address",
  /** column name */
  Balance = "balance",
  /** column name */
  OnchainBalance = "onchain_balance",
  /** column name */
  Scores = "scores",
  /** column name */
  StakedAmount = "staked_amount",
  /** column name */
  TotalBalance = "total_balance",
  /** column name */
  UnqualifyBlockNumber = "unqualify_block_number",
  /** column name */
  UnstakedAmount = "unstaked_amount",
}

/** Streaming cursor of the table "liquidity_bootstrap_stage_2" */
export type Liquidity_Bootstrap_Stage_2_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Liquidity_Bootstrap_Stage_2_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Liquidity_Bootstrap_Stage_2_Stream_Cursor_Value_Input = {
  address?: InputMaybe<Scalars["bytea"]>
  balance?: InputMaybe<Scalars["numeric"]>
  onchain_balance?: InputMaybe<Scalars["numeric"]>
  scores?: InputMaybe<Scalars["numeric"]>
  staked_amount?: InputMaybe<Scalars["numeric"]>
  total_balance?: InputMaybe<Scalars["numeric"]>
  unqualify_block_number?: InputMaybe<Scalars["bigint"]>
  unstaked_amount?: InputMaybe<Scalars["numeric"]>
}

/** columns and relationships of "liquidity_bootstrap_stage_3" */
export type Liquidity_Bootstrap_Stage_3 = {
  __typename?: "liquidity_bootstrap_stage_3"
  address?: Maybe<Scalars["bytea"]>
  balance?: Maybe<Scalars["numeric"]>
  onchain_balance?: Maybe<Scalars["numeric"]>
  scores?: Maybe<Scalars["numeric"]>
  staked_amount?: Maybe<Scalars["numeric"]>
  total_balance?: Maybe<Scalars["numeric"]>
  unqualify_block_number?: Maybe<Scalars["bigint"]>
  unstaked_amount?: Maybe<Scalars["numeric"]>
}

/** Boolean expression to filter rows from the table "liquidity_bootstrap_stage_3". All fields are combined with a logical 'AND'. */
export type Liquidity_Bootstrap_Stage_3_Bool_Exp = {
  _and?: InputMaybe<Array<Liquidity_Bootstrap_Stage_3_Bool_Exp>>
  _not?: InputMaybe<Liquidity_Bootstrap_Stage_3_Bool_Exp>
  _or?: InputMaybe<Array<Liquidity_Bootstrap_Stage_3_Bool_Exp>>
  address?: InputMaybe<Bytea_Comparison_Exp>
  balance?: InputMaybe<Numeric_Comparison_Exp>
  onchain_balance?: InputMaybe<Numeric_Comparison_Exp>
  scores?: InputMaybe<Numeric_Comparison_Exp>
  staked_amount?: InputMaybe<Numeric_Comparison_Exp>
  total_balance?: InputMaybe<Numeric_Comparison_Exp>
  unqualify_block_number?: InputMaybe<Bigint_Comparison_Exp>
  unstaked_amount?: InputMaybe<Numeric_Comparison_Exp>
}

/** Ordering options when selecting data from "liquidity_bootstrap_stage_3". */
export type Liquidity_Bootstrap_Stage_3_Order_By = {
  address?: InputMaybe<Order_By>
  balance?: InputMaybe<Order_By>
  onchain_balance?: InputMaybe<Order_By>
  scores?: InputMaybe<Order_By>
  staked_amount?: InputMaybe<Order_By>
  total_balance?: InputMaybe<Order_By>
  unqualify_block_number?: InputMaybe<Order_By>
  unstaked_amount?: InputMaybe<Order_By>
}

/** select columns of table "liquidity_bootstrap_stage_3" */
export enum Liquidity_Bootstrap_Stage_3_Select_Column {
  /** column name */
  Address = "address",
  /** column name */
  Balance = "balance",
  /** column name */
  OnchainBalance = "onchain_balance",
  /** column name */
  Scores = "scores",
  /** column name */
  StakedAmount = "staked_amount",
  /** column name */
  TotalBalance = "total_balance",
  /** column name */
  UnqualifyBlockNumber = "unqualify_block_number",
  /** column name */
  UnstakedAmount = "unstaked_amount",
}

/** Streaming cursor of the table "liquidity_bootstrap_stage_3" */
export type Liquidity_Bootstrap_Stage_3_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Liquidity_Bootstrap_Stage_3_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Liquidity_Bootstrap_Stage_3_Stream_Cursor_Value_Input = {
  address?: InputMaybe<Scalars["bytea"]>
  balance?: InputMaybe<Scalars["numeric"]>
  onchain_balance?: InputMaybe<Scalars["numeric"]>
  scores?: InputMaybe<Scalars["numeric"]>
  staked_amount?: InputMaybe<Scalars["numeric"]>
  total_balance?: InputMaybe<Scalars["numeric"]>
  unqualify_block_number?: InputMaybe<Scalars["bigint"]>
  unstaked_amount?: InputMaybe<Scalars["numeric"]>
}

/** Boolean expression to compare columns of type "numeric". All fields are combined with logical 'AND'. */
export type Numeric_Comparison_Exp = {
  _eq?: InputMaybe<Scalars["numeric"]>
  _gt?: InputMaybe<Scalars["numeric"]>
  _gte?: InputMaybe<Scalars["numeric"]>
  _in?: InputMaybe<Array<Scalars["numeric"]>>
  _is_null?: InputMaybe<Scalars["Boolean"]>
  _lt?: InputMaybe<Scalars["numeric"]>
  _lte?: InputMaybe<Scalars["numeric"]>
  _neq?: InputMaybe<Scalars["numeric"]>
  _nin?: InputMaybe<Array<Scalars["numeric"]>>
}

/** columns and relationships of "open_limit_orders" */
export type Open_Limit_Orders = {
  __typename?: "open_limit_orders"
  is_buy: Scalars["Boolean"]
  last_update_block_number: Scalars["bigint"]
  last_update_block_time: Scalars["timestamptz"]
  last_update_log_index: Scalars["bigint"]
  leverage: Scalars["numeric"]
  margin: Scalars["numeric"]
  open_block: Scalars["bigint"]
  open_price: Scalars["numeric"]
  order_hash: Scalars["bytea"]
  price_id: Scalars["bytea"]
  profit_target: Scalars["numeric"]
  stop_loss: Scalars["numeric"]
  user: Scalars["bytea"]
}

/** Boolean expression to filter rows from the table "open_limit_orders". All fields are combined with a logical 'AND'. */
export type Open_Limit_Orders_Bool_Exp = {
  _and?: InputMaybe<Array<Open_Limit_Orders_Bool_Exp>>
  _not?: InputMaybe<Open_Limit_Orders_Bool_Exp>
  _or?: InputMaybe<Array<Open_Limit_Orders_Bool_Exp>>
  is_buy?: InputMaybe<Boolean_Comparison_Exp>
  last_update_block_number?: InputMaybe<Bigint_Comparison_Exp>
  last_update_block_time?: InputMaybe<Timestamptz_Comparison_Exp>
  last_update_log_index?: InputMaybe<Bigint_Comparison_Exp>
  leverage?: InputMaybe<Numeric_Comparison_Exp>
  margin?: InputMaybe<Numeric_Comparison_Exp>
  open_block?: InputMaybe<Bigint_Comparison_Exp>
  open_price?: InputMaybe<Numeric_Comparison_Exp>
  order_hash?: InputMaybe<Bytea_Comparison_Exp>
  price_id?: InputMaybe<Bytea_Comparison_Exp>
  profit_target?: InputMaybe<Numeric_Comparison_Exp>
  stop_loss?: InputMaybe<Numeric_Comparison_Exp>
  user?: InputMaybe<Bytea_Comparison_Exp>
}

/** Ordering options when selecting data from "open_limit_orders". */
export type Open_Limit_Orders_Order_By = {
  is_buy?: InputMaybe<Order_By>
  last_update_block_number?: InputMaybe<Order_By>
  last_update_block_time?: InputMaybe<Order_By>
  last_update_log_index?: InputMaybe<Order_By>
  leverage?: InputMaybe<Order_By>
  margin?: InputMaybe<Order_By>
  open_block?: InputMaybe<Order_By>
  open_price?: InputMaybe<Order_By>
  order_hash?: InputMaybe<Order_By>
  price_id?: InputMaybe<Order_By>
  profit_target?: InputMaybe<Order_By>
  stop_loss?: InputMaybe<Order_By>
  user?: InputMaybe<Order_By>
}

/** select columns of table "open_limit_orders" */
export enum Open_Limit_Orders_Select_Column {
  /** column name */
  IsBuy = "is_buy",
  /** column name */
  LastUpdateBlockNumber = "last_update_block_number",
  /** column name */
  LastUpdateBlockTime = "last_update_block_time",
  /** column name */
  LastUpdateLogIndex = "last_update_log_index",
  /** column name */
  Leverage = "leverage",
  /** column name */
  Margin = "margin",
  /** column name */
  OpenBlock = "open_block",
  /** column name */
  OpenPrice = "open_price",
  /** column name */
  OrderHash = "order_hash",
  /** column name */
  PriceId = "price_id",
  /** column name */
  ProfitTarget = "profit_target",
  /** column name */
  StopLoss = "stop_loss",
  /** column name */
  User = "user",
}

/** Streaming cursor of the table "open_limit_orders" */
export type Open_Limit_Orders_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Open_Limit_Orders_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Open_Limit_Orders_Stream_Cursor_Value_Input = {
  is_buy?: InputMaybe<Scalars["Boolean"]>
  last_update_block_number?: InputMaybe<Scalars["bigint"]>
  last_update_block_time?: InputMaybe<Scalars["timestamptz"]>
  last_update_log_index?: InputMaybe<Scalars["bigint"]>
  leverage?: InputMaybe<Scalars["numeric"]>
  margin?: InputMaybe<Scalars["numeric"]>
  open_block?: InputMaybe<Scalars["bigint"]>
  open_price?: InputMaybe<Scalars["numeric"]>
  order_hash?: InputMaybe<Scalars["bytea"]>
  price_id?: InputMaybe<Scalars["bytea"]>
  profit_target?: InputMaybe<Scalars["numeric"]>
  stop_loss?: InputMaybe<Scalars["numeric"]>
  user?: InputMaybe<Scalars["bytea"]>
}

/** columns and relationships of "open_market_orders" */
export type Open_Market_Orders = {
  __typename?: "open_market_orders"
  accrued_fee_last_update: Scalars["bigint"]
  accrued_funding_fee: Scalars["numeric"]
  accrued_rollover_fee: Scalars["numeric"]
  fee: Scalars["numeric"]
  fee_balance: Scalars["numeric"]
  fee_base: Scalars["numeric"]
  fee_last_update: Scalars["bigint"]
  is_buy: Scalars["Boolean"]
  last_update_block_number: Scalars["bigint"]
  last_update_block_time: Scalars["timestamptz"]
  last_update_log_index: Scalars["bigint"]
  leverage: Scalars["numeric"]
  liquidation_price: Scalars["numeric"]
  margin: Scalars["numeric"]
  max_percentage_pnL: Scalars["numeric"]
  open_block: Scalars["bigint"]
  open_price: Scalars["numeric"]
  open_time: Scalars["bigint"]
  order_hash: Scalars["bytea"]
  price_id: Scalars["bytea"]
  profit_target: Scalars["numeric"]
  referral_code: Scalars["String"]
  referral_fee: Scalars["numeric"]
  referrer: Scalars["bytea"]
  salt: Scalars["numeric"]
  slippage: Scalars["numeric"]
  stop_loss: Scalars["numeric"]
  user: Scalars["bytea"]
}

/** Boolean expression to filter rows from the table "open_market_orders". All fields are combined with a logical 'AND'. */
export type Open_Market_Orders_Bool_Exp = {
  _and?: InputMaybe<Array<Open_Market_Orders_Bool_Exp>>
  _not?: InputMaybe<Open_Market_Orders_Bool_Exp>
  _or?: InputMaybe<Array<Open_Market_Orders_Bool_Exp>>
  accrued_fee_last_update?: InputMaybe<Bigint_Comparison_Exp>
  accrued_funding_fee?: InputMaybe<Numeric_Comparison_Exp>
  accrued_rollover_fee?: InputMaybe<Numeric_Comparison_Exp>
  fee?: InputMaybe<Numeric_Comparison_Exp>
  fee_balance?: InputMaybe<Numeric_Comparison_Exp>
  fee_base?: InputMaybe<Numeric_Comparison_Exp>
  fee_last_update?: InputMaybe<Bigint_Comparison_Exp>
  is_buy?: InputMaybe<Boolean_Comparison_Exp>
  last_update_block_number?: InputMaybe<Bigint_Comparison_Exp>
  last_update_block_time?: InputMaybe<Timestamptz_Comparison_Exp>
  last_update_log_index?: InputMaybe<Bigint_Comparison_Exp>
  leverage?: InputMaybe<Numeric_Comparison_Exp>
  liquidation_price?: InputMaybe<Numeric_Comparison_Exp>
  margin?: InputMaybe<Numeric_Comparison_Exp>
  max_percentage_pnL?: InputMaybe<Numeric_Comparison_Exp>
  open_block?: InputMaybe<Bigint_Comparison_Exp>
  open_price?: InputMaybe<Numeric_Comparison_Exp>
  open_time?: InputMaybe<Bigint_Comparison_Exp>
  order_hash?: InputMaybe<Bytea_Comparison_Exp>
  price_id?: InputMaybe<Bytea_Comparison_Exp>
  profit_target?: InputMaybe<Numeric_Comparison_Exp>
  referral_code?: InputMaybe<String_Comparison_Exp>
  referral_fee?: InputMaybe<Numeric_Comparison_Exp>
  referrer?: InputMaybe<Bytea_Comparison_Exp>
  salt?: InputMaybe<Numeric_Comparison_Exp>
  slippage?: InputMaybe<Numeric_Comparison_Exp>
  stop_loss?: InputMaybe<Numeric_Comparison_Exp>
  user?: InputMaybe<Bytea_Comparison_Exp>
}

/** Ordering options when selecting data from "open_market_orders". */
export type Open_Market_Orders_Order_By = {
  accrued_fee_last_update?: InputMaybe<Order_By>
  accrued_funding_fee?: InputMaybe<Order_By>
  accrued_rollover_fee?: InputMaybe<Order_By>
  fee?: InputMaybe<Order_By>
  fee_balance?: InputMaybe<Order_By>
  fee_base?: InputMaybe<Order_By>
  fee_last_update?: InputMaybe<Order_By>
  is_buy?: InputMaybe<Order_By>
  last_update_block_number?: InputMaybe<Order_By>
  last_update_block_time?: InputMaybe<Order_By>
  last_update_log_index?: InputMaybe<Order_By>
  leverage?: InputMaybe<Order_By>
  liquidation_price?: InputMaybe<Order_By>
  margin?: InputMaybe<Order_By>
  max_percentage_pnL?: InputMaybe<Order_By>
  open_block?: InputMaybe<Order_By>
  open_price?: InputMaybe<Order_By>
  open_time?: InputMaybe<Order_By>
  order_hash?: InputMaybe<Order_By>
  price_id?: InputMaybe<Order_By>
  profit_target?: InputMaybe<Order_By>
  referral_code?: InputMaybe<Order_By>
  referral_fee?: InputMaybe<Order_By>
  referrer?: InputMaybe<Order_By>
  salt?: InputMaybe<Order_By>
  slippage?: InputMaybe<Order_By>
  stop_loss?: InputMaybe<Order_By>
  user?: InputMaybe<Order_By>
}

/** select columns of table "open_market_orders" */
export enum Open_Market_Orders_Select_Column {
  /** column name */
  AccruedFeeLastUpdate = "accrued_fee_last_update",
  /** column name */
  AccruedFundingFee = "accrued_funding_fee",
  /** column name */
  AccruedRolloverFee = "accrued_rollover_fee",
  /** column name */
  Fee = "fee",
  /** column name */
  FeeBalance = "fee_balance",
  /** column name */
  FeeBase = "fee_base",
  /** column name */
  FeeLastUpdate = "fee_last_update",
  /** column name */
  IsBuy = "is_buy",
  /** column name */
  LastUpdateBlockNumber = "last_update_block_number",
  /** column name */
  LastUpdateBlockTime = "last_update_block_time",
  /** column name */
  LastUpdateLogIndex = "last_update_log_index",
  /** column name */
  Leverage = "leverage",
  /** column name */
  LiquidationPrice = "liquidation_price",
  /** column name */
  Margin = "margin",
  /** column name */
  MaxPercentagePnL = "max_percentage_pnL",
  /** column name */
  OpenBlock = "open_block",
  /** column name */
  OpenPrice = "open_price",
  /** column name */
  OpenTime = "open_time",
  /** column name */
  OrderHash = "order_hash",
  /** column name */
  PriceId = "price_id",
  /** column name */
  ProfitTarget = "profit_target",
  /** column name */
  ReferralCode = "referral_code",
  /** column name */
  ReferralFee = "referral_fee",
  /** column name */
  Referrer = "referrer",
  /** column name */
  Salt = "salt",
  /** column name */
  Slippage = "slippage",
  /** column name */
  StopLoss = "stop_loss",
  /** column name */
  User = "user",
}

/** Streaming cursor of the table "open_market_orders" */
export type Open_Market_Orders_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Open_Market_Orders_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Open_Market_Orders_Stream_Cursor_Value_Input = {
  accrued_fee_last_update?: InputMaybe<Scalars["bigint"]>
  accrued_funding_fee?: InputMaybe<Scalars["numeric"]>
  accrued_rollover_fee?: InputMaybe<Scalars["numeric"]>
  fee?: InputMaybe<Scalars["numeric"]>
  fee_balance?: InputMaybe<Scalars["numeric"]>
  fee_base?: InputMaybe<Scalars["numeric"]>
  fee_last_update?: InputMaybe<Scalars["bigint"]>
  is_buy?: InputMaybe<Scalars["Boolean"]>
  last_update_block_number?: InputMaybe<Scalars["bigint"]>
  last_update_block_time?: InputMaybe<Scalars["timestamptz"]>
  last_update_log_index?: InputMaybe<Scalars["bigint"]>
  leverage?: InputMaybe<Scalars["numeric"]>
  liquidation_price?: InputMaybe<Scalars["numeric"]>
  margin?: InputMaybe<Scalars["numeric"]>
  max_percentage_pnL?: InputMaybe<Scalars["numeric"]>
  open_block?: InputMaybe<Scalars["bigint"]>
  open_price?: InputMaybe<Scalars["numeric"]>
  open_time?: InputMaybe<Scalars["bigint"]>
  order_hash?: InputMaybe<Scalars["bytea"]>
  price_id?: InputMaybe<Scalars["bytea"]>
  profit_target?: InputMaybe<Scalars["numeric"]>
  referral_code?: InputMaybe<Scalars["String"]>
  referral_fee?: InputMaybe<Scalars["numeric"]>
  referrer?: InputMaybe<Scalars["bytea"]>
  salt?: InputMaybe<Scalars["numeric"]>
  slippage?: InputMaybe<Scalars["numeric"]>
  stop_loss?: InputMaybe<Scalars["numeric"]>
  user?: InputMaybe<Scalars["bytea"]>
}

/** column ordering options */
export enum Order_By {
  /** in ascending order, nulls last */
  Asc = "asc",
  /** in ascending order, nulls first */
  AscNullsFirst = "asc_nulls_first",
  /** in ascending order, nulls last */
  AscNullsLast = "asc_nulls_last",
  /** in descending order, nulls first */
  Desc = "desc",
  /** in descending order, nulls first */
  DescNullsFirst = "desc_nulls_first",
  /** in descending order, nulls last */
  DescNullsLast = "desc_nulls_last",
}

/** columns and relationships of "per_block_trade_stats" */
export type Per_Block_Trade_Stats = {
  __typename?: "per_block_trade_stats"
  apr: Scalars["numeric"]
  block_number: Scalars["Int"]
  duration: Scalars["Int"]
  fee: Scalars["numeric"]
  lp_value: Scalars["numeric"]
  timestamp: Scalars["Int"]
  traders: Scalars["numeric"]
  trades: Scalars["numeric"]
  volume: Scalars["numeric"]
}

/** Boolean expression to filter rows from the table "per_block_trade_stats". All fields are combined with a logical 'AND'. */
export type Per_Block_Trade_Stats_Bool_Exp = {
  _and?: InputMaybe<Array<Per_Block_Trade_Stats_Bool_Exp>>
  _not?: InputMaybe<Per_Block_Trade_Stats_Bool_Exp>
  _or?: InputMaybe<Array<Per_Block_Trade_Stats_Bool_Exp>>
  apr?: InputMaybe<Numeric_Comparison_Exp>
  block_number?: InputMaybe<Int_Comparison_Exp>
  duration?: InputMaybe<Int_Comparison_Exp>
  fee?: InputMaybe<Numeric_Comparison_Exp>
  lp_value?: InputMaybe<Numeric_Comparison_Exp>
  timestamp?: InputMaybe<Int_Comparison_Exp>
  traders?: InputMaybe<Numeric_Comparison_Exp>
  trades?: InputMaybe<Numeric_Comparison_Exp>
  volume?: InputMaybe<Numeric_Comparison_Exp>
}

/** Ordering options when selecting data from "per_block_trade_stats". */
export type Per_Block_Trade_Stats_Order_By = {
  apr?: InputMaybe<Order_By>
  block_number?: InputMaybe<Order_By>
  duration?: InputMaybe<Order_By>
  fee?: InputMaybe<Order_By>
  lp_value?: InputMaybe<Order_By>
  timestamp?: InputMaybe<Order_By>
  traders?: InputMaybe<Order_By>
  trades?: InputMaybe<Order_By>
  volume?: InputMaybe<Order_By>
}

/** select columns of table "per_block_trade_stats" */
export enum Per_Block_Trade_Stats_Select_Column {
  /** column name */
  Apr = "apr",
  /** column name */
  BlockNumber = "block_number",
  /** column name */
  Duration = "duration",
  /** column name */
  Fee = "fee",
  /** column name */
  LpValue = "lp_value",
  /** column name */
  Timestamp = "timestamp",
  /** column name */
  Traders = "traders",
  /** column name */
  Trades = "trades",
  /** column name */
  Volume = "volume",
}

/** Streaming cursor of the table "per_block_trade_stats" */
export type Per_Block_Trade_Stats_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Per_Block_Trade_Stats_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Per_Block_Trade_Stats_Stream_Cursor_Value_Input = {
  apr?: InputMaybe<Scalars["numeric"]>
  block_number?: InputMaybe<Scalars["Int"]>
  duration?: InputMaybe<Scalars["Int"]>
  fee?: InputMaybe<Scalars["numeric"]>
  lp_value?: InputMaybe<Scalars["numeric"]>
  timestamp?: InputMaybe<Scalars["Int"]>
  traders?: InputMaybe<Scalars["numeric"]>
  trades?: InputMaybe<Scalars["numeric"]>
  volume?: InputMaybe<Scalars["numeric"]>
}

/** columns and relationships of "pyth_prices" */
export type Pyth_Prices = {
  __typename?: "pyth_prices"
  conf: Scalars["numeric"]
  ema_conf: Scalars["numeric"]
  ema_price: Scalars["numeric"]
  env: Scalars["String"]
  expo: Scalars["Int"]
  id: Scalars["bigint"]
  price: Scalars["numeric"]
  price_data?: Maybe<Scalars["bytea"]>
  price_id: Scalars["bytea"]
  publish_time: Scalars["timestamptz"]
  symbol: Scalars["String"]
}

/** columns and relationships of "pyth_prices_1d" */
export type Pyth_Prices_1d = {
  __typename?: "pyth_prices_1d"
  aggregated_time: Scalars["timestamptz"]
  close_price: Scalars["numeric"]
  env: Scalars["String"]
  high_ask_conf: Scalars["numeric"]
  high_ask_price: Scalars["numeric"]
  high_price: Scalars["numeric"]
  low_bid_conf: Scalars["numeric"]
  low_bid_price: Scalars["numeric"]
  low_price: Scalars["numeric"]
  open_price: Scalars["numeric"]
  price_id: Scalars["bytea"]
  symbol: Scalars["String"]
}

/** Boolean expression to filter rows from the table "pyth_prices_1d". All fields are combined with a logical 'AND'. */
export type Pyth_Prices_1d_Bool_Exp = {
  _and?: InputMaybe<Array<Pyth_Prices_1d_Bool_Exp>>
  _not?: InputMaybe<Pyth_Prices_1d_Bool_Exp>
  _or?: InputMaybe<Array<Pyth_Prices_1d_Bool_Exp>>
  aggregated_time?: InputMaybe<Timestamptz_Comparison_Exp>
  close_price?: InputMaybe<Numeric_Comparison_Exp>
  env?: InputMaybe<String_Comparison_Exp>
  high_ask_conf?: InputMaybe<Numeric_Comparison_Exp>
  high_ask_price?: InputMaybe<Numeric_Comparison_Exp>
  high_price?: InputMaybe<Numeric_Comparison_Exp>
  low_bid_conf?: InputMaybe<Numeric_Comparison_Exp>
  low_bid_price?: InputMaybe<Numeric_Comparison_Exp>
  low_price?: InputMaybe<Numeric_Comparison_Exp>
  open_price?: InputMaybe<Numeric_Comparison_Exp>
  price_id?: InputMaybe<Bytea_Comparison_Exp>
  symbol?: InputMaybe<String_Comparison_Exp>
}

/** Ordering options when selecting data from "pyth_prices_1d". */
export type Pyth_Prices_1d_Order_By = {
  aggregated_time?: InputMaybe<Order_By>
  close_price?: InputMaybe<Order_By>
  env?: InputMaybe<Order_By>
  high_ask_conf?: InputMaybe<Order_By>
  high_ask_price?: InputMaybe<Order_By>
  high_price?: InputMaybe<Order_By>
  low_bid_conf?: InputMaybe<Order_By>
  low_bid_price?: InputMaybe<Order_By>
  low_price?: InputMaybe<Order_By>
  open_price?: InputMaybe<Order_By>
  price_id?: InputMaybe<Order_By>
  symbol?: InputMaybe<Order_By>
}

/** select columns of table "pyth_prices_1d" */
export enum Pyth_Prices_1d_Select_Column {
  /** column name */
  AggregatedTime = "aggregated_time",
  /** column name */
  ClosePrice = "close_price",
  /** column name */
  Env = "env",
  /** column name */
  HighAskConf = "high_ask_conf",
  /** column name */
  HighAskPrice = "high_ask_price",
  /** column name */
  HighPrice = "high_price",
  /** column name */
  LowBidConf = "low_bid_conf",
  /** column name */
  LowBidPrice = "low_bid_price",
  /** column name */
  LowPrice = "low_price",
  /** column name */
  OpenPrice = "open_price",
  /** column name */
  PriceId = "price_id",
  /** column name */
  Symbol = "symbol",
}

/** Streaming cursor of the table "pyth_prices_1d" */
export type Pyth_Prices_1d_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Pyth_Prices_1d_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Pyth_Prices_1d_Stream_Cursor_Value_Input = {
  aggregated_time?: InputMaybe<Scalars["timestamptz"]>
  close_price?: InputMaybe<Scalars["numeric"]>
  env?: InputMaybe<Scalars["String"]>
  high_ask_conf?: InputMaybe<Scalars["numeric"]>
  high_ask_price?: InputMaybe<Scalars["numeric"]>
  high_price?: InputMaybe<Scalars["numeric"]>
  low_bid_conf?: InputMaybe<Scalars["numeric"]>
  low_bid_price?: InputMaybe<Scalars["numeric"]>
  low_price?: InputMaybe<Scalars["numeric"]>
  open_price?: InputMaybe<Scalars["numeric"]>
  price_id?: InputMaybe<Scalars["bytea"]>
  symbol?: InputMaybe<Scalars["String"]>
}

/** columns and relationships of "pyth_prices_1h" */
export type Pyth_Prices_1h = {
  __typename?: "pyth_prices_1h"
  aggregated_time: Scalars["timestamptz"]
  close_price: Scalars["numeric"]
  env: Scalars["String"]
  high_ask_conf: Scalars["numeric"]
  high_ask_price: Scalars["numeric"]
  high_price: Scalars["numeric"]
  low_bid_conf: Scalars["numeric"]
  low_bid_price: Scalars["numeric"]
  low_price: Scalars["numeric"]
  open_price: Scalars["numeric"]
  price_id: Scalars["bytea"]
  symbol: Scalars["String"]
}

/** Boolean expression to filter rows from the table "pyth_prices_1h". All fields are combined with a logical 'AND'. */
export type Pyth_Prices_1h_Bool_Exp = {
  _and?: InputMaybe<Array<Pyth_Prices_1h_Bool_Exp>>
  _not?: InputMaybe<Pyth_Prices_1h_Bool_Exp>
  _or?: InputMaybe<Array<Pyth_Prices_1h_Bool_Exp>>
  aggregated_time?: InputMaybe<Timestamptz_Comparison_Exp>
  close_price?: InputMaybe<Numeric_Comparison_Exp>
  env?: InputMaybe<String_Comparison_Exp>
  high_ask_conf?: InputMaybe<Numeric_Comparison_Exp>
  high_ask_price?: InputMaybe<Numeric_Comparison_Exp>
  high_price?: InputMaybe<Numeric_Comparison_Exp>
  low_bid_conf?: InputMaybe<Numeric_Comparison_Exp>
  low_bid_price?: InputMaybe<Numeric_Comparison_Exp>
  low_price?: InputMaybe<Numeric_Comparison_Exp>
  open_price?: InputMaybe<Numeric_Comparison_Exp>
  price_id?: InputMaybe<Bytea_Comparison_Exp>
  symbol?: InputMaybe<String_Comparison_Exp>
}

/** Ordering options when selecting data from "pyth_prices_1h". */
export type Pyth_Prices_1h_Order_By = {
  aggregated_time?: InputMaybe<Order_By>
  close_price?: InputMaybe<Order_By>
  env?: InputMaybe<Order_By>
  high_ask_conf?: InputMaybe<Order_By>
  high_ask_price?: InputMaybe<Order_By>
  high_price?: InputMaybe<Order_By>
  low_bid_conf?: InputMaybe<Order_By>
  low_bid_price?: InputMaybe<Order_By>
  low_price?: InputMaybe<Order_By>
  open_price?: InputMaybe<Order_By>
  price_id?: InputMaybe<Order_By>
  symbol?: InputMaybe<Order_By>
}

/** select columns of table "pyth_prices_1h" */
export enum Pyth_Prices_1h_Select_Column {
  /** column name */
  AggregatedTime = "aggregated_time",
  /** column name */
  ClosePrice = "close_price",
  /** column name */
  Env = "env",
  /** column name */
  HighAskConf = "high_ask_conf",
  /** column name */
  HighAskPrice = "high_ask_price",
  /** column name */
  HighPrice = "high_price",
  /** column name */
  LowBidConf = "low_bid_conf",
  /** column name */
  LowBidPrice = "low_bid_price",
  /** column name */
  LowPrice = "low_price",
  /** column name */
  OpenPrice = "open_price",
  /** column name */
  PriceId = "price_id",
  /** column name */
  Symbol = "symbol",
}

/** Streaming cursor of the table "pyth_prices_1h" */
export type Pyth_Prices_1h_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Pyth_Prices_1h_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Pyth_Prices_1h_Stream_Cursor_Value_Input = {
  aggregated_time?: InputMaybe<Scalars["timestamptz"]>
  close_price?: InputMaybe<Scalars["numeric"]>
  env?: InputMaybe<Scalars["String"]>
  high_ask_conf?: InputMaybe<Scalars["numeric"]>
  high_ask_price?: InputMaybe<Scalars["numeric"]>
  high_price?: InputMaybe<Scalars["numeric"]>
  low_bid_conf?: InputMaybe<Scalars["numeric"]>
  low_bid_price?: InputMaybe<Scalars["numeric"]>
  low_price?: InputMaybe<Scalars["numeric"]>
  open_price?: InputMaybe<Scalars["numeric"]>
  price_id?: InputMaybe<Scalars["bytea"]>
  symbol?: InputMaybe<Scalars["String"]>
}

/** columns and relationships of "pyth_prices_1m" */
export type Pyth_Prices_1m = {
  __typename?: "pyth_prices_1m"
  aggregated_time: Scalars["timestamptz"]
  close_price: Scalars["numeric"]
  env: Scalars["String"]
  high_ask_conf: Scalars["numeric"]
  high_ask_price: Scalars["numeric"]
  high_price: Scalars["numeric"]
  low_bid_conf: Scalars["numeric"]
  low_bid_price: Scalars["numeric"]
  low_price: Scalars["numeric"]
  open_price: Scalars["numeric"]
  price_id: Scalars["bytea"]
  symbol: Scalars["String"]
}

/** Boolean expression to filter rows from the table "pyth_prices_1m". All fields are combined with a logical 'AND'. */
export type Pyth_Prices_1m_Bool_Exp = {
  _and?: InputMaybe<Array<Pyth_Prices_1m_Bool_Exp>>
  _not?: InputMaybe<Pyth_Prices_1m_Bool_Exp>
  _or?: InputMaybe<Array<Pyth_Prices_1m_Bool_Exp>>
  aggregated_time?: InputMaybe<Timestamptz_Comparison_Exp>
  close_price?: InputMaybe<Numeric_Comparison_Exp>
  env?: InputMaybe<String_Comparison_Exp>
  high_ask_conf?: InputMaybe<Numeric_Comparison_Exp>
  high_ask_price?: InputMaybe<Numeric_Comparison_Exp>
  high_price?: InputMaybe<Numeric_Comparison_Exp>
  low_bid_conf?: InputMaybe<Numeric_Comparison_Exp>
  low_bid_price?: InputMaybe<Numeric_Comparison_Exp>
  low_price?: InputMaybe<Numeric_Comparison_Exp>
  open_price?: InputMaybe<Numeric_Comparison_Exp>
  price_id?: InputMaybe<Bytea_Comparison_Exp>
  symbol?: InputMaybe<String_Comparison_Exp>
}

/** Ordering options when selecting data from "pyth_prices_1m". */
export type Pyth_Prices_1m_Order_By = {
  aggregated_time?: InputMaybe<Order_By>
  close_price?: InputMaybe<Order_By>
  env?: InputMaybe<Order_By>
  high_ask_conf?: InputMaybe<Order_By>
  high_ask_price?: InputMaybe<Order_By>
  high_price?: InputMaybe<Order_By>
  low_bid_conf?: InputMaybe<Order_By>
  low_bid_price?: InputMaybe<Order_By>
  low_price?: InputMaybe<Order_By>
  open_price?: InputMaybe<Order_By>
  price_id?: InputMaybe<Order_By>
  symbol?: InputMaybe<Order_By>
}

/** select columns of table "pyth_prices_1m" */
export enum Pyth_Prices_1m_Select_Column {
  /** column name */
  AggregatedTime = "aggregated_time",
  /** column name */
  ClosePrice = "close_price",
  /** column name */
  Env = "env",
  /** column name */
  HighAskConf = "high_ask_conf",
  /** column name */
  HighAskPrice = "high_ask_price",
  /** column name */
  HighPrice = "high_price",
  /** column name */
  LowBidConf = "low_bid_conf",
  /** column name */
  LowBidPrice = "low_bid_price",
  /** column name */
  LowPrice = "low_price",
  /** column name */
  OpenPrice = "open_price",
  /** column name */
  PriceId = "price_id",
  /** column name */
  Symbol = "symbol",
}

/** Streaming cursor of the table "pyth_prices_1m" */
export type Pyth_Prices_1m_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Pyth_Prices_1m_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Pyth_Prices_1m_Stream_Cursor_Value_Input = {
  aggregated_time?: InputMaybe<Scalars["timestamptz"]>
  close_price?: InputMaybe<Scalars["numeric"]>
  env?: InputMaybe<Scalars["String"]>
  high_ask_conf?: InputMaybe<Scalars["numeric"]>
  high_ask_price?: InputMaybe<Scalars["numeric"]>
  high_price?: InputMaybe<Scalars["numeric"]>
  low_bid_conf?: InputMaybe<Scalars["numeric"]>
  low_bid_price?: InputMaybe<Scalars["numeric"]>
  low_price?: InputMaybe<Scalars["numeric"]>
  open_price?: InputMaybe<Scalars["numeric"]>
  price_id?: InputMaybe<Scalars["bytea"]>
  symbol?: InputMaybe<Scalars["String"]>
}

/** columns and relationships of "pyth_prices_15m" */
export type Pyth_Prices_15m = {
  __typename?: "pyth_prices_15m"
  aggregated_time: Scalars["timestamptz"]
  close_price: Scalars["numeric"]
  env: Scalars["String"]
  high_ask_conf: Scalars["numeric"]
  high_ask_price: Scalars["numeric"]
  high_price: Scalars["numeric"]
  low_bid_conf: Scalars["numeric"]
  low_bid_price: Scalars["numeric"]
  low_price: Scalars["numeric"]
  open_price: Scalars["numeric"]
  price_id: Scalars["bytea"]
  symbol: Scalars["String"]
}

/** Boolean expression to filter rows from the table "pyth_prices_15m". All fields are combined with a logical 'AND'. */
export type Pyth_Prices_15m_Bool_Exp = {
  _and?: InputMaybe<Array<Pyth_Prices_15m_Bool_Exp>>
  _not?: InputMaybe<Pyth_Prices_15m_Bool_Exp>
  _or?: InputMaybe<Array<Pyth_Prices_15m_Bool_Exp>>
  aggregated_time?: InputMaybe<Timestamptz_Comparison_Exp>
  close_price?: InputMaybe<Numeric_Comparison_Exp>
  env?: InputMaybe<String_Comparison_Exp>
  high_ask_conf?: InputMaybe<Numeric_Comparison_Exp>
  high_ask_price?: InputMaybe<Numeric_Comparison_Exp>
  high_price?: InputMaybe<Numeric_Comparison_Exp>
  low_bid_conf?: InputMaybe<Numeric_Comparison_Exp>
  low_bid_price?: InputMaybe<Numeric_Comparison_Exp>
  low_price?: InputMaybe<Numeric_Comparison_Exp>
  open_price?: InputMaybe<Numeric_Comparison_Exp>
  price_id?: InputMaybe<Bytea_Comparison_Exp>
  symbol?: InputMaybe<String_Comparison_Exp>
}

/** Ordering options when selecting data from "pyth_prices_15m". */
export type Pyth_Prices_15m_Order_By = {
  aggregated_time?: InputMaybe<Order_By>
  close_price?: InputMaybe<Order_By>
  env?: InputMaybe<Order_By>
  high_ask_conf?: InputMaybe<Order_By>
  high_ask_price?: InputMaybe<Order_By>
  high_price?: InputMaybe<Order_By>
  low_bid_conf?: InputMaybe<Order_By>
  low_bid_price?: InputMaybe<Order_By>
  low_price?: InputMaybe<Order_By>
  open_price?: InputMaybe<Order_By>
  price_id?: InputMaybe<Order_By>
  symbol?: InputMaybe<Order_By>
}

/** select columns of table "pyth_prices_15m" */
export enum Pyth_Prices_15m_Select_Column {
  /** column name */
  AggregatedTime = "aggregated_time",
  /** column name */
  ClosePrice = "close_price",
  /** column name */
  Env = "env",
  /** column name */
  HighAskConf = "high_ask_conf",
  /** column name */
  HighAskPrice = "high_ask_price",
  /** column name */
  HighPrice = "high_price",
  /** column name */
  LowBidConf = "low_bid_conf",
  /** column name */
  LowBidPrice = "low_bid_price",
  /** column name */
  LowPrice = "low_price",
  /** column name */
  OpenPrice = "open_price",
  /** column name */
  PriceId = "price_id",
  /** column name */
  Symbol = "symbol",
}

/** Streaming cursor of the table "pyth_prices_15m" */
export type Pyth_Prices_15m_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Pyth_Prices_15m_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Pyth_Prices_15m_Stream_Cursor_Value_Input = {
  aggregated_time?: InputMaybe<Scalars["timestamptz"]>
  close_price?: InputMaybe<Scalars["numeric"]>
  env?: InputMaybe<Scalars["String"]>
  high_ask_conf?: InputMaybe<Scalars["numeric"]>
  high_ask_price?: InputMaybe<Scalars["numeric"]>
  high_price?: InputMaybe<Scalars["numeric"]>
  low_bid_conf?: InputMaybe<Scalars["numeric"]>
  low_bid_price?: InputMaybe<Scalars["numeric"]>
  low_price?: InputMaybe<Scalars["numeric"]>
  open_price?: InputMaybe<Scalars["numeric"]>
  price_id?: InputMaybe<Scalars["bytea"]>
  symbol?: InputMaybe<Scalars["String"]>
}

/** Boolean expression to filter rows from the table "pyth_prices". All fields are combined with a logical 'AND'. */
export type Pyth_Prices_Bool_Exp = {
  _and?: InputMaybe<Array<Pyth_Prices_Bool_Exp>>
  _not?: InputMaybe<Pyth_Prices_Bool_Exp>
  _or?: InputMaybe<Array<Pyth_Prices_Bool_Exp>>
  conf?: InputMaybe<Numeric_Comparison_Exp>
  ema_conf?: InputMaybe<Numeric_Comparison_Exp>
  ema_price?: InputMaybe<Numeric_Comparison_Exp>
  env?: InputMaybe<String_Comparison_Exp>
  expo?: InputMaybe<Int_Comparison_Exp>
  id?: InputMaybe<Bigint_Comparison_Exp>
  price?: InputMaybe<Numeric_Comparison_Exp>
  price_data?: InputMaybe<Bytea_Comparison_Exp>
  price_id?: InputMaybe<Bytea_Comparison_Exp>
  publish_time?: InputMaybe<Timestamptz_Comparison_Exp>
  symbol?: InputMaybe<String_Comparison_Exp>
}

/** Ordering options when selecting data from "pyth_prices". */
export type Pyth_Prices_Order_By = {
  conf?: InputMaybe<Order_By>
  ema_conf?: InputMaybe<Order_By>
  ema_price?: InputMaybe<Order_By>
  env?: InputMaybe<Order_By>
  expo?: InputMaybe<Order_By>
  id?: InputMaybe<Order_By>
  price?: InputMaybe<Order_By>
  price_data?: InputMaybe<Order_By>
  price_id?: InputMaybe<Order_By>
  publish_time?: InputMaybe<Order_By>
  symbol?: InputMaybe<Order_By>
}

/** select columns of table "pyth_prices" */
export enum Pyth_Prices_Select_Column {
  /** column name */
  Conf = "conf",
  /** column name */
  EmaConf = "ema_conf",
  /** column name */
  EmaPrice = "ema_price",
  /** column name */
  Env = "env",
  /** column name */
  Expo = "expo",
  /** column name */
  Id = "id",
  /** column name */
  Price = "price",
  /** column name */
  PriceData = "price_data",
  /** column name */
  PriceId = "price_id",
  /** column name */
  PublishTime = "publish_time",
  /** column name */
  Symbol = "symbol",
}

/** Streaming cursor of the table "pyth_prices" */
export type Pyth_Prices_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Pyth_Prices_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Pyth_Prices_Stream_Cursor_Value_Input = {
  conf?: InputMaybe<Scalars["numeric"]>
  ema_conf?: InputMaybe<Scalars["numeric"]>
  ema_price?: InputMaybe<Scalars["numeric"]>
  env?: InputMaybe<Scalars["String"]>
  expo?: InputMaybe<Scalars["Int"]>
  id?: InputMaybe<Scalars["bigint"]>
  price?: InputMaybe<Scalars["numeric"]>
  price_data?: InputMaybe<Scalars["bytea"]>
  price_id?: InputMaybe<Scalars["bytea"]>
  publish_time?: InputMaybe<Scalars["timestamptz"]>
  symbol?: InputMaybe<Scalars["String"]>
}

export type Query_Root = {
  __typename?: "query_root"
  /** fetch data from the table: "ad_ranked_holders" */
  ad_ranked_holders: Array<Ad_Ranked_Holders>
  /** fetch data from the table: "ad_summary" */
  ad_summary: Array<Ad_Summary>
  /** fetch data from the table: "bsc.blocks" */
  bsc_blocks: Array<Bsc_Blocks>
  /** fetch data from the table: "bsc.blocks" using primary key columns */
  bsc_blocks_by_pk?: Maybe<Bsc_Blocks>
  /** fetch data from the table: "closed_market_orders" */
  closed_market_orders: Array<Closed_Market_Orders>
  /** fetch data from the table: "closed_market_orders" using primary key columns */
  closed_market_orders_by_pk?: Maybe<Closed_Market_Orders>
  /** fetch data from the table: "cp_referee_summary" */
  cp_referee_summary: Array<Cp_Referee_Summary>
  /** fetch data from the table: "cp_referral_counter" */
  cp_referral_counter: Array<Cp_Referral_Counter>
  /** fetch data from the table: "cp_referrer_summary" */
  cp_referrer_summary: Array<Cp_Referrer_Summary>
  /** fetch aggregated fields from the table: "cp_referrer_summary" */
  cp_referrer_summary_aggregate: Cp_Referrer_Summary_Aggregate
  /** fetch data from the table: "genesis_pass_holders" */
  genesis_pass_holders: Array<Genesis_Pass_Holders>
  /** fetch data from the table: "latest_pyth_prices" */
  latest_pyth_prices: Array<Latest_Pyth_Prices>
  /** fetch aggregated fields from the table: "latest_pyth_prices" */
  latest_pyth_prices_aggregate: Latest_Pyth_Prices_Aggregate
  /** fetch data from the table: "latest_pyth_prices" using primary key columns */
  latest_pyth_prices_by_pk?: Maybe<Latest_Pyth_Prices>
  /** fetch data from the table: "latest_trade_stats" */
  latest_trade_stats: Array<Latest_Trade_Stats>
  /** fetch data from the table: "latest_trade_stats_7d" */
  latest_trade_stats_7d: Array<Latest_Trade_Stats_7d>
  /** fetch data from the table: "liquidity_bootstrap_stage_1" */
  liquidity_bootstrap_stage_1: Array<Liquidity_Bootstrap_Stage_1>
  /** fetch data from the table: "liquidity_bootstrap_stage_2" */
  liquidity_bootstrap_stage_2: Array<Liquidity_Bootstrap_Stage_2>
  /** fetch data from the table: "liquidity_bootstrap_stage_3" */
  liquidity_bootstrap_stage_3: Array<Liquidity_Bootstrap_Stage_3>
  /** fetch data from the table: "open_limit_orders" */
  open_limit_orders: Array<Open_Limit_Orders>
  /** fetch data from the table: "open_limit_orders" using primary key columns */
  open_limit_orders_by_pk?: Maybe<Open_Limit_Orders>
  /** fetch data from the table: "open_market_orders" */
  open_market_orders: Array<Open_Market_Orders>
  /** fetch data from the table: "open_market_orders" using primary key columns */
  open_market_orders_by_pk?: Maybe<Open_Market_Orders>
  /** fetch data from the table: "per_block_trade_stats" */
  per_block_trade_stats: Array<Per_Block_Trade_Stats>
  /** fetch data from the table: "per_block_trade_stats" using primary key columns */
  per_block_trade_stats_by_pk?: Maybe<Per_Block_Trade_Stats>
  /** fetch data from the table: "pyth_prices" */
  pyth_prices: Array<Pyth_Prices>
  /** fetch data from the table: "pyth_prices_1d" */
  pyth_prices_1d: Array<Pyth_Prices_1d>
  /** fetch data from the table: "pyth_prices_1d" using primary key columns */
  pyth_prices_1d_by_pk?: Maybe<Pyth_Prices_1d>
  /** fetch data from the table: "pyth_prices_1h" */
  pyth_prices_1h: Array<Pyth_Prices_1h>
  /** fetch data from the table: "pyth_prices_1h" using primary key columns */
  pyth_prices_1h_by_pk?: Maybe<Pyth_Prices_1h>
  /** fetch data from the table: "pyth_prices_1m" */
  pyth_prices_1m: Array<Pyth_Prices_1m>
  /** fetch data from the table: "pyth_prices_1m" using primary key columns */
  pyth_prices_1m_by_pk?: Maybe<Pyth_Prices_1m>
  /** fetch data from the table: "pyth_prices_15m" */
  pyth_prices_15m: Array<Pyth_Prices_15m>
  /** fetch data from the table: "pyth_prices_15m" using primary key columns */
  pyth_prices_15m_by_pk?: Maybe<Pyth_Prices_15m>
  /** fetch data from the table: "pyth_prices" using primary key columns */
  pyth_prices_by_pk?: Maybe<Pyth_Prices>
  /** fetch data from the table: "share_links" */
  share_links: Array<Share_Links>
  /** fetch data from the table: "share_links" using primary key columns */
  share_links_by_pk?: Maybe<Share_Links>
  /** fetch data from the table: "testnet_poap_holders" */
  testnet_poap_holders: Array<Testnet_Poap_Holders>
  /** fetch data from the table: "trade_bootstrap_stage_1" */
  trade_bootstrap_stage_1: Array<Trade_Bootstrap_Stage_1>
  /** fetch data from the table: "trade_bootstrap_stage_2" */
  trade_bootstrap_stage_2: Array<Trade_Bootstrap_Stage_2>
  /** fetch data from the table: "traders" */
  traders: Array<Traders>
  /** fetch data from the table: "trading_total" */
  trading_total: Array<Trading_Total>
  /** fetch data from the table: "uniwhale_v1_bsc.LimitBook_evt_CloseLimitOrderEvent" */
  uniwhale_v1_bsc_LimitBook_evt_CloseLimitOrderEvent: Array<Uniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.LimitBook_evt_CloseLimitOrderEvent" using primary key columns */
  uniwhale_v1_bsc_LimitBook_evt_CloseLimitOrderEvent_by_pk?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.LimitBook_evt_ExecuteLimitOrderEvent" */
  uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent: Array<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent>
  /** fetch aggregated fields from the table: "uniwhale_v1_bsc.LimitBook_evt_ExecuteLimitOrderEvent" */
  uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent_aggregate: Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Aggregate
  /** fetch data from the table: "uniwhale_v1_bsc.LimitBook_evt_ExecuteLimitOrderEvent" using primary key columns */
  uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent_by_pk?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.LimitBook_evt_OpenLimitOrderEvent" */
  uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent: Array<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent>
  /** fetch aggregated fields from the table: "uniwhale_v1_bsc.LimitBook_evt_OpenLimitOrderEvent" */
  uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent_aggregate: Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Aggregate
  /** fetch data from the table: "uniwhale_v1_bsc.LimitBook_evt_OpenLimitOrderEvent" using primary key columns */
  uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent_by_pk?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.LimitBook_evt_PartialCloseLimitOrderEvent" */
  uniwhale_v1_bsc_LimitBook_evt_PartialCloseLimitOrderEvent: Array<Uniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.LimitBook_evt_PartialCloseLimitOrderEvent" using primary key columns */
  uniwhale_v1_bsc_LimitBook_evt_PartialCloseLimitOrderEvent_by_pk?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.LimitBook_evt_UpdateOpenLimitOrderEvent" */
  uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent: Array<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent>
  /** fetch aggregated fields from the table: "uniwhale_v1_bsc.LimitBook_evt_UpdateOpenLimitOrderEvent" */
  uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent_aggregate: Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Aggregate
  /** fetch data from the table: "uniwhale_v1_bsc.LimitBook_evt_UpdateOpenLimitOrderEvent" using primary key columns */
  uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent_by_pk?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.Referrals_evt_SetDefaultRebatePctEvent" */
  uniwhale_v1_bsc_Referrals_evt_SetDefaultRebatePctEvent: Array<Uniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.Referrals_evt_SetDefaultRebatePctEvent" using primary key columns */
  uniwhale_v1_bsc_Referrals_evt_SetDefaultRebatePctEvent_by_pk?: Maybe<Uniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.Referrals_evt_SetRebatePctEvent" */
  uniwhale_v1_bsc_Referrals_evt_SetRebatePctEvent: Array<Uniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.Referrals_evt_SetRebatePctEvent" using primary key columns */
  uniwhale_v1_bsc_Referrals_evt_SetRebatePctEvent_by_pk?: Maybe<Uniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.TradingCore_evt_CloseMarketOrderEvent" */
  uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent: Array<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent>
  /** fetch aggregated fields from the table: "uniwhale_v1_bsc.TradingCore_evt_CloseMarketOrderEvent" */
  uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent_aggregate: Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Aggregate
  /** fetch data from the table: "uniwhale_v1_bsc.TradingCore_evt_CloseMarketOrderEvent" using primary key columns */
  uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent_by_pk?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.TradingCore_evt_OpenMarketOrderEvent" */
  uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent: Array<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent>
  /** fetch aggregated fields from the table: "uniwhale_v1_bsc.TradingCore_evt_OpenMarketOrderEvent" */
  uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent_aggregate: Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Aggregate
  /** fetch data from the table: "uniwhale_v1_bsc.TradingCore_evt_OpenMarketOrderEvent" using primary key columns */
  uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent_by_pk?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.TradingCore_evt_UpdateOpenOrderEvent" */
  uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent: Array<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent>
  /** fetch aggregated fields from the table: "uniwhale_v1_bsc.TradingCore_evt_UpdateOpenOrderEvent" */
  uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent_aggregate: Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Aggregate
  /** fetch data from the table: "uniwhale_v1_bsc.TradingCore_evt_UpdateOpenOrderEvent" using primary key columns */
  uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent_by_pk?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.UniwhaleGenesisPass_evt_Transfer" */
  uniwhale_v1_bsc_UniwhaleGenesisPass_evt_Transfer: Array<Uniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_Transfer>
  /** fetch data from the table: "uniwhale_v1_bsc.UniwhaleGenesisPass_evt_Transfer" using primary key columns */
  uniwhale_v1_bsc_UniwhaleGenesisPass_evt_Transfer_by_pk?: Maybe<Uniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_Transfer>
  /** fetch data from the table: "uniwhale_v1_bsc.UniwhaleTestnetPOAP_evt_Transfer" */
  uniwhale_v1_bsc_UniwhaleTestnetPOAP_evt_Transfer: Array<Uniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_Transfer>
  /** fetch data from the table: "uniwhale_v1_bsc.UniwhaleTestnetPOAP_evt_Transfer" using primary key columns */
  uniwhale_v1_bsc_UniwhaleTestnetPOAP_evt_Transfer_by_pk?: Maybe<Uniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_Transfer>
}

export type Query_RootAd_Ranked_HoldersArgs = {
  distinct_on?: InputMaybe<Array<Ad_Ranked_Holders_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Ad_Ranked_Holders_Order_By>>
  where?: InputMaybe<Ad_Ranked_Holders_Bool_Exp>
}

export type Query_RootAd_SummaryArgs = {
  distinct_on?: InputMaybe<Array<Ad_Summary_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Ad_Summary_Order_By>>
  where?: InputMaybe<Ad_Summary_Bool_Exp>
}

export type Query_RootBsc_BlocksArgs = {
  distinct_on?: InputMaybe<Array<Bsc_Blocks_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Bsc_Blocks_Order_By>>
  where?: InputMaybe<Bsc_Blocks_Bool_Exp>
}

export type Query_RootBsc_Blocks_By_PkArgs = {
  hash: Scalars["bytea"]
}

export type Query_RootClosed_Market_OrdersArgs = {
  distinct_on?: InputMaybe<Array<Closed_Market_Orders_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Closed_Market_Orders_Order_By>>
  where?: InputMaybe<Closed_Market_Orders_Bool_Exp>
}

export type Query_RootClosed_Market_Orders_By_PkArgs = {
  order_hash: Scalars["bytea"]
}

export type Query_RootCp_Referee_SummaryArgs = {
  distinct_on?: InputMaybe<Array<Cp_Referee_Summary_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Cp_Referee_Summary_Order_By>>
  where?: InputMaybe<Cp_Referee_Summary_Bool_Exp>
}

export type Query_RootCp_Referral_CounterArgs = {
  distinct_on?: InputMaybe<Array<Cp_Referral_Counter_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Cp_Referral_Counter_Order_By>>
  where?: InputMaybe<Cp_Referral_Counter_Bool_Exp>
}

export type Query_RootCp_Referrer_SummaryArgs = {
  distinct_on?: InputMaybe<Array<Cp_Referrer_Summary_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Cp_Referrer_Summary_Order_By>>
  where?: InputMaybe<Cp_Referrer_Summary_Bool_Exp>
}

export type Query_RootCp_Referrer_Summary_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cp_Referrer_Summary_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Cp_Referrer_Summary_Order_By>>
  where?: InputMaybe<Cp_Referrer_Summary_Bool_Exp>
}

export type Query_RootGenesis_Pass_HoldersArgs = {
  distinct_on?: InputMaybe<Array<Genesis_Pass_Holders_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Genesis_Pass_Holders_Order_By>>
  where?: InputMaybe<Genesis_Pass_Holders_Bool_Exp>
}

export type Query_RootLatest_Pyth_PricesArgs = {
  distinct_on?: InputMaybe<Array<Latest_Pyth_Prices_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Latest_Pyth_Prices_Order_By>>
  where?: InputMaybe<Latest_Pyth_Prices_Bool_Exp>
}

export type Query_RootLatest_Pyth_Prices_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Latest_Pyth_Prices_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Latest_Pyth_Prices_Order_By>>
  where?: InputMaybe<Latest_Pyth_Prices_Bool_Exp>
}

export type Query_RootLatest_Pyth_Prices_By_PkArgs = {
  price_id: Scalars["bytea"]
}

export type Query_RootLatest_Trade_StatsArgs = {
  distinct_on?: InputMaybe<Array<Latest_Trade_Stats_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Latest_Trade_Stats_Order_By>>
  where?: InputMaybe<Latest_Trade_Stats_Bool_Exp>
}

export type Query_RootLatest_Trade_Stats_7dArgs = {
  distinct_on?: InputMaybe<Array<Latest_Trade_Stats_7d_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Latest_Trade_Stats_7d_Order_By>>
  where?: InputMaybe<Latest_Trade_Stats_7d_Bool_Exp>
}

export type Query_RootLiquidity_Bootstrap_Stage_1Args = {
  distinct_on?: InputMaybe<Array<Liquidity_Bootstrap_Stage_1_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Liquidity_Bootstrap_Stage_1_Order_By>>
  where?: InputMaybe<Liquidity_Bootstrap_Stage_1_Bool_Exp>
}

export type Query_RootLiquidity_Bootstrap_Stage_2Args = {
  distinct_on?: InputMaybe<Array<Liquidity_Bootstrap_Stage_2_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Liquidity_Bootstrap_Stage_2_Order_By>>
  where?: InputMaybe<Liquidity_Bootstrap_Stage_2_Bool_Exp>
}

export type Query_RootLiquidity_Bootstrap_Stage_3Args = {
  distinct_on?: InputMaybe<Array<Liquidity_Bootstrap_Stage_3_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Liquidity_Bootstrap_Stage_3_Order_By>>
  where?: InputMaybe<Liquidity_Bootstrap_Stage_3_Bool_Exp>
}

export type Query_RootOpen_Limit_OrdersArgs = {
  distinct_on?: InputMaybe<Array<Open_Limit_Orders_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Open_Limit_Orders_Order_By>>
  where?: InputMaybe<Open_Limit_Orders_Bool_Exp>
}

export type Query_RootOpen_Limit_Orders_By_PkArgs = {
  order_hash: Scalars["bytea"]
}

export type Query_RootOpen_Market_OrdersArgs = {
  distinct_on?: InputMaybe<Array<Open_Market_Orders_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Open_Market_Orders_Order_By>>
  where?: InputMaybe<Open_Market_Orders_Bool_Exp>
}

export type Query_RootOpen_Market_Orders_By_PkArgs = {
  order_hash: Scalars["bytea"]
}

export type Query_RootPer_Block_Trade_StatsArgs = {
  distinct_on?: InputMaybe<Array<Per_Block_Trade_Stats_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Per_Block_Trade_Stats_Order_By>>
  where?: InputMaybe<Per_Block_Trade_Stats_Bool_Exp>
}

export type Query_RootPer_Block_Trade_Stats_By_PkArgs = {
  block_number: Scalars["Int"]
}

export type Query_RootPyth_PricesArgs = {
  distinct_on?: InputMaybe<Array<Pyth_Prices_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Pyth_Prices_Order_By>>
  where?: InputMaybe<Pyth_Prices_Bool_Exp>
}

export type Query_RootPyth_Prices_1dArgs = {
  distinct_on?: InputMaybe<Array<Pyth_Prices_1d_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Pyth_Prices_1d_Order_By>>
  where?: InputMaybe<Pyth_Prices_1d_Bool_Exp>
}

export type Query_RootPyth_Prices_1d_By_PkArgs = {
  aggregated_time: Scalars["timestamptz"]
  price_id: Scalars["bytea"]
}

export type Query_RootPyth_Prices_1hArgs = {
  distinct_on?: InputMaybe<Array<Pyth_Prices_1h_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Pyth_Prices_1h_Order_By>>
  where?: InputMaybe<Pyth_Prices_1h_Bool_Exp>
}

export type Query_RootPyth_Prices_1h_By_PkArgs = {
  aggregated_time: Scalars["timestamptz"]
  price_id: Scalars["bytea"]
}

export type Query_RootPyth_Prices_1mArgs = {
  distinct_on?: InputMaybe<Array<Pyth_Prices_1m_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Pyth_Prices_1m_Order_By>>
  where?: InputMaybe<Pyth_Prices_1m_Bool_Exp>
}

export type Query_RootPyth_Prices_1m_By_PkArgs = {
  aggregated_time: Scalars["timestamptz"]
  price_id: Scalars["bytea"]
}

export type Query_RootPyth_Prices_15mArgs = {
  distinct_on?: InputMaybe<Array<Pyth_Prices_15m_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Pyth_Prices_15m_Order_By>>
  where?: InputMaybe<Pyth_Prices_15m_Bool_Exp>
}

export type Query_RootPyth_Prices_15m_By_PkArgs = {
  aggregated_time: Scalars["timestamptz"]
  price_id: Scalars["bytea"]
}

export type Query_RootPyth_Prices_By_PkArgs = {
  id: Scalars["bigint"]
}

export type Query_RootShare_LinksArgs = {
  distinct_on?: InputMaybe<Array<Share_Links_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Share_Links_Order_By>>
  where?: InputMaybe<Share_Links_Bool_Exp>
}

export type Query_RootShare_Links_By_PkArgs = {
  id: Scalars["String"]
}

export type Query_RootTestnet_Poap_HoldersArgs = {
  distinct_on?: InputMaybe<Array<Testnet_Poap_Holders_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Testnet_Poap_Holders_Order_By>>
  where?: InputMaybe<Testnet_Poap_Holders_Bool_Exp>
}

export type Query_RootTrade_Bootstrap_Stage_1Args = {
  distinct_on?: InputMaybe<Array<Trade_Bootstrap_Stage_1_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Trade_Bootstrap_Stage_1_Order_By>>
  where?: InputMaybe<Trade_Bootstrap_Stage_1_Bool_Exp>
}

export type Query_RootTrade_Bootstrap_Stage_2Args = {
  distinct_on?: InputMaybe<Array<Trade_Bootstrap_Stage_2_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Trade_Bootstrap_Stage_2_Order_By>>
  where?: InputMaybe<Trade_Bootstrap_Stage_2_Bool_Exp>
}

export type Query_RootTradersArgs = {
  distinct_on?: InputMaybe<Array<Traders_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Traders_Order_By>>
  where?: InputMaybe<Traders_Bool_Exp>
}

export type Query_RootTrading_TotalArgs = {
  distinct_on?: InputMaybe<Array<Trading_Total_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Trading_Total_Order_By>>
  where?: InputMaybe<Trading_Total_Bool_Exp>
}

export type Query_RootUniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEventArgs = {
  distinct_on?: InputMaybe<
    Array<Uniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEvent_Select_Column>
  >
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<
    Array<Uniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEvent_Order_By>
  >
  where?: InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEvent_Bool_Exp>
}

export type Query_RootUniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEvent_By_PkArgs =
  {
    evt_index: Scalars["numeric"]
    evt_tx_hash: Scalars["bytea"]
  }

export type Query_RootUniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEventArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Bool_Exp>
  }

export type Query_RootUniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_AggregateArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Bool_Exp>
  }

export type Query_RootUniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_By_PkArgs =
  {
    evt_index: Scalars["numeric"]
    evt_tx_hash: Scalars["bytea"]
  }

export type Query_RootUniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEventArgs = {
  distinct_on?: InputMaybe<
    Array<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Select_Column>
  >
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<
    Array<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Order_By>
  >
  where?: InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Bool_Exp>
}

export type Query_RootUniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_AggregateArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Bool_Exp>
  }

export type Query_RootUniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_By_PkArgs =
  {
    evt_index: Scalars["numeric"]
    evt_tx_hash: Scalars["bytea"]
  }

export type Query_RootUniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEventArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEvent_Bool_Exp>
  }

export type Query_RootUniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEvent_By_PkArgs =
  {
    evt_index: Scalars["numeric"]
    evt_tx_hash: Scalars["bytea"]
  }

export type Query_RootUniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEventArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Bool_Exp>
  }

export type Query_RootUniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_AggregateArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Bool_Exp>
  }

export type Query_RootUniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_By_PkArgs =
  {
    evt_index: Scalars["numeric"]
    evt_tx_hash: Scalars["bytea"]
  }

export type Query_RootUniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEventArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEvent_Bool_Exp>
  }

export type Query_RootUniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEvent_By_PkArgs =
  {
    evt_index: Scalars["numeric"]
    evt_tx_hash: Scalars["bytea"]
  }

export type Query_RootUniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEventArgs = {
  distinct_on?: InputMaybe<
    Array<Uniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEvent_Select_Column>
  >
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<
    Array<Uniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEvent_Order_By>
  >
  where?: InputMaybe<Uniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEvent_Bool_Exp>
}

export type Query_RootUniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEvent_By_PkArgs =
  {
    evt_index: Scalars["numeric"]
    evt_tx_hash: Scalars["bytea"]
  }

export type Query_RootUniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEventArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Bool_Exp>
  }

export type Query_RootUniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_AggregateArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Bool_Exp>
  }

export type Query_RootUniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_By_PkArgs =
  {
    evt_index: Scalars["numeric"]
    evt_tx_hash: Scalars["bytea"]
  }

export type Query_RootUniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEventArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Bool_Exp>
  }

export type Query_RootUniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_AggregateArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Bool_Exp>
  }

export type Query_RootUniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_By_PkArgs =
  {
    evt_index: Scalars["numeric"]
    evt_tx_hash: Scalars["bytea"]
  }

export type Query_RootUniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEventArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Bool_Exp>
  }

export type Query_RootUniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_AggregateArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Bool_Exp>
  }

export type Query_RootUniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_By_PkArgs =
  {
    evt_index: Scalars["numeric"]
    evt_tx_hash: Scalars["bytea"]
  }

export type Query_RootUniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_TransferArgs = {
  distinct_on?: InputMaybe<
    Array<Uniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_Transfer_Select_Column>
  >
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<
    Array<Uniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_Transfer_Order_By>
  >
  where?: InputMaybe<Uniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_Transfer_Bool_Exp>
}

export type Query_RootUniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_Transfer_By_PkArgs =
  {
    evt_index: Scalars["numeric"]
    evt_tx_hash: Scalars["bytea"]
  }

export type Query_RootUniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_TransferArgs = {
  distinct_on?: InputMaybe<
    Array<Uniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_Transfer_Select_Column>
  >
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<
    Array<Uniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_Transfer_Order_By>
  >
  where?: InputMaybe<Uniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_Transfer_Bool_Exp>
}

export type Query_RootUniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_Transfer_By_PkArgs =
  {
    evt_index: Scalars["numeric"]
    evt_tx_hash: Scalars["bytea"]
  }

/** columns and relationships of "share_links" */
export type Share_Links = {
  __typename?: "share_links"
  created_at: Scalars["timestamptz"]
  id: Scalars["String"]
  referrer_address: Scalars["bytea"]
}

/** Boolean expression to filter rows from the table "share_links". All fields are combined with a logical 'AND'. */
export type Share_Links_Bool_Exp = {
  _and?: InputMaybe<Array<Share_Links_Bool_Exp>>
  _not?: InputMaybe<Share_Links_Bool_Exp>
  _or?: InputMaybe<Array<Share_Links_Bool_Exp>>
  created_at?: InputMaybe<Timestamptz_Comparison_Exp>
  id?: InputMaybe<String_Comparison_Exp>
  referrer_address?: InputMaybe<Bytea_Comparison_Exp>
}

/** Ordering options when selecting data from "share_links". */
export type Share_Links_Order_By = {
  created_at?: InputMaybe<Order_By>
  id?: InputMaybe<Order_By>
  referrer_address?: InputMaybe<Order_By>
}

/** select columns of table "share_links" */
export enum Share_Links_Select_Column {
  /** column name */
  CreatedAt = "created_at",
  /** column name */
  Id = "id",
  /** column name */
  ReferrerAddress = "referrer_address",
}

/** Streaming cursor of the table "share_links" */
export type Share_Links_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Share_Links_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Share_Links_Stream_Cursor_Value_Input = {
  created_at?: InputMaybe<Scalars["timestamptz"]>
  id?: InputMaybe<Scalars["String"]>
  referrer_address?: InputMaybe<Scalars["bytea"]>
}

export type Subscription_Root = {
  __typename?: "subscription_root"
  /** fetch data from the table: "ad_ranked_holders" */
  ad_ranked_holders: Array<Ad_Ranked_Holders>
  /** fetch data from the table in a streaming manner: "ad_ranked_holders" */
  ad_ranked_holders_stream: Array<Ad_Ranked_Holders>
  /** fetch data from the table: "ad_summary" */
  ad_summary: Array<Ad_Summary>
  /** fetch data from the table in a streaming manner: "ad_summary" */
  ad_summary_stream: Array<Ad_Summary>
  /** fetch data from the table: "bsc.blocks" */
  bsc_blocks: Array<Bsc_Blocks>
  /** fetch data from the table: "bsc.blocks" using primary key columns */
  bsc_blocks_by_pk?: Maybe<Bsc_Blocks>
  /** fetch data from the table in a streaming manner: "bsc.blocks" */
  bsc_blocks_stream: Array<Bsc_Blocks>
  /** fetch data from the table: "closed_market_orders" */
  closed_market_orders: Array<Closed_Market_Orders>
  /** fetch data from the table: "closed_market_orders" using primary key columns */
  closed_market_orders_by_pk?: Maybe<Closed_Market_Orders>
  /** fetch data from the table in a streaming manner: "closed_market_orders" */
  closed_market_orders_stream: Array<Closed_Market_Orders>
  /** fetch data from the table: "cp_referee_summary" */
  cp_referee_summary: Array<Cp_Referee_Summary>
  /** fetch data from the table in a streaming manner: "cp_referee_summary" */
  cp_referee_summary_stream: Array<Cp_Referee_Summary>
  /** fetch data from the table: "cp_referral_counter" */
  cp_referral_counter: Array<Cp_Referral_Counter>
  /** fetch data from the table in a streaming manner: "cp_referral_counter" */
  cp_referral_counter_stream: Array<Cp_Referral_Counter>
  /** fetch data from the table: "cp_referrer_summary" */
  cp_referrer_summary: Array<Cp_Referrer_Summary>
  /** fetch aggregated fields from the table: "cp_referrer_summary" */
  cp_referrer_summary_aggregate: Cp_Referrer_Summary_Aggregate
  /** fetch data from the table in a streaming manner: "cp_referrer_summary" */
  cp_referrer_summary_stream: Array<Cp_Referrer_Summary>
  /** fetch data from the table: "genesis_pass_holders" */
  genesis_pass_holders: Array<Genesis_Pass_Holders>
  /** fetch data from the table in a streaming manner: "genesis_pass_holders" */
  genesis_pass_holders_stream: Array<Genesis_Pass_Holders>
  /** fetch data from the table: "latest_pyth_prices" */
  latest_pyth_prices: Array<Latest_Pyth_Prices>
  /** fetch aggregated fields from the table: "latest_pyth_prices" */
  latest_pyth_prices_aggregate: Latest_Pyth_Prices_Aggregate
  /** fetch data from the table: "latest_pyth_prices" using primary key columns */
  latest_pyth_prices_by_pk?: Maybe<Latest_Pyth_Prices>
  /** fetch data from the table in a streaming manner: "latest_pyth_prices" */
  latest_pyth_prices_stream: Array<Latest_Pyth_Prices>
  /** fetch data from the table: "latest_trade_stats" */
  latest_trade_stats: Array<Latest_Trade_Stats>
  /** fetch data from the table: "latest_trade_stats_7d" */
  latest_trade_stats_7d: Array<Latest_Trade_Stats_7d>
  /** fetch data from the table in a streaming manner: "latest_trade_stats_7d" */
  latest_trade_stats_7d_stream: Array<Latest_Trade_Stats_7d>
  /** fetch data from the table in a streaming manner: "latest_trade_stats" */
  latest_trade_stats_stream: Array<Latest_Trade_Stats>
  /** fetch data from the table: "liquidity_bootstrap_stage_1" */
  liquidity_bootstrap_stage_1: Array<Liquidity_Bootstrap_Stage_1>
  /** fetch data from the table in a streaming manner: "liquidity_bootstrap_stage_1" */
  liquidity_bootstrap_stage_1_stream: Array<Liquidity_Bootstrap_Stage_1>
  /** fetch data from the table: "liquidity_bootstrap_stage_2" */
  liquidity_bootstrap_stage_2: Array<Liquidity_Bootstrap_Stage_2>
  /** fetch data from the table in a streaming manner: "liquidity_bootstrap_stage_2" */
  liquidity_bootstrap_stage_2_stream: Array<Liquidity_Bootstrap_Stage_2>
  /** fetch data from the table: "liquidity_bootstrap_stage_3" */
  liquidity_bootstrap_stage_3: Array<Liquidity_Bootstrap_Stage_3>
  /** fetch data from the table in a streaming manner: "liquidity_bootstrap_stage_3" */
  liquidity_bootstrap_stage_3_stream: Array<Liquidity_Bootstrap_Stage_3>
  /** fetch data from the table: "open_limit_orders" */
  open_limit_orders: Array<Open_Limit_Orders>
  /** fetch data from the table: "open_limit_orders" using primary key columns */
  open_limit_orders_by_pk?: Maybe<Open_Limit_Orders>
  /** fetch data from the table in a streaming manner: "open_limit_orders" */
  open_limit_orders_stream: Array<Open_Limit_Orders>
  /** fetch data from the table: "open_market_orders" */
  open_market_orders: Array<Open_Market_Orders>
  /** fetch data from the table: "open_market_orders" using primary key columns */
  open_market_orders_by_pk?: Maybe<Open_Market_Orders>
  /** fetch data from the table in a streaming manner: "open_market_orders" */
  open_market_orders_stream: Array<Open_Market_Orders>
  /** fetch data from the table: "per_block_trade_stats" */
  per_block_trade_stats: Array<Per_Block_Trade_Stats>
  /** fetch data from the table: "per_block_trade_stats" using primary key columns */
  per_block_trade_stats_by_pk?: Maybe<Per_Block_Trade_Stats>
  /** fetch data from the table in a streaming manner: "per_block_trade_stats" */
  per_block_trade_stats_stream: Array<Per_Block_Trade_Stats>
  /** fetch data from the table: "pyth_prices" */
  pyth_prices: Array<Pyth_Prices>
  /** fetch data from the table: "pyth_prices_1d" */
  pyth_prices_1d: Array<Pyth_Prices_1d>
  /** fetch data from the table: "pyth_prices_1d" using primary key columns */
  pyth_prices_1d_by_pk?: Maybe<Pyth_Prices_1d>
  /** fetch data from the table in a streaming manner: "pyth_prices_1d" */
  pyth_prices_1d_stream: Array<Pyth_Prices_1d>
  /** fetch data from the table: "pyth_prices_1h" */
  pyth_prices_1h: Array<Pyth_Prices_1h>
  /** fetch data from the table: "pyth_prices_1h" using primary key columns */
  pyth_prices_1h_by_pk?: Maybe<Pyth_Prices_1h>
  /** fetch data from the table in a streaming manner: "pyth_prices_1h" */
  pyth_prices_1h_stream: Array<Pyth_Prices_1h>
  /** fetch data from the table: "pyth_prices_1m" */
  pyth_prices_1m: Array<Pyth_Prices_1m>
  /** fetch data from the table: "pyth_prices_1m" using primary key columns */
  pyth_prices_1m_by_pk?: Maybe<Pyth_Prices_1m>
  /** fetch data from the table in a streaming manner: "pyth_prices_1m" */
  pyth_prices_1m_stream: Array<Pyth_Prices_1m>
  /** fetch data from the table: "pyth_prices_15m" */
  pyth_prices_15m: Array<Pyth_Prices_15m>
  /** fetch data from the table: "pyth_prices_15m" using primary key columns */
  pyth_prices_15m_by_pk?: Maybe<Pyth_Prices_15m>
  /** fetch data from the table in a streaming manner: "pyth_prices_15m" */
  pyth_prices_15m_stream: Array<Pyth_Prices_15m>
  /** fetch data from the table: "pyth_prices" using primary key columns */
  pyth_prices_by_pk?: Maybe<Pyth_Prices>
  /** fetch data from the table in a streaming manner: "pyth_prices" */
  pyth_prices_stream: Array<Pyth_Prices>
  /** fetch data from the table: "share_links" */
  share_links: Array<Share_Links>
  /** fetch data from the table: "share_links" using primary key columns */
  share_links_by_pk?: Maybe<Share_Links>
  /** fetch data from the table in a streaming manner: "share_links" */
  share_links_stream: Array<Share_Links>
  /** fetch data from the table: "testnet_poap_holders" */
  testnet_poap_holders: Array<Testnet_Poap_Holders>
  /** fetch data from the table in a streaming manner: "testnet_poap_holders" */
  testnet_poap_holders_stream: Array<Testnet_Poap_Holders>
  /** fetch data from the table: "trade_bootstrap_stage_1" */
  trade_bootstrap_stage_1: Array<Trade_Bootstrap_Stage_1>
  /** fetch data from the table in a streaming manner: "trade_bootstrap_stage_1" */
  trade_bootstrap_stage_1_stream: Array<Trade_Bootstrap_Stage_1>
  /** fetch data from the table: "trade_bootstrap_stage_2" */
  trade_bootstrap_stage_2: Array<Trade_Bootstrap_Stage_2>
  /** fetch data from the table in a streaming manner: "trade_bootstrap_stage_2" */
  trade_bootstrap_stage_2_stream: Array<Trade_Bootstrap_Stage_2>
  /** fetch data from the table: "traders" */
  traders: Array<Traders>
  /** fetch data from the table in a streaming manner: "traders" */
  traders_stream: Array<Traders>
  /** fetch data from the table: "trading_total" */
  trading_total: Array<Trading_Total>
  /** fetch data from the table in a streaming manner: "trading_total" */
  trading_total_stream: Array<Trading_Total>
  /** fetch data from the table: "uniwhale_v1_bsc.LimitBook_evt_CloseLimitOrderEvent" */
  uniwhale_v1_bsc_LimitBook_evt_CloseLimitOrderEvent: Array<Uniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.LimitBook_evt_CloseLimitOrderEvent" using primary key columns */
  uniwhale_v1_bsc_LimitBook_evt_CloseLimitOrderEvent_by_pk?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEvent>
  /** fetch data from the table in a streaming manner: "uniwhale_v1_bsc.LimitBook_evt_CloseLimitOrderEvent" */
  uniwhale_v1_bsc_LimitBook_evt_CloseLimitOrderEvent_stream: Array<Uniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.LimitBook_evt_ExecuteLimitOrderEvent" */
  uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent: Array<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent>
  /** fetch aggregated fields from the table: "uniwhale_v1_bsc.LimitBook_evt_ExecuteLimitOrderEvent" */
  uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent_aggregate: Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Aggregate
  /** fetch data from the table: "uniwhale_v1_bsc.LimitBook_evt_ExecuteLimitOrderEvent" using primary key columns */
  uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent_by_pk?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent>
  /** fetch data from the table in a streaming manner: "uniwhale_v1_bsc.LimitBook_evt_ExecuteLimitOrderEvent" */
  uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent_stream: Array<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.LimitBook_evt_OpenLimitOrderEvent" */
  uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent: Array<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent>
  /** fetch aggregated fields from the table: "uniwhale_v1_bsc.LimitBook_evt_OpenLimitOrderEvent" */
  uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent_aggregate: Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Aggregate
  /** fetch data from the table: "uniwhale_v1_bsc.LimitBook_evt_OpenLimitOrderEvent" using primary key columns */
  uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent_by_pk?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent>
  /** fetch data from the table in a streaming manner: "uniwhale_v1_bsc.LimitBook_evt_OpenLimitOrderEvent" */
  uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent_stream: Array<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.LimitBook_evt_PartialCloseLimitOrderEvent" */
  uniwhale_v1_bsc_LimitBook_evt_PartialCloseLimitOrderEvent: Array<Uniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.LimitBook_evt_PartialCloseLimitOrderEvent" using primary key columns */
  uniwhale_v1_bsc_LimitBook_evt_PartialCloseLimitOrderEvent_by_pk?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEvent>
  /** fetch data from the table in a streaming manner: "uniwhale_v1_bsc.LimitBook_evt_PartialCloseLimitOrderEvent" */
  uniwhale_v1_bsc_LimitBook_evt_PartialCloseLimitOrderEvent_stream: Array<Uniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.LimitBook_evt_UpdateOpenLimitOrderEvent" */
  uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent: Array<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent>
  /** fetch aggregated fields from the table: "uniwhale_v1_bsc.LimitBook_evt_UpdateOpenLimitOrderEvent" */
  uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent_aggregate: Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Aggregate
  /** fetch data from the table: "uniwhale_v1_bsc.LimitBook_evt_UpdateOpenLimitOrderEvent" using primary key columns */
  uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent_by_pk?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent>
  /** fetch data from the table in a streaming manner: "uniwhale_v1_bsc.LimitBook_evt_UpdateOpenLimitOrderEvent" */
  uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent_stream: Array<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.Referrals_evt_SetDefaultRebatePctEvent" */
  uniwhale_v1_bsc_Referrals_evt_SetDefaultRebatePctEvent: Array<Uniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.Referrals_evt_SetDefaultRebatePctEvent" using primary key columns */
  uniwhale_v1_bsc_Referrals_evt_SetDefaultRebatePctEvent_by_pk?: Maybe<Uniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEvent>
  /** fetch data from the table in a streaming manner: "uniwhale_v1_bsc.Referrals_evt_SetDefaultRebatePctEvent" */
  uniwhale_v1_bsc_Referrals_evt_SetDefaultRebatePctEvent_stream: Array<Uniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.Referrals_evt_SetRebatePctEvent" */
  uniwhale_v1_bsc_Referrals_evt_SetRebatePctEvent: Array<Uniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.Referrals_evt_SetRebatePctEvent" using primary key columns */
  uniwhale_v1_bsc_Referrals_evt_SetRebatePctEvent_by_pk?: Maybe<Uniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEvent>
  /** fetch data from the table in a streaming manner: "uniwhale_v1_bsc.Referrals_evt_SetRebatePctEvent" */
  uniwhale_v1_bsc_Referrals_evt_SetRebatePctEvent_stream: Array<Uniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.TradingCore_evt_CloseMarketOrderEvent" */
  uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent: Array<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent>
  /** fetch aggregated fields from the table: "uniwhale_v1_bsc.TradingCore_evt_CloseMarketOrderEvent" */
  uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent_aggregate: Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Aggregate
  /** fetch data from the table: "uniwhale_v1_bsc.TradingCore_evt_CloseMarketOrderEvent" using primary key columns */
  uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent_by_pk?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent>
  /** fetch data from the table in a streaming manner: "uniwhale_v1_bsc.TradingCore_evt_CloseMarketOrderEvent" */
  uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent_stream: Array<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.TradingCore_evt_OpenMarketOrderEvent" */
  uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent: Array<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent>
  /** fetch aggregated fields from the table: "uniwhale_v1_bsc.TradingCore_evt_OpenMarketOrderEvent" */
  uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent_aggregate: Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Aggregate
  /** fetch data from the table: "uniwhale_v1_bsc.TradingCore_evt_OpenMarketOrderEvent" using primary key columns */
  uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent_by_pk?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent>
  /** fetch data from the table in a streaming manner: "uniwhale_v1_bsc.TradingCore_evt_OpenMarketOrderEvent" */
  uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent_stream: Array<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.TradingCore_evt_UpdateOpenOrderEvent" */
  uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent: Array<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent>
  /** fetch aggregated fields from the table: "uniwhale_v1_bsc.TradingCore_evt_UpdateOpenOrderEvent" */
  uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent_aggregate: Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Aggregate
  /** fetch data from the table: "uniwhale_v1_bsc.TradingCore_evt_UpdateOpenOrderEvent" using primary key columns */
  uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent_by_pk?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent>
  /** fetch data from the table in a streaming manner: "uniwhale_v1_bsc.TradingCore_evt_UpdateOpenOrderEvent" */
  uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent_stream: Array<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent>
  /** fetch data from the table: "uniwhale_v1_bsc.UniwhaleGenesisPass_evt_Transfer" */
  uniwhale_v1_bsc_UniwhaleGenesisPass_evt_Transfer: Array<Uniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_Transfer>
  /** fetch data from the table: "uniwhale_v1_bsc.UniwhaleGenesisPass_evt_Transfer" using primary key columns */
  uniwhale_v1_bsc_UniwhaleGenesisPass_evt_Transfer_by_pk?: Maybe<Uniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_Transfer>
  /** fetch data from the table in a streaming manner: "uniwhale_v1_bsc.UniwhaleGenesisPass_evt_Transfer" */
  uniwhale_v1_bsc_UniwhaleGenesisPass_evt_Transfer_stream: Array<Uniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_Transfer>
  /** fetch data from the table: "uniwhale_v1_bsc.UniwhaleTestnetPOAP_evt_Transfer" */
  uniwhale_v1_bsc_UniwhaleTestnetPOAP_evt_Transfer: Array<Uniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_Transfer>
  /** fetch data from the table: "uniwhale_v1_bsc.UniwhaleTestnetPOAP_evt_Transfer" using primary key columns */
  uniwhale_v1_bsc_UniwhaleTestnetPOAP_evt_Transfer_by_pk?: Maybe<Uniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_Transfer>
  /** fetch data from the table in a streaming manner: "uniwhale_v1_bsc.UniwhaleTestnetPOAP_evt_Transfer" */
  uniwhale_v1_bsc_UniwhaleTestnetPOAP_evt_Transfer_stream: Array<Uniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_Transfer>
}

export type Subscription_RootAd_Ranked_HoldersArgs = {
  distinct_on?: InputMaybe<Array<Ad_Ranked_Holders_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Ad_Ranked_Holders_Order_By>>
  where?: InputMaybe<Ad_Ranked_Holders_Bool_Exp>
}

export type Subscription_RootAd_Ranked_Holders_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Ad_Ranked_Holders_Stream_Cursor_Input>>
  where?: InputMaybe<Ad_Ranked_Holders_Bool_Exp>
}

export type Subscription_RootAd_SummaryArgs = {
  distinct_on?: InputMaybe<Array<Ad_Summary_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Ad_Summary_Order_By>>
  where?: InputMaybe<Ad_Summary_Bool_Exp>
}

export type Subscription_RootAd_Summary_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Ad_Summary_Stream_Cursor_Input>>
  where?: InputMaybe<Ad_Summary_Bool_Exp>
}

export type Subscription_RootBsc_BlocksArgs = {
  distinct_on?: InputMaybe<Array<Bsc_Blocks_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Bsc_Blocks_Order_By>>
  where?: InputMaybe<Bsc_Blocks_Bool_Exp>
}

export type Subscription_RootBsc_Blocks_By_PkArgs = {
  hash: Scalars["bytea"]
}

export type Subscription_RootBsc_Blocks_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Bsc_Blocks_Stream_Cursor_Input>>
  where?: InputMaybe<Bsc_Blocks_Bool_Exp>
}

export type Subscription_RootClosed_Market_OrdersArgs = {
  distinct_on?: InputMaybe<Array<Closed_Market_Orders_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Closed_Market_Orders_Order_By>>
  where?: InputMaybe<Closed_Market_Orders_Bool_Exp>
}

export type Subscription_RootClosed_Market_Orders_By_PkArgs = {
  order_hash: Scalars["bytea"]
}

export type Subscription_RootClosed_Market_Orders_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Closed_Market_Orders_Stream_Cursor_Input>>
  where?: InputMaybe<Closed_Market_Orders_Bool_Exp>
}

export type Subscription_RootCp_Referee_SummaryArgs = {
  distinct_on?: InputMaybe<Array<Cp_Referee_Summary_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Cp_Referee_Summary_Order_By>>
  where?: InputMaybe<Cp_Referee_Summary_Bool_Exp>
}

export type Subscription_RootCp_Referee_Summary_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Cp_Referee_Summary_Stream_Cursor_Input>>
  where?: InputMaybe<Cp_Referee_Summary_Bool_Exp>
}

export type Subscription_RootCp_Referral_CounterArgs = {
  distinct_on?: InputMaybe<Array<Cp_Referral_Counter_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Cp_Referral_Counter_Order_By>>
  where?: InputMaybe<Cp_Referral_Counter_Bool_Exp>
}

export type Subscription_RootCp_Referral_Counter_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Cp_Referral_Counter_Stream_Cursor_Input>>
  where?: InputMaybe<Cp_Referral_Counter_Bool_Exp>
}

export type Subscription_RootCp_Referrer_SummaryArgs = {
  distinct_on?: InputMaybe<Array<Cp_Referrer_Summary_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Cp_Referrer_Summary_Order_By>>
  where?: InputMaybe<Cp_Referrer_Summary_Bool_Exp>
}

export type Subscription_RootCp_Referrer_Summary_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Cp_Referrer_Summary_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Cp_Referrer_Summary_Order_By>>
  where?: InputMaybe<Cp_Referrer_Summary_Bool_Exp>
}

export type Subscription_RootCp_Referrer_Summary_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Cp_Referrer_Summary_Stream_Cursor_Input>>
  where?: InputMaybe<Cp_Referrer_Summary_Bool_Exp>
}

export type Subscription_RootGenesis_Pass_HoldersArgs = {
  distinct_on?: InputMaybe<Array<Genesis_Pass_Holders_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Genesis_Pass_Holders_Order_By>>
  where?: InputMaybe<Genesis_Pass_Holders_Bool_Exp>
}

export type Subscription_RootGenesis_Pass_Holders_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Genesis_Pass_Holders_Stream_Cursor_Input>>
  where?: InputMaybe<Genesis_Pass_Holders_Bool_Exp>
}

export type Subscription_RootLatest_Pyth_PricesArgs = {
  distinct_on?: InputMaybe<Array<Latest_Pyth_Prices_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Latest_Pyth_Prices_Order_By>>
  where?: InputMaybe<Latest_Pyth_Prices_Bool_Exp>
}

export type Subscription_RootLatest_Pyth_Prices_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Latest_Pyth_Prices_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Latest_Pyth_Prices_Order_By>>
  where?: InputMaybe<Latest_Pyth_Prices_Bool_Exp>
}

export type Subscription_RootLatest_Pyth_Prices_By_PkArgs = {
  price_id: Scalars["bytea"]
}

export type Subscription_RootLatest_Pyth_Prices_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Latest_Pyth_Prices_Stream_Cursor_Input>>
  where?: InputMaybe<Latest_Pyth_Prices_Bool_Exp>
}

export type Subscription_RootLatest_Trade_StatsArgs = {
  distinct_on?: InputMaybe<Array<Latest_Trade_Stats_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Latest_Trade_Stats_Order_By>>
  where?: InputMaybe<Latest_Trade_Stats_Bool_Exp>
}

export type Subscription_RootLatest_Trade_Stats_7dArgs = {
  distinct_on?: InputMaybe<Array<Latest_Trade_Stats_7d_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Latest_Trade_Stats_7d_Order_By>>
  where?: InputMaybe<Latest_Trade_Stats_7d_Bool_Exp>
}

export type Subscription_RootLatest_Trade_Stats_7d_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Latest_Trade_Stats_7d_Stream_Cursor_Input>>
  where?: InputMaybe<Latest_Trade_Stats_7d_Bool_Exp>
}

export type Subscription_RootLatest_Trade_Stats_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Latest_Trade_Stats_Stream_Cursor_Input>>
  where?: InputMaybe<Latest_Trade_Stats_Bool_Exp>
}

export type Subscription_RootLiquidity_Bootstrap_Stage_1Args = {
  distinct_on?: InputMaybe<Array<Liquidity_Bootstrap_Stage_1_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Liquidity_Bootstrap_Stage_1_Order_By>>
  where?: InputMaybe<Liquidity_Bootstrap_Stage_1_Bool_Exp>
}

export type Subscription_RootLiquidity_Bootstrap_Stage_1_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Liquidity_Bootstrap_Stage_1_Stream_Cursor_Input>>
  where?: InputMaybe<Liquidity_Bootstrap_Stage_1_Bool_Exp>
}

export type Subscription_RootLiquidity_Bootstrap_Stage_2Args = {
  distinct_on?: InputMaybe<Array<Liquidity_Bootstrap_Stage_2_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Liquidity_Bootstrap_Stage_2_Order_By>>
  where?: InputMaybe<Liquidity_Bootstrap_Stage_2_Bool_Exp>
}

export type Subscription_RootLiquidity_Bootstrap_Stage_2_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Liquidity_Bootstrap_Stage_2_Stream_Cursor_Input>>
  where?: InputMaybe<Liquidity_Bootstrap_Stage_2_Bool_Exp>
}

export type Subscription_RootLiquidity_Bootstrap_Stage_3Args = {
  distinct_on?: InputMaybe<Array<Liquidity_Bootstrap_Stage_3_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Liquidity_Bootstrap_Stage_3_Order_By>>
  where?: InputMaybe<Liquidity_Bootstrap_Stage_3_Bool_Exp>
}

export type Subscription_RootLiquidity_Bootstrap_Stage_3_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Liquidity_Bootstrap_Stage_3_Stream_Cursor_Input>>
  where?: InputMaybe<Liquidity_Bootstrap_Stage_3_Bool_Exp>
}

export type Subscription_RootOpen_Limit_OrdersArgs = {
  distinct_on?: InputMaybe<Array<Open_Limit_Orders_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Open_Limit_Orders_Order_By>>
  where?: InputMaybe<Open_Limit_Orders_Bool_Exp>
}

export type Subscription_RootOpen_Limit_Orders_By_PkArgs = {
  order_hash: Scalars["bytea"]
}

export type Subscription_RootOpen_Limit_Orders_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Open_Limit_Orders_Stream_Cursor_Input>>
  where?: InputMaybe<Open_Limit_Orders_Bool_Exp>
}

export type Subscription_RootOpen_Market_OrdersArgs = {
  distinct_on?: InputMaybe<Array<Open_Market_Orders_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Open_Market_Orders_Order_By>>
  where?: InputMaybe<Open_Market_Orders_Bool_Exp>
}

export type Subscription_RootOpen_Market_Orders_By_PkArgs = {
  order_hash: Scalars["bytea"]
}

export type Subscription_RootOpen_Market_Orders_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Open_Market_Orders_Stream_Cursor_Input>>
  where?: InputMaybe<Open_Market_Orders_Bool_Exp>
}

export type Subscription_RootPer_Block_Trade_StatsArgs = {
  distinct_on?: InputMaybe<Array<Per_Block_Trade_Stats_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Per_Block_Trade_Stats_Order_By>>
  where?: InputMaybe<Per_Block_Trade_Stats_Bool_Exp>
}

export type Subscription_RootPer_Block_Trade_Stats_By_PkArgs = {
  block_number: Scalars["Int"]
}

export type Subscription_RootPer_Block_Trade_Stats_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Per_Block_Trade_Stats_Stream_Cursor_Input>>
  where?: InputMaybe<Per_Block_Trade_Stats_Bool_Exp>
}

export type Subscription_RootPyth_PricesArgs = {
  distinct_on?: InputMaybe<Array<Pyth_Prices_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Pyth_Prices_Order_By>>
  where?: InputMaybe<Pyth_Prices_Bool_Exp>
}

export type Subscription_RootPyth_Prices_1dArgs = {
  distinct_on?: InputMaybe<Array<Pyth_Prices_1d_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Pyth_Prices_1d_Order_By>>
  where?: InputMaybe<Pyth_Prices_1d_Bool_Exp>
}

export type Subscription_RootPyth_Prices_1d_By_PkArgs = {
  aggregated_time: Scalars["timestamptz"]
  price_id: Scalars["bytea"]
}

export type Subscription_RootPyth_Prices_1d_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Pyth_Prices_1d_Stream_Cursor_Input>>
  where?: InputMaybe<Pyth_Prices_1d_Bool_Exp>
}

export type Subscription_RootPyth_Prices_1hArgs = {
  distinct_on?: InputMaybe<Array<Pyth_Prices_1h_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Pyth_Prices_1h_Order_By>>
  where?: InputMaybe<Pyth_Prices_1h_Bool_Exp>
}

export type Subscription_RootPyth_Prices_1h_By_PkArgs = {
  aggregated_time: Scalars["timestamptz"]
  price_id: Scalars["bytea"]
}

export type Subscription_RootPyth_Prices_1h_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Pyth_Prices_1h_Stream_Cursor_Input>>
  where?: InputMaybe<Pyth_Prices_1h_Bool_Exp>
}

export type Subscription_RootPyth_Prices_1mArgs = {
  distinct_on?: InputMaybe<Array<Pyth_Prices_1m_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Pyth_Prices_1m_Order_By>>
  where?: InputMaybe<Pyth_Prices_1m_Bool_Exp>
}

export type Subscription_RootPyth_Prices_1m_By_PkArgs = {
  aggregated_time: Scalars["timestamptz"]
  price_id: Scalars["bytea"]
}

export type Subscription_RootPyth_Prices_1m_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Pyth_Prices_1m_Stream_Cursor_Input>>
  where?: InputMaybe<Pyth_Prices_1m_Bool_Exp>
}

export type Subscription_RootPyth_Prices_15mArgs = {
  distinct_on?: InputMaybe<Array<Pyth_Prices_15m_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Pyth_Prices_15m_Order_By>>
  where?: InputMaybe<Pyth_Prices_15m_Bool_Exp>
}

export type Subscription_RootPyth_Prices_15m_By_PkArgs = {
  aggregated_time: Scalars["timestamptz"]
  price_id: Scalars["bytea"]
}

export type Subscription_RootPyth_Prices_15m_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Pyth_Prices_15m_Stream_Cursor_Input>>
  where?: InputMaybe<Pyth_Prices_15m_Bool_Exp>
}

export type Subscription_RootPyth_Prices_By_PkArgs = {
  id: Scalars["bigint"]
}

export type Subscription_RootPyth_Prices_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Pyth_Prices_Stream_Cursor_Input>>
  where?: InputMaybe<Pyth_Prices_Bool_Exp>
}

export type Subscription_RootShare_LinksArgs = {
  distinct_on?: InputMaybe<Array<Share_Links_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Share_Links_Order_By>>
  where?: InputMaybe<Share_Links_Bool_Exp>
}

export type Subscription_RootShare_Links_By_PkArgs = {
  id: Scalars["String"]
}

export type Subscription_RootShare_Links_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Share_Links_Stream_Cursor_Input>>
  where?: InputMaybe<Share_Links_Bool_Exp>
}

export type Subscription_RootTestnet_Poap_HoldersArgs = {
  distinct_on?: InputMaybe<Array<Testnet_Poap_Holders_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Testnet_Poap_Holders_Order_By>>
  where?: InputMaybe<Testnet_Poap_Holders_Bool_Exp>
}

export type Subscription_RootTestnet_Poap_Holders_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Testnet_Poap_Holders_Stream_Cursor_Input>>
  where?: InputMaybe<Testnet_Poap_Holders_Bool_Exp>
}

export type Subscription_RootTrade_Bootstrap_Stage_1Args = {
  distinct_on?: InputMaybe<Array<Trade_Bootstrap_Stage_1_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Trade_Bootstrap_Stage_1_Order_By>>
  where?: InputMaybe<Trade_Bootstrap_Stage_1_Bool_Exp>
}

export type Subscription_RootTrade_Bootstrap_Stage_1_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Trade_Bootstrap_Stage_1_Stream_Cursor_Input>>
  where?: InputMaybe<Trade_Bootstrap_Stage_1_Bool_Exp>
}

export type Subscription_RootTrade_Bootstrap_Stage_2Args = {
  distinct_on?: InputMaybe<Array<Trade_Bootstrap_Stage_2_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Trade_Bootstrap_Stage_2_Order_By>>
  where?: InputMaybe<Trade_Bootstrap_Stage_2_Bool_Exp>
}

export type Subscription_RootTrade_Bootstrap_Stage_2_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Trade_Bootstrap_Stage_2_Stream_Cursor_Input>>
  where?: InputMaybe<Trade_Bootstrap_Stage_2_Bool_Exp>
}

export type Subscription_RootTradersArgs = {
  distinct_on?: InputMaybe<Array<Traders_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Traders_Order_By>>
  where?: InputMaybe<Traders_Bool_Exp>
}

export type Subscription_RootTraders_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Traders_Stream_Cursor_Input>>
  where?: InputMaybe<Traders_Bool_Exp>
}

export type Subscription_RootTrading_TotalArgs = {
  distinct_on?: InputMaybe<Array<Trading_Total_Select_Column>>
  limit?: InputMaybe<Scalars["Int"]>
  offset?: InputMaybe<Scalars["Int"]>
  order_by?: InputMaybe<Array<Trading_Total_Order_By>>
  where?: InputMaybe<Trading_Total_Bool_Exp>
}

export type Subscription_RootTrading_Total_StreamArgs = {
  batch_size: Scalars["Int"]
  cursor: Array<InputMaybe<Trading_Total_Stream_Cursor_Input>>
  where?: InputMaybe<Trading_Total_Bool_Exp>
}

export type Subscription_RootUniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEventArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEvent_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEvent_By_PkArgs =
  {
    evt_index: Scalars["numeric"]
    evt_tx_hash: Scalars["bytea"]
  }

export type Subscription_RootUniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEvent_StreamArgs =
  {
    batch_size: Scalars["Int"]
    cursor: Array<
      InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEvent_Stream_Cursor_Input>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEvent_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEventArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_AggregateArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_By_PkArgs =
  {
    evt_index: Scalars["numeric"]
    evt_tx_hash: Scalars["bytea"]
  }

export type Subscription_RootUniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_StreamArgs =
  {
    batch_size: Scalars["Int"]
    cursor: Array<
      InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Stream_Cursor_Input>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEventArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_AggregateArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_By_PkArgs =
  {
    evt_index: Scalars["numeric"]
    evt_tx_hash: Scalars["bytea"]
  }

export type Subscription_RootUniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_StreamArgs =
  {
    batch_size: Scalars["Int"]
    cursor: Array<
      InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Stream_Cursor_Input>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEventArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEvent_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEvent_By_PkArgs =
  {
    evt_index: Scalars["numeric"]
    evt_tx_hash: Scalars["bytea"]
  }

export type Subscription_RootUniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEvent_StreamArgs =
  {
    batch_size: Scalars["Int"]
    cursor: Array<
      InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEvent_Stream_Cursor_Input>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEvent_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEventArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_AggregateArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_By_PkArgs =
  {
    evt_index: Scalars["numeric"]
    evt_tx_hash: Scalars["bytea"]
  }

export type Subscription_RootUniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_StreamArgs =
  {
    batch_size: Scalars["Int"]
    cursor: Array<
      InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Stream_Cursor_Input>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEventArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEvent_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEvent_By_PkArgs =
  {
    evt_index: Scalars["numeric"]
    evt_tx_hash: Scalars["bytea"]
  }

export type Subscription_RootUniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEvent_StreamArgs =
  {
    batch_size: Scalars["Int"]
    cursor: Array<
      InputMaybe<Uniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEvent_Stream_Cursor_Input>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEvent_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEventArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEvent_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEvent_By_PkArgs =
  {
    evt_index: Scalars["numeric"]
    evt_tx_hash: Scalars["bytea"]
  }

export type Subscription_RootUniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEvent_StreamArgs =
  {
    batch_size: Scalars["Int"]
    cursor: Array<
      InputMaybe<Uniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEvent_Stream_Cursor_Input>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEvent_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEventArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_AggregateArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_By_PkArgs =
  {
    evt_index: Scalars["numeric"]
    evt_tx_hash: Scalars["bytea"]
  }

export type Subscription_RootUniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_StreamArgs =
  {
    batch_size: Scalars["Int"]
    cursor: Array<
      InputMaybe<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Stream_Cursor_Input>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEventArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_AggregateArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_By_PkArgs =
  {
    evt_index: Scalars["numeric"]
    evt_tx_hash: Scalars["bytea"]
  }

export type Subscription_RootUniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_StreamArgs =
  {
    batch_size: Scalars["Int"]
    cursor: Array<
      InputMaybe<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Stream_Cursor_Input>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEventArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_AggregateArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_By_PkArgs =
  {
    evt_index: Scalars["numeric"]
    evt_tx_hash: Scalars["bytea"]
  }

export type Subscription_RootUniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_StreamArgs =
  {
    batch_size: Scalars["Int"]
    cursor: Array<
      InputMaybe<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Stream_Cursor_Input>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_TransferArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_Transfer_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_Transfer_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_Transfer_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_Transfer_By_PkArgs =
  {
    evt_index: Scalars["numeric"]
    evt_tx_hash: Scalars["bytea"]
  }

export type Subscription_RootUniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_Transfer_StreamArgs =
  {
    batch_size: Scalars["Int"]
    cursor: Array<
      InputMaybe<Uniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_Transfer_Stream_Cursor_Input>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_Transfer_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_TransferArgs =
  {
    distinct_on?: InputMaybe<
      Array<Uniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_Transfer_Select_Column>
    >
    limit?: InputMaybe<Scalars["Int"]>
    offset?: InputMaybe<Scalars["Int"]>
    order_by?: InputMaybe<
      Array<Uniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_Transfer_Order_By>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_Transfer_Bool_Exp>
  }

export type Subscription_RootUniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_Transfer_By_PkArgs =
  {
    evt_index: Scalars["numeric"]
    evt_tx_hash: Scalars["bytea"]
  }

export type Subscription_RootUniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_Transfer_StreamArgs =
  {
    batch_size: Scalars["Int"]
    cursor: Array<
      InputMaybe<Uniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_Transfer_Stream_Cursor_Input>
    >
    where?: InputMaybe<Uniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_Transfer_Bool_Exp>
  }

/** columns and relationships of "testnet_poap_holders" */
export type Testnet_Poap_Holders = {
  __typename?: "testnet_poap_holders"
  contract_address?: Maybe<Scalars["bytea"]>
  evt_block_number?: Maybe<Scalars["bigint"]>
  evt_index?: Maybe<Scalars["numeric"]>
  evt_tx_hash?: Maybe<Scalars["bytea"]>
  evt_tx_index?: Maybe<Scalars["numeric"]>
  from?: Maybe<Scalars["bytea"]>
  to?: Maybe<Scalars["bytea"]>
  tokenId?: Maybe<Scalars["numeric"]>
}

/** Boolean expression to filter rows from the table "testnet_poap_holders". All fields are combined with a logical 'AND'. */
export type Testnet_Poap_Holders_Bool_Exp = {
  _and?: InputMaybe<Array<Testnet_Poap_Holders_Bool_Exp>>
  _not?: InputMaybe<Testnet_Poap_Holders_Bool_Exp>
  _or?: InputMaybe<Array<Testnet_Poap_Holders_Bool_Exp>>
  contract_address?: InputMaybe<Bytea_Comparison_Exp>
  evt_block_number?: InputMaybe<Bigint_Comparison_Exp>
  evt_index?: InputMaybe<Numeric_Comparison_Exp>
  evt_tx_hash?: InputMaybe<Bytea_Comparison_Exp>
  evt_tx_index?: InputMaybe<Numeric_Comparison_Exp>
  from?: InputMaybe<Bytea_Comparison_Exp>
  to?: InputMaybe<Bytea_Comparison_Exp>
  tokenId?: InputMaybe<Numeric_Comparison_Exp>
}

/** Ordering options when selecting data from "testnet_poap_holders". */
export type Testnet_Poap_Holders_Order_By = {
  contract_address?: InputMaybe<Order_By>
  evt_block_number?: InputMaybe<Order_By>
  evt_index?: InputMaybe<Order_By>
  evt_tx_hash?: InputMaybe<Order_By>
  evt_tx_index?: InputMaybe<Order_By>
  from?: InputMaybe<Order_By>
  to?: InputMaybe<Order_By>
  tokenId?: InputMaybe<Order_By>
}

/** select columns of table "testnet_poap_holders" */
export enum Testnet_Poap_Holders_Select_Column {
  /** column name */
  ContractAddress = "contract_address",
  /** column name */
  EvtBlockNumber = "evt_block_number",
  /** column name */
  EvtIndex = "evt_index",
  /** column name */
  EvtTxHash = "evt_tx_hash",
  /** column name */
  EvtTxIndex = "evt_tx_index",
  /** column name */
  From = "from",
  /** column name */
  To = "to",
  /** column name */
  TokenId = "tokenId",
}

/** Streaming cursor of the table "testnet_poap_holders" */
export type Testnet_Poap_Holders_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Testnet_Poap_Holders_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Testnet_Poap_Holders_Stream_Cursor_Value_Input = {
  contract_address?: InputMaybe<Scalars["bytea"]>
  evt_block_number?: InputMaybe<Scalars["bigint"]>
  evt_index?: InputMaybe<Scalars["numeric"]>
  evt_tx_hash?: InputMaybe<Scalars["bytea"]>
  evt_tx_index?: InputMaybe<Scalars["numeric"]>
  from?: InputMaybe<Scalars["bytea"]>
  to?: InputMaybe<Scalars["bytea"]>
  tokenId?: InputMaybe<Scalars["numeric"]>
}

/** Boolean expression to compare columns of type "timestamptz". All fields are combined with logical 'AND'. */
export type Timestamptz_Comparison_Exp = {
  _eq?: InputMaybe<Scalars["timestamptz"]>
  _gt?: InputMaybe<Scalars["timestamptz"]>
  _gte?: InputMaybe<Scalars["timestamptz"]>
  _in?: InputMaybe<Array<Scalars["timestamptz"]>>
  _is_null?: InputMaybe<Scalars["Boolean"]>
  _lt?: InputMaybe<Scalars["timestamptz"]>
  _lte?: InputMaybe<Scalars["timestamptz"]>
  _neq?: InputMaybe<Scalars["timestamptz"]>
  _nin?: InputMaybe<Array<Scalars["timestamptz"]>>
}

/** columns and relationships of "trade_bootstrap_stage_1" */
export type Trade_Bootstrap_Stage_1 = {
  __typename?: "trade_bootstrap_stage_1"
  close_position?: Maybe<Scalars["numeric"]>
  open_position?: Maybe<Scalars["numeric"]>
  total_trade_volume?: Maybe<Scalars["numeric"]>
  trade_user?: Maybe<Scalars["bytea"]>
}

/** Boolean expression to filter rows from the table "trade_bootstrap_stage_1". All fields are combined with a logical 'AND'. */
export type Trade_Bootstrap_Stage_1_Bool_Exp = {
  _and?: InputMaybe<Array<Trade_Bootstrap_Stage_1_Bool_Exp>>
  _not?: InputMaybe<Trade_Bootstrap_Stage_1_Bool_Exp>
  _or?: InputMaybe<Array<Trade_Bootstrap_Stage_1_Bool_Exp>>
  close_position?: InputMaybe<Numeric_Comparison_Exp>
  open_position?: InputMaybe<Numeric_Comparison_Exp>
  total_trade_volume?: InputMaybe<Numeric_Comparison_Exp>
  trade_user?: InputMaybe<Bytea_Comparison_Exp>
}

/** Ordering options when selecting data from "trade_bootstrap_stage_1". */
export type Trade_Bootstrap_Stage_1_Order_By = {
  close_position?: InputMaybe<Order_By>
  open_position?: InputMaybe<Order_By>
  total_trade_volume?: InputMaybe<Order_By>
  trade_user?: InputMaybe<Order_By>
}

/** select columns of table "trade_bootstrap_stage_1" */
export enum Trade_Bootstrap_Stage_1_Select_Column {
  /** column name */
  ClosePosition = "close_position",
  /** column name */
  OpenPosition = "open_position",
  /** column name */
  TotalTradeVolume = "total_trade_volume",
  /** column name */
  TradeUser = "trade_user",
}

/** Streaming cursor of the table "trade_bootstrap_stage_1" */
export type Trade_Bootstrap_Stage_1_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Trade_Bootstrap_Stage_1_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Trade_Bootstrap_Stage_1_Stream_Cursor_Value_Input = {
  close_position?: InputMaybe<Scalars["numeric"]>
  open_position?: InputMaybe<Scalars["numeric"]>
  total_trade_volume?: InputMaybe<Scalars["numeric"]>
  trade_user?: InputMaybe<Scalars["bytea"]>
}

/** columns and relationships of "trade_bootstrap_stage_2" */
export type Trade_Bootstrap_Stage_2 = {
  __typename?: "trade_bootstrap_stage_2"
  close_position?: Maybe<Scalars["numeric"]>
  open_position?: Maybe<Scalars["numeric"]>
  total_trade_volume?: Maybe<Scalars["numeric"]>
  trade_user?: Maybe<Scalars["bytea"]>
}

/** Boolean expression to filter rows from the table "trade_bootstrap_stage_2". All fields are combined with a logical 'AND'. */
export type Trade_Bootstrap_Stage_2_Bool_Exp = {
  _and?: InputMaybe<Array<Trade_Bootstrap_Stage_2_Bool_Exp>>
  _not?: InputMaybe<Trade_Bootstrap_Stage_2_Bool_Exp>
  _or?: InputMaybe<Array<Trade_Bootstrap_Stage_2_Bool_Exp>>
  close_position?: InputMaybe<Numeric_Comparison_Exp>
  open_position?: InputMaybe<Numeric_Comparison_Exp>
  total_trade_volume?: InputMaybe<Numeric_Comparison_Exp>
  trade_user?: InputMaybe<Bytea_Comparison_Exp>
}

/** Ordering options when selecting data from "trade_bootstrap_stage_2". */
export type Trade_Bootstrap_Stage_2_Order_By = {
  close_position?: InputMaybe<Order_By>
  open_position?: InputMaybe<Order_By>
  total_trade_volume?: InputMaybe<Order_By>
  trade_user?: InputMaybe<Order_By>
}

/** select columns of table "trade_bootstrap_stage_2" */
export enum Trade_Bootstrap_Stage_2_Select_Column {
  /** column name */
  ClosePosition = "close_position",
  /** column name */
  OpenPosition = "open_position",
  /** column name */
  TotalTradeVolume = "total_trade_volume",
  /** column name */
  TradeUser = "trade_user",
}

/** Streaming cursor of the table "trade_bootstrap_stage_2" */
export type Trade_Bootstrap_Stage_2_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Trade_Bootstrap_Stage_2_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Trade_Bootstrap_Stage_2_Stream_Cursor_Value_Input = {
  close_position?: InputMaybe<Scalars["numeric"]>
  open_position?: InputMaybe<Scalars["numeric"]>
  total_trade_volume?: InputMaybe<Scalars["numeric"]>
  trade_user?: InputMaybe<Scalars["bytea"]>
}

/** columns and relationships of "traders" */
export type Traders = {
  __typename?: "traders"
  first_trade_block?: Maybe<Scalars["bigint"]>
  trade_user?: Maybe<Scalars["bytea"]>
}

/** Boolean expression to filter rows from the table "traders". All fields are combined with a logical 'AND'. */
export type Traders_Bool_Exp = {
  _and?: InputMaybe<Array<Traders_Bool_Exp>>
  _not?: InputMaybe<Traders_Bool_Exp>
  _or?: InputMaybe<Array<Traders_Bool_Exp>>
  first_trade_block?: InputMaybe<Bigint_Comparison_Exp>
  trade_user?: InputMaybe<Bytea_Comparison_Exp>
}

/** Ordering options when selecting data from "traders". */
export type Traders_Order_By = {
  first_trade_block?: InputMaybe<Order_By>
  trade_user?: InputMaybe<Order_By>
}

/** select columns of table "traders" */
export enum Traders_Select_Column {
  /** column name */
  FirstTradeBlock = "first_trade_block",
  /** column name */
  TradeUser = "trade_user",
}

/** Streaming cursor of the table "traders" */
export type Traders_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Traders_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Traders_Stream_Cursor_Value_Input = {
  first_trade_block?: InputMaybe<Scalars["bigint"]>
  trade_user?: InputMaybe<Scalars["bytea"]>
}

/** columns and relationships of "trading_total" */
export type Trading_Total = {
  __typename?: "trading_total"
  total_close_position?: Maybe<Scalars["numeric"]>
  total_open_position?: Maybe<Scalars["numeric"]>
  total_trade_volume?: Maybe<Scalars["numeric"]>
}

/** Boolean expression to filter rows from the table "trading_total". All fields are combined with a logical 'AND'. */
export type Trading_Total_Bool_Exp = {
  _and?: InputMaybe<Array<Trading_Total_Bool_Exp>>
  _not?: InputMaybe<Trading_Total_Bool_Exp>
  _or?: InputMaybe<Array<Trading_Total_Bool_Exp>>
  total_close_position?: InputMaybe<Numeric_Comparison_Exp>
  total_open_position?: InputMaybe<Numeric_Comparison_Exp>
  total_trade_volume?: InputMaybe<Numeric_Comparison_Exp>
}

/** Ordering options when selecting data from "trading_total". */
export type Trading_Total_Order_By = {
  total_close_position?: InputMaybe<Order_By>
  total_open_position?: InputMaybe<Order_By>
  total_trade_volume?: InputMaybe<Order_By>
}

/** select columns of table "trading_total" */
export enum Trading_Total_Select_Column {
  /** column name */
  TotalClosePosition = "total_close_position",
  /** column name */
  TotalOpenPosition = "total_open_position",
  /** column name */
  TotalTradeVolume = "total_trade_volume",
}

/** Streaming cursor of the table "trading_total" */
export type Trading_Total_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Trading_Total_Stream_Cursor_Value_Input
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>
}

/** Initial value of the column from where the streaming should start */
export type Trading_Total_Stream_Cursor_Value_Input = {
  total_close_position?: InputMaybe<Scalars["numeric"]>
  total_open_position?: InputMaybe<Scalars["numeric"]>
  total_trade_volume?: InputMaybe<Scalars["numeric"]>
}

/** columns and relationships of "uniwhale_v1_bsc.LimitBook_evt_CloseLimitOrderEvent" */
export type Uniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEvent = {
  __typename?: "uniwhale_v1_bsc_LimitBook_evt_CloseLimitOrderEvent"
  contract_address: Scalars["bytea"]
  evt_block_number: Scalars["bigint"]
  evt_block_time: Scalars["timestamptz"]
  evt_index: Scalars["numeric"]
  evt_tx_hash: Scalars["bytea"]
  evt_tx_index: Scalars["numeric"]
  orderHash: Scalars["bytea"]
  sender: Scalars["bytea"]
  trade_priceId: Scalars["bytea"]
  trade_user: Scalars["bytea"]
}

/** Boolean expression to filter rows from the table "uniwhale_v1_bsc.LimitBook_evt_CloseLimitOrderEvent". All fields are combined with a logical 'AND'. */
export type Uniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEvent_Bool_Exp = {
  _and?: InputMaybe<
    Array<Uniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEvent_Bool_Exp>
  >
  _not?: InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEvent_Bool_Exp>
  _or?: InputMaybe<
    Array<Uniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEvent_Bool_Exp>
  >
  contract_address?: InputMaybe<Bytea_Comparison_Exp>
  evt_block_number?: InputMaybe<Bigint_Comparison_Exp>
  evt_block_time?: InputMaybe<Timestamptz_Comparison_Exp>
  evt_index?: InputMaybe<Numeric_Comparison_Exp>
  evt_tx_hash?: InputMaybe<Bytea_Comparison_Exp>
  evt_tx_index?: InputMaybe<Numeric_Comparison_Exp>
  orderHash?: InputMaybe<Bytea_Comparison_Exp>
  sender?: InputMaybe<Bytea_Comparison_Exp>
  trade_priceId?: InputMaybe<Bytea_Comparison_Exp>
  trade_user?: InputMaybe<Bytea_Comparison_Exp>
}

/** Ordering options when selecting data from "uniwhale_v1_bsc.LimitBook_evt_CloseLimitOrderEvent". */
export type Uniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEvent_Order_By = {
  contract_address?: InputMaybe<Order_By>
  evt_block_number?: InputMaybe<Order_By>
  evt_block_time?: InputMaybe<Order_By>
  evt_index?: InputMaybe<Order_By>
  evt_tx_hash?: InputMaybe<Order_By>
  evt_tx_index?: InputMaybe<Order_By>
  orderHash?: InputMaybe<Order_By>
  sender?: InputMaybe<Order_By>
  trade_priceId?: InputMaybe<Order_By>
  trade_user?: InputMaybe<Order_By>
}

/** select columns of table "uniwhale_v1_bsc.LimitBook_evt_CloseLimitOrderEvent" */
export enum Uniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEvent_Select_Column {
  /** column name */
  ContractAddress = "contract_address",
  /** column name */
  EvtBlockNumber = "evt_block_number",
  /** column name */
  EvtBlockTime = "evt_block_time",
  /** column name */
  EvtIndex = "evt_index",
  /** column name */
  EvtTxHash = "evt_tx_hash",
  /** column name */
  EvtTxIndex = "evt_tx_index",
  /** column name */
  OrderHash = "orderHash",
  /** column name */
  Sender = "sender",
  /** column name */
  TradePriceId = "trade_priceId",
  /** column name */
  TradeUser = "trade_user",
}

/** Streaming cursor of the table "uniwhale_v1_bsc_LimitBook_evt_CloseLimitOrderEvent" */
export type Uniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEvent_Stream_Cursor_Input =
  {
    /** Stream column input with initial value */
    initial_value: Uniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEvent_Stream_Cursor_Value_Input
    /** cursor ordering */
    ordering?: InputMaybe<Cursor_Ordering>
  }

/** Initial value of the column from where the streaming should start */
export type Uniwhale_V1_Bsc_LimitBook_Evt_CloseLimitOrderEvent_Stream_Cursor_Value_Input =
  {
    contract_address?: InputMaybe<Scalars["bytea"]>
    evt_block_number?: InputMaybe<Scalars["bigint"]>
    evt_block_time?: InputMaybe<Scalars["timestamptz"]>
    evt_index?: InputMaybe<Scalars["numeric"]>
    evt_tx_hash?: InputMaybe<Scalars["bytea"]>
    evt_tx_index?: InputMaybe<Scalars["numeric"]>
    orderHash?: InputMaybe<Scalars["bytea"]>
    sender?: InputMaybe<Scalars["bytea"]>
    trade_priceId?: InputMaybe<Scalars["bytea"]>
    trade_user?: InputMaybe<Scalars["bytea"]>
  }

/** columns and relationships of "uniwhale_v1_bsc.LimitBook_evt_ExecuteLimitOrderEvent" */
export type Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent = {
  __typename?: "uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent"
  contract_address: Scalars["bytea"]
  evt_block_number: Scalars["bigint"]
  evt_block_time: Scalars["timestamptz"]
  evt_index: Scalars["numeric"]
  evt_tx_hash: Scalars["bytea"]
  evt_tx_index: Scalars["numeric"]
  orderHash: Scalars["bytea"]
  sender: Scalars["bytea"]
  trade_priceId: Scalars["bytea"]
  trade_user: Scalars["bytea"]
}

/** aggregated selection of "uniwhale_v1_bsc.LimitBook_evt_ExecuteLimitOrderEvent" */
export type Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Aggregate = {
  __typename?: "uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent_aggregate"
  aggregate?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Aggregate_Fields>
  nodes: Array<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent>
}

/** aggregate fields of "uniwhale_v1_bsc.LimitBook_evt_ExecuteLimitOrderEvent" */
export type Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Aggregate_Fields =
  {
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent_aggregate_fields"
    avg?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Avg_Fields>
    count: Scalars["Int"]
    max?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Max_Fields>
    min?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Min_Fields>
    stddev?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Stddev_Fields>
    stddev_pop?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Stddev_Pop_Fields>
    stddev_samp?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Stddev_Samp_Fields>
    sum?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Sum_Fields>
    var_pop?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Var_Pop_Fields>
    var_samp?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Var_Samp_Fields>
    variance?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Variance_Fields>
  }

/** aggregate fields of "uniwhale_v1_bsc.LimitBook_evt_ExecuteLimitOrderEvent" */
export type Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Aggregate_FieldsCountArgs =
  {
    columns?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Select_Column>
    >
    distinct?: InputMaybe<Scalars["Boolean"]>
  }

/** aggregate avg on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Avg_Fields = {
  __typename?: "uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent_avg_fields"
  evt_block_number?: Maybe<Scalars["Float"]>
  evt_index?: Maybe<Scalars["Float"]>
  evt_tx_index?: Maybe<Scalars["Float"]>
}

/** Boolean expression to filter rows from the table "uniwhale_v1_bsc.LimitBook_evt_ExecuteLimitOrderEvent". All fields are combined with a logical 'AND'. */
export type Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Bool_Exp = {
  _and?: InputMaybe<
    Array<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Bool_Exp>
  >
  _not?: InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Bool_Exp>
  _or?: InputMaybe<
    Array<Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Bool_Exp>
  >
  contract_address?: InputMaybe<Bytea_Comparison_Exp>
  evt_block_number?: InputMaybe<Bigint_Comparison_Exp>
  evt_block_time?: InputMaybe<Timestamptz_Comparison_Exp>
  evt_index?: InputMaybe<Numeric_Comparison_Exp>
  evt_tx_hash?: InputMaybe<Bytea_Comparison_Exp>
  evt_tx_index?: InputMaybe<Numeric_Comparison_Exp>
  orderHash?: InputMaybe<Bytea_Comparison_Exp>
  sender?: InputMaybe<Bytea_Comparison_Exp>
  trade_priceId?: InputMaybe<Bytea_Comparison_Exp>
  trade_user?: InputMaybe<Bytea_Comparison_Exp>
}

/** aggregate max on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Max_Fields = {
  __typename?: "uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent_max_fields"
  evt_block_number?: Maybe<Scalars["bigint"]>
  evt_block_time?: Maybe<Scalars["timestamptz"]>
  evt_index?: Maybe<Scalars["numeric"]>
  evt_tx_index?: Maybe<Scalars["numeric"]>
}

/** aggregate min on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Min_Fields = {
  __typename?: "uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent_min_fields"
  evt_block_number?: Maybe<Scalars["bigint"]>
  evt_block_time?: Maybe<Scalars["timestamptz"]>
  evt_index?: Maybe<Scalars["numeric"]>
  evt_tx_index?: Maybe<Scalars["numeric"]>
}

/** Ordering options when selecting data from "uniwhale_v1_bsc.LimitBook_evt_ExecuteLimitOrderEvent". */
export type Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Order_By = {
  contract_address?: InputMaybe<Order_By>
  evt_block_number?: InputMaybe<Order_By>
  evt_block_time?: InputMaybe<Order_By>
  evt_index?: InputMaybe<Order_By>
  evt_tx_hash?: InputMaybe<Order_By>
  evt_tx_index?: InputMaybe<Order_By>
  orderHash?: InputMaybe<Order_By>
  sender?: InputMaybe<Order_By>
  trade_priceId?: InputMaybe<Order_By>
  trade_user?: InputMaybe<Order_By>
}

/** select columns of table "uniwhale_v1_bsc.LimitBook_evt_ExecuteLimitOrderEvent" */
export enum Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Select_Column {
  /** column name */
  ContractAddress = "contract_address",
  /** column name */
  EvtBlockNumber = "evt_block_number",
  /** column name */
  EvtBlockTime = "evt_block_time",
  /** column name */
  EvtIndex = "evt_index",
  /** column name */
  EvtTxHash = "evt_tx_hash",
  /** column name */
  EvtTxIndex = "evt_tx_index",
  /** column name */
  OrderHash = "orderHash",
  /** column name */
  Sender = "sender",
  /** column name */
  TradePriceId = "trade_priceId",
  /** column name */
  TradeUser = "trade_user",
}

/** aggregate stddev on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Stddev_Fields =
  {
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent_stddev_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
  }

/** aggregate stddev_pop on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Stddev_Pop_Fields =
  {
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent_stddev_pop_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
  }

/** aggregate stddev_samp on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Stddev_Samp_Fields =
  {
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent_stddev_samp_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
  }

/** Streaming cursor of the table "uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent" */
export type Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Stream_Cursor_Input =
  {
    /** Stream column input with initial value */
    initial_value: Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Stream_Cursor_Value_Input
    /** cursor ordering */
    ordering?: InputMaybe<Cursor_Ordering>
  }

/** Initial value of the column from where the streaming should start */
export type Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Stream_Cursor_Value_Input =
  {
    contract_address?: InputMaybe<Scalars["bytea"]>
    evt_block_number?: InputMaybe<Scalars["bigint"]>
    evt_block_time?: InputMaybe<Scalars["timestamptz"]>
    evt_index?: InputMaybe<Scalars["numeric"]>
    evt_tx_hash?: InputMaybe<Scalars["bytea"]>
    evt_tx_index?: InputMaybe<Scalars["numeric"]>
    orderHash?: InputMaybe<Scalars["bytea"]>
    sender?: InputMaybe<Scalars["bytea"]>
    trade_priceId?: InputMaybe<Scalars["bytea"]>
    trade_user?: InputMaybe<Scalars["bytea"]>
  }

/** aggregate sum on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Sum_Fields = {
  __typename?: "uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent_sum_fields"
  evt_block_number?: Maybe<Scalars["bigint"]>
  evt_index?: Maybe<Scalars["numeric"]>
  evt_tx_index?: Maybe<Scalars["numeric"]>
}

/** aggregate var_pop on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Var_Pop_Fields =
  {
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent_var_pop_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
  }

/** aggregate var_samp on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Var_Samp_Fields =
  {
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent_var_samp_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
  }

/** aggregate variance on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_ExecuteLimitOrderEvent_Variance_Fields =
  {
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent_variance_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
  }

/** columns and relationships of "uniwhale_v1_bsc.LimitBook_evt_OpenLimitOrderEvent" */
export type Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent = {
  __typename?: "uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent"
  contract_address: Scalars["bytea"]
  evt_block_number: Scalars["bigint"]
  evt_block_time: Scalars["timestamptz"]
  evt_index: Scalars["numeric"]
  evt_tx_hash: Scalars["bytea"]
  evt_tx_index: Scalars["numeric"]
  orderHash: Scalars["bytea"]
  sender: Scalars["bytea"]
  trade_executionBlock: Scalars["numeric"]
  trade_executionTime: Scalars["numeric"]
  trade_isBuy: Scalars["Boolean"]
  trade_leverage: Scalars["numeric"]
  trade_liquidationPrice: Scalars["numeric"]
  trade_margin: Scalars["numeric"]
  trade_maxPercentagePnL: Scalars["numeric"]
  trade_openPrice: Scalars["numeric"]
  trade_priceId: Scalars["bytea"]
  trade_profitTarget: Scalars["numeric"]
  trade_salt: Scalars["numeric"]
  trade_slippage: Scalars["numeric"]
  trade_stopLoss: Scalars["numeric"]
  trade_user: Scalars["bytea"]
}

/** aggregated selection of "uniwhale_v1_bsc.LimitBook_evt_OpenLimitOrderEvent" */
export type Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Aggregate = {
  __typename?: "uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent_aggregate"
  aggregate?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Aggregate_Fields>
  nodes: Array<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent>
}

/** aggregate fields of "uniwhale_v1_bsc.LimitBook_evt_OpenLimitOrderEvent" */
export type Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Aggregate_Fields =
  {
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent_aggregate_fields"
    avg?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Avg_Fields>
    count: Scalars["Int"]
    max?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Max_Fields>
    min?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Min_Fields>
    stddev?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Stddev_Fields>
    stddev_pop?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Stddev_Pop_Fields>
    stddev_samp?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Stddev_Samp_Fields>
    sum?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Sum_Fields>
    var_pop?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Var_Pop_Fields>
    var_samp?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Var_Samp_Fields>
    variance?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Variance_Fields>
  }

/** aggregate fields of "uniwhale_v1_bsc.LimitBook_evt_OpenLimitOrderEvent" */
export type Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Aggregate_FieldsCountArgs =
  {
    columns?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Select_Column>
    >
    distinct?: InputMaybe<Scalars["Boolean"]>
  }

/** aggregate avg on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Avg_Fields = {
  __typename?: "uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent_avg_fields"
  evt_block_number?: Maybe<Scalars["Float"]>
  evt_index?: Maybe<Scalars["Float"]>
  evt_tx_index?: Maybe<Scalars["Float"]>
  trade_executionBlock?: Maybe<Scalars["Float"]>
  trade_executionTime?: Maybe<Scalars["Float"]>
  trade_leverage?: Maybe<Scalars["Float"]>
  trade_liquidationPrice?: Maybe<Scalars["Float"]>
  trade_margin?: Maybe<Scalars["Float"]>
  trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
  trade_openPrice?: Maybe<Scalars["Float"]>
  trade_profitTarget?: Maybe<Scalars["Float"]>
  trade_salt?: Maybe<Scalars["Float"]>
  trade_slippage?: Maybe<Scalars["Float"]>
  trade_stopLoss?: Maybe<Scalars["Float"]>
}

/** Boolean expression to filter rows from the table "uniwhale_v1_bsc.LimitBook_evt_OpenLimitOrderEvent". All fields are combined with a logical 'AND'. */
export type Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Bool_Exp = {
  _and?: InputMaybe<
    Array<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Bool_Exp>
  >
  _not?: InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Bool_Exp>
  _or?: InputMaybe<
    Array<Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Bool_Exp>
  >
  contract_address?: InputMaybe<Bytea_Comparison_Exp>
  evt_block_number?: InputMaybe<Bigint_Comparison_Exp>
  evt_block_time?: InputMaybe<Timestamptz_Comparison_Exp>
  evt_index?: InputMaybe<Numeric_Comparison_Exp>
  evt_tx_hash?: InputMaybe<Bytea_Comparison_Exp>
  evt_tx_index?: InputMaybe<Numeric_Comparison_Exp>
  orderHash?: InputMaybe<Bytea_Comparison_Exp>
  sender?: InputMaybe<Bytea_Comparison_Exp>
  trade_executionBlock?: InputMaybe<Numeric_Comparison_Exp>
  trade_executionTime?: InputMaybe<Numeric_Comparison_Exp>
  trade_isBuy?: InputMaybe<Boolean_Comparison_Exp>
  trade_leverage?: InputMaybe<Numeric_Comparison_Exp>
  trade_liquidationPrice?: InputMaybe<Numeric_Comparison_Exp>
  trade_margin?: InputMaybe<Numeric_Comparison_Exp>
  trade_maxPercentagePnL?: InputMaybe<Numeric_Comparison_Exp>
  trade_openPrice?: InputMaybe<Numeric_Comparison_Exp>
  trade_priceId?: InputMaybe<Bytea_Comparison_Exp>
  trade_profitTarget?: InputMaybe<Numeric_Comparison_Exp>
  trade_salt?: InputMaybe<Numeric_Comparison_Exp>
  trade_slippage?: InputMaybe<Numeric_Comparison_Exp>
  trade_stopLoss?: InputMaybe<Numeric_Comparison_Exp>
  trade_user?: InputMaybe<Bytea_Comparison_Exp>
}

/** aggregate max on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Max_Fields = {
  __typename?: "uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent_max_fields"
  evt_block_number?: Maybe<Scalars["bigint"]>
  evt_block_time?: Maybe<Scalars["timestamptz"]>
  evt_index?: Maybe<Scalars["numeric"]>
  evt_tx_index?: Maybe<Scalars["numeric"]>
  trade_executionBlock?: Maybe<Scalars["numeric"]>
  trade_executionTime?: Maybe<Scalars["numeric"]>
  trade_leverage?: Maybe<Scalars["numeric"]>
  trade_liquidationPrice?: Maybe<Scalars["numeric"]>
  trade_margin?: Maybe<Scalars["numeric"]>
  trade_maxPercentagePnL?: Maybe<Scalars["numeric"]>
  trade_openPrice?: Maybe<Scalars["numeric"]>
  trade_profitTarget?: Maybe<Scalars["numeric"]>
  trade_salt?: Maybe<Scalars["numeric"]>
  trade_slippage?: Maybe<Scalars["numeric"]>
  trade_stopLoss?: Maybe<Scalars["numeric"]>
}

/** aggregate min on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Min_Fields = {
  __typename?: "uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent_min_fields"
  evt_block_number?: Maybe<Scalars["bigint"]>
  evt_block_time?: Maybe<Scalars["timestamptz"]>
  evt_index?: Maybe<Scalars["numeric"]>
  evt_tx_index?: Maybe<Scalars["numeric"]>
  trade_executionBlock?: Maybe<Scalars["numeric"]>
  trade_executionTime?: Maybe<Scalars["numeric"]>
  trade_leverage?: Maybe<Scalars["numeric"]>
  trade_liquidationPrice?: Maybe<Scalars["numeric"]>
  trade_margin?: Maybe<Scalars["numeric"]>
  trade_maxPercentagePnL?: Maybe<Scalars["numeric"]>
  trade_openPrice?: Maybe<Scalars["numeric"]>
  trade_profitTarget?: Maybe<Scalars["numeric"]>
  trade_salt?: Maybe<Scalars["numeric"]>
  trade_slippage?: Maybe<Scalars["numeric"]>
  trade_stopLoss?: Maybe<Scalars["numeric"]>
}

/** Ordering options when selecting data from "uniwhale_v1_bsc.LimitBook_evt_OpenLimitOrderEvent". */
export type Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Order_By = {
  contract_address?: InputMaybe<Order_By>
  evt_block_number?: InputMaybe<Order_By>
  evt_block_time?: InputMaybe<Order_By>
  evt_index?: InputMaybe<Order_By>
  evt_tx_hash?: InputMaybe<Order_By>
  evt_tx_index?: InputMaybe<Order_By>
  orderHash?: InputMaybe<Order_By>
  sender?: InputMaybe<Order_By>
  trade_executionBlock?: InputMaybe<Order_By>
  trade_executionTime?: InputMaybe<Order_By>
  trade_isBuy?: InputMaybe<Order_By>
  trade_leverage?: InputMaybe<Order_By>
  trade_liquidationPrice?: InputMaybe<Order_By>
  trade_margin?: InputMaybe<Order_By>
  trade_maxPercentagePnL?: InputMaybe<Order_By>
  trade_openPrice?: InputMaybe<Order_By>
  trade_priceId?: InputMaybe<Order_By>
  trade_profitTarget?: InputMaybe<Order_By>
  trade_salt?: InputMaybe<Order_By>
  trade_slippage?: InputMaybe<Order_By>
  trade_stopLoss?: InputMaybe<Order_By>
  trade_user?: InputMaybe<Order_By>
}

/** select columns of table "uniwhale_v1_bsc.LimitBook_evt_OpenLimitOrderEvent" */
export enum Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Select_Column {
  /** column name */
  ContractAddress = "contract_address",
  /** column name */
  EvtBlockNumber = "evt_block_number",
  /** column name */
  EvtBlockTime = "evt_block_time",
  /** column name */
  EvtIndex = "evt_index",
  /** column name */
  EvtTxHash = "evt_tx_hash",
  /** column name */
  EvtTxIndex = "evt_tx_index",
  /** column name */
  OrderHash = "orderHash",
  /** column name */
  Sender = "sender",
  /** column name */
  TradeExecutionBlock = "trade_executionBlock",
  /** column name */
  TradeExecutionTime = "trade_executionTime",
  /** column name */
  TradeIsBuy = "trade_isBuy",
  /** column name */
  TradeLeverage = "trade_leverage",
  /** column name */
  TradeLiquidationPrice = "trade_liquidationPrice",
  /** column name */
  TradeMargin = "trade_margin",
  /** column name */
  TradeMaxPercentagePnL = "trade_maxPercentagePnL",
  /** column name */
  TradeOpenPrice = "trade_openPrice",
  /** column name */
  TradePriceId = "trade_priceId",
  /** column name */
  TradeProfitTarget = "trade_profitTarget",
  /** column name */
  TradeSalt = "trade_salt",
  /** column name */
  TradeSlippage = "trade_slippage",
  /** column name */
  TradeStopLoss = "trade_stopLoss",
  /** column name */
  TradeUser = "trade_user",
}

/** aggregate stddev on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Stddev_Fields = {
  __typename?: "uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent_stddev_fields"
  evt_block_number?: Maybe<Scalars["Float"]>
  evt_index?: Maybe<Scalars["Float"]>
  evt_tx_index?: Maybe<Scalars["Float"]>
  trade_executionBlock?: Maybe<Scalars["Float"]>
  trade_executionTime?: Maybe<Scalars["Float"]>
  trade_leverage?: Maybe<Scalars["Float"]>
  trade_liquidationPrice?: Maybe<Scalars["Float"]>
  trade_margin?: Maybe<Scalars["Float"]>
  trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
  trade_openPrice?: Maybe<Scalars["Float"]>
  trade_profitTarget?: Maybe<Scalars["Float"]>
  trade_salt?: Maybe<Scalars["Float"]>
  trade_slippage?: Maybe<Scalars["Float"]>
  trade_stopLoss?: Maybe<Scalars["Float"]>
}

/** aggregate stddev_pop on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Stddev_Pop_Fields =
  {
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent_stddev_pop_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** aggregate stddev_samp on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Stddev_Samp_Fields =
  {
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent_stddev_samp_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** Streaming cursor of the table "uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent" */
export type Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Stream_Cursor_Input =
  {
    /** Stream column input with initial value */
    initial_value: Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Stream_Cursor_Value_Input
    /** cursor ordering */
    ordering?: InputMaybe<Cursor_Ordering>
  }

/** Initial value of the column from where the streaming should start */
export type Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Stream_Cursor_Value_Input =
  {
    contract_address?: InputMaybe<Scalars["bytea"]>
    evt_block_number?: InputMaybe<Scalars["bigint"]>
    evt_block_time?: InputMaybe<Scalars["timestamptz"]>
    evt_index?: InputMaybe<Scalars["numeric"]>
    evt_tx_hash?: InputMaybe<Scalars["bytea"]>
    evt_tx_index?: InputMaybe<Scalars["numeric"]>
    orderHash?: InputMaybe<Scalars["bytea"]>
    sender?: InputMaybe<Scalars["bytea"]>
    trade_executionBlock?: InputMaybe<Scalars["numeric"]>
    trade_executionTime?: InputMaybe<Scalars["numeric"]>
    trade_isBuy?: InputMaybe<Scalars["Boolean"]>
    trade_leverage?: InputMaybe<Scalars["numeric"]>
    trade_liquidationPrice?: InputMaybe<Scalars["numeric"]>
    trade_margin?: InputMaybe<Scalars["numeric"]>
    trade_maxPercentagePnL?: InputMaybe<Scalars["numeric"]>
    trade_openPrice?: InputMaybe<Scalars["numeric"]>
    trade_priceId?: InputMaybe<Scalars["bytea"]>
    trade_profitTarget?: InputMaybe<Scalars["numeric"]>
    trade_salt?: InputMaybe<Scalars["numeric"]>
    trade_slippage?: InputMaybe<Scalars["numeric"]>
    trade_stopLoss?: InputMaybe<Scalars["numeric"]>
    trade_user?: InputMaybe<Scalars["bytea"]>
  }

/** aggregate sum on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Sum_Fields = {
  __typename?: "uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent_sum_fields"
  evt_block_number?: Maybe<Scalars["bigint"]>
  evt_index?: Maybe<Scalars["numeric"]>
  evt_tx_index?: Maybe<Scalars["numeric"]>
  trade_executionBlock?: Maybe<Scalars["numeric"]>
  trade_executionTime?: Maybe<Scalars["numeric"]>
  trade_leverage?: Maybe<Scalars["numeric"]>
  trade_liquidationPrice?: Maybe<Scalars["numeric"]>
  trade_margin?: Maybe<Scalars["numeric"]>
  trade_maxPercentagePnL?: Maybe<Scalars["numeric"]>
  trade_openPrice?: Maybe<Scalars["numeric"]>
  trade_profitTarget?: Maybe<Scalars["numeric"]>
  trade_salt?: Maybe<Scalars["numeric"]>
  trade_slippage?: Maybe<Scalars["numeric"]>
  trade_stopLoss?: Maybe<Scalars["numeric"]>
}

/** aggregate var_pop on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Var_Pop_Fields = {
  __typename?: "uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent_var_pop_fields"
  evt_block_number?: Maybe<Scalars["Float"]>
  evt_index?: Maybe<Scalars["Float"]>
  evt_tx_index?: Maybe<Scalars["Float"]>
  trade_executionBlock?: Maybe<Scalars["Float"]>
  trade_executionTime?: Maybe<Scalars["Float"]>
  trade_leverage?: Maybe<Scalars["Float"]>
  trade_liquidationPrice?: Maybe<Scalars["Float"]>
  trade_margin?: Maybe<Scalars["Float"]>
  trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
  trade_openPrice?: Maybe<Scalars["Float"]>
  trade_profitTarget?: Maybe<Scalars["Float"]>
  trade_salt?: Maybe<Scalars["Float"]>
  trade_slippage?: Maybe<Scalars["Float"]>
  trade_stopLoss?: Maybe<Scalars["Float"]>
}

/** aggregate var_samp on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Var_Samp_Fields =
  {
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent_var_samp_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** aggregate variance on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_OpenLimitOrderEvent_Variance_Fields =
  {
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent_variance_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** columns and relationships of "uniwhale_v1_bsc.LimitBook_evt_PartialCloseLimitOrderEvent" */
export type Uniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEvent = {
  __typename?: "uniwhale_v1_bsc_LimitBook_evt_PartialCloseLimitOrderEvent"
  closePercent: Scalars["numeric"]
  contract_address: Scalars["bytea"]
  evt_block_number: Scalars["bigint"]
  evt_block_time: Scalars["timestamptz"]
  evt_index: Scalars["numeric"]
  evt_tx_hash: Scalars["bytea"]
  evt_tx_index: Scalars["numeric"]
  orderHash: Scalars["bytea"]
  sender: Scalars["bytea"]
  trade_executionBlock: Scalars["numeric"]
  trade_executionTime: Scalars["numeric"]
  trade_isBuy: Scalars["Boolean"]
  trade_leverage: Scalars["numeric"]
  trade_liquidationPrice: Scalars["numeric"]
  trade_margin: Scalars["numeric"]
  trade_maxPercentagePnL: Scalars["numeric"]
  trade_openPrice: Scalars["numeric"]
  trade_priceId: Scalars["bytea"]
  trade_profitTarget: Scalars["numeric"]
  trade_salt: Scalars["numeric"]
  trade_slippage: Scalars["numeric"]
  trade_stopLoss: Scalars["numeric"]
  trade_user: Scalars["bytea"]
}

/** Boolean expression to filter rows from the table "uniwhale_v1_bsc.LimitBook_evt_PartialCloseLimitOrderEvent". All fields are combined with a logical 'AND'. */
export type Uniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEvent_Bool_Exp =
  {
    _and?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEvent_Bool_Exp>
    >
    _not?: InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEvent_Bool_Exp>
    _or?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEvent_Bool_Exp>
    >
    closePercent?: InputMaybe<Numeric_Comparison_Exp>
    contract_address?: InputMaybe<Bytea_Comparison_Exp>
    evt_block_number?: InputMaybe<Bigint_Comparison_Exp>
    evt_block_time?: InputMaybe<Timestamptz_Comparison_Exp>
    evt_index?: InputMaybe<Numeric_Comparison_Exp>
    evt_tx_hash?: InputMaybe<Bytea_Comparison_Exp>
    evt_tx_index?: InputMaybe<Numeric_Comparison_Exp>
    orderHash?: InputMaybe<Bytea_Comparison_Exp>
    sender?: InputMaybe<Bytea_Comparison_Exp>
    trade_executionBlock?: InputMaybe<Numeric_Comparison_Exp>
    trade_executionTime?: InputMaybe<Numeric_Comparison_Exp>
    trade_isBuy?: InputMaybe<Boolean_Comparison_Exp>
    trade_leverage?: InputMaybe<Numeric_Comparison_Exp>
    trade_liquidationPrice?: InputMaybe<Numeric_Comparison_Exp>
    trade_margin?: InputMaybe<Numeric_Comparison_Exp>
    trade_maxPercentagePnL?: InputMaybe<Numeric_Comparison_Exp>
    trade_openPrice?: InputMaybe<Numeric_Comparison_Exp>
    trade_priceId?: InputMaybe<Bytea_Comparison_Exp>
    trade_profitTarget?: InputMaybe<Numeric_Comparison_Exp>
    trade_salt?: InputMaybe<Numeric_Comparison_Exp>
    trade_slippage?: InputMaybe<Numeric_Comparison_Exp>
    trade_stopLoss?: InputMaybe<Numeric_Comparison_Exp>
    trade_user?: InputMaybe<Bytea_Comparison_Exp>
  }

/** Ordering options when selecting data from "uniwhale_v1_bsc.LimitBook_evt_PartialCloseLimitOrderEvent". */
export type Uniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEvent_Order_By =
  {
    closePercent?: InputMaybe<Order_By>
    contract_address?: InputMaybe<Order_By>
    evt_block_number?: InputMaybe<Order_By>
    evt_block_time?: InputMaybe<Order_By>
    evt_index?: InputMaybe<Order_By>
    evt_tx_hash?: InputMaybe<Order_By>
    evt_tx_index?: InputMaybe<Order_By>
    orderHash?: InputMaybe<Order_By>
    sender?: InputMaybe<Order_By>
    trade_executionBlock?: InputMaybe<Order_By>
    trade_executionTime?: InputMaybe<Order_By>
    trade_isBuy?: InputMaybe<Order_By>
    trade_leverage?: InputMaybe<Order_By>
    trade_liquidationPrice?: InputMaybe<Order_By>
    trade_margin?: InputMaybe<Order_By>
    trade_maxPercentagePnL?: InputMaybe<Order_By>
    trade_openPrice?: InputMaybe<Order_By>
    trade_priceId?: InputMaybe<Order_By>
    trade_profitTarget?: InputMaybe<Order_By>
    trade_salt?: InputMaybe<Order_By>
    trade_slippage?: InputMaybe<Order_By>
    trade_stopLoss?: InputMaybe<Order_By>
    trade_user?: InputMaybe<Order_By>
  }

/** select columns of table "uniwhale_v1_bsc.LimitBook_evt_PartialCloseLimitOrderEvent" */
export enum Uniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEvent_Select_Column {
  /** column name */
  ClosePercent = "closePercent",
  /** column name */
  ContractAddress = "contract_address",
  /** column name */
  EvtBlockNumber = "evt_block_number",
  /** column name */
  EvtBlockTime = "evt_block_time",
  /** column name */
  EvtIndex = "evt_index",
  /** column name */
  EvtTxHash = "evt_tx_hash",
  /** column name */
  EvtTxIndex = "evt_tx_index",
  /** column name */
  OrderHash = "orderHash",
  /** column name */
  Sender = "sender",
  /** column name */
  TradeExecutionBlock = "trade_executionBlock",
  /** column name */
  TradeExecutionTime = "trade_executionTime",
  /** column name */
  TradeIsBuy = "trade_isBuy",
  /** column name */
  TradeLeverage = "trade_leverage",
  /** column name */
  TradeLiquidationPrice = "trade_liquidationPrice",
  /** column name */
  TradeMargin = "trade_margin",
  /** column name */
  TradeMaxPercentagePnL = "trade_maxPercentagePnL",
  /** column name */
  TradeOpenPrice = "trade_openPrice",
  /** column name */
  TradePriceId = "trade_priceId",
  /** column name */
  TradeProfitTarget = "trade_profitTarget",
  /** column name */
  TradeSalt = "trade_salt",
  /** column name */
  TradeSlippage = "trade_slippage",
  /** column name */
  TradeStopLoss = "trade_stopLoss",
  /** column name */
  TradeUser = "trade_user",
}

/** Streaming cursor of the table "uniwhale_v1_bsc_LimitBook_evt_PartialCloseLimitOrderEvent" */
export type Uniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEvent_Stream_Cursor_Input =
  {
    /** Stream column input with initial value */
    initial_value: Uniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEvent_Stream_Cursor_Value_Input
    /** cursor ordering */
    ordering?: InputMaybe<Cursor_Ordering>
  }

/** Initial value of the column from where the streaming should start */
export type Uniwhale_V1_Bsc_LimitBook_Evt_PartialCloseLimitOrderEvent_Stream_Cursor_Value_Input =
  {
    closePercent?: InputMaybe<Scalars["numeric"]>
    contract_address?: InputMaybe<Scalars["bytea"]>
    evt_block_number?: InputMaybe<Scalars["bigint"]>
    evt_block_time?: InputMaybe<Scalars["timestamptz"]>
    evt_index?: InputMaybe<Scalars["numeric"]>
    evt_tx_hash?: InputMaybe<Scalars["bytea"]>
    evt_tx_index?: InputMaybe<Scalars["numeric"]>
    orderHash?: InputMaybe<Scalars["bytea"]>
    sender?: InputMaybe<Scalars["bytea"]>
    trade_executionBlock?: InputMaybe<Scalars["numeric"]>
    trade_executionTime?: InputMaybe<Scalars["numeric"]>
    trade_isBuy?: InputMaybe<Scalars["Boolean"]>
    trade_leverage?: InputMaybe<Scalars["numeric"]>
    trade_liquidationPrice?: InputMaybe<Scalars["numeric"]>
    trade_margin?: InputMaybe<Scalars["numeric"]>
    trade_maxPercentagePnL?: InputMaybe<Scalars["numeric"]>
    trade_openPrice?: InputMaybe<Scalars["numeric"]>
    trade_priceId?: InputMaybe<Scalars["bytea"]>
    trade_profitTarget?: InputMaybe<Scalars["numeric"]>
    trade_salt?: InputMaybe<Scalars["numeric"]>
    trade_slippage?: InputMaybe<Scalars["numeric"]>
    trade_stopLoss?: InputMaybe<Scalars["numeric"]>
    trade_user?: InputMaybe<Scalars["bytea"]>
  }

/** columns and relationships of "uniwhale_v1_bsc.LimitBook_evt_UpdateOpenLimitOrderEvent" */
export type Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent = {
  __typename?: "uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent"
  contract_address: Scalars["bytea"]
  evt_block_number: Scalars["bigint"]
  evt_block_time: Scalars["timestamptz"]
  evt_index: Scalars["numeric"]
  evt_tx_hash: Scalars["bytea"]
  evt_tx_index: Scalars["numeric"]
  orderHash: Scalars["bytea"]
  sender: Scalars["bytea"]
  trade_executionBlock: Scalars["numeric"]
  trade_executionTime: Scalars["numeric"]
  trade_isBuy: Scalars["Boolean"]
  trade_leverage: Scalars["numeric"]
  trade_liquidationPrice: Scalars["numeric"]
  trade_margin: Scalars["numeric"]
  trade_maxPercentagePnL: Scalars["numeric"]
  trade_openPrice: Scalars["numeric"]
  trade_priceId: Scalars["bytea"]
  trade_profitTarget: Scalars["numeric"]
  trade_salt: Scalars["numeric"]
  trade_slippage: Scalars["numeric"]
  trade_stopLoss: Scalars["numeric"]
  trade_user: Scalars["bytea"]
}

/** aggregated selection of "uniwhale_v1_bsc.LimitBook_evt_UpdateOpenLimitOrderEvent" */
export type Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Aggregate =
  {
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent_aggregate"
    aggregate?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Aggregate_Fields>
    nodes: Array<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent>
  }

/** aggregate fields of "uniwhale_v1_bsc.LimitBook_evt_UpdateOpenLimitOrderEvent" */
export type Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Aggregate_Fields =
  {
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent_aggregate_fields"
    avg?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Avg_Fields>
    count: Scalars["Int"]
    max?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Max_Fields>
    min?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Min_Fields>
    stddev?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Stddev_Fields>
    stddev_pop?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Stddev_Pop_Fields>
    stddev_samp?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Stddev_Samp_Fields>
    sum?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Sum_Fields>
    var_pop?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Var_Pop_Fields>
    var_samp?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Var_Samp_Fields>
    variance?: Maybe<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Variance_Fields>
  }

/** aggregate fields of "uniwhale_v1_bsc.LimitBook_evt_UpdateOpenLimitOrderEvent" */
export type Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Aggregate_FieldsCountArgs =
  {
    columns?: InputMaybe<
      Array<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Select_Column>
    >
    distinct?: InputMaybe<Scalars["Boolean"]>
  }

/** aggregate avg on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Avg_Fields =
  {
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent_avg_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** Boolean expression to filter rows from the table "uniwhale_v1_bsc.LimitBook_evt_UpdateOpenLimitOrderEvent". All fields are combined with a logical 'AND'. */
export type Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Bool_Exp = {
  _and?: InputMaybe<
    Array<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Bool_Exp>
  >
  _not?: InputMaybe<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Bool_Exp>
  _or?: InputMaybe<
    Array<Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Bool_Exp>
  >
  contract_address?: InputMaybe<Bytea_Comparison_Exp>
  evt_block_number?: InputMaybe<Bigint_Comparison_Exp>
  evt_block_time?: InputMaybe<Timestamptz_Comparison_Exp>
  evt_index?: InputMaybe<Numeric_Comparison_Exp>
  evt_tx_hash?: InputMaybe<Bytea_Comparison_Exp>
  evt_tx_index?: InputMaybe<Numeric_Comparison_Exp>
  orderHash?: InputMaybe<Bytea_Comparison_Exp>
  sender?: InputMaybe<Bytea_Comparison_Exp>
  trade_executionBlock?: InputMaybe<Numeric_Comparison_Exp>
  trade_executionTime?: InputMaybe<Numeric_Comparison_Exp>
  trade_isBuy?: InputMaybe<Boolean_Comparison_Exp>
  trade_leverage?: InputMaybe<Numeric_Comparison_Exp>
  trade_liquidationPrice?: InputMaybe<Numeric_Comparison_Exp>
  trade_margin?: InputMaybe<Numeric_Comparison_Exp>
  trade_maxPercentagePnL?: InputMaybe<Numeric_Comparison_Exp>
  trade_openPrice?: InputMaybe<Numeric_Comparison_Exp>
  trade_priceId?: InputMaybe<Bytea_Comparison_Exp>
  trade_profitTarget?: InputMaybe<Numeric_Comparison_Exp>
  trade_salt?: InputMaybe<Numeric_Comparison_Exp>
  trade_slippage?: InputMaybe<Numeric_Comparison_Exp>
  trade_stopLoss?: InputMaybe<Numeric_Comparison_Exp>
  trade_user?: InputMaybe<Bytea_Comparison_Exp>
}

/** aggregate max on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Max_Fields =
  {
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent_max_fields"
    evt_block_number?: Maybe<Scalars["bigint"]>
    evt_block_time?: Maybe<Scalars["timestamptz"]>
    evt_index?: Maybe<Scalars["numeric"]>
    evt_tx_index?: Maybe<Scalars["numeric"]>
    trade_executionBlock?: Maybe<Scalars["numeric"]>
    trade_executionTime?: Maybe<Scalars["numeric"]>
    trade_leverage?: Maybe<Scalars["numeric"]>
    trade_liquidationPrice?: Maybe<Scalars["numeric"]>
    trade_margin?: Maybe<Scalars["numeric"]>
    trade_maxPercentagePnL?: Maybe<Scalars["numeric"]>
    trade_openPrice?: Maybe<Scalars["numeric"]>
    trade_profitTarget?: Maybe<Scalars["numeric"]>
    trade_salt?: Maybe<Scalars["numeric"]>
    trade_slippage?: Maybe<Scalars["numeric"]>
    trade_stopLoss?: Maybe<Scalars["numeric"]>
  }

/** aggregate min on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Min_Fields =
  {
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent_min_fields"
    evt_block_number?: Maybe<Scalars["bigint"]>
    evt_block_time?: Maybe<Scalars["timestamptz"]>
    evt_index?: Maybe<Scalars["numeric"]>
    evt_tx_index?: Maybe<Scalars["numeric"]>
    trade_executionBlock?: Maybe<Scalars["numeric"]>
    trade_executionTime?: Maybe<Scalars["numeric"]>
    trade_leverage?: Maybe<Scalars["numeric"]>
    trade_liquidationPrice?: Maybe<Scalars["numeric"]>
    trade_margin?: Maybe<Scalars["numeric"]>
    trade_maxPercentagePnL?: Maybe<Scalars["numeric"]>
    trade_openPrice?: Maybe<Scalars["numeric"]>
    trade_profitTarget?: Maybe<Scalars["numeric"]>
    trade_salt?: Maybe<Scalars["numeric"]>
    trade_slippage?: Maybe<Scalars["numeric"]>
    trade_stopLoss?: Maybe<Scalars["numeric"]>
  }

/** Ordering options when selecting data from "uniwhale_v1_bsc.LimitBook_evt_UpdateOpenLimitOrderEvent". */
export type Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Order_By = {
  contract_address?: InputMaybe<Order_By>
  evt_block_number?: InputMaybe<Order_By>
  evt_block_time?: InputMaybe<Order_By>
  evt_index?: InputMaybe<Order_By>
  evt_tx_hash?: InputMaybe<Order_By>
  evt_tx_index?: InputMaybe<Order_By>
  orderHash?: InputMaybe<Order_By>
  sender?: InputMaybe<Order_By>
  trade_executionBlock?: InputMaybe<Order_By>
  trade_executionTime?: InputMaybe<Order_By>
  trade_isBuy?: InputMaybe<Order_By>
  trade_leverage?: InputMaybe<Order_By>
  trade_liquidationPrice?: InputMaybe<Order_By>
  trade_margin?: InputMaybe<Order_By>
  trade_maxPercentagePnL?: InputMaybe<Order_By>
  trade_openPrice?: InputMaybe<Order_By>
  trade_priceId?: InputMaybe<Order_By>
  trade_profitTarget?: InputMaybe<Order_By>
  trade_salt?: InputMaybe<Order_By>
  trade_slippage?: InputMaybe<Order_By>
  trade_stopLoss?: InputMaybe<Order_By>
  trade_user?: InputMaybe<Order_By>
}

/** select columns of table "uniwhale_v1_bsc.LimitBook_evt_UpdateOpenLimitOrderEvent" */
export enum Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Select_Column {
  /** column name */
  ContractAddress = "contract_address",
  /** column name */
  EvtBlockNumber = "evt_block_number",
  /** column name */
  EvtBlockTime = "evt_block_time",
  /** column name */
  EvtIndex = "evt_index",
  /** column name */
  EvtTxHash = "evt_tx_hash",
  /** column name */
  EvtTxIndex = "evt_tx_index",
  /** column name */
  OrderHash = "orderHash",
  /** column name */
  Sender = "sender",
  /** column name */
  TradeExecutionBlock = "trade_executionBlock",
  /** column name */
  TradeExecutionTime = "trade_executionTime",
  /** column name */
  TradeIsBuy = "trade_isBuy",
  /** column name */
  TradeLeverage = "trade_leverage",
  /** column name */
  TradeLiquidationPrice = "trade_liquidationPrice",
  /** column name */
  TradeMargin = "trade_margin",
  /** column name */
  TradeMaxPercentagePnL = "trade_maxPercentagePnL",
  /** column name */
  TradeOpenPrice = "trade_openPrice",
  /** column name */
  TradePriceId = "trade_priceId",
  /** column name */
  TradeProfitTarget = "trade_profitTarget",
  /** column name */
  TradeSalt = "trade_salt",
  /** column name */
  TradeSlippage = "trade_slippage",
  /** column name */
  TradeStopLoss = "trade_stopLoss",
  /** column name */
  TradeUser = "trade_user",
}

/** aggregate stddev on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Stddev_Fields =
  {
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent_stddev_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** aggregate stddev_pop on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Stddev_Pop_Fields =
  {
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent_stddev_pop_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** aggregate stddev_samp on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Stddev_Samp_Fields =
  {
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent_stddev_samp_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** Streaming cursor of the table "uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent" */
export type Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Stream_Cursor_Input =
  {
    /** Stream column input with initial value */
    initial_value: Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Stream_Cursor_Value_Input
    /** cursor ordering */
    ordering?: InputMaybe<Cursor_Ordering>
  }

/** Initial value of the column from where the streaming should start */
export type Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Stream_Cursor_Value_Input =
  {
    contract_address?: InputMaybe<Scalars["bytea"]>
    evt_block_number?: InputMaybe<Scalars["bigint"]>
    evt_block_time?: InputMaybe<Scalars["timestamptz"]>
    evt_index?: InputMaybe<Scalars["numeric"]>
    evt_tx_hash?: InputMaybe<Scalars["bytea"]>
    evt_tx_index?: InputMaybe<Scalars["numeric"]>
    orderHash?: InputMaybe<Scalars["bytea"]>
    sender?: InputMaybe<Scalars["bytea"]>
    trade_executionBlock?: InputMaybe<Scalars["numeric"]>
    trade_executionTime?: InputMaybe<Scalars["numeric"]>
    trade_isBuy?: InputMaybe<Scalars["Boolean"]>
    trade_leverage?: InputMaybe<Scalars["numeric"]>
    trade_liquidationPrice?: InputMaybe<Scalars["numeric"]>
    trade_margin?: InputMaybe<Scalars["numeric"]>
    trade_maxPercentagePnL?: InputMaybe<Scalars["numeric"]>
    trade_openPrice?: InputMaybe<Scalars["numeric"]>
    trade_priceId?: InputMaybe<Scalars["bytea"]>
    trade_profitTarget?: InputMaybe<Scalars["numeric"]>
    trade_salt?: InputMaybe<Scalars["numeric"]>
    trade_slippage?: InputMaybe<Scalars["numeric"]>
    trade_stopLoss?: InputMaybe<Scalars["numeric"]>
    trade_user?: InputMaybe<Scalars["bytea"]>
  }

/** aggregate sum on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Sum_Fields =
  {
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent_sum_fields"
    evt_block_number?: Maybe<Scalars["bigint"]>
    evt_index?: Maybe<Scalars["numeric"]>
    evt_tx_index?: Maybe<Scalars["numeric"]>
    trade_executionBlock?: Maybe<Scalars["numeric"]>
    trade_executionTime?: Maybe<Scalars["numeric"]>
    trade_leverage?: Maybe<Scalars["numeric"]>
    trade_liquidationPrice?: Maybe<Scalars["numeric"]>
    trade_margin?: Maybe<Scalars["numeric"]>
    trade_maxPercentagePnL?: Maybe<Scalars["numeric"]>
    trade_openPrice?: Maybe<Scalars["numeric"]>
    trade_profitTarget?: Maybe<Scalars["numeric"]>
    trade_salt?: Maybe<Scalars["numeric"]>
    trade_slippage?: Maybe<Scalars["numeric"]>
    trade_stopLoss?: Maybe<Scalars["numeric"]>
  }

/** aggregate var_pop on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Var_Pop_Fields =
  {
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent_var_pop_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** aggregate var_samp on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Var_Samp_Fields =
  {
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent_var_samp_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** aggregate variance on columns */
export type Uniwhale_V1_Bsc_LimitBook_Evt_UpdateOpenLimitOrderEvent_Variance_Fields =
  {
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent_variance_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** columns and relationships of "uniwhale_v1_bsc.Referrals_evt_SetDefaultRebatePctEvent" */
export type Uniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEvent = {
  __typename?: "uniwhale_v1_bsc_Referrals_evt_SetDefaultRebatePctEvent"
  contract_address: Scalars["bytea"]
  evt_block_number: Scalars["bigint"]
  evt_block_time: Scalars["timestamptz"]
  evt_index: Scalars["numeric"]
  evt_tx_hash: Scalars["bytea"]
  evt_tx_index?: Maybe<Scalars["numeric"]>
  referralRebatePct: Scalars["numeric"]
  referredRebatePct: Scalars["numeric"]
}

/** Boolean expression to filter rows from the table "uniwhale_v1_bsc.Referrals_evt_SetDefaultRebatePctEvent". All fields are combined with a logical 'AND'. */
export type Uniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEvent_Bool_Exp = {
  _and?: InputMaybe<
    Array<Uniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEvent_Bool_Exp>
  >
  _not?: InputMaybe<Uniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEvent_Bool_Exp>
  _or?: InputMaybe<
    Array<Uniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEvent_Bool_Exp>
  >
  contract_address?: InputMaybe<Bytea_Comparison_Exp>
  evt_block_number?: InputMaybe<Bigint_Comparison_Exp>
  evt_block_time?: InputMaybe<Timestamptz_Comparison_Exp>
  evt_index?: InputMaybe<Numeric_Comparison_Exp>
  evt_tx_hash?: InputMaybe<Bytea_Comparison_Exp>
  evt_tx_index?: InputMaybe<Numeric_Comparison_Exp>
  referralRebatePct?: InputMaybe<Numeric_Comparison_Exp>
  referredRebatePct?: InputMaybe<Numeric_Comparison_Exp>
}

/** Ordering options when selecting data from "uniwhale_v1_bsc.Referrals_evt_SetDefaultRebatePctEvent". */
export type Uniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEvent_Order_By = {
  contract_address?: InputMaybe<Order_By>
  evt_block_number?: InputMaybe<Order_By>
  evt_block_time?: InputMaybe<Order_By>
  evt_index?: InputMaybe<Order_By>
  evt_tx_hash?: InputMaybe<Order_By>
  evt_tx_index?: InputMaybe<Order_By>
  referralRebatePct?: InputMaybe<Order_By>
  referredRebatePct?: InputMaybe<Order_By>
}

/** select columns of table "uniwhale_v1_bsc.Referrals_evt_SetDefaultRebatePctEvent" */
export enum Uniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEvent_Select_Column {
  /** column name */
  ContractAddress = "contract_address",
  /** column name */
  EvtBlockNumber = "evt_block_number",
  /** column name */
  EvtBlockTime = "evt_block_time",
  /** column name */
  EvtIndex = "evt_index",
  /** column name */
  EvtTxHash = "evt_tx_hash",
  /** column name */
  EvtTxIndex = "evt_tx_index",
  /** column name */
  ReferralRebatePct = "referralRebatePct",
  /** column name */
  ReferredRebatePct = "referredRebatePct",
}

/** Streaming cursor of the table "uniwhale_v1_bsc_Referrals_evt_SetDefaultRebatePctEvent" */
export type Uniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEvent_Stream_Cursor_Input =
  {
    /** Stream column input with initial value */
    initial_value: Uniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEvent_Stream_Cursor_Value_Input
    /** cursor ordering */
    ordering?: InputMaybe<Cursor_Ordering>
  }

/** Initial value of the column from where the streaming should start */
export type Uniwhale_V1_Bsc_Referrals_Evt_SetDefaultRebatePctEvent_Stream_Cursor_Value_Input =
  {
    contract_address?: InputMaybe<Scalars["bytea"]>
    evt_block_number?: InputMaybe<Scalars["bigint"]>
    evt_block_time?: InputMaybe<Scalars["timestamptz"]>
    evt_index?: InputMaybe<Scalars["numeric"]>
    evt_tx_hash?: InputMaybe<Scalars["bytea"]>
    evt_tx_index?: InputMaybe<Scalars["numeric"]>
    referralRebatePct?: InputMaybe<Scalars["numeric"]>
    referredRebatePct?: InputMaybe<Scalars["numeric"]>
  }

/** columns and relationships of "uniwhale_v1_bsc.Referrals_evt_SetRebatePctEvent" */
export type Uniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEvent = {
  __typename?: "uniwhale_v1_bsc_Referrals_evt_SetRebatePctEvent"
  contract_address: Scalars["bytea"]
  evt_block_number: Scalars["bigint"]
  evt_block_time: Scalars["timestamptz"]
  evt_index: Scalars["numeric"]
  evt_tx_hash: Scalars["bytea"]
  evt_tx_index?: Maybe<Scalars["numeric"]>
  referralCode: Scalars["bytea"]
  referralRebatePct: Scalars["numeric"]
  referredRebatePct: Scalars["numeric"]
}

/** Boolean expression to filter rows from the table "uniwhale_v1_bsc.Referrals_evt_SetRebatePctEvent". All fields are combined with a logical 'AND'. */
export type Uniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEvent_Bool_Exp = {
  _and?: InputMaybe<
    Array<Uniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEvent_Bool_Exp>
  >
  _not?: InputMaybe<Uniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEvent_Bool_Exp>
  _or?: InputMaybe<
    Array<Uniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEvent_Bool_Exp>
  >
  contract_address?: InputMaybe<Bytea_Comparison_Exp>
  evt_block_number?: InputMaybe<Bigint_Comparison_Exp>
  evt_block_time?: InputMaybe<Timestamptz_Comparison_Exp>
  evt_index?: InputMaybe<Numeric_Comparison_Exp>
  evt_tx_hash?: InputMaybe<Bytea_Comparison_Exp>
  evt_tx_index?: InputMaybe<Numeric_Comparison_Exp>
  referralCode?: InputMaybe<Bytea_Comparison_Exp>
  referralRebatePct?: InputMaybe<Numeric_Comparison_Exp>
  referredRebatePct?: InputMaybe<Numeric_Comparison_Exp>
}

/** Ordering options when selecting data from "uniwhale_v1_bsc.Referrals_evt_SetRebatePctEvent". */
export type Uniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEvent_Order_By = {
  contract_address?: InputMaybe<Order_By>
  evt_block_number?: InputMaybe<Order_By>
  evt_block_time?: InputMaybe<Order_By>
  evt_index?: InputMaybe<Order_By>
  evt_tx_hash?: InputMaybe<Order_By>
  evt_tx_index?: InputMaybe<Order_By>
  referralCode?: InputMaybe<Order_By>
  referralRebatePct?: InputMaybe<Order_By>
  referredRebatePct?: InputMaybe<Order_By>
}

/** select columns of table "uniwhale_v1_bsc.Referrals_evt_SetRebatePctEvent" */
export enum Uniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEvent_Select_Column {
  /** column name */
  ContractAddress = "contract_address",
  /** column name */
  EvtBlockNumber = "evt_block_number",
  /** column name */
  EvtBlockTime = "evt_block_time",
  /** column name */
  EvtIndex = "evt_index",
  /** column name */
  EvtTxHash = "evt_tx_hash",
  /** column name */
  EvtTxIndex = "evt_tx_index",
  /** column name */
  ReferralCode = "referralCode",
  /** column name */
  ReferralRebatePct = "referralRebatePct",
  /** column name */
  ReferredRebatePct = "referredRebatePct",
}

/** Streaming cursor of the table "uniwhale_v1_bsc_Referrals_evt_SetRebatePctEvent" */
export type Uniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEvent_Stream_Cursor_Input =
  {
    /** Stream column input with initial value */
    initial_value: Uniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEvent_Stream_Cursor_Value_Input
    /** cursor ordering */
    ordering?: InputMaybe<Cursor_Ordering>
  }

/** Initial value of the column from where the streaming should start */
export type Uniwhale_V1_Bsc_Referrals_Evt_SetRebatePctEvent_Stream_Cursor_Value_Input =
  {
    contract_address?: InputMaybe<Scalars["bytea"]>
    evt_block_number?: InputMaybe<Scalars["bigint"]>
    evt_block_time?: InputMaybe<Scalars["timestamptz"]>
    evt_index?: InputMaybe<Scalars["numeric"]>
    evt_tx_hash?: InputMaybe<Scalars["bytea"]>
    evt_tx_index?: InputMaybe<Scalars["numeric"]>
    referralCode?: InputMaybe<Scalars["bytea"]>
    referralRebatePct?: InputMaybe<Scalars["numeric"]>
    referredRebatePct?: InputMaybe<Scalars["numeric"]>
  }

/** columns and relationships of "uniwhale_v1_bsc.TradingCore_evt_CloseMarketOrderEvent" */
export type Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent = {
  __typename?: "uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent"
  afterCloseTrade_fees_fee: Scalars["numeric"]
  afterCloseTrade_fees_referralCode: Scalars["bytea"]
  afterCloseTrade_fees_referralFee: Scalars["numeric"]
  afterCloseTrade_fees_referredFee: Scalars["numeric"]
  afterCloseTrade_fees_referrer: Scalars["bytea"]
  afterCloseTrade_liquidationFee: Scalars["numeric"]
  afterCloseTrade_oraclePrice: Scalars["numeric"]
  afterCloseTrade_settled: Scalars["numeric"]
  closePercent: Scalars["numeric"]
  contract_address: Scalars["bytea"]
  evt_block_number: Scalars["bigint"]
  evt_block_time: Scalars["timestamptz"]
  evt_index: Scalars["numeric"]
  evt_tx_hash: Scalars["bytea"]
  evt_tx_index: Scalars["numeric"]
  onCloseTrade_closeNet: Scalars["numeric"]
  onCloseTrade_fundingFee: Scalars["numeric"]
  onCloseTrade_grossPnL: Scalars["numeric"]
  onCloseTrade_isLiquidated: Scalars["Boolean"]
  onCloseTrade_isStop: Scalars["Boolean"]
  onCloseTrade_rolloverFee: Scalars["numeric"]
  onCloseTrade_slippage: Scalars["numeric"]
  onOrderUpdate_accruedFee_fundingFee: Scalars["numeric"]
  onOrderUpdate_accruedFee_lastUpdate: Scalars["numeric"]
  onOrderUpdate_accruedFee_rolloverFee: Scalars["numeric"]
  onOrderUpdate_feeBalance_balance: Scalars["numeric"]
  onOrderUpdate_feeBalance_lastUpdate: Scalars["numeric"]
  onOrderUpdate_feeBase: Scalars["numeric"]
  orderHash: Scalars["bytea"]
  trade_executionBlock: Scalars["numeric"]
  trade_executionTime: Scalars["numeric"]
  trade_isBuy: Scalars["Boolean"]
  trade_leverage: Scalars["numeric"]
  trade_liquidationPrice: Scalars["numeric"]
  trade_margin: Scalars["numeric"]
  trade_maxPercentagePnL: Scalars["numeric"]
  trade_openPrice: Scalars["numeric"]
  trade_priceId: Scalars["bytea"]
  trade_profitTarget: Scalars["numeric"]
  trade_salt: Scalars["numeric"]
  trade_slippage: Scalars["numeric"]
  trade_stopLoss: Scalars["numeric"]
  trade_user: Scalars["bytea"]
  user: Scalars["bytea"]
}

/** aggregated selection of "uniwhale_v1_bsc.TradingCore_evt_CloseMarketOrderEvent" */
export type Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Aggregate = {
  __typename?: "uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent_aggregate"
  aggregate?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Aggregate_Fields>
  nodes: Array<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent>
}

/** aggregate fields of "uniwhale_v1_bsc.TradingCore_evt_CloseMarketOrderEvent" */
export type Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Aggregate_Fields =
  {
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent_aggregate_fields"
    avg?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Avg_Fields>
    count: Scalars["Int"]
    max?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Max_Fields>
    min?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Min_Fields>
    stddev?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Stddev_Fields>
    stddev_pop?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Stddev_Pop_Fields>
    stddev_samp?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Stddev_Samp_Fields>
    sum?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Sum_Fields>
    var_pop?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Var_Pop_Fields>
    var_samp?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Var_Samp_Fields>
    variance?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Variance_Fields>
  }

/** aggregate fields of "uniwhale_v1_bsc.TradingCore_evt_CloseMarketOrderEvent" */
export type Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Aggregate_FieldsCountArgs =
  {
    columns?: InputMaybe<
      Array<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Select_Column>
    >
    distinct?: InputMaybe<Scalars["Boolean"]>
  }

/** aggregate avg on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Avg_Fields = {
  __typename?: "uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent_avg_fields"
  afterCloseTrade_fees_fee?: Maybe<Scalars["Float"]>
  afterCloseTrade_fees_referralFee?: Maybe<Scalars["Float"]>
  afterCloseTrade_fees_referredFee?: Maybe<Scalars["Float"]>
  afterCloseTrade_liquidationFee?: Maybe<Scalars["Float"]>
  afterCloseTrade_oraclePrice?: Maybe<Scalars["Float"]>
  afterCloseTrade_settled?: Maybe<Scalars["Float"]>
  closePercent?: Maybe<Scalars["Float"]>
  evt_block_number?: Maybe<Scalars["Float"]>
  evt_index?: Maybe<Scalars["Float"]>
  evt_tx_index?: Maybe<Scalars["Float"]>
  onCloseTrade_closeNet?: Maybe<Scalars["Float"]>
  onCloseTrade_fundingFee?: Maybe<Scalars["Float"]>
  onCloseTrade_grossPnL?: Maybe<Scalars["Float"]>
  onCloseTrade_rolloverFee?: Maybe<Scalars["Float"]>
  onCloseTrade_slippage?: Maybe<Scalars["Float"]>
  onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["Float"]>
  onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["Float"]>
  onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["Float"]>
  onOrderUpdate_feeBalance_balance?: Maybe<Scalars["Float"]>
  onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["Float"]>
  onOrderUpdate_feeBase?: Maybe<Scalars["Float"]>
  trade_executionBlock?: Maybe<Scalars["Float"]>
  trade_executionTime?: Maybe<Scalars["Float"]>
  trade_leverage?: Maybe<Scalars["Float"]>
  trade_liquidationPrice?: Maybe<Scalars["Float"]>
  trade_margin?: Maybe<Scalars["Float"]>
  trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
  trade_openPrice?: Maybe<Scalars["Float"]>
  trade_profitTarget?: Maybe<Scalars["Float"]>
  trade_salt?: Maybe<Scalars["Float"]>
  trade_slippage?: Maybe<Scalars["Float"]>
  trade_stopLoss?: Maybe<Scalars["Float"]>
}

/** Boolean expression to filter rows from the table "uniwhale_v1_bsc.TradingCore_evt_CloseMarketOrderEvent". All fields are combined with a logical 'AND'. */
export type Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Bool_Exp = {
  _and?: InputMaybe<
    Array<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Bool_Exp>
  >
  _not?: InputMaybe<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Bool_Exp>
  _or?: InputMaybe<
    Array<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Bool_Exp>
  >
  afterCloseTrade_fees_fee?: InputMaybe<Numeric_Comparison_Exp>
  afterCloseTrade_fees_referralCode?: InputMaybe<Bytea_Comparison_Exp>
  afterCloseTrade_fees_referralFee?: InputMaybe<Numeric_Comparison_Exp>
  afterCloseTrade_fees_referredFee?: InputMaybe<Numeric_Comparison_Exp>
  afterCloseTrade_fees_referrer?: InputMaybe<Bytea_Comparison_Exp>
  afterCloseTrade_liquidationFee?: InputMaybe<Numeric_Comparison_Exp>
  afterCloseTrade_oraclePrice?: InputMaybe<Numeric_Comparison_Exp>
  afterCloseTrade_settled?: InputMaybe<Numeric_Comparison_Exp>
  closePercent?: InputMaybe<Numeric_Comparison_Exp>
  contract_address?: InputMaybe<Bytea_Comparison_Exp>
  evt_block_number?: InputMaybe<Bigint_Comparison_Exp>
  evt_block_time?: InputMaybe<Timestamptz_Comparison_Exp>
  evt_index?: InputMaybe<Numeric_Comparison_Exp>
  evt_tx_hash?: InputMaybe<Bytea_Comparison_Exp>
  evt_tx_index?: InputMaybe<Numeric_Comparison_Exp>
  onCloseTrade_closeNet?: InputMaybe<Numeric_Comparison_Exp>
  onCloseTrade_fundingFee?: InputMaybe<Numeric_Comparison_Exp>
  onCloseTrade_grossPnL?: InputMaybe<Numeric_Comparison_Exp>
  onCloseTrade_isLiquidated?: InputMaybe<Boolean_Comparison_Exp>
  onCloseTrade_isStop?: InputMaybe<Boolean_Comparison_Exp>
  onCloseTrade_rolloverFee?: InputMaybe<Numeric_Comparison_Exp>
  onCloseTrade_slippage?: InputMaybe<Numeric_Comparison_Exp>
  onOrderUpdate_accruedFee_fundingFee?: InputMaybe<Numeric_Comparison_Exp>
  onOrderUpdate_accruedFee_lastUpdate?: InputMaybe<Numeric_Comparison_Exp>
  onOrderUpdate_accruedFee_rolloverFee?: InputMaybe<Numeric_Comparison_Exp>
  onOrderUpdate_feeBalance_balance?: InputMaybe<Numeric_Comparison_Exp>
  onOrderUpdate_feeBalance_lastUpdate?: InputMaybe<Numeric_Comparison_Exp>
  onOrderUpdate_feeBase?: InputMaybe<Numeric_Comparison_Exp>
  orderHash?: InputMaybe<Bytea_Comparison_Exp>
  trade_executionBlock?: InputMaybe<Numeric_Comparison_Exp>
  trade_executionTime?: InputMaybe<Numeric_Comparison_Exp>
  trade_isBuy?: InputMaybe<Boolean_Comparison_Exp>
  trade_leverage?: InputMaybe<Numeric_Comparison_Exp>
  trade_liquidationPrice?: InputMaybe<Numeric_Comparison_Exp>
  trade_margin?: InputMaybe<Numeric_Comparison_Exp>
  trade_maxPercentagePnL?: InputMaybe<Numeric_Comparison_Exp>
  trade_openPrice?: InputMaybe<Numeric_Comparison_Exp>
  trade_priceId?: InputMaybe<Bytea_Comparison_Exp>
  trade_profitTarget?: InputMaybe<Numeric_Comparison_Exp>
  trade_salt?: InputMaybe<Numeric_Comparison_Exp>
  trade_slippage?: InputMaybe<Numeric_Comparison_Exp>
  trade_stopLoss?: InputMaybe<Numeric_Comparison_Exp>
  trade_user?: InputMaybe<Bytea_Comparison_Exp>
  user?: InputMaybe<Bytea_Comparison_Exp>
}

/** aggregate max on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Max_Fields = {
  __typename?: "uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent_max_fields"
  afterCloseTrade_fees_fee?: Maybe<Scalars["numeric"]>
  afterCloseTrade_fees_referralFee?: Maybe<Scalars["numeric"]>
  afterCloseTrade_fees_referredFee?: Maybe<Scalars["numeric"]>
  afterCloseTrade_liquidationFee?: Maybe<Scalars["numeric"]>
  afterCloseTrade_oraclePrice?: Maybe<Scalars["numeric"]>
  afterCloseTrade_settled?: Maybe<Scalars["numeric"]>
  closePercent?: Maybe<Scalars["numeric"]>
  evt_block_number?: Maybe<Scalars["bigint"]>
  evt_block_time?: Maybe<Scalars["timestamptz"]>
  evt_index?: Maybe<Scalars["numeric"]>
  evt_tx_index?: Maybe<Scalars["numeric"]>
  onCloseTrade_closeNet?: Maybe<Scalars["numeric"]>
  onCloseTrade_fundingFee?: Maybe<Scalars["numeric"]>
  onCloseTrade_grossPnL?: Maybe<Scalars["numeric"]>
  onCloseTrade_rolloverFee?: Maybe<Scalars["numeric"]>
  onCloseTrade_slippage?: Maybe<Scalars["numeric"]>
  onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["numeric"]>
  onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["numeric"]>
  onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["numeric"]>
  onOrderUpdate_feeBalance_balance?: Maybe<Scalars["numeric"]>
  onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["numeric"]>
  onOrderUpdate_feeBase?: Maybe<Scalars["numeric"]>
  trade_executionBlock?: Maybe<Scalars["numeric"]>
  trade_executionTime?: Maybe<Scalars["numeric"]>
  trade_leverage?: Maybe<Scalars["numeric"]>
  trade_liquidationPrice?: Maybe<Scalars["numeric"]>
  trade_margin?: Maybe<Scalars["numeric"]>
  trade_maxPercentagePnL?: Maybe<Scalars["numeric"]>
  trade_openPrice?: Maybe<Scalars["numeric"]>
  trade_profitTarget?: Maybe<Scalars["numeric"]>
  trade_salt?: Maybe<Scalars["numeric"]>
  trade_slippage?: Maybe<Scalars["numeric"]>
  trade_stopLoss?: Maybe<Scalars["numeric"]>
}

/** aggregate min on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Min_Fields = {
  __typename?: "uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent_min_fields"
  afterCloseTrade_fees_fee?: Maybe<Scalars["numeric"]>
  afterCloseTrade_fees_referralFee?: Maybe<Scalars["numeric"]>
  afterCloseTrade_fees_referredFee?: Maybe<Scalars["numeric"]>
  afterCloseTrade_liquidationFee?: Maybe<Scalars["numeric"]>
  afterCloseTrade_oraclePrice?: Maybe<Scalars["numeric"]>
  afterCloseTrade_settled?: Maybe<Scalars["numeric"]>
  closePercent?: Maybe<Scalars["numeric"]>
  evt_block_number?: Maybe<Scalars["bigint"]>
  evt_block_time?: Maybe<Scalars["timestamptz"]>
  evt_index?: Maybe<Scalars["numeric"]>
  evt_tx_index?: Maybe<Scalars["numeric"]>
  onCloseTrade_closeNet?: Maybe<Scalars["numeric"]>
  onCloseTrade_fundingFee?: Maybe<Scalars["numeric"]>
  onCloseTrade_grossPnL?: Maybe<Scalars["numeric"]>
  onCloseTrade_rolloverFee?: Maybe<Scalars["numeric"]>
  onCloseTrade_slippage?: Maybe<Scalars["numeric"]>
  onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["numeric"]>
  onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["numeric"]>
  onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["numeric"]>
  onOrderUpdate_feeBalance_balance?: Maybe<Scalars["numeric"]>
  onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["numeric"]>
  onOrderUpdate_feeBase?: Maybe<Scalars["numeric"]>
  trade_executionBlock?: Maybe<Scalars["numeric"]>
  trade_executionTime?: Maybe<Scalars["numeric"]>
  trade_leverage?: Maybe<Scalars["numeric"]>
  trade_liquidationPrice?: Maybe<Scalars["numeric"]>
  trade_margin?: Maybe<Scalars["numeric"]>
  trade_maxPercentagePnL?: Maybe<Scalars["numeric"]>
  trade_openPrice?: Maybe<Scalars["numeric"]>
  trade_profitTarget?: Maybe<Scalars["numeric"]>
  trade_salt?: Maybe<Scalars["numeric"]>
  trade_slippage?: Maybe<Scalars["numeric"]>
  trade_stopLoss?: Maybe<Scalars["numeric"]>
}

/** Ordering options when selecting data from "uniwhale_v1_bsc.TradingCore_evt_CloseMarketOrderEvent". */
export type Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Order_By = {
  afterCloseTrade_fees_fee?: InputMaybe<Order_By>
  afterCloseTrade_fees_referralCode?: InputMaybe<Order_By>
  afterCloseTrade_fees_referralFee?: InputMaybe<Order_By>
  afterCloseTrade_fees_referredFee?: InputMaybe<Order_By>
  afterCloseTrade_fees_referrer?: InputMaybe<Order_By>
  afterCloseTrade_liquidationFee?: InputMaybe<Order_By>
  afterCloseTrade_oraclePrice?: InputMaybe<Order_By>
  afterCloseTrade_settled?: InputMaybe<Order_By>
  closePercent?: InputMaybe<Order_By>
  contract_address?: InputMaybe<Order_By>
  evt_block_number?: InputMaybe<Order_By>
  evt_block_time?: InputMaybe<Order_By>
  evt_index?: InputMaybe<Order_By>
  evt_tx_hash?: InputMaybe<Order_By>
  evt_tx_index?: InputMaybe<Order_By>
  onCloseTrade_closeNet?: InputMaybe<Order_By>
  onCloseTrade_fundingFee?: InputMaybe<Order_By>
  onCloseTrade_grossPnL?: InputMaybe<Order_By>
  onCloseTrade_isLiquidated?: InputMaybe<Order_By>
  onCloseTrade_isStop?: InputMaybe<Order_By>
  onCloseTrade_rolloverFee?: InputMaybe<Order_By>
  onCloseTrade_slippage?: InputMaybe<Order_By>
  onOrderUpdate_accruedFee_fundingFee?: InputMaybe<Order_By>
  onOrderUpdate_accruedFee_lastUpdate?: InputMaybe<Order_By>
  onOrderUpdate_accruedFee_rolloverFee?: InputMaybe<Order_By>
  onOrderUpdate_feeBalance_balance?: InputMaybe<Order_By>
  onOrderUpdate_feeBalance_lastUpdate?: InputMaybe<Order_By>
  onOrderUpdate_feeBase?: InputMaybe<Order_By>
  orderHash?: InputMaybe<Order_By>
  trade_executionBlock?: InputMaybe<Order_By>
  trade_executionTime?: InputMaybe<Order_By>
  trade_isBuy?: InputMaybe<Order_By>
  trade_leverage?: InputMaybe<Order_By>
  trade_liquidationPrice?: InputMaybe<Order_By>
  trade_margin?: InputMaybe<Order_By>
  trade_maxPercentagePnL?: InputMaybe<Order_By>
  trade_openPrice?: InputMaybe<Order_By>
  trade_priceId?: InputMaybe<Order_By>
  trade_profitTarget?: InputMaybe<Order_By>
  trade_salt?: InputMaybe<Order_By>
  trade_slippage?: InputMaybe<Order_By>
  trade_stopLoss?: InputMaybe<Order_By>
  trade_user?: InputMaybe<Order_By>
  user?: InputMaybe<Order_By>
}

/** select columns of table "uniwhale_v1_bsc.TradingCore_evt_CloseMarketOrderEvent" */
export enum Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Select_Column {
  /** column name */
  AfterCloseTradeFeesFee = "afterCloseTrade_fees_fee",
  /** column name */
  AfterCloseTradeFeesReferralCode = "afterCloseTrade_fees_referralCode",
  /** column name */
  AfterCloseTradeFeesReferralFee = "afterCloseTrade_fees_referralFee",
  /** column name */
  AfterCloseTradeFeesReferredFee = "afterCloseTrade_fees_referredFee",
  /** column name */
  AfterCloseTradeFeesReferrer = "afterCloseTrade_fees_referrer",
  /** column name */
  AfterCloseTradeLiquidationFee = "afterCloseTrade_liquidationFee",
  /** column name */
  AfterCloseTradeOraclePrice = "afterCloseTrade_oraclePrice",
  /** column name */
  AfterCloseTradeSettled = "afterCloseTrade_settled",
  /** column name */
  ClosePercent = "closePercent",
  /** column name */
  ContractAddress = "contract_address",
  /** column name */
  EvtBlockNumber = "evt_block_number",
  /** column name */
  EvtBlockTime = "evt_block_time",
  /** column name */
  EvtIndex = "evt_index",
  /** column name */
  EvtTxHash = "evt_tx_hash",
  /** column name */
  EvtTxIndex = "evt_tx_index",
  /** column name */
  OnCloseTradeCloseNet = "onCloseTrade_closeNet",
  /** column name */
  OnCloseTradeFundingFee = "onCloseTrade_fundingFee",
  /** column name */
  OnCloseTradeGrossPnL = "onCloseTrade_grossPnL",
  /** column name */
  OnCloseTradeIsLiquidated = "onCloseTrade_isLiquidated",
  /** column name */
  OnCloseTradeIsStop = "onCloseTrade_isStop",
  /** column name */
  OnCloseTradeRolloverFee = "onCloseTrade_rolloverFee",
  /** column name */
  OnCloseTradeSlippage = "onCloseTrade_slippage",
  /** column name */
  OnOrderUpdateAccruedFeeFundingFee = "onOrderUpdate_accruedFee_fundingFee",
  /** column name */
  OnOrderUpdateAccruedFeeLastUpdate = "onOrderUpdate_accruedFee_lastUpdate",
  /** column name */
  OnOrderUpdateAccruedFeeRolloverFee = "onOrderUpdate_accruedFee_rolloverFee",
  /** column name */
  OnOrderUpdateFeeBalanceBalance = "onOrderUpdate_feeBalance_balance",
  /** column name */
  OnOrderUpdateFeeBalanceLastUpdate = "onOrderUpdate_feeBalance_lastUpdate",
  /** column name */
  OnOrderUpdateFeeBase = "onOrderUpdate_feeBase",
  /** column name */
  OrderHash = "orderHash",
  /** column name */
  TradeExecutionBlock = "trade_executionBlock",
  /** column name */
  TradeExecutionTime = "trade_executionTime",
  /** column name */
  TradeIsBuy = "trade_isBuy",
  /** column name */
  TradeLeverage = "trade_leverage",
  /** column name */
  TradeLiquidationPrice = "trade_liquidationPrice",
  /** column name */
  TradeMargin = "trade_margin",
  /** column name */
  TradeMaxPercentagePnL = "trade_maxPercentagePnL",
  /** column name */
  TradeOpenPrice = "trade_openPrice",
  /** column name */
  TradePriceId = "trade_priceId",
  /** column name */
  TradeProfitTarget = "trade_profitTarget",
  /** column name */
  TradeSalt = "trade_salt",
  /** column name */
  TradeSlippage = "trade_slippage",
  /** column name */
  TradeStopLoss = "trade_stopLoss",
  /** column name */
  TradeUser = "trade_user",
  /** column name */
  User = "user",
}

/** aggregate stddev on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Stddev_Fields =
  {
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent_stddev_fields"
    afterCloseTrade_fees_fee?: Maybe<Scalars["Float"]>
    afterCloseTrade_fees_referralFee?: Maybe<Scalars["Float"]>
    afterCloseTrade_fees_referredFee?: Maybe<Scalars["Float"]>
    afterCloseTrade_liquidationFee?: Maybe<Scalars["Float"]>
    afterCloseTrade_oraclePrice?: Maybe<Scalars["Float"]>
    afterCloseTrade_settled?: Maybe<Scalars["Float"]>
    closePercent?: Maybe<Scalars["Float"]>
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    onCloseTrade_closeNet?: Maybe<Scalars["Float"]>
    onCloseTrade_fundingFee?: Maybe<Scalars["Float"]>
    onCloseTrade_grossPnL?: Maybe<Scalars["Float"]>
    onCloseTrade_rolloverFee?: Maybe<Scalars["Float"]>
    onCloseTrade_slippage?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_balance?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBase?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** aggregate stddev_pop on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Stddev_Pop_Fields =
  {
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent_stddev_pop_fields"
    afterCloseTrade_fees_fee?: Maybe<Scalars["Float"]>
    afterCloseTrade_fees_referralFee?: Maybe<Scalars["Float"]>
    afterCloseTrade_fees_referredFee?: Maybe<Scalars["Float"]>
    afterCloseTrade_liquidationFee?: Maybe<Scalars["Float"]>
    afterCloseTrade_oraclePrice?: Maybe<Scalars["Float"]>
    afterCloseTrade_settled?: Maybe<Scalars["Float"]>
    closePercent?: Maybe<Scalars["Float"]>
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    onCloseTrade_closeNet?: Maybe<Scalars["Float"]>
    onCloseTrade_fundingFee?: Maybe<Scalars["Float"]>
    onCloseTrade_grossPnL?: Maybe<Scalars["Float"]>
    onCloseTrade_rolloverFee?: Maybe<Scalars["Float"]>
    onCloseTrade_slippage?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_balance?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBase?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** aggregate stddev_samp on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Stddev_Samp_Fields =
  {
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent_stddev_samp_fields"
    afterCloseTrade_fees_fee?: Maybe<Scalars["Float"]>
    afterCloseTrade_fees_referralFee?: Maybe<Scalars["Float"]>
    afterCloseTrade_fees_referredFee?: Maybe<Scalars["Float"]>
    afterCloseTrade_liquidationFee?: Maybe<Scalars["Float"]>
    afterCloseTrade_oraclePrice?: Maybe<Scalars["Float"]>
    afterCloseTrade_settled?: Maybe<Scalars["Float"]>
    closePercent?: Maybe<Scalars["Float"]>
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    onCloseTrade_closeNet?: Maybe<Scalars["Float"]>
    onCloseTrade_fundingFee?: Maybe<Scalars["Float"]>
    onCloseTrade_grossPnL?: Maybe<Scalars["Float"]>
    onCloseTrade_rolloverFee?: Maybe<Scalars["Float"]>
    onCloseTrade_slippage?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_balance?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBase?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** Streaming cursor of the table "uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent" */
export type Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Stream_Cursor_Input =
  {
    /** Stream column input with initial value */
    initial_value: Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Stream_Cursor_Value_Input
    /** cursor ordering */
    ordering?: InputMaybe<Cursor_Ordering>
  }

/** Initial value of the column from where the streaming should start */
export type Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Stream_Cursor_Value_Input =
  {
    afterCloseTrade_fees_fee?: InputMaybe<Scalars["numeric"]>
    afterCloseTrade_fees_referralCode?: InputMaybe<Scalars["bytea"]>
    afterCloseTrade_fees_referralFee?: InputMaybe<Scalars["numeric"]>
    afterCloseTrade_fees_referredFee?: InputMaybe<Scalars["numeric"]>
    afterCloseTrade_fees_referrer?: InputMaybe<Scalars["bytea"]>
    afterCloseTrade_liquidationFee?: InputMaybe<Scalars["numeric"]>
    afterCloseTrade_oraclePrice?: InputMaybe<Scalars["numeric"]>
    afterCloseTrade_settled?: InputMaybe<Scalars["numeric"]>
    closePercent?: InputMaybe<Scalars["numeric"]>
    contract_address?: InputMaybe<Scalars["bytea"]>
    evt_block_number?: InputMaybe<Scalars["bigint"]>
    evt_block_time?: InputMaybe<Scalars["timestamptz"]>
    evt_index?: InputMaybe<Scalars["numeric"]>
    evt_tx_hash?: InputMaybe<Scalars["bytea"]>
    evt_tx_index?: InputMaybe<Scalars["numeric"]>
    onCloseTrade_closeNet?: InputMaybe<Scalars["numeric"]>
    onCloseTrade_fundingFee?: InputMaybe<Scalars["numeric"]>
    onCloseTrade_grossPnL?: InputMaybe<Scalars["numeric"]>
    onCloseTrade_isLiquidated?: InputMaybe<Scalars["Boolean"]>
    onCloseTrade_isStop?: InputMaybe<Scalars["Boolean"]>
    onCloseTrade_rolloverFee?: InputMaybe<Scalars["numeric"]>
    onCloseTrade_slippage?: InputMaybe<Scalars["numeric"]>
    onOrderUpdate_accruedFee_fundingFee?: InputMaybe<Scalars["numeric"]>
    onOrderUpdate_accruedFee_lastUpdate?: InputMaybe<Scalars["numeric"]>
    onOrderUpdate_accruedFee_rolloverFee?: InputMaybe<Scalars["numeric"]>
    onOrderUpdate_feeBalance_balance?: InputMaybe<Scalars["numeric"]>
    onOrderUpdate_feeBalance_lastUpdate?: InputMaybe<Scalars["numeric"]>
    onOrderUpdate_feeBase?: InputMaybe<Scalars["numeric"]>
    orderHash?: InputMaybe<Scalars["bytea"]>
    trade_executionBlock?: InputMaybe<Scalars["numeric"]>
    trade_executionTime?: InputMaybe<Scalars["numeric"]>
    trade_isBuy?: InputMaybe<Scalars["Boolean"]>
    trade_leverage?: InputMaybe<Scalars["numeric"]>
    trade_liquidationPrice?: InputMaybe<Scalars["numeric"]>
    trade_margin?: InputMaybe<Scalars["numeric"]>
    trade_maxPercentagePnL?: InputMaybe<Scalars["numeric"]>
    trade_openPrice?: InputMaybe<Scalars["numeric"]>
    trade_priceId?: InputMaybe<Scalars["bytea"]>
    trade_profitTarget?: InputMaybe<Scalars["numeric"]>
    trade_salt?: InputMaybe<Scalars["numeric"]>
    trade_slippage?: InputMaybe<Scalars["numeric"]>
    trade_stopLoss?: InputMaybe<Scalars["numeric"]>
    trade_user?: InputMaybe<Scalars["bytea"]>
    user?: InputMaybe<Scalars["bytea"]>
  }

/** aggregate sum on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Sum_Fields = {
  __typename?: "uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent_sum_fields"
  afterCloseTrade_fees_fee?: Maybe<Scalars["numeric"]>
  afterCloseTrade_fees_referralFee?: Maybe<Scalars["numeric"]>
  afterCloseTrade_fees_referredFee?: Maybe<Scalars["numeric"]>
  afterCloseTrade_liquidationFee?: Maybe<Scalars["numeric"]>
  afterCloseTrade_oraclePrice?: Maybe<Scalars["numeric"]>
  afterCloseTrade_settled?: Maybe<Scalars["numeric"]>
  closePercent?: Maybe<Scalars["numeric"]>
  evt_block_number?: Maybe<Scalars["bigint"]>
  evt_index?: Maybe<Scalars["numeric"]>
  evt_tx_index?: Maybe<Scalars["numeric"]>
  onCloseTrade_closeNet?: Maybe<Scalars["numeric"]>
  onCloseTrade_fundingFee?: Maybe<Scalars["numeric"]>
  onCloseTrade_grossPnL?: Maybe<Scalars["numeric"]>
  onCloseTrade_rolloverFee?: Maybe<Scalars["numeric"]>
  onCloseTrade_slippage?: Maybe<Scalars["numeric"]>
  onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["numeric"]>
  onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["numeric"]>
  onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["numeric"]>
  onOrderUpdate_feeBalance_balance?: Maybe<Scalars["numeric"]>
  onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["numeric"]>
  onOrderUpdate_feeBase?: Maybe<Scalars["numeric"]>
  trade_executionBlock?: Maybe<Scalars["numeric"]>
  trade_executionTime?: Maybe<Scalars["numeric"]>
  trade_leverage?: Maybe<Scalars["numeric"]>
  trade_liquidationPrice?: Maybe<Scalars["numeric"]>
  trade_margin?: Maybe<Scalars["numeric"]>
  trade_maxPercentagePnL?: Maybe<Scalars["numeric"]>
  trade_openPrice?: Maybe<Scalars["numeric"]>
  trade_profitTarget?: Maybe<Scalars["numeric"]>
  trade_salt?: Maybe<Scalars["numeric"]>
  trade_slippage?: Maybe<Scalars["numeric"]>
  trade_stopLoss?: Maybe<Scalars["numeric"]>
}

/** aggregate var_pop on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Var_Pop_Fields =
  {
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent_var_pop_fields"
    afterCloseTrade_fees_fee?: Maybe<Scalars["Float"]>
    afterCloseTrade_fees_referralFee?: Maybe<Scalars["Float"]>
    afterCloseTrade_fees_referredFee?: Maybe<Scalars["Float"]>
    afterCloseTrade_liquidationFee?: Maybe<Scalars["Float"]>
    afterCloseTrade_oraclePrice?: Maybe<Scalars["Float"]>
    afterCloseTrade_settled?: Maybe<Scalars["Float"]>
    closePercent?: Maybe<Scalars["Float"]>
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    onCloseTrade_closeNet?: Maybe<Scalars["Float"]>
    onCloseTrade_fundingFee?: Maybe<Scalars["Float"]>
    onCloseTrade_grossPnL?: Maybe<Scalars["Float"]>
    onCloseTrade_rolloverFee?: Maybe<Scalars["Float"]>
    onCloseTrade_slippage?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_balance?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBase?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** aggregate var_samp on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Var_Samp_Fields =
  {
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent_var_samp_fields"
    afterCloseTrade_fees_fee?: Maybe<Scalars["Float"]>
    afterCloseTrade_fees_referralFee?: Maybe<Scalars["Float"]>
    afterCloseTrade_fees_referredFee?: Maybe<Scalars["Float"]>
    afterCloseTrade_liquidationFee?: Maybe<Scalars["Float"]>
    afterCloseTrade_oraclePrice?: Maybe<Scalars["Float"]>
    afterCloseTrade_settled?: Maybe<Scalars["Float"]>
    closePercent?: Maybe<Scalars["Float"]>
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    onCloseTrade_closeNet?: Maybe<Scalars["Float"]>
    onCloseTrade_fundingFee?: Maybe<Scalars["Float"]>
    onCloseTrade_grossPnL?: Maybe<Scalars["Float"]>
    onCloseTrade_rolloverFee?: Maybe<Scalars["Float"]>
    onCloseTrade_slippage?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_balance?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBase?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** aggregate variance on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Variance_Fields =
  {
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent_variance_fields"
    afterCloseTrade_fees_fee?: Maybe<Scalars["Float"]>
    afterCloseTrade_fees_referralFee?: Maybe<Scalars["Float"]>
    afterCloseTrade_fees_referredFee?: Maybe<Scalars["Float"]>
    afterCloseTrade_liquidationFee?: Maybe<Scalars["Float"]>
    afterCloseTrade_oraclePrice?: Maybe<Scalars["Float"]>
    afterCloseTrade_settled?: Maybe<Scalars["Float"]>
    closePercent?: Maybe<Scalars["Float"]>
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    onCloseTrade_closeNet?: Maybe<Scalars["Float"]>
    onCloseTrade_fundingFee?: Maybe<Scalars["Float"]>
    onCloseTrade_grossPnL?: Maybe<Scalars["Float"]>
    onCloseTrade_rolloverFee?: Maybe<Scalars["Float"]>
    onCloseTrade_slippage?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_balance?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBase?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** columns and relationships of "uniwhale_v1_bsc.TradingCore_evt_OpenMarketOrderEvent" */
export type Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent = {
  __typename?: "uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent"
  contract_address: Scalars["bytea"]
  evt_block_number: Scalars["bigint"]
  evt_block_time: Scalars["timestamptz"]
  evt_index: Scalars["numeric"]
  evt_tx_hash: Scalars["bytea"]
  evt_tx_index: Scalars["numeric"]
  fee_fee: Scalars["numeric"]
  fee_referralCode: Scalars["bytea"]
  fee_referralFee: Scalars["numeric"]
  fee_referredFee: Scalars["numeric"]
  fee_referrer: Scalars["bytea"]
  onOrderUpdate_accruedFee_fundingFee: Scalars["numeric"]
  onOrderUpdate_accruedFee_lastUpdate: Scalars["numeric"]
  onOrderUpdate_accruedFee_rolloverFee: Scalars["numeric"]
  onOrderUpdate_feeBalance_balance: Scalars["numeric"]
  onOrderUpdate_feeBalance_lastUpdate: Scalars["numeric"]
  onOrderUpdate_feeBase: Scalars["numeric"]
  orderHash: Scalars["bytea"]
  trade_executionBlock: Scalars["numeric"]
  trade_executionTime: Scalars["numeric"]
  trade_isBuy: Scalars["Boolean"]
  trade_leverage: Scalars["numeric"]
  trade_liquidationPrice: Scalars["numeric"]
  trade_margin: Scalars["numeric"]
  trade_maxPercentagePnL: Scalars["numeric"]
  trade_openPrice: Scalars["numeric"]
  trade_priceId: Scalars["bytea"]
  trade_profitTarget: Scalars["numeric"]
  trade_salt: Scalars["numeric"]
  trade_slippage: Scalars["numeric"]
  trade_stopLoss: Scalars["numeric"]
  trade_user: Scalars["bytea"]
  user: Scalars["bytea"]
}

/** aggregated selection of "uniwhale_v1_bsc.TradingCore_evt_OpenMarketOrderEvent" */
export type Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Aggregate = {
  __typename?: "uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent_aggregate"
  aggregate?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Aggregate_Fields>
  nodes: Array<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent>
}

/** aggregate fields of "uniwhale_v1_bsc.TradingCore_evt_OpenMarketOrderEvent" */
export type Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Aggregate_Fields =
  {
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent_aggregate_fields"
    avg?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Avg_Fields>
    count: Scalars["Int"]
    max?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Max_Fields>
    min?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Min_Fields>
    stddev?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Stddev_Fields>
    stddev_pop?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Stddev_Pop_Fields>
    stddev_samp?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Stddev_Samp_Fields>
    sum?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Sum_Fields>
    var_pop?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Var_Pop_Fields>
    var_samp?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Var_Samp_Fields>
    variance?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Variance_Fields>
  }

/** aggregate fields of "uniwhale_v1_bsc.TradingCore_evt_OpenMarketOrderEvent" */
export type Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Aggregate_FieldsCountArgs =
  {
    columns?: InputMaybe<
      Array<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Select_Column>
    >
    distinct?: InputMaybe<Scalars["Boolean"]>
  }

/** aggregate avg on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Avg_Fields = {
  __typename?: "uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent_avg_fields"
  evt_block_number?: Maybe<Scalars["Float"]>
  evt_index?: Maybe<Scalars["Float"]>
  evt_tx_index?: Maybe<Scalars["Float"]>
  fee_fee?: Maybe<Scalars["Float"]>
  fee_referralFee?: Maybe<Scalars["Float"]>
  fee_referredFee?: Maybe<Scalars["Float"]>
  onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["Float"]>
  onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["Float"]>
  onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["Float"]>
  onOrderUpdate_feeBalance_balance?: Maybe<Scalars["Float"]>
  onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["Float"]>
  onOrderUpdate_feeBase?: Maybe<Scalars["Float"]>
  trade_executionBlock?: Maybe<Scalars["Float"]>
  trade_executionTime?: Maybe<Scalars["Float"]>
  trade_leverage?: Maybe<Scalars["Float"]>
  trade_liquidationPrice?: Maybe<Scalars["Float"]>
  trade_margin?: Maybe<Scalars["Float"]>
  trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
  trade_openPrice?: Maybe<Scalars["Float"]>
  trade_profitTarget?: Maybe<Scalars["Float"]>
  trade_salt?: Maybe<Scalars["Float"]>
  trade_slippage?: Maybe<Scalars["Float"]>
  trade_stopLoss?: Maybe<Scalars["Float"]>
}

/** Boolean expression to filter rows from the table "uniwhale_v1_bsc.TradingCore_evt_OpenMarketOrderEvent". All fields are combined with a logical 'AND'. */
export type Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Bool_Exp = {
  _and?: InputMaybe<
    Array<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Bool_Exp>
  >
  _not?: InputMaybe<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Bool_Exp>
  _or?: InputMaybe<
    Array<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Bool_Exp>
  >
  contract_address?: InputMaybe<Bytea_Comparison_Exp>
  evt_block_number?: InputMaybe<Bigint_Comparison_Exp>
  evt_block_time?: InputMaybe<Timestamptz_Comparison_Exp>
  evt_index?: InputMaybe<Numeric_Comparison_Exp>
  evt_tx_hash?: InputMaybe<Bytea_Comparison_Exp>
  evt_tx_index?: InputMaybe<Numeric_Comparison_Exp>
  fee_fee?: InputMaybe<Numeric_Comparison_Exp>
  fee_referralCode?: InputMaybe<Bytea_Comparison_Exp>
  fee_referralFee?: InputMaybe<Numeric_Comparison_Exp>
  fee_referredFee?: InputMaybe<Numeric_Comparison_Exp>
  fee_referrer?: InputMaybe<Bytea_Comparison_Exp>
  onOrderUpdate_accruedFee_fundingFee?: InputMaybe<Numeric_Comparison_Exp>
  onOrderUpdate_accruedFee_lastUpdate?: InputMaybe<Numeric_Comparison_Exp>
  onOrderUpdate_accruedFee_rolloverFee?: InputMaybe<Numeric_Comparison_Exp>
  onOrderUpdate_feeBalance_balance?: InputMaybe<Numeric_Comparison_Exp>
  onOrderUpdate_feeBalance_lastUpdate?: InputMaybe<Numeric_Comparison_Exp>
  onOrderUpdate_feeBase?: InputMaybe<Numeric_Comparison_Exp>
  orderHash?: InputMaybe<Bytea_Comparison_Exp>
  trade_executionBlock?: InputMaybe<Numeric_Comparison_Exp>
  trade_executionTime?: InputMaybe<Numeric_Comparison_Exp>
  trade_isBuy?: InputMaybe<Boolean_Comparison_Exp>
  trade_leverage?: InputMaybe<Numeric_Comparison_Exp>
  trade_liquidationPrice?: InputMaybe<Numeric_Comparison_Exp>
  trade_margin?: InputMaybe<Numeric_Comparison_Exp>
  trade_maxPercentagePnL?: InputMaybe<Numeric_Comparison_Exp>
  trade_openPrice?: InputMaybe<Numeric_Comparison_Exp>
  trade_priceId?: InputMaybe<Bytea_Comparison_Exp>
  trade_profitTarget?: InputMaybe<Numeric_Comparison_Exp>
  trade_salt?: InputMaybe<Numeric_Comparison_Exp>
  trade_slippage?: InputMaybe<Numeric_Comparison_Exp>
  trade_stopLoss?: InputMaybe<Numeric_Comparison_Exp>
  trade_user?: InputMaybe<Bytea_Comparison_Exp>
  user?: InputMaybe<Bytea_Comparison_Exp>
}

/** aggregate max on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Max_Fields = {
  __typename?: "uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent_max_fields"
  evt_block_number?: Maybe<Scalars["bigint"]>
  evt_block_time?: Maybe<Scalars["timestamptz"]>
  evt_index?: Maybe<Scalars["numeric"]>
  evt_tx_index?: Maybe<Scalars["numeric"]>
  fee_fee?: Maybe<Scalars["numeric"]>
  fee_referralFee?: Maybe<Scalars["numeric"]>
  fee_referredFee?: Maybe<Scalars["numeric"]>
  onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["numeric"]>
  onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["numeric"]>
  onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["numeric"]>
  onOrderUpdate_feeBalance_balance?: Maybe<Scalars["numeric"]>
  onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["numeric"]>
  onOrderUpdate_feeBase?: Maybe<Scalars["numeric"]>
  trade_executionBlock?: Maybe<Scalars["numeric"]>
  trade_executionTime?: Maybe<Scalars["numeric"]>
  trade_leverage?: Maybe<Scalars["numeric"]>
  trade_liquidationPrice?: Maybe<Scalars["numeric"]>
  trade_margin?: Maybe<Scalars["numeric"]>
  trade_maxPercentagePnL?: Maybe<Scalars["numeric"]>
  trade_openPrice?: Maybe<Scalars["numeric"]>
  trade_profitTarget?: Maybe<Scalars["numeric"]>
  trade_salt?: Maybe<Scalars["numeric"]>
  trade_slippage?: Maybe<Scalars["numeric"]>
  trade_stopLoss?: Maybe<Scalars["numeric"]>
}

/** aggregate min on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Min_Fields = {
  __typename?: "uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent_min_fields"
  evt_block_number?: Maybe<Scalars["bigint"]>
  evt_block_time?: Maybe<Scalars["timestamptz"]>
  evt_index?: Maybe<Scalars["numeric"]>
  evt_tx_index?: Maybe<Scalars["numeric"]>
  fee_fee?: Maybe<Scalars["numeric"]>
  fee_referralFee?: Maybe<Scalars["numeric"]>
  fee_referredFee?: Maybe<Scalars["numeric"]>
  onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["numeric"]>
  onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["numeric"]>
  onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["numeric"]>
  onOrderUpdate_feeBalance_balance?: Maybe<Scalars["numeric"]>
  onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["numeric"]>
  onOrderUpdate_feeBase?: Maybe<Scalars["numeric"]>
  trade_executionBlock?: Maybe<Scalars["numeric"]>
  trade_executionTime?: Maybe<Scalars["numeric"]>
  trade_leverage?: Maybe<Scalars["numeric"]>
  trade_liquidationPrice?: Maybe<Scalars["numeric"]>
  trade_margin?: Maybe<Scalars["numeric"]>
  trade_maxPercentagePnL?: Maybe<Scalars["numeric"]>
  trade_openPrice?: Maybe<Scalars["numeric"]>
  trade_profitTarget?: Maybe<Scalars["numeric"]>
  trade_salt?: Maybe<Scalars["numeric"]>
  trade_slippage?: Maybe<Scalars["numeric"]>
  trade_stopLoss?: Maybe<Scalars["numeric"]>
}

/** Ordering options when selecting data from "uniwhale_v1_bsc.TradingCore_evt_OpenMarketOrderEvent". */
export type Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Order_By = {
  contract_address?: InputMaybe<Order_By>
  evt_block_number?: InputMaybe<Order_By>
  evt_block_time?: InputMaybe<Order_By>
  evt_index?: InputMaybe<Order_By>
  evt_tx_hash?: InputMaybe<Order_By>
  evt_tx_index?: InputMaybe<Order_By>
  fee_fee?: InputMaybe<Order_By>
  fee_referralCode?: InputMaybe<Order_By>
  fee_referralFee?: InputMaybe<Order_By>
  fee_referredFee?: InputMaybe<Order_By>
  fee_referrer?: InputMaybe<Order_By>
  onOrderUpdate_accruedFee_fundingFee?: InputMaybe<Order_By>
  onOrderUpdate_accruedFee_lastUpdate?: InputMaybe<Order_By>
  onOrderUpdate_accruedFee_rolloverFee?: InputMaybe<Order_By>
  onOrderUpdate_feeBalance_balance?: InputMaybe<Order_By>
  onOrderUpdate_feeBalance_lastUpdate?: InputMaybe<Order_By>
  onOrderUpdate_feeBase?: InputMaybe<Order_By>
  orderHash?: InputMaybe<Order_By>
  trade_executionBlock?: InputMaybe<Order_By>
  trade_executionTime?: InputMaybe<Order_By>
  trade_isBuy?: InputMaybe<Order_By>
  trade_leverage?: InputMaybe<Order_By>
  trade_liquidationPrice?: InputMaybe<Order_By>
  trade_margin?: InputMaybe<Order_By>
  trade_maxPercentagePnL?: InputMaybe<Order_By>
  trade_openPrice?: InputMaybe<Order_By>
  trade_priceId?: InputMaybe<Order_By>
  trade_profitTarget?: InputMaybe<Order_By>
  trade_salt?: InputMaybe<Order_By>
  trade_slippage?: InputMaybe<Order_By>
  trade_stopLoss?: InputMaybe<Order_By>
  trade_user?: InputMaybe<Order_By>
  user?: InputMaybe<Order_By>
}

/** select columns of table "uniwhale_v1_bsc.TradingCore_evt_OpenMarketOrderEvent" */
export enum Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Select_Column {
  /** column name */
  ContractAddress = "contract_address",
  /** column name */
  EvtBlockNumber = "evt_block_number",
  /** column name */
  EvtBlockTime = "evt_block_time",
  /** column name */
  EvtIndex = "evt_index",
  /** column name */
  EvtTxHash = "evt_tx_hash",
  /** column name */
  EvtTxIndex = "evt_tx_index",
  /** column name */
  FeeFee = "fee_fee",
  /** column name */
  FeeReferralCode = "fee_referralCode",
  /** column name */
  FeeReferralFee = "fee_referralFee",
  /** column name */
  FeeReferredFee = "fee_referredFee",
  /** column name */
  FeeReferrer = "fee_referrer",
  /** column name */
  OnOrderUpdateAccruedFeeFundingFee = "onOrderUpdate_accruedFee_fundingFee",
  /** column name */
  OnOrderUpdateAccruedFeeLastUpdate = "onOrderUpdate_accruedFee_lastUpdate",
  /** column name */
  OnOrderUpdateAccruedFeeRolloverFee = "onOrderUpdate_accruedFee_rolloverFee",
  /** column name */
  OnOrderUpdateFeeBalanceBalance = "onOrderUpdate_feeBalance_balance",
  /** column name */
  OnOrderUpdateFeeBalanceLastUpdate = "onOrderUpdate_feeBalance_lastUpdate",
  /** column name */
  OnOrderUpdateFeeBase = "onOrderUpdate_feeBase",
  /** column name */
  OrderHash = "orderHash",
  /** column name */
  TradeExecutionBlock = "trade_executionBlock",
  /** column name */
  TradeExecutionTime = "trade_executionTime",
  /** column name */
  TradeIsBuy = "trade_isBuy",
  /** column name */
  TradeLeverage = "trade_leverage",
  /** column name */
  TradeLiquidationPrice = "trade_liquidationPrice",
  /** column name */
  TradeMargin = "trade_margin",
  /** column name */
  TradeMaxPercentagePnL = "trade_maxPercentagePnL",
  /** column name */
  TradeOpenPrice = "trade_openPrice",
  /** column name */
  TradePriceId = "trade_priceId",
  /** column name */
  TradeProfitTarget = "trade_profitTarget",
  /** column name */
  TradeSalt = "trade_salt",
  /** column name */
  TradeSlippage = "trade_slippage",
  /** column name */
  TradeStopLoss = "trade_stopLoss",
  /** column name */
  TradeUser = "trade_user",
  /** column name */
  User = "user",
}

/** aggregate stddev on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Stddev_Fields =
  {
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent_stddev_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    fee_fee?: Maybe<Scalars["Float"]>
    fee_referralFee?: Maybe<Scalars["Float"]>
    fee_referredFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_balance?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBase?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** aggregate stddev_pop on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Stddev_Pop_Fields =
  {
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent_stddev_pop_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    fee_fee?: Maybe<Scalars["Float"]>
    fee_referralFee?: Maybe<Scalars["Float"]>
    fee_referredFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_balance?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBase?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** aggregate stddev_samp on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Stddev_Samp_Fields =
  {
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent_stddev_samp_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    fee_fee?: Maybe<Scalars["Float"]>
    fee_referralFee?: Maybe<Scalars["Float"]>
    fee_referredFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_balance?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBase?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** Streaming cursor of the table "uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent" */
export type Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Stream_Cursor_Input =
  {
    /** Stream column input with initial value */
    initial_value: Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Stream_Cursor_Value_Input
    /** cursor ordering */
    ordering?: InputMaybe<Cursor_Ordering>
  }

/** Initial value of the column from where the streaming should start */
export type Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Stream_Cursor_Value_Input =
  {
    contract_address?: InputMaybe<Scalars["bytea"]>
    evt_block_number?: InputMaybe<Scalars["bigint"]>
    evt_block_time?: InputMaybe<Scalars["timestamptz"]>
    evt_index?: InputMaybe<Scalars["numeric"]>
    evt_tx_hash?: InputMaybe<Scalars["bytea"]>
    evt_tx_index?: InputMaybe<Scalars["numeric"]>
    fee_fee?: InputMaybe<Scalars["numeric"]>
    fee_referralCode?: InputMaybe<Scalars["bytea"]>
    fee_referralFee?: InputMaybe<Scalars["numeric"]>
    fee_referredFee?: InputMaybe<Scalars["numeric"]>
    fee_referrer?: InputMaybe<Scalars["bytea"]>
    onOrderUpdate_accruedFee_fundingFee?: InputMaybe<Scalars["numeric"]>
    onOrderUpdate_accruedFee_lastUpdate?: InputMaybe<Scalars["numeric"]>
    onOrderUpdate_accruedFee_rolloverFee?: InputMaybe<Scalars["numeric"]>
    onOrderUpdate_feeBalance_balance?: InputMaybe<Scalars["numeric"]>
    onOrderUpdate_feeBalance_lastUpdate?: InputMaybe<Scalars["numeric"]>
    onOrderUpdate_feeBase?: InputMaybe<Scalars["numeric"]>
    orderHash?: InputMaybe<Scalars["bytea"]>
    trade_executionBlock?: InputMaybe<Scalars["numeric"]>
    trade_executionTime?: InputMaybe<Scalars["numeric"]>
    trade_isBuy?: InputMaybe<Scalars["Boolean"]>
    trade_leverage?: InputMaybe<Scalars["numeric"]>
    trade_liquidationPrice?: InputMaybe<Scalars["numeric"]>
    trade_margin?: InputMaybe<Scalars["numeric"]>
    trade_maxPercentagePnL?: InputMaybe<Scalars["numeric"]>
    trade_openPrice?: InputMaybe<Scalars["numeric"]>
    trade_priceId?: InputMaybe<Scalars["bytea"]>
    trade_profitTarget?: InputMaybe<Scalars["numeric"]>
    trade_salt?: InputMaybe<Scalars["numeric"]>
    trade_slippage?: InputMaybe<Scalars["numeric"]>
    trade_stopLoss?: InputMaybe<Scalars["numeric"]>
    trade_user?: InputMaybe<Scalars["bytea"]>
    user?: InputMaybe<Scalars["bytea"]>
  }

/** aggregate sum on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Sum_Fields = {
  __typename?: "uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent_sum_fields"
  evt_block_number?: Maybe<Scalars["bigint"]>
  evt_index?: Maybe<Scalars["numeric"]>
  evt_tx_index?: Maybe<Scalars["numeric"]>
  fee_fee?: Maybe<Scalars["numeric"]>
  fee_referralFee?: Maybe<Scalars["numeric"]>
  fee_referredFee?: Maybe<Scalars["numeric"]>
  onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["numeric"]>
  onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["numeric"]>
  onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["numeric"]>
  onOrderUpdate_feeBalance_balance?: Maybe<Scalars["numeric"]>
  onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["numeric"]>
  onOrderUpdate_feeBase?: Maybe<Scalars["numeric"]>
  trade_executionBlock?: Maybe<Scalars["numeric"]>
  trade_executionTime?: Maybe<Scalars["numeric"]>
  trade_leverage?: Maybe<Scalars["numeric"]>
  trade_liquidationPrice?: Maybe<Scalars["numeric"]>
  trade_margin?: Maybe<Scalars["numeric"]>
  trade_maxPercentagePnL?: Maybe<Scalars["numeric"]>
  trade_openPrice?: Maybe<Scalars["numeric"]>
  trade_profitTarget?: Maybe<Scalars["numeric"]>
  trade_salt?: Maybe<Scalars["numeric"]>
  trade_slippage?: Maybe<Scalars["numeric"]>
  trade_stopLoss?: Maybe<Scalars["numeric"]>
}

/** aggregate var_pop on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Var_Pop_Fields =
  {
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent_var_pop_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    fee_fee?: Maybe<Scalars["Float"]>
    fee_referralFee?: Maybe<Scalars["Float"]>
    fee_referredFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_balance?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBase?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** aggregate var_samp on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Var_Samp_Fields =
  {
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent_var_samp_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    fee_fee?: Maybe<Scalars["Float"]>
    fee_referralFee?: Maybe<Scalars["Float"]>
    fee_referredFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_balance?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBase?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** aggregate variance on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Variance_Fields =
  {
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent_variance_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    fee_fee?: Maybe<Scalars["Float"]>
    fee_referralFee?: Maybe<Scalars["Float"]>
    fee_referredFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_balance?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBase?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** columns and relationships of "uniwhale_v1_bsc.TradingCore_evt_UpdateOpenOrderEvent" */
export type Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent = {
  __typename?: "uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent"
  contract_address: Scalars["bytea"]
  evt_block_number: Scalars["bigint"]
  evt_block_time: Scalars["timestamptz"]
  evt_index: Scalars["numeric"]
  evt_tx_hash: Scalars["bytea"]
  evt_tx_index: Scalars["numeric"]
  onOrderUpdate_accruedFee_fundingFee: Scalars["numeric"]
  onOrderUpdate_accruedFee_lastUpdate: Scalars["numeric"]
  onOrderUpdate_accruedFee_rolloverFee: Scalars["numeric"]
  onOrderUpdate_feeBalance_balance: Scalars["numeric"]
  onOrderUpdate_feeBalance_lastUpdate: Scalars["numeric"]
  onOrderUpdate_feeBase: Scalars["numeric"]
  onUpdateTrade_isAdding: Scalars["Boolean"]
  onUpdateTrade_marginDelta: Scalars["numeric"]
  orderHash: Scalars["bytea"]
  trade_executionBlock: Scalars["numeric"]
  trade_executionTime: Scalars["numeric"]
  trade_isBuy: Scalars["Boolean"]
  trade_leverage: Scalars["numeric"]
  trade_liquidationPrice: Scalars["numeric"]
  trade_margin: Scalars["numeric"]
  trade_maxPercentagePnL: Scalars["numeric"]
  trade_openPrice: Scalars["numeric"]
  trade_priceId: Scalars["bytea"]
  trade_profitTarget: Scalars["numeric"]
  trade_salt: Scalars["numeric"]
  trade_slippage: Scalars["numeric"]
  trade_stopLoss: Scalars["numeric"]
  trade_user: Scalars["bytea"]
  user: Scalars["bytea"]
}

/** aggregated selection of "uniwhale_v1_bsc.TradingCore_evt_UpdateOpenOrderEvent" */
export type Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Aggregate = {
  __typename?: "uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent_aggregate"
  aggregate?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Aggregate_Fields>
  nodes: Array<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent>
}

/** aggregate fields of "uniwhale_v1_bsc.TradingCore_evt_UpdateOpenOrderEvent" */
export type Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Aggregate_Fields =
  {
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent_aggregate_fields"
    avg?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Avg_Fields>
    count: Scalars["Int"]
    max?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Max_Fields>
    min?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Min_Fields>
    stddev?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Stddev_Fields>
    stddev_pop?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Stddev_Pop_Fields>
    stddev_samp?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Stddev_Samp_Fields>
    sum?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Sum_Fields>
    var_pop?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Var_Pop_Fields>
    var_samp?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Var_Samp_Fields>
    variance?: Maybe<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Variance_Fields>
  }

/** aggregate fields of "uniwhale_v1_bsc.TradingCore_evt_UpdateOpenOrderEvent" */
export type Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Aggregate_FieldsCountArgs =
  {
    columns?: InputMaybe<
      Array<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Select_Column>
    >
    distinct?: InputMaybe<Scalars["Boolean"]>
  }

/** aggregate avg on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Avg_Fields = {
  __typename?: "uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent_avg_fields"
  evt_block_number?: Maybe<Scalars["Float"]>
  evt_index?: Maybe<Scalars["Float"]>
  evt_tx_index?: Maybe<Scalars["Float"]>
  onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["Float"]>
  onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["Float"]>
  onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["Float"]>
  onOrderUpdate_feeBalance_balance?: Maybe<Scalars["Float"]>
  onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["Float"]>
  onOrderUpdate_feeBase?: Maybe<Scalars["Float"]>
  onUpdateTrade_marginDelta?: Maybe<Scalars["Float"]>
  trade_executionBlock?: Maybe<Scalars["Float"]>
  trade_executionTime?: Maybe<Scalars["Float"]>
  trade_leverage?: Maybe<Scalars["Float"]>
  trade_liquidationPrice?: Maybe<Scalars["Float"]>
  trade_margin?: Maybe<Scalars["Float"]>
  trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
  trade_openPrice?: Maybe<Scalars["Float"]>
  trade_profitTarget?: Maybe<Scalars["Float"]>
  trade_salt?: Maybe<Scalars["Float"]>
  trade_slippage?: Maybe<Scalars["Float"]>
  trade_stopLoss?: Maybe<Scalars["Float"]>
}

/** Boolean expression to filter rows from the table "uniwhale_v1_bsc.TradingCore_evt_UpdateOpenOrderEvent". All fields are combined with a logical 'AND'. */
export type Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Bool_Exp = {
  _and?: InputMaybe<
    Array<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Bool_Exp>
  >
  _not?: InputMaybe<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Bool_Exp>
  _or?: InputMaybe<
    Array<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Bool_Exp>
  >
  contract_address?: InputMaybe<Bytea_Comparison_Exp>
  evt_block_number?: InputMaybe<Bigint_Comparison_Exp>
  evt_block_time?: InputMaybe<Timestamptz_Comparison_Exp>
  evt_index?: InputMaybe<Numeric_Comparison_Exp>
  evt_tx_hash?: InputMaybe<Bytea_Comparison_Exp>
  evt_tx_index?: InputMaybe<Numeric_Comparison_Exp>
  onOrderUpdate_accruedFee_fundingFee?: InputMaybe<Numeric_Comparison_Exp>
  onOrderUpdate_accruedFee_lastUpdate?: InputMaybe<Numeric_Comparison_Exp>
  onOrderUpdate_accruedFee_rolloverFee?: InputMaybe<Numeric_Comparison_Exp>
  onOrderUpdate_feeBalance_balance?: InputMaybe<Numeric_Comparison_Exp>
  onOrderUpdate_feeBalance_lastUpdate?: InputMaybe<Numeric_Comparison_Exp>
  onOrderUpdate_feeBase?: InputMaybe<Numeric_Comparison_Exp>
  onUpdateTrade_isAdding?: InputMaybe<Boolean_Comparison_Exp>
  onUpdateTrade_marginDelta?: InputMaybe<Numeric_Comparison_Exp>
  orderHash?: InputMaybe<Bytea_Comparison_Exp>
  trade_executionBlock?: InputMaybe<Numeric_Comparison_Exp>
  trade_executionTime?: InputMaybe<Numeric_Comparison_Exp>
  trade_isBuy?: InputMaybe<Boolean_Comparison_Exp>
  trade_leverage?: InputMaybe<Numeric_Comparison_Exp>
  trade_liquidationPrice?: InputMaybe<Numeric_Comparison_Exp>
  trade_margin?: InputMaybe<Numeric_Comparison_Exp>
  trade_maxPercentagePnL?: InputMaybe<Numeric_Comparison_Exp>
  trade_openPrice?: InputMaybe<Numeric_Comparison_Exp>
  trade_priceId?: InputMaybe<Bytea_Comparison_Exp>
  trade_profitTarget?: InputMaybe<Numeric_Comparison_Exp>
  trade_salt?: InputMaybe<Numeric_Comparison_Exp>
  trade_slippage?: InputMaybe<Numeric_Comparison_Exp>
  trade_stopLoss?: InputMaybe<Numeric_Comparison_Exp>
  trade_user?: InputMaybe<Bytea_Comparison_Exp>
  user?: InputMaybe<Bytea_Comparison_Exp>
}

/** aggregate max on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Max_Fields = {
  __typename?: "uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent_max_fields"
  evt_block_number?: Maybe<Scalars["bigint"]>
  evt_block_time?: Maybe<Scalars["timestamptz"]>
  evt_index?: Maybe<Scalars["numeric"]>
  evt_tx_index?: Maybe<Scalars["numeric"]>
  onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["numeric"]>
  onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["numeric"]>
  onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["numeric"]>
  onOrderUpdate_feeBalance_balance?: Maybe<Scalars["numeric"]>
  onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["numeric"]>
  onOrderUpdate_feeBase?: Maybe<Scalars["numeric"]>
  onUpdateTrade_marginDelta?: Maybe<Scalars["numeric"]>
  trade_executionBlock?: Maybe<Scalars["numeric"]>
  trade_executionTime?: Maybe<Scalars["numeric"]>
  trade_leverage?: Maybe<Scalars["numeric"]>
  trade_liquidationPrice?: Maybe<Scalars["numeric"]>
  trade_margin?: Maybe<Scalars["numeric"]>
  trade_maxPercentagePnL?: Maybe<Scalars["numeric"]>
  trade_openPrice?: Maybe<Scalars["numeric"]>
  trade_profitTarget?: Maybe<Scalars["numeric"]>
  trade_salt?: Maybe<Scalars["numeric"]>
  trade_slippage?: Maybe<Scalars["numeric"]>
  trade_stopLoss?: Maybe<Scalars["numeric"]>
}

/** aggregate min on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Min_Fields = {
  __typename?: "uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent_min_fields"
  evt_block_number?: Maybe<Scalars["bigint"]>
  evt_block_time?: Maybe<Scalars["timestamptz"]>
  evt_index?: Maybe<Scalars["numeric"]>
  evt_tx_index?: Maybe<Scalars["numeric"]>
  onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["numeric"]>
  onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["numeric"]>
  onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["numeric"]>
  onOrderUpdate_feeBalance_balance?: Maybe<Scalars["numeric"]>
  onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["numeric"]>
  onOrderUpdate_feeBase?: Maybe<Scalars["numeric"]>
  onUpdateTrade_marginDelta?: Maybe<Scalars["numeric"]>
  trade_executionBlock?: Maybe<Scalars["numeric"]>
  trade_executionTime?: Maybe<Scalars["numeric"]>
  trade_leverage?: Maybe<Scalars["numeric"]>
  trade_liquidationPrice?: Maybe<Scalars["numeric"]>
  trade_margin?: Maybe<Scalars["numeric"]>
  trade_maxPercentagePnL?: Maybe<Scalars["numeric"]>
  trade_openPrice?: Maybe<Scalars["numeric"]>
  trade_profitTarget?: Maybe<Scalars["numeric"]>
  trade_salt?: Maybe<Scalars["numeric"]>
  trade_slippage?: Maybe<Scalars["numeric"]>
  trade_stopLoss?: Maybe<Scalars["numeric"]>
}

/** Ordering options when selecting data from "uniwhale_v1_bsc.TradingCore_evt_UpdateOpenOrderEvent". */
export type Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Order_By = {
  contract_address?: InputMaybe<Order_By>
  evt_block_number?: InputMaybe<Order_By>
  evt_block_time?: InputMaybe<Order_By>
  evt_index?: InputMaybe<Order_By>
  evt_tx_hash?: InputMaybe<Order_By>
  evt_tx_index?: InputMaybe<Order_By>
  onOrderUpdate_accruedFee_fundingFee?: InputMaybe<Order_By>
  onOrderUpdate_accruedFee_lastUpdate?: InputMaybe<Order_By>
  onOrderUpdate_accruedFee_rolloverFee?: InputMaybe<Order_By>
  onOrderUpdate_feeBalance_balance?: InputMaybe<Order_By>
  onOrderUpdate_feeBalance_lastUpdate?: InputMaybe<Order_By>
  onOrderUpdate_feeBase?: InputMaybe<Order_By>
  onUpdateTrade_isAdding?: InputMaybe<Order_By>
  onUpdateTrade_marginDelta?: InputMaybe<Order_By>
  orderHash?: InputMaybe<Order_By>
  trade_executionBlock?: InputMaybe<Order_By>
  trade_executionTime?: InputMaybe<Order_By>
  trade_isBuy?: InputMaybe<Order_By>
  trade_leverage?: InputMaybe<Order_By>
  trade_liquidationPrice?: InputMaybe<Order_By>
  trade_margin?: InputMaybe<Order_By>
  trade_maxPercentagePnL?: InputMaybe<Order_By>
  trade_openPrice?: InputMaybe<Order_By>
  trade_priceId?: InputMaybe<Order_By>
  trade_profitTarget?: InputMaybe<Order_By>
  trade_salt?: InputMaybe<Order_By>
  trade_slippage?: InputMaybe<Order_By>
  trade_stopLoss?: InputMaybe<Order_By>
  trade_user?: InputMaybe<Order_By>
  user?: InputMaybe<Order_By>
}

/** select columns of table "uniwhale_v1_bsc.TradingCore_evt_UpdateOpenOrderEvent" */
export enum Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Select_Column {
  /** column name */
  ContractAddress = "contract_address",
  /** column name */
  EvtBlockNumber = "evt_block_number",
  /** column name */
  EvtBlockTime = "evt_block_time",
  /** column name */
  EvtIndex = "evt_index",
  /** column name */
  EvtTxHash = "evt_tx_hash",
  /** column name */
  EvtTxIndex = "evt_tx_index",
  /** column name */
  OnOrderUpdateAccruedFeeFundingFee = "onOrderUpdate_accruedFee_fundingFee",
  /** column name */
  OnOrderUpdateAccruedFeeLastUpdate = "onOrderUpdate_accruedFee_lastUpdate",
  /** column name */
  OnOrderUpdateAccruedFeeRolloverFee = "onOrderUpdate_accruedFee_rolloverFee",
  /** column name */
  OnOrderUpdateFeeBalanceBalance = "onOrderUpdate_feeBalance_balance",
  /** column name */
  OnOrderUpdateFeeBalanceLastUpdate = "onOrderUpdate_feeBalance_lastUpdate",
  /** column name */
  OnOrderUpdateFeeBase = "onOrderUpdate_feeBase",
  /** column name */
  OnUpdateTradeIsAdding = "onUpdateTrade_isAdding",
  /** column name */
  OnUpdateTradeMarginDelta = "onUpdateTrade_marginDelta",
  /** column name */
  OrderHash = "orderHash",
  /** column name */
  TradeExecutionBlock = "trade_executionBlock",
  /** column name */
  TradeExecutionTime = "trade_executionTime",
  /** column name */
  TradeIsBuy = "trade_isBuy",
  /** column name */
  TradeLeverage = "trade_leverage",
  /** column name */
  TradeLiquidationPrice = "trade_liquidationPrice",
  /** column name */
  TradeMargin = "trade_margin",
  /** column name */
  TradeMaxPercentagePnL = "trade_maxPercentagePnL",
  /** column name */
  TradeOpenPrice = "trade_openPrice",
  /** column name */
  TradePriceId = "trade_priceId",
  /** column name */
  TradeProfitTarget = "trade_profitTarget",
  /** column name */
  TradeSalt = "trade_salt",
  /** column name */
  TradeSlippage = "trade_slippage",
  /** column name */
  TradeStopLoss = "trade_stopLoss",
  /** column name */
  TradeUser = "trade_user",
  /** column name */
  User = "user",
}

/** aggregate stddev on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Stddev_Fields =
  {
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent_stddev_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_balance?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBase?: Maybe<Scalars["Float"]>
    onUpdateTrade_marginDelta?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** aggregate stddev_pop on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Stddev_Pop_Fields =
  {
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent_stddev_pop_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_balance?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBase?: Maybe<Scalars["Float"]>
    onUpdateTrade_marginDelta?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** aggregate stddev_samp on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Stddev_Samp_Fields =
  {
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent_stddev_samp_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_balance?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBase?: Maybe<Scalars["Float"]>
    onUpdateTrade_marginDelta?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** Streaming cursor of the table "uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent" */
export type Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Stream_Cursor_Input =
  {
    /** Stream column input with initial value */
    initial_value: Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Stream_Cursor_Value_Input
    /** cursor ordering */
    ordering?: InputMaybe<Cursor_Ordering>
  }

/** Initial value of the column from where the streaming should start */
export type Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Stream_Cursor_Value_Input =
  {
    contract_address?: InputMaybe<Scalars["bytea"]>
    evt_block_number?: InputMaybe<Scalars["bigint"]>
    evt_block_time?: InputMaybe<Scalars["timestamptz"]>
    evt_index?: InputMaybe<Scalars["numeric"]>
    evt_tx_hash?: InputMaybe<Scalars["bytea"]>
    evt_tx_index?: InputMaybe<Scalars["numeric"]>
    onOrderUpdate_accruedFee_fundingFee?: InputMaybe<Scalars["numeric"]>
    onOrderUpdate_accruedFee_lastUpdate?: InputMaybe<Scalars["numeric"]>
    onOrderUpdate_accruedFee_rolloverFee?: InputMaybe<Scalars["numeric"]>
    onOrderUpdate_feeBalance_balance?: InputMaybe<Scalars["numeric"]>
    onOrderUpdate_feeBalance_lastUpdate?: InputMaybe<Scalars["numeric"]>
    onOrderUpdate_feeBase?: InputMaybe<Scalars["numeric"]>
    onUpdateTrade_isAdding?: InputMaybe<Scalars["Boolean"]>
    onUpdateTrade_marginDelta?: InputMaybe<Scalars["numeric"]>
    orderHash?: InputMaybe<Scalars["bytea"]>
    trade_executionBlock?: InputMaybe<Scalars["numeric"]>
    trade_executionTime?: InputMaybe<Scalars["numeric"]>
    trade_isBuy?: InputMaybe<Scalars["Boolean"]>
    trade_leverage?: InputMaybe<Scalars["numeric"]>
    trade_liquidationPrice?: InputMaybe<Scalars["numeric"]>
    trade_margin?: InputMaybe<Scalars["numeric"]>
    trade_maxPercentagePnL?: InputMaybe<Scalars["numeric"]>
    trade_openPrice?: InputMaybe<Scalars["numeric"]>
    trade_priceId?: InputMaybe<Scalars["bytea"]>
    trade_profitTarget?: InputMaybe<Scalars["numeric"]>
    trade_salt?: InputMaybe<Scalars["numeric"]>
    trade_slippage?: InputMaybe<Scalars["numeric"]>
    trade_stopLoss?: InputMaybe<Scalars["numeric"]>
    trade_user?: InputMaybe<Scalars["bytea"]>
    user?: InputMaybe<Scalars["bytea"]>
  }

/** aggregate sum on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Sum_Fields = {
  __typename?: "uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent_sum_fields"
  evt_block_number?: Maybe<Scalars["bigint"]>
  evt_index?: Maybe<Scalars["numeric"]>
  evt_tx_index?: Maybe<Scalars["numeric"]>
  onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["numeric"]>
  onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["numeric"]>
  onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["numeric"]>
  onOrderUpdate_feeBalance_balance?: Maybe<Scalars["numeric"]>
  onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["numeric"]>
  onOrderUpdate_feeBase?: Maybe<Scalars["numeric"]>
  onUpdateTrade_marginDelta?: Maybe<Scalars["numeric"]>
  trade_executionBlock?: Maybe<Scalars["numeric"]>
  trade_executionTime?: Maybe<Scalars["numeric"]>
  trade_leverage?: Maybe<Scalars["numeric"]>
  trade_liquidationPrice?: Maybe<Scalars["numeric"]>
  trade_margin?: Maybe<Scalars["numeric"]>
  trade_maxPercentagePnL?: Maybe<Scalars["numeric"]>
  trade_openPrice?: Maybe<Scalars["numeric"]>
  trade_profitTarget?: Maybe<Scalars["numeric"]>
  trade_salt?: Maybe<Scalars["numeric"]>
  trade_slippage?: Maybe<Scalars["numeric"]>
  trade_stopLoss?: Maybe<Scalars["numeric"]>
}

/** aggregate var_pop on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Var_Pop_Fields =
  {
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent_var_pop_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_balance?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBase?: Maybe<Scalars["Float"]>
    onUpdateTrade_marginDelta?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** aggregate var_samp on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Var_Samp_Fields =
  {
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent_var_samp_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_balance?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBase?: Maybe<Scalars["Float"]>
    onUpdateTrade_marginDelta?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** aggregate variance on columns */
export type Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Variance_Fields =
  {
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent_variance_fields"
    evt_block_number?: Maybe<Scalars["Float"]>
    evt_index?: Maybe<Scalars["Float"]>
    evt_tx_index?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_fundingFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_accruedFee_rolloverFee?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_balance?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBalance_lastUpdate?: Maybe<Scalars["Float"]>
    onOrderUpdate_feeBase?: Maybe<Scalars["Float"]>
    onUpdateTrade_marginDelta?: Maybe<Scalars["Float"]>
    trade_executionBlock?: Maybe<Scalars["Float"]>
    trade_executionTime?: Maybe<Scalars["Float"]>
    trade_leverage?: Maybe<Scalars["Float"]>
    trade_liquidationPrice?: Maybe<Scalars["Float"]>
    trade_margin?: Maybe<Scalars["Float"]>
    trade_maxPercentagePnL?: Maybe<Scalars["Float"]>
    trade_openPrice?: Maybe<Scalars["Float"]>
    trade_profitTarget?: Maybe<Scalars["Float"]>
    trade_salt?: Maybe<Scalars["Float"]>
    trade_slippage?: Maybe<Scalars["Float"]>
    trade_stopLoss?: Maybe<Scalars["Float"]>
  }

/** columns and relationships of "uniwhale_v1_bsc.UniwhaleGenesisPass_evt_Transfer" */
export type Uniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_Transfer = {
  __typename?: "uniwhale_v1_bsc_UniwhaleGenesisPass_evt_Transfer"
  contract_address: Scalars["bytea"]
  evt_block_number: Scalars["bigint"]
  evt_index: Scalars["numeric"]
  evt_tx_hash: Scalars["bytea"]
  evt_tx_index: Scalars["numeric"]
  from: Scalars["bytea"]
  to: Scalars["bytea"]
  tokenId: Scalars["numeric"]
}

/** Boolean expression to filter rows from the table "uniwhale_v1_bsc.UniwhaleGenesisPass_evt_Transfer". All fields are combined with a logical 'AND'. */
export type Uniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_Transfer_Bool_Exp = {
  _and?: InputMaybe<
    Array<Uniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_Transfer_Bool_Exp>
  >
  _not?: InputMaybe<Uniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_Transfer_Bool_Exp>
  _or?: InputMaybe<
    Array<Uniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_Transfer_Bool_Exp>
  >
  contract_address?: InputMaybe<Bytea_Comparison_Exp>
  evt_block_number?: InputMaybe<Bigint_Comparison_Exp>
  evt_index?: InputMaybe<Numeric_Comparison_Exp>
  evt_tx_hash?: InputMaybe<Bytea_Comparison_Exp>
  evt_tx_index?: InputMaybe<Numeric_Comparison_Exp>
  from?: InputMaybe<Bytea_Comparison_Exp>
  to?: InputMaybe<Bytea_Comparison_Exp>
  tokenId?: InputMaybe<Numeric_Comparison_Exp>
}

/** Ordering options when selecting data from "uniwhale_v1_bsc.UniwhaleGenesisPass_evt_Transfer". */
export type Uniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_Transfer_Order_By = {
  contract_address?: InputMaybe<Order_By>
  evt_block_number?: InputMaybe<Order_By>
  evt_index?: InputMaybe<Order_By>
  evt_tx_hash?: InputMaybe<Order_By>
  evt_tx_index?: InputMaybe<Order_By>
  from?: InputMaybe<Order_By>
  to?: InputMaybe<Order_By>
  tokenId?: InputMaybe<Order_By>
}

/** select columns of table "uniwhale_v1_bsc.UniwhaleGenesisPass_evt_Transfer" */
export enum Uniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_Transfer_Select_Column {
  /** column name */
  ContractAddress = "contract_address",
  /** column name */
  EvtBlockNumber = "evt_block_number",
  /** column name */
  EvtIndex = "evt_index",
  /** column name */
  EvtTxHash = "evt_tx_hash",
  /** column name */
  EvtTxIndex = "evt_tx_index",
  /** column name */
  From = "from",
  /** column name */
  To = "to",
  /** column name */
  TokenId = "tokenId",
}

/** Streaming cursor of the table "uniwhale_v1_bsc_UniwhaleGenesisPass_evt_Transfer" */
export type Uniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_Transfer_Stream_Cursor_Input =
  {
    /** Stream column input with initial value */
    initial_value: Uniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_Transfer_Stream_Cursor_Value_Input
    /** cursor ordering */
    ordering?: InputMaybe<Cursor_Ordering>
  }

/** Initial value of the column from where the streaming should start */
export type Uniwhale_V1_Bsc_UniwhaleGenesisPass_Evt_Transfer_Stream_Cursor_Value_Input =
  {
    contract_address?: InputMaybe<Scalars["bytea"]>
    evt_block_number?: InputMaybe<Scalars["bigint"]>
    evt_index?: InputMaybe<Scalars["numeric"]>
    evt_tx_hash?: InputMaybe<Scalars["bytea"]>
    evt_tx_index?: InputMaybe<Scalars["numeric"]>
    from?: InputMaybe<Scalars["bytea"]>
    to?: InputMaybe<Scalars["bytea"]>
    tokenId?: InputMaybe<Scalars["numeric"]>
  }

/** columns and relationships of "uniwhale_v1_bsc.UniwhaleTestnetPOAP_evt_Transfer" */
export type Uniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_Transfer = {
  __typename?: "uniwhale_v1_bsc_UniwhaleTestnetPOAP_evt_Transfer"
  contract_address: Scalars["bytea"]
  evt_block_number: Scalars["bigint"]
  evt_index: Scalars["numeric"]
  evt_tx_hash: Scalars["bytea"]
  evt_tx_index: Scalars["numeric"]
  from: Scalars["bytea"]
  to: Scalars["bytea"]
  tokenId: Scalars["numeric"]
}

/** Boolean expression to filter rows from the table "uniwhale_v1_bsc.UniwhaleTestnetPOAP_evt_Transfer". All fields are combined with a logical 'AND'. */
export type Uniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_Transfer_Bool_Exp = {
  _and?: InputMaybe<
    Array<Uniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_Transfer_Bool_Exp>
  >
  _not?: InputMaybe<Uniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_Transfer_Bool_Exp>
  _or?: InputMaybe<
    Array<Uniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_Transfer_Bool_Exp>
  >
  contract_address?: InputMaybe<Bytea_Comparison_Exp>
  evt_block_number?: InputMaybe<Bigint_Comparison_Exp>
  evt_index?: InputMaybe<Numeric_Comparison_Exp>
  evt_tx_hash?: InputMaybe<Bytea_Comparison_Exp>
  evt_tx_index?: InputMaybe<Numeric_Comparison_Exp>
  from?: InputMaybe<Bytea_Comparison_Exp>
  to?: InputMaybe<Bytea_Comparison_Exp>
  tokenId?: InputMaybe<Numeric_Comparison_Exp>
}

/** Ordering options when selecting data from "uniwhale_v1_bsc.UniwhaleTestnetPOAP_evt_Transfer". */
export type Uniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_Transfer_Order_By = {
  contract_address?: InputMaybe<Order_By>
  evt_block_number?: InputMaybe<Order_By>
  evt_index?: InputMaybe<Order_By>
  evt_tx_hash?: InputMaybe<Order_By>
  evt_tx_index?: InputMaybe<Order_By>
  from?: InputMaybe<Order_By>
  to?: InputMaybe<Order_By>
  tokenId?: InputMaybe<Order_By>
}

/** select columns of table "uniwhale_v1_bsc.UniwhaleTestnetPOAP_evt_Transfer" */
export enum Uniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_Transfer_Select_Column {
  /** column name */
  ContractAddress = "contract_address",
  /** column name */
  EvtBlockNumber = "evt_block_number",
  /** column name */
  EvtIndex = "evt_index",
  /** column name */
  EvtTxHash = "evt_tx_hash",
  /** column name */
  EvtTxIndex = "evt_tx_index",
  /** column name */
  From = "from",
  /** column name */
  To = "to",
  /** column name */
  TokenId = "tokenId",
}

/** Streaming cursor of the table "uniwhale_v1_bsc_UniwhaleTestnetPOAP_evt_Transfer" */
export type Uniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_Transfer_Stream_Cursor_Input =
  {
    /** Stream column input with initial value */
    initial_value: Uniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_Transfer_Stream_Cursor_Value_Input
    /** cursor ordering */
    ordering?: InputMaybe<Cursor_Ordering>
  }

/** Initial value of the column from where the streaming should start */
export type Uniwhale_V1_Bsc_UniwhaleTestnetPoap_Evt_Transfer_Stream_Cursor_Value_Input =
  {
    contract_address?: InputMaybe<Scalars["bytea"]>
    evt_block_number?: InputMaybe<Scalars["bigint"]>
    evt_index?: InputMaybe<Scalars["numeric"]>
    evt_tx_hash?: InputMaybe<Scalars["bytea"]>
    evt_tx_index?: InputMaybe<Scalars["numeric"]>
    from?: InputMaybe<Scalars["bytea"]>
    to?: InputMaybe<Scalars["bytea"]>
    tokenId?: InputMaybe<Scalars["numeric"]>
  }

export type GetParticipantCountQueryVariables = Exact<{ [key: string]: never }>

export type GetParticipantCountQuery = {
  __typename?: "query_root"
  ad_summary: Array<{
    __typename?: "ad_summary"
    total_holders?: number | null
  }>
}

export type GetQuestScoresQueryVariables = Exact<{
  walletAddress: Scalars["bytea"]
}>

export type GetQuestScoresQuery = {
  __typename?: "query_root"
  ad_ranked_holders: Array<{
    __typename?: "ad_ranked_holders"
    total_scores?: number | null
    genesis_pass_token_scores?: number | null
    testnet_contributoers_scores?: number | null
    testnet_poap_token_scores?: number | null
  }>
}

export type FetchMainnetContributorQueryVariables = Exact<{
  walletAddress: Scalars["bytea"]
}>

export type FetchMainnetContributorQuery = {
  __typename?: "query_root"
  liquidity_bootstrap_stage_1: Array<{
    __typename?: "liquidity_bootstrap_stage_1"
    balance?: number | null
    scores?: number | null
  }>
  liquidity_bootstrap_stage_2: Array<{
    __typename?: "liquidity_bootstrap_stage_2"
    balance?: number | null
    scores?: number | null
  }>
  liquidity_bootstrap_stage_3: Array<{
    __typename?: "liquidity_bootstrap_stage_3"
    balance?: number | null
    scores?: number | null
  }>
  trade_bootstrap_stage_1: Array<{
    __typename?: "trade_bootstrap_stage_1"
    open_position?: number | null
    close_position?: number | null
  }>
  trade_bootstrap_stage_2: Array<{
    __typename?: "trade_bootstrap_stage_2"
    open_position?: number | null
    close_position?: number | null
  }>
  trading_total: Array<{
    __typename?: "trading_total"
    total_trade_volume?: number | null
  }>
}

export type PythPriceSubscriptionVariables = Exact<{
  priceId: Scalars["bytea"]
}>

export type PythPriceSubscription = {
  __typename?: "subscription_root"
  latest_pyth_prices_by_pk?: {
    __typename?: "latest_pyth_prices"
    price: number
    publish_time: string
    conf: number
    expo: number
    price_data?: string | null
  } | null
}

export type LatestTxsQueryVariables = Exact<{ [key: string]: never }>

export type LatestTxsQuery = {
  __typename?: "query_root"
  uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent: Array<{
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent"
    evt_tx_hash: string
    orderHash: string
    evt_block_number: number
    trade_user: string
  }>
  uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent: Array<{
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent"
    evt_tx_hash: string
    orderHash: string
    evt_block_number: number
    trade_user: string
  }>
  uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent: Array<{
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent"
    evt_tx_hash: string
    orderHash: string
    evt_block_number: number
    trade_user: string
  }>
}

export type OrderHashesByTxHashQueryVariables = Exact<{
  txHash: Scalars["bytea"]
}>

export type OrderHashesByTxHashQuery = {
  __typename?: "query_root"
  uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent: Array<{
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent"
    orderHash: string
  }>
  uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent: Array<{
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent"
    orderHash: string
  }>
  uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent: Array<{
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent"
    orderHash: string
  }>
  uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent: Array<{
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent"
    orderHash: string
  }>
  uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent: Array<{
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent"
    orderHash: string
  }>
  uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent: Array<{
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent"
    orderHash: string
  }>
  uniwhale_v1_bsc_LimitBook_evt_CloseLimitOrderEvent: Array<{
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_CloseLimitOrderEvent"
    orderHash: string
  }>
}

export type LatestTradeStatsQueryVariables = Exact<{ [key: string]: never }>

export type LatestTradeStatsQuery = {
  __typename?: "query_root"
  latest_trade_stats_7d: Array<{
    __typename?: "latest_trade_stats_7d"
    timestamp?: number | null
    apr_7d?: number | null
    fee_7d?: number | null
    accumulated_fee?: number | null
    unw_apr_7d?: number | null
    unw_emission_apr_7d?: number | null
    es_unw_apr_7d?: number | null
    es_unw_emission_apr_7d?: number | null
    ulp_emission_apr_7d?: number | null
  }>
}

export type GetReferredCountOfRefererQueryVariables = Exact<{
  walletAddress?: InputMaybe<Scalars["bytea"]>
}>

export type GetReferredCountOfRefererQuery = {
  __typename?: "query_root"
  cp_referral_counter: Array<{
    __typename?: "cp_referral_counter"
    count?: number | null
  }>
}

export type GetReferredCountOfReferralCodeQueryVariables = Exact<{
  walletAddress?: InputMaybe<Scalars["bytea"]>
  referralCode?: InputMaybe<Scalars["bytea"]>
}>

export type GetReferredCountOfReferralCodeQuery = {
  __typename?: "query_root"
  cp_referral_counter: Array<{
    __typename?: "cp_referral_counter"
    count?: number | null
  }>
}

export type GetTotalRebateVolumeOfRefererQueryVariables = Exact<{
  walletAddress?: InputMaybe<Scalars["bytea"]>
}>

export type GetTotalRebateVolumeOfRefererQuery = {
  __typename?: "query_root"
  cp_referrer_summary_aggregate: {
    __typename?: "cp_referrer_summary_aggregate"
    aggregate?: {
      __typename?: "cp_referrer_summary_aggregate_fields"
      sum?: {
        __typename?: "cp_referrer_summary_sum_fields"
        referral_fee?: number | null
      } | null
    } | null
  }
}

export type GetTotalRebateVolumeOfReferralCodeQueryVariables = Exact<{
  walletAddress?: InputMaybe<Scalars["bytea"]>
  referralCode?: InputMaybe<Scalars["bytea"]>
}>

export type GetTotalRebateVolumeOfReferralCodeQuery = {
  __typename?: "query_root"
  cp_referrer_summary_aggregate: {
    __typename?: "cp_referrer_summary_aggregate"
    aggregate?: {
      __typename?: "cp_referrer_summary_aggregate_fields"
      sum?: {
        __typename?: "cp_referrer_summary_sum_fields"
        referral_fee?: number | null
      } | null
    } | null
  }
}

export type GetTotalTradingVolumeOfRefererQueryVariables = Exact<{
  walletAddress?: InputMaybe<Scalars["bytea"]>
}>

export type GetTotalTradingVolumeOfRefererQuery = {
  __typename?: "query_root"
  cp_referrer_summary_aggregate: {
    __typename?: "cp_referrer_summary_aggregate"
    aggregate?: {
      __typename?: "cp_referrer_summary_aggregate_fields"
      sum?: {
        __typename?: "cp_referrer_summary_sum_fields"
        trade_volume?: number | null
      } | null
    } | null
  }
}

export type GetTotalTradingVolumeOfReferralCodeQueryVariables = Exact<{
  walletAddress?: InputMaybe<Scalars["bytea"]>
  referralCode?: InputMaybe<Scalars["bytea"]>
}>

export type GetTotalTradingVolumeOfReferralCodeQuery = {
  __typename?: "query_root"
  cp_referrer_summary_aggregate: {
    __typename?: "cp_referrer_summary_aggregate"
    aggregate?: {
      __typename?: "cp_referrer_summary_aggregate_fields"
      sum?: {
        __typename?: "cp_referrer_summary_sum_fields"
        trade_volume?: number | null
      } | null
    } | null
  }
}

export type GetAllReferralCodeInfoOfRefererQueryVariables = Exact<{
  walletAddress?: InputMaybe<Scalars["bytea"]>
}>

export type GetAllReferralCodeInfoOfRefererQuery = {
  __typename?: "query_root"
  cp_referrer_summary: Array<{
    __typename?: "cp_referrer_summary"
    fee_referralCode?: string | null
    trade_volume?: number | null
  }>
  cp_referral_counter: Array<{
    __typename?: "cp_referral_counter"
    referralCode?: string | null
    count?: number | null
  }>
}

export type GetTotalTradingVolumeOfTraderQueryVariables = Exact<{
  walletAddress?: InputMaybe<Scalars["bytea"]>
}>

export type GetTotalTradingVolumeOfTraderQuery = {
  __typename?: "query_root"
  cp_referee_summary: Array<{
    __typename?: "cp_referee_summary"
    trade_volume?: number | null
  }>
}

export type GetTotalDiscountVolumeOfTraderQueryVariables = Exact<{
  walletAddress?: InputMaybe<Scalars["bytea"]>
}>

export type GetTotalDiscountVolumeOfTraderQuery = {
  __typename?: "query_root"
  cp_referee_summary: Array<{
    __typename?: "cp_referee_summary"
    referred_fee?: number | null
  }>
}

export type OpenLimitOrderEventFragment = {
  __typename?: "uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent"
  trade_priceId: string
  orderHash: string
  trade_isBuy: boolean
  trade_margin: number
  trade_slippage: number
  trade_stopLoss: number
  trade_profitTarget: number
  trade_leverage: number
  trade_maxPercentagePnL: number
  trade_openPrice: number
  trade_liquidationPrice: number
  evt_block_number: number
  evt_block_time: string
  evt_tx_hash: string
}

export type OpenLimitOrderEventHistoriesQueryVariables = Exact<{
  myAddress: Scalars["bytea"]
}>

export type OpenLimitOrderEventHistoriesQuery = {
  __typename?: "query_root"
  uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent: Array<{
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent"
    trade_priceId: string
    orderHash: string
    trade_isBuy: boolean
    trade_margin: number
    trade_slippage: number
    trade_stopLoss: number
    trade_profitTarget: number
    trade_leverage: number
    trade_maxPercentagePnL: number
    trade_openPrice: number
    trade_liquidationPrice: number
    evt_block_number: number
    evt_block_time: string
    evt_tx_hash: string
  }>
}

export type OpenLimitOrderEventsSubscriptionVariables = Exact<{
  myAddress: Scalars["bytea"]
  currentBlock: Scalars["bigint"]
}>

export type OpenLimitOrderEventsSubscription = {
  __typename?: "subscription_root"
  uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent: Array<{
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent"
    trade_priceId: string
    orderHash: string
    trade_isBuy: boolean
    trade_margin: number
    trade_slippage: number
    trade_stopLoss: number
    trade_profitTarget: number
    trade_leverage: number
    trade_maxPercentagePnL: number
    trade_openPrice: number
    trade_liquidationPrice: number
    evt_block_number: number
    evt_block_time: string
    evt_tx_hash: string
  }>
}

export type UpdateLimitOrderEventFragment = {
  __typename?: "uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent"
  orderHash: string
  trade_priceId: string
  trade_isBuy: boolean
  trade_margin: number
  trade_slippage: number
  trade_stopLoss: number
  trade_profitTarget: number
  trade_leverage: number
  trade_maxPercentagePnL: number
  trade_openPrice: number
  trade_liquidationPrice: number
  evt_block_number: number
  evt_block_time: string
  evt_tx_hash: string
}

export type UpdateLimitOrderEventHistoriesQueryVariables = Exact<{
  myAddress: Scalars["bytea"]
}>

export type UpdateLimitOrderEventHistoriesQuery = {
  __typename?: "query_root"
  uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent: Array<{
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent"
    orderHash: string
    trade_priceId: string
    trade_isBuy: boolean
    trade_margin: number
    trade_slippage: number
    trade_stopLoss: number
    trade_profitTarget: number
    trade_leverage: number
    trade_maxPercentagePnL: number
    trade_openPrice: number
    trade_liquidationPrice: number
    evt_block_number: number
    evt_block_time: string
    evt_tx_hash: string
  }>
}

export type UpdateLimitOrderEventsSubscriptionVariables = Exact<{
  myAddress: Scalars["bytea"]
  currentBlock: Scalars["bigint"]
}>

export type UpdateLimitOrderEventsSubscription = {
  __typename?: "subscription_root"
  uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent: Array<{
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent"
    orderHash: string
    trade_priceId: string
    trade_isBuy: boolean
    trade_margin: number
    trade_slippage: number
    trade_stopLoss: number
    trade_profitTarget: number
    trade_leverage: number
    trade_maxPercentagePnL: number
    trade_openPrice: number
    trade_liquidationPrice: number
    evt_block_number: number
    evt_block_time: string
    evt_tx_hash: string
  }>
}

export type CloseLimitOrderEventFragment = {
  __typename?: "uniwhale_v1_bsc_LimitBook_evt_CloseLimitOrderEvent"
  orderHash: string
  trade_priceId: string
  evt_block_number: number
  evt_block_time: string
  evt_tx_hash: string
}

export type CloseLimitOrderEventHistoriesQueryVariables = Exact<{
  myAddress: Scalars["bytea"]
}>

export type CloseLimitOrderEventHistoriesQuery = {
  __typename?: "query_root"
  uniwhale_v1_bsc_LimitBook_evt_CloseLimitOrderEvent: Array<{
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_CloseLimitOrderEvent"
    orderHash: string
    trade_priceId: string
    evt_block_number: number
    evt_block_time: string
    evt_tx_hash: string
  }>
}

export type CloseLimitOrderEventsSubscriptionVariables = Exact<{
  myAddress: Scalars["bytea"]
  currentBlock: Scalars["bigint"]
}>

export type CloseLimitOrderEventsSubscription = {
  __typename?: "subscription_root"
  uniwhale_v1_bsc_LimitBook_evt_CloseLimitOrderEvent: Array<{
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_CloseLimitOrderEvent"
    orderHash: string
    trade_priceId: string
    evt_block_number: number
    evt_block_time: string
    evt_tx_hash: string
  }>
}

export type ExecuteLimitOrderEventFragment = {
  __typename?: "uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent"
  orderHash: string
  trade_priceId: string
  evt_block_number: number
  evt_block_time: string
  evt_tx_hash: string
}

export type ExecuteLimitOrderEventHistoriesQueryVariables = Exact<{
  myAddress: Scalars["bytea"]
}>

export type ExecuteLimitOrderEventHistoriesQuery = {
  __typename?: "query_root"
  uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent: Array<{
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent"
    orderHash: string
    trade_priceId: string
    evt_block_number: number
    evt_block_time: string
    evt_tx_hash: string
  }>
}

export type ExecuteLimitOrderEventsSubscriptionVariables = Exact<{
  myAddress: Scalars["bytea"]
  currentBlock: Scalars["bigint"]
}>

export type ExecuteLimitOrderEventsSubscription = {
  __typename?: "subscription_root"
  uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent: Array<{
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent"
    orderHash: string
    trade_priceId: string
    evt_block_number: number
    evt_block_time: string
    evt_tx_hash: string
  }>
}

export type PartialCloseLimitOrderEventFragment = {
  __typename?: "uniwhale_v1_bsc_LimitBook_evt_PartialCloseLimitOrderEvent"
  orderHash: string
  trade_priceId: string
  trade_isBuy: boolean
  trade_margin: number
  trade_slippage: number
  trade_stopLoss: number
  trade_profitTarget: number
  trade_leverage: number
  trade_maxPercentagePnL: number
  trade_openPrice: number
  trade_liquidationPrice: number
  evt_block_number: number
  evt_block_time: string
  evt_tx_hash: string
  closePercent: number
}

export type PartialCloseLimitOrderEventHistoriesQueryVariables = Exact<{
  myAddress: Scalars["bytea"]
}>

export type PartialCloseLimitOrderEventHistoriesQuery = {
  __typename?: "query_root"
  uniwhale_v1_bsc_LimitBook_evt_PartialCloseLimitOrderEvent: Array<{
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_PartialCloseLimitOrderEvent"
    orderHash: string
    trade_priceId: string
    trade_isBuy: boolean
    trade_margin: number
    trade_slippage: number
    trade_stopLoss: number
    trade_profitTarget: number
    trade_leverage: number
    trade_maxPercentagePnL: number
    trade_openPrice: number
    trade_liquidationPrice: number
    evt_block_number: number
    evt_block_time: string
    evt_tx_hash: string
    closePercent: number
  }>
}

export type PartialCloseLimitOrderEventsSubscriptionVariables = Exact<{
  myAddress: Scalars["bytea"]
  currentBlock: Scalars["bigint"]
}>

export type PartialCloseLimitOrderEventsSubscription = {
  __typename?: "subscription_root"
  uniwhale_v1_bsc_LimitBook_evt_PartialCloseLimitOrderEvent: Array<{
    __typename?: "uniwhale_v1_bsc_LimitBook_evt_PartialCloseLimitOrderEvent"
    orderHash: string
    trade_priceId: string
    trade_isBuy: boolean
    trade_margin: number
    trade_slippage: number
    trade_stopLoss: number
    trade_profitTarget: number
    trade_leverage: number
    trade_maxPercentagePnL: number
    trade_openPrice: number
    trade_liquidationPrice: number
    evt_block_number: number
    evt_block_time: string
    evt_tx_hash: string
    closePercent: number
  }>
}

export type OpenEventFragment = {
  __typename?: "uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent"
  orderHash: string
  trade_priceId: string
  trade_isBuy: boolean
  trade_margin: number
  trade_slippage: number
  trade_stopLoss: number
  trade_profitTarget: number
  trade_leverage: number
  trade_maxPercentagePnL: number
  trade_openPrice: number
  trade_liquidationPrice: number
  trade_executionBlock: number
  trade_user: string
  fee_fee: number
  onOrderUpdate_feeBase: number
  onOrderUpdate_accruedFee_fundingFee: number
  onOrderUpdate_accruedFee_rolloverFee: number
  onOrderUpdate_accruedFee_lastUpdate: number
  evt_block_number: number
  evt_block_time: string
  evt_tx_hash: string
}

export type OpenOrderEventHistoriesQueryVariables = Exact<{
  where?: InputMaybe<Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Bool_Exp>
  limit?: InputMaybe<Scalars["Int"]>
}>

export type OpenOrderEventHistoriesQuery = {
  __typename?: "query_root"
  uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent: Array<{
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent"
    orderHash: string
    trade_priceId: string
    trade_isBuy: boolean
    trade_margin: number
    trade_slippage: number
    trade_stopLoss: number
    trade_profitTarget: number
    trade_leverage: number
    trade_maxPercentagePnL: number
    trade_openPrice: number
    trade_liquidationPrice: number
    trade_executionBlock: number
    trade_user: string
    fee_fee: number
    onOrderUpdate_feeBase: number
    onOrderUpdate_accruedFee_fundingFee: number
    onOrderUpdate_accruedFee_rolloverFee: number
    onOrderUpdate_accruedFee_lastUpdate: number
    evt_block_number: number
    evt_block_time: string
    evt_tx_hash: string
  }>
}

export type OpenOrderEventsSubscriptionVariables = Exact<{
  where: Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Bool_Exp
}>

export type OpenOrderEventsSubscription = {
  __typename?: "subscription_root"
  uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent: Array<{
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent"
    orderHash: string
    trade_priceId: string
    trade_isBuy: boolean
    trade_margin: number
    trade_slippage: number
    trade_stopLoss: number
    trade_profitTarget: number
    trade_leverage: number
    trade_maxPercentagePnL: number
    trade_openPrice: number
    trade_liquidationPrice: number
    trade_executionBlock: number
    trade_user: string
    fee_fee: number
    onOrderUpdate_feeBase: number
    onOrderUpdate_accruedFee_fundingFee: number
    onOrderUpdate_accruedFee_rolloverFee: number
    onOrderUpdate_accruedFee_lastUpdate: number
    evt_block_number: number
    evt_block_time: string
    evt_tx_hash: string
  }>
}

export type UpdateEventFragment = {
  __typename?: "uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent"
  orderHash: string
  trade_priceId: string
  trade_isBuy: boolean
  trade_margin: number
  trade_slippage: number
  trade_stopLoss: number
  trade_profitTarget: number
  trade_leverage: number
  trade_maxPercentagePnL: number
  trade_openPrice: number
  trade_liquidationPrice: number
  trade_executionBlock: number
  trade_user: string
  onUpdateTrade_marginDelta: number
  onUpdateTrade_isAdding: boolean
  onOrderUpdate_feeBase: number
  onOrderUpdate_accruedFee_fundingFee: number
  onOrderUpdate_accruedFee_rolloverFee: number
  onOrderUpdate_accruedFee_lastUpdate: number
  evt_block_number: number
  evt_block_time: string
  evt_tx_hash: string
}

export type UpdateOrderEventHistoriesQueryVariables = Exact<{
  where?: InputMaybe<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Bool_Exp>
  limit?: InputMaybe<Scalars["Int"]>
}>

export type UpdateOrderEventHistoriesQuery = {
  __typename?: "query_root"
  uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent: Array<{
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent"
    orderHash: string
    trade_priceId: string
    trade_isBuy: boolean
    trade_margin: number
    trade_slippage: number
    trade_stopLoss: number
    trade_profitTarget: number
    trade_leverage: number
    trade_maxPercentagePnL: number
    trade_openPrice: number
    trade_liquidationPrice: number
    trade_executionBlock: number
    trade_user: string
    onUpdateTrade_marginDelta: number
    onUpdateTrade_isAdding: boolean
    onOrderUpdate_feeBase: number
    onOrderUpdate_accruedFee_fundingFee: number
    onOrderUpdate_accruedFee_rolloverFee: number
    onOrderUpdate_accruedFee_lastUpdate: number
    evt_block_number: number
    evt_block_time: string
    evt_tx_hash: string
  }>
}

export type UpdateOrderEventsSubscriptionVariables = Exact<{
  where?: InputMaybe<Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Bool_Exp>
}>

export type UpdateOrderEventsSubscription = {
  __typename?: "subscription_root"
  uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent: Array<{
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent"
    orderHash: string
    trade_priceId: string
    trade_isBuy: boolean
    trade_margin: number
    trade_slippage: number
    trade_stopLoss: number
    trade_profitTarget: number
    trade_leverage: number
    trade_maxPercentagePnL: number
    trade_openPrice: number
    trade_liquidationPrice: number
    trade_executionBlock: number
    trade_user: string
    onUpdateTrade_marginDelta: number
    onUpdateTrade_isAdding: boolean
    onOrderUpdate_feeBase: number
    onOrderUpdate_accruedFee_fundingFee: number
    onOrderUpdate_accruedFee_rolloverFee: number
    onOrderUpdate_accruedFee_lastUpdate: number
    evt_block_number: number
    evt_block_time: string
    evt_tx_hash: string
  }>
}

export type CloseEventFragment = {
  __typename?: "uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent"
  orderHash: string
  closePercent: number
  trade_priceId: string
  trade_isBuy: boolean
  trade_margin: number
  trade_slippage: number
  trade_stopLoss: number
  trade_profitTarget: number
  trade_leverage: number
  trade_maxPercentagePnL: number
  trade_openPrice: number
  trade_liquidationPrice: number
  trade_executionBlock: number
  trade_user: string
  afterCloseTrade_settled: number
  afterCloseTrade_fees_fee: number
  afterCloseTrade_liquidationFee: number
  afterCloseTrade_oraclePrice: number
  onCloseTrade_closeNet: number
  onCloseTrade_grossPnL: number
  onCloseTrade_isLiquidated: boolean
  onCloseTrade_isStop: boolean
  onCloseTrade_fundingFee: number
  onCloseTrade_rolloverFee: number
  onCloseTrade_slippage: number
  onOrderUpdate_feeBase: number
  onOrderUpdate_accruedFee_fundingFee: number
  onOrderUpdate_accruedFee_rolloverFee: number
  onOrderUpdate_accruedFee_lastUpdate: number
  evt_block_number: number
  evt_block_time: string
  evt_tx_hash: string
}

export type CloseOrderEventHistoriesQueryVariables = Exact<{
  where?: InputMaybe<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Bool_Exp>
  limit?: InputMaybe<Scalars["Int"]>
}>

export type CloseOrderEventHistoriesQuery = {
  __typename?: "query_root"
  uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent: Array<{
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent"
    orderHash: string
    closePercent: number
    trade_priceId: string
    trade_isBuy: boolean
    trade_margin: number
    trade_slippage: number
    trade_stopLoss: number
    trade_profitTarget: number
    trade_leverage: number
    trade_maxPercentagePnL: number
    trade_openPrice: number
    trade_liquidationPrice: number
    trade_executionBlock: number
    trade_user: string
    afterCloseTrade_settled: number
    afterCloseTrade_fees_fee: number
    afterCloseTrade_liquidationFee: number
    afterCloseTrade_oraclePrice: number
    onCloseTrade_closeNet: number
    onCloseTrade_grossPnL: number
    onCloseTrade_isLiquidated: boolean
    onCloseTrade_isStop: boolean
    onCloseTrade_fundingFee: number
    onCloseTrade_rolloverFee: number
    onCloseTrade_slippage: number
    onOrderUpdate_feeBase: number
    onOrderUpdate_accruedFee_fundingFee: number
    onOrderUpdate_accruedFee_rolloverFee: number
    onOrderUpdate_accruedFee_lastUpdate: number
    evt_block_number: number
    evt_block_time: string
    evt_tx_hash: string
  }>
}

export type CloseOrderEventsSubscriptionVariables = Exact<{
  where?: InputMaybe<Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Bool_Exp>
}>

export type CloseOrderEventsSubscription = {
  __typename?: "subscription_root"
  uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent: Array<{
    __typename?: "uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent"
    orderHash: string
    closePercent: number
    trade_priceId: string
    trade_isBuy: boolean
    trade_margin: number
    trade_slippage: number
    trade_stopLoss: number
    trade_profitTarget: number
    trade_leverage: number
    trade_maxPercentagePnL: number
    trade_openPrice: number
    trade_liquidationPrice: number
    trade_executionBlock: number
    trade_user: string
    afterCloseTrade_settled: number
    afterCloseTrade_fees_fee: number
    afterCloseTrade_liquidationFee: number
    afterCloseTrade_oraclePrice: number
    onCloseTrade_closeNet: number
    onCloseTrade_grossPnL: number
    onCloseTrade_isLiquidated: boolean
    onCloseTrade_isStop: boolean
    onCloseTrade_fundingFee: number
    onCloseTrade_rolloverFee: number
    onCloseTrade_slippage: number
    onOrderUpdate_feeBase: number
    onOrderUpdate_accruedFee_fundingFee: number
    onOrderUpdate_accruedFee_rolloverFee: number
    onOrderUpdate_accruedFee_lastUpdate: number
    evt_block_number: number
    evt_block_time: string
    evt_tx_hash: string
  }>
}

export type HistoricalPrices1MQueryVariables = Exact<{
  priceId: Scalars["bytea"]
}>

export type HistoricalPrices1MQuery = {
  __typename?: "query_root"
  pyth_prices_1m: Array<{
    __typename?: "pyth_prices_1m"
    aggregated_time: string
    close_price: number
    high_price: number
    high_ask_price: number
    high_ask_conf: number
    low_price: number
    low_bid_price: number
    low_bid_conf: number
  }>
}

export type HistoricalPrices15MQueryVariables = Exact<{
  priceId: Scalars["bytea"]
}>

export type HistoricalPrices15MQuery = {
  __typename?: "query_root"
  pyth_prices_15m: Array<{
    __typename?: "pyth_prices_15m"
    aggregated_time: string
    close_price: number
    high_price: number
    high_ask_price: number
    high_ask_conf: number
    low_price: number
    low_bid_price: number
    low_bid_conf: number
  }>
}

export type HistoricalPrices1hQueryVariables = Exact<{
  priceId: Scalars["bytea"]
}>

export type HistoricalPrices1hQuery = {
  __typename?: "query_root"
  pyth_prices_1h: Array<{
    __typename?: "pyth_prices_1h"
    aggregated_time: string
    close_price: number
    high_price: number
    high_ask_price: number
    high_ask_conf: number
    low_price: number
    low_bid_price: number
    low_bid_conf: number
  }>
}

export type HistoricalPrices1dQueryVariables = Exact<{
  priceId: Scalars["bytea"]
}>

export type HistoricalPrices1dQuery = {
  __typename?: "query_root"
  pyth_prices_1d: Array<{
    __typename?: "pyth_prices_1d"
    aggregated_time: string
    close_price: number
    high_price: number
    high_ask_price: number
    high_ask_conf: number
    low_price: number
    low_bid_price: number
    low_bid_conf: number
  }>
}

export type HistoricalPrices24hChangeQueryVariables = Exact<{
  priceId: Scalars["bytea"]
}>

export type HistoricalPrices24hChangeQuery = {
  __typename?: "query_root"
  pyth_prices_1h: Array<{
    __typename?: "pyth_prices_1h"
    aggregated_time: string
    open_price: number
    close_price: number
    high_price: number
    high_ask_price: number
    high_ask_conf: number
    low_price: number
    low_bid_conf: number
  }>
}

export type GetPriceBefore23HoursQueryVariables = Exact<{
  priceId: Scalars["bytea"]
  time: Scalars["timestamptz"]
}>

export type GetPriceBefore23HoursQuery = {
  __typename?: "query_root"
  pyth_prices_1m: Array<{
    __typename?: "pyth_prices_1m"
    aggregated_time: string
    open_price: number
  }>
}
