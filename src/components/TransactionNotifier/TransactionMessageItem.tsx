import { FC, ReactNode } from "react"
import { Path, Svg } from "react-native-svg"
import { wrapText } from "../../utils/reactHelpers/childrenHelpers"
import { HrefLink } from "../../utils/reactNavigationHelpers/HrefLink"
import {
  MessageItem,
  MessageItemBodyText,
  MessageItemProps,
} from "../MessageProvider/MessageItem"
import { useSpacing } from "../Themed/spacing"

export const TransactionMessageItem: FC<
  Omit<MessageItemProps, "title"> & {
    titleText: ReactNode
    transactionExplorerUrl?: string
  }
> = props => {
  const spacing = useSpacing()

  const title = props.transactionExplorerUrl ? (
    <HrefLink
      style={{ flexDirection: "row", alignItems: "center" }}
      href={props.transactionExplorerUrl}
    >
      <MessageItemBodyText>{props.titleText}</MessageItemBodyText>
      <Svg
        style={{ marginLeft: spacing(2.5) }}
        width="18"
        height="18"
        viewBox="0 0 18 18"
        fill="none"
      >
        <Path
          d="M15.75 9.75V17.25H0V3H9V4.5H1.5V15.75H14.25V9.75H15.75ZM18 0.75H9.759L12.7853 3.75L7.5525 9.0525L9.6735 11.1735L14.9062 5.871L18 9V0.75Z"
          opacity="0.5"
          fill="white"
        />
      </Svg>
    </HrefLink>
  ) : (
    <MessageItemBodyText>{props.titleText}</MessageItemBodyText>
  )

  const content =
    props.content != null && props.transactionExplorerUrl ? (
      <HrefLink href={props.transactionExplorerUrl}>
        {wrapText(props.content, {
          Text: MessageItemBodyText,
        })}
      </HrefLink>
    ) : (
      props.content
    )

  return <MessageItem {...props} title={title} content={content} />
}
