import { ComponentMeta } from "@storybook/react"
import { FC } from "react"
import { useIntl } from "react-intl"
import { View } from "react-native"
import { noop } from "../../utils/fnHelpers"
import { MessageItemOnCloseFnProvider } from "../MessageProvider/MessageItem"
import { TransactionMessageItem } from "./TransactionMessageItem"
import {
  confirmationsMessageItem,
  errorMessageItem,
  executedMessageItem,
  miningMessageItem,
} from "./_/messageItemPresets"

export default {
  title: "UI/TransactionNotifier/TransactionMessageItem",
  component: TransactionMessageItem,
  decorators: [
    Story => (
      <MessageItemOnCloseFnProvider onClose={noop}>
        <View style={{ margin: 10 }}>
          <Story />
        </View>
      </MessageItemOnCloseFnProvider>
    ),
  ],
} as ComponentMeta<typeof TransactionMessageItem>

export const MiningMessageItem: FC = () => {
  const intl = useIntl()
  return miningMessageItem(intl)
}

export const ConfirmationsMessageItem: FC = () => {
  const intl = useIntl()
  return confirmationsMessageItem(
    intl,
    "Some operation",
    "https://etherscan.io/tx/0x",
    33,
    100,
  )
}

export const ExecutedMessageItem: FC = () => {
  const intl = useIntl()
  return executedMessageItem(
    intl,
    "Some operation",
    "https://etherscan.io/tx/0x",
  )
}

export const ErrorMessageItem: FC = () => {
  const intl = useIntl()
  return errorMessageItem(
    intl,
    "Some operation",
    "ERROR_CODE",
    "https://etherscan.io/tx/0x",
  )
}
