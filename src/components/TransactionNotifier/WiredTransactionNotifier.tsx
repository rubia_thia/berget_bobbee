import { JsonRpcProvider } from "@ethersproject/providers"
import { ContractTransaction } from "ethers"
import { createContext, ReactNode, useContext, useMemo } from "react"
import { defineMessage, useIntl } from "react-intl"
import { useAppEnvStore } from "../../stores/AppEnvStore/useAppEnvStore"
import { useCurrencyStore } from "../../stores/CurrencyStore/useCurrencyStore"
import { FCC } from "../../utils/reactHelpers/types"
import { useMessage } from "../MessageProvider/MessageProvider"
import {
  confirmationsMessageItem,
  errorMessageItem,
  executedMessageItem,
  miningMessageItem,
} from "./_/messageItemPresets"

export interface ExecutedMessageItemRenderProps {
  explorerUrl: string
}
export type RenderExecutedMessageItemFn = (
  props: ExecutedMessageItemRenderProps,
) => JSX.Element

type TransactionNotifier = {
  broadcast(
    transaction: Promise<ContractTransaction>,
    operationTypeText: ReactNode,
  ): Promise<ContractTransaction>
  showConfirmationProgress(
    transaction: ContractTransaction,
    operationTypeText: ReactNode,
    options?: {
      renderExecutedMessageItem?: RenderExecutedMessageItemFn
      refresh?: () => void
    },
  ): Promise<void>
  broadcastAndShowConfirmation(
    transaction: Promise<ContractTransaction>,
    operationTypeText: ReactNode,
    options?: {
      renderExecutedMessageItem?: RenderExecutedMessageItemFn
      refresh?: () => void
    },
  ): Promise<void>
}

const WiredTransactionNotifierContext =
  createContext<TransactionNotifier | null>(null)

export const WiredTransactionNotifierProvider: FCC = props => {
  const message = useMessage()
  const appEnv = useAppEnvStore()
  const currency = useCurrencyStore()

  const intl = useIntl()

  const value = useMemo(
    (): TransactionNotifier => ({
      async broadcastAndShowConfirmation(
        transaction,
        operationType,
        options,
      ): Promise<void> {
        const broadcastTx = await this.broadcast(transaction, operationType)
        void this.showConfirmationProgress(broadcastTx, operationType, options)
      },
      async broadcast(
        transaction,
        operationType,
      ): Promise<ContractTransaction> {
        const dismiss = message.show({
          autoDismiss: null,
          message: intl.$t(
            defineMessage({
              defaultMessage: "Connecting to wallet...",
              description: "Operation Notification/connecting to wallet",
            }),
          ),
        })
        try {
          const result = await transaction
          dismiss()
          return result
        } catch (e) {
          dismiss()
          console.error(e)
          let errorMsg: string
          if (
            typeof e === "object" &&
            e != null &&
            "code" in e &&
            (e.code === "ACTION_REJECTED" || e.code === -32000) // metamask and wallet connect
          ) {
            throw new CancelError()
          } else if (!(e instanceof Error)) {
            errorMsg = "Unknown Error"
          } else if (!("reason" in e) || typeof e.reason !== "string") {
            errorMsg = e.message
          } else if (!e.reason.startsWith("UNW#")) {
            errorMsg = e.reason
          } else {
            errorMsg = errorCodes[e.reason.substring(4) as "0"]
          }
          message.show({
            message: errorMessageItem(intl, operationType, errorMsg, undefined),
          })
          throw new CancelError()
        }
      },
      async showConfirmationProgress(transaction, operationType, options) {
        if (transaction == null) return

        const key = `${transaction.hash}-${Date.now()}`

        const finalRenderExecutedMessageItem: RenderExecutedMessageItemFn =
          options?.renderExecutedMessageItem ??
          (p => executedMessageItem(intl, operationType, p.explorerUrl))

        message.show({
          key,
          autoDismiss: null,
          message: miningMessageItem(intl),
        })
        try {
          const provider = await waitFor(() => appEnv.publicJSONRPCProvider$)
          const jsonRPCTransaction = await retry(() =>
            provider
              .getTransaction(transaction.hash)
              .then(tx => tx ?? Promise.reject()),
          )
          await jsonRPCTransaction.wait()
          message.show({
            key,
            autoDismiss: null,
            message: confirmationsMessageItem(
              intl,
              operationType,
              appEnv.transactionExplorerUrl(transaction.hash),
              1,
              2,
            ),
          })
          await jsonRPCTransaction.wait(2)
          // message.show({
          //   key,
          //   autoDismiss: null,
          //   message: confirmationsMessageItem(
          //     intl,
          //     operationType,
          //     appEnv.transactionExplorerUrl(transaction.hash),
          //     2,
          //     2,
          //   ),
          // })
          // await jsonRPCTransaction.wait(3)
          message.show({
            key,
            message: finalRenderExecutedMessageItem({
              explorerUrl: appEnv.transactionExplorerUrl(transaction.hash),
            }),
          })
          await transaction.wait()
          if (typeof options?.refresh === "function") {
            options.refresh()
          } else {
            currency.refresh.refresh()
          }
        } catch (e) {
          const unwError = await getTransactionFailedReason(
            appEnv.publicJSONRPCProvider$,
            transaction,
          )
          message.show({
            key,
            message: errorMessageItem(
              intl,
              operationType,
              unwError ??
                (e instanceof Error && "code" in e && typeof e.code === "string"
                  ? e.code
                  : undefined),
              appEnv.transactionExplorerUrl(
                e instanceof Error && "transactionHash" in e
                  ? String(e.transactionHash)
                  : transaction.hash,
              ),
            ),
          })
          throw e
        }
      },
    }),
    [appEnv, currency, intl, message],
  )
  return (
    <WiredTransactionNotifierContext.Provider value={value}>
      {props.children}
    </WiredTransactionNotifierContext.Provider>
  )
}

export const useTransactionNotifier = (): TransactionNotifier => {
  const context = useContext(WiredTransactionNotifierContext)
  if (!context) {
    throw new Error(
      "useTransactionNotifier must be used within a WiredTransactionNotifierProvider",
    )
  }
  return context
}

import { waitFor } from "../../stores/utils/waitFor"
import { CancelError } from "../../utils/errorHelpers"
import { retry } from "../../utils/retry"
import errorCodes from "./contractErrorCodes.json"

async function getTransactionFailedReason(
  rpc: JsonRpcProvider,
  tx: ContractTransaction,
): Promise<string | undefined> {
  try {
    const data = await rpc.call(
      {
        ...tx,
        type: tx.type ?? undefined,
        maxFeePerGas: tx.gasPrice == null ? tx.maxFeePerGas : undefined,
        maxPriorityFeePerGas:
          tx.gasPrice == null ? tx.maxPriorityFeePerGas : undefined,
      },
      tx.blockNumber,
    )
    if (data.startsWith("0x08c379a0")) {
      const b = Buffer.from(data.substring(10), "hex")
      const offset = b.readBigUInt64BE(24)
      const length = b.readBigUInt64BE(24 + Number(offset))
      const contractError = b
        .subarray(Number(offset) + 32, Number(offset) + 32 + Number(length))
        .toString()
      if (contractError.startsWith("UNW#")) {
        return errorCodes[contractError.substring(4) as "0"]
      }
      return contractError
    }
    return undefined
  } catch (e) {
    return undefined
  }
}
