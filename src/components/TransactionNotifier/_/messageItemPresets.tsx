import { ReactNode } from "react"
import { defineMessage, IntlShape } from "react-intl"
import {
  MessageItemTitleText$Info,
  WarnIcon,
} from "../../MessageProvider/MessageItem"
import { TransactionMessageItem } from "../TransactionMessageItem"

export const errorMessageItem = (
  intl: IntlShape,
  operationTypeText: ReactNode,
  reasonText?: ReactNode,
  transactionUrl?: string,
): JSX.Element => (
  <TransactionMessageItem
    error={true}
    icon={<WarnIcon />}
    transactionExplorerUrl={transactionUrl}
    titleText={intl.$t(
      defineMessage({
        defaultMessage: `{operationType} failed`,
        description: "Operation Notification/title",
      }),
      { operationType: operationTypeText },
    )}
    content={
      reasonText
        ? intl.$t(
            defineMessage({
              defaultMessage: "Reason: {reasonText}",
              description: "Operation Notification/failure reason text",
            }),
            { reasonText },
          )
        : undefined
    }
  />
)

export const executedMessageItem = (
  intl: IntlShape,
  operationTypeText: ReactNode,
  transactionUrl: string,
): JSX.Element => (
  <TransactionMessageItem
    transactionExplorerUrl={transactionUrl}
    titleText={
      <MessageItemTitleText$Info>
        {intl.$t(
          defineMessage({
            defaultMessage: "{operationType} executed",
            description: "Operation Notification/title",
          }),
          { operationType: operationTypeText },
        )}
      </MessageItemTitleText$Info>
    }
  />
)

export const confirmationsMessageItem = (
  intl: IntlShape,
  operationTypeText: ReactNode,
  transactionUrl: string,
  progress: number,
  total: number,
): JSX.Element => (
  <TransactionMessageItem
    transactionExplorerUrl={transactionUrl}
    titleText={
      <MessageItemTitleText$Info>
        {intl.$t(
          defineMessage({
            defaultMessage: "{operationType} executing",
            description: "Operation Notification/title",
          }),
          { operationType: operationTypeText },
        )}
      </MessageItemTitleText$Info>
    }
    content={intl.$t(
      defineMessage({
        defaultMessage: "{progress}/{total} confirmations",
        description: "Operation Notification/confirmation progress text",
      }),
      { progress, total },
    )}
  />
)

export const miningMessageItem = (intl: IntlShape): JSX.Element => (
  <TransactionMessageItem
    titleText={intl.$t(
      defineMessage({
        defaultMessage: "Transaction mining...",
        description: "Operation Notification/title",
      }),
    )}
  />
)
