import { StyleProp, TextStyle } from "react-native"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { PositionStyle } from "../../utils/styleHelpers/PositionStyle"
import { SuspenseResource } from "../../utils/SuspenseResource"

export interface NumberInputRefValue {
  focus(): void
}

export interface NumberInputTextStyle {
  color?: TextStyle["color"]
  fontSize?: number
  lineHeight?: number
  textAlign?: TextStyle["textAlign"]
}

export interface NumberInputProps {
  style?: StyleProp<
    Partial<
      PositionStyle &
        NumberInputTextStyle & {
          flex: number
          outline: string
        }
    >
  >

  /**
   * @default 0
   */
  precision?: number

  /**
   * @default false
   */
  multiline?: boolean

  disabled?: boolean

  placeholder?: string

  onFocus?: () => void
  onBlur?: () => void

  value: SuspenseResource<null | BigNumber>
  onChange: (newValue: null | BigNumber) => void
}
