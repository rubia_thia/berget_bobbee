import { forwardRef, useImperativeHandle, useRef } from "react"
import { TextInput } from "react-native"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { safeReadResource } from "../../utils/SuspenseResource"
import { NumberInputProps, NumberInputRefValue } from "./types"

export { NumberInputProps, NumberInputRefValue } from "./types"

export const NumberInput = forwardRef<NumberInputRefValue, NumberInputProps>(
  (props, ref) => {
    const inputRef = useRef<TextInput>(null)

    useImperativeHandle(ref, () => ({
      focus() {
        inputRef.current?.focus()
      },
    }))

    const value = safeReadResource(props.value)
    return (
      <TextInput
        style={[props.style, { backgroundColor: "transparent" }]}
        keyboardType={"decimal-pad"}
        editable={!props.disabled}
        placeholder={props.placeholder}
        multiline={props.multiline ?? false}
        onFocus={props.onFocus}
        onBlur={props.onBlur}
        value={value == null ? "" : value.toString()}
        onChangeText={text => {
          if (props.disabled) return
          if (Number.isNaN(Number(text))) return
          if (text === "") {
            props.onChange(null)
          } else {
            props.onChange(
              BigNumber.setPrecision({ precision: props.precision }, text),
            )
          }
        }}
      />
    )
  },
)
