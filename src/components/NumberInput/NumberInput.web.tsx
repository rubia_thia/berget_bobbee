import IMask from "imask/esm/imask"
import "imask/esm/masked/number"
import { observer } from "mobx-react-lite"
import {
  forwardRef,
  MutableRefObject,
  Ref,
  RefObject,
  useCallback,
  useEffect,
  useImperativeHandle,
  useMemo,
  useRef,
} from "react"
import { StyleSheet, TextInput } from "react-native"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { composeCallbacks } from "../../utils/reactHelpers/composeCallbacks"
import { assignRef, useCombinedRef } from "../../utils/reactHelpers/refHelpers"
import { useIsFocusing } from "../../utils/reactHelpers/useIsFocusing"
import { useLatestValueRef } from "../../utils/reactHelpers/useLatestValueRef"
import { usePersistFn } from "../../utils/reactHelpers/usePersistFn"
import { safeReadResource } from "../../utils/SuspenseResource"
import { NumberInputProps, NumberInputRefValue } from "./types"

export { NumberInputProps } from "./types"
export { WrappedNumberInput as NumberInput }

const NumberInput = forwardRef<NumberInputRefValue, NumberInputProps>(
  (props, ref) => {
    const [isInputFocusing, useIsFocusingEventListeners] = useIsFocusing()
    const isInputFocusingRef = useLatestValueRef(isInputFocusing)

    const internalInputRef = useRef<HTMLInputElement>(null)

    useImperativeHandle(ref, () => ({
      focus() {
        internalInputRef.current?.focus()
      },
    }))

    const value = safeReadResource(props.value)

    const iMaskOptions = useMemo(
      () =>
        ({
          mask: Number,
          scale: props.precision,
          signed: false,
          thousandsSeparator: ",",
          padFractionalZeros: false,
          normalizeZeros: true,
          radix: ".",
          mapToRadix: ["."],
        } as IMask.MaskedNumberOptions),
      [props.precision],
    )
    const propOnChange = usePersistFn(props.onChange)
    const { ref: iMaskInputRef } = useIMask(
      value == null
        ? null
        : BigNumber.toNumber(
            BigNumber.round(
              { precision: props.precision, roundingMode: BigNumber.roundDown },
              value,
            ),
          ),
      iMaskOptions,
      {
        onAccept: useCallback(
          (imask: IMask.InputMask<typeof iMaskOptions>) => {
            const isChangedBecauseOfValueSetting = !isInputFocusingRef.current
            if (!isChangedBecauseOfValueSetting) {
              if (imask.value == "") {
                propOnChange?.(null)
                return
              }

              const num = BigNumber.safeFrom(imask.value.replace(/,/g, ""))
              propOnChange?.(
                num == null
                  ? // this case should not happen, cause imask should always only accept valid number
                    null
                  : num,
              )
            }
          },
          [isInputFocusingRef, propOnChange],
        ),
      },
    )

    const inputRef = useCombinedRef(
      internalInputRef,
      iMaskInputRef as Ref<HTMLInputElement>,
    )

    const inputEventListeners = composeCallbacks(useIsFocusingEventListeners, {
      onFocus: props.onFocus,
      onBlur: props.onBlur,
    })

    const refCallback = useCallback(
      (refValue: null | TextInput): void => {
        assignRef(refValue as any, inputRef)
      },
      [inputRef],
    )

    const style =
      props.style == null ? undefined : StyleSheet.flatten(props.style)

    return (
      <TextInput
        ref={refCallback}
        style={{
          minWidth: 0,
          border: "none",
          backgroundColor: "transparent",
          ...(style as any),
          textAlign: style?.textAlign === "auto" ? undefined : style?.textAlign,
          lineHeight:
            style?.lineHeight == null ? undefined : `${style.lineHeight}px`,
        }}
        {...inputEventListeners}
        keyboardType={"decimal-pad"}
        editable={!props.disabled}
        placeholder={props.placeholder}
        multiline={props.multiline ?? false}
      />
    )
  },
)
const WrappedNumberInput = observer(NumberInput)
WrappedNumberInput.displayName = "NumberInput"

type MaskableElement = HTMLInputElement | HTMLTextAreaElement
function useIMask<Opts extends IMask.AnyMaskedOptions = IMask.AnyMaskedOptions>(
  value: any,
  opts: Opts,
  callbacks: {
    onAccept?: (maskRef: IMask.InputMask<Opts>, e?: InputEvent) => void
    onComplete?: (maskRef: IMask.InputMask<Opts>, e?: InputEvent) => void
  } = {},
): {
  ref: MutableRefObject<null | MaskableElement>
  maskRef: RefObject<IMask.InputMask<Opts>>
} {
  const ref = useRef<null | MaskableElement>(null)
  const maskRef = useRef<null | IMask.InputMask<Opts>>(null)

  const cbOnAccept = callbacks.onAccept
  const onAccept = useCallback(
    (event?: InputEvent) => {
      if (!maskRef.current) return
      cbOnAccept?.(maskRef.current, event)
    },
    [cbOnAccept],
  )

  const cbOnComplete = callbacks.onComplete
  const onComplete = useCallback(
    (event?: InputEvent) => {
      if (!maskRef.current) return
      cbOnComplete?.(maskRef.current, event)
    },
    [cbOnComplete],
  )

  const destroyMask = useCallback(() => {
    maskRef.current?.destroy()
    maskRef.current = null
  }, [])

  // destroy instance on unmount
  useEffect(() => destroyMask, [destroyMask])

  useEffect(() => {
    const el = ref.current

    if (!el || !opts?.mask) {
      destroyMask()
      return
    }

    if (!maskRef.current) {
      maskRef.current = IMask(el, opts)
    } else {
      maskRef.current.updateOptions(opts)
    }
  }, [destroyMask, opts])

  // onAccept
  useEffect(() => {
    if (!ref.current || !maskRef.current) return

    maskRef.current.on("accept", onAccept)

    if (ref.current.defaultValue !== maskRef.current.value) {
      onAccept()
    }

    return () => {
      maskRef.current?.off("accept", onAccept)
    }
  }, [onAccept])

  // onComplete
  useEffect(() => {
    if (!ref.current || !maskRef.current) return

    maskRef.current.on("complete", onComplete)

    return () => {
      maskRef.current?.off("complete", onComplete)
    }
  }, [onComplete])

  useEffect(() => {
    if (maskRef.current == null) return
    if (value !== maskRef.current.typedValue) {
      maskRef.current.value = value ? String(value) : ""
    }
  }, [value])

  useEffect(() => destroyMask, [destroyMask])

  return {
    ref,
    maskRef,
  }
}
