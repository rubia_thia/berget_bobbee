import { ReactNode } from "react"
import { StyleProp, Text, TextProps, TextStyle, ViewStyle } from "react-native"
import { wrapText } from "../utils/reactHelpers/childrenHelpers"
import { FCC } from "../utils/reactHelpers/types"
import { styleGetters } from "../utils/reactHelpers/withProps/styleGetters"
import { withProps } from "../utils/reactHelpers/withProps/withProps"
import { CardBoxView } from "./CardBox/CardBox"
import { PopoverContent } from "./Popover/_/PopoverContent"
import { PopoverRoot, PopoverRootProps } from "./Popover/_/PopoverRoot"
import { PopoverTrigger } from "./Popover/_/PopoverTrigger"
import { useColors } from "./Themed/color"
import { useSpacing } from "./Themed/spacing"

export interface TooltipProps {
  style?: StyleProp<ViewStyle>
  content: ReactNode
  triggerMethod?: PopoverRootProps["triggerMethod"]
  dismissMethod?: PopoverRootProps["dismissMethod"]
}

export const Tooltip: FCC<TooltipProps> = props => {
  return (
    <PopoverRoot
      triggerMethod={props.triggerMethod}
      dismissMethod={props.dismissMethod}
    >
      <PopoverTrigger style={props.style}>{props.children}</PopoverTrigger>
      <PopoverContent>
        <CardBoxView className="p-2 max-w-[300px]" backgroundColor="black">
          {wrapText(props.content, {
            Text: TextInsideTooltip,
          })}
        </CardBoxView>
      </PopoverContent>
    </PopoverRoot>
  )
}

export const TextWithTooltippifiedStyle = withProps(
  styleGetters(({ colors }) => ({
    textDecorationStyle: "dotted",
    textDecorationLine: "underline",
    textDecorationColor: colors("gray-400"),
    cursor: "help",
  })),
  Text,
)

export const TextInsideTooltip: FCC<
  TextProps & { marginTop?: boolean }
> = props => {
  const colors = useColors()
  const spacing = useSpacing()

  return (
    <Text
      {...props}
      style={[
        {
          color: colors("white"),
          marginTop: props.marginTop ? spacing(3) : undefined,
        },
        props.style,
      ]}
    >
      {props.children}
    </Text>
  )
}

export const TooltippifiedText: FCC<
  TooltipProps & {
    textStyle?: StyleProp<TextStyle>
  }
> = props => {
  const { textStyle, children, ...tooltipProps } = props
  return (
    <Tooltip {...tooltipProps}>
      <TextWithTooltippifiedStyle style={textStyle}>
        {children}
      </TextWithTooltippifiedStyle>
    </Tooltip>
  )
}
