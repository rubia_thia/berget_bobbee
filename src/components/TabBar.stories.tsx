import { ComponentMeta } from "@storybook/react"
import { BackgroundColor, CardContainer } from "../../.storybook/decorators"
import { withTemplate } from "../../.storybook/utils"
import { TabBar, TabbarItem, TabBarProps } from "./TabBar"

export default {
  title: "UI/TabBar",
  component: TabBar,
  decorators: [
    CardContainer(
      { paddingTop: 6, paddingBottom: 16, paddingHorizontal: 16 },
      { backgroundColor: "#F3F4F6" },
    ),
    BackgroundColor(),
  ],
} as ComponentMeta<typeof TabBar>

const template = withTemplate<TabBarProps<1 | 2>>(TabBar, {
  style: {
    marginBottom: 12,
  },
  tabs: [
    {
      tab: p => (
        <TabbarItem isSelected={p.isSelected} count={10}>
          tab 1
        </TabbarItem>
      ),
      value: 1 as const,
    },
    {
      tab: p => <TabbarItem isSelected={p.isSelected}>tab 2</TabbarItem>,
      value: 2 as const,
    },
  ],
  children: ({ value }) => (
    <>{value === 1 ? "tab 1 content" : "tab 2 content"}</>
  ),
})

export const Normal = template()
