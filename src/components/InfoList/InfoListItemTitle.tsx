import { FC, useContext, useMemo } from "react"
import { StyleProp, Text, TextStyle, View, ViewStyle } from "react-native"
import { wrapText } from "../../utils/reactHelpers/childrenHelpers"
import { FCC, PropsWithChildren } from "../../utils/reactHelpers/types"
import { useColors } from "../Themed/color"
import { defaultFontSize } from "./_/defaultStyles"
import { InfoListDirectionContext } from "./_/InfoListDirectionContext"
import {
  InfoListRenderersContext,
  InfoListRenderersContextValue,
} from "./_/InfoListRenderersContext"

export const InfoListItemTitleTextStyleProvider: FCC<{
  style: StyleProp<TextStyle>
}> = props => {
  const ctxValue = useContext(InfoListRenderersContext)

  const newCtxValue: InfoListRenderersContextValue = useMemo(
    () => ({
      ...ctxValue,
      renderInfoListItemTitleText: p => (
        <DefaultInfoListItemTitleText {...p} style={[p.style, props.style]} />
      ),
    }),
    [ctxValue, props.style],
  )

  return (
    <InfoListRenderersContext.Provider value={newCtxValue}>
      {props.children}
    </InfoListRenderersContext.Provider>
  )
}

export interface InfoListItemTitleTextProps extends PropsWithChildren<unknown> {
  style?: StyleProp<TextStyle>
  numberOfLines?: number
}

export const InfoListItemTitleText: FC<InfoListItemTitleTextProps> = props => {
  const { renderInfoListItemTitleText } = useContext(InfoListRenderersContext)

  if (renderInfoListItemTitleText) {
    return <>{renderInfoListItemTitleText(props)}</>
  } else {
    return <DefaultInfoListItemTitleText {...props} />
  }
}

export const DefaultInfoListItemTitleText: FCC<
  InfoListItemTitleTextProps
> = props => {
  const colors = useColors()

  return (
    <Text
      style={[
        { color: colors("gray-500"), fontSize: defaultFontSize },
        props.style,
      ]}
      numberOfLines={props.numberOfLines}
    >
      {props.children}
    </Text>
  )
}

export interface InfoListItemTitleProps extends PropsWithChildren<unknown> {
  style?: StyleProp<ViewStyle>
}

export const InfoListItemTitle: FC<InfoListItemTitleProps> = props => {
  const { renderInfoListItemTitle } = useContext(InfoListRenderersContext)

  if (renderInfoListItemTitle) {
    return <>{renderInfoListItemTitle(props)}</>
  } else {
    return <DefaultInfoListItemTitle {...props} />
  }
}

export interface DefaultInfoListItemTitleProps extends InfoListItemTitleProps {
  textStyle?: StyleProp<TextStyle>
}

export const DefaultInfoListItemTitle: FC<
  DefaultInfoListItemTitleProps
> = props => {
  const { listItemDirection } = useContext(InfoListDirectionContext)

  return (
    <View style={[props.style, listItemDirection === "row" && { minWidth: 0 }]}>
      {wrapText(props.children, {
        oneLine: true,
        Text: InfoListItemTitleText,
      })}
    </View>
  )
}
