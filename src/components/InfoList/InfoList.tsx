import { useMemo } from "react"
import { StyleProp, View, ViewStyle } from "react-native"
import { RenderFn } from "../../utils/reactHelpers/childrenHelpers"
import { FCC } from "../../utils/reactHelpers/types"
import { ResponsiveValue, useResponsiveValue } from "../Themed/breakpoints"
import { InfoListItemProps } from "./InfoListItem"
import {
  InfoListItemDetailProps,
  InfoListItemDetailTextProps,
} from "./InfoListItemDetail"
import {
  InfoListItemTitleProps,
  InfoListItemTitleTextProps,
} from "./InfoListItemTitle"
import { InfoListProvider } from "./InfoListProvider"
import { InfoListDirectionContext } from "./_/InfoListDirectionContext"

export type ListItemDirection = "row" | "column" | "column-reverse"

export interface InfoListProps {
  style?: StyleProp<ViewStyle>

  inheritRenderers?: boolean

  /**
   * @default column
   */
  direction?: ResponsiveValue<"row" | "column">

  /**
   * @default row
   */
  listItemDirection?: ResponsiveValue<ListItemDirection>

  renderInfoListItem?: RenderFn<InfoListItemProps>
  renderInfoListItemTitle?: RenderFn<InfoListItemTitleProps>
  renderInfoListItemTitleText?: RenderFn<InfoListItemTitleTextProps>
  renderInfoListItemDetail?: RenderFn<InfoListItemDetailProps>
  renderInfoListItemDetailText?: RenderFn<InfoListItemDetailTextProps>
}

export const InfoList: FCC<InfoListProps> = props => {
  const direction = useResponsiveValue(props.direction) ?? "column"

  const listItemDirection = useResponsiveValue(props.listItemDirection) ?? "row"

  const directionCtxValue = useMemo(
    () => ({ listItemDirection }),
    [listItemDirection],
  )

  return (
    <InfoListProvider
      inheritRenderers={props.inheritRenderers}
      renderInfoListItem={props.renderInfoListItem}
      renderInfoListItemTitle={props.renderInfoListItemTitle}
      renderInfoListItemTitleText={props.renderInfoListItemTitleText}
      renderInfoListItemDetail={props.renderInfoListItemDetail}
      renderInfoListItemDetailText={props.renderInfoListItemDetailText}
    >
      <InfoListDirectionContext.Provider value={directionCtxValue}>
        <View style={[props.style, { flexDirection: direction }]}>
          {props.children}
        </View>
      </InfoListDirectionContext.Provider>
    </InfoListProvider>
  )
}
