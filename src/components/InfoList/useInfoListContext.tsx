import { useContext, useMemo } from "react"
import { ListItemDirection } from "./InfoList"
import { InfoListDirectionContext } from "./_/InfoListDirectionContext"

export interface AccessibleInfoListContext {
  listItemDirection: ListItemDirection
}

export const useInfoListContext = (): AccessibleInfoListContext => {
  const ctx = useContext(InfoListDirectionContext)

  return useMemo(
    () => ({ listItemDirection: ctx.listItemDirection }),
    [ctx.listItemDirection],
  )
}
