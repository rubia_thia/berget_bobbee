import { FC, useContext, useMemo } from "react"
import { StyleProp, Text, TextStyle, View, ViewStyle } from "react-native"
import { wrapText } from "../../utils/reactHelpers/childrenHelpers"
import { FCC, PropsWithChildren } from "../../utils/reactHelpers/types"
import { defaultFontSize } from "./_/defaultStyles"
import { InfoListDirectionContext } from "./_/InfoListDirectionContext"
import {
  InfoListRenderersContext,
  InfoListRenderersContextValue,
} from "./_/InfoListRenderersContext"

export const InfoListItemDetailTextStyleProvider: FCC<{
  style: StyleProp<TextStyle>
}> = props => {
  const ctxValue = useContext(InfoListRenderersContext)

  const newCtxValue: InfoListRenderersContextValue = useMemo(
    () => ({
      ...ctxValue,
      renderInfoListItemDetailText: p => (
        <DefaultInfoListItemDetailText {...p} style={[p.style, props.style]} />
      ),
    }),
    [ctxValue, props.style],
  )

  return (
    <InfoListRenderersContext.Provider value={newCtxValue}>
      {props.children}
    </InfoListRenderersContext.Provider>
  )
}

export interface InfoListItemDetailTextProps
  extends PropsWithChildren<unknown> {
  style?: StyleProp<TextStyle>
  numberOfLines?: number
}

export const InfoListItemDetailText: FCC<
  InfoListItemDetailTextProps
> = props => {
  const { renderInfoListItemDetailText } = useContext(InfoListRenderersContext)

  if (renderInfoListItemDetailText) {
    return <>{renderInfoListItemDetailText(props)}</>
  } else {
    return <DefaultInfoListItemDetailText {...props} />
  }
}

export const DefaultInfoListItemDetailText: FCC<
  InfoListItemDetailTextProps
> = props => {
  return (
    <Text
      style={[{ fontSize: defaultFontSize }, props.style]}
      numberOfLines={props.numberOfLines}
    >
      {props.children}
    </Text>
  )
}

export interface InfoListItemDetailProps extends PropsWithChildren<unknown> {
  style?: StyleProp<ViewStyle>

  /**
   * will be `left` when `ListItem` direction is `column`, and be `right` when
   * direction is `row`
   */
  alignSelf?: "left" | "right"
}

export const InfoListItemDetail: FC<InfoListItemDetailProps> = props => {
  const { renderInfoListItemDetail } = useContext(InfoListRenderersContext)

  if (renderInfoListItemDetail) {
    return <>{renderInfoListItemDetail(props)}</>
  } else {
    return <DefaultInfoListItemDetail {...props} />
  }
}

export interface DefaultInfoListItemDetailProps
  extends InfoListItemDetailProps {
  textStyle?: StyleProp<TextStyle>
}

export const DefaultInfoListItemDetail: FC<
  DefaultInfoListItemDetailProps
> = props => {
  const { listItemDirection } = useContext(InfoListDirectionContext)

  const alignSelf =
    props.alignSelf ?? listItemDirection === "column" ? "left" : "right"

  return (
    <View
      style={[props.style, alignSelf === "right" && { marginLeft: "auto" }]}
    >
      {wrapText(props.children, {
        oneLine: true,
        Text: InfoListItemDetailText,
      })}
    </View>
  )
}
