import { createContext } from "react"
import { RenderFn } from "../../../utils/reactHelpers/childrenHelpers"
import { InfoListItemProps } from "../InfoListItem"
import {
  InfoListItemDetailProps,
  InfoListItemDetailTextProps,
} from "../InfoListItemDetail"
import {
  InfoListItemTitleProps,
  InfoListItemTitleTextProps,
} from "../InfoListItemTitle"

export interface InfoListRenderersContextValue {
  renderInfoListItem?: undefined | RenderFn<InfoListItemProps>
  renderInfoListItemTitle?: undefined | RenderFn<InfoListItemTitleProps>
  renderInfoListItemTitleText?: undefined | RenderFn<InfoListItemTitleTextProps>
  renderInfoListItemDetail?: undefined | RenderFn<InfoListItemDetailProps>
  renderInfoListItemDetailText?:
    | undefined
    | RenderFn<InfoListItemDetailTextProps>
}

export const InfoListRenderersContext =
  createContext<InfoListRenderersContextValue>({})
