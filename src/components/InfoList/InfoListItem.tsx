import { FC, useContext } from "react"
import { StyleProp, View, ViewStyle } from "react-native"
import { PropsWithChildren } from "../../utils/reactHelpers/types"
import { InfoListDirectionContext } from "./_/InfoListDirectionContext"
import { InfoListRenderersContext } from "./_/InfoListRenderersContext"

export interface InfoListItemProps extends PropsWithChildren<unknown> {
  style?: StyleProp<ViewStyle>
}

export const InfoListItem: FC<InfoListItemProps> = props => {
  const { renderInfoListItem } = useContext(InfoListRenderersContext)

  if (renderInfoListItem) {
    return <>{renderInfoListItem(props)}</>
  } else {
    return <DefaultInfoListItem {...props} />
  }
}

export interface DefaultInfoListItemProps extends InfoListItemProps {}

export const DefaultInfoListItem: FC<DefaultInfoListItemProps> = props => {
  const { listItemDirection } = useContext(InfoListDirectionContext)

  return (
    <View
      style={[
        props.style,
        { flexDirection: listItemDirection },
        listItemDirection === "row" && {
          alignItems: "center",
        },
      ]}
    >
      {props.children}
    </View>
  )
}
