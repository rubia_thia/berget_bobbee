import { ComponentMeta, Story } from "@storybook/react"
import { BackgroundColor, CardContainer } from "../../../.storybook/decorators"
import { InfoList } from "./InfoList"
import { InfoListItem } from "./InfoListItem"
import { InfoListItemDetail } from "./InfoListItemDetail"
import {
  DefaultInfoListItemTitle,
  InfoListItemTitle,
} from "./InfoListItemTitle"

export default {
  title: "UI/InfoList",
  component: InfoList,
  decorators: [
    CardContainer({
      padding: 10,
      width: 500,
      maxWidth: "100%",
    }),
    BackgroundColor(),
  ],
} as ComponentMeta<typeof InfoList>

export const HorizontalList: Story = () => (
  <InfoList listItemDirection={"row"}>
    <InfoListItem>
      <InfoListItemTitle>Title</InfoListItemTitle>
      <InfoListItemDetail>Detail</InfoListItemDetail>
    </InfoListItem>
    <InfoListItem>
      <InfoListItemTitle>Title</InfoListItemTitle>
      <InfoListItemDetail>Detail</InfoListItemDetail>
    </InfoListItem>
    <InfoListItem>
      <InfoListItemTitle>Title</InfoListItemTitle>
      <InfoListItemDetail>Detail</InfoListItemDetail>
    </InfoListItem>
  </InfoList>
)

export const VerticalList: Story = () => (
  <InfoList listItemDirection={"column"}>
    <InfoListItem>
      <InfoListItemTitle>Title</InfoListItemTitle>
      <InfoListItemDetail>Detail</InfoListItemDetail>
    </InfoListItem>
    <InfoListItem>
      <InfoListItemTitle>Title</InfoListItemTitle>
      <InfoListItemDetail>Detail</InfoListItemDetail>
    </InfoListItem>
    <InfoListItem>
      <InfoListItemTitle>Title</InfoListItemTitle>
      <InfoListItemDetail>Detail</InfoListItemDetail>
    </InfoListItem>
  </InfoList>
)

export const ResponsiveDirection: Story = () => (
  <>
    <InfoList
      listItemDirection={{
        _: "column",
        sm: "row",
      }}
      renderInfoListItemTitle={p => (
        <DefaultInfoListItemTitle
          {...p}
          style={[p.style, { marginRight: 10 }]}
        />
      )}
    >
      <InfoListItem>
        <InfoListItemTitle>
          Very loooooooooooooooooooooooooooooooooong title
        </InfoListItemTitle>
        <InfoListItemDetail>
          Very loooooooooooooooooooooooooooooooooong detail
        </InfoListItemDetail>
      </InfoListItem>
      <InfoListItem>
        <InfoListItemTitle>Short title</InfoListItemTitle>
        <InfoListItemDetail>Short detail</InfoListItemDetail>
      </InfoListItem>
    </InfoList>
  </>
)

export const HorizontalRow: Story = () => (
  <InfoList direction={"row"} listItemDirection={"column"}>
    <InfoListItem>
      <InfoListItemTitle>Title</InfoListItemTitle>
      <InfoListItemDetail>Detail</InfoListItemDetail>
    </InfoListItem>
    <InfoListItem>
      <InfoListItemTitle>Title</InfoListItemTitle>
      <InfoListItemDetail>Detail</InfoListItemDetail>
    </InfoListItem>
    <InfoListItem>
      <InfoListItemTitle>Title</InfoListItemTitle>
      <InfoListItemDetail>Detail</InfoListItemDetail>
    </InfoListItem>
  </InfoList>
)

export const VerticalRow: Story = () => (
  <InfoList direction={"row"} listItemDirection={"row"}>
    <InfoListItem>
      <InfoListItemTitle>Title</InfoListItemTitle>
      <InfoListItemDetail>Detail</InfoListItemDetail>
    </InfoListItem>
    <InfoListItem>
      <InfoListItemTitle>Title</InfoListItemTitle>
      <InfoListItemDetail>Detail</InfoListItemDetail>
    </InfoListItem>
    <InfoListItem>
      <InfoListItemTitle>Title</InfoListItemTitle>
      <InfoListItemDetail>Detail</InfoListItemDetail>
    </InfoListItem>
  </InfoList>
)
