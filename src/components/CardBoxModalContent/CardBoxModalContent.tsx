import { Children, FC, isValidElement } from "react"
import { StyleProp, Text, TextStyle, View, ViewStyle } from "react-native"
import { Path, Svg } from "react-native-svg"
import { wrapText } from "../../utils/reactHelpers/childrenHelpers"
import { FCC } from "../../utils/reactHelpers/types"
import { styleGetters } from "../../utils/reactHelpers/withProps/styleGetters"
import { withProps } from "../../utils/reactHelpers/withProps/withProps"
import {
  PaddingStyle,
  useNormalizePaddingStyle,
} from "../../utils/styleHelpers/PaddingStyle"
import { checkObjEmpty } from "../../utils/typeHelpers"
import { PlainIconButtonVariant } from "../Button/PlainIconButtonVariant"
import { Button } from "../ButtonFramework/Button"
import { CardBox } from "../CardBox/CardBox"
import { Column, Columns, isColumnComponent } from "../Stack"
import { useSpacing } from "../Themed/spacing"

export interface CardBoxModalContentProps {
  style?: StyleProp<ViewStyle>

  padding?: number | PaddingStyle

  onClose?: () => void
}

export const CardBoxModalContent: FCC<CardBoxModalContentProps> = props => {
  const spacing = useSpacing()

  const paddingStyle = useNormalizePaddingStyle(props.padding)

  const iconSize = 16
  const iconPending = 4

  return (
    <CardBox>
      {({ style, ...rest }) => {
        checkObjEmpty(rest)

        return (
          <View style={[props.style, style, { width: "auto" }, paddingStyle]}>
            {props.children}

            {props.onClose && (
              <DefaultCloseButton
                style={{
                  position: "absolute",
                  top: (paddingStyle.paddingTop ?? spacing(4)) - iconPending,
                  right:
                    (paddingStyle.paddingRight ?? spacing(3.5)) - iconPending,
                }}
                iconPending={iconPending}
                size={iconSize}
                onPress={props.onClose}
              />
            )}
          </View>
        )
      }}
    </CardBox>
  )
}

export const TitleBar: FCC<{
  style?: StyleProp<ViewStyle>

  textStyle?: StyleProp<TextStyle>

  padding?: PaddingStyle
}> = props => {
  const paddingStyle = useNormalizePaddingStyle(props.padding)

  const children = wrapText(props.children, {
    Text: p => <TitleBarText {...p} style={props.textStyle} />,
  })

  return <View style={[props.style, paddingStyle]}>{children}</View>
}
export const TitleBarText = withProps(
  styleGetters(({}) => ({
    fontSize: 16,
    fontWeight: "600",
    textAlign: "center",
  })),
  Text,
)

export const ActionRowColumns: FCC<{
  style?: StyleProp<ViewStyle>
  gap?: number
}> = props => {
  const spacing = useSpacing()

  const children = Children.map(props.children, child => {
    if (!isValidElement(child)) return child
    if (isColumnComponent(child)) return
    return <Column key={child.key}>{child}</Column>
  })

  return (
    <Columns
      style={props.style}
      alignY={"center"}
      space={props.gap ?? spacing(4)}
    >
      {children}
    </Columns>
  )
}

export const DefaultCloseButton: FC<{
  style?: StyleProp<ViewStyle>
  iconStyle?: StyleProp<ViewStyle>
  /**
   * @default 4
   */
  iconPending?: number
  /**
   * @default 16
   */
  size?: number | string
  onPress: () => void
}> = props => {
  const size = props.size ?? 16
  const iconPending = props.iconPending ?? 4

  return (
    <Button
      style={props.style}
      Variant={variantProps => (
        <PlainIconButtonVariant
          {...variantProps}
          style={[variantProps.style, props.iconStyle]}
          iconLeft={
            <Svg
              style={{ padding: iconPending }}
              viewBox={"0 0 16 16"}
              width={size}
              height={size}
            >
              <Path d="M9.873 7.754l4.919-4.957a.549.549 0 000-.794l-.757-.795a.549.549 0 00-.794 0L8.284 6.165a.366.366 0 01-.53 0L2.797 1.17a.549.549 0 00-.794 0l-.795.795a.549.549 0 000 .794l4.957 4.957a.366.366 0 010 .53L1.17 13.24a.549.549 0 000 .794l.795.795a.549.549 0 00.794 0l4.957-4.957a.366.366 0 01.53 0l4.957 4.957a.549.549 0 00.794 0l.795-.795a.549.549 0 000-.794L9.873 8.284a.366.366 0 010-.53z" />
            </Svg>
          }
        />
      )}
      onPress={props.onPress}
    />
  )
}
