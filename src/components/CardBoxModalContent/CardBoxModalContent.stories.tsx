import { ComponentMeta } from "@storybook/react"
import { View } from "react-native"
import { BackgroundColor } from "../../../.storybook/decorators"
import { withTemplate } from "../../../.storybook/utils"
import { CardBoxModalContent } from "./CardBoxModalContent"

export default {
  title: "UI/CardBoxModalContent",
  component: CardBoxModalContent,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof CardBoxModalContent>

const template = withTemplate(CardBoxModalContent, {
  style: { margin: 10 },
  children: (
    <View
      style={{
        width: "100%",
        height: 300,
      }}
    />
  ),
})

export const Normal = template()
