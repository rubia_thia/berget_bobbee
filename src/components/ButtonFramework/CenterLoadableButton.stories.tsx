import { ComponentMeta, Story } from "@storybook/react"
import { useState } from "react"
import { View } from "react-native"
import { Button } from "./Button"
import { CenterLoadableButton } from "./CenterLoadableButton"

export default {
  title: "UI/ButtonFramework/CenterLoadableButton",
  component: CenterLoadableButton,
} as ComponentMeta<typeof CenterLoadableButton>

export const LoadableButton: Story = () => {
  const [loading, setLoading] = useState(false)

  return (
    <Button
      style={{ margin: 10 }}
      Variant={p => <CenterLoadableButton {...p} loading={loading} />}
      onPress={() => {
        setLoading(true)
        setTimeout(() => setLoading(false), 1000)
      }}
    >
      <View>
        <Button.Text>Loadable Button</Button.Text>
      </View>

      <View>
        <Button.Text>Loadable Button</Button.Text>
      </View>
    </Button>
  )
}
