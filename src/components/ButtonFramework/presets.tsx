import { ViewStyle } from "react-native"
import { ButtonTextStyle } from "./Button"

export const normalStyle: ViewStyle = {
  minWidth: 80,
  minHeight: 50,
  borderRadius: 25,
  paddingTop: 0,
  paddingBottom: 0,
  paddingLeft: 35,
  paddingRight: 35,
  flexDirection: "row",
  alignItems: "center",
  justifyContent: "center",
}

export const normalTextStyle: ButtonTextStyle = {
  // TODO
  // fontFamily: FontFamily.Bold,
  fontSize: 14,
  lineHeight: 22,
  letterSpacing: -0.34,
}
