import { FC, useState } from "react"
import { ActivityIndicator, View } from "react-native"
import { noop } from "../../utils/fnHelpers"
import { wrapText } from "../../utils/reactHelpers/childrenHelpers"
import { usePersistFn } from "../../utils/reactHelpers/usePersistFn"
import {
  Button,
  ButtonProps,
  ButtonText,
  ButtonTextStyleConsumer,
} from "./Button"

export interface SideLoadableButtonProps extends ButtonProps {
  loading?: undefined | boolean

  /**
   * @default white
   */
  activityIndicatorColor?: string
}

export function SideLoadableButton(
  props: SideLoadableButtonProps,
): JSX.Element {
  const { loading, activityIndicatorColor, ...restProps } = props

  const children = wrapText(props.children, { Text: ButtonText })

  return (
    <Button
      {...restProps}
      disabled={restProps.disabled || loading}
      onPress={props.loading ? noop : props.onPress}
    >
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <ButtonTextStyleConsumer>
          {style => (
            <ActivityIndicator
              style={{ opacity: loading ? 1 : 0, marginRight: 4 }}
              color={activityIndicatorColor ?? style.color ?? "white"}
              size={20}
            />
          )}
        </ButtonTextStyleConsumer>

        {children}

        <View style={{ paddingRight: 20 + 4 }} />
      </View>
    </Button>
  )
}

export const SmartSideLoadableButton: FC<
  Omit<SideLoadableButtonProps, "loading">
> = props => {
  const [loading, setLoading] = useState(false)

  const onPress = usePersistFn(async () => {
    if (loading) return
    try {
      setLoading(true)
      await props.onPress?.()
    } finally {
      setLoading(false)
    }
  })

  return <SideLoadableButton {...props} loading={loading} onPress={onPress} />
}
