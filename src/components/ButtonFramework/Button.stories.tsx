import { ComponentMeta } from "@storybook/react"
import { withTemplate } from "../../../.storybook/utils"
import { Button } from "./Button"

export default {
  title: "UI/ButtonFramework/Button",
  component: Button,
} as ComponentMeta<typeof Button>

const template = withTemplate(Button, {
  style: {
    margin: 10,
  },
})

export const NormalButton = template(p => {
  p.children = "Normal Size Button"
})

export const DisabledButton = template(p => {
  p.children = "Disabled Button"
  p.disabled = true
})
