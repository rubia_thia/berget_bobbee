import {
  createContext,
  FC,
  PropsWithChildren,
  useContext,
  useMemo,
} from "react"
import { Text, TextStyle } from "react-native"
import { FCC } from "../../../utils/reactHelpers/types"
import { styleSheetFlatCompose } from "../../../utils/styleHelpers/styleHelpers"

export interface ButtonTextStyle {
  fontFamily?: TextStyle["fontFamily"]
  fontWeight?: TextStyle["fontWeight"]
  fontSize?: TextStyle["fontSize"]
  lineHeight?: TextStyle["lineHeight"]
  letterSpacing?: TextStyle["letterSpacing"]
  color?: TextStyle["color"]
}

const defaultStyle: ButtonTextStyle = {}

const ButtonTextStyleContext = createContext<ButtonTextStyle>(defaultStyle)

export const ButtonTextStyleProvider: FCC<{
  style?: ButtonTextStyle
}> = props => {
  const parentLevelStyle = useButtonTextStyle()

  const style = useMemo(
    () => styleSheetFlatCompose(parentLevelStyle, props.style ?? defaultStyle),
    [parentLevelStyle, props.style],
  )

  return (
    <ButtonTextStyleContext.Provider value={style}>
      {props.children}
    </ButtonTextStyleContext.Provider>
  )
}

export const ButtonTextStyleConsumer: FC<{
  children: (style: ButtonTextStyle) => JSX.Element
}> = props => {
  return (
    <ButtonTextStyleContext.Consumer>
      {props.children}
    </ButtonTextStyleContext.Consumer>
  )
}

export const useButtonTextStyle = (): ButtonTextStyle => {
  return useContext(ButtonTextStyleContext)
}

export function ButtonText(
  props: PropsWithChildren<{ style?: ButtonTextStyle }>,
): JSX.Element {
  const style = useButtonTextStyle()

  return <Text style={[style, props.style]}>{props.children}</Text>
}
