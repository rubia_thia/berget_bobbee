import {
  ComponentType,
  createContext,
  PropsWithChildren,
  useContext,
} from "react"
import {
  AccessibilityRole,
  StyleProp,
  TouchableOpacity,
  ViewStyle,
} from "react-native"
import { FCC } from "../../../utils/reactHelpers/types"
import { ButtonTextStyleProvider } from "./ButtonText"

export interface ButtonVariantProps extends PropsWithChildren<unknown> {
  style?: StyleProp<ViewStyle>
  disabled?: undefined | boolean
  onPress?: undefined | (() => void)

  /**
   * If `href` is defined, the button is rendered as an anchor tag pointing to this URL.
   */
  href?: string
  /**
   * If `href` is defined, this prop defines related attributes to include on the anchor
   * (e.g., `download`, `rel`, `target`) which may modify its behavior.
   */
  hrefAttrs?: Record<string, any>

  accessibilityRole?: AccessibilityRole
}
export namespace ButtonVariantProps {
  export const pickCommonAriaProps = (
    props: ButtonVariantProps,
  ): {
    href?: string
    hrefAttrs?: Record<string, any>
    accessibilityRole?: AccessibilityRole
  } => {
    return {
      href: props.href,
      hrefAttrs: props.hrefAttrs,
      accessibilityRole: props.accessibilityRole,
    }
  }
}

const DefaultButtonVariantContext =
  createContext<ComponentType<ButtonVariantProps>>(DefaultButtonVariant)

export const DefaultButtonVariantProvider: FCC<{
  ButtonVariant: ComponentType<ButtonVariantProps>
}> = props => (
  <DefaultButtonVariantContext.Provider value={props.ButtonVariant}>
    {props.children}
  </DefaultButtonVariantContext.Provider>
)

export const useDefaultButtonVariant = (): ComponentType<ButtonVariantProps> =>
  useContext(DefaultButtonVariantContext)

export function DefaultButtonVariant(props: ButtonVariantProps): JSX.Element {
  return (
    <ButtonTextStyleProvider
      style={{
        fontSize: 16,
        color: "white",
      }}
    >
      <TouchableOpacity
        style={[
          {
            backgroundColor: "rgb(99, 102, 241)",
            borderRadius: 4,
            paddingTop: 15,
            paddingBottom: 15,
            paddingLeft: 35,
            paddingRight: 35,
            alignItems: "center",
            justifyContent: "center",
          },
          props.disabled && { opacity: 0.3 },
          props.style,
        ]}
        {...ButtonVariantProps.pickCommonAriaProps(props)}
        onPress={props.onPress}
        disabled={props.disabled}
      >
        {props.children}
      </TouchableOpacity>
    </ButtonTextStyleProvider>
  )
}
