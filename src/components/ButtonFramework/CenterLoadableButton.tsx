import { FC, useState } from "react"
import { ActivityIndicator, View } from "react-native"
import { noop } from "../../utils/fnHelpers"
import { wrapText } from "../../utils/reactHelpers/childrenHelpers"
import { usePersistFn } from "../../utils/reactHelpers/usePersistFn"
import { styleSheetFlatCompose } from "../../utils/styleHelpers/styleHelpers"
import {
  Button,
  ButtonProps,
  ButtonText,
  ButtonTextStyleConsumer,
  ButtonTextStyleProvider,
} from "./Button"

export interface CenterLoadableButtonProps
  extends Omit<ButtonProps, "onPress"> {
  loading?: boolean

  /**
   * @default white
   */
  activityIndicatorColor?: string

  onPress?: () => void | Promise<void>
}

export function CenterLoadableButton(
  props: CenterLoadableButtonProps,
): JSX.Element {
  const { loading, activityIndicatorColor, ...restProps } = props

  const children = wrapText(props.children, { Text: ButtonText })

  return (
    <Button
      {...restProps}
      disabled={restProps.disabled || loading}
      onPress={props.loading ? noop : props.onPress}
    >
      <ButtonTextStyleConsumer>
        {style => (
          <>
            <View
              style={{
                position: "absolute",
                top: 0,
                right: 0,
                bottom: 0,
                left: 0,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <ActivityIndicator
                style={{ opacity: loading ? 1 : 0 }}
                color={activityIndicatorColor ?? style.color ?? "white"}
                size={20}
              />
            </View>

            <ButtonTextStyleProvider
              style={styleSheetFlatCompose(style, {
                color: loading ? "transparent" : style.color,
              })}
            >
              {children}
            </ButtonTextStyleProvider>
          </>
        )}
      </ButtonTextStyleConsumer>
    </Button>
  )
}

export const SmartCenterLoadableButton: FC<
  CenterLoadableButtonProps
> = props => {
  const [loading, setLoading] = useState(false)

  const onPress = usePersistFn(async () => {
    if (loading) return
    try {
      setLoading(true)
      await props.onPress?.()
    } finally {
      setLoading(false)
    }
  })

  return (
    <CenterLoadableButton
      {...props}
      loading={props.loading || loading}
      onPress={onPress}
    />
  )
}
