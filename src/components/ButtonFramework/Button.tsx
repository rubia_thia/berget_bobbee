import { ComponentType } from "react"
import { dontObserver } from "../../utils/mobxHelpers"
import { wrapText } from "../../utils/reactHelpers/childrenHelpers"
import { ButtonText, ButtonTextStyle } from "./_/ButtonText"
import { ButtonVariantProps, useDefaultButtonVariant } from "./_/ButtonVariant"

export {
  ButtonText,
  ButtonTextStyleConsumer,
  ButtonTextStyleProvider,
  useButtonTextStyle,
} from "./_/ButtonText"
export type { ButtonTextStyle } from "./_/ButtonText"
export {
  ButtonVariantProps,
  DefaultButtonVariantProvider,
} from "./_/ButtonVariant"

export interface ButtonProps extends ButtonVariantProps {
  Variant?: ComponentType<ButtonVariantProps>
  renderVariant?: (props: ButtonVariantProps) => JSX.Element
}

export function Button(props: ButtonProps): JSX.Element {
  const DefaultButtonVariant = useDefaultButtonVariant()

  const {
    Variant: _Variant,
    renderVariant: _renderVariant,
    ...restProps
  } = props
  const renderVariant =
    _renderVariant ??
    dontObserver((props: ButtonVariantProps) => {
      const Variant = _Variant ?? DefaultButtonVariant
      return <Variant {...props} />
    })

  const children = wrapText(props.children, { Text: ButtonText })

  return renderVariant({ ...restProps, children })
}

export namespace Button {
  export type TextStyle = ButtonTextStyle

  export const Text = ButtonText
}
