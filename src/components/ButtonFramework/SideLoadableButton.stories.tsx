import { ComponentMeta, Story } from "@storybook/react"
import { useState } from "react"
import { Button } from "./Button"
import { SideLoadableButton } from "./SideLoadableButton"

export default {
  title: "UI/ButtonFramework/SideLoadableButton",
  component: SideLoadableButton,
} as ComponentMeta<typeof SideLoadableButton>

export const Normal: Story = () => {
  const [loading, setLoading] = useState(false)

  return (
    <Button
      style={{ margin: 10 }}
      Variant={props => <SideLoadableButton {...props} loading={loading} />}
      onPress={() => {
        setLoading(true)
        setTimeout(() => setLoading(false), 1000)
      }}
    >
      Loadable Button
    </Button>
  )
}
