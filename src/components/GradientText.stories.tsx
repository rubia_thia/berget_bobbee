import { ComponentMeta } from "@storybook/react"
import { withTemplate } from "../../.storybook/utils"
import { GradientText } from "./GradientText"

export default {
  title: "UI/GradientText",
  component: GradientText,
} as ComponentMeta<typeof GradientText>

const template = withTemplate(GradientText, {
  gradient: {
    colors: [
      { color: "#FFFFFF", offset: 0.0071 },
      { color: "#1D4ED8", opacity: 0.8, offset: 0.9929 },
    ],
  },
  children: "Hello",
})

export const Normal = template()
