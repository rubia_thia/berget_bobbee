import { ComponentMeta } from "@storybook/react"
import { FC } from "react"
import { Path, Svg, SvgProps } from "react-native-svg"
import { BackgroundColor, CardContainer } from "../../.storybook/decorators"
import { withTemplate } from "../../.storybook/utils"
import { Segment, SegmentControl } from "./SegmentControl"

export default {
  title: "UI/SegmentControl",
  component: SegmentControl,
  decorators: [
    CardContainer({
      margin: 10,
      padding: 10,
    }),
    BackgroundColor(),
  ],
} as ComponentMeta<typeof SegmentControl>

const template = withTemplate(SegmentControl, {
  children: (
    <>
      <Segment>First</Segment>
      <Segment active={true}>Second</Segment>
      <Segment disabled={true}>Third</Segment>
      <Segment active={true} disabled={true}>
        Fourth
      </Segment>
    </>
  ),
})

export const Normal = template()

export const WithIcon = template(p => {
  const Icon: FC<SvgProps> = props => (
    <Svg width="16" height="16" viewBox="0 0 16 16" fill="white" {...props}>
      <Path d="M7.03101 5.34513L9.27224 7.60946L12.28 4.59472H10.4437V3H15V7.56636H13.4515V5.82232L9.40924 9.94074L7.01023 7.51864L2.0552 12.4806L1 11.3823L7.03101 5.34513Z" />
    </Svg>
  )

  p.children = (
    <>
      <Segment icon={Icon}>First</Segment>
      <Segment icon={Icon} active={true}>
        Second
      </Segment>
      <Segment icon={Icon} disabled={true}>
        Third
      </Segment>
      <Segment icon={Icon} active={true} disabled={true}>
        Fourth
      </Segment>
    </>
  )
})
