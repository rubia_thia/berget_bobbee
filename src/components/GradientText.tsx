import MaskedView from "@react-native-masked-view/masked-view"
import { Text } from "react-native"
import { FCC } from "../utils/reactHelpers/types"
import { GradientTextProps } from "./GradientText.types"
import { LinearGradient } from "./LinearGradient"

export const GradientText: FCC<GradientTextProps> = props => {
  return (
    <MaskedView
      style={[{ flex: 1, flexDirection: "row", height: "100%" }, props.style]}
      maskElement={
        <Text style={props.textStyle} numberOfLines={props.numberOfLines}>
          {props.children}
        </Text>
      }
    >
      <LinearGradient
        style={{ flex: 1 }}
        colors={props.gradient.colors}
        direction={props.gradient.direction}
      />
    </MaskedView>
  )
}
