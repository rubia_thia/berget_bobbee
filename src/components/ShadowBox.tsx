import { FC, useMemo } from "react"
import { ShadowStyleIOS } from "react-native"

export type ShadowPropValue1 = [
  offsetX: number,
  offsetY: number,
  color?: string,
]
export type ShadowPropValue2 = [
  offsetX: number,
  offsetY: number,
  blurRadius: number,
  color?: string,
]
export type ShadowPropValue = ShadowPropValue1 | ShadowPropValue2

export interface ShadowBoxProps {
  shadow: Readonly<ShadowPropValue1 | ShadowPropValue2>
  children: (props: {
    style: ShadowStyleIOS & { elevation: number }
  }) => JSX.Element
}

export const ShadowBox: FC<ShadowBoxProps> = props => {
  const shadowStyle = useMemo(
    () => {
      if (props.shadow.length === 3) {
        const _shadow = props.shadow as ShadowPropValue1
        return boxShadow({
          offsetX: _shadow[0],
          offsetY: _shadow[1],
          color: _shadow[2],
        })
      } else {
        const _shadow = props.shadow as ShadowPropValue2
        return boxShadow({
          offsetX: _shadow[0],
          offsetY: _shadow[1],
          blurRadius: _shadow[2],
          color: _shadow[3],
        })
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [props.shadow.join(",")],
  )

  return <>{props.children({ style: shadowStyle })}</>
}

/**
 * React Native shadow style generator
 * port from styled-components:
 *  * https://github.com/styled-components/css-to-react-native/blob/3701eeec4b5f49b022b8a133253b3ba3d9324920/src/transforms/boxShadow.js
 *  * https://github.com/styled-components/css-to-react-native/blob/3701eeec4b5f49b022b8a133253b3ba3d9324920/src/transforms/util.js#L47
 */
export const boxShadow = (opts: {
  offsetX?: number
  offsetY?: number
  blurRadius?: number
  spreadRadius?: number
  inset?: boolean
  color?: string
}): ShadowStyleIOS & { elevation: number } => {
  const {
    color = "#000",
    offsetX,
    offsetY,
    blurRadius,
    // ignore rest opts
  } = opts

  return {
    shadowColor: color,
    shadowOffset: {
      width: offsetX ?? 0,
      height: offsetY ?? 0,
    },
    shadowOpacity: 1,
    shadowRadius: blurRadius ?? 0,
    elevation: 1,
  }
}
