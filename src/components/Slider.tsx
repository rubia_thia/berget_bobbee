import { Slider as RNSlider } from "@miblanchard/react-native-slider"
import { ComponentProps, ComponentType, FC, ReactNode } from "react"
import { StyleProp, Text, View, ViewStyle } from "react-native"
import { first, hasAny, init, last } from "../utils/arrayHelpers"
import { OneOrMore, Optional } from "../utils/types"
import { useColors } from "./Themed/color"

type RNSliderProps = ComponentProps<typeof RNSlider>

export type SliderTooltipContentProps = { value: number }

export interface SliderProps {
  style?: StyleProp<ViewStyle>

  value: number

  TooltipContent?: ComponentType<SliderTooltipContentProps>

  onChange?: (newValue: number) => void

  /**
   * @default 0
   */
  min?: number

  max: number

  segments?: [
    ...startAndMiddle: {
      endingPosition: number
      endingValue: number
      labelText?: ReactNode
      stepSize?: number
    }[],
    end: {
      stepSize?: number
    },
  ]

  disabled?: boolean
}

export const Slider: FC<SliderProps> = props => {
  const min = props.min ?? 0
  const max = props.max ?? 1
  const segments = normalizeSegments(min, max, props.segments)

  const matchingSegment = seekMatchingSegmentByValue(props.value, segments)
  const position = interpolate(
    {
      inputRange: [matchingSegment.startValue, matchingSegment.endValue],
      outputRange: [matchingSegment.startPosition, matchingSegment.endPosition],
    },
    props.value,
  )

  const onPositionChange: RNSliderProps["onValueChange"] = (position): void => {
    const _position = Array.isArray(position) ? first(position) ?? 0 : position
    const matchingSegment = seekMatchingSegmentByPosition(_position, segments)

    props.onChange?.(
      interpolate(
        {
          inputRange: [
            matchingSegment.startPosition,
            matchingSegment.endPosition,
          ],
          outputRange: [matchingSegment.startValue, matchingSegment.endValue],
        },
        _position,
      ),
    )
  }

  return (
    <View style={[props.style]}>
      <StyledRNSlider
        trackClickable={true}
        minimumValue={0}
        maximumValue={1}
        trackMarks={init(segments).map(s => s.endPosition)}
        segments={segments}
        value={position}
        onValueChange={onPositionChange}
      />
    </View>
  )
}

const StyledRNSlider: FC<
  Optional<RNSliderProps, "animationType"> & { segments: NormalizedSegment[] }
> = props => {
  const colors = useColors()

  const trackHeight = 6
  const containerHeight = 24
  const trackMarkContainerNegativeMargin = 12
  const trackMarkContainerWidth = 30
  const trackMarkLineHeight = 16
  const thumbSize = 20

  const trackMarkOccupiedHeight =
    (containerHeight - trackHeight) / 2 -
    trackMarkContainerNegativeMargin +
    trackMarkLineHeight

  return (
    <RNSlider
      trackStyle={{
        height: trackHeight,
        borderRadius: 9999,
        backgroundColor: colors("blue-50"),
      }}
      minimumTrackTintColor={colors("blue-600")}
      containerStyle={{
        marginBottom: trackMarkOccupiedHeight,
        height: containerHeight,
      }}
      renderThumbComponent={() => (
        <View
          style={{
            marginLeft: -(thumbSize / 2),
            width: thumbSize,
            height: thumbSize,
            borderRadius: 9999,
            borderWidth: 3,
            borderColor: colors("blue-600"),
            backgroundColor: colors("gray-50"),
          }}
        />
      )}
      renderTrackMarkComponent={idx => (
        <View
          style={{
            width: 1,
            height: trackHeight,
            backgroundColor: colors("gray-300", 0.5),
          }}
        >
          <Text
            style={{
              position: "absolute",
              width: trackMarkContainerWidth,
              top: containerHeight - trackMarkContainerNegativeMargin,
              left: "50%",
              marginLeft: -(trackMarkContainerWidth / 2),
              textAlign: "center",
              fontSize: 12,
              lineHeight: trackMarkLineHeight,
              color: colors("gray-500"),
            }}
            selectable={false}
          >
            {props.segments[idx]?.labelText}
          </Text>
        </View>
      )}
      {...props}
    />
  )
}

function interpolate(
  options: {
    inputRange: [min: number, max: number]
    outputRange: [min: number, max: number]
  },
  value: number,
): number {
  const [minInput, maxInput] = options.inputRange
  const [minOutput, maxOutput] = options.outputRange

  const position = (value - minInput) / (maxInput - minInput)
  return (maxOutput - minOutput) * position + minOutput
}

function seekMatchingSegmentByPosition(
  position: number,
  segments: OneOrMore<NormalizedSegment>,
): NormalizedSegment {
  if (position <= first(segments).endPosition) return first(segments)
  if (position > last(segments).startPosition) return last(segments)
  return segments.find(
    seg => seg.startPosition < position && position <= seg.endPosition,
  )!
}

function seekMatchingSegmentByValue(
  value: number,
  segments: OneOrMore<NormalizedSegment>,
): NormalizedSegment {
  if (value <= first(segments).endValue) return first(segments)
  if (value > last(segments).startValue) return last(segments)
  return segments.find(seg => seg.startValue < value && value <= seg.endValue)!
}

interface NormalizedSegment {
  startPosition: number
  endPosition: number
  startValue: number
  endValue: number
  stepSize: number
  labelText?: ReactNode
}
const normalizeSegments = (
  min: number,
  max: number,
  rawSegments: SliderProps["segments"],
): OneOrMore<NormalizedSegment> => {
  const defaultStepSize = 1

  const initParts = init(rawSegments ?? []).map(
    (segment, idx, segments): NormalizedSegment => {
      if (idx === 0) {
        return {
          startPosition: 0,
          endPosition: segment.endingPosition,
          startValue: min,
          endValue: segment.endingValue,
          stepSize: segment.stepSize ?? defaultStepSize,
          labelText: segment.labelText,
        }
      } else {
        return {
          startPosition: segments[idx - 1]!.endingPosition,
          endPosition: segment.endingPosition,
          startValue: segments[idx - 1]!.endingValue,
          endValue: segment.endingValue,
          stepSize: segment.stepSize ?? defaultStepSize,
          labelText: segment.labelText,
        }
      }
    },
  )

  if (!hasAny(initParts)) {
    return [
      {
        startPosition: 0,
        endPosition: 1,
        startValue: min,
        endValue: max,
        stepSize: defaultStepSize,
      },
    ]
  }

  return [
    ...initParts,
    {
      startPosition: last(initParts).endPosition,
      endPosition: 1,
      startValue: last(initParts).endValue,
      endValue: max,
      stepSize: last(rawSegments ?? [])?.stepSize ?? defaultStepSize,
    },
  ]
}
