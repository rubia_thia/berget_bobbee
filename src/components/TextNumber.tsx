import { FC, Suspense } from "react"
import { StyleProp, Text, TextStyle } from "react-native"
import { BigNumber } from "../utils/numberHelpers/BigNumber"
import { readResource, SuspenseResource } from "../utils/SuspenseResource"

export interface TextNumberProps extends TextNumberFormatOptions {
  style?: StyleProp<TextStyle>

  number: SuspenseResource<number | BigNumber>
}

export const TextNumber: FC<TextNumberProps> = props => {
  return (
    <Suspense fallback={<Text style={props.style}>-</Text>}>
      <TextNumberImpl {...props} />
    </Suspense>
  )
}

const TextNumberImpl: FC<TextNumberProps> = props => {
  const num = BigNumber.safeFrom(readResource(props.number))

  return (
    <Text style={props.style}>
      {num == null
        ? "-"
        : formatTextNumber(num, {
            precision: props.precision,
            padDecimals: props.padDecimals,
          })}
    </Text>
  )
}

export interface TextNumberFormatOptions {
  /**
   * @default 2
   */
  precision?: number

  /**
   * @default false
   */
  padDecimals?: boolean
}

export function formatTextNumber(
  n: BigNumber,
  options: TextNumberFormatOptions = {},
): string {
  const { padDecimals = false, precision = 2 } = options

  const formatter = new Intl.NumberFormat("en-US", {
    style: "decimal",
    maximumFractionDigits: precision,
    ...(padDecimals ? { minimumFractionDigits: precision } : {}),
  })

  if (BigNumber.getPrecision(n) <= precision) {
    return formatter.format(BigNumber.toNumber(n))
  }

  const roundDownCount = BigNumber.round(
    { precision: precision, roundingMode: BigNumber.roundDown },
    n,
  )
  const formattedIntegerPart = formatter
    .format(BigNumber.toNumber(roundDownCount))
    .split(".")[0]

  const formattedDecimalPart = BigNumber.getDecimalPart({ precision }, n)
  if (formattedDecimalPart == null || BigNumber.isZero(formattedDecimalPart)) {
    if (!padDecimals) {
      return String(formattedIntegerPart)
    } else {
      return `${formattedIntegerPart}.${"0".repeat(precision)}`
    }
  }

  return `${formattedIntegerPart}.${
    padDecimals
      ? formattedDecimalPart.padEnd(precision, "0")
      : formattedDecimalPart
  }`
}
