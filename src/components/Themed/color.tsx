import colorAlpha from "color-alpha"
import { createContext, useContext } from "react"
import { FCC } from "../../utils/reactHelpers/types"
import { usePersistFn } from "../../utils/reactHelpers/usePersistFn"

export interface ThemeColor {
  color(color: never): void
}

export type AnyColorsDefinition = Readonly<Record<string, string>>

const ColorsContext = createContext<null | AnyColorsDefinition>(null)

export const ColorsProvider: FCC<{
  colors: AnyColorsDefinition
}> = props => {
  return (
    <ColorsContext.Provider value={props.colors}>
      {props.children}
    </ColorsContext.Provider>
  )
}

export type ColorGetter = (
  color: Parameters<ThemeColor["color"]>[0],
  opacity?: number,
) => string

export const useColors = (): ColorGetter => {
  const palette = useContext(ColorsContext)

  if (palette == null) {
    throw new Error("ColorsProvider not found")
  }

  return usePersistFn((color, opacity) => {
    if (palette[color] == null) {
      throw new Error(`Color "${color}" not defined`)
    }

    if (opacity == null) {
      return palette[color]!
    } else {
      return setColorAlpha(palette[color]!, opacity)
    }
  })
}

export const setColorAlpha = (color: string, alpha: number): string => {
  return colorAlpha(color, alpha)
}
