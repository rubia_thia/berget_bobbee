import { FC, ReactNode } from "react"
import { useIntl } from "react-intl"
import { Text, TextStyle, View } from "react-native"
import { avbl$t } from "../../screens/TradeScreen/components/_/commonIntlMessages"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { SuspenseResource } from "../../utils/SuspenseResource"
import { TokenInfo } from "../../utils/TokenInfo"
import { TextTokenCount } from "../TextTokenCount"
import { useColors } from "../Themed/color"

export const BalanceTopArea: FC<{
  titleText: ReactNode
  token: SuspenseResource<TokenInfo>
  balance?: SuspenseResource<BigNumber>
}> = props => {
  const { $t } = useIntl()
  const colors = useColors()

  const textStyle: TextStyle = {
    color: colors("gray-500"),
    fontSize: 12,
    lineHeight: 16,
  }

  return (
    <View
      style={{
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
      }}
    >
      <Text style={textStyle}>{props.titleText}</Text>

      {props.balance != null && (
        <Text style={textStyle}>
          {$t(avbl$t)}{" "}
          <TextTokenCount token={props.token} count={props.balance} />
        </Text>
      )}
    </View>
  )
}
