import { FC, ReactNode, useCallback } from "react"
import { StyleProp, View, ViewStyle } from "react-native"
import { Path, Svg } from "react-native-svg"
import { noop } from "../../utils/fnHelpers"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import {
  readResource,
  safeReadResource,
  SuspenseResource,
} from "../../utils/SuspenseResource"
import { TokenInfo } from "../../utils/TokenInfo"
import { checkObjEmpty } from "../../utils/typeHelpers"
import { LightBlueRoundedButtonVariant } from "../Button/LightBlueRoundedButtonVariant"
import { PlainIconButtonVariant } from "../Button/PlainIconButtonVariant"
import { Button, ButtonVariantProps } from "../ButtonFramework/Button"
import { DropdownMenu, DropdownMenuItem } from "../DropdownMenu"
import { NumberInput } from "../NumberInput/NumberInput"
import {
  PopoverContent,
  PopoverRoot,
  PopoverTriggerRaw,
  PopoverTriggerRawRenderProps,
} from "../Popover/Popover"
import { Spensor } from "../Spensor"
import { Stack } from "../Stack"
import { useColors } from "../Themed/color"
import { useSpacing } from "../Themed/spacing"
import { TokenName } from "../TokenName"
import { InputStyleGuide } from "./InputStyleGuide"

interface TokenInputProps {
  style?: StyleProp<ViewStyle>
  paddingVertical?: number
  paddingHorizontal?: number

  error?: boolean
  readonly?: boolean

  topArea?: ReactNode
  placeholder?: string

  availableTokens: TokenInfo[]
  token: SuspenseResource<TokenInfo>
  onTokenChange?: (token: TokenInfo) => void

  value: SuspenseResource<null | BigNumber>
  onValueChange: (value: null | BigNumber) => void
  onPressMax?: SuspenseResource<(() => void) | undefined>
}

export const TokenInput: FC<TokenInputProps> = props => {
  const spacing = useSpacing()

  const { readonly = false } = props

  const token = safeReadResource(props.token)

  return (
    <InputStyleGuide
      error={!!props.error}
      readonly={readonly}
      hasValue={props.value != null}
    >
      {({
        textColor,
        borderColor,
        backgroundColor,
        onFocus,
        onBlur,
        ...rest
      }) => {
        checkObjEmpty(rest)
        return (
          <Stack
            style={[
              props.style,
              {
                paddingVertical: spacing(2.5),
                paddingHorizontal: spacing(3),
                borderRadius: 4,
                borderWidth: 1,
                borderColor,
                backgroundColor,
              },
            ]}
            space={spacing(2.5)}
          >
            {props.topArea}

            <View
              style={{
                minHeight: 28,
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <NumberInput
                style={{
                  flex: 1,
                  color: textColor,
                  fontSize: 24,
                  lineHeight: 32,
                  outline: "none",
                }}
                disabled={readonly}
                placeholder={props.placeholder}
                precision={
                  token != null
                    ? TokenInfo.getPrecision(token)
                    : TokenInfo.fallbackPrecision
                }
                value={props.value}
                onChange={readonly ? noop : props.onValueChange}
                onFocus={onFocus}
                onBlur={onBlur}
              />

              {props.onPressMax != null && (
                <Spensor>
                  {() => {
                    if (readonly) return null
                    const onPress = readResource(props.onPressMax)
                    if (onPress == null) {
                      return null
                    }
                    return (
                      <MaxButton
                        style={{ marginLeft: spacing(2.5) }}
                        onPress={onPress}
                      />
                    )
                  }}
                </Spensor>
              )}

              <Spensor>
                {() => (
                  <TokenSelector
                    style={{ marginLeft: spacing(2.5) }}
                    selectedToken={readResource(props.token)}
                    availableTokens={props.availableTokens}
                    onSelectedToken={props.onTokenChange}
                  />
                )}
              </Spensor>
            </View>
          </Stack>
        )
      }}
    </InputStyleGuide>
  )
}

const TokenSelector: FC<{
  style?: StyleProp<ViewStyle>
  availableTokens: TokenInfo[]
  selectedToken: TokenInfo
  onSelectedToken?: (token: TokenInfo) => void
}> = props => {
  const textStyle = {
    fontSize: 16,
    lineHeight: 21,
  }

  const { onSelectedToken } = props

  if (props.availableTokens.length <= 0 || onSelectedToken == null) {
    return (
      <View style={props.style}>
        <TokenName style={textStyle} token={props.selectedToken} />
      </View>
    )
  }

  return (
    <PopoverRoot triggerMethod={"press"} contentPlacement={"bottom-end"}>
      <PopoverTriggerRaw>
        {p => (
          <TokenSelectorTrigger
            style={props.style}
            textStyle={textStyle}
            token={props.selectedToken}
            popoverTriggerRawRenderProps={p}
          />
        )}
      </PopoverTriggerRaw>

      <PopoverContent>
        {({ close }) => (
          <DropdownMenu>
            {props.availableTokens.map((token, idx) => (
              <DropdownMenuItem
                key={idx}
                onPress={() => {
                  if (token !== props.selectedToken) {
                    onSelectedToken(token)
                  }
                  close()
                }}
              >
                <TokenName token={token} />
              </DropdownMenuItem>
            ))}
          </DropdownMenu>
        )}
      </PopoverContent>
    </PopoverRoot>
  )
}

interface TokenSelectorTriggerProps {
  style?: StyleProp<ViewStyle>
  textStyle?: Button.TextStyle
  token: TokenInfo
  popoverTriggerRawRenderProps?: PopoverTriggerRawRenderProps
}

const TokenSelectorTrigger: FC<TokenSelectorTriggerProps> = props => {
  const {
    ref,
    onPress,
    onLayout,
    onHoverOut,
    onHoverIn,
    isOpen: _isOpen,
    ...popoverTriggerRawRenderPropsRest
  } = props.popoverTriggerRawRenderProps ?? {}
  checkObjEmpty(popoverTriggerRawRenderPropsRest)

  const spacing = useSpacing()

  const Variant = useCallback(
    (p: ButtonVariantProps): JSX.Element => {
      const {
        style,
        disabled,
        onPress: buttonOnPress,
        children,
        href,
        hrefAttrs,
        accessibilityRole,
        ...rest
      } = p
      checkObjEmpty(rest)
      return (
        <PlainIconButtonVariant
          ref={ref}
          href={href}
          hrefAttrs={hrefAttrs}
          accessibilityRole={accessibilityRole}
          style={style}
          iconRight={
            <Svg width="16" height="16" viewBox="0 0 16 16" fill="black">
              <Path d="M8.27227 11.3335L2.45666 5.56098L3.39947 4.61816L8.27266 9.49133L13.1451 4.61892L14.0879 5.56172L8.27227 11.3335Z" />
            </Svg>
          }
          textStyle={{
            fontSize: 16,
            lineHeight: 21,
          }}
          gap={spacing(1)}
          disabled={disabled}
          onHoverIn={onHoverIn}
          onHoverOut={onHoverOut}
          onLayout={onLayout}
          onPress={() => {
            buttonOnPress?.()
            onPress?.()
          }}
        >
          {children}
        </PlainIconButtonVariant>
      )
    },
    [onHoverIn, onHoverOut, onLayout, onPress, ref, spacing],
  )

  return (
    <Button
      style={props.style}
      Variant={Variant}
      disabled={props.popoverTriggerRawRenderProps == null}
    >
      <TokenName style={props.textStyle} token={props.token} />
    </Button>
  )
}

const MaxButton: FC<{
  style?: StyleProp<ViewStyle>
  onPress: () => void
}> = props => {
  const spacing = useSpacing()
  const colors = useColors()

  const Variant = useCallback(
    (p: ButtonVariantProps) => (
      <LightBlueRoundedButtonVariant
        {...p}
        textStyle={{
          color: colors("blue-600"),
          fontSize: 12,
          lineHeight: 16,
        }}
        padding={{
          paddingHorizontal: spacing(1.5),
          paddingVertical: spacing(0.5),
        }}
      />
    ),
    [colors, spacing],
  )

  return (
    <Button style={props.style} Variant={Variant} onPress={props.onPress}>
      MAX
    </Button>
  )
}
