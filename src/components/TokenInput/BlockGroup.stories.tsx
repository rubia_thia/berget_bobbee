import { ComponentMeta } from "@storybook/react"
import { noop } from "lodash"
import { BackgroundColor, CardContainer } from "../../../.storybook/decorators"
import { withTemplate } from "../../../.storybook/utils"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../utils/TokenInfoPresets/TokenInfoPresets"
import { BlockGroup, BlockGroupDownArrowIcon } from "./BlockGroup"
import { TokenInput } from "./TokenInput"

export default {
  title: "UI/BlockGroup",
  component: BlockGroup,
  decorators: [CardContainer({ margin: 10, padding: 10 }), BackgroundColor()],
  subcomponents: { TokenInput, BlockGroupDownArrowIcon },
} as ComponentMeta<typeof BlockGroup>

const availableTokens = [TokenInfoPresets.MockBTC, TokenInfoPresets.MockBUSD]

const tpl = withTemplate(BlockGroup, {
  firstBlock: (
    <TokenInput
      availableTokens={availableTokens}
      token={availableTokens[0]!}
      value={BigNumber.from(10)}
      onValueChange={noop}
    />
  ),
  secondBlock: (
    <TokenInput
      availableTokens={availableTokens}
      token={availableTokens[1]!}
      value={BigNumber.from(10)}
      onValueChange={noop}
    />
  ),
})

export const Normal = tpl()

export const RowLayout = tpl(p => {
  p.direction = "row"
})

export const CustomIcon = tpl(p => {
  p.icon = <BlockGroupDownArrowIcon onPress={noop} />
})
