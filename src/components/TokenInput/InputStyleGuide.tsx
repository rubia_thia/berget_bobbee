import { FC, ReactNode, useMemo } from "react"
import {
  useIsFocusing,
  UseIsFocusingEventListeners,
} from "../../utils/reactHelpers/useIsFocusing"
import { useColors } from "../Themed/color"

export interface InputStyleGuideChildrenRenderProps
  extends UseIsFocusingEventListeners {
  textColor: string

  borderColor: string

  backgroundColor: string
}

interface InputStyleGuideProps {
  error: boolean
  readonly: boolean
  hasValue: boolean

  children: (renderProps: InputStyleGuideChildrenRenderProps) => ReactNode
}

export const InputStyleGuide: FC<InputStyleGuideProps> = props => {
  const colors = useColors()

  const [isFocusing, focusEventListeners] = useIsFocusing()

  const defaultBorderColor = colors("gray-200")

  const renderProps: InputStyleGuideChildrenRenderProps = useMemo(
    () => ({
      ...focusEventListeners,
      borderColor: defaultBorderColor,
      textColor: colors("gray-500"),
      backgroundColor: colors("white"),

      ...(!isFocusing ? {} : { borderColor: colors("blue-600") }),

      ...(!props.hasValue ? {} : { textColor: colors("gray-900") }),

      ...(!props.error
        ? {}
        : {
            textColor: colors("red-600"),
            borderColor: colors("red-600"),
            backgroundColor: colors("red-50"),
          }),

      ...(!props.readonly
        ? {}
        : {
            textColor: colors("gray-900"),
            borderColor: defaultBorderColor,
            backgroundColor: colors("gray-50"),
          }),
    }),
    [
      colors,
      defaultBorderColor,
      focusEventListeners,
      isFocusing,
      props.error,
      props.hasValue,
      props.readonly,
    ],
  )

  return <>{props.children(renderProps)}</>
}
