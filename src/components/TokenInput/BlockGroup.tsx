import { ComponentType, FC, ReactNode } from "react"
import {
  ColorValue,
  StyleProp,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import { Path, Svg, SvgProps } from "react-native-svg"
import {
  PropComponent,
  renderPropComponent,
} from "../../utils/reactHelpers/childrenHelpers"
import { FCC } from "../../utils/reactHelpers/types"
import { useColors } from "../Themed/color"
import { useSpacing } from "../Themed/spacing"

export const BlockGroup: FC<{
  style?: StyleProp<ViewStyle>

  /**
   * @default spacing(1)
   */
  gap?: number

  firstBlock: ReactNode

  secondBlock: ReactNode

  icon?: ReactNode

  /**
   * @default "column"
   */
  direction?: "row" | "column"
}> = props => {
  const spacing = useSpacing()

  const eachCellMargin = (props.gap ?? spacing(1)) / 2

  return (
    <View
      style={[
        { flexDirection: props.direction === "row" ? "row" : "column" },
        props.direction === "column" && { alignItems: "stretch" },
        props.style,
      ]}
    >
      <View style={{ alignItems: "stretch", marginBottom: eachCellMargin }}>
        {props.firstBlock}
      </View>
      {props.icon}
      <View style={{ alignItems: "stretch", marginTop: eachCellMargin }}>
        {props.secondBlock}
      </View>
    </View>
  )
}

export interface BlockGroupIconContainerIconProps {
  width: number
  height: number
}
export const BlockGroupIconContainer: FC<{
  /**
   * @default 32
   */
  size?: number
  icon: PropComponent.PL<BlockGroupIconContainerIconProps>
}> = props => {
  const { size = 32 } = props

  return (
    <View
      style={{
        alignItems: "center",
        justifyContent: "center",
        zIndex: 1,
      }}
    >
      <View style={{ position: "absolute" }}>
        {renderPropComponent(props.icon, { width: size, height: size })}
      </View>
    </View>
  )
}

export const BlockGroupIcon: FCC<{
  Icon: ComponentType<
    SvgProps | { fill?: ColorValue; width?: number; height?: number }
  >
  onPressIcon?: () => void
}> = props => {
  const spacing = useSpacing()
  const colors = useColors()

  const { Icon = PlusIcon } = props

  const padding = 4

  return (
    <BlockGroupIconContainer
      icon={cProps => (
        <View
          style={{
            padding: spacing(1),
            borderWidth: 1,
            borderColor: colors("gray-200"),
            borderRadius: 9999,
            backgroundColor: colors("white"),
          }}
        >
          <TouchableOpacity
            disabled={props.onPressIcon == null}
            onPress={props.onPressIcon}
          >
            <Icon
              fill={colors("black")}
              width={cProps.width - padding * 2 - 2}
              height={cProps.height - padding * 2 - 2}
            />
          </TouchableOpacity>
        </View>
      )}
    />
  )
}

export const BlockGroupPlusIcon: FC<{ onPress?: () => void }> = props => (
  <BlockGroupIcon Icon={PlusIcon} onPressIcon={props.onPress} />
)

export const BlockGroupDownArrowIcon: FC<{ onPress?: () => void }> = props => (
  <BlockGroupIcon Icon={DownArrowIcon} onPressIcon={props.onPress} />
)

const PlusIcon: FC<SvgProps> = props => (
  <Svg viewBox="0 0 24 24" {...props}>
    <Path d="M21 10.5h-7.5V3h-3v7.5H3v3h7.5V21h3v-7.5H21v-3z" />
  </Svg>
)

const DownArrowIcon: FC<SvgProps> = props => (
  <Svg viewBox="0 0 16 16" {...props}>
    <Path d="M8.27276 11.3335L2.45715 5.56098L3.39996 4.61816L8.27315 9.49133L13.1456 4.61892L14.0884 5.56172L8.27276 11.3335Z" />
  </Svg>
)
