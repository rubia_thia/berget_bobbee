import { ComponentMeta, Story } from "@storybook/react"
import { useState } from "react"
import { BackgroundColor, CardContainer } from "../../../.storybook/decorators"
import { noop } from "../../utils/fnHelpers"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../utils/TokenInfoPresets/TokenInfoPresets"
import { BalanceTopArea } from "./BalanceTopArea"
import { TokenInput } from "./TokenInput"

export default {
  title: "UI/TokenInput",
  component: TokenInput,
  decorators: [CardContainer({ margin: 10, padding: 10 }), BackgroundColor()],
} as ComponentMeta<typeof TokenInput>

const balance = BigNumber.from(136.4)
export const Normal: Story = () => {
  const [value, setValue] = useState<null | BigNumber>(null)

  return (
    <TokenInput
      topArea={
        <BalanceTopArea
          titleText={"Margin"}
          token={TokenInfoPresets.MockUSDC}
          balance={balance}
        />
      }
      placeholder={"0.0"}
      availableTokens={[TokenInfoPresets.MockBTC]}
      token={TokenInfoPresets.MockUSDC}
      value={value}
      onValueChange={setValue}
      onPressMax={() => {
        setValue(balance)
      }}
      onTokenChange={noop}
    />
  )
}

export const Error: Story = () => {
  return (
    <TokenInput
      error={true}
      topArea={
        <BalanceTopArea
          titleText={"Margin"}
          token={TokenInfoPresets.MockUSDC}
          balance={balance}
        />
      }
      placeholder={"0.0"}
      availableTokens={[TokenInfoPresets.MockBTC]}
      token={TokenInfoPresets.MockUSDC}
      value={BigNumber.from(111.11111111)}
      onValueChange={noop}
      onPressMax={noop}
      onTokenChange={noop}
    />
  )
}
