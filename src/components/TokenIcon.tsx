import { FC } from "react"
import { Image, StyleProp, View } from "react-native"
import { MarginStyle } from "../utils/styleHelpers/MarginStyle"
import { readResource, SuspenseResource } from "../utils/SuspenseResource"
import { TokenInfo } from "../utils/TokenInfo"
import { Spensor } from "./Spensor"

export interface TokenIconProps {
  style?: StyleProp<MarginStyle>

  token: SuspenseResource<TokenInfo>

  size: number
}

export const TokenIcon: FC<TokenIconProps> = props => {
  const { size } = props

  return (
    <View style={[{ width: size, height: size }, props.style]}>
      <Spensor>
        {() => (
          <Image
            style={{ width: size, height: size }}
            source={readResource(props.token).icon}
            resizeMode={"contain"}
          />
        )}
      </Spensor>
    </View>
  )
}
