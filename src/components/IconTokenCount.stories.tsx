import { ComponentMeta } from "@storybook/react"
import { withTemplate } from "../../.storybook/utils"
import { BigNumber } from "../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../utils/TokenInfoPresets/TokenInfoPresets"
import { IconTokenCount } from "./IconTokenCount"

export default {
  title: "UI/IconTokenCount",
  component: IconTokenCount,
} as ComponentMeta<typeof IconTokenCount>

const template = withTemplate(IconTokenCount, {
  token: TokenInfoPresets.MockBTC,
  count: BigNumber.from(100),
})

export const Standard = template()
