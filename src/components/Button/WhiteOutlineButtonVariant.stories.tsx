import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../.storybook/decorators"
import { withTemplate } from "../../../.storybook/utils"
import { Button } from "../ButtonFramework/Button"
import { WhiteOutlineButtonVariant } from "./WhiteOutlineButtonVariant"

export default {
  title: "UI/Button/WhiteOutlineButtonVariant",
  component: WhiteOutlineButtonVariant,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof WhiteOutlineButtonVariant>

const template = withTemplate(Button, {
  style: {
    margin: 10,
  },
  Variant: WhiteOutlineButtonVariant,
})

export const NormalButton = template(p => {
  p.children = "Normal Size Button"
})

export const DisabledButton = template(p => {
  p.children = "Disabled Button"
  p.disabled = true
})
