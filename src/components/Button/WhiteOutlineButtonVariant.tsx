import { ViewStyle } from "react-native"
import { FCC } from "../../utils/reactHelpers/types"
import {
  PaddingStyle,
  useNormalizePaddingStyle,
} from "../../utils/styleHelpers/PaddingStyle"
import {
  Button,
  ButtonTextStyleProvider,
  ButtonVariantProps,
} from "../ButtonFramework/Button"
import { Pressable } from "../Pressable"
import { useColors } from "../Themed/color"
import { useSelectByButtonState } from "./useButtonState"
import { useNormalButtonStyle } from "./useNormalButtonStyle"

export interface WhiteOutlineButtonVariantProps extends ButtonVariantProps {
  textStyle?: Button.TextStyle
  padding?: number | PaddingStyle
  borderRadius?: number
}

export const WhiteOutlineButtonVariant: FCC<
  WhiteOutlineButtonVariantProps
> = props => {
  const colors = useColors()

  const normal = useNormalButtonStyle({
    isCustomizedPadding: props.padding !== undefined,
  })

  const paddingStyle = useNormalizePaddingStyle(props.padding)

  const [stateRelatedStyle, eventListeners] = useSelectByButtonState<ViewStyle>(
    {
      disabled: props.disabled,
    },
    {
      normal: {
        backgroundColor: colors("white"),
      },
      hovering: {
        backgroundColor: "#e4e5e5",
      },
      pressing: {
        backgroundColor: "#cccccb",
      },
      disabled: {
        backgroundColor: colors("white"),
        opacity: 0.5,
      },
    },
  )

  return (
    <ButtonTextStyleProvider
      style={{
        ...normal.textStyle,
        color: colors("gray-900"),
        ...props.textStyle,
      }}
    >
      <Pressable
        {...eventListeners}
        {...ButtonVariantProps.pickCommonAriaProps(props)}
        style={[
          props.style,
          {
            alignItems: "center",
            justifyContent: "center",
            borderRadius: props.borderRadius ?? 6,
            borderWidth: 1,
            borderColor: colors("gray-300"),
          },
          normal.containerStyle,
          stateRelatedStyle,
          paddingStyle,
        ]}
        disabled={props.disabled}
        onPress={props.onPress}
      >
        {props.children}
      </Pressable>
    </ButtonTextStyleProvider>
  )
}
