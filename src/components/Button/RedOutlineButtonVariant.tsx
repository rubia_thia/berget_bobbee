import { ViewStyle } from "react-native"
import { FCC } from "../../utils/reactHelpers/types"
import {
  PaddingStyle,
  useNormalizePaddingStyle,
} from "../../utils/styleHelpers/PaddingStyle"
import {
  Button,
  ButtonTextStyleProvider,
  ButtonVariantProps,
} from "../ButtonFramework/Button"
import { Pressable } from "../Pressable"
import { useColors } from "../Themed/color"
import { useSelectByButtonState } from "./useButtonState"
import { useNormalButtonStyle } from "./useNormalButtonStyle"

export interface RedOutlineButtonVariantProps extends ButtonVariantProps {
  textStyle?: Button.TextStyle
  padding?: number | PaddingStyle
}

export const RedOutlineButtonVariant: FCC<
  RedOutlineButtonVariantProps
> = props => {
  const colors = useColors()

  const normal = useNormalButtonStyle({
    isCustomizedPadding: props.padding !== undefined,
  })

  const paddingStyle = useNormalizePaddingStyle(props.padding)

  const [stateRelatedStyle, eventListeners] = useSelectByButtonState<ViewStyle>(
    {
      disabled: props.disabled,
    },
    {
      normal: {
        backgroundColor: colors("red-500"),
      },
      hovering: {
        backgroundColor: colors("red-600"),
      },
      pressing: {
        backgroundColor: colors("red-700"),
      },
      disabled: {
        backgroundColor: colors("red-500"),
        opacity: 0.5,
      },
    },
  )

  return (
    <ButtonTextStyleProvider
      style={{
        ...normal.textStyle,
        color: colors("white"),
        ...props.textStyle,
      }}
    >
      <Pressable
        {...eventListeners}
        {...ButtonVariantProps.pickCommonAriaProps(props)}
        style={[
          props.style,
          {
            alignItems: "center",
            justifyContent: "center",
            borderRadius: 6,
            borderWidth: 1,
            borderColor: colors("red-600"),
          },
          normal.containerStyle,
          stateRelatedStyle,
          paddingStyle,
        ]}
        disabled={props.disabled}
        onPress={props.onPress}
      >
        {props.children}
      </Pressable>
    </ButtonTextStyleProvider>
  )
}
