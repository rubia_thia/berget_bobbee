import { useMemo } from "react"
import { noop } from "../../utils/fnHelpers"
import { composeCallbacks } from "../../utils/reactHelpers/composeCallbacks"
import {
  useIsHovering,
  UseIsHoveringEventListeners,
} from "../../utils/reactHelpers/useIsHovering"
import {
  useIsPressing,
  UseIsPressingEventListeners,
} from "../../utils/reactHelpers/useIsPressing"

export type ButtonState = "normal" | "hovering" | "pressing" | "disabled"

export interface UseButtonStateEventListeners
  extends UseIsHoveringEventListeners,
    UseIsPressingEventListeners {}

export const useButtonState = (
  contextInfo: { disabled?: boolean } = {},
): [state: ButtonState, eventListeners: UseButtonStateEventListeners] => {
  const [isHovering, hoverEventListeners] = useIsHovering()
  const [isPressing, pressEventListeners] = useIsPressing()

  const eventListeners: UseButtonStateEventListeners = useMemo(
    () => composeCallbacks(hoverEventListeners, pressEventListeners),
    [hoverEventListeners, pressEventListeners],
  )

  if (contextInfo.disabled) {
    return [
      "disabled",
      { onHoverIn: noop, onHoverOut: noop, onPressIn: noop, onPressOut: noop },
    ]
  }

  if (isPressing) {
    return ["pressing", eventListeners]
  }

  if (isHovering) {
    return ["hovering", eventListeners]
  }

  return ["normal", eventListeners]
}

export function selectByButtonState<T>(
  state: ButtonState,
  map: {
    [key in ButtonState]: T
  },
): T {
  return map[state]
}

export function useSelectByButtonState<T>(
  contextInfo: { disabled?: boolean } = {},
  map: {
    [key in ButtonState]: T
  },
): [selected: T, eventListeners: UseButtonStateEventListeners] {
  const [state, eventListeners] = useButtonState(contextInfo)
  return [selectByButtonState(state, map), eventListeners]
}
