import { ComponentMeta } from "@storybook/react"
import { withTemplate } from "../../../.storybook/utils"
import { Button } from "../ButtonFramework/Button"
import { GreenButtonVariant } from "./GreenButtonVariant"

export default {
  title: "Page/EarnScreen/GreenButtonVariant",
  component: GreenButtonVariant,
} as ComponentMeta<typeof GreenButtonVariant>

const template = withTemplate(Button, {
  style: {
    margin: 10,
  },
  Variant: GreenButtonVariant,
})

export const NormalButton = template(p => {
  p.children = "Normal Size Button"
})

export const DisabledButton = template(p => {
  p.children = "Disabled Button"
  p.disabled = true
})
