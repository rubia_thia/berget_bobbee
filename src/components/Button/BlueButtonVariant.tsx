import { FCC } from "../../utils/reactHelpers/types"
import { PaddingStyle } from "../../utils/styleHelpers/PaddingStyle"
import { Button, ButtonVariantProps } from "../ButtonFramework/Button"
import { useColors } from "../Themed/color"
import { ColorfulButtonVariant } from "./ColorfulButtonVariant"

export interface BlueButtonVariantProps extends ButtonVariantProps {
  textStyle?: Button.TextStyle
  padding?: number | PaddingStyle
  borderRadius?: number
}

export const BlueButtonVariant: FCC<BlueButtonVariantProps> = props => {
  const colors = useColors()

  return (
    <ColorfulButtonVariant
      {...props}
      bgColor={{
        normal: colors("blue-500"),
        hovering: colors("blue-600"),
        pressing: colors("blue-700"),
        disabled: colors("blue-500"),
      }}
    />
  )
}
