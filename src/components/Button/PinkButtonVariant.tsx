import { FCC } from "../../utils/reactHelpers/types"
import { ButtonVariantProps } from "../ButtonFramework/_/ButtonVariant"
import { useColors } from "../Themed/color"
import { ColorfulButtonVariant } from "./ColorfulButtonVariant"

export interface PinkButtonVariantProps extends ButtonVariantProps {
  padding?: number
}

export const PinkButtonVariant: FCC<PinkButtonVariantProps> = props => {
  const colors = useColors()

  return (
    <ColorfulButtonVariant
      {...props}
      bgColor={{
        normal: colors("pink-500"),
        hovering: colors("pink-600"),
        pressing: colors("pink-700"),
        disabled: colors("pink-500"),
      }}
    />
  )
}
