import { PaddingStyle } from "../../utils/styleHelpers/PaddingStyle"
import { Button } from "../ButtonFramework/Button"
import { useSpacing } from "../Themed/spacing"

export const useNormalButtonStyle = (contextInfo: {
  isCustomizedPadding?: boolean
}): {
  textStyle: Button.TextStyle
  containerStyle: PaddingStyle & { minHeight?: number }
} => {
  const spacing = useSpacing()

  return {
    textStyle: {
      fontSize: 14,
      fontWeight: "500",
    },

    containerStyle: {
      paddingVertical: spacing(2),
      paddingHorizontal: spacing(4),
      minHeight: contextInfo.isCustomizedPadding ? undefined : 48,
    },
  }
}
