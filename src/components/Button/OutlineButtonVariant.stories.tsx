import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../.storybook/decorators"
import { withTemplate } from "../../../.storybook/utils"
import { Button } from "../ButtonFramework/Button"
import { OutlineButtonVariant } from "./OutlineButtonVariant"

export default {
  title: "UI/Button/OutlineButtonVariant",
  component: OutlineButtonVariant,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof OutlineButtonVariant>

const template = withTemplate(Button, {
  style: {
    margin: 10,
  },
  Variant: OutlineButtonVariant,
})

export const NormalButton = template(p => {
  p.children = "Normal Size Button"
})

export const DisabledButton = template(p => {
  p.children = "Disabled Button"
  p.disabled = true
})
