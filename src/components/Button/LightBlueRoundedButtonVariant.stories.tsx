import { ComponentMeta } from "@storybook/react"
import { withTemplate } from "../../../.storybook/utils"
import { Button } from "../ButtonFramework/Button"
import { LightBlueRoundedButtonVariant } from "./LightBlueRoundedButtonVariant"

export default {
  title: "UI/Button/LightBlueRoundedButtonVariant",
  component: LightBlueRoundedButtonVariant,
} as ComponentMeta<typeof LightBlueRoundedButtonVariant>

const template = withTemplate(Button, {
  style: {
    margin: 10,
  },
  Variant: LightBlueRoundedButtonVariant,
})

export const NormalButton = template(p => {
  p.children = "Normal Size Button"
})

export const DisabledButton = template(p => {
  p.children = "Disabled Button"
  p.disabled = true
})
