import { ViewStyle } from "react-native"
import { FCC } from "../../utils/reactHelpers/types"
import {
  PaddingStyle,
  useNormalizePaddingStyle,
} from "../../utils/styleHelpers/PaddingStyle"
import {
  Button,
  ButtonTextStyleProvider,
  ButtonVariantProps,
} from "../ButtonFramework/Button"
import { Pressable } from "../Pressable"
import { selectByButtonState, useButtonState } from "./useButtonState"
import { useNormalButtonStyle } from "./useNormalButtonStyle"

export interface ColorfulButtonVariantColorSet {
  normal: string
  hovering: string
  pressing: string
  disabled: string
}

export interface ColorfulButtonVariantProps extends ButtonVariantProps {
  textStyle?: Button.TextStyle

  borderRadius?: number

  padding?: number | PaddingStyle

  textColor?: ColorfulButtonVariantColorSet

  bgColor: ColorfulButtonVariantColorSet
}

export const ColorfulButtonVariant: FCC<ColorfulButtonVariantProps> = props => {
  const [state, eventListeners] = useButtonState({
    disabled: props.disabled,
  })

  const normal = useNormalButtonStyle({
    isCustomizedPadding: props.padding !== undefined,
  })

  const paddingStyle = useNormalizePaddingStyle(props.padding)

  const stateRelatedStyle: { view: ViewStyle; text: Button.TextStyle } =
    selectByButtonState(state, {
      normal: {
        view: {
          backgroundColor: props.bgColor.normal,
        },
        text: {
          color: props.textColor?.normal ?? "white",
        },
      },
      hovering: {
        view: {
          backgroundColor: props.bgColor.hovering,
        },
        text: {
          color: props.textColor?.hovering ?? "white",
        },
      },
      pressing: {
        view: {
          backgroundColor: props.bgColor.pressing,
        },
        text: {
          color: props.textColor?.pressing ?? "white",
        },
      },
      disabled: {
        view: {
          backgroundColor: props.bgColor.disabled,
          opacity: 0.5,
        },
        text: {
          color: props.textColor?.disabled ?? "white",
        },
      },
    })

  return (
    <ButtonTextStyleProvider
      style={{
        ...normal.textStyle,
        ...props.textStyle,
        ...stateRelatedStyle.text,
      }}
    >
      <Pressable
        {...eventListeners}
        {...ButtonVariantProps.pickCommonAriaProps(props)}
        style={[
          props.style,
          {
            alignItems: "center",
            justifyContent: "center",
            borderRadius: props.borderRadius ?? 6,
          },
          normal.containerStyle,
          stateRelatedStyle.view,
          paddingStyle,
        ]}
        disabled={props.disabled}
        onPress={props.onPress}
      >
        {props.children}
      </Pressable>
    </ButtonTextStyleProvider>
  )
}
