import { ComponentMeta } from "@storybook/react"
import { FC } from "react"
import { View } from "react-native"
import { Path, Svg, SvgProps } from "react-native-svg"
import { withTemplate } from "../../../.storybook/utils"
import { Button } from "../ButtonFramework/Button"
import { PlainIconButtonVariant } from "./PlainIconButtonVariant"

export default {
  title: "UI/Button/PlainIconButtonVariant",
  component: PlainIconButtonVariant,
  decorators: [
    Story => (
      <View style={{ flex: 1, backgroundColor: "#eaeaea" }}>
        <Story />
      </View>
    ),
  ],
} as ComponentMeta<typeof PlainIconButtonVariant>

const template = withTemplate(Button, {
  style: {
    margin: 10,
  },
  Variant: props => (
    <PlainIconButtonVariant
      {...props}
      iconLeft={Icon}
      iconRight={props => <Icon rotation={180} {...props} />}
    />
  ),
})

export const NormalButton = template(p => {
  p.children = "Normal Size Button"
})

export const DisabledButton = template(p => {
  p.children = "Disabled Button"
  p.disabled = true
})

const Icon: FC<SvgProps> = props => (
  <Svg width="18" height="18" viewBox="0 0 18 18" fill="#C4C4C4" {...props}>
    <Path d="M9.28121 5.51242L3.28386 11.4653L4.25613 12.4376L9.28161 7.41217L14.3063 12.4368L15.2786 11.4646L9.28121 5.51242Z" />
  </Svg>
)
