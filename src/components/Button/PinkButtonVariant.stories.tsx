import { ComponentMeta } from "@storybook/react"
import { withTemplate } from "../../../.storybook/utils"
import { Button } from "../ButtonFramework/Button"
import { PinkButtonVariant } from "./PinkButtonVariant"

export default {
  title: "Page/EarnScreen/PinkButtonVariant",
  component: PinkButtonVariant,
} as ComponentMeta<typeof PinkButtonVariant>

const template = withTemplate(Button, {
  style: {
    margin: 10,
  },
  Variant: PinkButtonVariant,
})

export const NormalButton = template(p => {
  p.children = "Normal Size Button"
})

export const DisabledButton = template(p => {
  p.children = "Disabled Button"
  p.disabled = true
})
