import {
  cloneElement,
  ComponentType,
  forwardRef,
  isValidElement,
  ReactElement,
  Ref,
  useMemo,
} from "react"
import {
  ColorValue,
  LayoutChangeEvent,
  StyleProp,
  View,
  ViewStyle,
} from "react-native"
import { SvgProps } from "react-native-svg"
import { isChildrenEmpty } from "../../utils/reactHelpers/childrenHelpers"
import { composeCallbacks } from "../../utils/reactHelpers/composeCallbacks"
import { PropsWithChildren } from "../../utils/reactHelpers/types"
import {
  Button,
  ButtonTextStyle,
  ButtonTextStyleProvider,
  ButtonVariantProps,
} from "../ButtonFramework/Button"
import { Pressable } from "../Pressable"
import { useColors } from "../Themed/color"
import { useSpacing } from "../Themed/spacing"
import { selectByButtonState, useButtonState } from "./useButtonState"
import { useNormalButtonStyle } from "./useNormalButtonStyle"

type TextStyle = Button.TextStyle

export type PlainIconButtonVariantLayoutIcon = FillableTarget

export interface PlainIconButtonVariantLayoutProps {
  style?: StyleProp<ViewStyle>
  contentContainerStyle?: StyleProp<ViewStyle>

  iconLeft?: FillableTarget
  iconRight?: FillableTarget

  color?: ColorValue

  /**
   * @default spacing(1)
   */
  gap?: number
}

export const PlainIconButtonVariantLayout = forwardRef(
  (
    props: PropsWithChildren<PlainIconButtonVariantLayoutProps>,
    ref: Ref<View>,
  ) => {
    const spacing = useSpacing()

    const iconLeft = addFillAttribute(props.iconLeft, props.color)
    const iconRight = addFillAttribute(props.iconRight, props.color)

    const gap = props.gap ?? spacing(1)

    return (
      <View
        ref={ref}
        style={[props.style, { flexDirection: "row", alignItems: "center" }]}
      >
        {iconLeft}

        {!isChildrenEmpty(props.children) && (
          <View
            style={[
              {
                marginLeft: iconLeft == null ? 0 : gap,
                marginRight: iconRight == null ? 0 : gap,
              },
              props.contentContainerStyle,
            ]}
          >
            {props.children}
          </View>
        )}

        {iconRight}
      </View>
    )
  },
)
type FillableTarget = ReactElement<SvgProps> | ComponentType<SvgProps>
function addFillAttribute(
  Comp: undefined | FillableTarget,
  fillColor: undefined | ColorValue,
): undefined | ReactElement {
  if (Comp == null) return Comp

  return isValidElement(Comp) ? (
    cloneElement(Comp, { fill: fillColor })
  ) : (
    <Comp fill={fillColor} />
  )
}

export interface PlainIconButtonVariantProps extends ButtonVariantProps {
  textStyle?: TextStyle
  contentContainerStyle?: StyleProp<ViewStyle>

  iconLeft?: FillableTarget
  iconRight?: FillableTarget

  onHoverIn?: () => void
  onHoverOut?: () => void
  onLayout?: (event: LayoutChangeEvent) => void

  /**
   * @default 4
   */
  gap?: number
}

export const PlainIconButtonVariant = forwardRef(
  (props: PropsWithChildren<PlainIconButtonVariantProps>, ref: Ref<View>) => {
    const colors = useColors()

    const normal = useNormalButtonStyle({
      isCustomizedPadding: false,
    })

    const [state, eventListeners] = useButtonState({
      disabled: props.disabled,
    })

    const composedEventListeners = useMemo(
      () =>
        composeCallbacks(eventListeners, {
          onHoverIn: props.onHoverIn,
          onHoverOut: props.onHoverOut,
        }),
      [eventListeners, props.onHoverIn, props.onHoverOut],
    )

    const stateRelatedStyle: {
      container: ViewStyle
      text: ButtonTextStyle
    } = selectByButtonState(state, {
      normal: {
        container: {},
        text: {
          color: colors("gray-900"),
        },
      },
      hovering: {
        container: {},
        text: {
          color: colors("gray-600"),
        },
      },
      pressing: {
        container: {},
        text: {
          color: colors("blue-600"),
        },
      },
      disabled: {
        container: {
          opacity: 0.3,
        },
        text: {},
      },
    })

    const textStyle = {
      ...normal.textStyle,
      ...stateRelatedStyle.text,
      ...props.textStyle,
    }

    return (
      <ButtonTextStyleProvider style={textStyle}>
        <Pressable
          {...composedEventListeners}
          {...ButtonVariantProps.pickCommonAriaProps(props)}
          ref={ref}
          style={[props.style, stateRelatedStyle.container]}
          onPress={props.onPress}
          onLayout={props.onLayout}
          disabled={props.disabled}
        >
          <PlainIconButtonVariantLayout
            contentContainerStyle={props.contentContainerStyle}
            color={textStyle.color}
            iconLeft={props.iconLeft}
            iconRight={props.iconRight}
            gap={props.gap}
          >
            {props.children}
          </PlainIconButtonVariantLayout>
        </Pressable>
      </ButtonTextStyleProvider>
    )
  },
)
