import { ViewStyle } from "react-native"
import { FCC } from "../../utils/reactHelpers/types"
import {
  PaddingStyle,
  useNormalizePaddingStyle,
} from "../../utils/styleHelpers/PaddingStyle"
import {
  Button,
  ButtonTextStyleProvider,
  ButtonVariantProps,
} from "../ButtonFramework/Button"
import { Pressable } from "../Pressable"
import { useColors } from "../Themed/color"
import { selectByButtonState, useButtonState } from "./useButtonState"
import { useNormalButtonStyle } from "./useNormalButtonStyle"

export interface LightBlueRoundedButtonVariantProps extends ButtonVariantProps {
  textStyle?: Button.TextStyle
  padding?: number | PaddingStyle
}

export const LightBlueRoundedButtonVariant: FCC<
  LightBlueRoundedButtonVariantProps
> = props => {
  const colors = useColors()

  const normal = useNormalButtonStyle({
    isCustomizedPadding: props.padding !== undefined,
  })

  const paddingStyle = useNormalizePaddingStyle(props.padding)

  const [state, eventListeners] = useButtonState({
    disabled: props.disabled,
  })

  const stateRelatedStyle: ViewStyle = selectByButtonState(state, {
    normal: {
      backgroundColor: colors("blue-50"),
    },
    hovering: {
      backgroundColor: "#e3eaf2",
    },
    pressing: {
      backgroundColor: "#d7dde4",
    },
    disabled: {
      backgroundColor: colors("blue-50"),
      opacity: 0.5,
    },
  })

  return (
    <ButtonTextStyleProvider
      style={{
        ...normal.textStyle,
        color: colors("gray-900"),
        ...props.textStyle,
      }}
    >
      <Pressable
        {...eventListeners}
        {...ButtonVariantProps.pickCommonAriaProps(props)}
        style={[
          props.style,
          {
            alignItems: "center",
            justifyContent: "center",
            borderRadius: 999,
          },
          normal.containerStyle,
          stateRelatedStyle,
          paddingStyle,
        ]}
        disabled={props.disabled}
        onPress={props.onPress}
      >
        {props.children}
      </Pressable>
    </ButtonTextStyleProvider>
  )
}
