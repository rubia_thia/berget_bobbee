import { FCC } from "../../utils/reactHelpers/types"
import {
  PaddingStyle,
  useNormalizePaddingStyle,
} from "../../utils/styleHelpers/PaddingStyle"
import {
  Button,
  ButtonTextStyleProvider,
  ButtonVariantProps,
} from "../ButtonFramework/Button"
import { Pressable } from "../Pressable"
import { setColorAlpha, useColors } from "../Themed/color"
import { useSelectByButtonState } from "./useButtonState"
import { useNormalButtonStyle } from "./useNormalButtonStyle"

export interface OutlineButtonVariantProps extends ButtonVariantProps {
  textStyle?: Button.TextStyle
  padding?: number | PaddingStyle
  tintColor?: string
  backgroundColor?: string
}

export const OutlineButtonVariant: FCC<OutlineButtonVariantProps> = props => {
  const colors = useColors()

  const normal = useNormalButtonStyle({
    isCustomizedPadding: props.padding !== undefined,
  })

  const paddingStyle = useNormalizePaddingStyle(props.padding)

  const tintColor = props.tintColor ?? colors("gray-500")

  const [stateRelatedStyle, eventListeners] = useSelectByButtonState(
    {
      disabled: props.disabled,
    },
    {
      normal: {
        textColor: tintColor,
        borderColor: tintColor,
        backgroundColor: setColorAlpha(tintColor, 0),
        opacity: 1,
      },
      hovering: {
        textColor: tintColor,
        borderColor: tintColor,
        backgroundColor: setColorAlpha(tintColor, 0.1),
        opacity: 1,
      },
      pressing: {
        textColor: tintColor,
        borderColor: tintColor,
        backgroundColor: setColorAlpha(tintColor, 0.2),
        opacity: 1,
      },
      disabled: {
        textColor: tintColor,
        borderColor: tintColor,
        backgroundColor: setColorAlpha(tintColor, 0),
        opacity: 0.5,
      },
    },
  )

  return (
    <ButtonTextStyleProvider
      style={{
        ...normal.textStyle,
        color: stateRelatedStyle.textColor,
        ...props.textStyle,
      }}
    >
      <Pressable
        {...eventListeners}
        {...ButtonVariantProps.pickCommonAriaProps(props)}
        style={[
          props.style,
          {
            alignItems: "center",
            justifyContent: "center",
            borderRadius: 6,
            borderWidth: 1,
            borderColor: stateRelatedStyle.borderColor,
            opacity: stateRelatedStyle.opacity,
            backgroundColor: stateRelatedStyle.backgroundColor,
          },
          normal.containerStyle,
          paddingStyle,
        ]}
        disabled={props.disabled}
        onPress={props.onPress}
      >
        {props.children}
      </Pressable>
    </ButtonTextStyleProvider>
  )
}
