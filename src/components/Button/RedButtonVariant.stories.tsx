import { ComponentMeta } from "@storybook/react"
import { withTemplate } from "../../../.storybook/utils"
import { Button } from "../ButtonFramework/Button"
import { RedButtonVariant } from "./RedButtonVariant"

export default {
  title: "UI/Button/RedButtonVariant",
  component: RedButtonVariant,
} as ComponentMeta<typeof RedButtonVariant>

const template = withTemplate(Button, {
  style: {
    margin: 10,
  },
  Variant: RedButtonVariant,
})

export const NormalButton = template(p => {
  p.children = "Normal Size Button"
})

export const DisabledButton = template(p => {
  p.children = "Disabled Button"
  p.disabled = true
})
