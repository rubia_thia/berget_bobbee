import { ComponentMeta } from "@storybook/react"
import { withTemplate } from "../../../.storybook/utils"
import { Button } from "../ButtonFramework/Button"
import { ColorfulButtonVariant } from "./ColorfulButtonVariant"

export default {
  title: "UI/Button/ColorfulButtonVariant",
  component: ColorfulButtonVariant,
} as ComponentMeta<typeof ColorfulButtonVariant>

const template = withTemplate(Button, {
  style: {
    margin: 10,
  },
  Variant: p => (
    <ColorfulButtonVariant
      {...p}
      bgColor={{
        normal: "#16A34A",
        hovering: "#13803D",
        pressing: "#0F6A31",
        disabled: "#16A34A",
      }}
    />
  ),
})

export const NormalButton = template(p => {
  p.children = "Normal Size Button"
})

export const DisabledButton = template(p => {
  p.children = "Disabled Button"
  p.disabled = true
})
