import { ComponentType, FC, useMemo } from "react"
import { PaddingStyle } from "../../utils/styleHelpers/PaddingStyle"
import { useSpacing } from "../Themed/spacing"

type PropsRequired = {
  padding?: number | PaddingStyle
}

export function useCompactButtonVariant<P extends PropsRequired>(
  Comp: ComponentType<P>,
): keyof PropsRequired extends keyof P ? ComponentType<P> : never {
  const CompactComp = useMemo(() => {
    const CompactComp: FC<P> = props => {
      const spacing = useSpacing()

      return (
        <Comp
          padding={{
            paddingVertical: spacing(2),
            paddingHorizontal: spacing(4),
          }}
          {...(props as any)}
        />
      )
    }

    CompactComp.displayName = `Compact<${Comp.displayName ?? Comp.name}>`

    return CompactComp
  }, [Comp])

  return CompactComp as any
}
