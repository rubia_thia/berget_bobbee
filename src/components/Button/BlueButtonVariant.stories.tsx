import { ComponentMeta } from "@storybook/react"
import { withTemplate } from "../../../.storybook/utils"
import { Button } from "../ButtonFramework/Button"
import { BlueButtonVariant } from "./BlueButtonVariant"

export default {
  title: "UI/Button/BlueButtonVariant",
  component: BlueButtonVariant,
} as ComponentMeta<typeof BlueButtonVariant>

const template = withTemplate(Button, {
  style: {
    margin: 10,
  },
  Variant: BlueButtonVariant,
})

export const NormalButton = template(p => {
  p.children = "Normal Size Button"
})

export const DisabledButton = template(p => {
  p.children = "Disabled Button"
  p.disabled = true
})
