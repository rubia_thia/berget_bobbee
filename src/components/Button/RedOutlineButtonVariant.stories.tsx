import { ComponentMeta } from "@storybook/react"
import { withTemplate } from "../../../.storybook/utils"
import { Button } from "../ButtonFramework/Button"
import { RedOutlineButtonVariant } from "./RedOutlineButtonVariant"

export default {
  title: "UI/Button/RedOutlineButtonVariant",
  component: RedOutlineButtonVariant,
} as ComponentMeta<typeof RedOutlineButtonVariant>

const template = withTemplate(Button, {
  style: {
    margin: 10,
  },
  Variant: RedOutlineButtonVariant,
})

export const NormalButton = template(p => {
  p.children = "Normal Size Button"
})

export const DisabledButton = template(p => {
  p.children = "Disabled Button"
  p.disabled = true
})
