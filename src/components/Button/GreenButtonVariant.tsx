import { FCC } from "../../utils/reactHelpers/types"
import { ButtonVariantProps } from "../ButtonFramework/_/ButtonVariant"
import { useColors } from "../Themed/color"
import { ColorfulButtonVariant } from "./ColorfulButtonVariant"

export interface GreenButtonVariantProps extends ButtonVariantProps {
  padding?: number
}

export const GreenButtonVariant: FCC<GreenButtonVariantProps> = props => {
  const colors = useColors()

  return (
    <ColorfulButtonVariant
      {...props}
      bgColor={{
        normal: colors("green-600"),
        hovering: colors("green-700"),
        pressing: colors("green-800"),
        disabled: colors("green-600"),
      }}
    />
  )
}
