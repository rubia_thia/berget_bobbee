import { FCC } from "../../utils/reactHelpers/types"
import { ButtonVariantProps } from "../ButtonFramework/Button"
import { useColors } from "../Themed/color"
import { ColorfulButtonVariant } from "./ColorfulButtonVariant"

export interface RedButtonVariantProps extends ButtonVariantProps {
  padding?: number
}

export const RedButtonVariant: FCC<RedButtonVariantProps> = props => {
  const colors = useColors()

  return (
    <ColorfulButtonVariant
      {...props}
      bgColor={{
        normal: colors("red-500"),
        hovering: colors("red-600"),
        pressing: colors("red-700"),
        disabled: colors("red-500"),
      }}
    />
  )
}
