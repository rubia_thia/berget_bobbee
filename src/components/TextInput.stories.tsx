import { ComponentMeta, Story } from "@storybook/react"
import { useState } from "react"
import { BackgroundColor, CardContainer } from "../../.storybook/decorators"
import { withTemplate } from "../../.storybook/utils"
import { TextInput } from "./TextInput"

export default {
  title: "Page/TradeScreen/TextInput",
  component: TextInput,
  decorators: [CardContainer({ margin: 10, padding: 10 }), BackgroundColor()],
} as ComponentMeta<typeof TextInput>

const tpl = withTemplate(TextInput, {
  placeholder: "Price",
  value: "abc",
})

export const Normal: Story = () => {
  const [value, setValue] = useState<null | string>(null)

  return (
    <TextInput placeholder="Price" value={value} onValueChange={setValue} />
  )
}

export const Readonly = tpl(p => {
  p.onValueChange = undefined
})

export const Error = tpl(p => {
  p.error = true
})
