import { FC } from "react"
import {
  StyleProp,
  TextInput as RNTextInput,
  TextInputProps as RNTextInputProps,
  TextStyle,
  View,
} from "react-native"
import {
  PaddingStyle,
  useNormalizePaddingStyle,
} from "../utils/styleHelpers/PaddingStyle"
import { safeReadResource, SuspenseResource } from "../utils/SuspenseResource"
import { checkObjEmpty } from "../utils/typeHelpers"
import { useSpacing } from "./Themed/spacing"
import { InputStyleGuide } from "./TokenInput/InputStyleGuide"

export interface TextInputProps {
  style?: RNTextInputProps["style"]
  textStyle?: StyleProp<TextStyle>
  padding?: number | PaddingStyle
  error?: boolean
  textAlign?: RNTextInputProps["textAlign"]
  placeholder?: string
  placeholderTextColor?: string
  /**
   * @default false
   */
  multiline?: boolean
  value: SuspenseResource<null | string>
  onValueChange?: (newValue: null | string) => void
  autoCapitalize?: "none" | "sentences" | "words" | "characters" | undefined
}

export const TextInput: FC<TextInputProps> = props => {
  const spacing = useSpacing()

  const paddingStyle = useNormalizePaddingStyle(props.padding)

  return (
    <InputStyleGuide
      hasValue={safeReadResource(props.value) != null}
      readonly={props.onValueChange == null}
      error={!!props.error}
    >
      {({
        textColor,
        borderColor,
        backgroundColor,
        onFocus,
        onBlur,
        ...rest
      }) => {
        checkObjEmpty(rest)
        const { onValueChange } = props

        const textStyle: TextStyle = {
          paddingLeft: spacing(3),
          paddingRight: spacing(3),
          paddingTop: spacing(2),
          paddingBottom: spacing(2),
          fontSize: 16,
          lineHeight: 24,
          textAlign: props.textAlign ?? "center",
          color: textColor,
          ...paddingStyle,
        }

        return (
          <View
            style={[
              props.style,
              {
                overflow: "hidden",
                borderRadius: 6,
                borderWidth: 1,
                borderColor,
                backgroundColor,
              },
            ]}
          >
            <RNTextInput
              style={[
                { borderWidth: 0, outline: 0 } as any,
                textStyle,
                props.textStyle,
                { textAlign: props.textAlign },
              ]}
              onFocus={onFocus}
              onBlur={onBlur}
              multiline={props.multiline}
              placeholder={props.placeholder}
              placeholderTextColor={props.placeholderTextColor}
              value={safeReadResource(props.value) ?? ""}
              onChange={e => onValueChange?.(e.nativeEvent.text || null)}
              autoCapitalize={props.autoCapitalize}
            />
          </View>
        )
      }}
    </InputStyleGuide>
  )
}
