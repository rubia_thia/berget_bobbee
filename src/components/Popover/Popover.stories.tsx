import { Meta, Story } from "@storybook/react"
import { ScrollView, Text, View } from "react-native"
import { BackgroundColor, CardContainer } from "../../../.storybook/decorators"
import { CardBoxView } from "../CardBox/CardBox"
import {
  PopoverContent as Content,
  PopoverRoot as Root,
  PopoverScrollViewPropsProvider,
  PopoverTrigger as Trigger,
} from "./Popover"

export default {
  title: "UI/Popover",
  decorators: [
    CardContainer({
      padding: 10,
    }),
    BackgroundColor(),
  ],
} as Meta

export const Normal: Story = () => (
  <Root triggerMethod={"press"}>
    <Trigger style={{ alignSelf: "flex-start", width: "auto" }}>
      <Text>Open Popover</Text>
    </Trigger>

    <Content>
      <CardBoxView style={{ padding: 10, backgroundColor: "#eaeaea" }}>
        <Text>Some text</Text>
      </CardBoxView>
    </Content>
  </Root>
)

export const Hover: Story = () => (
  <Root triggerMethod={"hover"}>
    <Trigger style={{ alignSelf: "flex-start", width: "auto" }}>
      <Text>Open Popover</Text>
    </Trigger>

    <Content>
      <CardBoxView style={{ padding: 10, backgroundColor: "#eaeaea" }}>
        <Text>Some text</Text>
      </CardBoxView>
    </Content>
  </Root>
)

export const InScrollView: Story = () => (
  <PopoverScrollViewPropsProvider>
    {scrollViewProps => (
      <ScrollView {...scrollViewProps}>
        <View style={{ height: 400, backgroundColor: "#eaeaea" }} />
        <Root triggerMethod={"press"} dismissMethod={["press-trigger"]}>
          <Trigger style={{ alignSelf: "flex-start", width: "auto" }}>
            <Text>Open Popover 1</Text>
          </Trigger>

          <Content>
            <CardBoxView style={{ padding: 10, backgroundColor: "#eaeaea" }}>
              <Text>Some text 1</Text>
            </CardBoxView>
          </Content>
        </Root>
        <View style={{ height: 400, backgroundColor: "#eaeaea" }} />
        <Root triggerMethod={"press"} dismissMethod={["press-trigger"]}>
          <Trigger style={{ alignSelf: "flex-start", width: "auto" }}>
            <Text>Open Popover 2</Text>
          </Trigger>

          <Content>
            <CardBoxView style={{ padding: 10, backgroundColor: "#eaeaea" }}>
              <Text>Some text 2</Text>
            </CardBoxView>
          </Content>
        </Root>
        <View style={{ height: 400, backgroundColor: "#eaeaea" }} />
      </ScrollView>
    )}
  </PopoverScrollViewPropsProvider>
)
