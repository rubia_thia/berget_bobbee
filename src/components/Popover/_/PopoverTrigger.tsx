import { FC, Fragment, ReactNode, Ref, useContext, useMemo } from "react"
import { LayoutChangeEvent, StyleProp, View, ViewStyle } from "react-native"
import { FCC } from "../../../utils/reactHelpers/types"
import {
  useHoveringCallback,
  UseIsHoveringEventListeners,
} from "../../../utils/reactHelpers/useIsHovering"
import { usePersistFn } from "../../../utils/reactHelpers/usePersistFn"
import { Pressable } from "../../Pressable"
import { PopoverRootContext } from "./PopoverRootContext"

export interface PopoverTriggerProps {
  style?: StyleProp<ViewStyle>
}

export const PopoverTrigger: FCC<PopoverTriggerProps> = props => {
  return (
    <PopoverTriggerRaw>
      {p => (
        <View ref={p.ref} style={props.style} onLayout={p.onLayout}>
          <Pressable
            {...p}
            shouldReceiveBubbledHoverEvent={true}
            ref={undefined}
            onLayout={undefined}
          >
            {props.children}
          </Pressable>
        </View>
      )}
    </PopoverTriggerRaw>
  )
}

export interface PopoverTriggerRawRenderProps
  extends UseIsHoveringEventListeners {
  /**
   * Don't pass `ref` to `Pressable`, it will make the app crash
   */
  ref: Ref<View>
  isOpen: boolean
  onPress: () => void
  onLayout: (event: LayoutChangeEvent) => void
}

export interface PopoverTriggerRawProps {
  children: (props: PopoverTriggerRawRenderProps) => ReactNode
}

export const PopoverTriggerRaw: FC<PopoverTriggerRawProps> = props => {
  const {
    triggerRef,
    triggerMethod,
    dismissMethod,
    refreshPopoverPosition,
    visible,
    setVisibleFromTrigger,
  } = useContext(PopoverRootContext)

  const hoverEventListeners = useHoveringCallback(hovering => {
    if (hovering) {
      if (triggerMethod === "hover") {
        setVisibleFromTrigger(true)
      }
    } else {
      if (dismissMethod.includes("hover-outside")) {
        setVisibleFromTrigger(false)
      }
    }
  })

  const onPress = usePersistFn((): void => {
    if (!visible) {
      if (triggerMethod === "press") {
        setVisibleFromTrigger(true)
      }
    } else {
      if (dismissMethod.includes("press-trigger")) {
        setVisibleFromTrigger(false)
      }
    }
  })

  const childrenProps = useMemo(
    () => ({
      ref: triggerRef,
      isOpen: visible,
      ...hoverEventListeners,
      onPress,
      onLayout: refreshPopoverPosition,
    }),
    [triggerRef, visible, hoverEventListeners, onPress, refreshPopoverPosition],
  )

  return (
    <Fragment key={"PopoverTriggerRaw"}>
      {props.children(childrenProps)}
    </Fragment>
  )
}
