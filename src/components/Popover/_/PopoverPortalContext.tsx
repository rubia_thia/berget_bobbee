import { createContext, ReactElement } from "react"

export interface PopoverPortalContextValue {
  updatePopoverElements: (
    portalId: string,
    element: null | ReactElement,
  ) => void
}

export const PopoverPortalContext =
  createContext<null | PopoverPortalContextValue>(null)
