import { flip, offset, shift, useFloating } from "@floating-ui/react-native"
import {
  MutableRefObject,
  RefObject,
  SetStateAction,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react"
import { Platform, View } from "react-native"
import { noop } from "../../../utils/fnHelpers"
import { useCombinedRef } from "../../../utils/reactHelpers/refHelpers"
import { FCC } from "../../../utils/reactHelpers/types"
import { usePersistFn } from "../../../utils/reactHelpers/usePersistFn"
import { isTouchDevice } from "../../../utils/runtimeEnvHelpers/isTouchDevice"
import { checkNever } from "../../../utils/typeHelpers"
import {
  PopoverRootContext,
  PopoverRootContextValue,
} from "./PopoverRootContext"
import { PopoverScrollViewPropsContext } from "./PopoverScrollViewPropsContext"
import { ContentPlacement, DismissMethod, TriggerMethod } from "./types"
import {
  useAutoUpdateWhenWindowScrollOnWebPlatform,
  useIsTriggerOutOfWebBrowserViewport,
} from "./webPlatformAffectedLogic"

export interface PopoverRootProps {
  /**
   * if be used in touch device, the value will forcibly be `click`,
   * otherwise the default value is `hover`
   */
  triggerMethod?: TriggerMethod
  /**
   * if `triggerMethod` is `hover`, the default value is `hover-outside`
   * if `triggerMethod` is `click`, the default value is `click-outside`
   */
  dismissMethod?: DismissMethod | DismissMethod[]

  /**
   * Has two functionally:
   *  * delay the popover content's disappearance
   *  * make user can move mouse from trigger to content without content disappearing
   *
   * @default 100
   */
  closeDelayMs?: number

  contentOffset?: number
  contentPlacement?: ContentPlacement

  visible?: boolean
  onVisibleChange?: (visible: boolean) => void
}

export const PopoverRoot: FCC<PopoverRootProps> = props => {
  const triggerMethod = getTriggerMethod(props.triggerMethod)
  const dismissMethod = getDismissMethod(props.dismissMethod, triggerMethod)

  const {
    x: contentX,
    y: contentY,
    reference: floatingTriggerRef,
    floating: contentRef,
    update: refreshPopoverPosition,
    scrollProps,
  } = useFloating({
    sameScrollView: false,
    placement: props.contentPlacement ?? "bottom",
    middleware: [
      // `offset` should be keep at the beginning
      offset(props.contentOffset ?? 0),
      shift(),
      flip(),
    ],
  })

  useAutoUpdateWhenWindowScrollOnWebPlatform(refreshPopoverPosition)

  const { addScrollViewProps } = useContext(PopoverScrollViewPropsContext) ?? {}
  useEffect(
    () => addScrollViewProps?.(scrollProps),
    [scrollProps, addScrollViewProps],
  )

  const checkViewportTriggerRef = useRef<View>(null)
  const outOfWebBrowserViewport = useIsTriggerOutOfWebBrowserViewport(
    checkViewportTriggerRef,
  )
  const outOfTheScrollViewport = useIsTriggerOutOfScrollViewport(
    checkViewportTriggerRef,
  )

  const [visibleFromTrigger, _setVisibleFromTrigger] = useState(false)
  const [visibleFromContent, _setVisibleFromContent] = useState(false)

  const visible =
    (props.visible != null
      ? props.visible
      : visibleFromTrigger || visibleFromContent) &&
    !outOfTheScrollViewport &&
    !outOfWebBrowserViewport

  const onVisibleChange = usePersistFn(
    (
      visible: SetStateAction<boolean>,
      timerRef: MutableRefObject<any>,
      updateInternalVisibleState: (visible: SetStateAction<boolean>) => void,
    ) => {
      clearTimeout(timerRef.current)

      if (props.visible == null) {
        updateInternalVisibleState(prev => {
          const finalVisible =
            typeof visible === "function" ? visible(prev) : visible

          if (finalVisible) return finalVisible

          timerRef.current = setTimeout(() => {
            updateInternalVisibleState(finalVisible)
          }, props.closeDelayMs ?? 100)

          return prev
        })
        return
      }

      if (typeof visible === "function") {
        props.onVisibleChange?.(visible(props.visible ?? false))
      } else {
        props.onVisibleChange?.(visible)
      }
    },
  )
  const setVisibleFromTriggerTimerRef = useRef<any>(null)
  const setVisibleFromTrigger = usePersistFn(
    (visible: SetStateAction<boolean>): void => {
      onVisibleChange(
        visible,
        setVisibleFromTriggerTimerRef,
        _setVisibleFromTrigger,
      )
    },
  )
  const setVisibleFromContentTimerRef = useRef<any>(null)
  const setVisibleFromContent = usePersistFn(
    (visible: SetStateAction<boolean>): void => {
      onVisibleChange(
        visible,
        setVisibleFromContentTimerRef,
        _setVisibleFromContent,
      )
    },
  )

  const triggerRef = useCombinedRef(floatingTriggerRef, checkViewportTriggerRef)
  const value: PopoverRootContextValue = useMemo(
    () => ({
      triggerRef,
      contentRef,

      contentX: contentX == null ? null : contentX,
      contentY: contentY == null ? null : contentY,
      refreshPopoverPosition,

      triggerMethod,
      dismissMethod,

      visibleFromTrigger,
      setVisibleFromTrigger,

      visibleFromContent,
      setVisibleFromContent,

      visible,
    }),
    [
      triggerRef,
      contentRef,
      contentX,
      contentY,
      refreshPopoverPosition,
      triggerMethod,
      dismissMethod,
      visibleFromTrigger,
      setVisibleFromTrigger,
      visibleFromContent,
      setVisibleFromContent,
      visible,
    ],
  )

  return (
    <PopoverRootContext.Provider value={value}>
      {props.children}
    </PopoverRootContext.Provider>
  )
}

function useIsTriggerOutOfScrollViewport(
  checkViewportTriggerRef: RefObject<View>,
): boolean {
  const { addScrollViewProps, scrollViewRef } =
    useContext(PopoverScrollViewPropsContext) ?? {}

  const [outOfTheScrollViewport, setOutOfTheScrollViewport] = useState(false)

  useEffect(
    () =>
      addScrollViewProps?.({
        onScroll(e) {
          if (scrollViewRef?.current == null) return
          const contentOffset = { ...e.nativeEvent.contentOffset }
          const viewportSize = { ...e.nativeEvent.layoutMeasurement }
          checkViewportTriggerRef.current?.measureLayout(
            Platform.OS === "web"
              ? scrollViewRef.current.getInnerViewNode()
              : (scrollViewRef.current as any),
            (x, y, width, height) => {
              // prettier-ignore
              setOutOfTheScrollViewport(
                (
                  // not reached
                  viewportSize.height + contentOffset.y < y + height ||
                  // scrolled over
                  contentOffset.y >= y
                ) || (
                  // not reached
                  viewportSize.width + contentOffset.x < x + width ||
                  // scrolled over
                  contentOffset.x > x
                )
              )
            },
            noop,
          )
        },
      }),
    [addScrollViewProps, checkViewportTriggerRef, scrollViewRef],
  )

  return outOfTheScrollViewport
}

const getTriggerMethod = (
  definedTriggerMethod?: TriggerMethod,
): TriggerMethod => {
  if (isTouchDevice()) {
    return "press"
  } else {
    return definedTriggerMethod ?? "hover"
  }
}

const getDismissMethod = (
  definedDismissMethod: undefined | DismissMethod | DismissMethod[],
  triggerMethod: TriggerMethod,
): DismissMethod[] => {
  if (definedDismissMethod != null) {
    return ensureArray(definedDismissMethod)
  }

  if (triggerMethod === "hover") {
    return ["hover-outside"]
  } else if (triggerMethod === "press") {
    return ["press-outside"]
  } else {
    checkNever(triggerMethod)
    return []
  }

  function ensureArray<T>(a: T | T[]): T[] {
    return Array.isArray(a) ? a : [a]
  }
}
