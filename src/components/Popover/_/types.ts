export type ContentPlacement =
  | "top"
  | "right"
  | "bottom"
  | "left"
  | "top-start"
  | "top-end"
  | "right-start"
  | "right-end"
  | "bottom-start"
  | "bottom-end"
  | "left-start"
  | "left-end"

export type TriggerMethod = "hover" | "press"

export type DismissMethod = "hover-outside" | "press-outside" | "press-trigger"

export interface TriggerMeasurements {
  viewportX: number
  viewportY: number
  width: number
  height: number
}
