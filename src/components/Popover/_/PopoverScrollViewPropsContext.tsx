import { useFloating } from "@floating-ui/react-native"
import { createContext } from "react"
import {
  NativeScrollEvent,
  NativeSyntheticEvent,
  ScrollView,
} from "react-native"
import { ImmutableRefObject } from "../../../utils/reactHelpers/refHelpers"

type UseFloatingReturn = ReturnType<typeof useFloating>

export type PopoverScrollViewProps = Partial<
  Omit<UseFloatingReturn["scrollProps"], "onScroll">
> & {
  onScroll?: (event: NativeSyntheticEvent<NativeScrollEvent>) => void
}

export interface PopoverScrollViewPropsContextValue {
  scrollViewRef: ImmutableRefObject<null | ScrollView>
  addScrollViewProps(props: PopoverScrollViewProps): () => void
}

export const PopoverScrollViewPropsContext =
  createContext<null | PopoverScrollViewPropsContextValue>(null)
