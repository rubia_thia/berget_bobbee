import { FC, useMemo, useRef, useState } from "react"
import { ScrollView, ScrollViewProps } from "react-native"
import {
  PopoverScrollViewProps,
  PopoverScrollViewPropsContext,
  PopoverScrollViewPropsContextValue,
} from "./PopoverScrollViewPropsContext"

export interface PopoverScrollViewPropsProviderProps {
  children: (scrollViewProps: ScrollViewProps) => JSX.Element
}

export const PopoverScrollViewPropsProvider: FC<
  PopoverScrollViewPropsProviderProps
> = props => {
  const scrollViewRef = useRef<ScrollView>(null)

  const [scrollViewProps, updateScrollViewProps] = useState<
    PopoverScrollViewProps[]
  >([])

  const value: PopoverScrollViewPropsContextValue = useMemo(
    () => ({
      scrollViewRef,
      addScrollViewProps(props) {
        updateScrollViewProps(prev => [...prev, props])
        return () => {
          updateScrollViewProps(prev => prev.filter(p => p !== props))
        }
      },
    }),
    [],
  )

  const finalScrollViewProps = useMemo(() => {
    const combined = scrollViewProps.reduce((acc, props) => {
      const newAcc = { ...acc, ...props }
      Object.keys(props).forEach(key => {
        // @ts-ignore
        if (typeof props[key] === "function") {
          // @ts-ignore
          newAcc[key] = (...args: any[]) => {
            // @ts-ignore
            acc[key]?.(...args)
            // @ts-ignore
            props[key](...args)
          }
        }
      })
      return newAcc
    }, {} as ScrollViewProps)

    return {
      ...combined,
      ref: scrollViewRef,
    }
  }, [scrollViewProps])

  return (
    <PopoverScrollViewPropsContext.Provider value={value}>
      {props.children(finalScrollViewProps)}
    </PopoverScrollViewPropsContext.Provider>
  )
}
