import { toPairs } from "ramda"
import { Fragment, ReactElement, useMemo, useState } from "react"
import { FCC } from "../../../utils/reactHelpers/types"
import { usePersistFn } from "../../../utils/reactHelpers/usePersistFn"
import {
  PopoverPortalContext,
  PopoverPortalContextValue,
} from "./PopoverPortalContext"

export interface PopoverPortalProviderProps {}

export const PopoverPortalProvider: FCC<PopoverPortalProviderProps> = props => {
  const [showingPopoverElements, setShowingPopoverElements] = useState<
    Record<string, ReactElement>
  >({})

  const updatePopoverElements: PopoverPortalContextValue["updatePopoverElements"] =
    usePersistFn((portalId, element) => {
      if (element == null) {
        setShowingPopoverElements(prev => {
          const { [portalId]: _, ...rest } = prev
          return rest
        })
      } else {
        setShowingPopoverElements(prev => ({
          ...prev,
          [portalId]: element,
        }))
      }
    })

  const value: PopoverPortalContextValue = useMemo(
    () => ({
      updatePopoverElements,
    }),
    [updatePopoverElements],
  )

  return (
    <PopoverPortalContext.Provider value={value}>
      <>
        {props.children}

        {toPairs(showingPopoverElements).map(([portalId, element]) => (
          <Fragment key={portalId}>{element}</Fragment>
        ))}
      </>
    </PopoverPortalContext.Provider>
  )
}
