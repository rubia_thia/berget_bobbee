import { createContext, Ref, SetStateAction } from "react"
import { View } from "react-native"
import { noop } from "../../../utils/fnHelpers"
import { DismissMethod, TriggerMethod } from "./types"

export interface PopoverRootContextValue {
  triggerRef: Ref<View>
  contentRef: Ref<View>

  contentX: null | number
  contentY: null | number
  refreshPopoverPosition: () => void

  triggerMethod: TriggerMethod
  dismissMethod: DismissMethod[]

  visibleFromTrigger: boolean
  setVisibleFromTrigger: (visible: SetStateAction<boolean>) => void

  visibleFromContent: boolean
  setVisibleFromContent: (visible: SetStateAction<boolean>) => void

  visible: boolean
}

export const PopoverRootContext = createContext<PopoverRootContextValue>({
  triggerRef: noop,
  contentRef: noop,
  contentX: 0,
  contentY: 0,
  refreshPopoverPosition: noop,
  triggerMethod: "hover",
  dismissMethod: ["hover-outside"],
  visibleFromTrigger: false,
  setVisibleFromTrigger: noop,
  visibleFromContent: false,
  setVisibleFromContent: noop,
  visible: false,
})
