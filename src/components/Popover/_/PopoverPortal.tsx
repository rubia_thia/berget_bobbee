import { useContext, useEffect, useId } from "react"
import { FCC } from "../../../utils/reactHelpers/types"
import { PopoverPortalContext } from "./PopoverPortalContext"
import { PopoverRootContext } from "./PopoverRootContext"

export interface PopoverPortalProps {}

export const PopoverPortal: FCC<PopoverPortalProps> = props => {
  const ctxValue = useContext(PopoverPortalContext)

  if (ctxValue == null) {
    throw new Error(
      "[PopoverPortal] must be used in PopoverPortalProvider subtree",
    )
  }

  const { updatePopoverElements } = ctxValue
  const rootCtxValue = useContext(PopoverRootContext)

  const portalId = useId()

  useEffect(() => {
    updatePopoverElements(
      portalId,
      !rootCtxValue.visible ? null : (
        <PopoverRootContext.Provider value={rootCtxValue}>
          <>{props.children}</>
        </PopoverRootContext.Provider>
      ),
    )
  }, [portalId, props.children, rootCtxValue, updatePopoverElements])

  useEffect(() => {
    return () => {
      updatePopoverElements(portalId, null)
    }
  }, [portalId, updatePopoverElements])

  return null
}
