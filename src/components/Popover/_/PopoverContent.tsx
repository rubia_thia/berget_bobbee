import { FC, ReactNode, useContext, useEffect, useMemo, useState } from "react"
import {
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  ViewStyle,
} from "react-native"
import { useHoveringCallback } from "../../../utils/reactHelpers/useIsHovering"
import { FullWindowOverlay } from "../../FullWindowOverlay"
import { Pressable } from "../../Pressable"
import { PopoverPortal } from "./PopoverPortal"
import { PopoverRootContext } from "./PopoverRootContext"
import { fullWindowContainerStyle } from "./webPlatformAffectedLogic"

export interface PopoverContentChildrenRenderProps {
  close: () => void
}

export type PopoverContentChildrenRenderer = (
  renderProps: PopoverContentChildrenRenderProps,
) => ReactNode

export interface PopoverContentProps {
  children: ReactNode | PopoverContentChildrenRenderer
}

export const PopoverContent: FC<PopoverContentProps> = props => {
  const {
    contentX,
    contentY,
    refreshPopoverPosition,
    contentRef,
    triggerMethod,
    dismissMethod,
    visible,
    setVisibleFromContent,
    setVisibleFromTrigger,
  } = useContext(PopoverRootContext)

  const hoverEventListeners = useHoveringCallback(hovering => {
    if (hovering) {
      if (triggerMethod === "hover") {
        setVisibleFromContent(true)
      }
    } else {
      if (dismissMethod.includes("hover-outside")) {
        setVisibleFromContent(false)
      }
    }
  })

  const childrenRenderProps: PopoverContentChildrenRenderProps = useMemo(
    () => ({
      close: () => {
        setVisibleFromContent(false)
        setVisibleFromTrigger(false)
      },
    }),
    [setVisibleFromContent, setVisibleFromTrigger],
  )

  const [contentStyles, setContentStyles] = useState<ViewStyle>({})
  useEffect(() => {
    if (visible && contentX != null && contentY != null) {
      setContentStyles({
        left: contentX,
        top: contentY,
        opacity: 1,
        zIndex: undefined,
      })
    } else {
      setContentStyles(style => ({
        // it's no need to move the content anymore
        ...style,
        opacity: 0,
        zIndex: -1,
      }))
    }
  }, [contentX, contentY, visible])

  const onPressOverlay = (): void => {
    if (dismissMethod.includes("press-outside")) {
      setVisibleFromContent(false)
      setVisibleFromTrigger(false)
    }
  }

  const content = (
    <Pressable
      ref={contentRef}
      style={[
        contentStyles,
        { cursor: "auto" } as any,
        { position: "absolute" },
      ]}
      onLayout={refreshPopoverPosition}
      {...hoverEventListeners}
    >
      {typeof props.children === "function"
        ? props.children(childrenRenderProps)
        : props.children}
    </Pressable>
  )

  if (!dismissMethod.includes("press-outside")) {
    return (
      <PopoverPortal>
        <View style={fullWindowContainerStyle}>{content}</View>
      </PopoverPortal>
    )
  }

  return (
    <PopoverPortal>
      <FullWindowOverlay style={fullWindowContainerStyle}>
        <TouchableWithoutFeedback onPress={onPressOverlay}>
          <View style={StyleSheet.absoluteFill} />
        </TouchableWithoutFeedback>

        {content}
      </FullWindowOverlay>
    </PopoverPortal>
  )
}
