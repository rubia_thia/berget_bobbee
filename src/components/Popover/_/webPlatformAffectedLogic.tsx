import { RefObject, useEffect, useState } from "react"
import { Dimensions, Platform, View, ViewStyle } from "react-native"
import { throttle } from "../../../utils/fnHelpers"

export const fullWindowContainerStyle: ViewStyle = {
  /**
   * On native, the root element is not scrollable,
   */
  position: Platform.OS === "web" ? ("fixed" as any) : "absolute",
  top: 0,
  left: 0,
  right: 0,
}

export const useAutoUpdateWhenWindowScrollOnWebPlatform = (
  refreshPopoverPosition: () => void,
): void => {
  useEffect(() => {
    if (Platform.OS !== "web") return
    const callback = throttle(refreshPopoverPosition, 10)
    window.addEventListener("scroll", callback, { passive: true })
    return () => window.removeEventListener("scroll", callback)
  }, [refreshPopoverPosition])
}

export const useIsTriggerOutOfWebBrowserViewport = (
  triggerRef: RefObject<View>,
): boolean => {
  const [
    isTriggerOutOfWebBrowserViewport,
    setIsTriggerOutOfWebBrowserViewport,
  ] = useState(false)

  useEffect(() => {
    if (Platform.OS !== "web") return
    const triggerElement = triggerRef.current
    const callback = throttle(doCheck, 10)
    window.addEventListener("scroll", callback, { passive: true })
    return () => window.removeEventListener("scroll", callback)

    function doCheck(): void {
      if (triggerElement == null) return
      triggerElement.measureInWindow((x, y, width, height) => {
        const dim = Dimensions.get("window")
        setIsTriggerOutOfWebBrowserViewport(
          !isOverlapped(
            { x, y, width, height },
            { x: 0, y: 0, width: dim.width, height: dim.height },
          ),
        )
      })
    }
  }, [triggerRef])

  return isTriggerOutOfWebBrowserViewport
}

interface Area {
  x: number
  y: number
  width: number
  height: number
}
function isOverlapped(a1: Area, a2: Area): boolean {
  const isVerticalDownsideOverlapped = (a: Area, b: Area): boolean =>
    a.y < b.y + b.height && a.y + a.height > b.y

  const isVerticalUpsideOverlapped = (a: Area, b: Area): boolean =>
    isVerticalDownsideOverlapped(b, a)

  const isHorizontalLeftsideOverlapped = (a: Area, b: Area): boolean =>
    a.x + a.width > b.x && a.x < b.x + b.width

  const isHorizontalRightsideOverlapped = (a: Area, b: Area): boolean =>
    isHorizontalLeftsideOverlapped(b, a)

  return (
    (isVerticalDownsideOverlapped(a1, a2) ||
      isVerticalUpsideOverlapped(a1, a2)) &&
    (isHorizontalLeftsideOverlapped(a1, a2) ||
      isHorizontalRightsideOverlapped(a1, a2))
  )
}
