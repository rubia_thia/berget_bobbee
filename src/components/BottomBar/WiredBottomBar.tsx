import { action } from "mobx"
import { WiredDisclaimerModal } from "../../screens/TradeScreen/wiredComponents/WiredDisclaimerModal"
import { useAppEnvStore } from "../../stores/AppEnvStore/useAppEnvStore"
import { FCS } from "../../utils/reactHelpers/types"
import { BottomBar } from "./BottomBar"

export const WiredBottomBar: FCS = ({ style }) => {
  const app = useAppEnvStore()
  return (
    <>
      <BottomBar
        onPressDisclaimer={action(() => (app.acceptedDisclaimers = false))}
        style={style}
      />
      <WiredDisclaimerModal />
    </>
  )
}
