import { defineMessage, useIntl } from "react-intl"
import { Text, View } from "react-native"
import { FCS } from "../../utils/reactHelpers/types"
import { HrefLink } from "../../utils/reactNavigationHelpers/HrefLink"
import { MediaLinks } from "../MediaLinks/MediaLinks"
import { useColors } from "../Themed/color"
import { useSpacing } from "../Themed/spacing"
import Logo from "./assets/logo.svg"

const LinkText: FCS<{ children: string }> = ({ children, style }) => (
  <Text className="text-sm font-medium text-gray-500 px-2 py-2" style={style}>
    {children}
  </Text>
)

export const BottomBar: FCS<{ onPressDisclaimer: () => void }> = props => {
  const spacing = useSpacing()
  const colors = useColors()
  const { $t } = useIntl()

  return (
    <View
      className="py-2 md:py-4 flex-col md:flex-row-reverse bg-white"
      style={props.style}
    >
      <View className="flex-row justify-center space-x-8 px-4 md:px-8">
        <MediaLinks
          itemPadding={spacing(2)}
          gap={spacing(8)}
          size={18}
          tint={colors("gray-500")}
          links={{
            twitter: "https://twitter.com/UniwhaleEx",
            discord: "https://discord.com/invite/Uniwhale",
            medium: "https://medium.com/uniwhale",
            github: "https://github.com/uniwhale-io",
          }}
        />
      </View>
      <View className="w-[1px] h-8 self-center bg-gray-300 hidden md:flex" />
      <View className="flex-row items-center px-4 md:px-8 md:flex-1">
        <Logo style={{ marginRight: "auto" }} />
        <HrefLink href="https://uniwhale.co/">
          <LinkText>
            {$t(
              defineMessage({
                defaultMessage: "About",
                description: "bottom bar/about link",
              }),
            )}
          </LinkText>
        </HrefLink>
        <HrefLink onPress={props.onPressDisclaimer}>
          <LinkText>
            {$t(
              defineMessage({
                defaultMessage: "Disclaimer",
                description: "bottom bar/disclaimer",
              }),
            )}
          </LinkText>
        </HrefLink>
      </View>
    </View>
  )
}
