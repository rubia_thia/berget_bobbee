import { ComponentMeta } from "@storybook/react"
import { BackgroundColor, CardContainer } from "../../../.storybook/decorators"
import { withTemplate } from "../../../.storybook/utils"
import { noop } from "../../utils/fnHelpers"
import { BottomBar } from "./BottomBar"

export default {
  title: "UI/BottomBar",
  component: BottomBar,
  decorators: [CardContainer({ margin: 10, padding: 10 }), BackgroundColor()],
} as ComponentMeta<typeof BottomBar>

const template = withTemplate(BottomBar, {
  onPressDisclaimer: noop,
})

export const Normal = template()
