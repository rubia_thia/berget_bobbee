import { StyleProp, TextStyle, ViewStyle } from "react-native"
import { OneOrMore } from "../utils/types"
import { LinearGradientColor } from "./LinearGradient"

export interface GradientTextProps {
  style?: StyleProp<ViewStyle>

  textStyle?: StyleProp<TextStyle>

  gradient: {
    /**
     * Currently only supports linear gradient
     */
    type?: "linear"

    /**
     * @default "horizontal"
     */
    direction?: "horizontal" | "vertical" | number

    colors: OneOrMore<LinearGradientColor>
  }

  /**
   * Currently we only support 1 line text, it will get a different render result
   * between web and native when rendering multi-line text.
   */
  numberOfLines?: 1
}
