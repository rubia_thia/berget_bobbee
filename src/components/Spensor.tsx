import { observer } from "mobx-react-lite"
import { mapObjIndexed } from "ramda"
import { FC, ReactNode, Suspense, useMemo } from "react"
import {
  readResource,
  SuspenseResource,
  UnboxSuspenseResourceCollection,
} from "../utils/SuspenseResource"

export const RenderChildrenFn: FC<{
  suspenseTag?: string
  children: () => ReactNode
}> = props => {
  const WrappedComp = useMemo(() => {
    const Component = (p: { children: () => any }): any => <>{p.children()}</>
    Component.displayName = `RenderChildrenFnInside<${props.suspenseTag}>`
    return observer(Component)
  }, [props.suspenseTag])

  if (props.suspenseTag != null) {
    return <WrappedComp>{props.children}</WrappedComp>
  } else {
    return <>{props.children()}</>
  }
}

export const Spensor: FC<{
  spensorTag?: string
  children: () => ReactNode
  fallback?: ReactNode
}> = props => {
  return (
    <Suspense fallback={props.fallback ?? null}>
      <RenderChildrenFn suspenseTag={props.spensorTag}>
        {props.children}
      </RenderChildrenFn>
    </Suspense>
  )
}

export function SpensorR<
  T extends Record<string, SuspenseResource<any>>,
>(props: {
  spensorTag?: string
  read?: T
  children: (values: UnboxSuspenseResourceCollection<T>) => ReactNode
  fallback?: ReactNode
}): JSX.Element {
  return (
    <Suspense fallback={props.fallback ?? null}>
      <RenderChildrenFn suspenseTag={props.spensorTag}>
        {() => {
          const results: UnboxSuspenseResourceCollection<T> = mapObjIndexed(
            readResource,
            props.read ?? {},
          ) as any
          return props.children(results)
        }}
      </RenderChildrenFn>
    </Suspense>
  )
}
