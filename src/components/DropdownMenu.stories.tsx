import { Meta, Story } from "@storybook/react"
import { ActivityIndicator, Switch } from "react-native"
import { BackgroundColor, CardContainer } from "../../.storybook/decorators"
import {
  DropdownMenu,
  DropdownMenuDivider,
  DropdownMenuItem,
  DropdownMenuItemRichLayout,
  DropdownMenuTitle,
} from "./DropdownMenu"

export default {
  title: "UI/DropdownMenu",
  decorators: [CardContainer(), BackgroundColor()],
} as Meta

export const SimpleMenu: Story = () => {
  return (
    <DropdownMenu>
      <DropdownMenuTitle titleText={"Group 1"} />
      <DropdownMenuItem disabled={true}>Disabled</DropdownMenuItem>
      <DropdownMenuItem>Item 2</DropdownMenuItem>
      <DropdownMenuDivider />
      <DropdownMenuTitle titleText={"Group 2"} />
      <DropdownMenuItem>Item 3</DropdownMenuItem>
      <DropdownMenuItem>Item 4</DropdownMenuItem>
      <DropdownMenuItem>
        <DropdownMenuItemRichLayout
          icon={<ActivityIndicator size={18} />}
          accessory={<Switch value={false} />}
        >
          Item 5
        </DropdownMenuItemRichLayout>
      </DropdownMenuItem>
      <DropdownMenuItem>
        <DropdownMenuItemRichLayout>Item 5</DropdownMenuItemRichLayout>
      </DropdownMenuItem>
    </DropdownMenu>
  )
}
