import { Text } from "react-native"
import { FCC } from "../utils/reactHelpers/types"
import { GradientTextProps } from "./GradientText.types"
import { linearGradientColorToCss } from "./LinearGradient"

export const GradientText: FCC<GradientTextProps> = props => {
  const direction = props.gradient.direction ?? "horizontal"
  const cssDirection =
    typeof direction === "number"
      ? `${direction}deg`
      : direction === "horizontal"
      ? "to right"
      : "to bottom"

  const backgroundImage = `linear-gradient(${cssDirection}, ${props.gradient.colors
    .map(linearGradientColorToCss)
    .join(", ")})`

  return (
    <Text
      style={[props.textStyle, props.style]}
      numberOfLines={props.numberOfLines}
    >
      <span
        style={{
          ...extraStyle,
          color: "transparent",
          backgroundColor: "transparent",
          backgroundImage,
          ...{
            backgroundClip: "text",
            WebkitBackgroundClip: "text",
            MozBackgroundClip: "text",
          },
        }}
      >
        {props.children}
      </span>
    </Text>
  )
}

// from https://developer.mozilla.org/en-US/docs/Web/HTTP/Browser_detection_using_the_user_agent#browser_name
const isSafari =
  navigator.userAgent.includes("Safari/") &&
  !navigator.userAgent.includes("Chrome/")
const extraStyle = !isSafari
  ? {}
  : {
      // https://stackoverflow.com/a/65330979/1226532
      "-webkit-box-decoration-break": "clone",
    }
