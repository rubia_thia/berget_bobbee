import { StyleProp, View, ViewStyle } from "react-native"
import { FCC } from "../../utils/reactHelpers/types"
import {
  PaddingStyle,
  useNormalizePaddingStyle,
} from "../../utils/styleHelpers/PaddingStyle"
import { useColors } from "../Themed/color"

export const CardInset: FCC<{
  style?: StyleProp<ViewStyle>
  padding?: number | PaddingStyle
  backgroundColor?: string
}> = props => {
  const colors = useColors()

  const paddingStyle = useNormalizePaddingStyle(props.padding)
  const defaultPaddingStyle = useNormalizePaddingStyle({
    paddingVertical: 10,
    paddingHorizontal: 12,
  })

  return (
    <View
      style={[
        props.style,
        defaultPaddingStyle,
        paddingStyle,
        {
          borderRadius: 4,
          backgroundColor: props.backgroundColor ?? colors("gray-100"),
        },
      ]}
    >
      {props.children}
    </View>
  )
}
