import { FC, ForwardedRef, forwardRef } from "react"
import { StyleProp, StyleSheet, View, ViewProps, ViewStyle } from "react-native"
import { PropsWithChildren } from "../../utils/reactHelpers/types"
import {
  PaddingStyle,
  useNormalizePaddingStyle,
} from "../../utils/styleHelpers/PaddingStyle"
import { checkObjEmpty } from "../../utils/typeHelpers"
import { ShadowBox, ShadowPropValue } from "../ShadowBox"

const cardShadow = [0, 1, 1, "rgba(0, 0, 0, 0.05)"] as const

export interface CardBoxProps {
  shadow?: ShadowPropValue
  children: (props: { style: ViewStyle }) => JSX.Element
}

export const CardBox: FC<CardBoxProps> = props => {
  return (
    <ShadowBox shadow={props.shadow ?? cardShadow}>
      {({ style: shadowStyle, ...rest }) => {
        checkObjEmpty(rest)

        return (
          <>
            {props.children({
              style: StyleSheet.flatten([
                shadowStyle,
                {
                  borderRadius: 8,
                  backgroundColor: "white",
                },
              ]),
            })}
          </>
        )
      }}
    </ShadowBox>
  )
}

export interface CardBoxViewProps {
  style?: StyleProp<ViewStyle>
  padding?: number | PaddingStyle
  backgroundColor?: string
  onLayout?: ViewProps["onLayout"]
}
export const CardBoxView = forwardRef(
  (props: PropsWithChildren<CardBoxViewProps>, ref: ForwardedRef<View>) => {
    const paddingStyle = useNormalizePaddingStyle(props.padding)

    return (
      <CardBox shadow={[0, 1, 4, "rgba(0, 0, 0, 0.1)"]}>
        {({ style, ...rest }) => {
          checkObjEmpty(rest)

          return (
            <View
              ref={ref}
              style={[
                style,
                props.style,
                paddingStyle,
                { backgroundColor: props.backgroundColor ?? "white" },
              ]}
              collapsable={false}
              onLayout={props.onLayout}
            >
              {props.children}
            </View>
          )
        }}
      </CardBox>
    )
  },
)
