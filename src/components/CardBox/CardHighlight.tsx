import { FC, ReactNode } from "react"
import { StyleProp, ViewStyle } from "react-native"
import { PaddingStyle } from "../../utils/styleHelpers/PaddingStyle"
import { useColors } from "../Themed/color"
import { CardInset } from "./CardInset"

export const CardHighlight: FC<{
  style?: StyleProp<ViewStyle>
  padding?: number | PaddingStyle
  backgroundColor?: string
  children: ReactNode
}> = props => {
  const colors = useColors()

  return (
    <CardInset
      style={[props.style, { borderRadius: 4 }]}
      padding={props.padding}
      backgroundColor={props.backgroundColor ?? colors("blue-50")}
    >
      {props.children}
    </CardInset>
  )
}
