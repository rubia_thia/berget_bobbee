import { ComponentMeta } from "@storybook/react"
import { Text, View } from "react-native"
import { withTemplate } from "../../.storybook/utils"
import { Slider, SliderProps } from "./Slider"

export default {
  title: "UI/Slider",
  component: Slider,
} as ComponentMeta<typeof Slider>

const template = withTemplate(
  (props: SliderProps) => (
    <View className={"w-[400px]"}>
      <Slider {...props} />
    </View>
  ),
  {
    value: 10,
    min: 0,
    max: 100,
  },
)

export const Normal = template()

export const OnlyOne = template(props => {
  props.min = 1
  props.max = 1
  props.value = 1
})

export const Disabled = template(props => {
  props.disabled = true
})

export const WithTicks = template(p => {
  p.segments = [
    { endingPosition: 0.3, endingValue: 5, labelText: "5" },
    { endingPosition: 0.6, endingValue: 40, labelText: "40" },
    {},
  ]
})

export const WithCustomTooltip = template(p => {
  p.TooltipContent = p => <Text>{p.value}%</Text>
})
