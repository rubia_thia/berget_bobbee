import { ComponentMeta } from "@storybook/react"
import { FC } from "react"
import { TextTokenCount } from "./TextTokenCount"
import {
  Banner as _Banner,
  TopNavBarButton as _TopNavBarButton,
} from "./tradeCompetitionComponents"

export default {
  title: "UI/Trade Competition Components",
  component: TextTokenCount,
} as ComponentMeta<typeof TextTokenCount>

export const TopNavBarButton: FC = () => <_TopNavBarButton />

export const Banner: FC = () => <_Banner />
