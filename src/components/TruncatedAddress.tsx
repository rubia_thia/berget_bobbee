import { FC } from "react"
import { StyleProp, Text, TextStyle } from "react-native"

export interface TruncatedAddressTextProps {
  style?: StyleProp<TextStyle>

  address: string

  /**
   * @default 2
   */
  head?: number

  /**
   * @default 3
   */
  tail?: number

  onlyIfLongerThan?: number
}

export const TruncatedAddressText: FC<TruncatedAddressTextProps> = props => {
  const { head = 2, tail = 3 } = props

  let result: string
  if (
    props.onlyIfLongerThan != null &&
    props.address.length <= props.onlyIfLongerThan
  ) {
    result = props.address
  } else {
    const rest = "..." + props.address.slice(-tail)
    if (props.address.startsWith("0x")) {
      result = props.address.slice(0, head + 2) + rest
    } else {
      result = "0x" + props.address.slice(0, head) + rest
    }
  }

  return <Text>{result}</Text>
}
