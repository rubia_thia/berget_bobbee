import * as S from "@mobily/stacks"
import { omit, pick } from "ramda"
import { ComponentProps, ReactElement } from "react"
import { FCC } from "../utils/reactHelpers/types"
import { ResponsiveValue, useResponsiveValues } from "./Themed/breakpoints"

export const markAsColumn = S.markAsColumn
export const isColumnComponent = (node: ReactElement): boolean => {
  /**
   * from https://github.com/mobily/stacks/blob/81064af3899d13375aecffc32b1deacd370ff499/src/Stacks_utils.js#L46-L48
   */

  return (
    typeof node === "object" &&
    "type" in node &&
    (node.type as any).__isColumn__
  )
}

export const markAsRow = S.markAsRow
export const isRowComponent = (node: ReactElement): boolean => {
  /**
   * from https://github.com/mobily/stacks/blob/81064af3899d13375aecffc32b1deacd370ff499/src/Stacks_utils.js#L50-L52
   */

  return (
    typeof node === "object" && "type" in node && (node.type as any).__isRow__
  )
}

export { Hidden, HiddenProps } from "@mobily/stacks"

export type TransformResponsiveProp<T, PropKey> = {
  [K in keyof T]: K extends PropKey
    ? T[K] extends infer R | S.ResponsiveProp<infer J>
      ? R | ResponsiveValue<J>
      : never
    : T[K]
}

const marginResponsivePropNames = [
  "margin",
  "marginX",
  "marginY",
  "marginTop",
  "marginRight",
  "marginBottom",
  "marginLeft",
  "marginStart",
  "marginEnd",
] as const
const paddingResponsivePropNames = [
  "padding",
  "paddingX",
  "paddingY",
  "paddingTop",
  "paddingRight",
  "paddingBottom",
  "paddingLeft",
  "paddingStart",
  "paddingEnd",
] as const
const fourDirectionResponsivePropNames = [
  "top",
  "right",
  "bottom",
  "left",
] as const

const stackResponsivePropNames = [
  "space",
  "align",
  "horizontal",
  ...paddingResponsivePropNames,
  ...marginResponsivePropNames,
] as const
export type StackProps = TransformResponsiveProp<
  S.StackProps,
  typeof stackResponsivePropNames[number]
>
export const Stack: FCC<StackProps> = props => {
  const restProps = omit(stackResponsivePropNames, props)
  const transformingProps = pick(stackResponsivePropNames, props)
  const transformedProps = useResponsiveValues(transformingProps)

  return <S.Stack {...restProps} {...transformedProps} />
}

const boxResponsivePropNames = [
  "alignX",
  "alignY",
  "alignSelf",
  "direction",
  "wrap",
  "flex",
  "space",
  "align",
  "horizontal",
  ...paddingResponsivePropNames,
  ...marginResponsivePropNames,
] as const
export type BoxProps = TransformResponsiveProp<
  S.BoxProps,
  typeof boxResponsivePropNames[number]
>
export const Box: FCC<BoxProps> = props => {
  const restProps = omit(boxResponsivePropNames, props)
  const transformingProps = pick(boxResponsivePropNames, props)
  const transformedProps = useResponsiveValues(transformingProps)

  return <S.Box {...restProps} {...transformedProps} />
}

const fillViewResponsivePropNames = [
  "alignX",
  "alignY",
  "alignSelf",
  "direction",
  "wrap",
  ...paddingResponsivePropNames,
  ...marginResponsivePropNames,
  ...fourDirectionResponsivePropNames,
] as const
export type FillViewProps = TransformResponsiveProp<
  ComponentProps<typeof S.FillView>,
  typeof fillViewResponsivePropNames[number]
>
export const FillView: FCC<FillViewProps> = props => {
  const restProps = omit(fillViewResponsivePropNames, props)
  const transformingProps = pick(fillViewResponsivePropNames, props)
  const transformedProps = useResponsiveValues(transformingProps)

  return <S.FillView {...restProps} {...transformedProps} />
}

const bleedResponsivePropNames = [
  "space",
  "horizontal",
  "vertical",
  ...fourDirectionResponsivePropNames,
] as const
export type BleedProps = TransformResponsiveProp<
  S.BleedProps,
  typeof bleedResponsivePropNames[number]
>
export const Bleed: FCC<BleedProps> = props => {
  const restProps = omit(bleedResponsivePropNames, props)
  const transformingProps = pick(bleedResponsivePropNames, props)
  const transformedProps = useResponsiveValues(transformingProps)

  return <S.Bleed {...restProps} {...transformedProps} />
}

const inlineResponsivePropNames = [
  "space",
  "alignX",
  "alignY",
  ...marginResponsivePropNames,
  ...paddingResponsivePropNames,
] as const
export type InlineProps = TransformResponsiveProp<
  S.InlineProps,
  typeof inlineResponsivePropNames[number]
>
export const Inline: FCC<InlineProps> = props => {
  const restProps = omit(inlineResponsivePropNames, props)
  const transformingProps = pick(inlineResponsivePropNames, props)
  const transformedProps = useResponsiveValues(transformingProps)

  return <S.Inline {...restProps} {...transformedProps} />
}

const insetResponsivePropNames = [
  "space",
  "horizontal",
  "vertical",
  ...fourDirectionResponsivePropNames,
] as const
export type InsetProps = TransformResponsiveProp<
  S.InsetProps,
  typeof insetResponsivePropNames[number]
>
export const Inset: FCC<InsetProps> = props => {
  const restProps = omit(insetResponsivePropNames, props)
  const transformingProps = pick(insetResponsivePropNames, props)
  const transformedProps = useResponsiveValues(transformingProps)

  return <S.Inset {...restProps} {...transformedProps} />
}

const tilesResponsivePropNames = [
  "columns",
  "space",
  "empty",
  ...paddingResponsivePropNames,
  ...marginResponsivePropNames,
] as const
export type TilesProps = TransformResponsiveProp<
  S.TilesProps,
  typeof tilesResponsivePropNames[number]
>
export const Tiles: FCC<TilesProps> = props => {
  const restProps = omit(tilesResponsivePropNames, props)
  const transformingProps = pick(tilesResponsivePropNames, props)
  const transformedProps = useResponsiveValues(transformingProps)

  return <S.Tiles {...restProps} {...transformedProps} />
}

const columnsResponsivePropNames = [
  "space",
  "height",
  "alignX",
  "alignY",
  ...marginResponsivePropNames,
  ...paddingResponsivePropNames,
] as const
export type ColumnsProps = TransformResponsiveProp<
  S.ColumnsProps,
  typeof columnsResponsivePropNames[number]
>
export const Columns: FCC<ColumnsProps> = props => {
  const restProps = omit(columnsResponsivePropNames, props)
  const transformingProps = pick(columnsResponsivePropNames, props)
  const transformedProps = useResponsiveValues(transformingProps)

  return <S.Columns {...restProps} {...transformedProps} />
}

const columnResponsivePropNames = [
  "width",
  ...paddingResponsivePropNames,
] as const
export type ColumnProps = TransformResponsiveProp<
  S.ColumnProps,
  typeof columnResponsivePropNames[number]
>
export const Column: FCC<ColumnProps> = props => {
  const restProps = omit(columnResponsivePropNames, props)
  const transformingProps = pick(columnResponsivePropNames, props)
  const transformedProps = useResponsiveValues(transformingProps)

  return <S.Column {...restProps} {...transformedProps} />
}
markAsColumn(Column)

const rowsResponsivePropNames = [
  "space",
  "alignX",
  "alignY",
  ...marginResponsivePropNames,
  ...paddingResponsivePropNames,
] as const
export type RowsProps = TransformResponsiveProp<
  S.RowsProps,
  typeof rowsResponsivePropNames[number]
>
export const Rows: FCC<RowsProps> = props => {
  const restProps = omit(rowsResponsivePropNames, props)
  const transformingProps = pick(rowsResponsivePropNames, props)
  const transformedProps = useResponsiveValues(transformingProps)

  return <S.Rows {...restProps} {...transformedProps} />
}

const rowResponsivePropNames = [
  "width",
  "height",
  ...paddingResponsivePropNames,
] as const
export type RowProps = TransformResponsiveProp<
  S.RowProps,
  typeof rowResponsivePropNames[number]
>
export const Row: FCC<RowProps> = props => {
  const restProps = omit(rowResponsivePropNames, props)
  const transformingProps = pick(rowResponsivePropNames, props)
  const transformedProps = useResponsiveValues(transformingProps)

  return <S.Row {...restProps} {...transformedProps} />
}
markAsRow(Row)
