import { values } from "ramda"
import { FC, ReactNode } from "react"
import { StyleProp, Text, TextStyle } from "react-native"
import { oneOf } from "../utils/arrayHelpers"
import { BigNumber } from "../utils/numberHelpers/BigNumber"
import { readResource, SuspenseResource } from "../utils/SuspenseResource"
import { TokenInfo } from "../utils/TokenInfo"
import { Spensor } from "./Spensor"

export interface TokenCountProps {
  style?: StyleProp<TextStyle>

  color?: string

  token: SuspenseResource<TokenPresets | TokenInfo>

  count: SuspenseResource<number | BigNumber>

  /**
   * @default false
   *
   * make BigNumber.from(0.3) render as 0.30000
   */
  padDecimals?: boolean

  renderText?: RenderTextFn
}

export const TokenCount: FC<TokenCountProps> = props => {
  const { renderText = defaultRenderText } = props

  return (
    <>
      {renderText({
        style: [props.style, { color: props.color }],
        children: (
          <Spensor fallback="-">
            {() =>
              formatTokenCount(
                readResource(props.token),
                readResource(props.count),
                {
                  padDecimals: props.padDecimals,
                },
              )
            }
          </Spensor>
        ),
      })}
    </>
  )
}

export type RenderTextFn = (props: {
  style?: StyleProp<TextStyle>
  children: ReactNode
}) => ReactNode
export const defaultRenderText: RenderTextFn = props => <Text {...props} />

export function formatTokenCount(
  token: TokenPresets | TokenInfo,
  count: number | BigNumber,
  options: {
    padDecimals?: boolean
  } = {},
): string {
  const { padDecimals = false } = options

  const precision = TokenPresetsHelpers.isTokenPresets(token)
    ? TokenPresetsHelpers.getPrecision(token)
    : TokenInfo.getPrecision(token)
  const formatter = new Intl.NumberFormat("en-US", {
    style: "decimal",
    maximumFractionDigits: precision,
    ...(padDecimals ? { minimumFractionDigits: precision } : {}),
  })

  if (BigNumber.getPrecision(count) <= precision) {
    return formatter.format(BigNumber.toNumber(count))
  }

  const roundDownCount = BigNumber.round(
    { precision: precision, roundingMode: BigNumber.roundDown },
    count,
  )
  const formattedIntegerPart = formatter
    .format(BigNumber.toNumber(roundDownCount))
    .split(".")[0]

  const formattedDecimalPart = BigNumber.getDecimalPart({ precision }, count)
  if (formattedDecimalPart == null || BigNumber.isZero(formattedDecimalPart)) {
    if (!padDecimals) {
      return String(formattedIntegerPart)
    } else {
      return `${formattedIntegerPart}.${"0".repeat(precision)}`
    }
  }

  return `${formattedIntegerPart}.${
    padDecimals
      ? formattedDecimalPart.padEnd(precision, "0")
      : formattedDecimalPart
  }`
}

export enum TokenPresets {
  USD = "USD",
}
export namespace TokenPresetsHelpers {
  export function isTokenPresets(value: any): value is TokenPresets {
    return oneOf(...values(TokenPresets))(value)
  }

  export function getPrecision(token: TokenPresets): number {
    switch (token) {
      case TokenPresets.USD:
        return 2
      default:
        return 0
    }
  }
}
