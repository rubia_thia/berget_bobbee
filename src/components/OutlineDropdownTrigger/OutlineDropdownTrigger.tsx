import { StyleProp, View, ViewStyle } from "react-native"
import { FCC } from "../../utils/reactHelpers/types"
import { Stack } from "../Stack"
import { useColors } from "../Themed/color"
import { useSpacing } from "../Themed/spacing"
import DownArrowIcon from "./_/downArrow.svg"

export interface OutlineSelectorTriggerProps {
  style?: StyleProp<ViewStyle>
}

export const OutlineDropdownTrigger: FCC<
  OutlineSelectorTriggerProps
> = props => {
  const spacing = useSpacing()
  const color = useColors()

  return (
    <View
      style={[
        props.style,
        {
          paddingVertical: spacing(2),
          paddingHorizontal: spacing(4),
          borderWidth: 1,
          borderColor: color("gray-300"),
          borderRadius: 6,
          backgroundColor: "white",
        },
      ]}
    >
      <Stack horizontal align={"center"} space={spacing(2.5)}>
        {props.children}
        <DownArrowIcon width={16} height={16} fill={color("gray-400")} />
      </Stack>
    </View>
  )
}
