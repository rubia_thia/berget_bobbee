import { ComponentMeta } from "@storybook/react"
import { Text } from "react-native"
import { BackgroundColor } from "../../../.storybook/decorators"
import { withTemplate } from "../../../.storybook/utils"
import { OutlineDropdownTrigger } from "./OutlineDropdownTrigger"

export default {
  title: "UI/OutlineDropdownTrigger",
  component: OutlineDropdownTrigger,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof OutlineDropdownTrigger>

const template = withTemplate(OutlineDropdownTrigger, {
  style: { margin: 10, alignSelf: "flex-start" },
  children: <Text>Select</Text>,
})

export const Normal = template()
