import { FC } from "react"

import { readResource, SuspenseResource } from "../utils/SuspenseResource"
import { Spensor } from "./Spensor"
import { TokenCount, TokenPresets } from "./TokenCount"

import { StyleProp, Text, TextStyle } from "react-native"
import { BigNumber } from "../utils/numberHelpers/BigNumber"
import { TokenInfo } from "../utils/TokenInfo"
import { TokenName } from "./TokenName"

export const TextTokenCount: FC<{
  style?: StyleProp<TextStyle>
  tokenNameStyle?: StyleProp<TextStyle>
  token: SuspenseResource<TokenInfo>
  count: SuspenseResource<number | BigNumber>
  padDecimals?: boolean
}> = props => (
  <Spensor fallback={"-"}>
    {() => (
      <Text style={props.style}>
        <TokenCount
          token={readResource(props.token)}
          count={readResource(props.count)}
          padDecimals={props.padDecimals}
        />
        &nbsp;
        <TokenName
          style={props.tokenNameStyle}
          token={readResource(props.token)}
        />
      </Text>
    )}
  </Spensor>
)

export const TokenCountAsCurrency: FC<{
  style?: StyleProp<TextStyle>
  tokenNameStyle?: StyleProp<TextStyle>
  token: TokenInfo | TokenPresets
  count: SuspenseResource<number | BigNumber>
  padDecimals?: boolean
}> = props => (
  <Spensor fallback={"-"}>
    {() => (
      <Text style={props.style}>
        $
        <TokenCount
          token={props.token}
          count={readResource(props.count)}
          padDecimals={props.padDecimals}
        />
      </Text>
    )}
  </Spensor>
)
