import { FC } from "react"

import { readResource, SuspenseResource } from "../utils/SuspenseResource"
import { Spensor } from "./Spensor"
import { defaultRenderText, RenderTextFn, TokenCount } from "./TokenCount"

import { StyleProp, TextStyle, View } from "react-native"
import { BigNumber } from "../utils/numberHelpers/BigNumber"
import { MarginStyle } from "../utils/styleHelpers/MarginStyle"
import { TokenInfo } from "../utils/TokenInfo"
import { useSpacing } from "./Themed/spacing"
import { TokenIcon, TokenIconProps } from "./TokenIcon"

export const IconTokenCount: FC<{
  style?: StyleProp<MarginStyle>
  iconStyle?: TokenIconProps["style"]
  countStyle?: StyleProp<TextStyle>
  iconSize?: number
  renderText?: RenderTextFn
  gap?: number
  token: SuspenseResource<TokenInfo>
  count: SuspenseResource<BigNumber>
  padDecimals?: boolean
}> = props => {
  const spacing = useSpacing()

  const {
    gap = spacing(1),
    iconSize = 16,
    renderText = defaultRenderText,
  } = props

  const textStyle: StyleProp<TextStyle> = [
    { fontSize: 14 },
    props.countStyle,
    { marginLeft: gap },
  ]

  return (
    <Spensor fallback={renderText({ style: textStyle, children: <>-</> })}>
      {() => {
        // dont show the component content if token info not prepared
        const token = readResource(props.token)

        return (
          <View
            style={[
              props.style,
              { flexDirection: "row", alignItems: "center" },
            ]}
          >
            <TokenIcon style={props.iconStyle} token={token} size={iconSize} />
            {renderText({
              style: textStyle,
              children: (
                <TokenCount
                  token={token}
                  count={props.count}
                  padDecimals={props.padDecimals}
                />
              ),
            })}
          </View>
        )
      }}
    </Spensor>
  )
}
