import { FC } from "react"
import { StyleProp, Text, TextStyle } from "react-native"
import { BigNumber } from "../utils/numberHelpers/BigNumber"
import { SuspenseResource } from "../utils/SuspenseResource"
import { SpensorR } from "./Spensor"

export interface SimplifiedNumberProps {
  style?: StyleProp<TextStyle>
  number: SuspenseResource<BigNumber>
}

export const SimplifiedNumber: FC<SimplifiedNumberProps> = props => {
  return (
    <Text style={props.style}>
      <SpensorR read={{ number: props.number }} fallback="-">
        {({ number }) => {
          const [num, unit] = simplifyWithUnit(number)
          return (
            <>
              {num}
              {unit.toUpperCase()}
            </>
          )
        }}
      </SpensorR>
    </Text>
  )
}

/**
 * simplifyWithUnit(1_000_000_000) // => [1, 'm']
 */
export function simplifyWithUnit(
  num: BigNumber,
  fractionDigits = 2,
): [formatted: number, unit: string] {
  const toFixed = BigNumber.toFixed({ precision: fractionDigits })
  const isLt = BigNumber.isLt(num)
  const div = BigNumber.div(num)

  if (isLt(1_000)) {
    return [Number(toFixed(num)), ""]
  }

  if (isLt(1_000_000)) {
    return [Number(toFixed(div(1_000))), "k"]
  }

  if (isLt(1_000_000_000)) {
    return [Number(toFixed(div(1_000_000))), "m"]
  }

  if (isLt(1_000_000_000_000)) {
    return [Number(toFixed(div(1_000_000_000))), "b"]
  }

  if (isLt(1_000_000_000_000_000)) {
    return [Number(toFixed(div(1_000_000_000_000))), "t"]
  }

  if (isLt(1_000_000_000_000_000_000)) {
    return [Number(toFixed(div(1_000_000_000_000_000))), "p"]
  }

  if (isLt(1_000_000_000_000_000_000_000)) {
    return [Number(toFixed(div(1_000_000_000_000_000_000))), "e"]
  }

  throw new Error(`Unsupported number ${num}`)
}
export namespace simplifyWithUnit {
  // JavaScript does not support normal numbers greater than 1_000_000_000_000_000_000_000
  export const maxNumber = 1_000_000_000_000_000_000_000
}
