import { ComponentType, useState } from "react"
import { ViewStyle } from "react-native"
import { FCC } from "../../utils/reactHelpers/types"
import { useSpacing } from "../Themed/spacing"

export interface TriggerButtonProps {
  style?: ViewStyle
  collapsed: boolean
  onPress: () => void
}

export interface CollapsableProps {
  TriggerButton: ComponentType<TriggerButtonProps>

  /**
   * @default spacing(1)
   */
  gap?: number

  /**
   * @default true
   */
  defaultCollapsed?: boolean
}

export const Collapsable: FCC<CollapsableProps> = props => {
  const spacing = useSpacing()

  const [collapsed, setCollapsed] = useState(props.defaultCollapsed ?? true)

  return (
    <>
      <props.TriggerButton
        style={{
          marginLeft: "auto",
          marginRight: "auto",
          marginBottom: props.gap ?? spacing(1),
        }}
        collapsed={collapsed}
        onPress={() => {
          setCollapsed(collapsed => !collapsed)
        }}
      />

      {!collapsed && props.children}
    </>
  )
}
