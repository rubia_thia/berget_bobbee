import { FC, ReactNode } from "react"
import { defineMessage, useIntl } from "react-intl"
import { Platform } from "react-native"
import { PlainIconButtonVariant } from "../Button/PlainIconButtonVariant"
import { Button } from "../ButtonFramework/Button"
import { TriggerButtonProps } from "./Collapsable"
import CollapseIcon from "./_/collapse.svg"

export interface DefaultTriggerButtonProps extends TriggerButtonProps {
  text?: ReactNode
}

export const DefaultTriggerButton: FC<DefaultTriggerButtonProps> = props => {
  const { $t } = useIntl()

  return (
    <Button
      style={props.style}
      Variant={p => (
        <PlainIconButtonVariant
          {...p}
          iconLeft={
            props.collapsed ? (
              <CollapseIcon
                style={Platform.select({
                  web: { transform: "rotate(180deg)" as any },
                  native: { transform: [{ rotate: "180deg" }] },
                })}
              />
            ) : (
              <CollapseIcon />
            )
          }
        />
      )}
      onPress={props.onPress}
    >
      {props.text ??
        $t(
          defineMessage({
            defaultMessage: "Details",
            description:
              "Common/Default button text for the collapsable component",
          }),
        )}
    </Button>
  )
}
