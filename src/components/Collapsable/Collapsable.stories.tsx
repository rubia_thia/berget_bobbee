import { ComponentMeta } from "@storybook/react"
import { Text } from "react-native"
import { BackgroundColor, CardContainer } from "../../../.storybook/decorators"
import { withTemplate } from "../../../.storybook/utils"
import { CardInset } from "../CardBox/CardInset"
import { Collapsable } from "./Collapsable"
import { DefaultTriggerButton } from "./DefaultTriggerButton"

export default {
  title: "UI/Collapsable",
  component: Collapsable,
  decorators: [
    CardContainer({
      padding: 10,
    }),
    BackgroundColor(),
  ],
} as ComponentMeta<typeof Collapsable>

const template = withTemplate(Collapsable, {
  TriggerButton: DefaultTriggerButton,
  children: (
    <CardInset style={{ alignSelf: "stretch" }}>
      <Text>Some content</Text>
    </CardInset>
  ),
})

export const Normal = template()
