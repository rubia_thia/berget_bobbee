import { ViewProps } from "react-native"
import { FullWindowOverlay as _FullWindowOverlay } from "react-native-screens"
import { FCC } from "../utils/reactHelpers/types"
import { useDimensions } from "../utils/reactHelpers/useDimensions"

export interface FullWindowOverlayProps extends ViewProps {}

export const FullWindowOverlay: FCC<FullWindowOverlayProps> = props => {
  const dim = useDimensions()

  const { ...restProps } = props

  return (
    <_FullWindowOverlay
      {...restProps}
      style={[
        // https://github.com/software-mansion/react-native-screens/issues/1202
        { width: dim.width, height: dim.height },
        props.style,
      ]}
    />
  )
}
