export type UseReloadAppFn = (resetError: () => void) => () => Promise<void>
