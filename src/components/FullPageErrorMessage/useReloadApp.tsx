import { UseReloadAppFn } from "./useReloadApp.types"

export const useReloadApp: UseReloadAppFn = resetError => {
  /**
   * TODO: we need to replace the impl after we launched Expo EAS update
   */
  return () => {
    resetError()
    return Promise.resolve()
  }
}
