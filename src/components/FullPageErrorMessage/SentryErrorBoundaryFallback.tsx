import { FallbackRender } from "@sentry/react/types/errorboundary"
import * as Clipboard from "expo-clipboard"
import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, View, ViewStyle } from "react-native"
import { DisplayableError, formatError } from "../../utils/errorHelpers"
import { Button } from "../ButtonFramework/Button"
import { SmartLoadableButton } from "../ButtonFramework/LoadableButton"
import {
  ActionButtonVariant,
  DetailText,
  DetailTextInput,
  FullPageErrorMessage,
  recommendedMaxWidth,
} from "./FullPageErrorMessage"
import { useReloadApp } from "./useReloadApp"

type FallbackInfo = Parameters<FallbackRender>[0]

export interface SentryErrorBoundaryFallbackProps extends FallbackInfo {
  style?: StyleProp<ViewStyle>
}

export const SentryErrorBoundaryFallback: FC<
  SentryErrorBoundaryFallbackProps
> = props => {
  const { $t } = useIntl()

  const reloadApp = useReloadApp(props.resetError)

  const { error: _error } = props
  const error: undefined | typeof _error = _error

  const subtitle = error instanceof DisplayableError ? error.message : undefined

  const detailText = `${formatError(error)}\\\\n\\\\nvvvvvvvvvvvv\\\\n${
    props.componentStack
  }`

  const onCopyDetails = async (): Promise<void> => {
    await Clipboard.setStringAsync(detailText)
    alert("Copied")
  }

  return (
    <View style={[props.style, { padding: 20 }]}>
      <FullPageErrorMessage
        style={{
          marginTop: "auto",
          marginBottom: "auto",
          marginLeft: "auto",
          marginRight: "auto",
          width: "100%",
          maxWidth: recommendedMaxWidth,
        }}
        titleText={$t(
          defineMessage({
            defaultMessage: "Oops! Something went wrong.",
            description: "CommonErrorPage/title",
          }),
        )}
        subtitleText={subtitle}
        detail={
          <View>
            <DetailText>
              {$t(
                defineMessage({
                  defaultMessage: "Error Trace: {eventId}",
                  description: "CommonErrorPage/error trace",
                }),
                { eventId: props.eventId },
              )}
            </DetailText>
            <DetailTextInput
              style={{
                marginTop: 4 * 2,
                padding: 4 * 2,
                width: "100%",
                height: 100,
                borderRadius: 4,
                backgroundColor: "rgba(0,0,0,0.1)",
              }}
              value={detailText}
            />
          </View>
        }
        actionArea={
          <View style={{ flexDirection: "row", alignSelf: "flex-start" }}>
            <SmartLoadableButton
              Variant={ActionButtonVariant}
              onPress={reloadApp}
            >
              {$t(
                defineMessage({
                  defaultMessage: "Reload",
                  description: "CommonErrorPage/reload button text",
                }),
              )}
            </SmartLoadableButton>

            <Button
              style={{ marginLeft: 4 * 2 }}
              Variant={ActionButtonVariant}
              onPress={onCopyDetails}
            >
              {$t(
                defineMessage({
                  defaultMessage: "Copy details",
                  description: "CommonErrorPage/copy details button text",
                }),
              )}
            </Button>
          </View>
        }
      />
    </View>
  )
}
