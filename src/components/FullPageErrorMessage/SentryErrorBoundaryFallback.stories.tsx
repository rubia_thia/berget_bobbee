import { action } from "@storybook/addon-actions"
import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../.storybook/decorators"
import { withTemplate } from "../../../.storybook/utils"
import { DisplayableError } from "../../utils/errorHelpers"
import { SentryErrorBoundaryFallback } from "./SentryErrorBoundaryFallback"

export default {
  title: "UI/FullPageErrorMessage/SentryErrorBoundaryFallback",
  component: SentryErrorBoundaryFallback,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof SentryErrorBoundaryFallback>

const causeError = new Error("This is cause error", {
  cause: "Some method name",
})
const error = new DisplayableError("This is a message for a test error", {
  cause: causeError,
})

const tpl = withTemplate(SentryErrorBoundaryFallback, {
  style: {
    flex: 1,
  },
  error,
  eventId: "test-event-id",
  componentStack:
    "at Button (src/components/ButtonFramework/Button.tsx:123:456)",
  resetError: action("resetError"),
})

export const Normal = tpl()
