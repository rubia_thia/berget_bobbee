import { Workbox } from "workbox-window"
import { onWorkboxReady } from "../../utils/enableWorkboxInBrowser"
import { untilArriveWaitingState } from "../../utils/serviceWorkerHelpers"
import { UseReloadAppFn } from "./useReloadApp.types"

function reload(): void {
  window.location.reload()
}

export const useReloadApp: UseReloadAppFn = () => {
  return async () => {
    const wb = await new Promise<undefined | Workbox>(res =>
      onWorkboxReady(res),
    )

    if (wb == null) {
      reload()
      return
    }

    await wb.update()
    const state = await untilArriveWaitingState(await wb.getSW())
    if (state !== "waiting") {
      reload()
      return
    }

    wb.addEventListener("controlling", reload)
    wb.messageSkipWaiting()
  }
}
