import { FC, ReactNode } from "react"
import {
  Image,
  StyleProp,
  Text,
  TextInput,
  TextStyle,
  View,
  ViewStyle,
} from "react-native"
import { useSizeQuery } from "../../utils/reactHelpers/useSizeQuery"
import { styleGetters } from "../../utils/reactHelpers/withProps/styleGetters"
import { withProps } from "../../utils/reactHelpers/withProps/withProps"
import { LightBlueRoundedButtonVariant } from "../Button/LightBlueRoundedButtonVariant"
import { useCompactButtonVariant } from "../Button/useCompactButtonVariant"
import { ButtonVariantProps } from "../ButtonFramework/_/ButtonVariant"
import { useSpacing } from "../Themed/spacing"

export const recommendedMaxWidth = 600

export interface FullPageErrorMessageProps {
  style?: StyleProp<ViewStyle>
  titleText: ReactNode
  subtitleText?: ReactNode
  detail?: ReactNode
  actionArea?: ReactNode
}

export const FullPageErrorMessage: FC<FullPageErrorMessageProps> = props => {
  const spacing = useSpacing()

  const [layout = "row", onLayout] = useSizeQuery({
    0: "column",
    500: "row",
  } as const)

  return (
    <View
      style={[props.style, { flexDirection: layout, alignItems: "flex-start" }]}
      onLayout={onLayout}
    >
      <Image
        style={{ width: 79, height: 72 }}
        source={require("./_/submernia.png")}
      />

      <View
        style={
          layout === "row"
            ? { marginLeft: 50, flex: 1 }
            : { marginTop: 20, alignSelf: "stretch" }
        }
      >
        <TitleText>{props.titleText}</TitleText>

        {props.subtitleText && (
          <SubtitleText>{props.subtitleText}</SubtitleText>
        )}

        {props.detail && (
          <View style={{ marginTop: spacing(5) }}>{props.detail}</View>
        )}

        {props.actionArea && (
          <View style={{ marginTop: spacing(5) }}>{props.actionArea}</View>
        )}
      </View>
    </View>
  )
}

export const ActionButtonVariant: FC<ButtonVariantProps> = props => {
  const CompactButton = useCompactButtonVariant(LightBlueRoundedButtonVariant)

  return <CompactButton {...props} />
}

export const TitleText = withProps(
  styleGetters(({ colors }) => ({
    color: colors("blue-500"),
    fontSize: 24,
    lineHeight: 32,
    fontWeight: "500",
  })),
  Text,
)

export const SubtitleText = withProps(
  styleGetters(({ colors }) => ({
    color: colors("gray-900"),
    fontSize: 16,
    lineHeight: 24,
    fontWeight: "400",
  })),
  Text,
)

export const DetailText = withProps(
  styleGetters(({ colors }) => ({
    color: colors("gray-600"),
    fontSize: 12,
    lineHeight: 16,
    fontWeight: "400",
  })),
  Text,
)
export const DetailTextInput = withProps(
  p => ({
    ...p,
    ...styleGetters<TextStyle>(({ colors }) => ({
      color: colors("gray-600"),
      fontSize: 12,
      lineHeight: 16,
      fontWeight: "400",
    }))(p),
    multiline: true,
  }),
  TextInput,
)
