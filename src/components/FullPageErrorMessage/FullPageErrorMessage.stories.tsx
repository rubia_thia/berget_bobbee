import { ComponentMeta } from "@storybook/react"
import { View } from "react-native"
import { BackgroundColor } from "../../../.storybook/decorators"
import { withTemplate } from "../../../.storybook/utils"
import { Button } from "../ButtonFramework/Button"
import {
  ActionButtonVariant,
  DetailText,
  FullPageErrorMessage,
} from "./FullPageErrorMessage"

export default {
  title: "UI/FullPageErrorMessage",
  component: FullPageErrorMessage,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof FullPageErrorMessage>

const tpl = withTemplate(FullPageErrorMessage, {
  style: {
    marginTop: "auto",
    marginBottom: "auto",
    marginLeft: "auto",
    marginRight: "auto",
  },
  titleText: "Oops! Something went wrong",
  subtitleText: "timeout of 187.393938191937381ms exceeded",
  detail: (
    <View>
      <DetailText>Error Trace:</DetailText>
      <DetailText>e5c87f58d93c46f68d86a0ea8e911c3a</DetailText>
    </View>
  ),
  actionArea: <Button Variant={ActionButtonVariant}>Reload</Button>,
})

export const Normal = tpl()
