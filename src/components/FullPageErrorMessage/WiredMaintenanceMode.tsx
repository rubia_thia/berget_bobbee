import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { View } from "react-native"
import { useAppEnvStore } from "../../stores/AppEnvStore/useAppEnvStore"
import { safelyGet } from "../../stores/utils/waitFor"
import { FCC } from "../../utils/reactHelpers/types"
import { SmartLoadableButton } from "../ButtonFramework/LoadableButton"
import {
  ActionButtonVariant,
  FullPageErrorMessage,
  recommendedMaxWidth,
} from "./FullPageErrorMessage"

const MaintenanceModeContent: FC = () => {
  const { $t } = useIntl()
  return (
    <View className="flex-1">
      <FullPageErrorMessage
        style={{
          marginTop: "auto",
          marginBottom: "auto",
          marginLeft: "auto",
          marginRight: "auto",
          width: "100%",
          maxWidth: recommendedMaxWidth,
        }}
        titleText={$t(
          defineMessage({
            defaultMessage: "Down for Maintenance",
            description: "MaintenancePage/title",
          }),
        )}
        subtitleText="Our app is currently undergoing maintenance. Thank you for your understanding."
        actionArea={
          <View style={{ flexDirection: "row", alignSelf: "flex-start" }}>
            <SmartLoadableButton
              Variant={ActionButtonVariant}
              onPress={() => {
                // use reload
                window.location.reload()
              }}
            >
              {$t(
                defineMessage({
                  defaultMessage: "Reload",
                  description: "MaintenancePage/reload button text",
                }),
              )}
            </SmartLoadableButton>
          </View>
        }
      />
    </View>
  )
}

export const WiredMaintenanceMode: FCC = ({ children }) => {
  const env = useAppEnvStore()
  if (safelyGet(() => env.appEnv$.maintenanceMode) === true) {
    return <MaintenanceModeContent />
  }
  return <>{children}</>
}
