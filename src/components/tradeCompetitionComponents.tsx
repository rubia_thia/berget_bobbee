import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, Text, ViewStyle } from "react-native"
import { Path, Svg } from "react-native-svg"
import { HrefLink } from "../utils/reactNavigationHelpers/HrefLink"
import { CardBoxView } from "./CardBox/CardBox"
import { GradientText } from "./GradientText"
import { LinearGradient } from "./LinearGradient"
import { useSpacing } from "./Themed/spacing"

const link$t = defineMessage({
  defaultMessage:
    "https://medium.com/uniwhale/uniwhale-testnet-trading-competition-f0a03f012f2c",
  description: "Trade Competition Banner/intro article link",
})

const title$t = defineMessage({
  defaultMessage: "Trade Competition",
  description: "Trade Competition Banner/title",
})

const gradientBackground = (
  <LinearGradient
    style={{
      position: "absolute",
      top: 0,
      left: 0,
      width: "100%",
      height: "100%",
    }}
    direction={"vertical"}
    colors={["#1D366D", "#4166B4"]}
  />
)

export const TopNavBarButton: FC<{ style?: StyleProp<ViewStyle> }> = props => {
  const spacing = useSpacing()

  const { $t } = useIntl()

  return (
    <HrefLink
      style={[
        {
          paddingHorizontal: spacing(3),
          paddingVertical: spacing(2),
          borderRadius: 6,
          overflow: "hidden",
        },
        props.style,
      ]}
      href={$t(link$t)}
    >
      {gradientBackground}

      <Text style={{ zIndex: 0, fontSize: 12 }}>
        🏆{" "}
        <GradientText
          gradient={{
            colors: [
              { color: "#FFFFFF", offset: 0.6393 },
              { color: "#006DFF", offset: 1.0479 },
            ],
          }}
        >
          {$t(title$t)}
        </GradientText>
      </Text>
    </HrefLink>
  )
}

export const Banner: FC<{ style?: StyleProp<ViewStyle> }> = props => {
  const { $t } = useIntl()

  const spacing = useSpacing()

  return (
    <HrefLink style={props.style} href={$t(link$t)}>
      <CardBoxView
        style={{
          overflow: "hidden",
          flexDirection: "row",
          alignItems: "center",
        }}
        padding={spacing(4)}
      >
        {gradientBackground}

        <Text style={{ zIndex: 0, fontSize: 16 }}>
          🏆{" "}
          <GradientText
            gradient={{
              colors: [
                { color: "#FFFFFF", offset: 0.6393 },
                { color: "#006DFF", offset: 1.0479 },
              ],
            }}
          >
            {$t(title$t)}
          </GradientText>
        </Text>

        <Svg
          style={{ marginLeft: "auto" }}
          width="16"
          height="16"
          viewBox="0 0 16 16"
          fill="white"
          opacity="0.3"
        >
          <Path d="M14 8.66699V15.3337H0V2.66699H8V4.00033H1.33333V14.0003H12.6667V8.66699H14ZM16 0.666992H8.67467L11.3647 3.33366L6.71333 8.04699L8.59867 9.93233L13.25 5.21899L16 8.00033V0.666992Z" />
        </Svg>
      </CardBoxView>
    </HrefLink>
  )
}
