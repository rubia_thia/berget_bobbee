import { ComponentMeta } from "@storybook/react"
import { BackgroundColor, CardContainer } from "../../.storybook/decorators"
import { withTemplate } from "../../.storybook/utils"
import { AspectRatio } from "./AspectRatio"

export default {
  title: "UI/AspectRatio",
  component: AspectRatio,
  decorators: [CardContainer({ margin: 10, padding: 10 }), BackgroundColor()],
} as ComponentMeta<typeof AspectRatio>

const tpl = withTemplate(AspectRatio, {
  style: { backgroundColor: "blue" },
  ratio: 16 / 9,
})

export const Normal = tpl()
