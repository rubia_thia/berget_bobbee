import { FC } from "react"
import { StyleProp, Text, TextStyle } from "react-native"
import { useColors } from "../Themed/color"
import { PaginationInfo } from "./Pagination"

export const PaginationCountText: FC<{
  style?: StyleProp<TextStyle>
  pagination: PaginationInfo
}> = props => {
  const colors = useColors()

  const startNum =
    props.pagination.pageSize * (props.pagination.current - 1) + 1

  const endNum = Math.min(
    props.pagination.pageSize * props.pagination.current,
    props.pagination.total,
  )

  return (
    <Text style={[{ color: colors("gray-900"), fontSize: 12 }, props.style]}>
      {startNum} - {endNum} / {props.pagination.total}
    </Text>
  )
}
