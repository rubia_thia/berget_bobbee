import { FC } from "react"
import { StyleProp, TouchableOpacity, View, ViewStyle } from "react-native"
import { Path, Svg } from "react-native-svg"
import { FCC } from "../../utils/reactHelpers/types"
import { useSpacing } from "../Themed/spacing"
import type { PaginationInfo, PaginationPatch } from "./Pagination"
import { PaginationCountText } from "./PaginationCountText"

export interface SimplePaginationProps extends PaginationInfo {
  style?: StyleProp<ViewStyle>

  onChange: (patch: PaginationPatch) => void
}

export const SimplePagination: FC<SimplePaginationProps> = props => {
  const spacing = useSpacing()

  const maxPage = Math.ceil(props.total / props.pageSize)

  return (
    <View style={[{ flexDirection: "row", alignItems: "center" }, props.style]}>
      <ArrowButton
        style={{ marginRight: spacing(2.5) }}
        disabled={props.current <= 1}
        onPress={() =>
          props.onChange({
            current: Math.max(1, props.current - 1),
          })
        }
      >
        <Svg width="16" height="16" viewBox="0 0 16 16" fill="black">
          <Path d="M4.91355 7.97577L10.6861 13.7914L11.6289 12.8486L6.75574 7.97539L11.6282 3.10297L10.6854 2.16016L4.91355 7.97577Z" />
        </Svg>
      </ArrowButton>

      <PaginationCountText
        style={{ marginLeft: "auto", marginRight: "auto" }}
        pagination={props}
      />

      <ArrowButton
        style={{ marginRight: spacing(2.5) }}
        disabled={props.current >= maxPage}
        onPress={() =>
          props.onChange({
            current: Math.min(maxPage, props.current + 1),
          })
        }
      >
        <Svg width="16" height="16" viewBox="0 0 16 16" fill="black">
          <Path d="M11.6294 7.97577L5.85688 13.7914L4.91406 12.8486L9.78723 7.97539L4.91481 3.10297L5.85761 2.16016L11.6294 7.97577Z" />
        </Svg>
      </ArrowButton>
    </View>
  )
}

const ArrowButton: FCC<{
  style?: StyleProp<ViewStyle>
  disabled?: boolean
  onPress: () => void
}> = props => {
  const spacing = useSpacing()

  return (
    <TouchableOpacity
      style={[
        props.style,
        props.disabled ? { opacity: 0.5 } : {},
        {
          paddingVertical: spacing(2.5),
          paddingHorizontal: spacing(4),
        },
      ]}
      disabled={props.disabled}
      onPress={props.onPress}
    >
      {props.children}
    </TouchableOpacity>
  )
}
