import { ComponentMeta } from "@storybook/react"
import { withTemplate } from "../../../.storybook/utils"
import { SimplePagination } from "./SimplePagination"

export default {
  title: "UI/Pagination/SimplePagination",
  component: SimplePagination,
} as ComponentMeta<typeof SimplePagination>

const template = withTemplate(SimplePagination, {
  current: 3,
  pageSize: 20,
  total: 1034,
})

export const Normal = template()
