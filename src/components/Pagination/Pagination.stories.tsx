import { ComponentMeta } from "@storybook/react"
import { withTemplate } from "../../../.storybook/utils"
import { Pagination } from "./Pagination"

export default {
  title: "UI/Pagination/Pagination",
  component: Pagination,
} as ComponentMeta<typeof Pagination>

const template = withTemplate(Pagination, {
  current: 3,
  pageSize: 20,
  total: 1034,
})

export const Normal = template()
