import { FC } from "react"
import { StyleProp, Text, ViewStyle } from "react-native"
import { Path, Svg } from "react-native-svg"
import { useButtonState } from "../Button/useButtonState"
import { Pressable } from "../Pressable"
import { Stack } from "../Stack"
import { useColors } from "../Themed/color"
import { useSpacing } from "../Themed/spacing"
import { PaginationItem, usePagination } from "./usePagination"

export interface PaginationPatch {
  current: number
}

export interface PaginationInfo {
  /**
   * 1-based index of current page
   */
  current: number

  total: number

  pageSize: number
}

export interface PaginationProps extends PaginationInfo {
  style?: StyleProp<ViewStyle>

  onChange: (patch: PaginationPatch) => void
}

export const Pagination: FC<PaginationProps> = props => {
  const spacing = useSpacing()

  const items = usePagination({
    pageSize: props.pageSize ?? 20,
    current: props.current,
    total: props.total,
  })

  return (
    <Stack
      style={props.style}
      horizontal={true}
      align={"center"}
      space={spacing(0.5)}
    >
      {items.map((item, idx) => (
        <ItemButton
          item={item}
          key={idx}
          onPress={() => {
            if ("disabled" in item && item.disabled) return
            if (props.current === item.page) return
            props.onChange({ current: item.page })
          }}
        />
      ))}
    </Stack>
  )
}

const ItemButton: FC<{
  style?: StyleProp<ViewStyle>
  item: PaginationItem
  onPress: () => void
}> = props => {
  const spacing = useSpacing()
  const colors = useColors()

  const [state, eventListeners] = useButtonState({
    disabled: "disabled" in props.item && props.item.disabled,
  })

  const contentColor =
    state === "hovering" || state === "pressing"
      ? colors("blue-600")
      : props.item.type === "jump-prev" || props.item.type === "jump-next"
      ? colors("gray-500")
      : colors("gray-900")

  let content: JSX.Element
  if (props.item.type === "prev") {
    content = (
      <Svg width="16" height="16" viewBox="0 0 16 16" fill={contentColor}>
        <Path d="M4.91551 8.75312L10.688 14.5687L11.6309 13.6259L6.75769 8.75273L11.6301 3.88032L10.6873 2.9375L4.91551 8.75312Z" />
      </Svg>
    )
  } else if (props.item.type === "next") {
    content = (
      <Svg width="16" height="16" viewBox="0 0 16 16" fill={contentColor}>
        <Path d="M11.6304 8.75312L5.85786 14.5687L4.91504 13.6259L9.78821 8.75273L4.91579 3.88032L5.85859 2.9375L11.6304 8.75312Z" />
      </Svg>
    )
  } else {
    content = (
      <Text
        style={{
          fontSize: 12,
          lineHeight: 16,
          color: contentColor,
          fontWeight: "500",
        }}
      >
        {props.item.type === "jump-prev" || props.item.type === "jump-next"
          ? "..."
          : props.item.page}
      </Text>
    )
  }

  return (
    <Pressable
      {...eventListeners}
      style={[
        props.style,
        {
          paddingVertical: spacing(1),
          paddingHorizontal: spacing(4),
          borderRadius: 8,
        },
        "active" in props.item &&
          props.item.active && { backgroundColor: colors("blue-100") },
        state === "pressing" && { backgroundColor: colors("blue-200") },
        state === "disabled" && { opacity: 0.3 },
      ]}
      onPress={props.onPress}
    >
      {content}
    </Pressable>
  )
}
