import { ComponentMeta } from "@storybook/react"
import { withTemplate } from "../../../.storybook/utils"
import { PaginationCountText } from "./PaginationCountText"

export default {
  title: "UI/Pagination/PaginationCountText",
  component: PaginationCountText,
} as ComponentMeta<typeof PaginationCountText>

const template = withTemplate(PaginationCountText, {
  pagination: {
    current: 3,
    pageSize: 20,
    total: 1034,
  },
})

export const Normal = template()
