import { Story } from "@storybook/react"
import { useState } from "react"
import { Text, TouchableOpacity, View } from "react-native"
import { checkNever } from "../../utils/typeHelpers"
import { usePagination } from "./usePagination"

export default {
  title: "UI/Pagination/usePagination",
}

export const Normal: Story = () => {
  const [currentPage, setCurrentPage] = useState(3)

  const items = usePagination({
    current: currentPage,
    pageSize: 20,
    total: 1034,
  })

  return (
    <View style={{ flexDirection: "row" }}>
      {items.map((item, idx) => (
        <TouchableOpacity
          key={idx}
          style={{
            marginRight: 8,
            padding: 5,
            opacity: "disabled" in item && item.disabled ? 0.3 : 1,
            borderRadius: 2,
            backgroundColor: "#eaeaea",
          }}
          disabled={"disabled" in item && item.disabled}
          onPress={() => setCurrentPage(item.page)}
        >
          {item.type === "pager" ||
          item.type === "jump-first" ||
          item.type === "jump-last" ? (
            <Text style={{ color: item.active ? "black" : "blue" }}>
              {item.page}
            </Text>
          ) : item.type === "prev" ? (
            <Text>&lt;</Text>
          ) : item.type === "next" ? (
            <Text>&gt;</Text>
          ) : item.type === "jump-prev" || item.type === "jump-next" ? (
            <Text>...</Text>
          ) : (
            checkNever(item.type)
          )}
        </TouchableOpacity>
      ))}
    </View>
  )
}
