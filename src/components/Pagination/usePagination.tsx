import { useMemo } from "react"

function calcLastPage(pageSize: number, total: number): number {
  return Math.floor((total - 1) / pageSize) + 1
}

export type PaginationItem =
  | {
      type: "pager" | "jump-first" | "jump-last"
      page: number
      active?: boolean
    }
  | {
      type: "jump-prev" | "jump-next"
      page: number
    }
  | {
      type: "prev" | "next"
      page: number
      disabled?: boolean
    }

export interface UsePaginationOptions {
  /**
   * 1 based page index
   */
  current?: number
  total?: number
  pageSize?: number
  showLessItems?: boolean
}

export function usePagination(options: UsePaginationOptions): PaginationItem[] {
  const { current = 1, total = 0, pageSize = 10, showLessItems } = options

  const pageBufferSize = showLessItems ? 1 : 2
  const centerPagerBtnCount = 1 /* current page btn */ + pageBufferSize * 2
  const lastPage = calcLastPage(pageSize, total)
  const hasPrevPage = current > 1
  const hasNextPage = current < lastPage

  return useMemo(() => {
    const prevBtn: PaginationItem = {
      type: "prev",
      page: Math.max(current - 1, 0),
      disabled: !hasPrevPage || !lastPage,
    }
    const lastBtn: PaginationItem = {
      type: "next",
      page: Math.min(current + 1, lastPage),
      disabled: !hasNextPage || !lastPage,
    }

    const jumpPrevBtn: PaginationItem = {
      type: "jump-prev",
      page: Math.max(1, current - centerPagerBtnCount),
    }
    const jumpNextBtn: PaginationItem = {
      type: "jump-next",
      page: Math.min(lastPage, current + centerPagerBtnCount),
    }

    const jumpFirstBtn: PaginationItem = {
      type: "jump-first",
      page: 1,
      active: current === 1,
    }
    const jumpLastBtn: PaginationItem = {
      type: "jump-last",
      page: lastPage,
      active: current === lastPage,
    }

    const itemList: PaginationItem[] = []
    if (!lastPage) {
      itemList.push({ type: "pager", page: 1, active: true })
    } else if (lastPage <= 2 /* prev/next btn */ + centerPagerBtnCount) {
      /* < 1 2 3 4 5 > */
      for (let i = 1; i <= lastPage; i += 1) {
        itemList.push({ type: "pager", page: i, active: current === i })
      }
    } else {
      let left = Math.max(1, current - pageBufferSize)
      let right = Math.min(current + pageBufferSize, lastPage)
      if (current - 1 <= pageBufferSize) {
        /* < 1 2 [3] 4 5 ... 23 > */
        right = 1 + pageBufferSize * 2
      }
      if (lastPage - pageBufferSize <= current) {
        /* < 1 ... 19 20 [21] 22 23 > */
        left = lastPage - pageBufferSize * 2
      }

      for (let i = left; i <= right; i += 1) {
        itemList.push({ type: "pager", page: i, active: current === i })
      }

      if (current - 1 >= pageBufferSize * 2 && current !== 1 + 2) {
        itemList.unshift(jumpPrevBtn)
      }
      if (
        lastPage - current >= pageBufferSize * 2 &&
        current !== lastPage - 2
      ) {
        itemList.push(jumpNextBtn)
      }

      if (left !== 1) {
        itemList.unshift(jumpFirstBtn)
      }
      if (right !== lastPage) {
        itemList.push(jumpLastBtn)
      }
    }
    return [prevBtn, ...itemList, lastBtn]
  }, [
    centerPagerBtnCount,
    current,
    hasNextPage,
    hasPrevPage,
    lastPage,
    pageBufferSize,
  ])
}
