import { Meta } from "@storybook/react"
import { noop } from "lodash"
import { FC } from "react"
import { withTemplate } from "../../../.storybook/utils"
import {
  ErrorIcon,
  MessageItem,
  MessageItemBodyText,
  MessageItemCountdownProvider,
  MessageItemTitleText,
  SuccessIcon,
} from "./MessageItem"
import { MessageProviderDecorator } from "./MessageProvider.storiesHelpers"
import { SwitchToLatestAppVersionMessageItem as _SwitchToLatestAppVersionMessageItem } from "./SwitchToLatestAppVersionMessageItem"

const longText =
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

export default {
  title: "UI/MessageProvider/MessageItem",
  decorators: [MessageProviderDecorator()],
} as Meta

const messageItemTpl = withTemplate(MessageItem, {
  title: longText,
  onClose: noop,
})

export const Normal = messageItemTpl()

export const WithIcon = messageItemTpl(p => {
  p.icon = <SuccessIcon />
})

export const WithLongContent = messageItemTpl(p => {
  p.icon = <ErrorIcon />
  p.title = <MessageItemTitleText>{longText}</MessageItemTitleText>
  p.content = <MessageItemBodyText>{longText}</MessageItemBodyText>
})

export const WithCountdown = messageItemTpl(p => {
  p.title = <MessageItemTitleText>Title</MessageItemTitleText>
  p.content = <MessageItemBodyText>Some content</MessageItemBodyText>
})
WithCountdown.decorators = [
  Story => (
    <MessageItemCountdownProvider countdownMs={1000 * 30}>
      <Story />
    </MessageItemCountdownProvider>
  ),
]

export const WithErrorState = messageItemTpl(p => {
  p.title = <MessageItemTitleText>Title</MessageItemTitleText>
  p.content = <MessageItemBodyText>Some content</MessageItemBodyText>
  p.error = true
})

export const SwitchToLatestAppVersionMessageItem: FC = () => (
  <_SwitchToLatestAppVersionMessageItem onSwitch={noop} onDismiss={noop} />
)
