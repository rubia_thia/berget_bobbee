import { Platform } from "react-native"
import { TopNavBarHeight } from "../TopNavBar/TopNavBar"
import { MessageProviderProps } from "./MessageProvider"

export const wiredRecommendedMessageProviderProps: MessageProviderProps = {
  messagesContainerStyles:
    Platform.OS === "web"
      ? {
          position: "fixed" as any,
          top: TopNavBarHeight + 10,
          right: 10,
          width: "auto",
        }
      : {
          position: "absolute",
          left: "auto",
          bottom: "auto",
          top: TopNavBarHeight + 10,
          right: 10,
          width: "auto",
          alignSelf: "flex-end",
        },
}
