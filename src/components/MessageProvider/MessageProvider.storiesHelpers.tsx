import { DecoratorFn } from "@storybook/react"
import { MessageProvider } from "./MessageProvider"
import { wiredRecommendedMessageProviderProps } from "./wiredRecommendedMessageProviderProps"

export const MessageProviderDecorator = (): DecoratorFn => Story => {
  return (
    <MessageProvider {...wiredRecommendedMessageProviderProps}>
      <Story />
    </MessageProvider>
  )
}
