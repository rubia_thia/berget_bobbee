import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { TouchableOpacity } from "react-native"
import {
  MessageItem,
  MessageItemBodyText,
  MessageItemBodyTextForLink,
  MessageItemTitleText$Info,
} from "./MessageItem"

export const SwitchToLatestAppVersionMessageItem: FC<{
  onSwitch: () => void
  onDismiss: () => void
}> = props => {
  const { $t } = useIntl()

  return (
    <MessageItem
      title={
        <MessageItemTitleText$Info>
          {$t(
            defineMessage({
              defaultMessage: "App version updated",
              description: "AppUpdatedNotification/title",
            }),
          )}
        </MessageItemTitleText$Info>
      }
      content={
        <>
          <MessageItemBodyText>
            {$t(
              defineMessage({
                defaultMessage: 'Press "Switch" to switch to the new version',
                description: "AppUpdatedNotification/body text",
              }),
            )}
          </MessageItemBodyText>
          <TouchableOpacity onPress={props.onSwitch}>
            <MessageItemBodyTextForLink>
              {$t(
                defineMessage({
                  defaultMessage: "Switch",
                  description: "AppUpdatedNotification/switch button text",
                }),
              )}
            </MessageItemBodyTextForLink>
          </TouchableOpacity>
        </>
      }
      onClose={props.onDismiss}
    />
  )
}
