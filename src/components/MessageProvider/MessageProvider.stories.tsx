import { Meta } from "@storybook/react"
import { FC } from "react"
import { Button } from "../ButtonFramework/Button"
import { useMessage } from "./MessageProvider"
import { MessageProviderDecorator } from "./MessageProvider.storiesHelpers"

const longText =
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

export default {
  title: "UI/MessageProvider",
  decorators: [MessageProviderDecorator()],
} as Meta

export const Basic: FC = () => {
  const message = useMessage()

  return (
    <Button
      onPress={() => {
        message.show({ message: longText.slice(0, 30) })
      }}
    >
      Show a message
    </Button>
  )
}
