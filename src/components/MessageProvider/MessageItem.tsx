import { createContext, FC, ReactNode, useContext, useEffect } from "react"
import { StyleProp, Text, TextStyle, View, ViewStyle } from "react-native"
import Animated, {
  Easing,
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from "react-native-reanimated"
import { Path, Svg } from "react-native-svg"
import { wrapText } from "../../utils/reactHelpers/childrenHelpers"
import { FCC } from "../../utils/reactHelpers/types"
import { useDimensions } from "../../utils/reactHelpers/useDimensions"
import { styleGetters } from "../../utils/reactHelpers/withProps/styleGetters"
import { withProps } from "../../utils/reactHelpers/withProps/withProps"
import {
  PaddingStyle,
  useNormalizePaddingStyle,
} from "../../utils/styleHelpers/PaddingStyle"
import { PlainIconButtonVariant } from "../Button/PlainIconButtonVariant"
import { Button } from "../ButtonFramework/Button"
import { CardBoxView } from "../CardBox/CardBox"
import { useColors } from "../Themed/color"
import { useSpacing } from "../Themed/spacing"

const MessageItemCountdownContext = createContext<null | number>(null)
export const MessageItemCountdownProvider: FCC<{
  countdownMs?: null | number
}> = props => {
  return (
    <MessageItemCountdownContext.Provider value={props.countdownMs ?? null}>
      {props.children}
    </MessageItemCountdownContext.Provider>
  )
}

const MessageItemOnCloseFnContext = createContext<null | (() => void)>(null)
export const MessageItemOnCloseFnProvider: FCC<{
  onClose: () => void
}> = props => {
  return (
    <MessageItemOnCloseFnContext.Provider value={props.onClose}>
      {props.children}
    </MessageItemOnCloseFnContext.Provider>
  )
}

export interface MessageItemProps {
  style?: StyleProp<ViewStyle>
  error?: boolean
  icon?: ReactNode
  title: ReactNode
  content?: ReactNode
  onClose?: () => void
}

export const MessageItem: FC<MessageItemProps> = props => {
  const spacing = useSpacing()
  const colors = useColors()

  const paddingStyle = useNormalizePaddingStyle(spacing(4))

  const contextualOnClose = useContext(MessageItemOnCloseFnContext)
  const onClose = props.onClose ?? contextualOnClose

  const countdownProgress = useSharedValue(0)
  const countdownMs = useContext(MessageItemCountdownContext)
  useEffect(() => {
    if (countdownMs == null) return

    countdownProgress.value = 0
    countdownProgress.value = withTiming(100, {
      easing: Easing.linear,
      duration: countdownMs,
    })
  }, [countdownProgress, countdownMs])

  return (
    <MessageItemContainer
      style={props.style}
      padding={{
        ...paddingStyle,
        ...(countdownMs != null ? { paddingBottom: spacing(2) } : {}),
      }}
      backgroundColor={props.error ? colors("red-500") : undefined}
    >
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        {props.icon != null && (
          <View style={{ flexShrink: 0, marginRight: spacing(2.5) }}>
            {props.icon}
          </View>
        )}

        <View style={{ flex: 1 }}>
          {wrapText(props.title, {
            Text: MessageItemTitleText,
          })}
        </View>

        {onClose != null && (
          <Button
            style={{ marginLeft: spacing(2.5), flexShrink: 0 }}
            onPress={onClose}
            renderVariant={p => (
              <PlainIconButtonVariant iconLeft={<CloseIcon />} {...p} />
            )}
          />
        )}
      </View>

      {props.content != null && (
        <View style={{ marginTop: spacing(1) }}>
          {wrapText(props.content, {
            Text: MessageItemBodyText,
          })}
        </View>
      )}

      {countdownMs != null && (
        <ProgressBar
          style={{ marginTop: paddingStyle.paddingBottom }}
          progress={countdownProgress}
        />
      )}
    </MessageItemContainer>
  )
}

export const MessageItemContainer: FCC<{
  style?: StyleProp<ViewStyle>
  padding?: number | PaddingStyle
  backgroundColor?: string
}> = props => {
  const dim = useDimensions()
  return (
    <CardBoxView
      style={[
        {
          maxWidth: dim.width * 0.8,
          minWidth: 300,
          borderRadius: 8,
        },
        props.style,
      ]}
      padding={props.padding}
      backgroundColor={props.backgroundColor ?? "rgba(26, 26, 26, 0.9)"}
    >
      {props.children}
    </CardBoxView>
  )
}

export const MessageItemTitleText: FCC<{
  style?: StyleProp<TextStyle>
  /**
   * @default true
   */
  uppercase?: boolean
}> = props => {
  return (
    <MessageItemBodyText
      style={[
        (props.uppercase ?? true) && { textTransform: "uppercase" },
        props.style,
      ]}
    >
      {props.children}
    </MessageItemBodyText>
  )
}
export const MessageItemTitleText$Info = withProps(
  styleGetters(({ colors }) => ({
    color: colors("blue-500"),
  })),
  MessageItemTitleText,
)
export const MessageItemTitleText$Warn = withProps(
  styleGetters(({ colors }) => ({
    color: colors("yellow-400"),
  })),
  MessageItemTitleText,
)

export const MessageItemBodyText: FCC<{
  style?: StyleProp<TextStyle>
}> = props => {
  const colors = useColors()

  return (
    <Text
      style={[
        {
          fontSize: 16,
          lineHeight: 24,
          fontWeight: "500",
          color: colors("white"),
        },
        props.style,
      ]}
    >
      {props.children}
    </Text>
  )
}
export const MessageItemBodyTextForLink = withProps(
  styleGetters(({ colors }) => ({
    color: colors("blue-500"),
  })),
  MessageItemBodyText,
)

export const ProgressBar: FC<{
  style?: StyleProp<ViewStyle>

  /**
   * 0-100
   */
  progress: Animated.SharedValue<number>

  /**
   * @default colors('gray-900')
   */
  backgroundColor?: string

  /**
   * @default colors('gray-400')
   */
  progressColor?: string
}> = props => {
  const colors = useColors()

  const height = 5

  const style = useAnimatedStyle(() => ({
    width: `${Math.min(props.progress.value, 100)}%`,
  }))

  return (
    <View
      style={[
        {
          height: height,
          backgroundColor: props.backgroundColor ?? colors("gray-900"),
        },
        props.style,
      ]}
    >
      <Animated.View
        style={[
          style,
          {
            height: height,
            backgroundColor: props.progressColor ?? colors("gray-400"),
          },
        ]}
      />
    </View>
  )
}

export const SuccessIcon: FC = () => (
  <Svg width={18} height={18} viewBox="0 0 18 18" fill="#16A34A">
    <Path d="M9 0a9 9 0 100 18A9 9 0 009 0zm-.938 12.389l-3.374-3.24 1.046-1.076 2.309 2.203 4.579-4.664 1.066 1.057-5.626 5.72z" />
  </Svg>
)

export const ErrorIcon: FC = () => (
  <Svg width={18} height={18} viewBox="0 0 18 18" fill="#DC2626">
    <Path d="M9 0a9 9 0 100 18A9 9 0 009 0zm3.448 13.466l-3.444-3.413L5.588 13.5l-1.053-1.054 3.41-3.444L4.5 5.588l1.054-1.053 3.44 3.407 3.41-3.442 1.062 1.052-3.41 3.44 3.444 3.411-1.052 1.063z" />
  </Svg>
)

export const WarnIcon: FC = () => (
  <Svg width="18" height="18" viewBox="0 0 16 16" fill="none">
    <Path
      d="M9.33327 1.8666C9.33327 1.93327 9.4666 2.0666 9.6666 2.39994C9.8666 2.73327 10.0666 3.0666 10.3333 3.59993C10.5999 4.13327 10.9333 4.59993 11.2666 5.19993C11.5999 5.79993 11.9999 6.39994 12.3333 6.99994C12.6666 7.59994 13.0666 8.2666 13.3999 8.8666C13.7333 9.4666 14.0666 10.0666 14.3999 10.5333C14.6666 11.0666 14.9333 11.4666 15.1999 11.8666C15.4666 12.2666 15.5333 12.4666 15.6666 12.5999C15.7999 12.8666 15.9333 13.1333 15.9333 13.3999C15.9999 13.6666 15.9333 13.8666 15.8666 14.1333C15.7999 14.3999 15.6666 14.5333 15.5333 14.6666C15.3333 14.7999 15.1333 14.8666 14.8666 14.8666H1.39993C0.999935 14.8666 0.666602 14.7999 0.466602 14.6666C0.266602 14.5333 0.133268 14.3333 0.0666016 14.1333C-6.51032e-05 13.9333 -6.51032e-05 13.6666 0.0666016 13.4666C0.133268 13.1999 0.199935 12.9999 0.399935 12.7333C0.466602 12.5999 0.599935 12.3999 0.799935 11.9999C0.999935 11.6666 1.2666 11.1999 1.59993 10.7333C1.93327 10.1999 2.2666 9.6666 2.59993 9.0666C2.93327 8.4666 3.33327 7.79993 3.73327 7.19993C4.13327 6.59994 4.4666 5.93327 4.79993 5.33327C5.13327 4.73327 5.4666 4.19993 5.73327 3.73327C5.99993 3.2666 6.2666 2.8666 6.4666 2.53327L6.79993 1.99993C6.93327 1.79993 7.13327 1.59993 7.39993 1.4666C7.6666 1.33327 7.8666 1.2666 8.13327 1.2666C8.39993 1.2666 8.59994 1.33327 8.8666 1.39993C8.99994 1.4666 9.19993 1.6666 9.33327 1.8666ZM8.93327 4.93327C8.93327 4.79993 8.93327 4.6666 8.8666 4.59993C8.79994 4.4666 8.73327 4.39994 8.6666 4.33327C8.59994 4.2666 8.4666 4.19993 8.33327 4.13327C8.19993 4.0666 8.0666 3.99993 7.93327 3.99993C7.6666 3.99993 7.4666 4.0666 7.2666 4.2666C7.0666 4.4666 6.93327 4.6666 6.93327 4.93327V9.0666C6.93327 9.33327 7.0666 9.53327 7.2666 9.73327C7.4666 9.93327 7.6666 9.99994 7.93327 9.99994C8.19993 9.99994 8.39994 9.93327 8.59994 9.73327C8.79994 9.53327 8.93327 9.33327 8.93327 9.0666V4.93327ZM7.93327 10.9333C7.6666 10.9333 7.39993 10.9999 7.2666 11.1999C7.13327 11.3999 6.99993 11.5999 6.99993 11.8666C6.99993 12.1333 7.0666 12.3999 7.2666 12.5333C7.4666 12.7333 7.6666 12.7999 7.93327 12.7999C8.19993 12.7999 8.4666 12.7333 8.59994 12.5333C8.79994 12.3333 8.8666 12.1333 8.8666 11.8666C8.8666 11.5999 8.79994 11.3333 8.59994 11.1999C8.4666 10.9999 8.19993 10.9333 7.93327 10.9333Z"
      fill="#ECD71F"
    />
    <Path
      d="M8.93359 4.93333C8.93359 4.8 8.93359 4.66667 8.86693 4.6C8.80026 4.46667 8.73359 4.4 8.66693 4.33333C8.60026 4.26667 8.46693 4.2 8.33359 4.13333C8.20026 4.06667 8.06693 4 7.93359 4C7.66693 4 7.46693 4.06667 7.26693 4.26667C7.06693 4.46667 6.93359 4.66667 6.93359 4.93333V9.06667C6.93359 9.33333 7.06693 9.53333 7.26693 9.73333C7.46693 9.93333 7.66693 10 7.93359 10C8.20026 10 8.40026 9.93333 8.60026 9.73333C8.80026 9.53333 8.93359 9.33333 8.93359 9.06667V4.93333ZM7.93359 10.9333C7.66693 10.9333 7.40026 11 7.26693 11.2C7.13359 11.4 7.00026 11.6 7.00026 11.8667C7.00026 12.1333 7.06693 12.4 7.26693 12.5333C7.46693 12.7333 7.66693 12.8 7.93359 12.8C8.20026 12.8 8.46693 12.7333 8.60026 12.5333C8.80026 12.3333 8.86693 12.1333 8.86693 11.8667C8.86693 11.6 8.80026 11.3333 8.60026 11.2C8.46693 11 8.20026 10.9333 7.93359 10.9333Z"
      fill="black"
    />
  </Svg>
)

export const CloseIcon: FC = () => (
  <Svg width="16" height="16" viewBox="0 0 16 16" fill="none">
    <Path
      d="M9.62602 7.80488L13.8537 3.54472C14.0488 3.34959 14.0488 3.05691 13.8537 2.86179L13.2033 2.17886C13.0081 1.98374 12.7154 1.98374 12.5203 2.17886L8.26016 6.43902C8.13008 6.5691 7.93496 6.5691 7.80488 6.43902L3.54472 2.14634C3.34959 1.95122 3.05691 1.95122 2.86179 2.14634L2.17886 2.82927C1.98374 3.02439 1.98374 3.31707 2.17886 3.51219L6.43902 7.77236C6.56911 7.90244 6.56911 8.09756 6.43902 8.22764L2.14634 12.5203C1.95122 12.7154 1.95122 13.0081 2.14634 13.2032L2.82927 13.8862C3.02439 14.0813 3.31707 14.0813 3.51219 13.8862L7.77236 9.62601C7.90244 9.49593 8.09756 9.49593 8.22764 9.62601L12.4878 13.8862C12.6829 14.0813 12.9756 14.0813 13.1707 13.8862L13.8537 13.2032C14.0488 13.0081 14.0488 12.7154 13.8537 12.5203L9.62602 8.26016C9.49593 8.13008 9.49593 7.93496 9.62602 7.80488Z"
      fill="white"
      opacity={0.44}
    />
  </Svg>
)
