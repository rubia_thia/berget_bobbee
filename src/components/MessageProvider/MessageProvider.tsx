import {
  createContext,
  ReactElement,
  Ref,
  useContext,
  useEffect,
  useImperativeHandle,
  useMemo,
  useRef,
  useState,
} from "react"
import { ViewStyle } from "react-native"
import { FCC } from "../../utils/reactHelpers/types"
import { usePersistFn } from "../../utils/reactHelpers/usePersistFn"
import { Stack } from "../Stack"
import {
  MessageItem,
  MessageItemCountdownProvider,
  MessageItemOnCloseFnProvider,
  MessageItemTitleText,
} from "./MessageItem"

export interface MessageController {
  show(
    options:
      | MessageController.ShowOptions
      | MessageController.ShowOptionsFactory,
  ): () => void

  error(options: MessageController.ShowOptions): void
}

export namespace MessageController {
  export interface ShowOptionsFactoryInfo {
    close: () => void
  }

  export type ShowOptionsFactory = (info: ShowOptionsFactoryInfo) => ShowOptions

  export interface ShowOptions {
    /**
     * Message will be replaced by the new message which has the same key
     */
    key?: string

    message: string | ReactElement

    /**
     * Auto dismiss after how many ms
     *
     * @default 5000 (5s)
     */
    autoDismiss?: null | number
  }
}

interface BaseMessageController {
  show(
    options:
      | MessageController.ShowOptions
      | MessageController.ShowOptionsFactory,
  ): () => void
}

const MessageContext = createContext<null | BaseMessageController>(null)

type ShowOptions = MessageController.ShowOptions
type ShowOptionsFactory = MessageController.ShowOptionsFactory

interface MessageInstance extends MessageController.ShowOptions {
  id: string
}

export interface MessageProviderProps {
  messagesContainerStyles?: ViewStyle

  messageControllerRef?: Ref<MessageController>
}

export const MessageProvider: FCC<MessageProviderProps> = props => {
  const [messages, setMessages] = useState<MessageInstance[]>([])

  const closedMessageIdsRef = useRef<string[]>([])

  const autoDismissTimersRef = useRef<ReturnType<typeof setTimeout>[]>([])

  const closeMsg = usePersistFn((msgId: string) => {
    closedMessageIdsRef.current.push(msgId)
    setMessages(msgs => msgs.filter(m => m.id !== msgId))
  })

  const addMsg = usePersistFn((_opts: ShowOptions | ShowOptionsFactory) => {
    const id = getGuid()

    const closeThisMsg = (): void => {
      closeMsg(id)
    }

    const factoryInfo: MessageController.ShowOptionsFactoryInfo = {
      close: closeThisMsg,
    }

    const opts = {
      autoDismiss: 1000 * 5,
      ...(typeof _opts === "function" ? _opts(factoryInfo) : _opts),
    }

    setMessages(messages => {
      if (opts.key != null) {
        messages = messages.filter(m => m.key !== opts.key)
      }

      return messages.concat({ id, ...opts })
    })

    if (opts.autoDismiss != null) {
      autoDismissTimersRef.current.push(
        setTimeout(() => {
          closeMsg(id)
        }, opts.autoDismiss),
      )
    }

    return closeThisMsg
  })

  const ctrl: BaseMessageController = useMemo(
    () => ({
      show(opts) {
        return addMsg(opts)
      },
    }),
    [addMsg],
  )

  useEffect(
    () => () => {
      autoDismissTimersRef.current.forEach(timer => {
        clearTimeout(timer)
      })
    },
    [],
  )

  useImperativeHandle(
    props.messageControllerRef,
    () => completeInternalMessageController(ctrl),
    [ctrl],
  )

  return (
    <>
      <MessageContext.Provider value={ctrl}>
        {props.children}
      </MessageContext.Provider>

      <Stack style={props.messagesContainerStyles} space={2.5}>
        {messages
          .slice()
          .reverse()
          .slice(0, 5)
          .map(m => (
            <div key={m.id} data-id={m.id}>
              <MessageItemCountdownProvider countdownMs={m.autoDismiss}>
                <MessageItemOnCloseFnProvider onClose={() => closeMsg(m.id)}>
                  {typeof m.message === "string" ? (
                    <MessageItem
                      title={
                        <MessageItemTitleText>{m.message}</MessageItemTitleText>
                      }
                      onClose={() => closeMsg(m.id)}
                    />
                  ) : (
                    m.message
                  )}
                </MessageItemOnCloseFnProvider>
              </MessageItemCountdownProvider>
            </div>
          ))}
      </Stack>
    </>
  )
}

export const useMessage = (): MessageController => {
  const ctrl = useContext(MessageContext)
  if (!ctrl) {
    throw new Error("[useMessage] must be used in MessageProvider subtree")
  }
  return completeInternalMessageController(ctrl)
}

const completeInternalMessageController = (
  baseController: BaseMessageController,
): MessageController => {
  return {
    ...baseController,
    error(opts) {
      baseController.show(ctx => ({
        ...opts,
        message: (
          <MessageItem
            error={true}
            title={
              <MessageItemTitleText uppercase={false}>
                {opts.message}
              </MessageItemTitleText>
            }
            onClose={ctx.close}
          />
        ),
      }))
    },
  }
}

const getGuid = (() => {
  let id = 0
  return (): string => {
    return "" + id++
  }
})()
