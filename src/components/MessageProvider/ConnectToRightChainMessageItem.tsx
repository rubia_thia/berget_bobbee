import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { OutlineButtonVariant } from "../Button/OutlineButtonVariant"
import { Button } from "../ButtonFramework/Button"
import { useSpacing } from "../Themed/spacing"
import { MessageItem } from "./MessageItem"

export const ConnectToRightChainMessageItem: FC<{
  onSwitch: () => void
  onDismiss?: () => void
}> = props => {
  const { $t } = useIntl()

  const spacing = useSpacing()

  return (
    <MessageItem
      style={{ maxWidth: 380 }}
      error={true}
      title={$t(
        defineMessage({
          defaultMessage: "Your wallet is not connected to supported network.",
          description: "Connect to Right Chain Notification/title",
        }),
      )}
      content={
        <Button
          style={{ marginTop: spacing(1.5), alignSelf: "flex-start" }}
          Variant={p => (
            <OutlineButtonVariant
              {...p}
              tintColor={"white"}
              padding={{
                paddingHorizontal: spacing(4),
                paddingVertical: spacing(2),
              }}
            />
          )}
          onPress={props.onSwitch}
        >
          {$t(
            defineMessage({
              defaultMessage: "Switch to supported network",
              description: "Connect to Right Chain Notification/button text",
            }),
          )}
        </Button>
      }
      onClose={props.onDismiss}
    />
  )
}
