import { useState } from "react"
import { StyleProp, View, ViewStyle } from "react-native"
import { FCC } from "../utils/reactHelpers/types"

export interface AspectRatioProps {
  style?: StyleProp<ViewStyle>
  ratio: number
}

export const AspectRatio: FCC<AspectRatioProps> = props => {
  const [width, setWidth] = useState<null | number>(null)

  return (
    <View
      style={[props.style, width != null && { height: width / props.ratio }]}
      onLayout={e => {
        setWidth(e.nativeEvent.layout.width)
      }}
    >
      {props.children}
    </View>
  )
}
