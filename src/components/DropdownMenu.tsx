import { FC, ReactElement, ReactNode } from "react"
import { StyleProp, Text, TextProps, View, ViewStyle } from "react-native"
import { wrapText } from "../utils/reactHelpers/childrenHelpers"
import { FCC } from "../utils/reactHelpers/types"
import { PaddingStyle } from "../utils/styleHelpers/PaddingStyle"
import { selectByButtonState, useButtonState } from "./Button/useButtonState"
import { ButtonVariantProps } from "./ButtonFramework/_/ButtonVariant"
import { CardBoxView } from "./CardBox/CardBox"
import { Divider } from "./Divider"
import { Pressable } from "./Pressable"
import { useColors } from "./Themed/color"
import { useSpacing } from "./Themed/spacing"

const menuItemPaddingUnit = 2

export interface DropdownMenuProps {
  style?: StyleProp<ViewStyle>
}

export const DropdownMenu: FCC<DropdownMenuProps> = props => {
  const spacing = useSpacing()

  return (
    <CardBoxView
      style={[
        props.style,
        {
          minWidth: 180,
          padding: spacing(2),
          borderRadius: spacing(2),
        },
      ]}
    >
      {props.children}
    </CardBoxView>
  )
}

export interface DropdownMenuItemProps extends ButtonVariantProps {
  padding?: PaddingStyle
}

export const DropdownMenuItem: FCC<DropdownMenuItemProps> = props => {
  const spacing = useSpacing()
  const colors = useColors()

  const [state, eventListeners] = useButtonState({
    disabled: props.disabled,
  })

  const stateRelatedStyle: ViewStyle = selectByButtonState(state, {
    normal: {},
    hovering: {
      backgroundColor: colors("blue-100"),
    },
    pressing: {
      backgroundColor: colors("blue-200"),
    },
    disabled: {
      opacity: 0.6,
    },
  })

  const children = wrapText(props.children, {
    Text: DropdownMenuItemTextLayout,
    oneLine: true,
  })

  return (
    <Pressable
      {...eventListeners}
      {...ButtonVariantProps.pickCommonAriaProps(props)}
      style={[
        props.style,
        stateRelatedStyle,
        { borderRadius: spacing(1) },
        props.padding ?? { padding: spacing(2) },
      ]}
      disabled={props.disabled}
      onPress={props.onPress}
    >
      {children}
    </Pressable>
  )
}

export const DropdownMenuItemTextLayout: FCC<{
  style?: StyleProp<ViewStyle>
}> = props => {
  return (
    <DropdownMenuMainText style={[props.style]}>
      {props.children}
    </DropdownMenuMainText>
  )
}

export const DropdownMenuItemRichLayout: FCC<{
  style?: StyleProp<ViewStyle>
  icon?: ReactElement
  accessory?: ReactElement
}> = props => {
  const spacing = useSpacing()

  const children = wrapText(props.children, {
    Text: DropdownMenuMainText,
  })

  return (
    <View style={[props.style, { flexDirection: "row", alignItems: "center" }]}>
      <View style={{ minWidth: 18 }}>{props.icon}</View>

      <View style={{ marginLeft: spacing(2), flex: 1 }}>{children}</View>

      <View style={{ marginLeft: spacing(2), minWidth: 24 }}>
        {props.accessory}
      </View>
    </View>
  )
}

export interface DropdownMenuDividerProps {
  style?: StyleProp<ViewStyle>

  /**
   * @default spacing(1)
   */
  padding?: number

  /**
   * @default colors("gray-200")
   */
  color?: string
}

export const DropdownMenuDivider: FC<DropdownMenuDividerProps> = props => {
  const colors = useColors()
  const spacing = useSpacing()

  return (
    <Divider
      style={props.style}
      direction={"horizontal"}
      padding={props.padding ?? spacing(1)}
      color={colors("gray-200")}
    />
  )
}

export const DropdownMenuTitle: FC<{
  style?: StyleProp<ViewStyle>
  titleText?: ReactNode
}> = props => {
  const spacing = useSpacing()
  const colors = useColors()

  return (
    <View style={[props.style, { padding: spacing(menuItemPaddingUnit) }]}>
      <Text
        style={{
          fontSize: 14,
          lineHeight: 20,
          fontWeight: "500",
          color: colors("gray-400"),
        }}
      >
        {props.titleText}
      </Text>
    </View>
  )
}

export const DropdownMenuMainText: FC<TextProps> = props => {
  const colors = useColors()

  return (
    <Text
      {...props}
      style={[
        {
          color: colors("gray-900"),
          fontWeight: "500",
          fontSize: 16,
          lineHeight: 24,
        },
        props.style,
      ]}
    >
      {props.children}
    </Text>
  )
}
