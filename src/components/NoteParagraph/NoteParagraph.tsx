import { ComponentType, useMemo } from "react"
import {
  ColorValue,
  StyleProp,
  Text,
  TextStyle,
  View,
  ViewStyle,
} from "react-native"
import { NumberProp } from "react-native-svg"
import { wrapText } from "../../utils/reactHelpers/childrenHelpers"
import { FCC } from "../../utils/reactHelpers/types"
import { useColors } from "../Themed/color"
import { useSpacing } from "../Themed/spacing"
import InfoIcon from "./_/info.svg"

export { default as InfoIcon } from "./_/info.svg"
export { default as SuccessIcon } from "./_/success.svg"
export { default as WarnIcon } from "./_/warn.svg"

export const NoteParagraph: FCC<{
  style?: StyleProp<ViewStyle>

  Icon?: ComponentType<{
    fill?: ColorValue
    width?: NumberProp
    height?: NumberProp
  }>

  /**
   * @default spacing(2.5)
   */
  gap?: number

  textLineHeight?: number
  textSize?: number
  textColor?: string
}> = props => {
  const spacing = useSpacing()
  const colors = useColors()

  const defaultTextStyle = useDefaultTextStyle()
  const textColor = props.textColor ?? colors("gray-500")
  const textSize = props.textSize ?? defaultTextStyle.fontSize
  const textLineHeight = props.textLineHeight ?? defaultTextStyle.lineHeight

  const Icon = props.Icon ?? InfoIcon

  const children = wrapText(props.children, {
    Text: p => (
      <NoteParagraphText
        {...p}
        style={{
          ...defaultTextStyle,
          color: textColor,
          fontSize: textSize,
          lineHeight: textLineHeight,
        }}
      />
    ),
  })

  return (
    <View style={[props.style, { flexDirection: "row" }]}>
      <View
        style={{
          height: textLineHeight,
          alignItems: "center",
          marginRight: props.gap ?? spacing(2.5),
        }}
      >
        <Icon fill={textColor} width={textLineHeight} height={textLineHeight} />
      </View>

      <View style={{ flex: 1, alignSelf: "center" }}>{children}</View>
    </View>
  )
}

const useDefaultTextStyle = (): TextStyle => {
  const colors = useColors()

  return useMemo(
    () => ({
      color: colors("gray-500"),
      fontSize: 12,
      lineHeight: 16,
    }),
    [colors],
  )
}

export interface NoteParagraphTextProps {
  style?: StyleProp<TextStyle>
}

export const NoteParagraphText: FCC<NoteParagraphTextProps> = props => {
  const defaultTextStyle = useDefaultTextStyle()

  return <Text style={[defaultTextStyle, props.style]}>{props.children}</Text>
}
