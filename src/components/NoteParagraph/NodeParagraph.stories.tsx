import { ComponentMeta } from "@storybook/react"
import { BackgroundColor, CardContainer } from "../../../.storybook/decorators"
import { withTemplate } from "../../../.storybook/utils"
import { NoteParagraph } from "./NoteParagraph"

export default {
  title: "UI/NoteParagraph",
  component: NoteParagraph,
  decorators: [
    CardContainer({
      padding: 10,
    }),
    BackgroundColor(),
  ],
} as ComponentMeta<typeof NoteParagraph>

const template = withTemplate(NoteParagraph, {
  children: "Some text",
})

export const Normal = template()

export const WithLongText = template(props => {
  props.children =
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
})
