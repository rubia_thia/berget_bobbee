import {
  createContext,
  ForwardedRef,
  forwardRef,
  useContext,
  useMemo,
} from "react"
import {
  MouseEvent,
  Pressable as _Pressable,
  PressableProps as _PressableProps,
  View,
} from "react-native"
import { usePersistFn } from "../utils/reactHelpers/usePersistFn"

export interface PressableProps extends _PressableProps {
  shouldReceiveBubbledHoverEvent?: boolean
}

export const Pressable = forwardRef(
  (props: PressableProps, ref: ForwardedRef<View>) => {
    const hoverEvents = useContext(HoverEventContext)

    const {
      shouldReceiveBubbledHoverEvent,
      onHoverIn,
      onHoverOut,
      ...restProps
    } = props

    const outsideOnHoverIn = usePersistFn((event: MouseEvent): void => {
      if (shouldReceiveBubbledHoverEvent) {
        onHoverIn?.(event)
      }
    })
    const outsideOnHoverOut = usePersistFn((event: MouseEvent): void => {
      if (shouldReceiveBubbledHoverEvent) {
        onHoverOut?.(event)
      }
    })
    const insideOnHoverIn = usePersistFn((event: MouseEvent): void => {
      hoverEvents?.onHoverIn(event)
      onHoverIn?.(event)
    })
    const insideOnHoverOut = usePersistFn((event: MouseEvent): void => {
      hoverEvents?.onHoverOut(event)
      onHoverOut?.(event)
    })

    const ctxValue: HoverEventContextValue = useMemo(
      () => ({
        onHoverIn: outsideOnHoverIn,
        onHoverOut: outsideOnHoverOut,
      }),
      [outsideOnHoverIn, outsideOnHoverOut],
    )

    return (
      <HoverEventContext.Provider value={ctxValue}>
        <_Pressable
          {...restProps}
          ref={ref}
          onHoverIn={insideOnHoverIn}
          onHoverOut={insideOnHoverOut}
        />
      </HoverEventContext.Provider>
    )
  },
)

interface HoverEventContextValue {
  onHoverIn: NonNullable<PressableProps["onHoverIn"]>
  onHoverOut: NonNullable<PressableProps["onHoverOut"]>
}
const HoverEventContext = createContext<null | HoverEventContextValue>(null)
