import { Text, View } from "react-native"
import { styleGetters } from "../utils/reactHelpers/withProps/styleGetters"
import { withProps } from "../utils/reactHelpers/withProps/withProps"

export const TableHeaderRow = withProps(
  styleGetters(({ spacing, colors }) => ({
    flexDirection: "row",
    alignItems: "center",
    paddingBottom: spacing(2.5),
    borderColor: colors("gray-200"),
    borderBottomWidth: 1,
  })),
  View,
)

export const TableHeaderText = withProps(
  styleGetters(({ colors }) => ({
    color: colors("gray-500"),
    fontSize: 12,
  })),
  Text,
)

export const TableBodyRow = withProps(
  styleGetters(({ spacing, colors }) => ({
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: spacing(3),
    borderColor: colors("gray-200"),
    borderBottomWidth: 1,
  })),
  View,
)

export const TableBodyText = withProps(
  styleGetters(({ colors }) => ({
    color: colors("gray-900"),
    fontSize: 14,
  })),
  Text,
)
