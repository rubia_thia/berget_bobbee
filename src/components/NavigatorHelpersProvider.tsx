import type { NavigationProp } from "@react-navigation/native"
import { FCC } from "../utils/reactHelpers/types"
import { usePersistFn } from "../utils/reactHelpers/usePersistFn"
import { useNavBack } from "../utils/reactNavigationHelpers/useNavBack"
import { QuitFlowProvider } from "../utils/reactNavigationHelpers/useQuitFlow"

export const FlowNavigatorHelpersProvider: FCC<{
  navigatorLevelNavigation: NavigationProp<any>
}> = props => {
  const quitFlow = useNavBack(props.navigatorLevelNavigation)

  return (
    <QuitFlowProvider quitFlow={quitFlow}>{props.children}</QuitFlowProvider>
  )
}

export const TopLevelNavigatorHelpersProvider: FCC<{
  topLevelNavigation: NavigationProp<any>
}> = props => {
  const quitFlow = usePersistFn(() => {
    props.topLevelNavigation.reset({
      ...props.topLevelNavigation.getState(),
      index: 0,
    })
  })

  return (
    <QuitFlowProvider quitFlow={quitFlow}>{props.children}</QuitFlowProvider>
  )
}
