import { StyleProp, View, ViewStyle } from "react-native"
import { FCC } from "../utils/reactHelpers/types"
import {
  BorderRadiusStyle,
  useNormalizeBorderRadiusStyle,
} from "../utils/styleHelpers/BorderRadiusStyle"
import { OneOrMore } from "../utils/types"
import { LinearGradient, LinearGradientColor } from "./LinearGradient"

export const LinearGradientContainer: FCC<{
  style?: StyleProp<ViewStyle>

  borderRadius?: number | BorderRadiusStyle

  backgroundColor?: string

  /**
   * @default vertical
   */
  direction?: "vertical" | "horizontal" | number

  colors: OneOrMore<LinearGradientColor>
}> = props => {
  const borderRadius = useNormalizeBorderRadiusStyle(props.borderRadius)

  return (
    <View style={[props.style, { backgroundColor: props.backgroundColor }]}>
      <LinearGradient
        style={[
          {
            position: "absolute",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            width: "100%",
            height: "100%",
            overflow: "hidden",
          },
          borderRadius,
        ]}
        direction={props.direction}
        colors={props.colors}
      />

      {props.children}
    </View>
  )
}
