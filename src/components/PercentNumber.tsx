import { FC } from "react"
import { StyleProp, Text, TextStyle } from "react-native"
import { BigNumber } from "../utils/numberHelpers/BigNumber"
import { getPrecisionFloor } from "../utils/numberHelpers/getPrecisionFloor"
import { safeReadResource, SuspenseResource } from "../utils/SuspenseResource"

export interface PercentNumberProps extends PercentNumberFormatOptions {
  style?: StyleProp<TextStyle>
  number: SuspenseResource<number | BigNumber>
  padDecimals?: boolean
}

export const PercentNumber: FC<PercentNumberProps> = props => {
  const read = safeReadResource(props.number)
  const num = read != null ? BigNumber.safeFrom(read) : null

  return (
    <Text style={props.style}>
      {num == null
        ? "-"
        : formatPercentNumber(num, {
            precision: props.precision,
            padDecimals: props.padDecimals,
          })}
    </Text>
  )
}

export interface PercentNumberFormatOptions {
  /**
   * @default 2
   */
  precision?: number

  padDecimals?: boolean
}

export function formatPercentNumber(
  n: BigNumber,
  options: PercentNumberFormatOptions = {},
): string {
  const { precision = 2, padDecimals = false } = options

  const formatter = new Intl.NumberFormat("en-US", {
    style: "percent",
    maximumFractionDigits: precision,
    ...(padDecimals ? { minimumFractionDigits: precision } : {}),
  })

  if (BigNumber.isZero(n)) return formatter.format(0)

  const precisionFloor = getPrecisionFloor(precision + 2)
  if (BigNumber.isGt(precisionFloor, BigNumber.abs(n))) {
    const sign = BigNumber.isNegative(n) ? "-" : ""
    return `≤ ${sign}${formatter.format(BigNumber.toNumber(precisionFloor))}`
  }

  return formatter.format(BigNumber.toNumber(n))
}
