import { ComponentMeta } from "@storybook/react"
import { withTemplate } from "../../.storybook/utils"
import { BigNumber } from "../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../utils/TokenInfoPresets/TokenInfoPresets"
import { TextTokenCount } from "./TextTokenCount"

export default {
  title: "UI/TextTokenCount",
  component: TextTokenCount,
} as ComponentMeta<typeof TextTokenCount>

const template = withTemplate(TextTokenCount, {
  token: TokenInfoPresets.MockBTC,
  count: BigNumber.from(100),
})

export const Standard = template()
