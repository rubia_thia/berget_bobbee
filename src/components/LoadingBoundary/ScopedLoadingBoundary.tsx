import { FC, ReactNode, Suspense } from "react"
import { StyleProp, View, ViewStyle } from "react-native"
import { Spensor } from "../Spensor"
import { LoadingControllerFactoryProvider } from "./useLoading"
import { useLoadingControllerFactory } from "./useLoadingControllerFactory"

export interface OverlayFnContextInfo {
  isSuspense: boolean
}
export type OverlayFn = (contextInfo: OverlayFnContextInfo) => boolean

export const defaultOverlayFn: OverlayFn = ({ isSuspense }) => !isSuspense

export const ScopedLoadingBoundary: FC<{
  style?: StyleProp<ViewStyle>
  spensorTag?: string
  overlay?: boolean | OverlayFn
  isLoading?: boolean
  loadingIndicator: ReactNode
  placeholder?: ReactNode
  children: ReactNode | (() => ReactNode)
}> = props => {
  const [isSubtreeLoading, ctrlFactory] = useLoadingControllerFactory()

  const propsOverlay = props.overlay
  const overlay =
    propsOverlay == null
      ? defaultOverlayFn
      : typeof propsOverlay === "boolean"
      ? () => propsOverlay
      : propsOverlay

  const isLoading = isSubtreeLoading || props.isLoading

  const loadingJsx = (
    contextInfo: OverlayFnContextInfo,
    extraInfo: { placeholder?: ReactNode } = {},
  ): JSX.Element => (
    <>
      {extraInfo?.placeholder}
      <View
        style={{
          position: "absolute",
          top: 0,
          right: 0,
          bottom: 0,
          left: 0,
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: overlay(contextInfo)
            ? "rgba(0, 0, 0, .3)"
            : "transparent",
        }}
      >
        {props.loadingIndicator}
      </View>
    </>
  )

  return (
    <View style={[props.style, { flex: 1 }]}>
      <LoadingControllerFactoryProvider factory={ctrlFactory}>
        {typeof props.children === "function" ? (
          <Spensor
            spensorTag={props.spensorTag}
            fallback={loadingJsx(
              { isSuspense: true },
              { placeholder: props.placeholder },
            )}
          >
            {props.children}
          </Spensor>
        ) : (
          <Suspense
            fallback={loadingJsx(
              { isSuspense: true },
              { placeholder: props.placeholder },
            )}
          >
            {props.children}
          </Suspense>
        )}
      </LoadingControllerFactoryProvider>

      {isLoading && loadingJsx({ isSuspense: false })}
    </View>
  )
}
