import { ReactNode, Suspense } from "react"
import { View } from "react-native"
import { FCC } from "../../utils/reactHelpers/types"
import { LoadingControllerFactoryProvider } from "./useLoading"
import { useLoadingControllerFactory } from "./useLoadingControllerFactory"

export const GlobalLoadingBoundary: FCC<{
  loadingIndicator: ReactNode
}> = props => {
  const [isSubtreeLoading, ctrlFactory] = useLoadingControllerFactory()

  const loadingJsx = (
    <View
      style={{
        position: "absolute",
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        zIndex: 1,
        backgroundColor: "rgba(107, 114, 128, 0.5)",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      {props.loadingIndicator}
    </View>
  )

  return (
    <LoadingControllerFactoryProvider factory={ctrlFactory}>
      <Suspense fallback={loadingJsx}>{props.children}</Suspense>
      {isSubtreeLoading && loadingJsx}
    </LoadingControllerFactoryProvider>
  )
}
