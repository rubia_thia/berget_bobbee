import { ComponentMeta } from "@storybook/react"
import { FC } from "react"
import { ActivityIndicator } from "react-native"
import { withTemplate } from "../../../.storybook/utils"
import { Button } from "../ButtonFramework/Button"
import { GlobalLoadingBoundary } from "./GlobalLoadingBoundary"
import { useLoading } from "./useLoading"

export default {
  title: "UI/LoadingBoundary/GlobalLoadingBoundary",
  component: GlobalLoadingBoundary,
} as ComponentMeta<typeof GlobalLoadingBoundary>

const LoadingTrigger: FC = () => {
  const loading = useLoading()

  const onPress = (): void => {
    alert("The loading animation will dismiss after 3 seconds")
    loading.show()
    setTimeout(() => {
      loading.hide()
    }, 3000)
  }

  return <Button onPress={onPress}>Show Loading</Button>
}

const template = withTemplate(GlobalLoadingBoundary, {
  loadingIndicator: <ActivityIndicator />,
})

export const Normal = template(props => {
  props.children = <LoadingTrigger />
})
