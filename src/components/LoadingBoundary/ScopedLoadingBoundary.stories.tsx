import { ComponentMeta } from "@storybook/react"
import { FC } from "react"
import { ActivityIndicator, Button, Text, View } from "react-native"
import { withTemplate } from "../../../.storybook/utils"
import { ScopedLoadingBoundary } from "./ScopedLoadingBoundary"
import { useLoading } from "./useLoading"

export default {
  title: "UI/LoadingBoundary/ScopedLoadingBoundary",
  component: ScopedLoadingBoundary,
} as ComponentMeta<typeof ScopedLoadingBoundary>

const LoadingTrigger: FC = () => {
  const loading = useLoading()

  const onPress = (): void => {
    loading.show()
    setTimeout(() => {
      loading.hide()
    }, 3000)
  }

  return (
    <View>
      <Text>The loading animation will dismiss after 3 seconds</Text>
      <View>
        <Button title={"Show Loading"} onPress={onPress} />
      </View>
    </View>
  )
}

const template = withTemplate(ScopedLoadingBoundary, {
  style: {
    width: 300,
    height: 200,
    backgroundColor: "#eaeaea",
  },
  loadingIndicator: <ActivityIndicator />,
  children: null,
})

export const Normal = template(props => {
  props.children = <LoadingTrigger />
})
