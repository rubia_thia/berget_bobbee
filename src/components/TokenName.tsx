import { FC } from "react"
import { StyleProp, Text, TextStyle } from "react-native"
import { TokenInfo } from "../utils/TokenInfo"

export interface TokenNameProps {
  style?: StyleProp<TextStyle>
  token: TokenInfo
}

export const TokenName: FC<TokenNameProps> = props => {
  return <Text style={props.style}>{formatTokenName(props.token)}</Text>
}
export const formatTokenName = (token: TokenInfo): string => {
  return token.displayName
}
