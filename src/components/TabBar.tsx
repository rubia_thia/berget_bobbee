import { cloneElement, ReactNode, useRef, useState } from "react"
import {
  StyleProp,
  StyleSheet,
  Text,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import { first } from "../utils/arrayHelpers"
import { isElementOfType } from "../utils/reactHelpers/isElementOfType"
import { FCC, PropsWithChildren } from "../utils/reactHelpers/types"
import { readResource, SuspenseResource } from "../utils/SuspenseResource"
import { OneOrMore } from "../utils/types"
import { Spensor } from "./Spensor"
import { useColors } from "./Themed/color"
import { useSpacing } from "./Themed/spacing"

export interface TabItem<T> {
  tab: ReactNode | ((info: { isSelected: boolean }) => ReactNode)
  value: T
}

export interface TabBarProps<T>
  extends PropsWithChildren<
    unknown,
    ReactNode | ReactNode[] | ((info: { value: T }) => ReactNode | ReactNode[])
  > {
  style?: StyleProp<ViewStyle>

  tabs: OneOrMore<TabItem<T>>

  selectedTab?: {
    tabValue: T
    onChange?: (selectedTab: TabItem<T>) => void
  }

  rightSide?: ReactNode

  /**
   * @default true
   */
  separator?: boolean
}

export function TabBar<T>(props: TabBarProps<T>): JSX.Element {
  const colors = useColors()
  const spacing = useSpacing()

  const [_selectedTab, _setSelectedTab] = useState(
    () => first(props.tabs).value,
  )
  const _onSelectedTabChanged = (tab: TabItem<T>): void => {
    _setSelectedTab(tab.value)
  }

  const [selectedTab, setSelectedTab] =
    props.selectedTab != null
      ? [props.selectedTab.tabValue, props.selectedTab.onChange]
      : [_selectedTab, _onSelectedTabChanged]

  const tabbarListRef = useRef<View>(null)

  const { separator = true } = props

  return (
    <>
      <View
        style={[
          {
            flexDirection: "row",
            alignItems: "center",
            borderBottomWidth: separator ? StyleSheet.hairlineWidth : 0,
            borderColor: colors("gray-200"),
          },
          separator && { marginBottom: spacing(3.5) },
          props.style,
        ]}
      >
        <View ref={tabbarListRef} style={{ flexDirection: "row" }}>
          {props.tabs.map((t, idx) => (
            <TouchableOpacity
              key={idx}
              style={{ marginRight: spacing(2) }}
              onPress={() => setSelectedTab?.(t)}
            >
              {typeof t.tab === "function"
                ? t.tab({ isSelected: selectedTab === t.value })
                : isElementOfType(t.tab, TabbarItem)
                ? cloneElement(t.tab, { isSelected: selectedTab === t.value })
                : t.tab}
            </TouchableOpacity>
          ))}
        </View>

        {props.rightSide}
      </View>

      {typeof props.children === "function"
        ? props.children({ value: selectedTab })
        : props.children}
    </>
  )
}

export const TabbarItemVerticalPaddingUnit = 3
export const TabbarItem: FCC<{
  style?: StyleProp<ViewStyle>
  textStyle?: StyleProp<TextStyle>
  isSelected?: boolean
  withoutPadding?: boolean
  count?: SuspenseResource<number>
}> = props => {
  const colors = useColors()
  const spacing = useSpacing()

  const textColor = props.isSelected ? colors("blue-600") : colors("gray-900")

  return (
    <View
      style={[
        props.style,
        {
          flexDirection: "row",
          alignItems: "center",
        },
        !props.withoutPadding && {
          paddingVertical: spacing(TabbarItemVerticalPaddingUnit),
          paddingRight: spacing(6),
        },
      ]}
    >
      <Text style={[{ color: textColor, fontSize: 14 }, props.textStyle]}>
        {props.children}
      </Text>

      {props.count != null && (
        <Spensor>
          {() =>
            readResource(props.count) === 0 ? null : (
              <View
                style={{
                  marginLeft: spacing(1),
                  borderRadius: 9999,
                  overflow: "hidden",
                }}
              >
                <Text
                  style={{
                    paddingVertical: spacing(0.5),
                    paddingHorizontal: spacing(2),
                    fontSize: 12,
                    color: textColor,
                    backgroundColor: colors("white"),
                  }}
                >
                  {readResource(props.count)}
                </Text>
              </View>
            )
          }
        </Spensor>
      )}
    </View>
  )
}
