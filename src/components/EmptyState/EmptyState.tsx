import { FC, ReactNode } from "react"
import { useIntl } from "react-intl"
import { StyleProp, Text, View, ViewStyle } from "react-native"
import { noData$t } from "../../commonIntlMessages"
import { useColors } from "../Themed/color"
import { useSpacing } from "../Themed/spacing"
import EmptyIcon from "./_/EmptyStateIcon.svg"

export const EmptyState: FC<{
  style?: StyleProp<ViewStyle>
  descriptionText?: ReactNode
}> = props => {
  const { $t } = useIntl()

  const colors = useColors()
  const spacing = useSpacing()

  return (
    <View
      style={[
        props.style,
        {
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
        },
      ]}
    >
      <EmptyIcon />

      <Text
        style={{
          marginLeft: spacing(5),
          color: colors("gray-400"),
          fontSize: 14,
        }}
      >
        {props.descriptionText ?? $t(noData$t)}
      </Text>
    </View>
  )
}
