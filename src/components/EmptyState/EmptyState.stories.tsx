import { ComponentMeta } from "@storybook/react"
import { BackgroundColor, CardContainer } from "../../../.storybook/decorators"
import { withTemplate } from "../../../.storybook/utils"
import { EmptyState } from "./EmptyState"

export default {
  title: "UI/EmptyState",
  component: EmptyState,
  decorators: [CardContainer(), BackgroundColor()],
} as ComponentMeta<typeof EmptyState>

const template = withTemplate(EmptyState, {})

export const Normal = template()
