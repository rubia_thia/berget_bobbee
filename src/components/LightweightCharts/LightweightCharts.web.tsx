import {
  CandlestickData,
  createChart,
  IChartApi,
  ISeriesApi,
  LineData,
  MismatchDirection,
  MouseEventHandler,
  MouseEventParams,
  WhitespaceData,
} from "lightweight-charts"
import { FC, useEffect, useRef } from "react"
import { View } from "react-native"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import {
  neverUpdateComparer,
  useLatestValueRef,
} from "../../utils/reactHelpers/useLatestValueRef"
import { useLazyRef } from "../../utils/reactHelpers/useLazyRef"
import { usePersistFn } from "../../utils/reactHelpers/usePersistFn"
import {
  defaultColors,
  LightweightChartsProps,
  PriceChange,
} from "./LightweightCharts.types"
import { useAutoResizeChart } from "./_/useAutoResizeChart"
import { useAutoUpdateChartLocale } from "./_/useAutoUpdateChartLocale"
import { useGetCommonChartOptions } from "./_/useGetCommonChartOptions"
import { useSubscribePriceChanges } from "./_/useSubscribePriceChanges"
import { getChartCommonStyles } from "./_webviewHtml/crossCommon/getChartCommonStyles"
import {
  dateFromLightweightTime,
  getLocalizationOptions,
} from "./_webviewHtml/crossCommon/lightweightTimeHelpers"

const chatCommonStyles = getChartCommonStyles()

export const LightweightCharts: FC<LightweightChartsProps> = props => {
  const viewRef = useRef<View>(null)

  const colors = useLatestValueRef(
    props.initialColors ?? defaultColors,
    neverUpdateComparer,
  ).current

  const chartRef = useRef<null | IChartApi>(null)
  const priceSeriesRef = useRef<null | ISeriesApi<"Candlestick">>(null)
  const spreadRangeLinesRef = useRef<null | {
    highestAsk: ISeriesApi<"Line">
    lowestBid: ISeriesApi<"Line">
    currAsk: ISeriesApi<"Line">
    currBid: ISeriesApi<"Line">
  }>(null)

  const getCommonOptions = useGetCommonChartOptions()
  const initialToken = useLazyRef(() => props.token).current

  useEffect(() => {
    if (viewRef.current == null) return
    const instance: unknown = viewRef.current
    if (!(instance instanceof HTMLElement)) {
      throw new Error(
        "[LightweightCharts] View instance is no longer render as HTMLElement",
      )
    }

    const chart = createChart(instance, getCommonOptions(initialToken))

    chartRef.current = chart
    priceSeriesRef.current = chart.addCandlestickSeries({
      borderVisible: false,
      wickUpColor: colors.increase,
      wickDownColor: colors.decrease,
      upColor: colors.increase,
      downColor: colors.decrease,
    })

    if (props.isSpreadRangeVisible) {
      spreadRangeLinesRef.current = {
        highestAsk: chart.addLineSeries({
          ...chatCommonStyles.spreadRangeLines.highestAsk,
        }),
        lowestBid: chart.addLineSeries({
          ...chatCommonStyles.spreadRangeLines.lowestBid,
        }),
        currAsk: chart.addLineSeries({
          ...chatCommonStyles.spreadRangeLines.currentAsk,
        }),
        currBid: chart.addLineSeries({
          ...chatCommonStyles.spreadRangeLines.currentBid,
        }),
      }
    }

    return () => {
      chart.remove()
      chartRef.current = null
      priceSeriesRef.current = null
      spreadRangeLinesRef.current = null
    }
  }, [
    colors.decrease,
    colors.increase,
    getCommonOptions,
    initialToken,
    props.isSpreadRangeVisible,
  ])

  const onContainerSizeChange = useAutoResizeChart(
    (width, height, forceRepaint) => {
      chartRef.current?.resize(width, height, forceRepaint)
    },
  )

  useSubscribePriceChanges(
    undefined,
    props.priceChanges,
    usePersistFn((data, prev) => {
      priceSeriesRef.current?.update({
        time: data.time,
        open: data.start,
        close: data.end,
        high: data.high,
        low: data.low,
      })
      spreadRangeLinesRef.current?.highestAsk.update({
        time: data.time,
        value: data.highestAsk,
      })
      spreadRangeLinesRef.current?.lowestBid.update({
        time: data.time,
        value: data.lowestBid,
      })

      // reset the prev point data, we only render the latest price data
      if (prev != null) {
        spreadRangeLinesRef.current?.currAsk.update({ time: prev.time })
      }
      spreadRangeLinesRef.current?.currAsk.update({
        time: data.time,
        value: data.ask,
      })

      if (prev != null) {
        spreadRangeLinesRef.current?.currBid.update({ time: prev.time })
      }
      spreadRangeLinesRef.current?.currBid.update({
        time: data.time,
        value: data.bid,
      })
    }),
  )

  useAutoUpdateChartLocale(
    undefined,
    usePersistFn((locale: string): void => {
      chartRef.current?.applyOptions({
        localization: getLocalizationOptions(locale, props.token),
      })
    }),
  )

  useEffect(() => {
    const priceSeries = priceSeriesRef.current
    const spreadRangeLines = spreadRangeLinesRef.current

    if (priceSeries == null || spreadRangeLines == null) return

    const handler: MouseEventHandler = (params): void => {
      if (props.onHoveringPriceChange == null) return
      tryEmitPriceChangeEvent(
        priceSeries,
        spreadRangeLines,
        params,
        props.onHoveringPriceChange,
      )
    }
    chartRef.current?.subscribeCrosshairMove(handler)
    return () => {
      chartRef.current?.unsubscribeCrosshairMove(handler)
    }
  }, [props.onHoveringPriceChange])

  return (
    <View
      ref={viewRef}
      style={props.style}
      onLayout={e => {
        onContainerSizeChange({
          width: e.nativeEvent.layout.width,
          height: e.nativeEvent.layout.height,
        })
      }}
    />
  )
}

function tryEmitPriceChangeEvent(
  priceSeries: ISeriesApi<"Candlestick">,
  spreadRangeLines: {
    highestAsk: ISeriesApi<"Line">
    lowestBid: ISeriesApi<"Line">
    currAsk: ISeriesApi<"Line">
    currBid: ISeriesApi<"Line">
  },
  params: MouseEventParams,
  onHoveringPriceChange: NonNullable<
    LightweightChartsProps["onHoveringPriceChange"]
  >,
): void {
  const priceData = params.seriesData.get(priceSeries) as
    | undefined
    | CandlestickData
  const spreadRangeLinesData = {
    highestAsk: params.seriesData.get(spreadRangeLines.highestAsk) as
      | undefined
      | LineData,
    lowestBid: params.seriesData.get(spreadRangeLines.lowestBid) as
      | undefined
      | LineData,
    currAsk: params.seriesData.get(spreadRangeLines.currAsk) as
      | undefined
      | LineData,
    currBid: params.seriesData.get(spreadRangeLines.currBid) as
      | undefined
      | LineData,
  }

  if (
    params.logical == null ||
    params.time == null ||
    priceData == null ||
    spreadRangeLinesData.highestAsk == null ||
    spreadRangeLinesData.lowestBid == null ||
    spreadRangeLinesData.currAsk == null ||
    spreadRangeLinesData.currBid == null
  ) {
    onHoveringPriceChange(null)
    return
  }

  const priceChange = constructPriceChange(
    priceData,
    spreadRangeLinesData.highestAsk,
    spreadRangeLinesData.lowestBid,
    spreadRangeLinesData.currAsk,
    spreadRangeLinesData.currBid,
  )

  const prevPointCandleData = priceSeries.dataByIndex(
    params.logical - 1,
    MismatchDirection.NearestLeft,
  ) as undefined | CandlestickData
  const prevPointHighestAskData = spreadRangeLines.highestAsk.dataByIndex(
    params.logical - 1,
    MismatchDirection.NearestLeft,
  ) as undefined | LineData
  const prevPointLowestBidData = spreadRangeLines.lowestBid.dataByIndex(
    params.logical - 1,
    MismatchDirection.NearestLeft,
  ) as undefined | LineData
  const prevPointAskData = spreadRangeLines.currAsk.dataByIndex(
    params.logical - 1,
    MismatchDirection.NearestLeft,
  ) as undefined | LineData | WhitespaceData
  const prevPointBidData = spreadRangeLines.currBid.dataByIndex(
    params.logical - 1,
    MismatchDirection.NearestLeft,
  ) as undefined | LineData | WhitespaceData
  const prevPointPriceChange =
    prevPointCandleData == null ||
    prevPointHighestAskData == null ||
    prevPointLowestBidData == null ||
    prevPointAskData == null ||
    prevPointBidData == null
      ? undefined
      : constructPriceChange(
          prevPointCandleData,
          prevPointHighestAskData,
          prevPointLowestBidData,
          prevPointAskData,
          prevPointBidData,
        )

  const nextPointCandleData = priceSeries.dataByIndex(
    params.logical + 1,
    MismatchDirection.NearestRight,
  ) as undefined | CandlestickData
  const nextPointHighestAskData = spreadRangeLines.highestAsk.dataByIndex(
    params.logical + 1,
    MismatchDirection.NearestRight,
  ) as undefined | LineData
  const nextPointLowestBidData = spreadRangeLines.lowestBid.dataByIndex(
    params.logical + 1,
    MismatchDirection.NearestRight,
  ) as undefined | LineData
  const nextPointAskData = spreadRangeLines.currAsk.dataByIndex(
    params.logical + 1,
    MismatchDirection.NearestRight,
  ) as undefined | LineData | WhitespaceData
  const nextPointBidData = spreadRangeLines.currBid.dataByIndex(
    params.logical + 1,
    MismatchDirection.NearestRight,
  ) as undefined | LineData | WhitespaceData
  const nextPointPriceChange =
    nextPointCandleData == null ||
    nextPointHighestAskData == null ||
    nextPointLowestBidData == null ||
    nextPointAskData == null ||
    nextPointBidData == null
      ? undefined
      : constructPriceChange(
          nextPointCandleData,
          nextPointHighestAskData,
          nextPointLowestBidData,
          nextPointAskData,
          nextPointBidData,
        )

  onHoveringPriceChange({
    prevPoint: prevPointPriceChange,
    nextPoint: nextPointPriceChange,
    currPoint: priceChange,
  })
}

function constructPriceChange(
  priceData: CandlestickData,
  highestAskData: LineData,
  lowestBidData: LineData,
  askData: LineData | WhitespaceData,
  bidData: LineData | WhitespaceData,
): PriceChange {
  return {
    time: dateFromLightweightTime(priceData.time),
    start: BigNumber.from(priceData.open),
    end: BigNumber.from(priceData.close),
    high: BigNumber.from(priceData.high),
    low: BigNumber.from(priceData.low),
    highestAsk: BigNumber.from(highestAskData.value),
    lowestBid: BigNumber.from(lowestBidData.value),
    ask:
      "value" in askData && askData.value != null
        ? BigNumber.from(askData.value)
        : undefined,
    bid:
      "value" in bidData && bidData.value != null
        ? BigNumber.from(bidData.value)
        : undefined,
  }
}
