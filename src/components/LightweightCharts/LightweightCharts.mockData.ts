import { identity, range } from "ramda"
import { interval, scan, startWith } from "rxjs"
import { last } from "../../utils/arrayHelpers"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { PriceChange, VolumeChange } from "./LightweightCharts.types"

export const priceChanges = interval(1000).pipe(
  startWith(-1),
  scan((prev): PriceChange[] => {
    if (prev.length === 0) {
      let prevItem = generatePriceChange()
      return range(0, 50)
        .map(idx => 50 - idx)
        .map(idx => {
          prevItem = generatePriceChange({
            time: new Date(Date.now() - idx * 1000),
            start: prevItem.end,
          })
          return prevItem
        })
    }
    return [generatePriceChange({ start: last(prev)?.end })]
  }, [] as PriceChange[]),
)

export const volumeChanges = interval(1000).pipe(
  startWith(-1),
  scan((prev): VolumeChange[] => {
    if (prev.length === 0) {
      return range(0, 50)
        .map(idx => 50 - idx)
        .map(idx =>
          generateVolumeChange({
            time: new Date(Date.now() - idx * 1000),
          }),
        )
    }
    return [generateVolumeChange()]
  }, [] as VolumeChange[]),
)

export function generateVolumeChange(
  prelude: Partial<VolumeChange> = {},
): VolumeChange {
  const time = prelude.time ?? new Date()
  const volume = (Math.random() < 0.5 ? BigNumber.neg : identity)(
    prelude.volume ?? BigNumber.from(Math.random() * 10000),
  )
  return { time, volume }
}

export function generatePriceChange(
  prelude: Partial<PriceChange> = {},
): PriceChange {
  const time = prelude.time ?? new Date()
  const start = prelude.start ?? BigNumber.from(Math.random() * 100 + 1200)
  const end = prelude.end ?? BigNumber.from(Math.random() * 100 + 1200)
  const high =
    prelude.high ??
    BigNumber.from(
      BigNumber.max([start, end, BigNumber.from(Math.random() * 100 + 1200)]),
    )
  const low =
    prelude.low ??
    BigNumber.from(
      BigNumber.min([start, end, BigNumber.from(Math.random() * 100 + 1200)]),
    )
  return {
    time,
    start,
    end,
    high,
    low,
    highestAsk: high,
    lowestBid: low,
    ask: high,
    bid: low,
  }
}
