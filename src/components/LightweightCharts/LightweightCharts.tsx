import { toPairs } from "ramda"
import { FC, useRef } from "react"
import { Platform } from "react-native"
import { WebView } from "react-native-webview"
import { defer } from "../../utils/promiseHelpers"
import { useLazyRef } from "../../utils/reactHelpers/useLazyRef"
import { usePersistFn } from "../../utils/reactHelpers/usePersistFn"
import { LightweightChartsProps } from "./LightweightCharts.types"
import { useAutoUpdateChartLocale } from "./_/useAutoUpdateChartLocale"
import { useGetCommonChartOptions } from "./_/useGetCommonChartOptions"
import { useSubscribePriceChanges } from "./_/useSubscribePriceChanges"
import { parseMessage } from "./_webviewHtml/crossCommon/messages"
import {
  callSeriesUpdateData,
  currentTokenInfo,
  initColors,
  initOptions,
  updatedLocale,
} from "./_webviewHtml/crossCommon/replacement"
import updateLocaleCode from "./_webviewHtml/updateLocaleCode.ts?inlineScript"
import updatePriceSeriesCode from "./_webviewHtml/updatePriceSeriesCode.ts?inlineScript"
import webviewHtml from "./_webviewHtml/webview.html?inlineHtml"

export const LightweightCharts: FC<LightweightChartsProps> = props => {
  const webviewRef = useRef<WebView>(null)

  const { current: webviewReadyDefer } = useLazyRef(() => defer())

  const getCommonOptions = useGetCommonChartOptions()
  const { current: initHtml } = useLazyRef(() =>
    replaceCodeReplacement(webviewHtml, {
      [initOptions]: getCommonOptions(props.token),
      [initColors]: props.initialColors ? props.initialColors : undefined,
    }),
  )

  useSubscribePriceChanges(
    webviewReadyDefer,
    props.priceChanges,
    usePersistFn(data => {
      webviewRef.current?.injectJavaScript(
        generateInjectableScript(
          replaceCodeReplacement(updatePriceSeriesCode, {
            [callSeriesUpdateData]: data,
          }),
        ),
      )
    }),
  )

  useAutoUpdateChartLocale(
    webviewReadyDefer,
    usePersistFn((locale: string): void => {
      webviewRef.current?.injectJavaScript(
        generateInjectableScript(
          replaceCodeReplacement(updateLocaleCode, {
            [updatedLocale]: locale,
            [currentTokenInfo]: props.token,
          }),
        ),
      )
    }),
  )

  return (
    <WebView
      ref={webviewRef}
      style={props.style}
      allowFileAccessFromFileURLs={true}
      originWhitelist={["*"]}
      source={{ html: initHtml }}
      {...Platform.select({
        android: {
          domStorageEnabled: true,
          allowFileAccess: true,
          allowUniversalAccessFromFileURLs: true,
          onShouldStartLoadWithRequest: () => false,
        },
      })}
      onMessage={event => {
        const message = parseMessage(event)
        if (message == null) return
        console.log("received data from webview", message)
        if (message.type === "ready") {
          webviewReadyDefer.resolve()
        }
      }}
    />
  )
}

function replaceCodeReplacement(
  code: string,
  replacements: Record<string, any>,
): string {
  return toPairs(replacements).reduce(
    (code, [placeholder, value]) =>
      code.replace(`"${placeholder}"`, JSON.stringify(value)),
    code,
  )
}

function generateInjectableScript(script: string): string {
  return `
;(function() { ${script} })();

// note: this is required, or you'll sometimes get silent failures 
// (https://github.com/react-native-webview/react-native-webview/blob/master/docs/Guide.md#the-injectedjavascript-prop)
true
  `
}
