import { ChartOptions, CrosshairMode } from "lightweight-charts"
import { useIntl } from "react-intl"
import { usePersistFn } from "../../../utils/reactHelpers/usePersistFn"
import { TokenInfo } from "../../../utils/TokenInfo"
import { DeepPartial } from "../../../utils/types"
import { getLocalizationOptions } from "../_webviewHtml/crossCommon/lightweightTimeHelpers"

const axisColor = "rgba(0,0,0,0)"
const gridLineColor = "#f3f3f3"

export const useGetCommonChartOptions = (): ((
  token: TokenInfo,
) => DeepPartial<ChartOptions>) => {
  const { locale } = useIntl()

  return usePersistFn(token => ({
    leftPriceScale: {
      borderColor: axisColor,
    },
    rightPriceScale: {
      borderColor: axisColor,
    },
    timeScale: {
      borderColor: axisColor,
      timeVisible: true,
    },
    crosshair: {
      mode: CrosshairMode.Normal,
    },
    overlayPriceScales: {
      borderColor: axisColor,
    },
    grid: {
      horzLines: {
        color: gridLineColor,
      },
      vertLines: {
        color: gridLineColor,
      },
    },
    localization: getLocalizationOptions(locale, token),
  }))
}
