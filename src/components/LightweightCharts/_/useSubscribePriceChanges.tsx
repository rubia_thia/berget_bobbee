import { sortBy } from "ramda"
import { useEffect, useRef } from "react"
import { from, Observable, switchMap } from "rxjs"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { defer } from "../../../utils/promiseHelpers"
import {
  PriceChange,
  PriceChangeWithNativeNumber,
} from "../LightweightCharts.types"
import { dateToLightweightTime } from "../_webviewHtml/crossCommon/lightweightTimeHelpers"

export const useSubscribePriceChanges = (
  waitingDefer: undefined | defer.Deferred<void>,
  priceChanges: Observable<PriceChange[]>,
  updatePriceSeries: (
    curr: PriceChangeWithNativeNumber,
    prev: undefined | PriceChangeWithNativeNumber,
  ) => void,
): void => {
  const lastTimePriceChangeRef = useRef<
    undefined | PriceChangeWithNativeNumber
  >()

  useEffect(() => {
    const sub = from(waitingDefer?.promise ?? Promise.resolve())
      .pipe(switchMap(() => priceChanges))
      .subscribe(changes => {
        sortBy(c => c.time.getTime(), changes).forEach(change => {
          const curr: PriceChangeWithNativeNumber = {
            time: dateToLightweightTime(change.time),
            start: BigNumber.toNumber(change.start),
            end: BigNumber.toNumber(change.end),
            high: BigNumber.toNumber(change.high),
            low: BigNumber.toNumber(change.low),
            highestAsk: BigNumber.toNumber(change.highestAsk),
            lowestBid: BigNumber.toNumber(change.lowestBid),
            ask:
              change.ask == null ? undefined : BigNumber.toNumber(change.ask),
            bid:
              change.bid == null ? undefined : BigNumber.toNumber(change.bid),
          }

          updatePriceSeries(curr, lastTimePriceChangeRef.current)
          lastTimePriceChangeRef.current = curr
        })
      })
    return () => sub.unsubscribe()
  }, [priceChanges, updatePriceSeries, waitingDefer?.promise])
}
