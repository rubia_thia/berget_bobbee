import { useEffect } from "react"
import { useIntl } from "react-intl"
import { from } from "rxjs"
import { defer } from "../../../utils/promiseHelpers"

export const useAutoUpdateChartLocale = (
  waitingDefer: undefined | defer.Deferred<void>,
  updateLocale: (locale: string) => void,
): void => {
  const { locale } = useIntl()

  useEffect(() => {
    const sub = from(waitingDefer?.promise ?? Promise.resolve()).subscribe(
      () => {
        updateLocale(locale)
      },
    )
    return () => sub.unsubscribe()
  }, [locale, updateLocale, waitingDefer?.promise])
}
