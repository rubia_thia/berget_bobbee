import { useEffect, useState } from "react"

export const useAutoResizeChart = (
  updateChartSize: (
    width: number,
    height: number,
    forceRepaint?: boolean,
  ) => void,
): ((size: { width: number; height: number }) => void) => {
  const [containerSize, setContainerSize] = useState({ width: 0, height: 0 })

  useEffect(() => {
    updateChartSize(containerSize.width, containerSize.height)
  }, [updateChartSize, containerSize])

  return setContainerSize
}
