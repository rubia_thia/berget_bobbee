import { HistogramData, UTCTimestamp } from "lightweight-charts"
import { useEffect } from "react"
import { Observable } from "rxjs"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { VolumeChange } from "../LightweightCharts.types"

export const useSubscribeVolumeChanges = (
  volumeChanges: Observable<VolumeChange[]>,
  updateVolumeSeries: (data: HistogramData) => void,
  colors: { buy: string; sell: string },
): void => {
  useEffect(() => {
    const sub = volumeChanges.subscribe(changes => {
      changes.forEach(change => {
        updateVolumeSeries({
          time: Math.round(change.time.getTime() / 1000) as UTCTimestamp,
          value: BigNumber.toNumber(BigNumber.abs(change.volume)),
          color: BigNumber.isNegative(change.volume) ? colors.sell : colors.buy,
        })
      })
    })
    return () => sub.unsubscribe()
  }, [colors.buy, colors.sell, updateVolumeSeries, volumeChanges])
}
