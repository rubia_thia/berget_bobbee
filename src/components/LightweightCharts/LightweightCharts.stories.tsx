import { ComponentMeta } from "@storybook/react"
import { BackgroundColor, CardContainer } from "../../../.storybook/decorators"
import { withTemplate } from "../../../.storybook/utils"
import { TokenInfoPresets } from "../../utils/TokenInfoPresets/TokenInfoPresets"
import { LightweightCharts } from "./LightweightCharts"
import { priceChanges } from "./LightweightCharts.mockData"

export default {
  title: "UI/LightweightCharts",
  component: LightweightCharts,
  decorators: [
    CardContainer({ margin: 10, padding: 10, height: 300 }),
    BackgroundColor(),
  ],
} as ComponentMeta<typeof LightweightCharts>

const tpl = withTemplate(LightweightCharts, {
  style: { flex: 1 },
  token: TokenInfoPresets.MockBTC,
  priceChanges,
})

export const Normal = tpl()
