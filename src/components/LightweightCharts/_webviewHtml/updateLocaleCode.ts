import { TokenInfo } from "../../../utils/TokenInfo"
import { getLocalizationOptions } from "./crossCommon/lightweightTimeHelpers"
import { currentTokenInfo, updatedLocale } from "./crossCommon/replacement"
import { getChart } from "./inPageCommon/chartAccessor"
import { receiveInjectedData } from "./inPageCommon/receiveInjectedData"

const locale = receiveInjectedData<string>(updatedLocale)
const token = receiveInjectedData<TokenInfo>(currentTokenInfo)

if (locale && token) {
  getChart().applyOptions({
    localization: getLocalizationOptions(locale, token),
  })
}
