import { isStillPlaceholderStr } from "../crossCommon/replacement"

export function receiveInjectedData<T>(replacementStr: string): undefined | T
export function receiveInjectedData<T>(
  replacementStr: string,
  defaultValue: T,
): T
export function receiveInjectedData<T>(
  replacementStr: string,
  defaultValue?: T,
): undefined | T {
  if (isStillPlaceholderStr(replacementStr)) {
    return defaultValue
  } else {
    return replacementStr as any
  }
}
