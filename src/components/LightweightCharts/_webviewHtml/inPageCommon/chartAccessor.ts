import { IChartApi, ISeriesApi } from "lightweight-charts"

export const getChart = (): IChartApi => {
  return (window as any).uniwhaleChart
}
export const setChart = (chart: IChartApi): void => {
  ;(window as any).uniwhaleChart = chart
}

export const getPriceSeries = (): ISeriesApi<"Candlestick"> => {
  return (window as any).priceSeries
}
export const setPriceSeries = (series: ISeriesApi<"Candlestick">): void => {
  ;(window as any).priceSeries = series
}

export const getSpreadRangeLines = (): {
  ask: ISeriesApi<"Line">
  bid: ISeriesApi<"Line">
} => {
  return (window as any).spreadRangeLines
}
export const setSpreadRangeLines = (lines: {
  ask: ISeriesApi<"Line">
  bid: ISeriesApi<"Line">
}): void => {
  ;(window as any).spreadRangeLines = lines
}
