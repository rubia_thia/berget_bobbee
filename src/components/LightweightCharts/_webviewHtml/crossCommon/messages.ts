import { WebViewMessageEvent } from "react-native-webview"

export type MessageData = OnWebViewReady

export interface OnWebViewReady {
  type: "ready"
}

export const postMessage = (message: MessageData): void => {
  ;(window as any).ReactNativeWebView.postMessage(JSON.stringify(message))
}

export const parseMessage = (
  event: WebViewMessageEvent,
): undefined | MessageData => {
  try {
    return JSON.parse(event.nativeEvent.data)
  } catch {
    return undefined
  }
}
