import { LineSeriesPartialOptions, LineStyle } from "lightweight-charts"

export const getChartCommonStyles = (): {
  spreadRangeLines: {
    highestAsk: LineSeriesPartialOptions
    lowestBid: LineSeriesPartialOptions
    currentAsk: LineSeriesPartialOptions
    currentBid: LineSeriesPartialOptions
  }
} => {
  return {
    spreadRangeLines: {
      highestAsk: {
        lineWidth: 1,
        lineStyle: LineStyle.Solid,
        color: "#86EFAC",
        lastValueVisible: false,
        priceLineVisible: false,
        crosshairMarkerVisible: false,
      },
      lowestBid: {
        lineWidth: 1,
        lineStyle: LineStyle.Solid,
        color: "#FECACA",
        lastValueVisible: false,
        priceLineVisible: false,
        crosshairMarkerVisible: false,
      },
      currentAsk: {
        color: "transparent",
        priceLineVisible: true,
        baseLineVisible: false,
        crosshairMarkerVisible: false,
        lastValueVisible: true,
        priceLineColor: "#86EFAC",
      },
      currentBid: {
        color: "transparent",
        priceLineVisible: true,
        baseLineVisible: false,
        crosshairMarkerVisible: false,
        lastValueVisible: true,
        priceLineColor: "#FECACA",
      },
    },
  }
}
