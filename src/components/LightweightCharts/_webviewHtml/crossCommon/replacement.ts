export const initOptions = "{{initOptions}}"

export const initColors = "{{initColors}}"

export const callSeriesUpdateData = "{{callSeriesUpdateData}}"

export const updatedLocale = "{{updatedLocale}}"

export const currentTokenInfo = "{{currentTokenInfo}}"

export const isStillPlaceholderStr = (input: string): boolean => {
  if (typeof input !== "string") {
    return false
  }

  return input.startsWith("{{") && input.endsWith("}}")
}
