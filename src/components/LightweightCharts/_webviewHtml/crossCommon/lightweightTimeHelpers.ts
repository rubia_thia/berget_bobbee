import {
  BarPrice,
  BusinessDay,
  LocalizationOptions,
  UTCTimestamp,
} from "lightweight-charts"
import { TokenInfo } from "../../../../utils/TokenInfo"

const timeAxisLabelFormat = "yyyy-MM-dd"

const timeAxisFormatterOptions: Intl.DateTimeFormatOptions = {
  year: "numeric",
  month: "numeric",
  day: "numeric",
  hour: "numeric",
  minute: "numeric",
  second: "numeric",
  hour12: false,
}
const timeFormatter = (locale: string, time: LightweightTime): string => {
  const formatter = new Intl.DateTimeFormat(locale, timeAxisFormatterOptions)
  return formatter.format(dateFromLightweightTime(time))
}

export const getLocalizationOptions = (
  locale: string,
  currentToken: TokenInfo,
): LocalizationOptions => ({
  locale,
  dateFormat: timeAxisLabelFormat,
  priceFormatter: (value: BarPrice): string => {
    const precision = TokenInfo.getPrecision(currentToken)
    const formatter = new Intl.NumberFormat("en-US", {
      maximumFractionDigits: precision + 10,
      minimumFractionDigits: precision + 10,
    })
    return formatter
      .formatToParts(value)
      .map(p => (p.type === "fraction" ? p.value.slice(0, precision) : p.value))
      .join("")
  },
  timeFormatter: time => timeFormatter(locale, time),
})

export type LightweightTime = BusinessDay | UTCTimestamp | string

/**
 * https://tradingview.github.io/lightweight-charts/docs/time-zones#date-solution
 */
export const dateFromLightweightTime = (time: LightweightTime): Date => {
  if (typeof time === "string") {
    return new Date(time)
  }

  if (typeof time !== "number") {
    return new Date(time.year, time.month, time.day)
  }

  const offsettedDate = new Date(time * 1000)

  return new Date(
    offsettedDate.getUTCFullYear(),
    offsettedDate.getUTCMonth(),
    offsettedDate.getUTCDate(),
    offsettedDate.getUTCHours(),
    offsettedDate.getUTCMinutes(),
    offsettedDate.getUTCSeconds(),
    offsettedDate.getUTCMilliseconds(),
  )
}

/**
 * https://tradingview.github.io/lightweight-charts/docs/time-zones#date-solution
 */
export function dateToLightweightTime(d: Date): UTCTimestamp {
  return (Date.UTC(
    d.getFullYear(),
    d.getMonth(),
    d.getDate(),
    d.getHours(),
    d.getMinutes(),
    d.getSeconds(),
    d.getMilliseconds(),
  ) / 1000) as UTCTimestamp
}
