import { PriceChangeWithNativeNumber } from "../LightweightCharts.types"
import { callSeriesUpdateData } from "./crossCommon/replacement"
import {
  getPriceSeries,
  getSpreadRangeLines,
} from "./inPageCommon/chartAccessor"
import { receiveInjectedData } from "./inPageCommon/receiveInjectedData"

const data =
  receiveInjectedData<PriceChangeWithNativeNumber>(callSeriesUpdateData)

if (data) {
  getPriceSeries().update({
    time: data.time,
    open: data.start,
    close: data.end,
    high: data.high,
    low: data.low,
  })
  getSpreadRangeLines().ask.update({
    time: data.time,
    value: data.ask,
  })
  getSpreadRangeLines().bid.update({
    time: data.time,
    value: data.bid,
  })
}
