import { ChartOptions, createChart, DeepPartial } from "lightweight-charts"
import { defaultColors } from "../LightweightCharts.types"
import { getChartCommonStyles } from "./crossCommon/getChartCommonStyles"
import { postMessage } from "./crossCommon/messages"
import { initColors, initOptions } from "./crossCommon/replacement"
import {
  setChart,
  setPriceSeries,
  setSpreadRangeLines,
} from "./inPageCommon/chartAccessor"
import { receiveInjectedData } from "./inPageCommon/receiveInjectedData"

const chatCommonStyles = getChartCommonStyles()

const options = receiveInjectedData<DeepPartial<ChartOptions>>(initOptions, {})
const colors = {
  ...defaultColors,
  ...receiveInjectedData(initColors, {}),
}

const uniwhaleChart = createChart(document.getElementById("chart")!, {
  width: window.innerWidth,
  height: window.innerHeight,
  ...options,
})
setChart(uniwhaleChart)

const priceSeries = uniwhaleChart.addCandlestickSeries({
  borderUpColor: colors.increase,
  borderDownColor: colors.decrease,
})
setPriceSeries(priceSeries)

const spreadRangeLines = {
  ask: uniwhaleChart.addLineSeries({
    ...chatCommonStyles.spreadRangeLines.highestAsk,
  }),
  bid: uniwhaleChart.addLineSeries({
    ...chatCommonStyles.spreadRangeLines.lowestBid,
  }),
}
setSpreadRangeLines(spreadRangeLines)

window.addEventListener("resize", () => {
  uniwhaleChart.resize(window.innerWidth, window.innerHeight)
})

postMessage({ type: "ready" })
