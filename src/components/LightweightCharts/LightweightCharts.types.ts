import { UTCTimestamp } from "lightweight-charts"
import { StyleProp, ViewStyle } from "react-native"
import { Observable } from "rxjs"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { TokenInfo } from "../../utils/TokenInfo"

export interface PriceChange {
  time: Date
  start: BigNumber
  end: BigNumber
  high: BigNumber
  low: BigNumber
  highestAsk: BigNumber
  lowestBid: BigNumber
  ask?: BigNumber
  bid?: BigNumber
}
export interface PriceChangeWithNativeNumber {
  time: UTCTimestamp
  start: number
  end: number
  high: number
  low: number
  highestAsk: number
  lowestBid: number
  ask?: number
  bid?: number
}

export interface VolumeChange {
  time: Date
  volume: BigNumber
}

export interface LightweightChartsProps {
  style?: StyleProp<ViewStyle>

  isSpreadRangeVisible?: boolean

  token: TokenInfo

  initialColors?: {
    increase: string
    decrease: string
    buy: string
    sell: string
  }

  priceChanges: Observable<PriceChange[]>

  onHoveringPriceChange?: OnHoveringPriceChangeCallback
}

export type OnHoveringPriceChangeCallback = (
  hoveringData: null | OnHoveringPriceChangeHoveringData,
) => void

export interface OnHoveringPriceChangeHoveringData {
  prevPoint: undefined | PriceChange
  nextPoint: undefined | PriceChange
  currPoint: PriceChange
}

export const defaultColors: NonNullable<
  LightweightChartsProps["initialColors"]
> = {
  increase: "#16A34A",
  decrease: "#EF4444",
  buy: "#16A34A",
  sell: "#EF4444",
}
