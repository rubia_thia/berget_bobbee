import { hideAsync, preventAutoHideAsync } from "expo-splash-screen"
import { FC, useEffect } from "react"

export const SplashScreen: FC = () => {
  useEffect(() => {
    void preventAutoHideAsync()
    return () => {
      void hideAsync()
    }
  }, [])
  return null
}
