import { FC } from "react"
import { StyleProp, View, ViewStyle } from "react-native"
import { useColors } from "./Themed/color"

export interface DividerProps {
  style?: StyleProp<ViewStyle>

  /**
   * @default horizontal
   */
  direction?: "horizontal" | "vertical"

  /**
   * Padding between divider and the container's cross axis edge
   *
   * @default 0
   */
  crossPadding?: number

  /**
   * @default 0
   */
  padding?: number

  /**
   * @default 1
   */
  width?: number

  /**
   * @default solid
   */
  lineStyle?: "solid" | "dashed" | "dotted"

  /**
   * @default colors("gray-300")
   */
  color?: string
}

export const Divider: FC<DividerProps> = props => {
  const colors = useColors()

  const { direction = "horizontal", padding = 0, crossPadding = 0 } = props

  return (
    <View
      style={[
        props.style,
        {
          flexDirection: direction === "horizontal" ? "row" : "column",
          alignSelf: "stretch", // make self fill full available sizing in cross axis
        },
        direction === "horizontal" && {
          paddingTop: padding,
          paddingBottom: padding,
          paddingLeft: crossPadding,
          paddingRight: crossPadding,
        },
        direction === "vertical" && {
          paddingTop: crossPadding,
          paddingBottom: crossPadding,
          paddingLeft: padding,
          paddingRight: padding,
        },
      ]}
    >
      <View
        style={[
          {
            flex: 1,
            borderStyle: props.lineStyle ?? "solid",
            borderColor: props.color ?? colors("gray-300"),
          },
          direction === "horizontal" && {
            borderBottomWidth: props.width ?? 1,
          },
          direction === "vertical" && {
            borderLeftWidth: props.width ?? 1,
          },
        ]}
      />
    </View>
  )
}
