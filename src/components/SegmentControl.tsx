import { StyleProp, Text, TextStyle, View, ViewStyle } from "react-native"
import { FCC } from "../utils/reactHelpers/types"
import {
  PlainIconButtonVariantLayout,
  PlainIconButtonVariantLayoutProps,
} from "./Button/PlainIconButtonVariant"
import { useSelectByButtonState } from "./Button/useButtonState"
import { ButtonVariantProps } from "./ButtonFramework/Button"
import { Pressable } from "./Pressable"
import { useColors } from "./Themed/color"
import { useSpacing } from "./Themed/spacing"

export interface SegmentProps extends ButtonVariantProps {
  icon?: PlainIconButtonVariantLayoutProps["iconLeft"]

  /**
   * @default spacing(2.5)
   */
  padding?: number

  active?: boolean
}

export const Segment: FCC<SegmentProps> = props => {
  const spacing = useSpacing()
  const colors = useColors()

  const activeStyle = {
    container: {
      backgroundColor: colors("blue-500"),
    },
    text: {
      color: colors("white"),
    },
  }

  const [stateRelatedStyle, eventListeners] = useSelectByButtonState<{
    container: ViewStyle
    text: TextStyle
  }>(
    { disabled: props.disabled },
    {
      normal: props.active
        ? activeStyle
        : {
            container: {
              backgroundColor: colors("blue-50"),
            },
            text: {
              color: colors("gray-900"),
            },
          },
      hovering: props.active
        ? activeStyle
        : {
            container: {
              backgroundColor: colors("blue-100"),
            },
            text: {
              color: colors("gray-900"),
            },
          },
      pressing: activeStyle,
      disabled: {
        container: props.active
          ? activeStyle.container
          : { backgroundColor: colors("blue-50") },
        text: {
          ...(props.active ? activeStyle.text : {}),
          color: props.active ? colors("white", 0.6) : colors("gray-900", 0.6),
        },
      },
    },
  )

  return (
    <Pressable
      {...eventListeners}
      style={[
        {
          flex: 1,
          alignItems: "center",
          padding: props.padding ?? spacing(2.5),
        },
        stateRelatedStyle.container,
      ]}
      disabled={props.disabled}
      onPress={props.onPress}
    >
      <PlainIconButtonVariantLayout
        iconLeft={props.icon}
        color={stateRelatedStyle.text.color}
        gap={spacing(2)}
      >
        <Text style={[{ textAlign: "center" }, stateRelatedStyle.text]}>
          {props.children}
        </Text>
      </PlainIconButtonVariantLayout>
    </Pressable>
  )
}

export const SegmentControl: FCC<{
  style?: StyleProp<ViewStyle>
}> = props => {
  return (
    <View
      style={[
        {
          flexDirection: "row",
          alignItems: "center",
          borderRadius: 4,
          overflow: "hidden",
        },
        props.style,
      ]}
    >
      {props.children}
    </View>
  )
}
