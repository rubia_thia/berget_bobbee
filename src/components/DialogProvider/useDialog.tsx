import { ReactNode, useContext, useMemo } from "react"
import { wrapText } from "../../utils/reactHelpers/childrenHelpers"
import { usePersistFn } from "../../utils/reactHelpers/usePersistFn"
import {
  AlertDialogModalContent,
  AlertDialogModalText,
} from "./AlertDialogModalContent"
import {
  ConfirmDialogModalContent,
  ConfirmDialogModalText,
} from "./ConfirmDialogModalContent"
import { DialogOverlayContainer } from "./DialogOverlayContainer"
import { DialogInstance } from "./types"
import { DialogContext } from "./_/DialogContext"
import { DialogBaseController } from "./_/types"

export interface DialogExtendedController extends DialogBaseController {
  confirm(message: ReactNode): DialogInstance<{ confirmed: boolean }>
  alert(message: ReactNode): DialogInstance<void>
}

export const useDialog = (): DialogExtendedController => {
  const ctrl = useContext(DialogContext)

  if (!ctrl) {
    throw new Error("[useDialog] must be used in DialogProvider subtree")
  }

  const showConfirmDialog = usePersistFn(
    (message: ReactNode): DialogInstance<{ confirmed: boolean }> => {
      const _message = wrapText(message, { Text: ConfirmDialogModalText })
      return ctrl.show(info => ({
        dialogContent: (
          <DialogOverlayContainer
            onDismiss={() => info.finish({ confirmed: false })}
          >
            <ConfirmDialogModalContent
              message={_message}
              onConfirm={() => info.finish({ confirmed: true })}
              onCancel={() => info.finish({ confirmed: false })}
            />
          </DialogOverlayContainer>
        ),
      }))
    },
  )

  const showAlertDialog = usePersistFn(
    (message: ReactNode): DialogInstance<void> => {
      const _message = wrapText(message, { Text: AlertDialogModalText })
      return ctrl.show(info => ({
        dialogContent: (
          <DialogOverlayContainer onDismiss={() => info.finish()}>
            <AlertDialogModalContent
              message={_message}
              onClose={() => info.finish()}
            />
          </DialogOverlayContainer>
        ),
      }))
    },
  )

  return useMemo(
    () => ({
      ...ctrl,
      confirm: showConfirmDialog,
      alert: showAlertDialog,
    }),
    [ctrl, showAlertDialog, showConfirmDialog],
  )
}
