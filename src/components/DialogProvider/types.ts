import { ReactElement } from "react"

export interface DialogControllerShowOptionsFactoryInfo<T> {
  finish(value: T): void
}

export type DialogControllerShowOptionsFactory<T> = (
  info: DialogControllerShowOptionsFactoryInfo<T>,
) => DialogControllerShowOptions

export interface DialogControllerShowOptions {
  dialogContent: string | ReactElement
}

export interface DialogInstance<T> {
  update(options: DialogControllerShowOptions): void

  value: Promise<T>

  finish(value: T): void
}
