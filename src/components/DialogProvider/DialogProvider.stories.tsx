import { ComponentMeta, Story } from "@storybook/react"
import { Button } from "react-native"
import { Stack } from "../Stack"
import { ConfirmDialogModalText } from "./ConfirmDialogModalContent"
import { DialogProvider } from "./DialogProvider"
import { useDialog } from "./useDialog"

const longText =
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

export default {
  title: "UI/DialogProvider/DialogProvider",
  component: DialogProvider,
  decorators: [
    Story => (
      <DialogProvider>
        <Story />
      </DialogProvider>
    ),
  ],
} as ComponentMeta<typeof DialogProvider>

export const Normal: Story = () => {
  const dialog = useDialog()

  return (
    <Stack padding={20} space={10}>
      <Button
        title={"Confirm"}
        onPress={() => {
          dialog.confirm("Confirm text")
        }}
      />

      <Button
        title={"Confirm Long text Dialog"}
        onPress={() => {
          dialog.confirm(
            <>
              <ConfirmDialogModalText>{longText}</ConfirmDialogModalText>
              <ConfirmDialogModalText>{longText}</ConfirmDialogModalText>
              <ConfirmDialogModalText>{longText}</ConfirmDialogModalText>
              <ConfirmDialogModalText>{longText}</ConfirmDialogModalText>
              <ConfirmDialogModalText>{longText}</ConfirmDialogModalText>
              <ConfirmDialogModalText>{longText}</ConfirmDialogModalText>
              <ConfirmDialogModalText>{longText}</ConfirmDialogModalText>
            </>,
          )
        }}
      />

      <Button
        title={"Alert"}
        onPress={() => {
          dialog.alert("Alert text")
        }}
      />
    </Stack>
  )
}
