import { FC, ReactNode } from "react"
import { StyleProp, View, ViewStyle } from "react-native"
import { BlueButtonVariant } from "../Button/BlueButtonVariant"
import { WhiteOutlineButtonVariant } from "../Button/WhiteOutlineButtonVariant"
import { Button } from "../ButtonFramework/Button"
import {
  ActionRowColumns,
  CardBoxModalContent,
  TitleBar,
} from "../CardBoxModalContent/CardBoxModalContent"
import { useSpacing } from "../Themed/spacing"
import { DialogModalText } from "./_/DialogModalText"

export interface ConfirmDialogModalContentProps {
  style?: StyleProp<ViewStyle>
  titleText?: ReactNode
  message: ReactNode
  confirmText?: ReactNode
  cancelText?: ReactNode
  onConfirm: () => void
  onCancel: () => void
}

export const ConfirmDialogModalContent: FC<
  ConfirmDialogModalContentProps
> = props => {
  const spacing = useSpacing()

  return (
    <CardBoxModalContent
      style={props.style}
      padding={spacing(4)}
      onClose={props.onCancel}
    >
      <TitleBar>{props.titleText ?? "Confirm"}</TitleBar>

      <View
        style={{
          paddingVertical: 20,
          paddingHorizontal: 10,
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        {props.message}
      </View>

      <ActionRowColumns>
        <Button Variant={WhiteOutlineButtonVariant} onPress={props.onCancel}>
          {props.cancelText ?? "No"}
        </Button>
        <Button Variant={BlueButtonVariant} onPress={props.onConfirm}>
          {props.confirmText ?? "Yes"}
        </Button>
      </ActionRowColumns>
    </CardBoxModalContent>
  )
}

export const ConfirmDialogModalText = DialogModalText
