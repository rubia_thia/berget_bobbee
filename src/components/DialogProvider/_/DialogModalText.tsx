import { Text, TextProps } from "react-native"
import { FCC } from "../../../utils/reactHelpers/types"
import { useColors } from "../../Themed/color"

export const DialogModalText: FCC<TextProps> = props => {
  const colors = useColors()

  return (
    <Text
      {...props}
      style={[
        {
          color: colors("gray-900"),
          fontSize: 14,
          fontWeight: "400",
        },
        props.style,
      ]}
    >
      {props.children}
    </Text>
  )
}
