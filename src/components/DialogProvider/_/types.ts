import {
  DialogControllerShowOptions,
  DialogControllerShowOptionsFactory,
  DialogInstance,
} from "../types"

export interface DialogBaseController {
  show<T>(
    options:
      | DialogControllerShowOptions
      | DialogControllerShowOptionsFactory<T>,
  ): DialogInstance<T>
}
