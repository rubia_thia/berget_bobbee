import { createContext } from "react"
import { DialogBaseController } from "./types"

export const DialogContext = createContext<null | DialogBaseController>(null)
