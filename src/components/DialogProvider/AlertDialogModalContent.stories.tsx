import { ComponentMeta } from "@storybook/react"
import { View } from "react-native"
import { BackgroundColor } from "../../../.storybook/decorators"
import { withTemplate } from "../../../.storybook/utils"
import { noop } from "../../utils/fnHelpers"
import {
  AlertDialogModalContent,
  AlertDialogModalText,
} from "./AlertDialogModalContent"

const longText =
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

export default {
  title: "UI/DialogProvider/AlertDialogModalContent",
  component: AlertDialogModalContent,
  decorators: [
    Story => (
      <View style={{ margin: 10 }}>
        <Story />
      </View>
    ),
    BackgroundColor(),
  ],
} as ComponentMeta<typeof AlertDialogModalContent>

const template = withTemplate(AlertDialogModalContent, {
  message: <AlertDialogModalText>{longText}</AlertDialogModalText>,
  onClose: noop,
})

export const Normal = template()

export const CustomizeText = template(draft => {
  draft.closeText = "Do it!"
})
