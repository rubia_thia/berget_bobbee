import { useEffect, useId } from "react"
import {
  Platform,
  ScrollView,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
} from "react-native"
import { FCC } from "../../utils/reactHelpers/types"
import { useDimensions } from "../../utils/reactHelpers/useDimensions"
import { FullWindowOverlay } from "../FullWindowOverlay"
import { useResponsiveValue } from "../Themed/breakpoints"

const edgePadding = 10

export const DialogOverlayContainer: FCC<{
  width?: number
  onDismiss?: () => void
}> = props => {
  const dim = useDimensions()

  const suggestedWidth = props.width ?? 400

  const containerWidth =
    useResponsiveValue({
      _: dim.width - edgePadding * 2,
      sm: suggestedWidth,
    }) ?? suggestedWidth

  useMakeBrowserPageUnscrollable()

  return (
    <FullWindowOverlay
      style={[
        StyleSheet.absoluteFill,
        Platform.OS === "web" && ({ position: "fixed" } as any),
      ]}
    >
      <ScrollView
        style={{
          flex: 1,
          backgroundColor: "rgba(0, 0, 0, 0.3)",
        }}
        contentContainerStyle={{
          justifyContent: "flex-start",
          padding: edgePadding,
          minHeight: "100%",
        }}
      >
        <TouchableWithoutFeedback
          disabled={props.onDismiss == null}
          onPress={props.onDismiss}
        >
          <View style={StyleSheet.absoluteFill} />
        </TouchableWithoutFeedback>

        <View
          style={{
            marginTop: "auto",
            marginRight: "auto",
            marginBottom: "auto",
            marginLeft: "auto",
            width: containerWidth,
          }}
        >
          {props.children}
        </View>
      </ScrollView>
    </FullWindowOverlay>
  )
}

function useMakeBrowserPageUnscrollable(): void {
  const compId = useId()

  useEffect(() => {
    if (Platform.OS !== "web") return

    const overflowHiddenClassName = `overflow-hidden-${compId.replace(
      /\W/g,
      "",
    )}-${String(Math.random()).replace(".", "")}`

    const el = window.document.createElement("style")
    el.innerHTML = `.${overflowHiddenClassName} { overflow: hidden; }`
    window.document.body.appendChild(el)

    window.document.documentElement.classList.add(overflowHiddenClassName)

    return () => {
      window.document.documentElement.classList.remove(overflowHiddenClassName)
      el.remove()
    }
  }, [compId])
}
