import { Fragment, useMemo, useState } from "react"
import { defer } from "../../utils/promiseHelpers"
import { FCC } from "../../utils/reactHelpers/types"
import { usePersistFn } from "../../utils/reactHelpers/usePersistFn"
import {
  DialogControllerShowOptions,
  DialogControllerShowOptionsFactory,
  DialogControllerShowOptionsFactoryInfo,
  DialogInstance,
} from "./types"
import { DialogContext } from "./_/DialogContext"
import { DialogBaseController } from "./_/types"

export const DialogProvider: FCC = props => {
  const [dialogs, setDialogs] = useState<
    {
      dialogInstance: DialogInstance<unknown>
      options: DialogControllerShowOptions
    }[]
  >([])

  const addDialog = usePersistFn(
    (
      _opts:
        | DialogControllerShowOptions
        | DialogControllerShowOptionsFactory<unknown>,
    ): DialogInstance<unknown> => {
      const valueDefer = defer<unknown>()

      const resolveThisDialog = (value: unknown): void => {
        valueDefer.resolve(value)
        setDialogs(dialogs =>
          dialogs.filter(m => m.dialogInstance !== instance),
        )
      }

      const updateThisDialog = (options: DialogControllerShowOptions): void => {
        setDialogs(dialogs =>
          dialogs.map(m =>
            m.dialogInstance === instance ? { ...m, options } : m,
          ),
        )
      }

      const instance: DialogInstance<unknown> = {
        value: valueDefer.promise,
        update: updateThisDialog,
        finish: resolveThisDialog,
      }

      const factoryInfo: DialogControllerShowOptionsFactoryInfo<unknown> = {
        finish: resolveThisDialog,
      }

      const opts = typeof _opts === "function" ? _opts(factoryInfo) : _opts

      setDialogs(dialog =>
        dialog.concat({ dialogInstance: instance, options: opts }),
      )

      return instance
    },
  )

  const ctrl: DialogBaseController = useMemo(
    () => ({
      show<T>(
        opts:
          | DialogControllerShowOptions
          | DialogControllerShowOptionsFactory<T>,
      ) {
        return addDialog(opts) as DialogInstance<T>
      },
    }),
    [addDialog],
  )

  return (
    <>
      <DialogContext.Provider value={ctrl}>
        {props.children}
      </DialogContext.Provider>
      {dialogs.map((d, idx) => (
        <Fragment key={idx}>{d.options.dialogContent}</Fragment>
      ))}
    </>
  )
}
