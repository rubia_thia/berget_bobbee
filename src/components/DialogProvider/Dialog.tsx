import { FC, ReactNode, useContext, useEffect, useRef } from "react"
import { DialogOverlayContainer } from "./DialogOverlayContainer"
import { DialogInstance } from "./types"
import { DialogContext } from "./_/DialogContext"

export interface DialogProps {
  visible: boolean
  width?: number
  onClose: () => void
  children: ReactNode
}

export const Dialog: FC<DialogProps> = props => {
  const ctrl = useContext(DialogContext)

  if (!ctrl) {
    throw new Error("[Dialog] must be used in DialogProvider subtree")
  }

  const latestInstanceRef = useRef<null | DialogInstance<void>>(null)

  useEffect(() => {
    const dialogContent = (
      <DialogOverlayContainer width={props.width} onDismiss={props.onClose}>
        {props.children}
      </DialogOverlayContainer>
    )

    if (props.visible) {
      if (!latestInstanceRef.current) {
        latestInstanceRef.current = ctrl.show({ dialogContent })
      } else {
        latestInstanceRef.current.update({ dialogContent })
      }
    } else {
      latestInstanceRef.current?.finish()
      latestInstanceRef.current = null
    }
  }, [ctrl, props.children, props.onClose, props.visible, props.width])

  useEffect(() => {
    return () => {
      latestInstanceRef.current?.finish()
      latestInstanceRef.current = null
    }
  }, [])

  return null
}
