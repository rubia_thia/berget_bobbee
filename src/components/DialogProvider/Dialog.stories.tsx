import { ComponentMeta, Story } from "@storybook/react"
import { useState } from "react"
import { Button, Text, TextInput, View } from "react-native"
import {
  CardBoxModalContent,
  TitleBar,
} from "../CardBoxModalContent/CardBoxModalContent"
import { Stack } from "../Stack"
import {
  ConfirmDialogModalContent,
  ConfirmDialogModalText,
} from "./ConfirmDialogModalContent"
import { Dialog } from "./Dialog"
import { DialogProvider } from "./DialogProvider"

const longText =
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

export default {
  title: "UI/DialogProvider/Dialog",
  component: DialogProvider,
  decorators: [
    Story => (
      <DialogProvider>
        <Story />
      </DialogProvider>
    ),
  ],
} as ComponentMeta<typeof DialogProvider>

export const Normal: Story = () => {
  const [showType, setShowType] = useState<null | "short" | "long">(null)

  const [inputContent, setInputContent] = useState("")

  return (
    <Stack padding={20} space={10}>
      <Text>TextInput content inside long text dialog: {inputContent}</Text>

      <Button
        title={"Confirm"}
        onPress={() => {
          setShowType("short")
        }}
      />

      <Dialog visible={showType === "short"} onClose={() => setShowType(null)}>
        <ConfirmDialogModalContent
          message={"Confirm text"}
          onConfirm={() => setShowType(null)}
          onCancel={() => setShowType(null)}
        />
      </Dialog>

      <Button
        title={"Open Long text Dialog"}
        onPress={() => {
          setShowType("long")
        }}
      />

      <Dialog visible={showType === "long"} onClose={() => setShowType(null)}>
        <CardBoxModalContent
          style={{ padding: 20 }}
          onClose={() => setShowType(null)}
        >
          <TitleBar>With Long Text</TitleBar>
          <View>
            <ConfirmDialogModalText>
              Input content: {inputContent}
            </ConfirmDialogModalText>
            <TextInput
              style={{ borderWidth: 1 }}
              value={inputContent}
              onChangeText={setInputContent}
            />
          </View>
          <ConfirmDialogModalText>{longText}</ConfirmDialogModalText>
          <ConfirmDialogModalText>{longText}</ConfirmDialogModalText>
          <ConfirmDialogModalText>{longText}</ConfirmDialogModalText>
          <ConfirmDialogModalText>{longText}</ConfirmDialogModalText>
          <ConfirmDialogModalText>{longText}</ConfirmDialogModalText>
          <ConfirmDialogModalText>{longText}</ConfirmDialogModalText>
          <ConfirmDialogModalText>{longText}</ConfirmDialogModalText>
        </CardBoxModalContent>
      </Dialog>
    </Stack>
  )
}
