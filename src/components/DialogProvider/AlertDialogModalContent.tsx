import { FC, ReactNode } from "react"
import { StyleProp, View, ViewStyle } from "react-native"
import { BlueButtonVariant } from "../Button/BlueButtonVariant"
import { Button } from "../ButtonFramework/Button"
import {
  ActionRowColumns,
  CardBoxModalContent,
  TitleBar,
} from "../CardBoxModalContent/CardBoxModalContent"
import { useSpacing } from "../Themed/spacing"
import { DialogModalText } from "./_/DialogModalText"

export interface AlertDialogModalContentProps {
  style?: StyleProp<ViewStyle>
  titleText?: ReactNode
  message: ReactNode
  closeText?: ReactNode
  onClose: () => void
}

export const AlertDialogModalContent: FC<
  AlertDialogModalContentProps
> = props => {
  const spacing = useSpacing()

  return (
    <CardBoxModalContent
      style={props.style}
      padding={spacing(4)}
      onClose={props.onClose}
    >
      <TitleBar>{props.titleText ?? "Alert"}</TitleBar>

      <View
        style={{
          paddingVertical: 20,
          paddingHorizontal: 10,
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        {props.message}
      </View>

      <ActionRowColumns>
        <Button Variant={BlueButtonVariant} onPress={props.onClose}>
          {props.closeText ?? "Okay"}
        </Button>
      </ActionRowColumns>
    </CardBoxModalContent>
  )
}

export const AlertDialogModalText = DialogModalText
