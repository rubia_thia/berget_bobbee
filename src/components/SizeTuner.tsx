import { values } from "ramda"
import {
  createContext,
  useContext,
  useEffect,
  useId,
  useMemo,
  useRef,
  useState,
} from "react"
import { LayoutChangeEvent, StyleProp, View, ViewStyle } from "react-native"
import { isShallowEqual } from "../utils/isShallowEqual/isShallowEqual"
import { PropsWithChildren } from "../utils/reactHelpers/types"
import { usePersistFn } from "../utils/reactHelpers/usePersistFn"

export type SimpleSize = {
  width: number
  height: number
}

export type ComplexSize<T> = SimpleSize & {
  data: T
}

interface SizeTunerContextValue<T> {
  finalSize?: SimpleSize
  updateSize: (compId: string, size: null | ComplexSize<T>) => void
}

const SizeTunerContext = createContext<null | SizeTunerContextValue<unknown>>(
  null,
)

export type Transformer<T> = (allSize: ComplexSize<T>[]) => SimpleSize

export function createSizeTuner<T>(): {
  SizeTuner: typeof SizeTuner<T>
  SizeConsumer: typeof SizeConsumer<T>
} {
  return {
    SizeTuner,
    SizeConsumer,
  }
}

function SizeTuner<T>(
  props: PropsWithChildren<{
    transformer?: Transformer<T>
  }>,
): JSX.Element {
  const { transformer = defaultTransformer } = props

  const allSizeRef = useRef<Record<string, ComplexSize<T>>>({})

  const [finalSize, setFinalSize] = useState<undefined | SimpleSize>()

  const updateSize: SizeTunerContextValue<T>["updateSize"] = usePersistFn(
    (id, size) => {
      type AllSize = typeof allSizeRef["current"]
      let updated: AllSize
      if (size == null) {
        if (allSizeRef.current[id] == null) return
        const { [id]: _, ..._updated } = allSizeRef.current
        updated = _updated
      } else {
        updated = { [id]: size, ...allSizeRef.current }
      }
      allSizeRef.current = updated
      setFinalSize(oldSize => {
        const allSize = values(updated)
        if (!allSize.length) return undefined
        const newSize = transformer(allSize)
        return isShallowEqual(oldSize, newSize) ? oldSize : newSize
      })
    },
  )

  const value: SizeTunerContextValue<T> = useMemo(
    () => ({ finalSize, updateSize }),
    [finalSize, updateSize],
  )

  return (
    <SizeTunerContext.Provider value={value as SizeTunerContextValue<unknown>}>
      {props.children}
    </SizeTunerContext.Provider>
  )
}

function SizeConsumer<T>(
  props: PropsWithChildren<{
    style?: StyleProp<ViewStyle>
    containerStyle?: StyleProp<ViewStyle>
    data: T
  }>,
): JSX.Element {
  const ctx = useContext(SizeTunerContext)
  if (ctx == null) {
    throw new Error("SizeConsumer should be use in subtree of SizeTuner")
  }

  const compId = useId()

  const { updateSize } = ctx
  useEffect(() => () => updateSize(compId, null), [compId, updateSize])

  const onLayout = (event: LayoutChangeEvent): void => {
    const { layout } = event.nativeEvent
    ctx.updateSize(compId, {
      width: layout.width,
      height: layout.height,
      data: props.data,
    })
  }

  return (
    <View
      style={[props.style, ctx.finalSize, { overflow: "hidden" }]}
      collapsable={false}
    >
      <View
        style={[
          props.containerStyle,
          ctx.finalSize != null && { position: "absolute" },
        ]}
        onLayout={onLayout}
        collapsable={false}
      >
        {props.children}
      </View>
    </View>
  )
}

const defaultTransformer: Transformer<unknown> = allSize => {
  return {
    width: Math.max(...allSize.map(s => s.width), 0),
    height: Math.max(...allSize.map(s => s.height), 0),
  }
}
