import { defineMessage, useIntl } from "react-intl"
import { Text } from "react-native"
import { FCS } from "../../utils/reactHelpers/types"
import { PlainIconButtonVariantLayout } from "../Button/PlainIconButtonVariant"
import { CardBoxModalContent } from "../CardBoxModalContent/CardBoxModalContent"
import {
  DropdownMenu,
  DropdownMenuItem,
  DropdownMenuTitle,
} from "../DropdownMenu"
import { useColors } from "../Themed/color"
import CoinbaseIcon from "./_/coinbase.svg"
import MetabaseIcon from "./_/metamask.svg"
import WalletConnectIcon from "./_/walletConnect.svg"

export const ConnectWalletDialogContent: FCS<{
  onConnectMetamask?: () => void
  onConnectWalletConnect?: () => void
  onConnectCoinbaseWallet?: () => void
  onDismiss: () => void
}> = props => {
  const { $t } = useIntl()
  const colors = useColors()

  const textColor = colors("gray-900")

  return (
    <CardBoxModalContent style={props.style} onClose={props.onDismiss}>
      <DropdownMenu>
        <DropdownMenuTitle
          titleText={$t(
            defineMessage({
              defaultMessage: "Connect Wallet",
              description: "top nav bar/connect wallet/menu group title",
            }),
          )}
        />

        {props.onConnectMetamask != null && (
          <DropdownMenuItem onPress={props.onConnectMetamask}>
            <PlainIconButtonVariantLayout
              color={textColor}
              iconLeft={<MetabaseIcon width="16" height="16" />}
            >
              <Text>
                {$t(
                  defineMessage({
                    defaultMessage: "Metamask",
                    description: "top nav bar/connect wallet/menu item",
                  }),
                )}
              </Text>
            </PlainIconButtonVariantLayout>
          </DropdownMenuItem>
        )}

        {props.onConnectWalletConnect != null && (
          <DropdownMenuItem onPress={props.onConnectWalletConnect}>
            <PlainIconButtonVariantLayout
              color={textColor}
              iconLeft={<WalletConnectIcon width="16" height="16" />}
            >
              <Text>
                {$t(
                  defineMessage({
                    defaultMessage: "Wallet Connect",
                    description: "top nav bar/connect wallet/menu item",
                  }),
                )}
              </Text>
            </PlainIconButtonVariantLayout>
          </DropdownMenuItem>
        )}

        {props.onConnectCoinbaseWallet != null && (
          <DropdownMenuItem onPress={props.onConnectCoinbaseWallet}>
            <PlainIconButtonVariantLayout
              color={textColor}
              iconLeft={<CoinbaseIcon width="16" height="16" />}
            >
              <Text>
                {$t(
                  defineMessage({
                    defaultMessage: "Coinbase",
                    description: "top nav bar/connect wallet/menu item",
                  }),
                )}
              </Text>
            </PlainIconButtonVariantLayout>
          </DropdownMenuItem>
        )}
      </DropdownMenu>
    </CardBoxModalContent>
  )
}
