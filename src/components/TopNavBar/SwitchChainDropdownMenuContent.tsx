import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { ChainInfo } from "../../utils/ChainInfo"
import { OneOrMore } from "../../utils/types"
import { PlainIconButtonVariantLayout } from "../Button/PlainIconButtonVariant"
import {
  DropdownMenuItem,
  DropdownMenuMainText,
  DropdownMenuTitle,
} from "../DropdownMenu"
import { useColors } from "../Themed/color"
import { ActiveIndicator } from "./_/ActiveIndicator"

export interface SwitchableChains {
  chain: ChainInfo

  isActive: boolean

  onSwitch: () => void
}

export const SwitchChainDropdownMenuContent: FC<{
  textColor?: string
  chains: OneOrMore<SwitchableChains>
}> = props => {
  const { $t } = useIntl()

  const colors = useColors()

  const textColor = colors("gray-900")

  return (
    <>
      <DropdownMenuTitle
        titleText={$t(
          defineMessage({
            defaultMessage: "Networks",
            description: "top nav bar/switch network dialog/menu group title",
          }),
        )}
      />

      {props.chains.map(chain => (
        <DropdownMenuItem
          key={chain.chain.id}
          style={{ flexDirection: "row", alignItems: "center" }}
          onPress={chain.onSwitch}
        >
          <PlainIconButtonVariantLayout
            color={props.textColor ?? textColor}
            iconLeft={chain.chain.Icon}
          >
            <DropdownMenuMainText
              style={{ color: props.textColor ?? textColor }}
            >
              {chain.chain.displayName}
            </DropdownMenuMainText>
          </PlainIconButtonVariantLayout>

          <ActiveIndicator
            style={{
              marginLeft: "auto",
              opacity: chain.isActive ? 1 : 0,
            }}
          />
        </DropdownMenuItem>
      ))}
    </>
  )
}
