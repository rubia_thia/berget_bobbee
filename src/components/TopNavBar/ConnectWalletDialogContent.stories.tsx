import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../.storybook/decorators"
import { withTemplate } from "../../../.storybook/utils"
import { noop } from "../../utils/fnHelpers"
import { ConnectWalletDialogContent } from "./ConnectWalletDialogContent"

export default {
  title: "UI/TopNavBar/ConnectWalletDialogContent",
  component: ConnectWalletDialogContent,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof ConnectWalletDialogContent>

const template = withTemplate(ConnectWalletDialogContent, {
  style: { margin: 10 },
  onConnectMetamask: noop,
  onConnectWalletConnect: noop,
  onConnectCoinbaseWallet: noop,
  onDismiss: noop,
})

export const Normal = template()

export const NoMetamask = template(a => {
  a.onConnectMetamask = undefined
})
