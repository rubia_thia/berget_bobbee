import { ComponentMeta } from "@storybook/react"
import { equals } from "ramda"
import { BackgroundColor } from "../../../.storybook/decorators"
import { withTemplate } from "../../../.storybook/utils"
import { TopLevelNavigatorParamList } from "../../navigation/navigators/TopLevelNavigatorTypes"
import { ChainInfoPresets } from "../../utils/ChainInfoPresets/ChainInfoPresets"
import { noop } from "../../utils/fnHelpers"
import {
  MockLinkActiveRouteProvider,
  mockNavigation,
} from "../../utils/reactNavigationHelpers/linkTestHelpers"
import { LanguageDropdownMenuContent } from "./LanguageDropdownMenuContent"
import { MenuDialogContent } from "./MenuDialogContent"
import { NavLinksDropdownMenuContent } from "./NavLinksDropdownMenuContent"
import { SwitchChainDropdownMenuContent } from "./SwitchChainDropdownMenuContent"
import { defineTopNavBarNavItems } from "./TopNavBar"

export default {
  title: "UI/TopNavBar/MenuDialogContent",
  component: MenuDialogContent,
  decorators: [
    BackgroundColor(),
    Story => (
      <MockLinkActiveRouteProvider
        fn={to => ({
          isActive: false,
          isChildrenActive: equals(navItems[1]!.to, to),
        })}
      >
        <Story />
      </MockLinkActiveRouteProvider>
    ),
  ],
} as ComponentMeta<typeof MenuDialogContent>

const navigation = mockNavigation<
  TopLevelNavigatorParamList & {
    Dashboard: undefined
    Earn: undefined
    Buy: undefined
  }
>()
const navItems = defineTopNavBarNavItems([
  {
    navigation,
    to: { screen: "Trade" },
    titleText: "Trade",
  },
  {
    navigation,
    to: { screen: "Dashboard" },
    titleText: "Dashboard",
  },
  {
    navigation,
    to: { screen: "Earn" },
    titleText: "Earn",
  },
  {
    navigation,
    to: { screen: "Buy" },
    titleText: "Buy",
  },
])

const template = withTemplate(MenuDialogContent, {
  style: { margin: 10 },
  extraMenuContent: p => (
    <>
      <NavLinksDropdownMenuContent
        textColor={p.textColor}
        navItems={navItems}
        onPressedItem={noop}
        extraLinks={[]}
      />

      <SwitchChainDropdownMenuContent
        textColor={p.textColor}
        chains={[
          {
            chain: ChainInfoPresets.BSC,
            isActive: true,
            onSwitch: noop,
          },
        ]}
      />

      <LanguageDropdownMenuContent
        textColor={p.textColor}
        languages={[
          {
            displayName: "English",
            isSelected: true,
            onSelect: noop,
          },
          {
            displayName: "简体中文",
            isSelected: false,
            onSelect: noop,
          },
        ]}
      />
    </>
  ),
  onDismiss: noop,
})

export const Normal = template()
