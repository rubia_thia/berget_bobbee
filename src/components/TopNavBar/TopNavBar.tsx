import { NavigationProp } from "@react-navigation/native"
import { FC, ReactNode } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, Text, View, ViewStyle } from "react-native"
import { connectWallet$t } from "../../commonIntlMessages"
import { ChainInfo } from "../../utils/ChainInfo"
import { HrefLink } from "../../utils/reactNavigationHelpers/HrefLink"
import { NavLink } from "../../utils/reactNavigationHelpers/NavLink"
import { NavLinkInfo } from "../../utils/reactNavigationHelpers/reactNavigationHelpers"
import { readResource, SuspenseResource } from "../../utils/SuspenseResource"
import { AnyObject, OneOrMore } from "../../utils/types"
import { PlainIconButtonVariant } from "../Button/PlainIconButtonVariant"
import { RedOutlineButtonVariant } from "../Button/RedOutlineButtonVariant"
import { Button, ButtonTextStyleConsumer } from "../ButtonFramework/Button"
import { Spensor } from "../Spensor"
import { useResponsiveValue } from "../Themed/breakpoints"
import { useColors } from "../Themed/color"
import { useSpacing } from "../Themed/spacing"
import { TruncatedAddressText } from "../TruncatedAddress"
import { ActiveIndicator } from "./_/ActiveIndicator"
import { BlueButton, OutlineButton } from "./_/buttons"
import DownArrowIcon from "./_/downArrow.svg"
import ExternalLinkIcon from "./_/externalLink.svg"
import LanguageIcon from "./_/language.svg"
import Logo from "./_/logo.svg"
import LogoText from "./_/logoText.svg"
import MoreIcon from "./_/more.svg"
import WalletIcon from "./_/wallet.svg"

export interface TopNavBarNavItem<ParamsList extends AnyObject = any>
  extends NavLinkInfo<ParamsList> {
  titleText: ReactNode
}

export function defineTopNavBarNavItems<ParamsList extends AnyObject = any>(
  navItems: OneOrMore<
    Omit<TopNavBarNavItem<ParamsList>, "navigation"> & {
      navigation: NavigationProp<ParamsList>
    }
  >,
): OneOrMore<TopNavBarNavItem<ParamsList>> {
  return navItems as any
}

export const TopNavBarHeight = 56

export const TopNavBar: FC<{
  style?: StyleProp<ViewStyle>
  collapseItems: boolean
  navItems: TopNavBarNavItem[]
  extraLinks: { title: string; link: string }[]
  connectedWallet: SuspenseResource<{ address: string; isMock?: boolean }>
  connectedSupportedChain: SuspenseResource<null | ChainInfo>
  onConnectWallet: () => void
  onPressListSupportedChains: () => void
  onPressLanguage: () => void
  onPressAddress: () => void
  onPressMore: () => void
}> = props => {
  const { $t } = useIntl()
  const spacing = useSpacing()
  const colors = useColors()

  const gap = spacing(2.5)
  const isLogoTextVisible =
    useResponsiveValue({
      _: false,
      sm: true,
    }) ?? false

  return (
    <View
      style={[
        props.style,
        {
          flexDirection: "row",
          justifyContent: "space-between",
          padding: spacing(2.5),
          backgroundColor: colors("white"),
        },
      ]}
    >
      <View
        style={{ flexDirection: "row", alignItems: "center", marginRight: 24 }}
      >
        <Logo />
        {isLogoTextVisible && <LogoText style={{ marginLeft: spacing(1.5) }} />}
      </View>

      {!props.collapseItems && (
        <TopNavItemList
          navItems={props.navItems}
          extraLinks={props.extraLinks}
        />
      )}

      <View
        style={{
          marginLeft: "auto",
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <View style={{ marginLeft: gap }}>
          <Spensor
            fallback={
              <BlueButton iconLeft={WalletIcon} onPress={props.onConnectWallet}>
                {$t(connectWallet$t)}
              </BlueButton>
            }
          >
            {() => (
              <OutlineButton
                iconRight={DownArrowIcon}
                onPress={props.onPressAddress}
                Variant={
                  readResource(props.connectedWallet).isMock
                    ? RedOutlineButtonVariant
                    : undefined
                }
              >
                <TruncatedAddressText
                  address={readResource(props.connectedWallet).address}
                />
              </OutlineButton>
            )}
          </Spensor>
        </View>

        {!props.collapseItems && (
          <>
            <View style={{ marginLeft: gap }}>
              <Spensor>
                {() => {
                  const chain = readResource(props.connectedSupportedChain)

                  if (chain == null) {
                    return (
                      <OutlineButton
                        Variant={RedOutlineButtonVariant}
                        iconRight={DownArrowIcon}
                        onPress={props.onPressListSupportedChains}
                      >
                        {$t(
                          defineMessage({
                            defaultMessage: "Wrong Network",
                            description:
                              "TopNavBar/switch chain button/wrong chain state text",
                          }),
                        )}
                      </OutlineButton>
                    )
                  }

                  return (
                    <OutlineButton
                      iconLeft={chain.Icon}
                      iconRight={DownArrowIcon}
                      onPress={props.onPressListSupportedChains}
                    >
                      {chain.displayName}
                    </OutlineButton>
                  )
                }}
              </Spensor>
            </View>

            <View style={{ marginLeft: gap, minHeight: "auto" }}>
              <OutlineButton
                iconLeft={LanguageIcon}
                onPress={props.onPressLanguage}
              />
            </View>
          </>
        )}

        <OutlineButton
          style={{ marginLeft: gap }}
          iconLeft={MoreIcon}
          onPress={props.onPressMore}
        />
      </View>
    </View>
  )
}

const TopNavItemList: FC<{
  style?: StyleProp<ViewStyle>
  navItems: TopNavBarNavItem[]
  extraLinks: { title: string; link: string }[]
}> = props => {
  const spacing = useSpacing()

  return (
    <View style={[props.style, { flexDirection: "row" }]}>
      {props.navItems.map((item, idx) => (
        <NavLink
          key={idx}
          navigation={item.navigation}
          to={item.to}
          action={item.action}
        >
          {(linkProps, { isActive, isChildrenActive }) => (
            <Button
              style={{
                paddingVertical: spacing(2),
                paddingHorizontal: spacing(4),
              }}
              Variant={p => <PlainIconButtonVariant {...p} />}
              {...linkProps}
            >
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <ButtonTextStyleConsumer>
                  {style => (
                    <Text style={[style, { fontSize: 14, lineHeight: 20 }]}>
                      {item.titleText}
                    </Text>
                  )}
                </ButtonTextStyleConsumer>
                <ActiveIndicator
                  style={{
                    margin: spacing(1),
                    opacity: isActive || isChildrenActive ? 1 : 0,
                  }}
                />
              </View>
            </Button>
          )}
        </NavLink>
      ))}
      {props.extraLinks.map(link => (
        <HrefLink key={link.title} href={link.link}>
          {linkProps => (
            <Button
              Variant={p => <PlainIconButtonVariant {...p} />}
              {...linkProps}
              style={[
                {
                  paddingVertical: spacing(2),
                  paddingHorizontal: spacing(4),
                },
                linkProps.style,
              ]}
            >
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <ButtonTextStyleConsumer>
                  {style => (
                    <Text style={[style, { fontSize: 14, lineHeight: 20 }]}>
                      {link.title}
                    </Text>
                  )}
                </ButtonTextStyleConsumer>
                <ExternalLinkIcon
                  style={{
                    margin: spacing(1),
                  }}
                />
              </View>
            </Button>
          )}
        </HrefLink>
      ))}
    </View>
  )
}
