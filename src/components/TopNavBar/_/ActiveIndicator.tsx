import { FC } from "react"
import { View, ViewStyle } from "react-native"
import { useColors } from "../../Themed/color"
import { useSpacing } from "../../Themed/spacing"

export const ActiveIndicator: FC<{ style?: ViewStyle }> = props => {
  const spacing = useSpacing()
  const colors = useColors()

  return (
    <View
      style={[
        {
          width: spacing(1.5),
          height: spacing(1.5),
          backgroundColor: colors("blue-600"),
          borderRadius: 999,
        },
        props.style,
      ]}
    />
  )
}
