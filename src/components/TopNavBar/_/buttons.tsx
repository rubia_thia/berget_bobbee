import { ComponentType } from "react"
import { StyleProp, Text, ViewStyle } from "react-native"
import { FCC } from "../../../utils/reactHelpers/types"
import { PaddingStyle } from "../../../utils/styleHelpers/PaddingStyle"
import { BlueButtonVariant } from "../../Button/BlueButtonVariant"
import {
  PlainIconButtonVariantLayout,
  PlainIconButtonVariantLayoutIcon,
} from "../../Button/PlainIconButtonVariant"
import { WhiteOutlineButtonVariant } from "../../Button/WhiteOutlineButtonVariant"
import {
  Button,
  ButtonTextStyleConsumer,
  ButtonVariantProps,
} from "../../ButtonFramework/Button"
import { useSpacing } from "../../Themed/spacing"

const useButtonProps = (): {
  textStyle: Button.TextStyle
  padding: PaddingStyle
} => {
  const spacing = useSpacing()

  return {
    textStyle: { fontSize: 12 },
    padding: {
      paddingVertical: spacing(2),
      paddingHorizontal: spacing(4),
    },
  }
}

export const BlueButton: FCC<{
  style?: StyleProp<ViewStyle>
  iconLeft?: PlainIconButtonVariantLayoutIcon
  iconRight?: PlainIconButtonVariantLayoutIcon
  onPress: () => void
}> = props => {
  const buttonProps = useButtonProps()

  return (
    <Button
      style={props.style}
      Variant={p => <BlueButtonVariant {...p} {...buttonProps} />}
      onPress={props.onPress}
    >
      <ButtonTextStyleConsumer>
        {style => (
          <PlainIconButtonVariantLayout
            color={style.color}
            iconLeft={props.iconLeft}
            iconRight={props.iconRight}
          >
            {props.children != null && (
              <Text style={style}>{props.children}</Text>
            )}
          </PlainIconButtonVariantLayout>
        )}
      </ButtonTextStyleConsumer>
    </Button>
  )
}

export const OutlineButton: FCC<{
  style?: StyleProp<ViewStyle>
  Variant?: ComponentType<
    ButtonVariantProps & {
      textStyle?: Button.TextStyle
      padding?: number | PaddingStyle
    }
  >
  iconLeft?: PlainIconButtonVariantLayoutIcon
  iconRight?: PlainIconButtonVariantLayoutIcon
  onPress: () => void
}> = props => {
  const buttonProps = useButtonProps()

  const Variant = props.Variant ?? WhiteOutlineButtonVariant

  return (
    <Button
      style={props.style}
      Variant={p => <Variant {...p} {...buttonProps} />}
      onPress={props.onPress}
    >
      <ButtonTextStyleConsumer>
        {style => (
          <PlainIconButtonVariantLayout
            color={style.color}
            iconLeft={props.iconLeft}
            iconRight={props.iconRight}
          >
            {props.children != null && (
              <Text style={style}>{props.children}</Text>
            )}
          </PlainIconButtonVariantLayout>
        )}
      </ButtonTextStyleConsumer>
    </Button>
  )
}
