import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { HrefLink } from "../../utils/reactNavigationHelpers/HrefLink"
import { NavLink } from "../../utils/reactNavigationHelpers/NavLink"
import { OneOrMore } from "../../utils/types"
import {
  DropdownMenuItem,
  DropdownMenuMainText,
  DropdownMenuTitle,
} from "../DropdownMenu"
import { useColors } from "../Themed/color"
import { TopNavBarNavItem } from "./TopNavBar"
import { ActiveIndicator } from "./_/ActiveIndicator"
import ExternalLinkIcon from "./_/externalLink.svg"

export const NavLinksDropdownMenuContent: FC<{
  textColor?: string
  navItems: OneOrMore<TopNavBarNavItem>
  extraLinks: { title: string; link: string }[]
  onPressedItem: () => void
}> = props => {
  const { $t } = useIntl()

  const colors = useColors()

  const textColor = colors("gray-900")

  return (
    <>
      <DropdownMenuTitle
        titleText={$t(
          defineMessage({
            defaultMessage: "Menu",
            description: "top nav bar/menu dialog/menu group title",
          }),
        )}
      />

      {props.navItems.map((item, idx) => (
        <NavLink
          key={idx}
          navigation={item.navigation}
          to={item.to}
          action={item.action}
        >
          {(linkProps, { isActive, isChildrenActive }) => (
            <DropdownMenuItem
              style={{ flexDirection: "row", alignItems: "center" }}
              onPress={() => {
                linkProps.onPress()
                props.onPressedItem()
              }}
            >
              <DropdownMenuMainText
                style={{ color: props.textColor ?? textColor }}
              >
                {item.titleText}
              </DropdownMenuMainText>

              <ActiveIndicator
                style={{
                  marginLeft: "auto",
                  opacity: isActive || isChildrenActive ? 1 : 0,
                }}
              />
            </DropdownMenuItem>
          )}
        </NavLink>
      ))}
      {props.extraLinks.map(link => (
        <HrefLink href={link.link}>
          {linkProps => (
            <DropdownMenuItem
              {...linkProps}
              className="flex-row items-center space-x-2"
            >
              <DropdownMenuMainText>{link.title}</DropdownMenuMainText>
              <ExternalLinkIcon />
            </DropdownMenuItem>
          )}
        </HrefLink>
      ))}
    </>
  )
}
