import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { OneOrMore } from "../../utils/types"
import {
  DropdownMenuItem,
  DropdownMenuMainText,
  DropdownMenuTitle,
} from "../DropdownMenu"
import { useColors } from "../Themed/color"
import { ActiveIndicator } from "./_/ActiveIndicator"

export interface LanguageInfo {
  displayName: string

  isSelected: boolean

  onSelect: () => void
}

export const LanguageDropdownMenuContent: FC<{
  textColor?: string
  languages: OneOrMore<LanguageInfo>
}> = props => {
  const { $t } = useIntl()

  const colors = useColors()

  const textColor = colors("gray-900")

  return (
    <>
      <DropdownMenuTitle
        titleText={$t(
          defineMessage({
            defaultMessage: "Language",
            description: "top nav bar/menu dialog/menu group title",
          }),
        )}
      />

      {props.languages.map((l, idx) => (
        <DropdownMenuItem
          key={idx}
          style={{ flexDirection: "row", alignItems: "center" }}
          onPress={() => {
            l.onSelect()
          }}
        >
          <DropdownMenuMainText style={{ color: props.textColor ?? textColor }}>
            {l.displayName}
          </DropdownMenuMainText>

          <ActiveIndicator
            style={{
              marginLeft: "auto",
              opacity: l.isSelected ? 1 : 0,
            }}
          />
        </DropdownMenuItem>
      ))}
    </>
  )
}
