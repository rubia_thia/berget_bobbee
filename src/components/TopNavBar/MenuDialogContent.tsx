import { FC, ReactNode } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, ViewStyle } from "react-native"
import { HrefLink } from "../../utils/reactNavigationHelpers/HrefLink"
import { CardBoxModalContent } from "../CardBoxModalContent/CardBoxModalContent"
import {
  DropdownMenu,
  DropdownMenuItem,
  DropdownMenuMainText,
  DropdownMenuTitle,
} from "../DropdownMenu"
import { useColors } from "../Themed/color"

export const MenuDialogContent: FC<{
  style?: StyleProp<ViewStyle>

  extraMenuContent?: (renderProps: { textColor: string }) => ReactNode

  onDismiss: () => void
}> = props => {
  const { $t } = useIntl()

  const colors = useColors()

  const textColor = colors("gray-900")

  return (
    <CardBoxModalContent style={props.style} onClose={props.onDismiss}>
      <DropdownMenu>
        {props.extraMenuContent?.({ textColor })}

        <DropdownMenuTitle
          titleText={$t(
            defineMessage({
              defaultMessage: "Links",
              description: "top nav bar/menu dialog/menu group title",
            }),
          )}
        />

        <HrefLinkItem
          href="https://twitter.com/UniwhaleEx"
          text={$t(
            defineMessage({
              defaultMessage: "Twitter",
              description: "top nav bar/menu dialog/menu item",
            }),
          )}
        />
        <HrefLinkItem
          href="https://discord.com/invite/Uniwhale"
          text={$t(
            defineMessage({
              defaultMessage: "Discord",
              description: "top nav bar/menu dialog/menu item",
            }),
          )}
        />
        <HrefLinkItem
          href="https://medium.com/uniwhale"
          text={$t(
            defineMessage({
              defaultMessage: "Medium",
              description: "top nav bar/menu dialog/menu item",
            }),
          )}
        />
        <HrefLinkItem
          href="https://www.uniwhale.co/"
          text={$t(
            defineMessage({
              defaultMessage: "About",
              description: "top nav bar/menu dialog/menu item",
            }),
          )}
        />
      </DropdownMenu>
    </CardBoxModalContent>
  )
}

const HrefLinkItem: FC<{ href: string; text?: ReactNode }> = props => {
  return (
    <HrefLink href={props.href}>
      {linkProps => (
        <DropdownMenuItem {...linkProps}>
          <DropdownMenuMainText>{props.text}</DropdownMenuMainText>
        </DropdownMenuItem>
      )}
    </HrefLink>
  )
}
