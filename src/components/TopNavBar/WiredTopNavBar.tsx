import { NavigationProp } from "@react-navigation/native"
import * as Clipboard from "expo-clipboard"
import { action, reaction } from "mobx"
import { sortBy } from "ramda"
import { FC, useEffect, useRef } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, ViewStyle } from "react-native"
import { copied$t } from "../../commonIntlMessages"
import { TopLevelNavigatorParamList } from "../../navigation/navigators/TopLevelNavigatorTypes"
import { useAppEnvStore } from "../../stores/AppEnvStore/useAppEnvStore"
import { SupportLoginType } from "../../stores/AuthStore/AuthStore.service"
import { useAuthStore } from "../../stores/AuthStore/useAuthStore"
import { safelyGet } from "../../stores/utils/waitFor"
import { map } from "../../utils/arrayHelpers"
import { noop } from "../../utils/fnHelpers"
import { usePersistFn } from "../../utils/reactHelpers/usePersistFn"
import { suspenseResource } from "../../utils/SuspenseResource"
import { CardBoxModalContent } from "../CardBoxModalContent/CardBoxModalContent"
import { Dialog } from "../DialogProvider/Dialog"
import { DialogOverlayContainer } from "../DialogProvider/DialogOverlayContainer"
import { useDialog } from "../DialogProvider/useDialog"
import { DropdownMenu } from "../DropdownMenu"
import { useLocaleSwitch } from "../IntlProvider/LocaleSwitchProvider"
import { ConnectToRightChainMessageItem } from "../MessageProvider/ConnectToRightChainMessageItem"
import { useMessage } from "../MessageProvider/MessageProvider"
import { Spensor } from "../Spensor"
import { useResponsiveValue } from "../Themed/breakpoints"
import { AccountDialogContent } from "./AccountDialogContent"
import { ConnectWalletDialogContent } from "./ConnectWalletDialogContent"
import { LanguageDropdownMenuContent } from "./LanguageDropdownMenuContent"
import { MenuDialogContent } from "./MenuDialogContent"
import { NavLinksDropdownMenuContent } from "./NavLinksDropdownMenuContent"
import { SwitchChainDropdownMenuContent } from "./SwitchChainDropdownMenuContent"
import { defineTopNavBarNavItems, TopNavBar } from "./TopNavBar"

const WiredAccountDialogContent: FC<{
  onDismiss: () => void
}> = props => {
  const authStore = useAuthStore()
  const message = useMessage()
  const app = useAppEnvStore()

  const { $t } = useIntl()

  const onCopyAddress = async (): Promise<void> => {
    await Clipboard.setStringAsync(authStore.account$)
    message.show({ message: $t(copied$t) })
    props.onDismiss()
  }

  const onDisconnect = (): void => {
    void authStore.disconnect()
    props.onDismiss()
  }

  return (
    <AccountDialogContent
      explorerLink={suspenseResource(() =>
        app.addressExplorerUrl(authStore.account$),
      )}
      onCopyAddress={onCopyAddress}
      onDisconnect={onDisconnect}
      onDismiss={props.onDismiss}
    />
  )
}

const WiredConnectWalletDialog: FC = () => {
  const authStore = useAuthStore()
  const onDismiss = action(() => (authStore.showConnectWalletModal = false))
  const message = useMessage()
  return (
    <Dialog visible={authStore.showConnectWalletModal} onClose={onDismiss}>
      <ConnectWalletDialogContent
        onConnectMetamask={
          !authStore.isMetaMaskInstalled
            ? undefined
            : async () => {
                await authStore
                  .login(SupportLoginType.MetaMask)
                  .catch(e => message.error({ message: e.message }))
                onDismiss()
              }
        }
        onConnectWalletConnect={async () => {
          await authStore
            .login(SupportLoginType.WalletConnect)
            .catch(e => message.error({ message: e.message }))
          onDismiss()
        }}
        onConnectCoinbaseWallet={async () => {
          await authStore
            .login(SupportLoginType.CoinbaseWallet)
            .catch(e => message.error({ message: e.message }))
          onDismiss()
        }}
        onDismiss={onDismiss}
      />
    </Dialog>
  )
}

export const WiredTopNavBar: FC<{
  style?: StyleProp<ViewStyle>
  navigation: NavigationProp<TopLevelNavigatorParamList>
}> = props => {
  const authStore = useAuthStore()
  const dialogCtrl = useDialog()
  const appEnv = useAppEnvStore()
  const { $t } = useIntl()
  const message = useMessage()

  const collapseNavItems =
    useResponsiveValue({
      _: true,
      md: false,
    }) ?? false

  const { currentLocale, availableLocales, onLocaleSwitch } = useLocaleSwitch()

  const switchChainMenuContent = (renderProps: {
    textColor?: string
    onSwitched?: () => void
  }): JSX.Element => (
    <Spensor>
      {() => (
        <SwitchChainDropdownMenuContent
          textColor={renderProps.textColor}
          chains={[
            {
              chain: appEnv.chainInfo$,
              isActive: authStore.connectedToTheRightChain$,
              onSwitch: async () => {
                await authStore.switchToRightChain()
                renderProps.onSwitched?.()
              },
            },
          ]}
        />
      )}
    </Spensor>
  )

  const sortedLocales: typeof availableLocales = sortBy(
    l => l.languageTag,
    availableLocales,
  ) as any
  const switchLanguageMenuContent = (renderProps: {
    textColor?: string
    onSwitched?: () => void
  }): JSX.Element => (
    <LanguageDropdownMenuContent
      textColor={renderProps.textColor}
      languages={map(
        l => ({
          displayName: l.displayName,
          isSelected: l.languageTag === currentLocale.languageTag,
          onSelect: () => {
            onLocaleSwitch(l)
            renderProps.onSwitched?.()
          },
        }),
        sortedLocales,
      )}
    />
  )

  const onPressAddress = (): void => {
    dialogCtrl.show(info => ({
      dialogContent: (
        <DialogOverlayContainer
          width={300}
          onDismiss={() => info.finish(undefined)}
        >
          <WiredAccountDialogContent onDismiss={() => info.finish(undefined)} />
        </DialogOverlayContainer>
      ),
    }))
  }

  const onPressListSupportedChains = usePersistFn((): void => {
    dialogCtrl.show(info => ({
      dialogContent: (
        <DialogOverlayContainer
          width={300}
          onDismiss={() => info.finish(undefined)}
        >
          <CardBoxModalContent
            style={props.style}
            onClose={() => info.finish(undefined)}
          >
            <DropdownMenu>
              {switchChainMenuContent({
                onSwitched: () => info.finish(undefined),
              })}
            </DropdownMenu>
          </CardBoxModalContent>
        </DialogOverlayContainer>
      ),
    }))
  })

  const onPressLanguage = (): void => {
    dialogCtrl.show(info => ({
      dialogContent: (
        <DialogOverlayContainer
          width={300}
          onDismiss={() => info.finish(undefined)}
        >
          <CardBoxModalContent
            style={props.style}
            onClose={() => info.finish(undefined)}
          >
            <DropdownMenu>
              {switchLanguageMenuContent({
                onSwitched: () => info.finish(undefined),
              })}
            </DropdownMenu>
          </CardBoxModalContent>
        </DialogOverlayContainer>
      ),
    }))
  }

  const onPressMore = (): void => {
    dialogCtrl.show(info => ({
      dialogContent: (
        <DialogOverlayContainer onDismiss={() => info.finish(undefined)}>
          <MenuDialogContent
            extraMenuContent={
              !collapseNavItems
                ? undefined
                : p => (
                    <>
                      <NavLinksDropdownMenuContent
                        textColor={p.textColor}
                        navItems={navItems}
                        extraLinks={extraLinks}
                        onPressedItem={() => info.finish(undefined)}
                      />

                      {switchChainMenuContent({
                        textColor: p.textColor,
                        onSwitched: () => info.finish(undefined),
                      })}

                      {switchLanguageMenuContent({
                        textColor: p.textColor,
                        onSwitched: () => info.finish(undefined),
                      })}
                    </>
                  )
            }
            onDismiss={() => info.finish(undefined)}
          />
        </DialogOverlayContainer>
      ),
    }))
  }

  const navItems = defineTopNavBarNavItems([
    {
      navigation: props.navigation,
      to: { screen: "Trade" },
      titleText: $t(
        defineMessage({
          defaultMessage: "Trade",
          description: "Top nav bar/item title",
        }),
      ),
    },
    {
      navigation: props.navigation,
      to: { screen: "Liquidity" },
      titleText: $t(
        defineMessage({
          defaultMessage: "Liquidity",
          description: "Top nav bar/item title",
        }),
      ),
    },
    {
      navigation: props.navigation,
      to: { screen: "Earn" },
      titleText: $t(
        defineMessage({
          defaultMessage: "Earn",
          description: "Top nav bar/item title",
        }),
      ),
    },
    {
      navigation: props.navigation,
      to: { screen: "Airdrop" },
      titleText: $t(
        defineMessage({
          defaultMessage: "Airdrop",
          description: "Top nav bar/item title",
        }),
      ),
    },
    {
      navigation: props.navigation,
      to: { screen: "Referral" },
      titleText: $t(
        defineMessage({
          defaultMessage: "Referral",
          description: "Top nav bar/item title",
        }),
      ),
    },
    {
      navigation: props.navigation,
      to: { screen: "Faucet" },
      titleText: $t(
        defineMessage({
          defaultMessage: "Faucet",
          description: "Top nav bar/item title",
        }),
      ),
    },
  ])

  const extraLinks = [
    {
      title: $t(
        defineMessage({
          defaultMessage: "Stats",
          description: "Top nav bar/item title",
        }),
      ),
      link: "https://dune.com/uniwhale/uniwhale-overview",
    },
    {
      title: $t(
        defineMessage({
          defaultMessage: "Docs",
          description: "Top nav bar/item title",
        }),
      ),
      link: "https://docs.uniwhale.co",
    },
  ]

  function remove(screen: keyof TopLevelNavigatorParamList): void {
    navItems.splice(
      navItems.findIndex(a => {
        const to = a.to
        if (typeof to === "string") return false
        return to.screen === screen
      }),
      1,
    )
  }

  const showEarnTab = safelyGet(() => appEnv.appEnv$.showEarnTab) ?? false
  if (!showEarnTab) remove("Earn")

  const showLPTab = safelyGet(() => appEnv.appEnv$.showLPTab) ?? false
  if (!showLPTab) remove("Liquidity")

  const showAirdropTab = safelyGet(() => appEnv.appEnv$.showAirdropTab) ?? false
  if (!showAirdropTab) remove("Airdrop")

  const showReferralTab =
    safelyGet(() => appEnv.appEnv$.showReferralTab) ?? false
  if (!showReferralTab) remove("Referral")

  const showFaucetTab = safelyGet(() => appEnv.appEnv$.showFaucetTab) ?? false
  if (!showFaucetTab) remove("Faucet")

  const closeRightChainMessageFnRef = useRef<null | (() => void)>(null)
  useEffect(
    () =>
      reaction(
        () => authStore.connectedToTheRightChain$,
        isInRightChain => {
          const key = "WiredTopNavBar/connectedToTheRightChainMessage"

          if (isInRightChain) {
            closeRightChainMessageFnRef.current?.()
          } else {
            closeRightChainMessageFnRef.current = message.show({
              key,
              autoDismiss: null,
              message: (
                <ConnectToRightChainMessageItem
                  onSwitch={onPressListSupportedChains}
                />
              ),
            })
          }
        },
        { onError: noop },
      ),
    [authStore, message, onPressListSupportedChains],
  )

  return (
    <>
      <TopNavBar
        style={props.style}
        collapseItems={collapseNavItems}
        navItems={navItems}
        connectedWallet={suspenseResource(() => ({
          address: authStore.account$,
          isMock: authStore.isMockAccount$,
        }))}
        connectedSupportedChain={suspenseResource(() =>
          authStore.connectedToTheRightChain$ ? appEnv.chainInfo$ : null,
        )}
        onPressListSupportedChains={onPressListSupportedChains}
        onConnectWallet={action(
          () => (authStore.showConnectWalletModal = true),
        )}
        onPressAddress={onPressAddress}
        onPressLanguage={onPressLanguage}
        onPressMore={onPressMore}
        extraLinks={extraLinks}
      />
      <WiredConnectWalletDialog />
    </>
  )
}
