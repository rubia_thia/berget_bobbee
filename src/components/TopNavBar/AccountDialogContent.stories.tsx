import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../.storybook/decorators"
import { withTemplate } from "../../../.storybook/utils"
import { noop } from "../../utils/fnHelpers"
import { AccountDialogContent } from "./AccountDialogContent"

export default {
  title: "UI/TopNavBar/AccountDialogContent",
  component: AccountDialogContent,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof AccountDialogContent>

const template = withTemplate(AccountDialogContent, {
  style: { margin: 10 },
  explorerLink: "https://google.com",
  onCopyAddress: noop,
  onDisconnect: noop,
  onDismiss: noop,
})

export const Normal = template()
