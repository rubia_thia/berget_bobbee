import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, Text, ViewStyle } from "react-native"
import { Path, Svg } from "react-native-svg"
import { HrefLink } from "../../utils/reactNavigationHelpers/HrefLink"
import {
  safeReadResource,
  SuspenseResource,
} from "../../utils/SuspenseResource"
import { PlainIconButtonVariantLayout } from "../Button/PlainIconButtonVariant"
import { CardBoxModalContent } from "../CardBoxModalContent/CardBoxModalContent"
import {
  DropdownMenu,
  DropdownMenuItem,
  DropdownMenuTitle,
} from "../DropdownMenu"
import { useColors } from "../Themed/color"

export const AccountDialogContent: FC<{
  style?: StyleProp<ViewStyle>
  explorerLink: SuspenseResource<string>
  onCopyAddress: () => void
  onDisconnect: () => void
  onDismiss: () => void
}> = props => {
  const { $t } = useIntl()
  const colors = useColors()

  const textColor = colors("gray-900")

  const explorerLink = safeReadResource(props.explorerLink)

  return (
    <CardBoxModalContent style={props.style} onClose={props.onDismiss}>
      <DropdownMenu>
        <DropdownMenuTitle
          titleText={$t(
            defineMessage({
              defaultMessage: "Account",
              description: "top nav bar/account dialog/menu group title",
            }),
          )}
        />

        <DropdownMenuItem onPress={props.onCopyAddress}>
          <PlainIconButtonVariantLayout
            color={textColor}
            iconLeft={
              <Svg width="16" height="16" viewBox="0 0 16 16" fill="#111827">
                <Path d="M12 4V0H0V12H4V16H16V4H12ZM4 10.6667H1.33333V1.33333H10.6667V4H4V10.6667ZM14.6667 14.6667H5.33333V5.33333H14.6667V14.6667Z" />
              </Svg>
            }
          >
            <Text>
              {$t(
                defineMessage({
                  defaultMessage: "Copy Address",
                  description: "top nav bar/account dialog/menu item",
                }),
              )}
            </Text>
          </PlainIconButtonVariantLayout>
        </DropdownMenuItem>

        <HrefLink href={explorerLink}>
          {linkProps => (
            <DropdownMenuItem {...linkProps}>
              <PlainIconButtonVariantLayout
                color={textColor}
                iconLeft={
                  <Svg
                    width="16"
                    height="16"
                    viewBox="0 0 16 16"
                    fill="#111827"
                  >
                    <Path d="M14 8.66602V15.3327H0V2.66602H8V3.99935H1.33333V13.9993H12.6667V8.66602H14ZM16 0.666016H8.67467L11.3647 3.33268L6.71333 8.04602L8.59867 9.93135L13.25 5.21802L16 7.99935V0.666016Z" />
                  </Svg>
                }
              >
                <Text>
                  {$t(
                    defineMessage({
                      defaultMessage: "View in Explorer",
                      description: "top nav bar/account dialog/menu item",
                    }),
                  )}
                </Text>
              </PlainIconButtonVariantLayout>
            </DropdownMenuItem>
          )}
        </HrefLink>

        <DropdownMenuItem onPress={props.onDisconnect}>
          <PlainIconButtonVariantLayout
            color={textColor}
            iconLeft={
              <Svg width="16" height="16" viewBox="0 0 16 16" fill="#111827">
                <Path d="M10.6667 6.00065V3.33398L16 8.00065L10.6667 12.6673V10.0007H5.33333V6.00065H10.6667ZM0 1.33398V14.6673H9.33333V13.334H1.33333V2.66732H9.33333V1.33398H0Z" />
              </Svg>
            }
          >
            <Text>
              {$t(
                defineMessage({
                  defaultMessage: "Disconnect",
                  description: "top nav bar/account dialog/menu item",
                }),
              )}
            </Text>
          </PlainIconButtonVariantLayout>
        </DropdownMenuItem>
      </DropdownMenu>
    </CardBoxModalContent>
  )
}
