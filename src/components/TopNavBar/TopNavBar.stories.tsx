import { ComponentMeta, Story } from "@storybook/react"
import * as Clipboard from "expo-clipboard"
import { equals } from "ramda"
import { BackgroundColor, CardContainer } from "../../../.storybook/decorators"
import { withTemplate } from "../../../.storybook/utils"
import { TopLevelNavigatorParamList } from "../../navigation/navigators/TopLevelNavigatorTypes"
import { noop } from "../../utils/fnHelpers"
import {
  MockLinkActiveRouteProvider,
  mockNavigation,
} from "../../utils/reactNavigationHelpers/linkTestHelpers"
import { suspenseResource } from "../../utils/SuspenseResource"
import { DialogOverlayContainer } from "../DialogProvider/DialogOverlayContainer"
import { DialogProvider } from "../DialogProvider/DialogProvider"
import { useDialog } from "../DialogProvider/useDialog"
import { AccountDialogContent } from "./AccountDialogContent"
import { defineTopNavBarNavItems, TopNavBar } from "./TopNavBar"

export default {
  title: "UI/TopNavBar",
  component: TopNavBar,
  decorators: [
    CardContainer({ margin: 10, padding: 10 }),
    BackgroundColor(),
    Story => (
      <DialogProvider>
        <Story />
      </DialogProvider>
    ),
    Story => (
      <MockLinkActiveRouteProvider
        fn={to => ({
          isActive: false,
          isChildrenActive: equals(navItems[1]!.to, to),
        })}
      >
        <Story />
      </MockLinkActiveRouteProvider>
    ),
  ],
} as ComponentMeta<typeof TopNavBar>

const navigation = mockNavigation<
  TopLevelNavigatorParamList & {
    Dashboard: undefined
    Earn: undefined
    Buy: undefined
  }
>()
const navItems = defineTopNavBarNavItems([
  {
    navigation,
    to: { screen: "Trade" },
    titleText: "Trade",
  },
  {
    navigation,
    to: { screen: "Dashboard" },
    titleText: "Dashboard",
  },
  {
    navigation,
    to: { screen: "Earn" },
    titleText: "Earn",
  },
  {
    navigation,
    to: { screen: "Buy" },
    titleText: "Buy",
  },
])

const template = withTemplate(TopNavBar, {
  collapseItems: false,
  navItems,
  connectedWallet: {
    address: "0xabcdefghijklmnopqrstuvwxyz",
  },
  onConnectWallet: noop,
  onPressAddress: noop,
  onPressMore: noop,
  connectedSupportedChain: null,
  onPressListSupportedChains: noop,
  extraLinks: [
    { link: "https://dune.com/uniwhale/uniwhale-overview", title: "Stats" },
  ],
})

export const Normal = template()

export const ConnectWallet = template(p => {
  p.connectedWallet = suspenseResource(() => {
    throw new Promise(noop)
  })
})

export const HideNavList = template(p => {
  p.collapseItems = true
})

export const OpenDialog: Story = () => {
  const address = "0x123456789"

  const dialogCtrl = useDialog()

  const onCopyAddress = async (): Promise<void> => {
    await Clipboard.setStringAsync(address)
    alert("Copied")
  }

  const onPressAddress = (): void => {
    dialogCtrl.show(info => ({
      dialogContent: (
        <DialogOverlayContainer onDismiss={() => info.finish(undefined)}>
          <AccountDialogContent
            explorerLink={"https://google.com"}
            onCopyAddress={onCopyAddress}
            onDisconnect={noop}
            onDismiss={() => info.finish(undefined)}
          />
        </DialogOverlayContainer>
      ),
    }))
  }

  return (
    <TopNavBar
      onPressLanguage={noop}
      connectedSupportedChain={null}
      onPressListSupportedChains={noop}
      collapseItems={false}
      navItems={navItems}
      extraLinks={[
        { link: "https://dune.com/uniwhale/uniwhale-overview", title: "Stats" },
      ]}
      connectedWallet={{ address: "0x123456789" }}
      onConnectWallet={noop}
      onPressAddress={onPressAddress}
      onPressMore={noop}
    />
  )
}
