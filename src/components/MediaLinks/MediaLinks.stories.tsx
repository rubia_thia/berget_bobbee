import { ComponentMeta } from "@storybook/react"
import { withTemplate } from "../../../.storybook/utils"
import { MediaLinks } from "./MediaLinks"

export default {
  title: "UI/MediaLinks",
  component: MediaLinks,
} as ComponentMeta<typeof MediaLinks>

const template = withTemplate(MediaLinks, {
  links: {
    website: "www.google.com",
    medium: "www.medium.com",
    twitter: "www.twitter.com",
    discord: "www.discord.com",
  },
})

export const Normal = template()
