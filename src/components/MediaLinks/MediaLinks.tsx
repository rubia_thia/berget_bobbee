import { FC } from "react"
import { StyleProp } from "react-native"
import { HrefLink } from "../../utils/reactNavigationHelpers/HrefLink"
import { MarginStyle } from "../../utils/styleHelpers/MarginStyle"
import { Stack } from "../Stack"
import { useColors } from "../Themed/color"
import { useSpacing } from "../Themed/spacing"
import DiscordIcon from "./_/discord.svg"
import GithubIcon from "./_/github.svg"
import MediumIcon from "./_/medium.svg"
import TelegramIcon from "./_/telegram.svg"
import TwitterIcon from "./_/twitter.svg"
import WebsiteIcon from "./_/website.svg"

export interface MediaLinksProps {
  style?: StyleProp<MarginStyle>

  itemPadding?: number

  gap?: number

  tint?: string

  size?: number

  links: {
    website?: string
    twitter?: string
    medium?: string
    discord?: string
    github?: string
    telegram?: string
  }
}

export const MediaLinks: FC<MediaLinksProps> = props => {
  const spacing = useSpacing()
  const colors = useColors()

  const {
    links,
    tint = colors("gray-500"),
    gap = spacing(6),
    size = spacing(4),
  } = props

  const commonProps = {
    tint,
    size,
    padding: props.itemPadding,
  }

  return (
    <Stack
      horizontal={true}
      style={[props.style, { alignItems: "center" }]}
      space={gap}
    >
      {links.website && (
        <Link Icon={WebsiteIcon} url={links.website} {...commonProps} />
      )}
      {links.telegram && (
        <Link Icon={TelegramIcon} url={links.telegram} {...commonProps} />
      )}
      {links.medium && (
        <Link Icon={MediumIcon} url={links.medium} {...commonProps} />
      )}
      {links.twitter && (
        <Link Icon={TwitterIcon} url={links.twitter} {...commonProps} />
      )}
      {links.discord && (
        <Link Icon={DiscordIcon} url={links.discord} {...commonProps} />
      )}
      {links.github && (
        <Link Icon={GithubIcon} url={links.github} {...commonProps} />
      )}
    </Stack>
  )
}

const Link: FC<{
  padding?: number
  Icon: typeof WebsiteIcon
  url: string
  tint: string
  size: number
}> = props => {
  return (
    <HrefLink style={{ padding: props.padding }} href={props.url}>
      <props.Icon width={props.size} height={props.size} fill={props.tint} />
    </HrefLink>
  )
}
