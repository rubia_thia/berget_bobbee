import angleToCoordinates from "css-gradient-angle-to-svg-gradient-coordinates"
import { FC } from "react"
import { StyleProp, ViewStyle } from "react-native"
import Svg, {
  Defs,
  LinearGradient as SvgLinearGradient,
  Rect,
  Stop,
} from "react-native-svg"
import { idFactory } from "../utils/stringHelpers"
import { OneOrMore } from "../utils/types"
import { setColorAlpha } from "./Themed/color"

const gradientId = idFactory("LinearGradient-gradient", "-")

export type LinearGradientColor =
  | string
  | { color: string; opacity?: number; offset?: number }

/**
 * Transform `LinearGradientColor` to a part of `linear-gradient` argument:
 *
 *     { color: "rgb(0, 0, 0)", opacity: 0.5, offset: 0.5 } => "rgba(0, 0, 0, 0.5) 50%"
 */
export const linearGradientColorToCss = (
  color: LinearGradientColor,
): string => {
  if (typeof color === "string") {
    return color
  }

  const { color: colorValue, opacity = 1, offset } = color
  const finaleColor = setColorAlpha(colorValue, opacity)

  return `${finaleColor} ${offset != null ? ` ${offset * 100}%` : ""}`.trim()
}

export const LinearGradient: FC<{
  style?: StyleProp<ViewStyle>

  /**
   * @default vertical
   */
  direction?: "vertical" | "horizontal" | number

  colors: OneOrMore<LinearGradientColor>
}> = props => {
  const id = gradientId()

  const degree =
    props.direction === "horizontal"
      ? 90
      : typeof props.direction === "number"
      ? props.direction
      : 0
  const points = angleToCoordinates(degree)

  return (
    <Svg style={props.style} fill="none">
      <Rect width={"100%"} height={"100%"} fill={`url(#g-${id})`} />
      <Defs>
        <SvgLinearGradient
          id={`g-${id}`}
          {...points}
          gradientUnits={"objectBoundingBox"}
        >
          {props.colors.map((color, index) => {
            const {
              color: colorValue,
              opacity = 1,
              offset = index / (props.colors.length - 1),
            } = typeof color === "string" ? { color } : color

            return (
              <Stop
                key={index}
                offset={offset}
                stopColor={colorValue}
                stopOpacity={opacity}
              />
            )
          })}
        </SvgLinearGradient>
      </Defs>
    </Svg>
  )
}
