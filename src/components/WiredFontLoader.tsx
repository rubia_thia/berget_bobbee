import { isLoaded, loadAsync } from "expo-font"
import { FC } from "react"

const fontMap = {
  "space-mono": require("../assets/fonts/SpaceMono-Regular.ttf"),
}

export const WiredFontLoader: FC = () => {
  if (!Object.keys(fontMap).every(f => isLoaded(f))) {
    throw loadAsync(fontMap)
  }
  return null
}
