import { ComponentMeta } from "@storybook/react"
import { BackgroundColor, CardContainer } from "../../.storybook/decorators"
import { withTemplate } from "../../.storybook/utils"
import { LinearGradient } from "./LinearGradient"

export default {
  title: "UI/LinearGradient",
  component: LinearGradient,
  decorators: [
    CardContainer({
      margin: 10,
      padding: 10,
    }),
    BackgroundColor(),
  ],
} as ComponentMeta<typeof LinearGradient>

const template = withTemplate(LinearGradient, {
  colors: [{ color: "black" }, { color: "white" }],
})

export const Normal = template()

export const Horizontal = template(p => {
  p.direction = "horizontal"
})

export const CustomDegree = template(p => {
  p.direction = 45
})
