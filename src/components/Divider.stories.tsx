import { ComponentMeta } from "@storybook/react"
import { BackgroundColor, CardContainer } from "../../.storybook/decorators"
import { withTemplate } from "../../.storybook/utils"
import { Divider } from "./Divider"

export default {
  title: "UI/Divider",
  component: Divider,
} as ComponentMeta<typeof Divider>

const tpl = withTemplate(Divider, {})

export const Horizontal = tpl(props => {
  props.direction = "horizontal"
})
Horizontal.decorators = [
  CardContainer({
    padding: 10,
  }),
  BackgroundColor(),
]

export const Vertical = tpl(props => {
  props.direction = "vertical"
})
Vertical.decorators = [
  CardContainer({
    flexDirection: "row",
    padding: 10,
    alignSelf: "flex-start",
    width: "auto",
    height: 300,
  }),
  BackgroundColor(),
]
