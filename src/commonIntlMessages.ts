import { defineMessage } from "react-intl"

export const connectWallet$t = defineMessage({
  defaultMessage: "Connect Wallet",
  description: "Common/connect wallet button text",
})

export const wrongNetwork$t = defineMessage({
  defaultMessage: "Wrong network",
  description: "Common/connect wallet button text",
})

export const loading$t = defineMessage({
  defaultMessage: "Loading",
  description: "Common/approve transaction button text",
})

export const approve$t = defineMessage({
  defaultMessage: "Approve",
  description: "Common/approve transaction button text",
})

export const clear$t = defineMessage({
  defaultMessage: "Clear",
  description: "Common/clear form content button text",
})

export const cancel$t = defineMessage({
  defaultMessage: "Cancel",
  description: "Common/cancel button text",
})

export const close$t = defineMessage({
  defaultMessage: "Close",
  description: "Common/close button text",
})

export const accept$t = defineMessage({
  defaultMessage: "Accept",
  description: "Common/confirm button text",
})

export const confirm$t = defineMessage({
  defaultMessage: "Confirm",
  description: "Common/confirm button text",
})

export const submit$t = defineMessage({
  defaultMessage: "Submit",
  description: "Common/submit button text",
})

export const confirmDialogTitleText$t = defineMessage({
  defaultMessage: "Confirm",
  description: "Common/confirm dialog title text",
})

export const confirmButtonText$t = defineMessage({
  defaultMessage: "Confirm",
  description: "Common/confirm button text",
})

export const save$t = defineMessage({
  defaultMessage: "Save",
  description: "Common/save button text",
})

export const copied$t = defineMessage({
  defaultMessage: "Copied",
  description: 'Common/notification text "Copied"',
})

export const copy$t = defineMessage({
  defaultMessage: "Copy",
  description: "Common/copy button",
})

export const share$t = defineMessage({
  defaultMessage: "Share",
  description: "Common/share button",
})

export const noData$t = defineMessage({
  defaultMessage: "No data",
  description: "Common/empty data table placeholder text",
})
