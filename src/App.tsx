import { StacksProvider } from "@mobily/stacks"
import { ErrorBoundary } from "@sentry/react-native"
import { FallbackRender } from "@sentry/react/types/errorboundary"
import { StatusBar } from "expo-status-bar"
import { FC, Ref, Suspense } from "react"
import { ActivityIndicator } from "react-native"
import { SafeAreaProvider } from "react-native-safe-area-context"
import { filter, firstValueFrom, ReplaySubject } from "rxjs"
import { BlueButtonVariant } from "./components/Button/BlueButtonVariant"
import { DefaultButtonVariantProvider } from "./components/ButtonFramework/Button"
import { DialogProvider } from "./components/DialogProvider/DialogProvider"
import { SentryErrorBoundaryFallback } from "./components/FullPageErrorMessage/SentryErrorBoundaryFallback"
import { WiredMaintenanceMode } from "./components/FullPageErrorMessage/WiredMaintenanceMode"
import { WiredIntlProvider } from "./components/IntlProvider/WiredIntlProvider"
import { GlobalLoadingBoundary } from "./components/LoadingBoundary/GlobalLoadingBoundary"
import {
  MessageController,
  MessageProvider,
} from "./components/MessageProvider/MessageProvider"
import { wiredRecommendedMessageProviderProps } from "./components/MessageProvider/wiredRecommendedMessageProviderProps"
import { PopoverPortalProvider } from "./components/Popover/Popover"
import { SplashScreen } from "./components/SplashScreen"
import { BreakpointsProvider } from "./components/Themed/breakpoints"
import { ColorsProvider } from "./components/Themed/color"
import { defaultThemeBreakpoints } from "./components/Themed/defaults/defaultThemeBreakpoints"
import { defaultThemePalette } from "./components/Themed/defaults/defaultThemePalette"
import { defaultThemeSpacing } from "./components/Themed/defaults/defaultThemeSpacing"
import { SpacingProvider } from "./components/Themed/spacing"
import { WiredTransactionNotifierProvider } from "./components/TransactionNotifier/WiredTransactionNotifier"
import { WiredFontLoader } from "./components/WiredFontLoader"
import { NavigationContainer } from "./navigation/NavigationContainer"
import { TopLevelNavigator } from "./navigation/navigators/TopLevelNavigator"
import { AppEnvStoreProvider } from "./stores/AppEnvStore/useAppEnvStore"
import {
  AuthStoreProvider,
  WalletConnectProvider,
} from "./stores/AuthStore/useAuthStore"
import { ContractStoreProvider } from "./stores/contracts/useContractStore"
import { CurrencyStoreProvider } from "./stores/CurrencyStore/useCurrencyStore"
import { TradeInfoStoreProvider } from "./stores/TradeStore/useTradeInfoStore"
import { GetMessageControllerFn } from "./utils/enableWorkboxInBrowser.types"
import { FCC } from "./utils/reactHelpers/types"
import { useColorScheme } from "./utils/reactHelpers/useColorScheme"
import { DimensionsProvider } from "./utils/reactHelpers/useDimensions"
import { isNotNull } from "./utils/typeHelpers"

declare module "./components/Themed/spacing" {
  interface ThemeSpacing {
    spacing(spacing: keyof typeof defaultThemeSpacing): void
  }
}
declare module "./components/Themed/color" {
  interface ThemeColor {
    color(color: keyof typeof defaultThemePalette): void
  }
}
declare module "./components/Themed/breakpoints" {
  interface ThemeBreakpoints {
    breakpoint(
      breakpoint: BreakpointsKeysFromDefinition<typeof defaultThemeBreakpoints>,
    ): void
  }
}

const UIProvider: FCC<{
  messageControllerRef?: Ref<MessageController>
}> = props => (
  <WiredIntlProvider>
    <DimensionsProvider>
      <StacksProvider spacing={1} breakpoints={defaultThemeBreakpoints}>
        <BreakpointsProvider breakpoints={defaultThemeBreakpoints}>
          <ColorsProvider colors={defaultThemePalette}>
            <SpacingProvider spacings={defaultThemeSpacing}>
              <DefaultButtonVariantProvider ButtonVariant={BlueButtonVariant}>
                <PopoverPortalProvider>
                  <MessageProvider
                    {...wiredRecommendedMessageProviderProps}
                    messageControllerRef={props.messageControllerRef}
                  >
                    <DialogProvider>
                      <SafeAreaProvider>
                        <GlobalLoadingBoundary
                          loadingIndicator={<ActivityIndicator />}
                        >
                          <WiredTransactionNotifierProvider>
                            {props.children}
                          </WiredTransactionNotifierProvider>
                        </GlobalLoadingBoundary>
                        <StatusBar />
                      </SafeAreaProvider>
                    </DialogProvider>
                  </MessageProvider>
                </PopoverPortalProvider>
              </DefaultButtonVariantProvider>
            </SpacingProvider>
          </ColorsProvider>
        </BreakpointsProvider>
      </StacksProvider>
    </DimensionsProvider>
  </WiredIntlProvider>
)

const StoreProvider: FCC = props => (
  <AppEnvStoreProvider>
    <WalletConnectProvider>
      <AuthStoreProvider>
        <CurrencyStoreProvider>
          <ContractStoreProvider>
            <TradeInfoStoreProvider>
              <>{props.children}</>
            </TradeInfoStoreProvider>
          </ContractStoreProvider>
        </CurrencyStoreProvider>
      </AuthStoreProvider>
    </WalletConnectProvider>
  </AppEnvStoreProvider>
)

export const App: FC = () => {
  return (
    <ErrorBoundary fallback={fallbackRender}>
      <Suspense fallback={<SplashScreen />}>
        <WiredFontLoader />
        <StoreProvider>
          <NavigationContainer colorScheme={useColorScheme()}>
            <UIProvider
              messageControllerRef={ctrl => messageControllerSubject.next(ctrl)}
            >
              <WiredMaintenanceMode>
                <ErrorBoundary fallback={fallbackRender}>
                  <Suspense>
                    <TopLevelNavigator />
                  </Suspense>
                </ErrorBoundary>
              </WiredMaintenanceMode>
            </UIProvider>
          </NavigationContainer>
        </StoreProvider>
      </Suspense>
    </ErrorBoundary>
  )
}

const fallbackRender: FallbackRender = props => {
  return <SentryErrorBoundaryFallback style={{ flex: 1 }} {...props} />
}

const messageControllerSubject = new ReplaySubject<null | MessageController>()
export const getMessageController: GetMessageControllerFn = callbackOnce =>
  firstValueFrom(messageControllerSubject.pipe(filter(isNotNull))).then(
    callbackOnce,
  )
