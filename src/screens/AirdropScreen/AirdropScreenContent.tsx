import { FC, ReactNode } from "react"
import { defineMessage, useIntl } from "react-intl"
import { Image, StyleProp, View, ViewStyle } from "react-native"
import { MediaLinks } from "../../components/MediaLinks/MediaLinks"
import { Spensor } from "../../components/Spensor"
import { useColors } from "../../components/Themed/color"
import { useSpacing } from "../../components/Themed/spacing"
import { useSizeQuery } from "../../utils/reactHelpers/useSizeQuery"
import { readResource, SuspenseResource } from "../../utils/SuspenseResource"
import {
  PageContainer,
  PageTitleMainText,
} from "../components/pageLayoutPresets"
import { ComingSoonPanel } from "./components/ComingSoonPanel"
import { PanelGrid, PanelRow } from "./components/EventPanel"
import { MainnetContributorPanelProps } from "./components/MainnetContributorPanel/MainnetContributorPanel"
import { SummaryPanelProps } from "./components/SummaryPanel/SummaryPanel"
import { TestnetContributorPanelProps } from "./components/TestnetContributorPanel"
import { UGPPanelProps } from "./components/UGPPanel"

export const AirdropScreenContent: FC<{
  style?: StyleProp<ViewStyle>
  enableMainnetContributorPanel: SuspenseResource<boolean>
  summaryPanel: (renderProps: Pick<SummaryPanelProps, "style">) => ReactNode
  ugpPanel: (renderProps: Pick<UGPPanelProps, "style">) => ReactNode
  mainnetContributorPanel: (
    renderProps: Pick<MainnetContributorPanelProps, "style">,
  ) => ReactNode
  testnetContributorPanel: (
    renderProps: Pick<TestnetContributorPanelProps, "style">,
  ) => ReactNode
}> = props => {
  const { $t } = useIntl()

  const colors = useColors()
  const spacing = useSpacing()

  const sectionGap = spacing(5)
  const panelGap = spacing(2.5)

  const [stackPanelRowLayout = "row", onStackPanelRowLayout] = useSizeQuery({
    0: "col",
    800: "row",
  } as const)

  return (
    <PageContainer style={props.style}>
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <PageTitleMainText>
          {$t(
            defineMessage({
              defaultMessage: "Airdrop",
              description: "AirdropScreen/title",
            }),
          )}
        </PageTitleMainText>

        <MediaLinks
          tint={colors("blue-600")}
          links={{
            twitter: "https://twitter.com/intent/user?screen_name=UniwhaleEx",
            discord: "https://discord.gg/WK9P2MsH4w",
            telegram: "https://t.me/UniwhaleEx",
          }}
        />
      </View>

      {props.summaryPanel({ style: { marginTop: sectionGap } })}

      <Spensor>
        {() =>
          readResource(props.enableMainnetContributorPanel) &&
          props.mainnetContributorPanel({ style: { marginTop: sectionGap } })
        }
      </Spensor>

      <PanelGrid
        style={{ marginTop: spacing(2.5) }}
        gap={panelGap}
        onLayout={onStackPanelRowLayout}
      >
        {p => (
          <>
            <PanelRow layout={stackPanelRowLayout} gap={panelGap}>
              {p => (
                <>
                  {props.ugpPanel({
                    style: p.eachElementStyle,
                  })}
                  {props.testnetContributorPanel({
                    style: [p.eachElementStyle, p.tailElementStyle],
                  })}
                </>
              )}
            </PanelRow>

            <PanelRow
              style={p.tailElementStyle}
              layout={stackPanelRowLayout}
              gap={panelGap}
            >
              {p => (
                <>
                  <ComingSoonPanel
                    style={p.eachElementStyle}
                    titleText={$t(
                      defineMessage({
                        defaultMessage: "Share with Friends",
                        description:
                          "AirdropScreen/Share With Friends Panel/title",
                      }),
                    )}
                    renderIcon={p => (
                      <Image
                        source={require("./_/friend.png")}
                        style={p}
                        resizeMode={"contain"}
                      />
                    )}
                  />

                  <ComingSoonPanel
                    style={[p.eachElementStyle, p.tailElementStyle]}
                    titleText={$t(
                      defineMessage({
                        defaultMessage: "I am a DeFi Legend",
                        description: "AirdropScreen/DeFi Legend Panel/title",
                      }),
                    )}
                    renderIcon={p => (
                      <Image
                        source={require("./_/award.png")}
                        style={p}
                        resizeMode={"contain"}
                      />
                    )}
                  />
                </>
              )}
            </PanelRow>

            <Spensor>
              {() =>
                !readResource(props.enableMainnetContributorPanel) && (
                  <ComingSoonPanel
                    style={p.tailElementStyle}
                    titleText={$t(
                      defineMessage({
                        defaultMessage: "Mainnet Contributors",
                        description: "AirdropScreen/Contributor Panel/title",
                      }),
                    )}
                    renderIcon={p => (
                      <Image
                        source={require("./_/clock.png")}
                        style={p}
                        resizeMode={"contain"}
                      />
                    )}
                  />
                )
              }
            </Spensor>
          </>
        )}
      </PanelGrid>
    </PageContainer>
  )
}
