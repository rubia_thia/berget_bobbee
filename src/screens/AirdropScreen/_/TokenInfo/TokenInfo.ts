import { TokenInfo } from "../../../../utils/TokenInfo"

export const AirdropPoint: TokenInfo = {
  id: "AirdropPoint",
  displayName: "AirdropPoint",
  precision: 0,
  icon: require("./AirdropPoint.png"),
}
