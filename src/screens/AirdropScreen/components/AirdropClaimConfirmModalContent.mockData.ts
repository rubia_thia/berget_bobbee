import { OptionalizeEventListeners } from "../../../../.storybook/utils"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../utils/TokenInfoPresets/TokenInfoPresets"
import { AirdropClaimConfirmModalContentProps } from "./AirdropClaimConfirmModalContent"

export const airdropClaimConfirmModalContentProps: OptionalizeEventListeners<AirdropClaimConfirmModalContentProps> =
  {
    rewards: [
      {
        token: TokenInfoPresets.UNW,
        amount: BigNumber.from(1570),
      },
      {
        token: TokenInfoPresets.esUNW,
        amount: BigNumber.from(13.333333333333),
      },
    ],
  }
