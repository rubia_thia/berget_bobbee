import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import {
  StyleProp,
  Text,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import { connectWallet$t } from "../../../../commonIntlMessages"
import { BlueButtonVariant } from "../../../../components/Button/BlueButtonVariant"
import { useCompactButtonVariant } from "../../../../components/Button/useCompactButtonVariant"
import { SmartLoadableButton } from "../../../../components/ButtonFramework/LoadableButton"
import { CardBoxView } from "../../../../components/CardBox/CardBox"
import { Divider } from "../../../../components/Divider"
import { IconTokenCount } from "../../../../components/IconTokenCount"
import { InfoList } from "../../../../components/InfoList/InfoList"
import {
  DefaultInfoListItem,
  InfoListItem,
} from "../../../../components/InfoList/InfoListItem"
import {
  DefaultInfoListItemDetailText,
  InfoListItemDetail,
  InfoListItemDetailText,
} from "../../../../components/InfoList/InfoListItemDetail"
import {
  DefaultInfoListItemTitleText,
  InfoListItemTitle,
} from "../../../../components/InfoList/InfoListItemTitle"
import { InfoListProvider } from "../../../../components/InfoList/InfoListProvider"
import { SpensorR } from "../../../../components/Spensor"
import { TextNumber } from "../../../../components/TextNumber"
import { TokenCountAsCurrency } from "../../../../components/TextTokenCount"
import { useColors } from "../../../../components/Themed/color"
import { useSpacing } from "../../../../components/Themed/spacing"
import { TokenCount } from "../../../../components/TokenCount"
import { TokenName } from "../../../../components/TokenName"
import { Tooltip } from "../../../../components/Tooltip"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { useSizeQuery } from "../../../../utils/reactHelpers/useSizeQuery"
import { HrefLink } from "../../../../utils/reactNavigationHelpers/HrefLink"
import { SuspenseResource } from "../../../../utils/SuspenseResource"
import { TokenInfo } from "../../../../utils/TokenInfo"
import { TokenInfoPresets } from "../../../../utils/TokenInfoPresets/TokenInfoPresets"
import InfoIcon from "./assets/info.svg"

export interface SummaryPanelProps
  extends FirstPanelCoreProps,
    SecondPanelCoreProps {
  style?: StyleProp<ViewStyle>
}

export const SummaryPanel: FC<SummaryPanelProps> = props => {
  const spacing = useSpacing()
  const colors = useColors()

  const [layout = "column", onLayout] = useSizeQuery({
    0: "column",
    800: "row",
  } as const)

  const dividerMargin = spacing(4)

  return (
    <CardBoxView
      style={[
        props.style,
        {
          flexDirection: layout,
          alignItems: "stretch",
        },
      ]}
      padding={spacing(6)}
      onLayout={onLayout}
    >
      <InfoListProvider
        renderInfoListItem={p => (
          <DefaultInfoListItem
            {...p}
            style={[p.style, { alignSelf: "stretch", alignItems: "stretch" }]}
          />
        )}
        renderInfoListItemTitleText={p => (
          <DefaultInfoListItemTitleText
            {...p}
            style={[p.style, { color: "#64748B", fontSize: 14 }]}
          />
        )}
        renderInfoListItemDetailText={p => (
          <DefaultInfoListItemDetailText
            {...p}
            style={[
              p.style,
              {
                color: colors("gray-900"),
                fontSize: 30,
                fontWeight: "600",
              },
            ]}
          />
        )}
      >
        <FirstPart
          style={[layout === "row" && { flex: 1 }]}
          dividerMargin={dividerMargin}
          liquidityToken={props.liquidityToken}
          liquidityStages={props.liquidityStages}
          currentTotalLiquidity={props.currentTotalLiquidity}
          targetTotalLiquidity={props.targetTotalLiquidity}
          participantCount={props.participantCount}
        />

        {layout === "row" && (
          <Divider
            style={{ marginHorizontal: dividerMargin }}
            direction={"vertical"}
          />
        )}

        <SecondPart
          style={[layout === "row" ? { flex: 1 } : { marginTop: spacing(6) }]}
          dividerMargin={dividerMargin}
          tgeDetailsLink={props.tgeDetailsLink}
          remainingTotalAirdrop={props.remainingTotalAirdrop}
          onConnectWallet={props.onConnectWallet}
          remainingEsUNWAirdrop={props.remainingEsUNWAirdrop}
          remainingUNWAirdrop={props.remainingUNWAirdrop}
          esUNWAmountToClaim={props.esUNWAmountToClaim}
          unwAmountToClaim={props.unwAmountToClaim}
          onClaim={props.onClaim}
          onShowClaimHistory={props.onShowClaimHistory}
        />
      </InfoListProvider>
    </CardBoxView>
  )
}

interface SecondPanelCoreProps {
  tgeDetailsLink?: string

  remainingTotalAirdrop: SuspenseResource<BigNumber>
  remainingUNWAirdrop: SuspenseResource<BigNumber>
  remainingEsUNWAirdrop: SuspenseResource<BigNumber>

  unwAmountToClaim: SuspenseResource<BigNumber>
  esUNWAmountToClaim: SuspenseResource<BigNumber>

  onClaim: SuspenseResource<(() => Promise<void>) | undefined>
  onShowClaimHistory?: () => void

  /**
   * If provided, the button will be shown instead of the claim button.
   */
  onConnectWallet?: () => void
}
const SecondPart: FC<
  SecondPanelCoreProps & {
    style?: StyleProp<ViewStyle>
    dividerMargin?: number
  }
> = props => {
  const spacing = useSpacing()
  const { $t } = useIntl()

  const CompactButton = useCompactButtonVariant(BlueButtonVariant)
  return (
    <View style={[{ justifyContent: "space-between" }, props.style]}>
      <InfoList
        inheritRenderers={true}
        direction={"row"}
        listItemDirection={"column"}
      >
        <InfoListItem style={{ flex: 1 }}>
          <InfoListItemTitle>
            {$t(
              defineMessage({
                defaultMessage: "My Remaining Airdrop",
                description: "AirdropScreen/SummaryPanel/field title",
              }),
            )}
          </InfoListItemTitle>
          <View className="flex-row space-x-2.5 items-center">
            <InfoListItemDetail>
              <InfoListItemDetailText>
                <TokenCount
                  token={TokenInfoPresets.UNW}
                  count={props.remainingTotalAirdrop}
                />
              </InfoListItemDetailText>
            </InfoListItemDetail>
            <Tooltip
              content={
                <Text className="text-white">
                  {$t(
                    defineMessage({
                      defaultMessage:
                        "My Remaining Airdrop:{br}<strong>{unwToken}: {unwCount}{br}{esUnwToken}: {esUnwCount}</strong>",
                      description: "AirdropScreen/SummaryPanel",
                    }),
                    {
                      br: <Text>{"\n"}</Text>,
                      strong: children => (
                        <Text style={{ fontWeight: "700" }}>{children}</Text>
                      ),
                      unwToken: <TokenName token={TokenInfoPresets.UNW} />,
                      unwCount: (
                        <TokenCount
                          token={TokenInfoPresets.UNW}
                          count={props.remainingUNWAirdrop}
                        />
                      ),
                      esUnwToken: <TokenName token={TokenInfoPresets.esUNW} />,
                      esUnwCount: (
                        <TokenCount
                          token={TokenInfoPresets.esUNW}
                          count={props.remainingEsUNWAirdrop}
                        />
                      ),
                    },
                  )}
                </Text>
              }
            >
              <InfoIcon />
            </Tooltip>
          </View>
          {props.tgeDetailsLink != null && (
            <HrefLink href={props.tgeDetailsLink}>
              {props => (
                <TouchableOpacity className={"mt-auto self-start"} {...props}>
                  <Text className="text-blue-600 text-[12px]">
                    {$t(
                      defineMessage({
                        defaultMessage: "TGE Details >",
                        description: "AirdropScreen/SummaryPanel/SecondPart",
                      }),
                    )}
                  </Text>
                </TouchableOpacity>
              )}
            </HrefLink>
          )}
        </InfoListItem>

        <Divider
          style={{ marginHorizontal: props.dividerMargin ?? spacing(4) }}
          direction={"vertical"}
        />

        <InfoListItem style={{ flex: 1 }}>
          <InfoListItemTitle>
            {$t(
              defineMessage({
                defaultMessage: "Airdrop to Claim",
                description: "AirdropScreen/SummaryPanel/field title",
              }),
            )}
          </InfoListItemTitle>
          <InfoListItemDetail>
            <View className="flex-1">
              <IconTokenCount
                iconSize={24}
                token={TokenInfoPresets.UNW}
                count={props.unwAmountToClaim}
                renderText={p => (
                  <InfoListItemDetailText {...p} numberOfLines={1} />
                )}
              />

              <IconTokenCount
                iconSize={24}
                token={TokenInfoPresets.esUNW}
                count={props.esUNWAmountToClaim}
                renderText={p => (
                  <InfoListItemDetailText {...p} numberOfLines={1} />
                )}
              />
            </View>

            <View className="mt-3 flex-row justify-between items-center">
              {props.onConnectWallet != null ? (
                <SmartLoadableButton
                  onPress={props.onConnectWallet}
                  Variant={CompactButton}
                >
                  {$t(connectWallet$t)}
                </SmartLoadableButton>
              ) : (
                <SpensorR read={{ claim: props.onClaim }}>
                  {({ claim }) => (
                    <SmartLoadableButton
                      disabled={claim == null}
                      onPress={claim}
                      Variant={CompactButton}
                    >
                      {$t(
                        defineMessage({
                          defaultMessage: "Claim",
                          description:
                            "AirdropScreen/SummaryPanel/SecondPart/claim button text",
                        }),
                      )}
                    </SmartLoadableButton>
                  )}
                </SpensorR>
              )}

              {props.onShowClaimHistory != null && (
                <TouchableOpacity onPress={props.onShowClaimHistory}>
                  <Text className="text-blue-600 text-[12px] self-start">
                    {$t(
                      defineMessage({
                        defaultMessage: "History >",
                        description: "AirdropScreen/SummaryPanel/SecondPart",
                      }),
                    )}
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          </InfoListItemDetail>
        </InfoListItem>
      </InfoList>
    </View>
  )
}

interface FirstPanelCoreProps {
  liquidityToken: TokenInfo
  liquidityStages: SuspenseResource<readonly BigNumber[]>
  currentTotalLiquidity: SuspenseResource<BigNumber>
  targetTotalLiquidity: SuspenseResource<BigNumber>
  participantCount: SuspenseResource<number>
}

const FirstPart: FC<
  FirstPanelCoreProps & {
    style?: StyleProp<ViewStyle>
    dividerMargin?: number
  }
> = props => {
  const spacing = useSpacing()
  const { $t } = useIntl()

  return (
    <InfoList
      style={props.style}
      inheritRenderers={true}
      direction={"row"}
      listItemDirection={"column"}
    >
      <InfoListItem style={{ flex: 1 }}>
        <InfoListItemTitle>
          {$t(
            defineMessage({
              defaultMessage: "Current Liquidity",
              description: "AirdropScreen/SummaryPanel/field title",
            }),
          )}
        </InfoListItemTitle>
        <InfoListItemDetail>
          <InfoListItemDetailText>
            <TokenCountAsCurrency
              token={props.liquidityToken}
              count={props.currentTotalLiquidity}
            />
          </InfoListItemDetailText>
        </InfoListItemDetail>
      </InfoListItem>

      <Divider
        style={{ marginHorizontal: spacing(4) }}
        direction={"vertical"}
      />

      <InfoListItem style={{ flex: 1 }}>
        <InfoListItemTitle>
          {$t(
            defineMessage({
              defaultMessage: "Participants",
              description: "AirdropScreen/SummaryPanel/field title",
            }),
          )}
        </InfoListItemTitle>
        <InfoListItemDetail>
          <InfoListItemDetailText>
            <TextNumber number={props.participantCount} precision={0} />
          </InfoListItemDetailText>
        </InfoListItemDetail>
      </InfoListItem>
    </InfoList>
  )
}
