import { OptionalizeEventListeners } from "../../../../../.storybook/utils"
import { noop } from "../../../../utils/fnHelpers"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { pNoop } from "../../../../utils/promiseHelpers"
import { TokenInfoPresets } from "../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { SummaryPanelProps } from "./SummaryPanel"

export const summaryPanelProps: OptionalizeEventListeners<SummaryPanelProps> = {
  liquidityToken: TokenInfoPresets.UNW,
  liquidityStages: [BigNumber.from(1_000_000), BigNumber.from(5_000_000)],
  participantCount: 66666,
  currentTotalLiquidity: BigNumber.from(1_666_666),
  targetTotalLiquidity: BigNumber.from(5_000_000),
  remainingTotalAirdrop: BigNumber.from(1570),
  unwAmountToClaim: BigNumber.from(123),
  remainingUNWAirdrop: BigNumber.from(123),
  remainingEsUNWAirdrop: BigNumber.from(123),
  esUNWAmountToClaim: BigNumber.from(123),
  tgeDetailsLink: "https://www.google.com",
  onClaim: pNoop,
  onShowClaimHistory: noop,
}
