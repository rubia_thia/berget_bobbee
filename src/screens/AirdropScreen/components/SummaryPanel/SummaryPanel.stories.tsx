import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { noop } from "../../../../utils/fnHelpers"
import { suspenseResource } from "../../../../utils/SuspenseResource"
import { SummaryPanel } from "./SummaryPanel"
import { summaryPanelProps } from "./SummaryPanel.mockData"

export default {
  title: "Page/AirdropScreen/SummaryPanel",
  component: SummaryPanel,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof SummaryPanel>

const template = withTemplate(SummaryPanel, {
  style: {
    margin: 10,
  },
  ...summaryPanelProps,
  onConnectWallet: undefined,
})

export const Normal = template()

export const WithConnectWallet = template(p => {
  p.onConnectWallet = noop
  p.remainingTotalAirdrop = suspenseResource(() => {
    throw new Promise(noop)
  })
})

export const WithDisabledClaimButton = template(p => {
  p.onClaim = undefined
})
