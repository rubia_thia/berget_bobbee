import { sum } from "ramda"
import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import {
  ActivityIndicator,
  ScrollView,
  StyleProp,
  Text,
  TextInput,
  View,
  ViewStyle,
} from "react-native"
import { Path, Svg } from "react-native-svg"
import { connectWallet$t } from "../../../commonIntlMessages"
import { Button } from "../../../components/ButtonFramework/Button"
import {
  CardBoxModalContent,
  TitleBar,
} from "../../../components/CardBoxModalContent/CardBoxModalContent"
import { EmptyState } from "../../../components/EmptyState/EmptyState"
import { ScopedLoadingBoundary } from "../../../components/LoadingBoundary/ScopedLoadingBoundary"
import {
  PaginationInfo,
  PaginationPatch,
} from "../../../components/Pagination/Pagination"
import { SimplePagination } from "../../../components/Pagination/SimplePagination"
import { Spensor } from "../../../components/Spensor"
import {
  TableBodyRow,
  TableBodyText,
  TableHeaderRow,
  TableHeaderText,
} from "../../../components/StyledTable"
import { TextNumber } from "../../../components/TextNumber"
import { useColors } from "../../../components/Themed/color"
import { useSpacing } from "../../../components/Themed/spacing"
import { TokenCount } from "../../../components/TokenCount"
import { TokenName } from "../../../components/TokenName"
import { readResource, SuspenseResource } from "../../../utils/SuspenseResource"
import { TokenInfo } from "../../../utils/TokenInfo"
import { UGPClaimingRecord } from "./types"

export interface UGPAirdropClaimCheckingModalContentProps {
  style?: StyleProp<ViewStyle>

  token: TokenInfo
  nextAirdropBlockNumber: SuspenseResource<number>
  leftWeekCount: SuspenseResource<number>
  searchingNftNumber: SuspenseResource<null | number>
  onSearchingNftNumberChange: (nftNumber: null | number) => void

  records: SuspenseResource<UGPClaimingRecord[]>

  pagination: SuspenseResource<PaginationInfo>
  onPaginationChange: (patch: PaginationPatch) => void

  onConnectWallet?: () => void
  onDismiss: () => void
}

export const UGPAirdropClaimCheckingModalContent: FC<
  UGPAirdropClaimCheckingModalContentProps
> = props => {
  const spacing = useSpacing()
  const colors = useColors()
  const { $t } = useIntl()

  const colMinWidth = [160, 210, 210] as const
  const cellGap = spacing(2.5)

  const tableMinWidth = sum(colMinWidth) + cellGap * (colMinWidth.length - 1)

  return (
    <CardBoxModalContent
      style={props.style}
      padding={spacing(4)}
      onClose={props.onDismiss}
    >
      <TitleBar>
        {$t(
          defineMessage({
            defaultMessage: "Genesis Pass Airdrop Details",
            description:
              "AirdropScreen/UGPAirdropClaimCheckingModalContent/title",
          }),
        )}
      </TitleBar>

      <View
        style={{
          marginTop: spacing(4),
          flexDirection: "row",
          flexWrap: "wrap",
          alignItems: "center",
        }}
      >
        <View style={{ flexGrow: 1, flexShrink: 0, marginRight: spacing(2.5) }}>
          <Text style={{ fontSize: 16, color: colors("gray-900") }}>
            {$t(
              defineMessage({
                defaultMessage: "Next airdrop block #{blockNumber}",
                description: "AirdropScreen/UGP Airdrop Claim Checking Modal",
              }),
              {
                blockNumber: (
                  <TextNumber number={props.nextAirdropBlockNumber} />
                ),
              },
            )}
          </Text>
          <Text style={{ fontSize: 14, color: colors("gray-500") }}>
            {$t(
              defineMessage({
                defaultMessage: "Airdrop {weekCount} weeks left",
                description: "AirdropScreen/UGP Airdrop Claim Checking Modal",
              }),
              {
                weekCount: <TextNumber number={props.leftWeekCount} />,
              },
            )}
          </Text>
        </View>

        <View
          style={{
            marginVertical: spacing(3),
            padding: spacing(2.5),
            flexDirection: "row",
            alignItems: "center",
            borderWidth: 1,
            borderColor: colors("gray-300"),
            borderRadius: 6,
          }}
        >
          <Svg
            width="16"
            height="16"
            viewBox="0 0 16 16"
            fill={colors("gray-500")}
          >
            <Path d="M15.8727 14.4307L11.736 10.294C12.514 9.224 12.974 7.908 12.974 6.48667C12.974 2.91 10.064 0 6.48667 0C2.91 0 0 2.91 0 6.48667C0 10.064 2.91 12.9733 6.48667 12.9733C7.84267 12.9733 9.102 12.5553 10.1447 11.8413L14.3033 16L15.8727 14.4307ZM1.90267 6.48667C1.90267 3.95867 3.95933 1.902 6.48733 1.902C9.01533 1.902 11.072 3.95867 11.072 6.48667C11.072 9.01467 9.01533 11.0713 6.48733 11.0713C3.95867 11.0713 1.90267 9.01467 1.90267 6.48667Z" />
          </Svg>

          <Spensor>
            {() => (
              <TextInput
                style={{
                  marginLeft: spacing(2),
                  color: colors("gray-900"),
                  fontSize: 14,
                  fontWeight: "500",
                }}
                placeholder={$t(
                  defineMessage({
                    defaultMessage: "UGP Pass Number",
                    description:
                      "AirdropScreen/UGP Airdrop Claim Checking Modal/search input placeholder",
                  }),
                )}
                placeholderTextColor={colors("gray-400")}
                value={
                  readResource(props.searchingNftNumber) == null
                    ? ""
                    : String(readResource(props.searchingNftNumber))
                }
                onChangeText={text => {
                  let nftNumber: null | number = text ? Number(text) : null
                  if (Number.isNaN(nftNumber)) {
                    nftNumber = null
                  }
                  props.onSearchingNftNumberChange?.(nftNumber)
                }}
              />
            )}
          </Spensor>
        </View>
      </View>

      <ScrollView
        horizontal={true}
        style={{ marginTop: spacing(2.5) }}
        contentContainerStyle={{ flex: 1 }}
      >
        <View style={{ flex: 1, minWidth: tableMinWidth }}>
          <TableHeaderRow>
            <TableHeaderText style={{ flex: 1, minWidth: colMinWidth[0] }}>
              {$t(
                defineMessage({
                  defaultMessage: "Genesis Pass",
                  description:
                    "AirdropScreen/UGP Airdrop Claim Checking Modal/table header",
                }),
              )}
            </TableHeaderText>
            <TableHeaderText style={{ minWidth: colMinWidth[1] }}>
              {$t(
                defineMessage({
                  defaultMessage: "Claimable ({token})",
                  description:
                    "AirdropScreen/UGP Airdrop Claim Checking Modal/table header",
                }),
                { token: <TokenName token={props.token} /> },
              )}
            </TableHeaderText>
            <TableHeaderText style={{ minWidth: colMinWidth[2] }}>
              {$t(
                defineMessage({
                  defaultMessage: "Remaining ({token}) including claimable",
                  description:
                    "AirdropScreen/UGP Airdrop Claim Checking Modal/table header",
                }),
                { token: <TokenName token={props.token} /> },
              )}
            </TableHeaderText>
          </TableHeaderRow>

          <ScopedLoadingBoundary
            loadingIndicator={
              props.onConnectWallet != null ? (
                <Button className={"m-auto"} onPress={props.onConnectWallet}>
                  {$t(connectWallet$t)}
                </Button>
              ) : (
                <ActivityIndicator />
              )
            }
            placeholder={<View className={"min-h-[200px]"} />}
          >
            {() => (
              <>
                {readResource(props.records).length <= 0 && (
                  <View className={"min-h-[200px]"}>
                    <EmptyState className={"m-auto"} />
                  </View>
                )}
                {readResource(props.records).map((r, idx) => (
                  <TableBodyRow key={`nft-${r.nftNumber}-${idx}`}>
                    <TableBodyText
                      style={{ flex: 1, minWidth: colMinWidth[0] }}
                    >
                      #{r.nftNumber}
                    </TableBodyText>
                    <TableBodyText style={{ minWidth: colMinWidth[1] }}>
                      <TokenCount
                        token={props.token}
                        count={r.claimableEsUNW}
                      />
                    </TableBodyText>
                    <TableBodyText style={{ minWidth: colMinWidth[2] }}>
                      <TokenCount
                        token={props.token}
                        count={r.remainingEsUNW}
                      />
                    </TableBodyText>
                  </TableBodyRow>
                ))}
              </>
            )}
          </ScopedLoadingBoundary>
        </View>
      </ScrollView>

      <Spensor>
        {() => (
          <View style={{ marginTop: spacing(2.5) }}>
            <SimplePagination
              {...readResource(props.pagination)}
              onChange={props.onPaginationChange}
            />
          </View>
        )}
      </Spensor>
    </CardBoxModalContent>
  )
}
