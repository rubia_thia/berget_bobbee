import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { AirdropClaimConfirmModalContent } from "./AirdropClaimConfirmModalContent"
import { airdropClaimConfirmModalContentProps } from "./AirdropClaimConfirmModalContent.mockData"

export default {
  title: "Page/AirdropScreen/AirdropClaimConfirmModalContent",
  component: AirdropClaimConfirmModalContent,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof AirdropClaimConfirmModalContent>

const template = withTemplate(AirdropClaimConfirmModalContent, {
  style: {
    margin: 10,
  },
  ...airdropClaimConfirmModalContentProps,
})

export const Normal = template()
