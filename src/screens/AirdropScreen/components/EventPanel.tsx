import { FC, ReactNode } from "react"
import { StyleProp, View, ViewProps, ViewStyle } from "react-native"
import { CardBoxView } from "../../../components/CardBox/CardBox"
import { useSpacing } from "../../../components/Themed/spacing"
import { FCC } from "../../../utils/reactHelpers/types"
import { FlexStyle } from "../../../utils/styleHelpers/FlexStyle"
import { MarginStyle } from "../../../utils/styleHelpers/MarginStyle"

export interface EventPanelProps {
  style?: StyleProp<ViewStyle>
}

export const EventPanel: FCC<EventPanelProps> = props => {
  const spacing = useSpacing()

  return (
    <CardBoxView style={props.style} padding={spacing(6)}>
      {props.children}
    </CardBoxView>
  )
}

export const PanelGrid: FC<{
  style?: StyleProp<MarginStyle & FlexStyle>
  gap?: number
  onLayout?: ViewProps["onLayout"]
  children: (renderProps: { tailElementStyle: MarginStyle }) => ReactNode
}> = props => {
  return (
    <View style={props.style} onLayout={props.onLayout}>
      {props.children({
        tailElementStyle: { marginTop: props.gap },
      })}
    </View>
  )
}

export const PanelRow: FC<{
  style?: StyleProp<MarginStyle & FlexStyle>
  layout?: "row" | "col"
  gap?: number
  children: (renderProps: {
    layout: "row" | "col"
    eachElementStyle: FlexStyle
    tailElementStyle: MarginStyle
  }) => ReactNode
}> = props => {
  const { layout = "row" } = props

  return (
    <View
      style={[
        props.style,
        { flexDirection: props.layout === "row" ? "row" : "column" },
      ]}
    >
      {props.children({
        layout,
        eachElementStyle: {
          flex: 1,
          flexBasis: layout === "row" ? 0 : "auto",
        },
        tailElementStyle:
          layout === "row"
            ? { marginLeft: props.gap }
            : { marginTop: props.gap },
      })}
    </View>
  )
}
