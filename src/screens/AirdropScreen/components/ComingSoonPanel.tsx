import { FC, ReactNode } from "react"
import { defineMessage, useIntl } from "react-intl"
import { FlexStyle, StyleProp, Text } from "react-native"
import { CardBoxView } from "../../../components/CardBox/CardBox"
import { useColors } from "../../../components/Themed/color"
import { useSpacing } from "../../../components/Themed/spacing"
import { MarginStyle } from "../../../utils/styleHelpers/MarginStyle"
import { PanelHead } from "./panelComponents/PanelHead"
import { PanelInset } from "./panelComponents/PanelInset"

export interface ComingSoonPanelProps {
  style?: StyleProp<MarginStyle & FlexStyle>

  titleText?: ReactNode

  renderIcon: (renderProps: { width: number; height: number }) => ReactNode
}

export const ComingSoonPanel: FC<ComingSoonPanelProps> = props => {
  const spacing = useSpacing()
  const colors = useColors()

  const { $t } = useIntl()

  return (
    <CardBoxView style={props.style} padding={spacing(6)}>
      <PanelHead mainText={props.titleText} />

      <PanelInset
        style={{
          marginTop: spacing(5),
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        {props.renderIcon({ width: 56, height: 56 })}

        <Text
          style={{
            marginLeft: spacing(2.5),
            fontSize: 16,
            color: colors("blue-700"),
          }}
        >
          {$t(
            defineMessage({
              defaultMessage: "Coming soon...",
              description: "AirdropScreen/ComingSoonPanel",
            }),
          )}
        </Text>
      </PanelInset>
    </CardBoxView>
  )
}
