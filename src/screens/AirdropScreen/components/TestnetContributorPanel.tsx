import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, ViewStyle } from "react-native"
import { Spensor, SpensorR } from "../../../components/Spensor"
import { useSpacing } from "../../../components/Themed/spacing"
import { hasAny } from "../../../utils/arrayHelpers"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import {
  readResource,
  suspenseResource,
  SuspenseResource,
} from "../../../utils/SuspenseResource"
import { TokenInfo } from "../../../utils/TokenInfo"
import { EventPanel } from "./EventPanel"
import {
  EarnedSection,
  EarnedSectionProps,
} from "./panelComponents/EarnedSection"
import { PanelHead } from "./panelComponents/PanelHead"
import { PanelInset, PanelInsetDivider } from "./panelComponents/PanelInset"
import { AwardPart, ExpiredBadge, QuestItem } from "./panelComponents/QuestItem"
import { SignedTokenCount } from "./SignedTokenCount"

export interface TestnetContributorPanelProps {
  style?: StyleProp<ViewStyle>

  totalEarned: SuspenseResource<EarnedSectionProps["earned"]>

  bugReportAward: SuspenseResource<{
    token: TokenInfo
    amountPerIssue: BigNumber
    wonAmount: BigNumber
  }>

  poapHolderAward: SuspenseResource<{
    token: TokenInfo
    amountPerPoap: BigNumber
    isHolder: boolean
  }>
}

export const TestnetContributorPanel: FC<
  TestnetContributorPanelProps
> = props => {
  const { $t } = useIntl()

  const spacing = useSpacing()

  return (
    <EventPanel style={props.style}>
      <PanelHead
        mainText={$t(
          defineMessage({
            defaultMessage: "Testnet Contributors",
            description: "AirdropScreen/Testnet Contributor Panel/title",
          }),
        )}
        statusBadge={<ExpiredBadge />}
      />

      <SpensorR read={{ totalEarned: props.totalEarned }}>
        {({ totalEarned }) =>
          hasAny(totalEarned) && (
            <EarnedSection
              style={{ marginTop: spacing(2.5) }}
              earned={totalEarned}
            />
          )
        }
      </SpensorR>

      <PanelInset style={{ marginTop: spacing(2.5) }}>
        {({ gap }) => (
          <>
            <QuestItem
              titleText={$t(
                defineMessage({
                  defaultMessage: "Raise valid testnet bugs/suggestions",
                  description:
                    "AirdropScreen/Testnet Contributor Panel/quest title",
                }),
              )}
              descriptionText={$t(
                defineMessage({
                  defaultMessage:
                    "{tokenCount}/each issue reported verified by project team",
                  description:
                    "AirdropScreen/Testnet Contributor Panel/quest description",
                }),
                {
                  tokenCount: (
                    <SignedTokenCount
                      token={suspenseResource(
                        () => readResource(props.bugReportAward).token,
                      )}
                      count={suspenseResource(
                        () => readResource(props.bugReportAward).amountPerIssue,
                      )}
                    />
                  ),
                },
              )}
              actionArea={
                <Spensor>
                  {() => {
                    const award = readResource(props.bugReportAward)
                    if (BigNumber.isZero(award.wonAmount)) return null
                    return (
                      <AwardPart
                        token={award.token}
                        contentText={
                          <Spensor fallback={"-"}>
                            {() => (
                              <SignedTokenCount
                                token={award.token}
                                count={award.wonAmount}
                              />
                            )}
                          </Spensor>
                        }
                      />
                    )
                  }}
                </Spensor>
              }
            />

            <PanelInsetDivider style={{ marginTop: gap }} />

            <QuestItem
              style={{ marginTop: gap }}
              titleText={$t(
                defineMessage({
                  defaultMessage: "Testnet POAP Holders",
                  description:
                    "AirdropScreen/Testnet Contributor Panel/quest title",
                }),
              )}
              descriptionText={$t(
                defineMessage({
                  defaultMessage: "{tokenCount}/POAP Holder",
                  description:
                    "AirdropScreen/Testnet Contributor Panel/quest description",
                }),
                {
                  tokenCount: (
                    <SignedTokenCount
                      token={suspenseResource(
                        () => readResource(props.poapHolderAward).token,
                      )}
                      count={suspenseResource(
                        () => readResource(props.poapHolderAward).amountPerPoap,
                      )}
                    />
                  ),
                },
              )}
              actionArea={
                <Spensor>
                  {() => {
                    const award = readResource(props.poapHolderAward)
                    if (!award.isHolder) return null
                    return (
                      <AwardPart
                        token={award.token}
                        contentText={
                          <Spensor fallback={"-"}>
                            {() => (
                              <SignedTokenCount
                                token={award.token}
                                count={award.amountPerPoap}
                              />
                            )}
                          </Spensor>
                        }
                      />
                    )
                  }}
                </Spensor>
              }
            />
          </>
        )}
      </PanelInset>
    </EventPanel>
  )
}
