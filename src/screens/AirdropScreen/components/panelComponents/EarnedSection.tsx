import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, Text, View, ViewStyle } from "react-native"
import { useColors } from "../../../../components/Themed/color"
import { useSpacing } from "../../../../components/Themed/spacing"
import { TokenCount } from "../../../../components/TokenCount"
import { TokenIcon } from "../../../../components/TokenIcon"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { readResource } from "../../../../utils/SuspenseResource"
import { TokenInfo } from "../../../../utils/TokenInfo"
import { OneOrMore } from "../../../../utils/types"

export interface EarnedItem {
  token: TokenInfo
  amount?: BigNumber
}
export interface EarnedSectionProps {
  style?: StyleProp<ViewStyle>
  earned: OneOrMore<EarnedItem>
}

export const EarnedSection: FC<EarnedSectionProps> = props => {
  const spacing = useSpacing()
  const colors = useColors()
  const { $t } = useIntl()

  return (
    <View style={[props.style, { flexDirection: "row", alignItems: "center" }]}>
      <Text
        style={{
          fontSize: 12,
          color: colors("gray-900"),
        }}
      >
        {$t(
          defineMessage({
            defaultMessage: "Your earned",
            description: "AirdropScreen/Earned Module/title",
          }),
        )}
      </Text>
      {props.earned.map((e, idx) => {
        const amount = readResource(e.amount)
        return (
          <View
            key={idx}
            style={{
              marginLeft: spacing(2.5),
              flexDirection: "row",
              alignItems: "center",
              borderRadius: 9999,
              paddingVertical: spacing(2),
              paddingHorizontal: spacing(3),
              backgroundColor: colors("blue-200"),
            }}
          >
            <TokenIcon token={e.token} size={32} />
            <Text
              style={{
                marginLeft: spacing(2.5),
                fontSize: 18,
                fontWeight: "bold",
                color: colors("blue-600"),
              }}
            >
              {amount == null ? (
                "-"
              ) : (
                <TokenCount token={e.token} count={amount} />
              )}
            </Text>
          </View>
        )
      })}
    </View>
  )
}
