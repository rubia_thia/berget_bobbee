import { FC, ReactNode } from "react"
import { StyleProp, Text, View, ViewStyle } from "react-native"
import { useColors } from "../../../../components/Themed/color"
import { useSpacing } from "../../../../components/Themed/spacing"

export const PanelHead: FC<{
  style?: StyleProp<ViewStyle>
  mainText: ReactNode
  secondaryText?: ReactNode
  statusBadge?: ReactNode
}> = props => {
  const colors = useColors()
  const spacing = useSpacing()

  return (
    <View style={[props.style]}>
      <View style={{ flexDirection: "row", alignItems: "center" }}>
        <Text
          style={{
            marginRight: spacing(2.5),
            fontSize: 20,
            fontWeight: "bold",
            color: colors("gray-900"),
          }}
        >
          {props.mainText}
        </Text>

        {props.statusBadge}
      </View>

      {props.secondaryText != null && (
        <Text
          style={{
            marginTop: spacing(1),
            fontSize: 12,
            color: colors("gray-400"),
          }}
        >
          {props.secondaryText}
        </Text>
      )}
    </View>
  )
}
