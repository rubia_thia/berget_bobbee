import { FC, ReactNode } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, Text, View, ViewStyle } from "react-native"
import { Path, Rect, Svg } from "react-native-svg"
import { BlueButtonVariant } from "../../../../components/Button/BlueButtonVariant"
import { WhiteOutlineButtonVariant } from "../../../../components/Button/WhiteOutlineButtonVariant"
import { ButtonVariantProps } from "../../../../components/ButtonFramework/_/ButtonVariant"
import { Spensor } from "../../../../components/Spensor"
import { useColors } from "../../../../components/Themed/color"
import { useSpacing } from "../../../../components/Themed/spacing"
import { TokenIcon } from "../../../../components/TokenIcon"
import { SuspenseResource } from "../../../../utils/SuspenseResource"
import { TokenInfo } from "../../../../utils/TokenInfo"
import { checkNever } from "../../../../utils/typeHelpers"
import {
  GrayBadge,
  GreenBadge,
  IndigoBadge,
} from "../../../TradeScreen/components/Badge"

export interface QuestItemProps {
  style?: StyleProp<ViewStyle>
  titleText: ReactNode
  descriptionText: ReactNode
  actionArea?: ReactNode
}

export const QuestItem: FC<QuestItemProps> = props => {
  const spacing = useSpacing()
  const colors = useColors()

  const gap = spacing(2.5)

  return (
    <View style={[props.style, { flexDirection: "row" }]}>
      <Svg
        style={{ marginTop: 2 }}
        width="24"
        height="24"
        viewBox="0 0 24 24"
        fill="black"
      >
        <Path d="M22 2V24H2V2H5C6.23 2 7.181 0.916 8 0H16C16.82 0.916 17.771 2 19 2H22ZM11 3C11 3.552 11.448 4 12 4C12.553 4 13 3.552 13 3C13 2.448 12.553 2 12 2C11.448 2 11 2.448 11 3ZM20 4H16L14 6H10.103L8 4H4V22H20V4Z" />
        <Rect x="6" y="9" width="12" height="1.5" />
        <Rect x="6" y="12.75" width="12" height="1.5" />
        <Rect x="6" y="16.5" width="12" height="1.5" />
      </Svg>

      <View style={{ marginHorizontal: gap, flex: 1 }}>
        <Text
          style={{
            fontSize: 14,
            fontWeight: "700",
            color: colors("blue-600"),
          }}
        >
          {props.titleText}
        </Text>

        <Text
          style={{
            fontSize: 12,
            fontWeight: "500",
            color: colors("gray-500"),
          }}
        >
          {props.descriptionText}
        </Text>
      </View>

      {props.actionArea}
    </View>
  )
}

export const ActionButtonVariant: FC<ButtonVariantProps> = props => {
  const spacing = useSpacing()

  if (props.disabled) {
    return (
      <WhiteOutlineButtonVariant
        borderRadius={9999}
        padding={{
          paddingHorizontal: spacing(5),
          paddingVertical: spacing(1.5),
        }}
        {...props}
      />
    )
  } else {
    return (
      <BlueButtonVariant
        borderRadius={9999}
        padding={{
          paddingHorizontal: spacing(5),
          paddingVertical: spacing(1.5),
        }}
        {...props}
      />
    )
  }
}

export type StatusBadgeStatus = "upcoming" | "live" | "finished"
export const StatusBadge: FC<{
  status: StatusBadgeStatus
}> = props => {
  switch (props.status) {
    case "upcoming":
      return <ComingSoonBadge />
    case "live":
      return <ProcessingBadge />
    case "finished":
      return <ExpiredBadge />
    default:
      checkNever(props.status)
      return <ComingSoonBadge />
  }
}

export const ComingSoonBadge: FC = () => {
  const { $t } = useIntl()
  return (
    <IndigoBadge
      text={$t(
        defineMessage({
          defaultMessage: "Coming soon",
          description: "AirdropScreen/Quest Item/status",
        }),
      )}
    />
  )
}

export const ProcessingBadge: FC = () => {
  const { $t } = useIntl()
  return (
    <GreenBadge
      text={$t(
        defineMessage({
          defaultMessage: "Live",
          description: "AirdropScreen/Quest Item/status",
        }),
      )}
    />
  )
}

export const ExpiredBadge: FC = () => {
  const { $t } = useIntl()
  return (
    <GrayBadge
      text={$t(
        defineMessage({
          defaultMessage: "Completed",
          description: "AirdropScreen/Quest Item/status",
        }),
      )}
    />
  )
}

export const AwardPart: FC<{
  style?: StyleProp<ViewStyle>
  token: SuspenseResource<TokenInfo>
  contentText: ReactNode
}> = props => {
  const colors = useColors()
  const spacing = useSpacing()

  return (
    <View style={[props.style, { flexDirection: "row", alignItems: "center" }]}>
      <Spensor>
        {() => (
          <>
            <TokenIcon token={props.token} size={24} />
            <Text
              style={{
                fontSize: 20,
                fontWeight: "600",
                color: colors("blue-600"),
                marginLeft: spacing(1),
              }}
            >
              {props.contentText}
            </Text>
          </>
        )}
      </Spensor>
    </View>
  )
}
