import { FC, ReactNode } from "react"
import { StyleProp, Text, View, ViewStyle } from "react-native"
import { CardHighlight } from "../../../../components/CardBox/CardHighlight"
import { Divider, DividerProps } from "../../../../components/Divider"
import { InfoList } from "../../../../components/InfoList/InfoList"
import { DefaultInfoListItem } from "../../../../components/InfoList/InfoListItem"
import { InfoListItemDetailTextStyleProvider } from "../../../../components/InfoList/InfoListItemDetail"
import { InfoListItemTitleTextStyleProvider } from "../../../../components/InfoList/InfoListItemTitle"
import { useColors } from "../../../../components/Themed/color"
import { useSpacing } from "../../../../components/Themed/spacing"
import { renderChildren } from "../../../../utils/reactHelpers/childrenHelpers"
import { FCC } from "../../../../utils/reactHelpers/types"
import {
  PaddingStyle,
  useNormalizePaddingStyle,
} from "../../../../utils/styleHelpers/PaddingStyle"

export const PanelInset: FC<{
  style?: StyleProp<ViewStyle>
  padding?: number | PaddingStyle
  children: ReactNode | ((renderProps: { gap: number }) => ReactNode)
}> = props => {
  const spacing = useSpacing()

  const paddingStyle = useNormalizePaddingStyle(props.padding)

  const gap = spacing(2.5)

  return (
    <CardHighlight
      style={props.style}
      padding={{
        paddingTop: paddingStyle.paddingTop ?? spacing(2.5),
        paddingBottom: paddingStyle.paddingBottom ?? spacing(2.5),
        paddingLeft: paddingStyle.paddingLeft ?? spacing(3),
        paddingRight: paddingStyle.paddingRight ?? spacing(3),
      }}
    >
      {renderChildren(props.children, { gap })}
    </CardHighlight>
  )
}

export const PanelInsetDivider: FC<DividerProps> = props => {
  return <Divider {...props} />
}

export const PanelInsetBlock: FCC<{
  style?: StyleProp<ViewStyle>
  padding?: number
}> = props => {
  const spacing = useSpacing()
  const colors = useColors()
  return (
    <View
      style={[
        {
          padding: props.padding ?? spacing(2.5),
          borderWidth: 1,
          borderColor: colors("gray-200"),
          borderRadius: 4,
          backgroundColor: colors("white"),
        },
        props.style,
      ]}
    >
      {props.children}
    </View>
  )
}

export const PanelInsetBlockInfoListDivider: FC<DividerProps> = props => {
  const spacing = useSpacing()
  return <Divider direction={"vertical"} padding={spacing(2.5)} {...props} />
}

export const PanelInsetBlockInfoList: FCC<{
  style?: StyleProp<ViewStyle>
}> = props => {
  const colors = useColors()
  return (
    <InfoList
      direction={"row"}
      listItemDirection={"column"}
      renderInfoListItem={p => (
        <DefaultInfoListItem {...p} style={[p.style, { flex: 1 }]} />
      )}
    >
      <InfoListItemTitleTextStyleProvider
        style={{
          fontSize: 14,
          color: colors("gray-900"),
        }}
      >
        <InfoListItemDetailTextStyleProvider
          style={{
            fontSize: 18,
            fontWeight: "600",
            color: colors("gray-900"),
          }}
        >
          {props.children}
        </InfoListItemDetailTextStyleProvider>
      </InfoListItemTitleTextStyleProvider>
    </InfoList>
  )
}

export const PlaceholderPanelInset: FC<{
  style?: StyleProp<ViewStyle>
  placeholderText: ReactNode
}> = props => {
  const spacing = useSpacing()
  const colors = useColors()

  return (
    <PanelInset
      style={[{ alignItems: "center" }, props.style]}
      padding={spacing(6)}
    >
      <Text
        style={{
          textAlign: "center",
          maxWidth: 340,
          fontSize: 12,
          color: colors("gray-500"),
        }}
      >
        {props.placeholderText}
      </Text>
    </PanelInset>
  )
}
