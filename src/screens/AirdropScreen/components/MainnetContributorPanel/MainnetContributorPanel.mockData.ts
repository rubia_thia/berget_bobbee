import { createElement } from "react"
import { OptionalizeEventListeners } from "../../../../../.storybook/utils"
import { LiquidityProvidersQuests } from "./LiquidityProvidersQuests"
import { liquidityProvidersQuestsCoreProps } from "./LiquidityProvidersQuests.mockData"
import { MainnetContributorPanelProps } from "./MainnetContributorPanel"
import { TradersQuests } from "./TradersQuests"
import { tradersQuestsCoreProps } from "./TradersQuests.mockData"

export const mainnetContributorPanelProps: OptionalizeEventListeners<MainnetContributorPanelProps> =
  {
    renderLiquidityProvidersQuests: p =>
      createElement(LiquidityProvidersQuests, {
        ...p,
        ...liquidityProvidersQuestsCoreProps,
      }),
    renderTradersQuests: p =>
      createElement(TradersQuests, {
        ...p,
        ...tradersQuestsCoreProps,
      }),
  }
