import { OptionalizeEventListeners } from "../../../../../.storybook/utils"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { AirdropPoint } from "../../_/TokenInfo/TokenInfo"
import { TradersQuestsCoreProps } from "./TradersQuests"

export const tradersQuestsCoreProps: OptionalizeEventListeners<TradersQuestsCoreProps> =
  {
    earnedAsTraders: [{ token: AirdropPoint }],
    stage1Status: "live",
    stage2Status: "live",
    stage1MyTradeVolume: BigNumber.from(10000),
    stage1TotalTradeVolume: BigNumber.from(100_000),
    stage1TradeVolumeTarget: BigNumber.from(50_000_000),
    stage1SnapshotBlockHeight: 123,
    stage1UNWAwardCount: BigNumber.from(5),
    stage1TotalUNWAwardCount: BigNumber.from(1_000_000),
    stage2MyTradeVolume: BigNumber.from(10000),
    stage2TotalTradeVolume: BigNumber.from(100_000),
    stage2TradeVolumeTarget: BigNumber.from(50_000_000),
    stage2SnapshotBlockHeight: 123,
    stage2UNWAwardCount: BigNumber.from(5),
    stage2TotalUNWAwardCount: BigNumber.from(1_000_000),
  }
