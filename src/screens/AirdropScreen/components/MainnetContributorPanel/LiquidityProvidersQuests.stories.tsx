import { ComponentMeta } from "@storybook/react"
import {
  BackgroundColor,
  CardContainer,
} from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { DELAYED } from "../../../../utils/SuspenseResource"
import { LiquidityProvidersQuests } from "./LiquidityProvidersQuests"
import { liquidityProvidersQuestsCoreProps } from "./LiquidityProvidersQuests.mockData"

export default {
  title: "Page/AirdropScreen/MainnetContributorPanel/LiquidityProvidersQuests",
  component: LiquidityProvidersQuests,
  decorators: [CardContainer(), BackgroundColor()],
} as ComponentMeta<typeof LiquidityProvidersQuests>

const template = withTemplate(LiquidityProvidersQuests, {
  style: {
    margin: 10,
  },
  ...liquidityProvidersQuestsCoreProps,
})

export const Normal = template()

export const Stage2Upcoming = template(p => {
  p.stage2Status = "upcoming"
})

export const DataLoading = template(p => {
  Object.assign(p, {
    earnedAsLiquidityProviders: DELAYED(),
    stage1LiquidityTarget: DELAYED(),
    stage1MyULPCount: DELAYED(),
    stage1UNWAwardCount: DELAYED(),
    stage1SnapshotBlockHeight: DELAYED(),
    stage1LoyaltyPhaseMyULPCount: DELAYED(),
    stage1LoyaltyPhaseUNWAwardCount: DELAYED(),
    stage1LoyaltyPhaseSnapshotBlockHeight: DELAYED(),
    stage2LiquidityTarget: DELAYED(),
    stage2MyULPCount: DELAYED(),
    stage2UNWAwardCount: DELAYED(),
    stage2SnapshotBlockHeight: DELAYED(),
  })
})
