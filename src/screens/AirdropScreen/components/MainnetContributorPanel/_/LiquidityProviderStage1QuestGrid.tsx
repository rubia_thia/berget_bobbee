import { FC, ReactNode } from "react"
import { defineMessage, useIntl } from "react-intl"
import { FlexStyle, StyleProp } from "react-native"
import { InfoListItem } from "../../../../../components/InfoList/InfoListItem"
import {
  InfoListItemDetail,
  InfoListItemDetailText,
} from "../../../../../components/InfoList/InfoListItemDetail"
import { InfoListItemTitle } from "../../../../../components/InfoList/InfoListItemTitle"
import { Spensor } from "../../../../../components/Spensor"
import {
  TextTokenCount,
  TokenCountAsCurrency,
} from "../../../../../components/TextTokenCount"
import { useSpacing } from "../../../../../components/Themed/spacing"
import { TokenCount, TokenPresets } from "../../../../../components/TokenCount"
import { FormattedMessageOutsideText } from "../../../../../utils/intlHelpers"
import { BigNumber } from "../../../../../utils/numberHelpers/BigNumber"
import { useSizeQuery } from "../../../../../utils/reactHelpers/useSizeQuery"
import { MarginStyle } from "../../../../../utils/styleHelpers/MarginStyle"
import {
  readResource,
  SuspenseResource,
} from "../../../../../utils/SuspenseResource"
import { TokenInfoPresets } from "../../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { AirdropPoint } from "../../../_/TokenInfo/TokenInfo"
import { PanelGrid, PanelRow } from "../../EventPanel"
import {
  PanelInset,
  PanelInsetBlock,
  PanelInsetBlockInfoList,
  PanelInsetBlockInfoListDivider,
} from "../../panelComponents/PanelInset"
import { AwardPart, QuestItem } from "../../panelComponents/QuestItem"
import { SuperStrongText } from "../../SuperStrongText"
import { RuleItem, RuleTitle } from "./ruleTextComponents"

export interface LiquidityProviderStage1QuestGridCoreProps {
  stage1LiquidityTarget: SuspenseResource<BigNumber>
  stage1MyULPCount: SuspenseResource<BigNumber>
  stage1SnapshotBlockHeight: SuspenseResource<number>
  stage1UNWAwardCount: SuspenseResource<BigNumber>
  stage1LoyaltyPhaseMyULPCount: SuspenseResource<BigNumber>
  stage1LoyaltyPhaseSnapshotBlockHeight: SuspenseResource<number>
  stage1LoyaltyPhaseUNWAwardCount: SuspenseResource<BigNumber>
  stage1LoyaltyPhaseIsQualified: SuspenseResource<boolean>
}

export const LiquidityProviderStage1QuestGrid: FC<
  LiquidityProviderStage1QuestGridCoreProps & {
    style?: StyleProp<MarginStyle & FlexStyle>
  }
> = props => {
  const spacing = useSpacing()
  const { $t } = useIntl()

  const [stackPanelRowLayout = "row", onStackPanelRowLayout] = useSizeQuery({
    0: "col",
    800: "row",
  } as const)

  const panelGap = spacing(2.5)

  return (
    <PanelGrid
      style={props.style}
      gap={panelGap}
      onLayout={onStackPanelRowLayout}
    >
      {p => (
        <>
          <PanelRow layout={stackPanelRowLayout} gap={panelGap}>
            {p => (
              <>
                <PanelInset style={p.eachElementStyle}>
                  <QuestItem
                    titleText={$t(
                      defineMessage({
                        defaultMessage: "Liquidity Bootstrapping",
                        description:
                          "AirdropScreen/Mainnet Contributors Panel/Liquidity Providers/quest title",
                      }),
                    )}
                    descriptionText={$t(
                      defineMessage({
                        defaultMessage:
                          "Provide Liquidity of the first {liquidityCount}",
                        description:
                          "AirdropScreen/Mainnet Contributors Panel/Liquidity Providers/quest description",
                      }),
                      {
                        liquidityCount: (
                          <TokenCountAsCurrency
                            token={TokenPresets.USD}
                            count={props.stage1LiquidityTarget}
                          />
                        ),
                      },
                    )}
                    actionArea={
                      <AwardPart
                        token={AirdropPoint}
                        contentText={
                          <TokenCount
                            token={AirdropPoint}
                            count={props.stage1UNWAwardCount}
                          />
                        }
                      />
                    }
                  />

                  <PanelInsetBlock style={{ marginTop: spacing(2.5) }}>
                    <PanelInsetBlockInfoList>
                      <InfoListItem>
                        <InfoListItemTitle>
                          {$t(
                            defineMessage({
                              defaultMessage: "Liquidity",
                              description:
                                "AirdropScreen/Mainnet Contributors Panel/Liquidity Providers/detail field title",
                            }),
                          )}
                        </InfoListItemTitle>

                        <InfoListItemDetail>
                          <InfoListItemDetailText>
                            <TextTokenCount
                              token={TokenInfoPresets.ULP}
                              count={props.stage1MyULPCount}
                            />
                          </InfoListItemDetailText>
                        </InfoListItemDetail>
                      </InfoListItem>

                      <PanelInsetBlockInfoListDivider />

                      <InfoListItem>
                        <InfoListItemTitle>
                          {$t(
                            defineMessage({
                              defaultMessage: "Snapshot Block",
                              description:
                                "AirdropScreen/Mainnet Contributors Panel/Liquidity Providers/detail field title",
                            }),
                          )}
                        </InfoListItemTitle>
                        <InfoListItemDetail>
                          <InfoListItemDetailText>
                            <Spensor fallback={"???"}>
                              {() => (
                                <>
                                  {readResource(
                                    props.stage1SnapshotBlockHeight,
                                  )}
                                </>
                              )}
                            </Spensor>
                          </InfoListItemDetailText>
                        </InfoListItemDetail>
                      </InfoListItem>
                    </PanelInsetBlockInfoList>
                  </PanelInsetBlock>

                  <PanelInsetBlock style={{ marginTop: spacing(2.5) }}>
                    <FormattedMessageOutsideText>
                      {$t<ReactNode>(
                        defineMessage({
                          description:
                            "AirdropScreen/Mainnet Contributors Panel/Liquidity Providers/stage 1 rule",
                          defaultMessage: `
<ruleTitle>Criteria:</ruleTitle>
<ruleItem>Among the first $1,000,000 of the protocol liquidity.</ruleItem>
<ruleItem>Snapshot will be taken randomly once the $1,000,000 liquidity target is reached.</ruleItem>
<ruleItem>750,000 <superStrong>UNW</superStrong> will be shared based on the snapshot results.</ruleItem>
                      `,
                        }),
                        {
                          ruleTitle: contents => (
                            <RuleTitle>{contents}</RuleTitle>
                          ),
                          ruleItem: contents => <RuleItem>{contents}</RuleItem>,
                          superStrong: contents => (
                            <SuperStrongText>{contents}</SuperStrongText>
                          ),
                        },
                      )}
                    </FormattedMessageOutsideText>
                  </PanelInsetBlock>
                </PanelInset>

                <PanelInset style={[p.eachElementStyle, p.tailElementStyle]}>
                  <QuestItem
                    titleText={$t(
                      defineMessage({
                        defaultMessage: "Liquidity Loyalty",
                        description:
                          "AirdropScreen/Mainnet Contributors Panel/Liquidity Providers/quest title",
                      }),
                    )}
                    descriptionText={$t(
                      defineMessage({
                        defaultMessage: "Hold your liquidity until the TGE.",
                        description:
                          "AirdropScreen/Mainnet Contributors Panel/Liquidity Providers/quest description",
                      }),
                    )}
                    actionArea={
                      <AwardPart
                        token={AirdropPoint}
                        contentText={
                          <TokenCount
                            token={AirdropPoint}
                            count={props.stage1LoyaltyPhaseUNWAwardCount}
                          />
                        }
                      />
                    }
                  />

                  <PanelInsetBlock style={{ marginTop: spacing(2.5) }}>
                    <PanelInsetBlockInfoList>
                      <InfoListItem>
                        <InfoListItemTitle>
                          {$t(
                            defineMessage({
                              defaultMessage: "Liquidity",
                              description:
                                "AirdropScreen/Mainnet Contributors Panel/Liquidity Providers/detail field title",
                            }),
                          )}
                        </InfoListItemTitle>

                        <InfoListItemDetail>
                          <InfoListItemDetailText>
                            <TextTokenCount
                              token={TokenInfoPresets.ULP}
                              count={props.stage1LoyaltyPhaseMyULPCount}
                            />
                          </InfoListItemDetailText>
                        </InfoListItemDetail>
                      </InfoListItem>

                      <PanelInsetBlockInfoListDivider />

                      <InfoListItem>
                        <InfoListItemTitle>
                          {$t(
                            defineMessage({
                              defaultMessage: "Snapshot Block",
                              description:
                                "AirdropScreen/Mainnet Contributors Panel/Liquidity Providers/detail field title",
                            }),
                          )}
                        </InfoListItemTitle>
                        <InfoListItemDetail>
                          <InfoListItemDetailText>
                            <Spensor fallback={"???"}>
                              {() => (
                                <>
                                  {readResource(
                                    props.stage1LoyaltyPhaseSnapshotBlockHeight,
                                  )}
                                </>
                              )}
                            </Spensor>
                          </InfoListItemDetailText>
                        </InfoListItemDetail>
                      </InfoListItem>
                    </PanelInsetBlockInfoList>
                  </PanelInsetBlock>

                  <PanelInsetBlock style={{ marginTop: spacing(2.5) }}>
                    <FormattedMessageOutsideText>
                      {$t<ReactNode>(
                        defineMessage({
                          description:
                            "AirdropScreen/Mainnet Contributors Panel/Liquidity Providers/stage 1 loyalty rule",
                          defaultMessage: `
<ruleTitle>Criteria:</ruleTitle>
<ruleItem>Among the first $1,000,000 of the protocol liquidity.</ruleItem>
<ruleItem><superStrong>YOU CANNOT REDEEM YOUR LIQUIDITY UNTIL THE TGE.</superStrong></ruleItem>
<ruleItem>Snapshot will be taken randomly upon the TGE milestone.</ruleItem>
<ruleItem>1,500,000 <superStrong>UNW</superStrong> will be shared based on the snapshot results.</ruleItem>
                      `,
                        }),
                        {
                          ruleTitle: contents => (
                            <RuleTitle>{contents}</RuleTitle>
                          ),
                          ruleItem: contents => <RuleItem>{contents}</RuleItem>,
                          superStrong: contents => (
                            <SuperStrongText>{contents}</SuperStrongText>
                          ),
                        },
                      )}
                    </FormattedMessageOutsideText>
                  </PanelInsetBlock>
                </PanelInset>
              </>
            )}
          </PanelRow>
        </>
      )}
    </PanelGrid>
  )
}
