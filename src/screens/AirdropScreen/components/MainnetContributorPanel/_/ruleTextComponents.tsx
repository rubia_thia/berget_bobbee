import { Text } from "react-native"
import { useColors } from "../../../../../components/Themed/color"
import { FCC } from "../../../../../utils/reactHelpers/types"

export const RuleTitle: FCC = props => {
  const colors = useColors()

  return (
    <Text
      style={{ fontSize: 10, fontWeight: "bold", color: colors("gray-900") }}
    >
      {props.children}
    </Text>
  )
}

export const RuleItem: FCC = props => {
  const colors = useColors()

  return (
    <Text style={{ fontSize: 10, color: colors("gray-900") }}>
      • {props.children}
    </Text>
  )
}
