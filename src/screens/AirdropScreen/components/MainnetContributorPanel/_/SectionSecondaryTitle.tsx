import { FC, ReactNode } from "react"
import { StyleProp, Text, View, ViewStyle } from "react-native"
import { useColors } from "../../../../../components/Themed/color"
import { useSpacing } from "../../../../../components/Themed/spacing"

export const SectionSecondaryTitle: FC<{
  style?: StyleProp<ViewStyle>
  titleText: ReactNode
  statusBadge?: ReactNode
}> = props => {
  const colors = useColors()
  const spacing = useSpacing()

  return (
    <View
      style={[
        {
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "flex-start",
        },
        props.style,
      ]}
    >
      <Text
        style={{
          marginRight: spacing(2.5),
          fontSize: 16,
          fontWeight: "bold",
          color: colors("gray-900"),
        }}
      >
        {props.titleText}
      </Text>
      {props.statusBadge}
    </View>
  )
}
