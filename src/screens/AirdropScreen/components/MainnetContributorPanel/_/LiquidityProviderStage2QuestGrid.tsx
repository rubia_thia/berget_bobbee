import { FC, ReactNode } from "react"
import { defineMessage, useIntl } from "react-intl"
import { FlexStyle, StyleProp } from "react-native"
import { InfoListItem } from "../../../../../components/InfoList/InfoListItem"
import {
  InfoListItemDetail,
  InfoListItemDetailText,
} from "../../../../../components/InfoList/InfoListItemDetail"
import { InfoListItemTitle } from "../../../../../components/InfoList/InfoListItemTitle"
import { Spensor } from "../../../../../components/Spensor"
import {
  TextTokenCount,
  TokenCountAsCurrency,
} from "../../../../../components/TextTokenCount"
import { useSpacing } from "../../../../../components/Themed/spacing"
import { TokenCount, TokenPresets } from "../../../../../components/TokenCount"
import { FormattedMessageOutsideText } from "../../../../../utils/intlHelpers"
import { BigNumber } from "../../../../../utils/numberHelpers/BigNumber"
import { MarginStyle } from "../../../../../utils/styleHelpers/MarginStyle"
import {
  readResource,
  SuspenseResource,
} from "../../../../../utils/SuspenseResource"
import { TokenInfoPresets } from "../../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { AirdropPoint } from "../../../_/TokenInfo/TokenInfo"
import {
  PanelInset,
  PanelInsetBlock,
  PanelInsetBlockInfoList,
  PanelInsetBlockInfoListDivider,
} from "../../panelComponents/PanelInset"
import { AwardPart, QuestItem } from "../../panelComponents/QuestItem"
import { SuperStrongText } from "../../SuperStrongText"
import { RuleItem, RuleTitle } from "./ruleTextComponents"

export interface LiquidityProviderStage2QuestGridCoreProps {
  stage2LiquidityTarget: SuspenseResource<BigNumber>
  stage2MyULPCount: SuspenseResource<BigNumber>
  stage2SnapshotBlockHeight: SuspenseResource<number>
  stage2UNWAwardCount: SuspenseResource<BigNumber>
}

export const LiquidityProviderStage2QuestGrid: FC<
  LiquidityProviderStage2QuestGridCoreProps & {
    style?: StyleProp<MarginStyle & FlexStyle>
  }
> = props => {
  const spacing = useSpacing()
  const { $t } = useIntl()

  return (
    <PanelInset style={props.style}>
      <QuestItem
        titleText={$t(
          defineMessage({
            defaultMessage: "Liquidity Bootstrapping",
            description:
              "AirdropScreen/Mainnet Contributors Panel/Liquidity Providers/quest title",
          }),
        )}
        descriptionText={$t(
          defineMessage({
            defaultMessage: "Provide Liquidity of the first {liquidityCount}",
            description:
              "AirdropScreen/Mainnet Contributors Panel/Liquidity Providers/quest description",
          }),
          {
            liquidityCount: (
              <TokenCountAsCurrency
                token={TokenPresets.USD}
                count={props.stage2LiquidityTarget}
              />
            ),
          },
        )}
        actionArea={
          <AwardPart
            token={AirdropPoint}
            contentText={
              <TokenCount
                token={AirdropPoint}
                count={props.stage2UNWAwardCount}
              />
            }
          />
        }
      />

      <PanelInsetBlock style={{ marginTop: spacing(2.5) }}>
        <PanelInsetBlockInfoList>
          <InfoListItem>
            <InfoListItemTitle>
              {$t(
                defineMessage({
                  defaultMessage: "Liquidity",
                  description:
                    "AirdropScreen/Mainnet Contributors Panel/Liquidity Providers/detail field title",
                }),
              )}
            </InfoListItemTitle>

            <InfoListItemDetail>
              <InfoListItemDetailText>
                <TextTokenCount
                  token={TokenInfoPresets.ULP}
                  count={props.stage2MyULPCount}
                />
              </InfoListItemDetailText>
            </InfoListItemDetail>
          </InfoListItem>

          <PanelInsetBlockInfoListDivider />

          <InfoListItem>
            <InfoListItemTitle>
              {$t(
                defineMessage({
                  defaultMessage: "Snapshot Block",
                  description:
                    "AirdropScreen/Mainnet Contributors Panel/Liquidity Providers/detail field title",
                }),
              )}
            </InfoListItemTitle>
            <InfoListItemDetail>
              <InfoListItemDetailText>
                <Spensor fallback={"???"}>
                  {() => <>{readResource(props.stage2SnapshotBlockHeight)}</>}
                </Spensor>
              </InfoListItemDetailText>
            </InfoListItemDetail>
          </InfoListItem>
        </PanelInsetBlockInfoList>
      </PanelInsetBlock>

      <PanelInsetBlock style={{ marginTop: spacing(2.5) }}>
        <FormattedMessageOutsideText>
          {$t<ReactNode>(
            defineMessage({
              description:
                "AirdropScreen/Mainnet Contributors Panel/Liquidity Providers/stage 2 rule",
              defaultMessage: `
<ruleTitle>Criteria:</ruleTitle>
<ruleItem>From the Stage 1 Snapshot Block to TGE.</ruleItem>
<ruleItem>Snapshot will be taken randomly around the TGE.</ruleItem>
<ruleItem>3,500,000 <superStrong>esUNW</superStrong> will be shared based on the snapshot results</ruleItem>
                      `,
            }),
            {
              ruleTitle: contents => <RuleTitle>{contents}</RuleTitle>,
              ruleItem: contents => <RuleItem>{contents}</RuleItem>,
              superStrong: contents => (
                <SuperStrongText>{contents}</SuperStrongText>
              ),
            },
          )}
        </FormattedMessageOutsideText>
      </PanelInsetBlock>
    </PanelInset>
  )
}
