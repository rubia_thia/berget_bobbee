import { ComponentType, FC, ReactNode } from "react"
import { StyleProp, View, ViewStyle } from "react-native"
import { SvgProps } from "react-native-svg"
import { GradientText } from "../../../../../components/GradientText"
import { SpensorR } from "../../../../../components/Spensor"
import { useColors } from "../../../../../components/Themed/color"
import { useSpacing } from "../../../../../components/Themed/spacing"
import { hasAny } from "../../../../../utils/arrayHelpers"
import { SuspenseResource } from "../../../../../utils/SuspenseResource"
import { EarnedItem, EarnedSection } from "../../panelComponents/EarnedSection"

export const SectionHeader: FC<{
  style?: StyleProp<ViewStyle>
  Icon: ComponentType<SvgProps>
  titleText: ReactNode
  earned: SuspenseResource<EarnedItem[]>
}> = props => {
  const colors = useColors()
  const spacing = useSpacing()

  return (
    <View
      style={[
        {
          flexDirection: "row",
          flexWrap: "wrap",
          alignItems: "center",
          justifyContent: "space-between",
        },
        props.style,
      ]}
    >
      <props.Icon
        style={{ marginRight: spacing(2.5) }}
        width={36}
        height={36}
        fill={colors("pink-500")}
      />

      <GradientText
        style={{ marginRight: "auto" }}
        textStyle={{
          fontSize: 30,
          fontWeight: "bold",
        }}
        gradient={{
          colors: [
            { color: colors("pink-500"), opacity: 1, offset: 0.5267 },
            { color: colors("pink-500"), opacity: 0, offset: 1.3215 },
          ],
        }}
      >
        {props.titleText}
      </GradientText>

      <SpensorR read={{ earned: props.earned }}>
        {({ earned }) => hasAny(earned) && <EarnedSection earned={earned} />}
      </SpensorR>
    </View>
  )
}
