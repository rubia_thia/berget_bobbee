import { FC, ReactNode } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, ViewStyle } from "react-native"
import { Divider } from "../../../../components/Divider"
import { useSpacing } from "../../../../components/Themed/spacing"
import { EventPanel } from "../EventPanel"
import { PanelHead } from "../panelComponents/PanelHead"

export interface MainnetContributorPanelProps {
  style?: StyleProp<ViewStyle>
  renderLiquidityProvidersQuests: (props: {
    style?: StyleProp<ViewStyle>
  }) => ReactNode
  renderTradersQuests: (props: { style?: StyleProp<ViewStyle> }) => ReactNode
}

export const MainnetContributorPanel: FC<
  MainnetContributorPanelProps
> = props => {
  const spacing = useSpacing()

  const { $t } = useIntl()

  return (
    <EventPanel style={props.style}>
      <PanelHead
        mainText={$t(
          defineMessage({
            defaultMessage: "Mainnet Contributors ",
            description: "AirdropScreen/Mainnet Contributors Panel/title",
          }),
        )}
      />

      {props.renderLiquidityProvidersQuests({
        style: { marginTop: spacing(5) },
      })}

      <Divider padding={spacing(6) + spacing(1.5)} />

      {props.renderTradersQuests({ style: {} })}
    </EventPanel>
  )
}
