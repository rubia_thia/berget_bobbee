import { OptionalizeEventListeners } from "../../../../../.storybook/utils"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { AirdropPoint } from "../../_/TokenInfo/TokenInfo"
import { LiquidityProvidersQuestsCoreProps } from "./LiquidityProvidersQuests"

export const liquidityProvidersQuestsCoreProps: OptionalizeEventListeners<LiquidityProvidersQuestsCoreProps> =
  {
    stage1Status: "live",
    stage2Status: "live",
    earnedAsLiquidityProviders: [
      { token: AirdropPoint, amount: BigNumber.from(333.33) },
    ],
    stage1LiquidityTarget: BigNumber.from(1_000_000),
    stage1MyULPCount: BigNumber.from(100),
    stage1UNWAwardCount: BigNumber.from(111.11),
    stage1SnapshotBlockHeight: 12345,
    stage1LoyaltyPhaseMyULPCount: BigNumber.from(100),
    stage1LoyaltyPhaseUNWAwardCount: BigNumber.from(111.11),
    stage1LoyaltyPhaseSnapshotBlockHeight: 12345,
    stage1LoyaltyPhaseIsQualified: true,
    stage2LiquidityTarget: BigNumber.from(1_000_000),
    stage2MyULPCount: BigNumber.from(100),
    stage2UNWAwardCount: BigNumber.from(111.11),
    stage2SnapshotBlockHeight: 12345,
  }
