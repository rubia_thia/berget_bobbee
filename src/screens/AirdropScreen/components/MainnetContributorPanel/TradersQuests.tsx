import { FC, ReactNode } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, View, ViewStyle } from "react-native"
import { InfoListItem } from "../../../../components/InfoList/InfoListItem"
import {
  InfoListItemDetail,
  InfoListItemDetailText,
} from "../../../../components/InfoList/InfoListItemDetail"
import { InfoListItemTitle } from "../../../../components/InfoList/InfoListItemTitle"
import { Spensor } from "../../../../components/Spensor"
import { TokenCountAsCurrency } from "../../../../components/TextTokenCount"
import { useSpacing } from "../../../../components/Themed/spacing"
import { TokenCount, TokenPresets } from "../../../../components/TokenCount"
import { FormattedMessageOutsideText } from "../../../../utils/intlHelpers"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import {
  readResource,
  safeReadResource,
  SuspenseResource,
} from "../../../../utils/SuspenseResource"
import { AirdropPoint } from "../../_/TokenInfo/TokenInfo"
import { EarnedItem } from "../panelComponents/EarnedSection"
import {
  PanelInset,
  PanelInsetBlock,
  PanelInsetBlockInfoList,
  PanelInsetBlockInfoListDivider,
} from "../panelComponents/PanelInset"
import {
  AwardPart,
  QuestItem,
  StatusBadge,
  StatusBadgeStatus,
} from "../panelComponents/QuestItem"
import { SuperStrongText } from "../SuperStrongText"
import { RuleItem, RuleTitle } from "./_/ruleTextComponents"
import { SectionHeader } from "./_/SectionHeader"
import { SectionSecondaryTitle } from "./_/SectionSecondaryTitle"
import TraderIcon from "./_/trader.svg"

export interface TradersQuestsCoreProps {
  earnedAsTraders: EarnedItem[]
  stage1Status: SuspenseResource<StatusBadgeStatus>
  stage2Status: SuspenseResource<StatusBadgeStatus>
  stage1TradeVolumeTarget: SuspenseResource<BigNumber>
  stage1MyTradeVolume: SuspenseResource<BigNumber>
  stage1TotalTradeVolume: SuspenseResource<BigNumber>
  stage1SnapshotBlockHeight: SuspenseResource<number>
  stage1UNWAwardCount: SuspenseResource<BigNumber>
  stage1TotalUNWAwardCount: SuspenseResource<BigNumber>
  stage2TradeVolumeTarget: SuspenseResource<BigNumber>
  stage2MyTradeVolume: SuspenseResource<BigNumber>
  stage2TotalTradeVolume: SuspenseResource<BigNumber>
  stage2SnapshotBlockHeight: SuspenseResource<number>
  stage2UNWAwardCount: SuspenseResource<BigNumber>
  stage2TotalUNWAwardCount: SuspenseResource<BigNumber>
}

export const TradersQuests: FC<
  TradersQuestsCoreProps & {
    style?: StyleProp<ViewStyle>
  }
> = props => {
  const spacing = useSpacing()
  const { $t } = useIntl()

  const stage1Status = safeReadResource(props.stage1Status) ?? "upcoming"

  return (
    <View style={props.style}>
      <SectionHeader
        Icon={TraderIcon}
        titleText={$t(
          defineMessage({
            defaultMessage: "Trader",
            description:
              "AirdropScreen/Mainnet Contributors Panel/Traders/title",
          }),
        )}
        earned={props.earnedAsTraders}
      />

      <SectionSecondaryTitle
        style={{ marginTop: spacing(6) }}
        titleText={$t(
          defineMessage({
            defaultMessage: "Trader Airdrop Stage 1",
            description:
              "AirdropScreen/Mainnet Contributors Panel/Trader Airdrop Stage 1/title",
          }),
        )}
        statusBadge={<StatusBadge status={stage1Status} />}
      />

      <PanelInset style={{ marginTop: spacing(2.5), flex: 1 }}>
        <QuestItem
          titleText={$t(
            defineMessage({
              defaultMessage: "Trader Bootstrapping",
              description:
                "AirdropScreen/Mainnet Contributors Panel/Traders/quest title",
            }),
          )}
          descriptionText={$t(
            defineMessage({
              defaultMessage:
                "Hold an open position among the first {liquidityCount} trading volume ",
              description:
                "AirdropScreen/Mainnet Contributors Panel/Trader Bootstrapping/quest description",
            }),
            {
              liquidityCount: (
                <TokenCountAsCurrency
                  token={TokenPresets.USD}
                  count={props.stage1TradeVolumeTarget}
                />
              ),
            },
          )}
          actionArea={
            <AwardPart
              token={AirdropPoint}
              contentText={
                <TokenCount
                  token={AirdropPoint}
                  count={props.stage1UNWAwardCount}
                />
              }
            />
          }
        />

        <PanelInsetBlock style={{ marginTop: spacing(2.5) }}>
          <PanelInsetBlockInfoList>
            <InfoListItem>
              <InfoListItemTitle>
                {$t(
                  defineMessage({
                    defaultMessage: "Your Trading Volume",
                    description:
                      "AirdropScreen/Mainnet Contributors Panel/Traders/quest detail label",
                  }),
                )}
              </InfoListItemTitle>
              <InfoListItemDetail>
                <InfoListItemDetailText>
                  <TokenCountAsCurrency
                    token={TokenPresets.USD}
                    count={props.stage1MyTradeVolume}
                  />
                </InfoListItemDetailText>
              </InfoListItemDetail>
            </InfoListItem>

            <PanelInsetBlockInfoListDivider />

            <InfoListItem>
              <InfoListItemTitle>
                {$t(
                  defineMessage({
                    defaultMessage: "Total Trading Volume",
                    description:
                      "AirdropScreen/Mainnet Contributors Panel/Traders/quest detail label",
                  }),
                )}
              </InfoListItemTitle>
              <InfoListItemDetail>
                <InfoListItemDetailText>
                  <TokenCountAsCurrency
                    token={TokenPresets.USD}
                    count={props.stage1TotalTradeVolume}
                  />
                </InfoListItemDetailText>
              </InfoListItemDetail>
            </InfoListItem>

            <PanelInsetBlockInfoListDivider />

            <InfoListItem>
              <InfoListItemTitle>
                {$t(
                  defineMessage({
                    defaultMessage: "Snapshot Block",
                    description:
                      "AirdropScreen/Mainnet Contributors Panel/Traders/quest detail label",
                  }),
                )}
              </InfoListItemTitle>
              <InfoListItemDetail>
                <InfoListItemDetailText>
                  <Spensor fallback={"???"}>
                    {() => <>{readResource(props.stage1SnapshotBlockHeight)}</>}
                  </Spensor>
                </InfoListItemDetailText>
              </InfoListItemDetail>
            </InfoListItem>
          </PanelInsetBlockInfoList>
        </PanelInsetBlock>

        <PanelInsetBlock style={{ marginTop: spacing(2.5) }}>
          <FormattedMessageOutsideText>
            {$t<ReactNode>(
              defineMessage({
                description:
                  "AirdropScreen/Mainnet Contributors Panel/Trader/stage 1 rule",
                defaultMessage: `
<ruleTitle>Criteria:</ruleTitle>
<ruleItem>Open/close positions within the first $100,000,000 trading volume.</ruleItem>
<ruleItem>1,000,000 <superStrong>UNW</superStrong> will be shared based on your trading volume.</ruleItem>
                      `,
              }),
              {
                totalAward: (
                  <TokenCountAsCurrency
                    token={TokenPresets.USD}
                    count={props.stage1TotalUNWAwardCount}
                  />
                ),
                superStrong: contents => (
                  <SuperStrongText>{contents}</SuperStrongText>
                ),
                ruleTitle: contents => <RuleTitle>{contents}</RuleTitle>,
                ruleItem: contents => <RuleItem>{contents}</RuleItem>,
              },
            )}
          </FormattedMessageOutsideText>
        </PanelInsetBlock>
      </PanelInset>
    </View>
  )
}
