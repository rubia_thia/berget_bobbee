import { ComponentMeta } from "@storybook/react"
import {
  BackgroundColor,
  CardContainer,
} from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { DELAYED } from "../../../../utils/SuspenseResource"
import { TradersQuests } from "./TradersQuests"
import { tradersQuestsCoreProps } from "./TradersQuests.mockData"

export default {
  title: "Page/AirdropScreen/MainnetContributorPanel/TradersQuests",
  component: TradersQuests,
  decorators: [CardContainer(), BackgroundColor()],
} as ComponentMeta<typeof TradersQuests>

const template = withTemplate(TradersQuests, {
  style: {
    margin: 10,
  },
  ...tradersQuestsCoreProps,
})

export const Normal = template()

export const Stage2NotStarted = template(p => {
  p.stage2Status = "upcoming"
})

export const DataLoading = template(p => {
  Object.assign(p, {
    stage1MyTradeVolume: DELAYED(),
    stage1TotalTradeVolume: DELAYED(),
    stage1TradeVolumeTarget: DELAYED(),
    stage1SnapshotBlockHeight: DELAYED(),
    stage1UNWAwardCount: DELAYED(),
    stage1TotalUNWAwardCount: DELAYED(),
    stage2MyTradeVolume: DELAYED(),
    stage2TotalTradeVolume: DELAYED(),
    stage2TradeVolumeTarget: DELAYED(),
    stage2SnapshotBlockHeight: DELAYED(),
    stage2UNWAwardCount: DELAYED(),
    stage2TotalUNWAwardCount: DELAYED(),
  })
})
