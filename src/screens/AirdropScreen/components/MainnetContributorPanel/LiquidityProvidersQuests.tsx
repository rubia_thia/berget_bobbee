import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, View, ViewStyle } from "react-native"
import { useSpacing } from "../../../../components/Themed/spacing"
import {
  safeReadResource,
  SuspenseResource,
} from "../../../../utils/SuspenseResource"
import { EarnedItem } from "../panelComponents/EarnedSection"
import { PlaceholderPanelInset } from "../panelComponents/PanelInset"
import { StatusBadge, StatusBadgeStatus } from "../panelComponents/QuestItem"
import LiquidityProviderIcon from "./_/liquidityProvider.svg"
import {
  LiquidityProviderStage1QuestGrid,
  LiquidityProviderStage1QuestGridCoreProps,
} from "./_/LiquidityProviderStage1QuestGrid"
import {
  LiquidityProviderStage2QuestGrid,
  LiquidityProviderStage2QuestGridCoreProps,
} from "./_/LiquidityProviderStage2QuestGrid"
import { SectionHeader } from "./_/SectionHeader"
import { SectionSecondaryTitle } from "./_/SectionSecondaryTitle"

export interface LiquidityProvidersQuestsCoreProps
  extends LiquidityProviderStage1QuestGridCoreProps,
    LiquidityProviderStage2QuestGridCoreProps {
  stage1Status: SuspenseResource<StatusBadgeStatus>
  stage2Status: SuspenseResource<StatusBadgeStatus>
  earnedAsLiquidityProviders: SuspenseResource<EarnedItem[]>
}

export const LiquidityProvidersQuests: FC<
  LiquidityProvidersQuestsCoreProps & {
    style?: StyleProp<ViewStyle>
  }
> = props => {
  const spacing = useSpacing()
  const { $t } = useIntl()

  const stage1Status = safeReadResource(props.stage1Status) ?? "upcoming"
  const stage2Status = safeReadResource(props.stage2Status) ?? "upcoming"

  return (
    <View style={props.style}>
      <SectionHeader
        Icon={LiquidityProviderIcon}
        titleText={$t(
          defineMessage({
            defaultMessage: "Liquidity Provider",
            description:
              "AirdropScreen/Mainnet Contributors Panel/Liquidity Providers/title",
          }),
        )}
        earned={props.earnedAsLiquidityProviders}
      />

      <SectionSecondaryTitle
        style={{ marginTop: spacing(6) }}
        titleText={$t(
          defineMessage({
            defaultMessage: "Liquidity Provision Stage 1",
            description:
              "AirdropScreen/Mainnet Contributors Panel/Liquidity Provision Stage 1/title",
          }),
        )}
        statusBadge={<StatusBadge status={stage1Status} />}
      />

      <LiquidityProviderStage1QuestGrid
        style={{ marginTop: spacing(2.5) }}
        stage1LiquidityTarget={props.stage1LiquidityTarget}
        stage1UNWAwardCount={props.stage1UNWAwardCount}
        stage1MyULPCount={props.stage1MyULPCount}
        stage1SnapshotBlockHeight={props.stage1SnapshotBlockHeight}
        stage1LoyaltyPhaseUNWAwardCount={props.stage1LoyaltyPhaseUNWAwardCount}
        stage1LoyaltyPhaseIsQualified={props.stage1LoyaltyPhaseIsQualified}
        stage1LoyaltyPhaseMyULPCount={props.stage1LoyaltyPhaseMyULPCount}
        stage1LoyaltyPhaseSnapshotBlockHeight={
          props.stage1LoyaltyPhaseSnapshotBlockHeight
        }
      />

      <SectionSecondaryTitle
        style={{ marginTop: spacing(6) }}
        titleText={$t(
          defineMessage({
            defaultMessage: "Liquidity Provision Stage 2",
            description:
              "AirdropScreen/Mainnet Contributors Panel/Liquidity Provision Stage 2/title",
          }),
        )}
        statusBadge={<StatusBadge status={stage2Status} />}
      />

      {stage2Status === "upcoming" && (
        <PlaceholderPanelInset
          style={{ marginTop: spacing(2.5) }}
          placeholderText={$t(
            defineMessage({
              defaultMessage:
                "Stage 1 is just the beginning. There's a lot more in store. Tune in for the Stage 2!",
              description:
                "AirdropScreen/Mainnet Contributors Panel/Liquidity Provision Stage 2/comming soon",
            }),
          )}
        />
      )}
      {stage2Status !== "upcoming" && (
        <LiquidityProviderStage2QuestGrid
          style={{ flex: 1, marginTop: spacing(2.5) }}
          stage2LiquidityTarget={props.stage2LiquidityTarget}
          stage2UNWAwardCount={props.stage2UNWAwardCount}
          stage2MyULPCount={props.stage2MyULPCount}
          stage2SnapshotBlockHeight={props.stage2SnapshotBlockHeight}
        />
      )}
    </View>
  )
}
