import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { LiquidityProvidersQuests } from "./LiquidityProvidersQuests"
import { MainnetContributorPanel } from "./MainnetContributorPanel"
import { mainnetContributorPanelProps } from "./MainnetContributorPanel.mockData"
import { TradersQuests } from "./TradersQuests"

export default {
  title: "Page/AirdropScreen/MainnetContributorPanel",
  component: MainnetContributorPanel,
  decorators: [BackgroundColor()],
  subcomponents: { LiquidityProvidersQuests, TradersQuests },
} as ComponentMeta<typeof MainnetContributorPanel>

const template = withTemplate(MainnetContributorPanel, {
  style: {
    margin: 10,
  },
  ...mainnetContributorPanelProps,
})

export const Normal = template()
