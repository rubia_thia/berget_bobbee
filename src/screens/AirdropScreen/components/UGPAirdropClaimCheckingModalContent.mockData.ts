import { range } from "ramda"
import { OptionalizeEventListeners } from "../../../../.storybook/utils"
import { randomIntegerInRange } from "../../../utils/numberHelpers"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../utils/TokenInfoPresets/TokenInfoPresets"
import { UGPAirdropClaimCheckingModalContentProps } from "./UGPAirdropClaimCheckingModalContent"

export const ugpAirdropClaimCheckingModalContentProps: OptionalizeEventListeners<UGPAirdropClaimCheckingModalContentProps> =
  {
    searchingNftNumber: null,
    token: TokenInfoPresets.esUNW,
    leftWeekCount: 10,
    nextAirdropBlockNumber: 111111,
    pagination: {
      current: 2,
      pageSize: 10,
      total: 1024,
    },
    records: range(0, 10).map(n => ({
      nftNumber: randomIntegerInRange(3888),
      claimedEsUNW: BigNumber.from(randomIntegerInRange(1046)),
      remainingEsUNW: BigNumber.from(randomIntegerInRange(1046)),
    })),
  }
