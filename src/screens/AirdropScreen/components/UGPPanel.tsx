import { FC, Fragment, ReactNode } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, Text, TouchableOpacity, ViewStyle } from "react-native"
import { Path, Svg } from "react-native-svg"
import {
  NoteParagraph,
  NoteParagraphText,
} from "../../../components/NoteParagraph/NoteParagraph"
import { Spensor } from "../../../components/Spensor"
import { useColors } from "../../../components/Themed/color"
import { useSpacing } from "../../../components/Themed/spacing"
import { arrayJoin } from "../../../utils/arrayHelpers"
import { FormattedMessageOutsideText } from "../../../utils/intlHelpers"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { readResource, SuspenseResource } from "../../../utils/SuspenseResource"
import { AirdropPoint } from "../_/TokenInfo/TokenInfo"
import { EventPanel } from "./EventPanel"
import { PanelHead } from "./panelComponents/PanelHead"
import { PanelInset } from "./panelComponents/PanelInset"
import { ProcessingBadge, QuestItem } from "./panelComponents/QuestItem"
import { SignedTokenCount } from "./SignedTokenCount"
import { SuperStrongText } from "./SuperStrongText"

export interface UGPPanelProps {
  style?: StyleProp<ViewStyle>

  awardUNWPerUGP: SuspenseResource<BigNumber>

  ugps: SuspenseResource<{ nftNumber: number }[]>

  onShowLookupModal: () => void
}

export const UGPPanel: FC<UGPPanelProps> = props => {
  const spacing = useSpacing()
  const colors = useColors()
  const { $t } = useIntl()

  return (
    <EventPanel style={props.style}>
      <PanelHead
        mainText={$t(
          defineMessage({
            defaultMessage: "Genesis Pass Holders and Stakers",
            description: "AirdropScreen/UGPPanel/title",
          }),
        )}
        statusBadge={<ProcessingBadge />}
      />

      <Spensor>
        {() => {
          const ugps = readResource(props.ugps)

          if (ugps.length <= 0) return null

          return (
            <Text
              style={{
                marginTop: spacing(2.5),
                padding: spacing(2.5),
                borderWidth: 1,
                borderColor: colors("gray-200"),
                borderRadius: 4,
                fontSize: 16,
                color: colors("gray-900"),
              }}
            >
              {$t(
                defineMessage({
                  defaultMessage: "Your Genesis Pass: {nftNumbers}",
                  description: "AirdropScreen/UGPPanel/yourUGP",
                }),
                {
                  nftNumbers: (
                    <>
                      {arrayJoin(
                        ({ index }) => (
                          <Fragment key={`sep-${index}`}>, </Fragment>
                        ),
                        ugps.map((ugp, idx) => (
                          <Text
                            key={`nft-${idx}`}
                            style={{ fontWeight: "600" }}
                          >
                            #{ugp.nftNumber}
                          </Text>
                        )),
                      )}
                    </>
                  ),
                },
              )}
            </Text>
          )
        }}
      </Spensor>

      <PanelInset style={{ marginTop: spacing(2.5) }}>
        <QuestItem
          titleText={$t(
            defineMessage({
              defaultMessage: "Uniwhale Genesis Pass Holders",
              description: "AirdropScreen/UGPPanel/quest title",
            }),
          )}
          descriptionText={$t(
            defineMessage({
              defaultMessage: "{tokenCount}/Genesis Pass Holder",
              description: "AirdropScreen/UGPPanel/quest description",
            }),
            {
              tokenCount: <SignedTokenCount token={AirdropPoint} count={960} />,
            },
          )}
          actionArea={
            <TouchableOpacity
              style={{ flexDirection: "row", alignItems: "center" }}
              onPress={props.onShowLookupModal}
            >
              <Svg
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill={colors("blue-500")}
              >
                <Path d="M23.809 21.646L17.604 15.441C18.771 13.836 19.461 11.862 19.461 9.73C19.461 4.365 15.096 0 9.73 0C4.365 0 0 4.365 0 9.73C0 15.096 4.365 19.46 9.73 19.46C11.764 19.46 13.653 18.833 15.217 17.762L21.455 24L23.809 21.646ZM2.854 9.73C2.854 5.938 5.939 2.853 9.731 2.853C13.523 2.853 16.608 5.938 16.608 9.73C16.608 13.522 13.523 16.607 9.731 16.607C5.938 16.607 2.854 13.522 2.854 9.73Z" />
              </Svg>

              <Text
                style={{
                  marginLeft: spacing(1),
                  color: colors("blue-600"),
                  fontSize: 16,
                  fontWeight: "600",
                }}
              >
                {$t(
                  defineMessage({
                    defaultMessage: "Check",
                    description: 'AirdropScreen/UGPPanel/quest "Check" button',
                  }),
                )}
              </Text>
            </TouchableOpacity>
          }
        />
      </PanelInset>

      <NoteParagraph style={{ marginTop: spacing(2.5) }}>
        <FormattedMessageOutsideText>
          {$t<ReactNode>(
            defineMessage({
              defaultMessage: `<notice>The distribution of UGP airdrops is linked to the NFT’s token ID.</notice>
<listItem>Please <superStrong>UNSTAKE</superStrong> your UGP before claiming the airdrops.</listItem>
<listItem>Only the current owner of the UGP can claim the airdrops.</listItem>`,
              description: "AirdropScreen/UGPPanel/bottom tip text",
            }),
            {
              notice: contents => (
                <NoteParagraphText>{contents}</NoteParagraphText>
              ),
              listItem: contents => (
                <NoteParagraphText>- {contents}</NoteParagraphText>
              ),
              superStrong: contents => (
                <SuperStrongText>{contents}</SuperStrongText>
              ),
            },
          )}
        </FormattedMessageOutsideText>
      </NoteParagraph>
    </EventPanel>
  )
}
