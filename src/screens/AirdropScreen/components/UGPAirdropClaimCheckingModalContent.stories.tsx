import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { DELAYED } from "../../../utils/SuspenseResource"
import { UGPAirdropClaimCheckingModalContent } from "./UGPAirdropClaimCheckingModalContent"
import { ugpAirdropClaimCheckingModalContentProps } from "./UGPAirdropClaimCheckingModalContent.mockData"

export default {
  title: "Page/AirdropScreen/UGPAirdropClaimCheckingModalContent",
  component: UGPAirdropClaimCheckingModalContent,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof UGPAirdropClaimCheckingModalContent>

const template = withTemplate(UGPAirdropClaimCheckingModalContent, {
  style: {
    margin: 10,
  },
  ...ugpAirdropClaimCheckingModalContentProps,
})

export const Normal = template()

export const Loading = template(p => {
  p.records = DELAYED()
})

export const Empty = template(p => {
  p.records = []
})
