import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { TokenInfo } from "../../../utils/TokenInfo"
import { TestnetContributorPanel } from "./TestnetContributorPanel"
import { testnetContributorPanelProps } from "./TestnetContributorPanel.mockData"

export default {
  title: "Page/AirdropScreen/TestnetContributorPanel",
  component: TestnetContributorPanel,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof TestnetContributorPanel>

const template = withTemplate(TestnetContributorPanel, {
  style: {
    margin: 10,
  },
  ...testnetContributorPanelProps,
})

export const Normal = template()

export const POAPVerified = template(p => {
  ;(
    p.poapHolderAward as {
      token: TokenInfo
      amountPerPoap: BigNumber
      isHolder: boolean
    }
  ).isHolder = true
})
