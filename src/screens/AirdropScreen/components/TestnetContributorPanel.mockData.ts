import { OptionalizeEventListeners } from "../../../../.storybook/utils"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../utils/TokenInfoPresets/TokenInfoPresets"
import { TestnetContributorPanelProps } from "./TestnetContributorPanel"

export const testnetContributorPanelProps: OptionalizeEventListeners<TestnetContributorPanelProps> =
  {
    totalEarned: [{ token: TokenInfoPresets.UNW, amount: BigNumber.from(120) }],
    bugReportAward: {
      token: TokenInfoPresets.UNW,
      amountPerIssue: BigNumber.from(20),
      wonAmount: BigNumber.from(80),
    },
    poapHolderAward: {
      token: TokenInfoPresets.UNW,
      amountPerPoap: BigNumber.from(10),
      isHolder: true,
    },
  }
