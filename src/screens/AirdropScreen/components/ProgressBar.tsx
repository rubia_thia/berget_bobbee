import { FC, ReactNode, useState } from "react"
import { LayoutRectangle, StyleProp, Text, View, ViewStyle } from "react-native"
import { Path, Rect, Svg } from "react-native-svg"
import { useColors } from "../../../components/Themed/color"
import { useSpacing } from "../../../components/Themed/spacing"
import { hasAny } from "../../../utils/arrayHelpers"

export const milestoneIconSize = 24

export interface ProgressBarProps {
  style?: StyleProp<ViewStyle>
  progress: number
  milestones?: {
    progress: number
    nameText: ReactNode
    textAlign?: "start" | "end" | "middle"
  }[]
}

export const ProgressBar: FC<ProgressBarProps> = props => {
  const colors = useColors()

  const { milestones = [] } = props

  const bgColor = colors("blue-200")
  const fgColor = colors("blue-600")

  const barHeight = 8

  const [layout, setLayout] = useState<LayoutRectangle>()

  return (
    <View style={props.style} onLayout={e => setLayout(e.nativeEvent.layout)}>
      <View
        style={{
          marginVertical: (milestoneIconSize - barHeight) / 2,
          width: "100%",
          height: barHeight,
          borderRadius: 999,
          backgroundColor: bgColor,
          overflow: "hidden",
        }}
      >
        <View
          style={{
            marginLeft: `-${(1 - props.progress) * 100}%`,
            width: `100%`,
            height: "100%",
            borderRadius: 999,
            backgroundColor: fgColor,
          }}
        />
      </View>

      {hasAny(milestones) && (
        <View style={{ marginTop: 0 - milestoneIconSize }}>
          {/* space holder */}
          <Milestone
            style={{ opacity: 0 }}
            icon={<MilestoneIconArrived />}
            labelText={<>&nbsp;</>}
          />

          {milestones.map((milestone, idx) => {
            if (layout == null) return

            const estLeft = milestone.progress * layout.width

            const isInTheMostStart = estLeft < milestoneIconSize / 2
            const isInTheMostEnd = estLeft > layout.width - milestoneIconSize
            const left = isInTheMostStart
              ? 0
              : isInTheMostEnd
              ? layout.width - milestoneIconSize / 2
              : estLeft

            return (
              <Milestone
                key={idx}
                style={{ position: "absolute", top: 0, left }}
                moveBackHalf={true}
                icon={
                  milestone.progress <= props.progress ? (
                    <MilestoneIconArrived />
                  ) : (
                    <MilestoneIconTarget />
                  )
                }
                labelText={milestone.nameText}
                labelAlign={milestone.textAlign}
              />
            )
          })}
        </View>
      )}
    </View>
  )
}

const Milestone: FC<{
  style?: StyleProp<ViewStyle>
  moveBackHalf?: boolean
  icon: ReactNode
  labelText: ReactNode
  labelAlign?: "start" | "end" | "middle"
}> = props => {
  const colors = useColors()
  const spacing = useSpacing()

  const [layout, setLayout] = useState<LayoutRectangle>()

  return (
    <View
      style={[
        {
          alignItems:
            props.labelAlign === "start"
              ? "flex-start"
              : props.labelAlign === "end"
              ? "flex-end"
              : "center",
        },
        props.moveBackHalf &&
          layout != null && {
            marginLeft:
              props.labelAlign === "start"
                ? 0
                : props.labelAlign === "end"
                ? -layout.width + milestoneIconSize / 2
                : -(layout.width / 2),
          },
        props.style,
      ]}
      onLayout={e => setLayout(e.nativeEvent.layout)}
    >
      {props.icon}
      <Text
        style={{
          marginTop: spacing(1),
          color: colors("blue-600"),
          fontSize: 14,
        }}
        numberOfLines={1}
      >
        {props.labelText}
      </Text>
    </View>
  )
}

const MilestoneIconTarget: FC<{ style?: StyleProp<ViewStyle> }> = props => (
  <Svg
    style={props.style}
    width={milestoneIconSize}
    height={milestoneIconSize}
    viewBox="0 0 24 24"
    fill="none"
  >
    <Rect x="2" y="2" width="20" height="20" rx="10" fill="white" />
    <Rect
      x="2"
      y="2"
      width="20"
      height="20"
      rx="10"
      stroke="#2563EB"
      strokeWidth="4"
    />
  </Svg>
)

const MilestoneIconArrived: FC<{ style?: StyleProp<ViewStyle> }> = props => (
  <Svg
    style={props.style}
    width={milestoneIconSize}
    height={milestoneIconSize}
    viewBox="0 0 24 24"
    fill="none"
  >
    <Rect width="24" height="24" rx="12" fill="#2563EB" />
    <Path
      d="M10.3846 17.7692L5 12.0626L6.50662 10.5232L10.3561 14.5471L17.4336 7L19 8.51092L10.3846 17.7692Z"
      fill="white"
    />
  </Svg>
)
