import { ComponentMeta } from "@storybook/react"
import { Text } from "react-native"
import {
  BackgroundColor,
  CardContainer,
} from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { ProgressBar } from "./ProgressBar"

export default {
  title: "Page/AirdropScreen/ProgressBar",
  component: ProgressBar,
  decorators: [CardContainer({ padding: 10 }), BackgroundColor()],
} as ComponentMeta<typeof ProgressBar>

const template = withTemplate(ProgressBar, {
  style: {
    width: 500,
  },
  progress: 0.68,
})

export const Normal = template()

export const WithMilestone = template(p => {
  p.milestones = [
    {
      progress: 0,
      nameText: (
        <Text>
          <Text style={{ fontWeight: "bold" }}>Stage 0</Text> ($0M)
        </Text>
      ),
      textAlign: "start",
    },
    {
      progress: 0.3,
      nameText: (
        <Text>
          <Text style={{ fontWeight: "bold" }}>Stage 1</Text> ($1M)
        </Text>
      ),
    },
    { progress: 0.5, nameText: <Text>Stage 2 ($2M)</Text> },
    { progress: 1, nameText: <Text>Stage 3 ($3M)</Text>, textAlign: "end" },
  ]
})
