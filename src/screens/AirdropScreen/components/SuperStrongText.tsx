import { Text } from "react-native"
import { useColors } from "../../../components/Themed/color"
import { FCC } from "../../../utils/reactHelpers/types"

export const SuperStrongText: FCC = props => {
  const colors = useColors()

  return (
    <Text style={{ fontWeight: "bold", color: colors("pink-500") }}>
      {props.children}
    </Text>
  )
}
