import { FC } from "react"
import { Text } from "react-native"
import { Spensor } from "../../../components/Spensor"
import { TokenCount, TokenCountProps } from "../../../components/TokenCount"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { readResource } from "../../../utils/SuspenseResource"

export const SignedTokenCount: FC<TokenCountProps> = props => {
  return (
    <Text style={props.style}>
      <Spensor>
        {() => (
          <>{BigNumber.isNegative(readResource(props.count)) ? "" : "+"}</>
        )}
      </Spensor>
      <TokenCount
        token={props.token}
        count={props.count}
        color={props.color}
        padDecimals={props.padDecimals}
      />
    </Text>
  )
}
