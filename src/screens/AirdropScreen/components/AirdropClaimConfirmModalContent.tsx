import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, View, ViewStyle } from "react-native"
import { WhiteOutlineButtonVariant } from "../../../components/Button/WhiteOutlineButtonVariant"
import { SmartLoadableButton } from "../../../components/ButtonFramework/LoadableButton"
import { CardInset } from "../../../components/CardBox/CardInset"
import {
  CardBoxModalContent,
  TitleBar,
} from "../../../components/CardBoxModalContent/CardBoxModalContent"
import { IconTokenCount } from "../../../components/IconTokenCount"
import { InfoList } from "../../../components/InfoList/InfoList"
import { InfoListItem } from "../../../components/InfoList/InfoListItem"
import { InfoListItemDetail } from "../../../components/InfoList/InfoListItemDetail"
import { InfoListItemTitle } from "../../../components/InfoList/InfoListItemTitle"
import { useColors } from "../../../components/Themed/color"
import { useSpacing } from "../../../components/Themed/spacing"
import { TokenName } from "../../../components/TokenName"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { TokenInfo } from "../../../utils/TokenInfo"

export interface AirdropClaimConfirmModalContentProps {
  style?: StyleProp<ViewStyle>

  rewards: {
    token: TokenInfo
    amount: BigNumber
  }[]

  onClaim: () => Promise<void>
  onClaimAndStake: () => Promise<void>
  onDismiss: () => void
}

export const AirdropClaimConfirmModalContent: FC<
  AirdropClaimConfirmModalContentProps
> = props => {
  const spacing = useSpacing()
  const colors = useColors()
  const { $t } = useIntl()

  const gap = spacing(6) + spacing(1.5)

  return (
    <CardBoxModalContent
      style={props.style}
      padding={spacing(5)}
      onClose={props.onDismiss}
    >
      <TitleBar>
        {$t(
          defineMessage({
            defaultMessage: "Your Airdrops",
            description: "AirdropScreen/AirdropClaimConfirmModalContent/title",
          }),
        )}
      </TitleBar>

      <CardInset
        style={{ marginTop: gap }}
        padding={{
          paddingVertical: spacing(4),
          paddingHorizontal: spacing(6),
        }}
      >
        <InfoList direction={"column"} listItemDirection={"row"}>
          {props.rewards.map((r, idx) => (
            <InfoListItem
              key={idx}
              style={[
                idx === 0 ? {} : { marginTop: spacing(2.5) },
                { justifyContent: "space-between" },
              ]}
            >
              <InfoListItemTitle>
                <IconTokenCount
                  iconSize={32}
                  countStyle={{
                    color: colors("blue-600"),
                    fontSize: 30,
                    fontWeight: "600",
                  }}
                  gap={spacing(2.5)}
                  token={r.token}
                  count={r.amount}
                />
              </InfoListItemTitle>
              <InfoListItemDetail style={{ marginLeft: spacing(5) }}>
                <TokenName
                  style={{ fontSize: 20, color: colors("gray-900") }}
                  token={r.token}
                />
              </InfoListItemDetail>
            </InfoListItem>
          ))}
        </InfoList>
      </CardInset>

      <View style={{ marginTop: gap }}>
        <SmartLoadableButton onPress={props.onClaim}>
          {$t(
            defineMessage({
              defaultMessage: "Claim",
              description:
                "AirdropScreen/AirdropClaimConfirmModalContent/Claim",
            }),
          )}
        </SmartLoadableButton>

        <SmartLoadableButton
          style={{ marginTop: spacing(4) }}
          Variant={WhiteOutlineButtonVariant}
          onPress={props.onClaimAndStake}
        >
          {$t(
            defineMessage({
              defaultMessage: "Claim and Stake",
              description:
                "AirdropScreen/AirdropClaimConfirmModalContent/Claim",
            }),
          )}
        </SmartLoadableButton>
      </View>
    </CardBoxModalContent>
  )
}
