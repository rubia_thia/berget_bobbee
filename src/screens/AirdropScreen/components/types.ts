import { BigNumber } from "../../../utils/numberHelpers/BigNumber"

export interface UGPClaimingRecord {
  nftNumber: number
  claimableEsUNW: BigNumber
  remainingEsUNW: BigNumber
}
