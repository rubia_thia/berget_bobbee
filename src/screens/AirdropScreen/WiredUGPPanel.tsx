import { action } from "mobx"
import { FC, useState } from "react"
import { StyleProp, ViewStyle } from "react-native"
import { Dialog } from "../../components/DialogProvider/Dialog"
import { useAirdropStore } from "../../stores/AirdropStore/useAirdropStore"
import { useAuthStore } from "../../stores/AuthStore/useAuthStore"
import { suspenseResource } from "../../utils/SuspenseResource"
import { TokenInfoPresets } from "../../utils/TokenInfoPresets/TokenInfoPresets"
import { UGPAirdropClaimCheckingModalContent } from "./components/UGPAirdropClaimCheckingModalContent"
import { UGPPanel } from "./components/UGPPanel"

export const WiredUGPPanel: FC<{
  style?: StyleProp<ViewStyle>
  showModalInitially?: boolean
}> = props => {
  const store = useAirdropStore()
  const authStore = useAuthStore()

  const [showModal, setShowModal] = useState(props.showModalInitially ?? false)

  return (
    <>
      <UGPPanel
        style={props.style}
        awardUNWPerUGP={suspenseResource(() => store.ugpModule.awardUNWPerUGP$)}
        ugps={suspenseResource(() =>
          store.ugpModule.ugpIndexes$.map(i => ({ nftNumber: i })),
        )}
        onShowLookupModal={() => setShowModal(true)}
      />
      <Dialog
        width={700}
        visible={showModal}
        onClose={() => setShowModal(false)}
      >
        <UGPAirdropClaimCheckingModalContent
          token={TokenInfoPresets.UNW}
          nextAirdropBlockNumber={suspenseResource(
            () => store.ugpModule.nextAirdropBlockNumber$,
          )}
          leftWeekCount={suspenseResource(() => store.ugpModule.cyclesLeft$)}
          searchingNftNumber={suspenseResource(
            () => store.ugpModule.searchingNft,
          )}
          onSearchingNftNumberChange={action(
            searching => (store.ugpModule.searchingNft = searching),
          )}
          records={suspenseResource(() => store.ugpModule.records.records$)}
          pagination={suspenseResource(
            () => store.ugpModule.records.paginationInfo$,
          )}
          onPaginationChange={store.ugpModule.records.onPaginationChange}
          onConnectWallet={
            authStore.connected
              ? undefined
              : () => authStore.triggerConnectWalletModal()
          }
          onDismiss={() => setShowModal(false)}
        />
      </Dialog>
    </>
  )
}
