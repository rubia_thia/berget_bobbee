import { FC, Suspense } from "react"
import { defineMessage, useIntl } from "react-intl"
import { useTransactionNotifier } from "../../components/TransactionNotifier/WiredTransactionNotifier"
import { TopLevelNavigatorScreenProps } from "../../navigation/navigators/TopLevelNavigatorTypes"
import {
  AirdropStoreProvider,
  useAirdropStore,
} from "../../stores/AirdropStore/useAirdropStore"
import { useAppEnvStore } from "../../stores/AppEnvStore/useAppEnvStore"
import { useAuthStore } from "../../stores/AuthStore/useAuthStore"
import { safelyGet } from "../../stores/utils/waitFor"
import { noop } from "../../utils/fnHelpers"
import { math } from "../../utils/numberHelpers/bigNumberExpressionParser"
import { suspenseResource } from "../../utils/SuspenseResource"
import { OneOrMore } from "../../utils/types"
import { HideScreenOnBlur } from "../components/HideScreenOnBlur"
import { AirdropScreenContent } from "./AirdropScreenContent"
import { LiquidityProvidersQuests } from "./components/MainnetContributorPanel/LiquidityProvidersQuests"
import { MainnetContributorPanel } from "./components/MainnetContributorPanel/MainnetContributorPanel"
import { TradersQuests } from "./components/MainnetContributorPanel/TradersQuests"
import { EarnedItem } from "./components/panelComponents/EarnedSection"
import { SummaryPanel } from "./components/SummaryPanel/SummaryPanel"
import { TestnetContributorPanel } from "./components/TestnetContributorPanel"
import { WiredUGPPanel } from "./WiredUGPPanel"
import { AirdropPoint } from "./_/TokenInfo/TokenInfo"

const Content: FC<{
  showUGPCheckerModalInitially?: boolean
}> = props => {
  const store = useAirdropStore()
  const appEnv = useAppEnvStore()
  const authStore = useAuthStore()
  const notifier = useTransactionNotifier()
  const { $t } = useIntl()

  return (
    <AirdropScreenContent
      enableMainnetContributorPanel={suspenseResource(
        () => appEnv.appEnv$.airdropEnableMainnetContributor,
      )}
      summaryPanel={p => (
        <SummaryPanel
          {...p}
          liquidityToken={store.lpTokenInfo}
          liquidityStages={suspenseResource(() => [
            store.mainnetContributorModule.liquidityProviderStage1Target$,
            store.mainnetContributorModule.liquidityProviderStage2Target$,
          ])}
          currentTotalLiquidity={suspenseResource(() => {
            if (appEnv.appEnv$.airdropEnableMainnetContributor) {
              return store.currentTotalLiquidity.value$
            } else {
              throw new Promise(noop)
            }
          })}
          targetTotalLiquidity={suspenseResource(
            () => store.targetTotalLiquidity$,
          )}
          participantCount={suspenseResource(
            () => store.participantCount.value$,
          )}
          remainingTotalAirdrop={suspenseResource(
            () => store.remainingAirdrop$,
          )}
          remainingUNWAirdrop={suspenseResource(
            () =>
              math`${store.remainingUNWAirdrop$} + ${store.remainingUGPAirdrop$} + ${store.remainingPOAPAirdrop$}`,
          )}
          remainingEsUNWAirdrop={suspenseResource(
            () => store.remainingEsUNWAirdrop$,
          )}
          unwAmountToClaim={suspenseResource(
            () =>
              math`${store.UNWAirdropToClaim$} + ${store.UGPAirdropToClaim$} + ${store.POAPAirdropToClaim$}`,
          )}
          esUNWAmountToClaim={suspenseResource(
            () => store.EsUNWAirdropToClaim$,
          )}
          tgeDetailsLink={
            "https://medium.com/uniwhale/tge-is-here-f630958c0d96"
          }
          onConnectWallet={
            authStore.connected
              ? undefined
              : () => authStore.triggerConnectWalletModal()
          }
          onClaim={suspenseResource(() => {
            const claimables = store.claimables$
            if (claimables.length === 0) {
              return undefined
            }
            return async () => {
              await notifier.broadcastAndShowConfirmation(
                store.claim(claimables),
                $t(
                  defineMessage({
                    defaultMessage: "Claiming Airdrop",
                    description: "AirdropScreen/Summary/ClaimText",
                  }),
                ),
                {
                  refresh: () => {
                    store.refresh.refresh()
                  },
                },
              )
            }
          })}
        />
      )}
      mainnetContributorPanel={p => (
        <MainnetContributorPanel
          {...p}
          renderLiquidityProvidersQuests={p => (
            <LiquidityProvidersQuests
              {...p}
              earnedAsLiquidityProviders={[
                {
                  token: AirdropPoint,
                  amount: safelyGet(
                    () =>
                      store.mainnetContributorModule
                        .earnedUNWAsLiquidityProvider$,
                  ),
                },
              ]}
              stage1Status={suspenseResource(
                () =>
                  store.mainnetContributorModule.liquidityProviderStage1Status$,
              )}
              stage2Status={suspenseResource(
                () =>
                  store.mainnetContributorModule.liquidityProviderStage2Status$,
              )}
              //
              // liquidity stage 1
              //
              stage1LiquidityTarget={suspenseResource(
                () =>
                  store.mainnetContributorModule.liquidityProviderStage1Target$,
              )}
              stage1UNWAwardCount={suspenseResource(
                () =>
                  store.mainnetContributorModule
                    .liquidityProviderStage1UNWAwardCount$,
              )}
              stage1MyULPCount={suspenseResource(
                () =>
                  store.mainnetContributorModule
                    .liquidityProviderStage1MyULPCount$,
              )}
              stage1SnapshotBlockHeight={suspenseResource(
                () =>
                  store.mainnetContributorModule
                    .liquidityProviderStage1SnapshotBlockHeight$,
              )}
              //
              // liquidity stage 1 loyalty phase
              //
              stage1LoyaltyPhaseUNWAwardCount={suspenseResource(
                () =>
                  store.mainnetContributorModule
                    .liquidityProviderStage1LoyaltyPhaseUNWAwardCount$,
              )}
              stage1LoyaltyPhaseIsQualified={suspenseResource(
                () =>
                  store.mainnetContributorModule
                    .liquidityProviderStage1LoyaltyPhaseIsQualified$,
              )}
              stage1LoyaltyPhaseMyULPCount={suspenseResource(
                () =>
                  store.mainnetContributorModule
                    .liquidityProviderStage1LoyaltyPhaseMyULPCount$,
              )}
              stage1LoyaltyPhaseSnapshotBlockHeight={suspenseResource(
                () =>
                  store.mainnetContributorModule
                    .liquidityProviderStage1LoyaltyPhaseSnapshotBlockHeight$,
              )}
              //
              // liquidity stage 2
              //
              stage2LiquidityTarget={suspenseResource(
                () =>
                  store.mainnetContributorModule.liquidityProviderStage2Target$,
              )}
              stage2UNWAwardCount={suspenseResource(
                () =>
                  store.mainnetContributorModule
                    .liquidityProviderStage2UNWAwardCount$,
              )}
              stage2MyULPCount={suspenseResource(
                () =>
                  store.mainnetContributorModule
                    .liquidityProviderStage2MyULPCount$,
              )}
              stage2SnapshotBlockHeight={suspenseResource(
                () =>
                  store.mainnetContributorModule
                    .liquidityProviderStage2SnapshotBlockHeight$,
              )}
            />
          )}
          renderTradersQuests={p => (
            <TradersQuests
              {...p}
              stage1Status={suspenseResource(
                () => store.mainnetContributorModule.traderStage1Status$,
              )}
              stage2Status={suspenseResource(
                () => store.mainnetContributorModule.traderStage2Status$,
              )}
              earnedAsTraders={[
                {
                  token: AirdropPoint,
                  amount: safelyGet(
                    () => store.mainnetContributorModule.earnedUNWAsTrader$,
                  ),
                },
              ]}
              //
              // trade stage 1
              //
              stage1SnapshotBlockHeight={suspenseResource(
                () =>
                  store.mainnetContributorModule
                    .traderStage1SnapshotBlockHeight$,
              )}
              stage1TradeVolumeTarget={suspenseResource(
                () => store.mainnetContributorModule.traderStage1Target$,
              )}
              stage1MyTradeVolume={suspenseResource(
                () => store.mainnetContributorModule.traderStage1MyTradeVolume$,
              )}
              stage1TotalTradeVolume={suspenseResource(
                () =>
                  store.mainnetContributorModule.traderStage1TotalTradeVolume$,
              )}
              stage1UNWAwardCount={suspenseResource(
                () => store.mainnetContributorModule.traderStage1TMyUNWAward$,
              )}
              stage1TotalUNWAwardCount={suspenseResource(
                () => store.mainnetContributorModule.traderStage1FinalUNWAward$,
              )}
              //
              // trade stage 2
              //
              stage2SnapshotBlockHeight={suspenseResource(
                () =>
                  store.mainnetContributorModule
                    .traderStage2SnapshotBlockHeight$,
              )}
              stage2TradeVolumeTarget={suspenseResource(
                () => store.mainnetContributorModule.traderStage2Target$,
              )}
              stage2MyTradeVolume={suspenseResource(
                () => store.mainnetContributorModule.traderStage2MyTradeVolume$,
              )}
              stage2TotalTradeVolume={suspenseResource(
                () =>
                  store.mainnetContributorModule.traderStage2TotalTradeVolume$,
              )}
              stage2UNWAwardCount={suspenseResource(
                () => store.mainnetContributorModule.traderStage2TMyUNWAward$,
              )}
              stage2TotalUNWAwardCount={suspenseResource(
                () => store.mainnetContributorModule.traderStage2FinalUNWAward$,
              )}
            />
          )}
        />
      )}
      ugpPanel={p => (
        <WiredUGPPanel
          {...p}
          showModalInitially={props.showUGPCheckerModalInitially}
        />
      )}
      testnetContributorPanel={p => (
        <TestnetContributorPanel
          {...p}
          totalEarned={suspenseResource(
            () =>
              [
                {
                  token: AirdropPoint,
                  amount: store.testnetContributorModule.totalEarnedUNW$,
                },
              ] as OneOrMore<EarnedItem>,
          )}
          bugReportAward={suspenseResource(() => ({
            token: AirdropPoint,
            amountPerIssue: store.testnetContributorModule.awardUNWPerIssue$,
            wonAmount:
              store.testnetContributorModule.bugReportAwardTotalWonAmount$,
          }))}
          poapHolderAward={suspenseResource(() => ({
            token: AirdropPoint,
            amountPerPoap: store.testnetContributorModule.awardUNWPerPoap$,
            isHolder:
              store.testnetContributorModule.isCurrentAccountPoapHolder$,
          }))}
        />
      )}
    />
  )
}

export const AirdropScreen: FC<
  TopLevelNavigatorScreenProps<"Airdrop">
> = props => {
  return (
    <HideScreenOnBlur>
      <AirdropStoreProvider>
        <Suspense>
          <Content
            showUGPCheckerModalInitially={
              props.route.params?.modal === "ugp-checker"
            }
          />
        </Suspense>
      </AirdropStoreProvider>
    </HideScreenOnBlur>
  )
}
