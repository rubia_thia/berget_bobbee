import { useIsFocused } from "@react-navigation/native"
import React from "react"
import { View } from "react-native"
import { FCC } from "../../utils/reactHelpers/types"

export const HideScreenOnBlur: FCC = ({ children }) => {
  const isFocused = useIsFocused()
  return (
    <View style={isFocused ? undefined : { display: "none" }}>{children}</View>
  )
}
