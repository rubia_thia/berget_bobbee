import { FC, ReactNode } from "react"
import { StyleProp, Text, View, ViewStyle } from "react-native"
import { useSpacing } from "../../components/Themed/spacing"
import {
  renderChildren,
  wrapText,
} from "../../utils/reactHelpers/childrenHelpers"
import { FCC } from "../../utils/reactHelpers/types"
import { useSizeQuery } from "../../utils/reactHelpers/useSizeQuery"
import { styleGetters } from "../../utils/reactHelpers/withProps/styleGetters"
import { withProps } from "../../utils/reactHelpers/withProps/withProps"

export const PageTitle: FCC<{
  style?: StyleProp<ViewStyle>
  descriptionText?: ReactNode
}> = props => {
  const spacing = useSpacing()

  return (
    <View style={props.style}>
      {wrapText(props.children, { Text: PageTitleMainText })}

      {props.descriptionText != null && (
        <PageTitleDescriptionText style={{ marginTop: spacing(1) }}>
          {props.descriptionText}
        </PageTitleDescriptionText>
      )}
    </View>
  )
}
export const PageTitleMainText = withProps(
  styleGetters(({ colors }) => ({
    color: colors("gray-900"),
    fontSize: 30,
    lineHeight: 36,
  })),
  Text,
)
export const PageTitleDescriptionText = withProps(
  styleGetters(({ colors }) => ({
    color: colors("gray-500"),
    fontSize: 12,
    lineHeight: 16,
  })),
  Text,
)
export const PageTitleDescriptionLinkText = withProps(
  styleGetters(({ colors }) => ({
    color: colors("blue-600"),
  })),
  PageTitleDescriptionText,
)

export const PageSectionTitleText = withProps(
  styleGetters(({ colors }) => ({
    color: colors("gray-900"),
    fontSize: 20,
    lineHeight: 28,
  })),
  Text,
)

export interface PageContainerChildrenRenderProps {
  horizontalMargin: number
  horizontalScrollable: boolean
}
export interface PageContainerProps {
  style?: StyleProp<ViewStyle>
  children:
    | ReactNode
    | ((props: Readonly<PageContainerChildrenRenderProps>) => ReactNode)
}

export const PageContainer: FC<PageContainerProps> = props => {
  const paddingHorizontal = 10
  const maxWidth = 1200

  const [
    childrenRenderProps = {
      horizontalScrollable: false,
      horizontalMargin: 0,
    },
    onLayout,
  ] = useSizeQuery({
    0: {
      horizontalScrollable: true,
      horizontalMargin: paddingHorizontal,
    },
    [maxWidth]: {
      horizontalScrollable: false,
      horizontalMargin: 0,
    },
  } as const)

  return (
    <View
      style={[
        props.style,
        {
          marginLeft: "auto",
          marginRight: "auto",
          marginVertical: 40,
          paddingHorizontal:
            typeof props.children === "function" ? 0 : paddingHorizontal,
          width: "100%",
          maxWidth,
        },
      ]}
      onLayout={onLayout}
    >
      {renderChildren(props.children, childrenRenderProps)}
    </View>
  )
}
