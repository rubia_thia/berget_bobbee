export interface CompoundStrategy {
  usdt: "claim" | "stake" | null
  unw: "claim" | "stake" | "stakeAsEsUnw" | null
  esUNW: "claim" | "stake" | null
}
