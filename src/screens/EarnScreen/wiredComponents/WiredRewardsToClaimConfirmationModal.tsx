import { action } from "mobx"
import { useCallback } from "react"
import { defineMessage, useIntl } from "react-intl"
import { useWindowDimensions } from "react-native"
import { Dialog } from "../../../components/DialogProvider/Dialog"
import { useTransactionNotifier } from "../../../components/TransactionNotifier/WiredTransactionNotifier"
import { useEarnStore } from "../../../stores/EarnStore/useEarnStore"
import { math } from "../../../utils/numberHelpers/bigNumberExpressionParser"
import { FCS } from "../../../utils/reactHelpers/types"
import { suspenseResource } from "../../../utils/SuspenseResource"
import { ClaimConfirmModalContent } from "../components/ClaimConfirmModalContent"

export const WiredRewardsToClaimConfirmationModal: FCS = () => {
  const store = useEarnStore()
  const notifier = useTransactionNotifier()
  const { $t } = useIntl()
  const onClaim = useCallback(async () => {
    await notifier.broadcastAndShowConfirmation(
      store.claimRewards(),
      $t(
        defineMessage({
          defaultMessage: "Claiming rewards",
          description: "EarnScreen/ClaimPanel/claim message title",
        }),
      ),
      {
        refresh: () => {
          store.unw.refresh.refresh()
          store.esUnw.refresh.refresh()
          store.currencyStore.refresh.refresh()
        },
      },
    )
  }, [$t, notifier, store])
  const onClose = action(() => (store.showClaimConfirmation = false))
  const { width } = useWindowDimensions()
  return (
    <Dialog
      visible={store.showClaimConfirmation}
      onClose={onClose}
      width={Math.min(500, width - 20)}
    >
      <ClaimConfirmModalContent
        rewardsToClaim={[
          {
            token: store.usdtRewardToken$,
            amount: suspenseResource(() => store.totalRevenueReward$),
            estimatedUSD: suspenseResource(() => store.totalRevenueReward$),
          },
          // {
          //   token: store.unwRewardToken$,
          //   amount: suspenseResource(() => store.totalUNWReward$),
          //   estimatedUSD: suspenseResource(
          //     () => math`${store.totalUNWReward$} * ${store.unw.price$}`,
          //   ),
          // },
          {
            token: store.esUnwRewardToken$,
            amount: suspenseResource(() => store.totalEsUNWReward$),
            estimatedUSD: suspenseResource(
              () => math`${store.totalEsUNWReward$} * ${store.unw.price$}`,
            ),
          },
        ]}
        onClaim={onClaim}
        onDismiss={onClose}
      />
    </Dialog>
  )
}
