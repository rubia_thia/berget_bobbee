import { useState } from "react"
import { defineMessage, useIntl } from "react-intl"
import { approve$t } from "../../../commonIntlMessages"
import { Dialog } from "../../../components/DialogProvider/Dialog"
import { TokenName } from "../../../components/TokenName"
import { useTransactionNotifier } from "../../../components/TransactionNotifier/WiredTransactionNotifier"
import { useEarnStore } from "../../../stores/EarnStore/useEarnStore"
import { waitFor } from "../../../stores/utils/waitFor"
import {
  math,
  mathIs,
} from "../../../utils/numberHelpers/bigNumberExpressionParser"
import { FCS } from "../../../utils/reactHelpers/types"
import { suspenseResource } from "../../../utils/SuspenseResource"
import { TokenInfoPresets } from "../../../utils/TokenInfoPresets/TokenInfoPresets"
import { StakeFormModalContent } from "../components/StakeFormModalContent"
import { EscrowUNWPanel } from "../components/TokenPanels/EscrowUNWPanel"
import { UnstakeFormModalContent } from "../components/UnstakeFormModalContent"

export const WiredEsUNWPanel: FCS = ({ style }) => {
  const { $t } = useIntl()
  const notifier = useTransactionNotifier()

  const store = useEarnStore()
  const { esUnw, unw } = store

  const [confirmStake, setConfirmStake] = useState(false)
  const [confirmUnstake, setConfirmUnstake] = useState(false)

  return (
    <>
      <EscrowUNWPanel
        style={style}
        rewards={[
          {
            token: store.usdtRewardToken$,
            amount: suspenseResource(() => esUnw.revenueReward$),
            valueEstimatedInUSD: suspenseResource(() => esUnw.revenueReward$),
          },
          {
            token: store.esUnwRewardToken$,
            amount: suspenseResource(() => esUnw.esUWNReward$),
            valueEstimatedInUSD: suspenseResource(
              () => math`${esUnw.esUWNReward$} * ${unw.price$}`,
            ),
          },
        ]}
        rewardsValueEstimatedInUSD={suspenseResource(
          () =>
            math`${esUnw.esUWNReward$} * ${unw.price$} + ${esUnw.revenueReward$}`,
        )}
        esUnwPrice={suspenseResource(() => esUnw.price$)}
        esUnwStakingAmount={suspenseResource(() => esUnw.staked$)}
        esUnwWalletBalance={suspenseResource(() => esUnw.balance$)}
        rewardsApr={suspenseResource(() => esUnw.apr$)}
        revenueAPR={suspenseResource(() => esUnw.feeAPR$)}
        emissionAPR={suspenseResource(() => esUnw.emissionAPR$)}
        totalStaked={suspenseResource(() => esUnw.totalStaked$)}
        onStake={suspenseResource(() =>
          mathIs`${esUnw.balance$} > ${0}`
            ? () => setConfirmStake(true)
            : undefined,
        )}
        onUnstake={suspenseResource(() =>
          mathIs`${esUnw.staked$} > ${0}`
            ? () => setConfirmUnstake(true)
            : undefined,
        )}
        onAddToWallet={() => esUnw.addToken()}
      />

      <Dialog visible={confirmStake} onClose={() => setConfirmStake(false)}>
        <StakeFormModalContent
          onDismiss={() => setConfirmStake(false)}
          tokenToStake={TokenInfoPresets.esUNW}
          tokenAmountAvailableToStake={suspenseResource(() => esUnw.balance$)}
          onStake={async amount => {
            const allowance = await waitFor(() => esUnw.currentAllowance$)
            if (mathIs`${amount} > ${allowance}`) {
              const approveTx = await notifier.broadcast(
                esUnw.approve(),
                $t(approve$t),
              )
              await notifier.showConfirmationProgress(
                approveTx,
                $t(approve$t),
                { refresh: () => esUnw.refresh.refresh() },
              )
            }
            await notifier.broadcastAndShowConfirmation(
              esUnw.stake(amount),
              $t(
                defineMessage({
                  defaultMessage: "Staking {token}",
                  description: "EarnScreen/esUNWPanel/Stake message title",
                }),
                { token: <TokenName token={TokenInfoPresets.esUNW} /> },
              ),
              { refresh: () => esUnw.refresh.refresh() },
            )
          }}
        />
      </Dialog>

      <Dialog visible={confirmUnstake} onClose={() => setConfirmUnstake(false)}>
        <UnstakeFormModalContent
          onDismiss={() => setConfirmUnstake(false)}
          tokenToUnstake={store.unwRewardToken$}
          tokenAmountAvailableToUnstake={suspenseResource(() => esUnw.staked$)}
          onUnstake={async amount => {
            await notifier.broadcastAndShowConfirmation(
              esUnw.unstake(amount),
              $t(
                defineMessage({
                  defaultMessage: "Unstaking {token}",
                  description: "EarnScreen/esUNWPanel/Unstake message title",
                }),
                { token: <TokenName token={store.unwRewardToken$} /> },
              ),
              { refresh: () => esUnw.refresh.refresh() },
            )
          }}
        />
      </Dialog>
    </>
  )
}
