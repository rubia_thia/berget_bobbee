import { useState } from "react"
import { defineMessage, useIntl } from "react-intl"
import { approve$t } from "../../../commonIntlMessages"
import { Dialog } from "../../../components/DialogProvider/Dialog"
import { TokenName } from "../../../components/TokenName"
import { useTransactionNotifier } from "../../../components/TransactionNotifier/WiredTransactionNotifier"
import { ERC20Tokens } from "../../../stores/CurrencyStore/CurrencyStore.service"
import { useCurrencyStore } from "../../../stores/CurrencyStore/useCurrencyStore"
import { useEarnStore } from "../../../stores/EarnStore/useEarnStore"
import { waitFor } from "../../../stores/utils/waitFor"
import {
  math,
  mathIs,
} from "../../../utils/numberHelpers/bigNumberExpressionParser"
import { FCS } from "../../../utils/reactHelpers/types"
import { suspenseResource } from "../../../utils/SuspenseResource"
import { BuyUNWModalContent } from "../components/BuyUNWModalContent/BuyUNWModalContent"
import { StakeFormModalContent } from "../components/StakeFormModalContent"
import { UNWPanel } from "../components/TokenPanels/UNWPanel"
import { UnstakeFormModalContent } from "../components/UnstakeFormModalContent"

export const WiredUNWPanel: FCS = ({ style }) => {
  const store = useEarnStore()
  const currencyStore = useCurrencyStore()
  const { unw } = store
  const [confirmStake, setConfirmStake] = useState(false)
  const [confirmUnstake, setConfirmUnstake] = useState(false)
  const [buyUNWModalVisibility, setBuyUNWModalVisibility] = useState(false)
  const notifier = useTransactionNotifier()
  const { $t } = useIntl()
  return (
    <>
      <UNWPanel
        style={style}
        unwWalletBalance={suspenseResource(() => unw.balance$)}
        unwPrice={suspenseResource(() => unw.price$)}
        rewardsValueEstimatedInUSD={suspenseResource(
          () => math`${unw.esUNWReward$} * ${unw.price$}`,
        )}
        rewardsApr={suspenseResource(() => unw.apr$)}
        unwStakingAmount={suspenseResource(() => unw.staked$)}
        totalStaked={suspenseResource(() => unw.totalStaked$)}
        rewards={[
          {
            token: store.esUnwRewardToken$,
            amount: suspenseResource(() => unw.esUNWReward$),
            valueEstimatedInUSD: suspenseResource(
              () => math`${unw.esUNWReward$} * ${unw.price$}`,
            ),
          },
        ]}
        onPressBuyUNW={() => {
          setBuyUNWModalVisibility(true)
        }}
        onStake={suspenseResource(() =>
          mathIs`${unw.balance$} > ${0}`
            ? () => setConfirmStake(true)
            : undefined,
        )}
        onUnstake={suspenseResource(() =>
          mathIs`${unw.staked$} > ${0}`
            ? () => setConfirmUnstake(true)
            : undefined,
        )}
        onAddToWallet={() => unw.addToken()}
      />

      <Dialog
        visible={buyUNWModalVisibility}
        onClose={() => setBuyUNWModalVisibility(false)}
      >
        <BuyUNWModalContent
          onDismiss={() => setBuyUNWModalVisibility(false)}
          buyingToken={store.unwRewardToken$}
          thenaLink={suspenseResource(
            () =>
              `https://www.thena.fi/swap?inputCurrency=0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3&outputCurrency=${currencyStore.contractAddress$(
                ERC20Tokens.UNW,
              )}`,
          )}
          pancakeLink={suspenseResource(
            () =>
              `https://pancakeswap.finance/swap?outputCurrency=${currencyStore.contractAddress$(
                ERC20Tokens.UNW,
              )}`,
          )}
        />
      </Dialog>

      <Dialog visible={confirmStake} onClose={() => setConfirmStake(false)}>
        <StakeFormModalContent
          onDismiss={() => setConfirmStake(false)}
          tokenToStake={store.unwRewardToken$}
          tokenAmountAvailableToStake={suspenseResource(() => unw.balance$)}
          onStake={async amount => {
            const allowance = await waitFor(() => unw.currentAllowance$)
            if (mathIs`${amount} > ${allowance}`) {
              const approveTx = await notifier.broadcast(
                unw.approve(),
                $t(approve$t),
              )
              await notifier.showConfirmationProgress(
                approveTx,
                $t(approve$t),
                { refresh: () => unw.refresh.refresh() },
              )
            }
            await notifier.broadcastAndShowConfirmation(
              unw.stake(amount),
              $t(
                defineMessage({
                  defaultMessage: "Staking {token}",
                  description: "EarnScreen/UNWPanel/Stake message title",
                }),
                { token: <TokenName token={store.unwRewardToken$} /> },
              ),
              { refresh: () => unw.refresh.refresh() },
            )
          }}
        />
      </Dialog>

      <Dialog visible={confirmUnstake} onClose={() => setConfirmUnstake(false)}>
        <UnstakeFormModalContent
          onDismiss={() => setConfirmUnstake(false)}
          tokenToUnstake={store.unwRewardToken$}
          tokenAmountAvailableToUnstake={suspenseResource(() => unw.staked$)}
          onUnstake={async amount => {
            await notifier.broadcastAndShowConfirmation(
              unw.unstake(amount),
              $t(
                defineMessage({
                  defaultMessage: "Unstaking {token}",
                  description: "EarnScreen/UNWPanel/Unstake message title",
                }),
                { token: <TokenName token={store.unwRewardToken$} /> },
              ),
              { refresh: () => unw.refresh.refresh() },
            )
          }}
        />
      </Dialog>
    </>
  )
}
