import { action } from "mobx"
import { useCallback, useState } from "react"
import { defineMessage, useIntl } from "react-intl"
import { Dialog } from "../../../components/DialogProvider/Dialog"
import { useTransactionNotifier } from "../../../components/TransactionNotifier/WiredTransactionNotifier"
import { useEarnStore } from "../../../stores/EarnStore/useEarnStore"
import { FCS } from "../../../utils/reactHelpers/types"
import { suspenseResource } from "../../../utils/SuspenseResource"
import { CompoundModalContent } from "../components/CompoundModalContent/CompoundModalContent"
import { CompoundStrategy } from "../types"

export const WiredCompoundModalContentModal: FCS = () => {
  const store = useEarnStore()
  const notifier = useTransactionNotifier()
  const { $t } = useIntl()
  const onClose = action(() => (store.showCompoundConfirmation = false))
  const [strategy, setStrategy] = useState<CompoundStrategy>({
    esUNW: "stake",
    unw: "stakeAsEsUnw",
    usdt: "stake",
  })
  const onClaim = useCallback(async () => {
    await notifier.broadcastAndShowConfirmation(
      store.compoundRewards(strategy),
      $t(
        defineMessage({
          defaultMessage: "Claiming rewards",
          description: "EarnScreen/ClaimPanel/claim message title",
        }),
      ),
      {
        refresh: () => {
          store.unw.refresh.refresh()
          store.esUnw.refresh.refresh()
          store.currencyStore.refresh.refresh()
        },
      },
    )
  }, [$t, notifier, store, strategy])
  return (
    <Dialog
      visible={store.showCompoundConfirmation}
      onClose={onClose}
      width={500}
    >
      <CompoundModalContent
        strategy={strategy}
        usdtTokenInfo={store.usdtRewardToken$}
        usdtAmount={suspenseResource(() => store.totalRevenueReward$)}
        usdtStakeAsULPAPR={suspenseResource(() => store.ulp.apr$)}
        unwTokenInfo={store.unwRewardToken$}
        unwAmount={suspenseResource(() => store.totalUNWReward$)}
        uwnStakeAsEsUNWAPR={suspenseResource(() => store.esUnw.apr$)}
        uwnStakeAPR={suspenseResource(() => store.unw.apr$)}
        esUNWTokenInfo={store.esUnwRewardToken$}
        esUNWAmount={suspenseResource(() => store.totalEsUNWReward$)}
        esUNWStakeAPR={suspenseResource(() => store.esUnw.apr$)}
        onStrategyChange={setStrategy}
        onSubmit={onClaim}
        onDismiss={onClose}
      />
    </Dialog>
  )
}
