import { action } from "mobx"
import { defineMessage, useIntl } from "react-intl"
import { useTransactionNotifier } from "../../../components/TransactionNotifier/WiredTransactionNotifier"
import { useAppEnvStore } from "../../../stores/AppEnvStore/useAppEnvStore"
import { useEarnStore } from "../../../stores/EarnStore/useEarnStore"
import { safelyGet } from "../../../stores/utils/waitFor"
import { math } from "../../../utils/numberHelpers/bigNumberExpressionParser"
import { FCS } from "../../../utils/reactHelpers/types"
import { suspenseResource } from "../../../utils/SuspenseResource"
import { RewardsToClaimPanel } from "../components/RewardsToClaimPanel"

export const WiredRewardsToClaimPanel: FCS = ({ style }) => {
  const store = useEarnStore()
  const env = useAppEnvStore()
  const notifier = useTransactionNotifier()
  const { $t } = useIntl()
  return (
    <RewardsToClaimPanel
      style={style}
      rewardsEstimatedUSD={suspenseResource(() => store.totalClaimableInUSDT$)}
      rewardsToClaim={{
        usdt: {
          token: store.usdtRewardToken$,
          amount: suspenseResource(() => store.totalRevenueReward$),
          valueInUSD: suspenseResource(() => store.totalRevenueReward$),
        },
        esUNW: {
          token: store.esUnwRewardToken$,
          traderReward: suspenseResource(() => store.traderRewards$),
          stakeReward: suspenseResource(() => store.totalStakeEsUWNReward$),
          valueInUSD: suspenseResource(
            () => math`${store.totalEsUNWReward$} * ${store.unw.price$}`,
          ),
          amount: suspenseResource(() => store.totalEsUNWReward$),
        },
        unw: {
          token: store.unwRewardToken$,
          amount: suspenseResource(() => store.vestEsUnw.vested$),
          valueInUSD: suspenseResource(
            () => math`${store.vestEsUnw.vested$} * ${store.unw.price$}`,
          ),
        },
      }}
      enableCompound={env.appEnv$.earn_enableCompound}
      onClaim={
        safelyGet(() => store.haveAnyReward$)
          ? action(compound => {
              if (compound) {
                store.showCompoundConfirmation = true
              } else {
                store.showClaimConfirmation = true
              }
            })
          : undefined
      }
      canVest={safelyGet(() => store.vestEsUnw.hasVested$) === true}
      onVest={async () => {
        await notifier.broadcastAndShowConfirmation(
          store.vestEsUnw.vest(),
          $t(
            defineMessage({
              defaultMessage: "Vesting",
              description: "EarnScreen/RewardsToClaim/claim button",
            }),
          ),
          {
            refresh: () => {
              store.vestEsUnw.refresh.refresh()
            },
          },
        )
      }}
    />
  )
}
