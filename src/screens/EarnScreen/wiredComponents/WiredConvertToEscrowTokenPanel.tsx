import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { approve$t } from "../../../commonIntlMessages"
import { Dialog } from "../../../components/DialogProvider/Dialog"
import { TokenName } from "../../../components/TokenName"
import { useTransactionNotifier } from "../../../components/TransactionNotifier/WiredTransactionNotifier"
import { useEarnStore } from "../../../stores/EarnStore/useEarnStore"
import { waitFor } from "../../../stores/utils/waitFor"
import { mathIs } from "../../../utils/numberHelpers/bigNumberExpressionParser"
import { MarginStyle } from "../../../utils/styleHelpers/MarginStyle"
import {
  DELAYED,
  readResource,
  suspenseResource,
} from "../../../utils/SuspenseResource"
import { TokenInfoPresets } from "../../../utils/TokenInfoPresets/TokenInfoPresets"
import { ConvertToEscrowTokenFormModelContent } from "../components/ConvertToEscrowTokenFormModelContent"
import { ConvertToEscrowTokenPanel } from "../components/ConvertToEscrowTokenPanel"
import { convertToEscrowTokenPanelProps } from "../components/ConvertToEscrowTokenPanel.mockData"

export const WiredConvertToEscrowTokenPanel: FC<{
  style: MarginStyle
}> = props => {
  const store = useEarnStore()

  const notifier = useTransactionNotifier()
  const { $t } = useIntl()

  const { unwConvertToEsUNWModule, unw, esUnw } = store

  return (
    <>
      <ConvertToEscrowTokenPanel
        style={props.style}
        {
          ...convertToEscrowTokenPanelProps /* TODO */
        }
        esTokenApr={suspenseResource(() => esUnw.apr$)}
        onConvert={() => {
          store.onStartConvertUNWToEsUNW()
        }}
      />

      <Dialog
        visible={unwConvertToEsUNWModule != null}
        onClose={() => store.onStopConvertUNWToEsUNW()}
      >
        <ConvertToEscrowTokenFormModelContent
          onDismiss={() => store.onStopConvertUNWToEsUNW()}
          token={TokenInfoPresets.UNW}
          esToken={TokenInfoPresets.esUNW}
          availableTokenAmount={suspenseResource(() => unw.balance$)}
          tokenAmount={suspenseResource(
            () => unwConvertToEsUNWModule?.tokenAmount.read$ ?? null,
          )}
          onTokenAmountChange={v =>
            unwConvertToEsUNWModule?.tokenAmount.set(v ?? undefined)
          }
          onPressMax={suspenseResource(() => {
            const { balance$ } = unw
            return () => unwConvertToEsUNWModule?.tokenAmount.set(balance$)
          })}
          willReceiveEsTokenAmount={suspenseResource(
            () =>
              unwConvertToEsUNWModule?.tokenAmount.read$ ??
              readResource(DELAYED()),
          )}
          vestPeriod={suspenseResource(() => store.vestEsUnw.vestingDuration$)}
          onConvert={suspenseResource(() => {
            if (unwConvertToEsUNWModule == null) return

            const amount = unwConvertToEsUNWModule.tokenAmount.get()

            if (
              amount == null ||
              mathIs`${amount} <= ${0} || ${amount} > ${
                unw.balance$ ?? Number.MAX_VALUE
              }`
            ) {
              return undefined
            }

            return async () => {
              const allowance = await waitFor(
                () => unwConvertToEsUNWModule.currentAllowance$,
              )
              if (mathIs`${amount} > ${allowance}`) {
                const approveTx = await notifier.broadcast(
                  unwConvertToEsUNWModule.approve(),
                  $t(approve$t),
                )
                await notifier.showConfirmationProgress(
                  approveTx,
                  $t(approve$t),
                )
              }
              await notifier.broadcastAndShowConfirmation(
                unwConvertToEsUNWModule.convert(amount),
                $t(
                  defineMessage({
                    defaultMessage: "Convert {token} to {token}",
                    description:
                      "EarnScreen/ConvertToEscrowTokenPanel/Convert message title",
                  }),
                  {
                    token: <TokenName token={TokenInfoPresets.UNW} />,
                    esToken: <TokenName token={TokenInfoPresets.esUNW} />,
                  },
                ),
                {
                  refresh: () => {
                    unw.refresh.refresh()
                    esUnw.refresh.refresh()
                  },
                },
              )
            }
          })}
        />
      </Dialog>
    </>
  )
}
