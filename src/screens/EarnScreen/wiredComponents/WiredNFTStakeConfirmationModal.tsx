import { action } from "mobx"
import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { Dialog } from "../../../components/DialogProvider/Dialog"
import { useTransactionNotifier } from "../../../components/TransactionNotifier/WiredTransactionNotifier"
import { useEarnStore } from "../../../stores/EarnStore/useEarnStore"
import { suspenseResource } from "../../../utils/SuspenseResource"
import { StakeNFTConfirmModalContent } from "../components/StakeNFTConfirmModalContent"

export const WiredNFTStakeConfirmationModal: FC = () => {
  const { ugp: store } = useEarnStore()
  const onClose = action(() => (store.showStakeConfirmation = false))
  const { $t } = useIntl()
  const notifier = useTransactionNotifier()
  return (
    <Dialog visible={store.showStakeConfirmation} onClose={onClose}>
      <StakeNFTConfirmModalContent
        onDismiss={onClose}
        ownedNFTs={suspenseResource(() => store.availableUGPToStake$)}
        onSelectNFT={action(
          (contractAddress, nftIndex) =>
            (store.selectedUGPToStake = { contractAddress, nftIndex }),
        )}
        selectedNFT={suspenseResource(() => store.selectedUGPToStake ?? null)}
        onStake={async () => {
          await notifier.broadcastAndShowConfirmation(
            store.stakeUGP(),
            $t(
              defineMessage({
                defaultMessage: "Stake UGP",
                description: "EarnScreen/UGPPanel/stake message title",
              }),
            ),
          )
          onClose()
        }}
      />
    </Dialog>
  )
}
