import { useState } from "react"
import { defineMessage, useIntl } from "react-intl"
import { Dialog } from "../../../components/DialogProvider/Dialog"
import { useTransactionNotifier } from "../../../components/TransactionNotifier/WiredTransactionNotifier"
import { useEarnStore } from "../../../stores/EarnStore/useEarnStore"
import { waitFor } from "../../../stores/utils/waitFor"
import {
  math,
  mathIs,
} from "../../../utils/numberHelpers/bigNumberExpressionParser"
import { FCS } from "../../../utils/reactHelpers/types"
import { suspenseResource } from "../../../utils/SuspenseResource"
import { TokenInfoPresets } from "../../../utils/TokenInfoPresets/TokenInfoPresets"
import { LockEscrowTokenFormModelContent } from "../components/LockEscrowTokenFormModelContent"
import { UnlockEscrowTokenFormModelContent } from "../components/UnlockEscrowTokenFormModelContent"
import { VestPanel } from "../components/VestPanel"

export const WiredVestPanel: FCS = ({ style }) => {
  const store = useEarnStore()
  const [locking, setLocking] = useState(false)
  const [unlocking, setUnlocking] = useState(false)
  const notifier = useTransactionNotifier()
  const { $t } = useIntl()
  return (
    <>
      <VestPanel
        style={style}
        vestingDetailsLink={
          "https://docs.uniwhale.co/tokenomics/comparison-of-unw-and-esunw"
        }
        vestPeriod={suspenseResource(() => store.vestEsUnw.vestingDuration$)}
        activeVestingAmount={suspenseResource(
          () => store.vestEsUnw.lockedAmount$,
        )}
        claimableVest={suspenseResource(() => ({
          fromEsUNIAmount: math`${store.vestEsUnw.vested$} / ${store.vestEsUnw.vestingRate$}`,
          toUNWAmount: store.vestEsUnw.vested$,
        }))}
        canLock={suspenseResource(
          () => mathIs`${store.vestEsUnw.balance$} > ${0}`,
        )}
        canUnlock={suspenseResource(
          () => mathIs`${store.vestEsUnw.lockedAmount$} > ${0}`,
        )}
        onLock={() => setLocking(true)}
        onUnlock={() => setUnlocking(true)}
      />
      <Dialog visible={locking} onClose={() => setLocking(false)}>
        <LockEscrowTokenFormModelContent
          onDismiss={() => setLocking(false)}
          esToken={TokenInfoPresets.esUNW}
          token={TokenInfoPresets.UNW}
          availableEsTokenAmount={suspenseResource(
            () => store.vestEsUnw.balance$,
          )}
          esTokenAmount={suspenseResource(
            () => store.vestEsUnw.amountToLock.read$,
          )}
          onEsTokenAmountChange={a =>
            store.vestEsUnw.amountToLock.set(a ?? undefined)
          }
          onPressMax={() => {
            store.vestEsUnw.amountToLock.set(store.vestEsUnw.balance$)
          }}
          willBeVestedTokenCount={suspenseResource(
            () =>
              math`${store.vestEsUnw.amountToLock.read$} * ${store.vestEsUnw.vestingRate$}`,
          )}
          vestPeriod={suspenseResource(() => store.vestEsUnw.vestingDuration$)}
          vestingDetailsLink={
            "https://docs.uniwhale.co/tokenomics/comparison-of-unw-and-esunw"
          }
          onLock={async () => {
            const allowance = await waitFor(
              () => store.vestEsUnw.currentAllowance$,
            )
            const amount = store.vestEsUnw.amountToLock.read$
            if (mathIs`${amount} > ${allowance}`) {
              const approveText = $t(
                defineMessage({
                  defaultMessage: "Approve",
                  description: "EarnScreen/VestPanel/lock message title",
                }),
              )
              const approvedTx = await notifier.broadcast(
                store.vestEsUnw.approve(),
                approveText,
              )
              await notifier.showConfirmationProgress(approvedTx, approveText)
            }
            await notifier.broadcastAndShowConfirmation(
              store.vestEsUnw.lock(amount),
              $t(
                defineMessage({
                  defaultMessage: "Locking tokens",
                  description: "EarnScreen/VestPanel/lock message title",
                }),
              ),
              {
                refresh: () => {
                  store.vestEsUnw.refresh.refresh()
                  store.esUnw.refresh.refresh()
                },
              },
            )
            setLocking(false)
          }}
        />
      </Dialog>
      <Dialog visible={unlocking} onClose={() => setUnlocking(false)}>
        <UnlockEscrowTokenFormModelContent
          onDismiss={() => setUnlocking(false)}
          esToken={TokenInfoPresets.esUNW}
          availableEsTokenAmount={suspenseResource(
            () => store.vestEsUnw.remainingLockedAmount$,
          )}
          esTokenAmount={suspenseResource(
            () => store.vestEsUnw.amountToUnlock.read$,
          )}
          onEsTokenAmountChange={a =>
            store.vestEsUnw.amountToUnlock.set(a ?? undefined)
          }
          onPressMax={() => {
            store.vestEsUnw.amountToUnlock.set(
              store.vestEsUnw.remainingLockedAmount$,
            )
          }}
          esTokenAmountBeforeUnlock={suspenseResource(
            () => store.vestEsUnw.remainingLockedAmount$,
          )}
          esTokenAmountAfterUnlock={suspenseResource(
            () =>
              math`${store.vestEsUnw.remainingLockedAmount$} - ${store.vestEsUnw.amountToUnlock.read$}`,
          )}
          onUnlock={async () => {
            const amount = store.vestEsUnw.amountToUnlock.read$
            await notifier.broadcastAndShowConfirmation(
              store.vestEsUnw.unlock(amount),
              $t(
                defineMessage({
                  defaultMessage: "Unlocking tokens",
                  description: "EarnScreen/VestPanel/unlock message title",
                }),
              ),
              {
                refresh: () => {
                  store.vestEsUnw.refresh.refresh()
                  store.esUnw.refresh.refresh()
                },
              },
            )
            setLocking(false)
          }}
        />
      </Dialog>
    </>
  )
}
