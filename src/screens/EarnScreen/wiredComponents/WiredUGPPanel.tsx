import { action } from "mobx"
import { defineMessage, useIntl } from "react-intl"
import { useTransactionNotifier } from "../../../components/TransactionNotifier/WiredTransactionNotifier"
import { useEarnStore } from "../../../stores/EarnStore/useEarnStore"
import { safelyGet } from "../../../stores/utils/waitFor"
import { FCS } from "../../../utils/reactHelpers/types"
import { suspenseResource } from "../../../utils/SuspenseResource"
import { UGPPanel } from "../components/TokenPanels/UGPPanel"
import { ugpPanelProps } from "../components/TokenPanels/UGPPanel.mockData"

export const WiredUGPPanel: FCS = ({ style }) => {
  const { ugp: store } = useEarnStore()
  const canStake = safelyGet(() => store.canStake$)
  const canUnStake = safelyGet(() => store.canUnStake$)
  const notifier = useTransactionNotifier()
  const { $t } = useIntl()
  return (
    <UGPPanel
      style={style}
      {...ugpPanelProps}
      spreadReductionPercentage={suspenseResource(
        () => store.spreadReductionPercentage$,
      )}
      ugpStakedIndex={suspenseResource(() => store.ugpStakedIndex$)}
      tradingFeeDiscountPercentage={suspenseResource(
        () => store.tradingFeeDiscountPercentage$,
      )}
      onStake={
        canStake
          ? action(() => {
              store.showStakeConfirmation = true
            })
          : undefined
      }
      onUnstake={
        canUnStake
          ? async () => {
              await notifier.broadcastAndShowConfirmation(
                store.unstakeUGP(),
                $t(
                  defineMessage({
                    defaultMessage: "Unstake UGP",
                    description: "EarnScreen/UGPPanel/unstake message title",
                  }),
                ),
              )
            }
          : undefined
      }
    />
  )
}
