import { useNavigation } from "@react-navigation/native"
import { FC, Suspense } from "react"
import { StyleProp, ViewStyle } from "react-native"
import { Spensor } from "../../components/Spensor"
import { TopLevelNavigatorScreenProps } from "../../navigation/navigators/TopLevelNavigatorTypes"
import { useAppEnvStore } from "../../stores/AppEnvStore/useAppEnvStore"
import {
  EarnStoreProvider,
  useEarnStore,
} from "../../stores/EarnStore/useEarnStore"
import { hasAny } from "../../utils/arrayHelpers"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { math } from "../../utils/numberHelpers/bigNumberExpressionParser"
import { FCS } from "../../utils/reactHelpers/types"
import { navLinkInfo } from "../../utils/reactNavigationHelpers/reactNavigationHelpers"
import { suspenseResource } from "../../utils/SuspenseResource"
import { isNotNull } from "../../utils/typeHelpers"
import { HideScreenOnBlur } from "../components/HideScreenOnBlur"
import {
  PoolInfo,
  ProvideLiquidityPanel,
} from "./components/ProvideLiquidityPanel/ProvideLiquidityPanel"
import { ULPPanel } from "./components/TokenPanels/ULPPanel"
import { EarnScreenContent } from "./EarnScreenContent"
import { WiredCompoundModalContentModal } from "./wiredComponents/WiredCompoundModalContentModal"
import { WiredConvertToEscrowTokenPanel } from "./wiredComponents/WiredConvertToEscrowTokenPanel"
import { WiredEsUNWPanel } from "./wiredComponents/WiredEsUNWPanel"
import { WiredNFTStakeConfirmationModal } from "./wiredComponents/WiredNFTStakeConfirmationModal"
import { WiredRewardsToClaimConfirmationModal } from "./wiredComponents/WiredRewardsToClaimConfirmationModal"
import { WiredRewardsToClaimPanel } from "./wiredComponents/WiredRewardsToClaimPanel"
import { WiredUGPPanel } from "./wiredComponents/WiredUGPPanel"
import { WiredUNWPanel } from "./wiredComponents/WiredUNWPanel"
import { WiredVestPanel } from "./wiredComponents/WiredVestPanel"

const WiredULPPanel: FCS = ({ style }) => {
  const nav =
    useNavigation<TopLevelNavigatorScreenProps<"Earn">["navigation"]>()
  const store = useEarnStore()
  const { ulp, unw, esUnwRewardToken$, usdtRewardToken$ } = store

  return (
    <ULPPanel
      style={style}
      buyUlpNavLinkInfo={navLinkInfo({
        navigation: nav,
        to: { screen: "Liquidity" },
      })}
      redeemUlpNavLinkInfo={navLinkInfo({
        navigation: nav,
        to: { screen: "Liquidity" },
      })}
      revenueAPR={suspenseResource(() => ulp.feeAPR$)}
      emissionAPR={suspenseResource(() => ulp.emissionAPR$)}
      rewards={[
        {
          token: usdtRewardToken$,
          amount: suspenseResource(() => ulp.revenueReward$),
          valueEstimatedInUSD: suspenseResource(() => ulp.revenueReward$),
        },
        {
          token: esUnwRewardToken$,
          amount: suspenseResource(() => ulp.esUwnReward$),
          valueEstimatedInUSD: suspenseResource(
            () => math`${ulp.esUwnReward$} * ${unw.price$}`,
          ),
        },
      ]}
      rewardsValueEstimatedInUSD={suspenseResource(
        () => math`${ulp.esUwnReward$} * ${unw.price$} + ${ulp.revenueReward$}`,
      )}
      ulpStakingAmount={suspenseResource(() => ulp.staked$)}
      rewardsApr={suspenseResource(() => ulp.apr$)}
      ulpPrice={suspenseResource(() => ulp.price$)}
    />
  )
}

const WiredProvideLiquidityPanel: FC<{
  style?: StyleProp<ViewStyle>
}> = props => {
  const env = useAppEnvStore()

  return (
    <Spensor>
      {() => {
        const pools: PoolInfo[] = [
          env.appEnv$.earn_pancakeLiquidityPoolLink != null
            ? {
                type: "pancake" as const,
                link: env.appEnv$.earn_pancakeLiquidityPoolLink,
                apr:
                  env.appEnv$.earn_pancakeLiquidityPoolApr != null
                    ? BigNumber.from(env.appEnv$.earn_pancakeLiquidityPoolApr)
                    : undefined,
              }
            : null,
          env.appEnv$.earn_thenaLiquidityPoolLink != null
            ? {
                type: "thena" as const,
                link: env.appEnv$.earn_thenaLiquidityPoolLink,
                apr:
                  env.appEnv$.earn_thenaLiquidityPoolApr != null
                    ? BigNumber.from(env.appEnv$.earn_thenaLiquidityPoolApr)
                    : undefined,
              }
            : null,
        ].filter(isNotNull)

        if (!hasAny(pools)) return null

        return <ProvideLiquidityPanel style={props.style} pools={pools} />
      }}
    </Spensor>
  )
}

export const EarnScreen: FC<TopLevelNavigatorScreenProps<"Earn">> = () => {
  return (
    <EarnStoreProvider>
      <HideScreenOnBlur>
        <Suspense>
          <EarnScreenContent
            claimPanel={p => <WiredRewardsToClaimPanel style={p.style} />}
            liquidityPanel={p => <WiredProvideLiquidityPanel style={p.style} />}
            convertPanel={p => (
              <WiredConvertToEscrowTokenPanel style={p.style} />
            )}
            unwPanel={p => <WiredUNWPanel style={p.style} />}
            ulpPanel={p => <WiredULPPanel style={p.style} />}
            ugpPanel={p => <WiredUGPPanel style={p.style} />}
            esUnwPanel={p => <WiredEsUNWPanel style={p.style} />}
            vestPanel={p => <WiredVestPanel style={p.style} />}
          />
          <WiredRewardsToClaimConfirmationModal />
          <WiredCompoundModalContentModal />
          <WiredNFTStakeConfirmationModal />
        </Suspense>
      </HideScreenOnBlur>
    </EarnStoreProvider>
  )
}
