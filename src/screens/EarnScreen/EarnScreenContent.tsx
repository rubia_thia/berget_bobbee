import { FC, ReactNode } from "react"
import { defineMessage, useIntl } from "react-intl"
import { FlexStyle, StyleProp, View, ViewStyle } from "react-native"
import { useSpacing } from "../../components/Themed/spacing"
import { useSizeQuery } from "../../utils/reactHelpers/useSizeQuery"
import { MarginStyle } from "../../utils/styleHelpers/MarginStyle"
import {
  PageContainer,
  PageTitleMainText,
} from "../components/pageLayoutPresets"
import { usePanelContainerGap } from "./components/misc"

export const EarnScreenContent: FC<{
  style?: StyleProp<ViewStyle>
  claimPanel: (renderProps: { style: MarginStyle }) => ReactNode
  liquidityPanel: (renderProps: { style: MarginStyle }) => ReactNode
  convertPanel: (renderProps: { style: MarginStyle }) => ReactNode
  unwPanel: (renderProps: {
    style: StyleProp<MarginStyle & FlexStyle>
  }) => ReactNode
  ulpPanel: (renderProps: {
    style: StyleProp<MarginStyle & FlexStyle>
  }) => ReactNode
  ugpPanel: (renderProps: {
    style: StyleProp<MarginStyle & FlexStyle>
  }) => ReactNode
  esUnwPanel: (renderProps: {
    style: StyleProp<MarginStyle & FlexStyle>
  }) => ReactNode
  vestPanel: (renderProps: {
    style: StyleProp<MarginStyle & FlexStyle>
  }) => ReactNode
}> = props => {
  const { $t } = useIntl()

  const spacing = useSpacing()

  const { vertical: verticalGap, horizontal: horizontalGap } =
    usePanelContainerGap()

  const [stackPanelRowLayout = "row", onStackPanelRowLayout] = useSizeQuery({
    0: "col",
    800: "row",
  } as const)

  return (
    <PageContainer style={props.style}>
      <PageTitleMainText>
        {$t(
          defineMessage({
            defaultMessage: "Earn",
            description: "EarnScreen/title",
          }),
        )}
      </PageTitleMainText>

      {props.claimPanel({
        style: { marginTop: spacing(5) },
      })}

      {props.liquidityPanel({
        style: { marginTop: spacing(2.5) },
      })}

      <View
        style={[
          { marginTop: verticalGap },
          stackPanelRowLayout === "row"
            ? { flexDirection: "row" }
            : { flexDirection: "column" },
        ]}
        onLayout={onStackPanelRowLayout}
      >
        {props.unwPanel({ style: { flex: 1 } })}

        {props.ulpPanel({
          style: [
            { flex: 1 },
            stackPanelRowLayout === "row"
              ? { marginLeft: horizontalGap }
              : { marginTop: horizontalGap },
          ],
        })}
      </View>

      <View
        style={[
          { marginTop: verticalGap },
          stackPanelRowLayout === "row"
            ? { flexDirection: "row" }
            : { flexDirection: "column" },
        ]}
      >
        {props.esUnwPanel({
          style: { flex: 1 },
        })}

        {props.ugpPanel({
          style: [
            { flex: 1 },
            stackPanelRowLayout === "row"
              ? { marginLeft: horizontalGap }
              : { marginTop: horizontalGap },
          ],
        })}
      </View>

      {props.convertPanel({
        style: { marginTop: verticalGap },
      })}

      {props.vestPanel({
        style: [{ marginTop: verticalGap }],
      })}
    </PageContainer>
  )
}
