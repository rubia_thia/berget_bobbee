import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../utils/TokenInfoPresets/TokenInfoPresets"
import { UnlockEscrowTokenFormModelContent } from "./UnlockEscrowTokenFormModelContent"

export default {
  title: "Page/EarnScreen/UnlockEscrowTokenFormModelContent",
  component: UnlockEscrowTokenFormModelContent,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof UnlockEscrowTokenFormModelContent>

const template = withTemplate(UnlockEscrowTokenFormModelContent, {
  style: {
    margin: 10,
  },
  esToken: TokenInfoPresets.esUNW,
  availableEsTokenAmount: BigNumber.from(1000),
  esTokenAmount: BigNumber.from(20),
  esTokenAmountBeforeUnlock: BigNumber.from(1111),
  esTokenAmountAfterUnlock: BigNumber.from(1091),
})

export const Normal = template()
