import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { StakeNFTConfirmModalContent } from "./StakeNFTConfirmModalContent"

export default {
  title: "Page/EarnScreen/StakeNFTConfirmModalContent",
  component: StakeNFTConfirmModalContent,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof StakeNFTConfirmModalContent>

const template = withTemplate(StakeNFTConfirmModalContent, {
  selectedNFT: null,
  ownedNFTs: [
    {
      contractAddress: "0x47325F13873AdBD926a0B05C9Bb1DaaaA2c167F3",
      nftIndex: 1,
    },
    {
      contractAddress: "0x47325F13873AdBD926a0B05C9Bb1DaaaA2c167F3",
      nftIndex: 2,
    },
  ],
})

export const Normal = template()
export const SelectedNormal = template(a => {
  a.selectedNFT = {
    contractAddress: "0x47325F13873AdBD926a0B05C9Bb1DaaaA2c167F3",
    nftIndex: 1,
  }
})
