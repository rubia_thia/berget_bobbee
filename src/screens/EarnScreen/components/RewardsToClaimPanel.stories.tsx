import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { RewardsToClaimPanel } from "./RewardsToClaimPanel"
import { rewardsToClaimProps } from "./RewardsToClaimPanel.mockData"

export default {
  title: "Page/EarnScreen/RewardsToClaimPanel",
  component: RewardsToClaimPanel,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof RewardsToClaimPanel>

const template = withTemplate(RewardsToClaimPanel, {
  style: {
    margin: 10,
  },
  enableCompound: true,
  ...rewardsToClaimProps,
})

export const Normal = template()

export const ColLayout = template(p => {
  p.style = { maxWidth: 500 }
  p.layout = "col"
})
