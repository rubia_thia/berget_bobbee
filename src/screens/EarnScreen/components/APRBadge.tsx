import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, Text, View, ViewStyle } from "react-native"
import { PercentNumber } from "../../../components/PercentNumber"
import { useColors } from "../../../components/Themed/color"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { SuspenseResource } from "../../../utils/SuspenseResource"

export const APRBadge: FC<{
  style?: StyleProp<ViewStyle>
  apr: SuspenseResource<BigNumber>
}> = props => {
  const colors = useColors()
  const { $t } = useIntl()

  return (
    <View
      style={[
        props.style,
        {
          paddingHorizontal: 6,
          paddingVertical: 1,
          borderRadius: 2,
          backgroundColor: colors("green-100"),
        },
      ]}
    >
      <Text
        style={{
          fontSize: 10,
          lineHeight: 16,
          color: colors("green-600"),
        }}
      >
        {$t(
          defineMessage({
            defaultMessage: "APR {apr}",
            description: "EarnScreen/ConvertToEscrowTokenPanel/apr badge",
          }),
          {
            apr: <PercentNumber number={props.apr} />,
          },
        )}
      </Text>
    </View>
  )
}
