import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import {
  StyleProp,
  Text,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import { WhiteOutlineButtonVariant } from "../../../components/Button/WhiteOutlineButtonVariant"
import { Button } from "../../../components/ButtonFramework/Button"
import { InfoListItem } from "../../../components/InfoList/InfoListItem"
import {
  InfoListItemDetail,
  InfoListItemDetailText,
} from "../../../components/InfoList/InfoListItemDetail"
import { InfoListItemTitle } from "../../../components/InfoList/InfoListItemTitle"
import { Spensor, SpensorR } from "../../../components/Spensor"
import { TextTokenCount } from "../../../components/TextTokenCount"
import { useColors } from "../../../components/Themed/color"
import { useSpacing } from "../../../components/Themed/spacing"
import { TokenName } from "../../../components/TokenName"
import { formatDuration } from "../../../utils/datetimeHelpers"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { HrefLink } from "../../../utils/reactNavigationHelpers/HrefLink"
import {
  readResource,
  safeReadResource,
  SuspenseResource,
} from "../../../utils/SuspenseResource"
import { TokenInfoPresets } from "../../../utils/TokenInfoPresets/TokenInfoPresets"
import { BreakdownList } from "./BreakdownList"
import {
  PanelContainer,
  PanelHeader,
  PanelHeaderTitleText,
  PanelInset,
  PanelInsetGroup,
} from "./misc"

export interface VestPanelProps {
  style?: StyleProp<ViewStyle>
  vestingDetailsLink: string
  vestPeriod: SuspenseResource<Duration>
  activeVestingAmount: SuspenseResource<BigNumber>
  claimableVest: SuspenseResource<{
    fromEsUNIAmount: BigNumber
    toUNWAmount: BigNumber
  }>
  canLock: SuspenseResource<boolean>
  canUnlock: SuspenseResource<boolean>
  onLock: () => void
  onUnlock: () => void
}

export const VestPanel: FC<VestPanelProps> = props => {
  const spacing = useSpacing()
  const colors = useColors()
  const intl = useIntl()
  const { $t } = intl

  return (
    <PanelContainer style={[props.style, { flexDirection: "column" }]}>
      <PanelHeader
        title={
          <PanelHeaderTitleText>
            {$t(
              defineMessage({
                defaultMessage: "Vest {token}",
                description: "EarnScreen/VestPanel/title",
              }),
              { token: <TokenName token={TokenInfoPresets.esUNW} /> },
            )}
          </PanelHeaderTitleText>
        }
        bottomText={$t(
          defineMessage({
            defaultMessage:
              "Once you lock to vest {token}, you will receive {token} every block linearly {vestDuration}. <vestingDetailsLink>Details ></vestingDetailsLink>",
            description: "EarnScreen/VestPanel/secondary title",
          }),
          {
            esToken: <TokenName token={TokenInfoPresets.esUNW} />,
            token: <TokenName token={TokenInfoPresets.UNW} />,
            vestDuration: (
              <Spensor fallback={"-"}>
                {() => (
                  <>{formatDuration(intl, readResource(props.vestPeriod))}</>
                )}
              </Spensor>
            ),
            vestingDetailsLink: contents => (
              <HrefLink href={props.vestingDetailsLink}>
                {p => (
                  <TouchableOpacity {...p}>
                    <Text style={{ color: colors("blue-600") }}>
                      {contents}
                    </Text>
                  </TouchableOpacity>
                )}
              </HrefLink>
            ),
          },
        )}
      />

      <PanelInsetGroup style={{ marginTop: spacing(5), flex: 1 }}>
        {({ gap }) => (
          <>
            <PanelInset>
              <BreakdownList
                headInfoListItem={
                  <InfoListItem>
                    <InfoListItemTitle>
                      {$t(
                        defineMessage({
                          defaultMessage: "Active Vesting",
                          description: "EarnScreen/VestPanel",
                        }),
                      )}
                    </InfoListItemTitle>
                    <InfoListItemDetail>
                      <InfoListItemDetailText>
                        <TextTokenCount
                          token={TokenInfoPresets.esUNW}
                          count={props.activeVestingAmount}
                        />
                      </InfoListItemDetailText>
                    </InfoListItemDetail>
                  </InfoListItem>
                }
                detailInfoListItems={
                  <InfoListItem>
                    <InfoListItemTitle>
                      {$t(
                        defineMessage({
                          defaultMessage: "Claimable Vest",
                          description: "EarnScreen/VestPanel",
                        }),
                      )}
                    </InfoListItemTitle>
                    <InfoListItemDetail>
                      <InfoListItemDetailText>
                        <SpensorR
                          read={{
                            claimableVest: props.claimableVest,
                          }}
                          fallback={"-"}
                        >
                          {({ claimableVest }) => (
                            <>
                              <TextTokenCount
                                token={TokenInfoPresets.esUNW}
                                count={claimableVest.fromEsUNIAmount}
                              />
                              &nbsp;→&nbsp;
                              <TextTokenCount
                                token={TokenInfoPresets.UNW}
                                count={claimableVest.toUNWAmount}
                              />
                            </>
                          )}
                        </SpensorR>
                      </InfoListItemDetailText>
                    </InfoListItemDetail>
                  </InfoListItem>
                }
              />
            </PanelInset>
          </>
        )}
      </PanelInsetGroup>

      <View
        style={{
          marginTop: spacing(5),
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <Button
          style={{ flex: 1, marginRight: spacing(4) }}
          Variant={WhiteOutlineButtonVariant}
          onPress={props.onLock}
          disabled={safeReadResource(props.canLock) === false}
        >
          <Button.Text>
            {$t(
              defineMessage({
                defaultMessage: "Lock {token} to Vest",
                description: "EarnScreen/VestPanel/stake button",
              }),
              { token: <TokenName token={TokenInfoPresets.esUNW} /> },
            )}
          </Button.Text>
        </Button>

        <Button
          style={{ flex: 1 }}
          Variant={WhiteOutlineButtonVariant}
          onPress={props.onUnlock}
          disabled={safeReadResource(props.canUnlock) === false}
        >
          <Button.Text>
            {$t(
              defineMessage({
                defaultMessage: "Unlock {token}",
                description: "EarnScreen/VestPanel/unstake button",
              }),
              { token: <TokenName token={TokenInfoPresets.esUNW} /> },
            )}
          </Button.Text>
        </Button>
      </View>
    </PanelContainer>
  )
}
