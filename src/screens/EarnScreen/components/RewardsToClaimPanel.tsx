import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, Text, View, ViewStyle } from "react-native"
import { WhiteOutlineButtonVariant } from "../../../components/Button/WhiteOutlineButtonVariant"
import { Button } from "../../../components/ButtonFramework/Button"
import { CardBoxView } from "../../../components/CardBox/CardBox"
import { CardInset } from "../../../components/CardBox/CardInset"
import { InfoList } from "../../../components/InfoList/InfoList"
import {
  DefaultInfoListItem,
  InfoListItem,
} from "../../../components/InfoList/InfoListItem"
import {
  DefaultInfoListItemDetailText,
  InfoListItemDetail,
  InfoListItemDetailText,
} from "../../../components/InfoList/InfoListItemDetail"
import {
  DefaultInfoListItemTitleText,
  InfoListItemTitle,
  InfoListItemTitleText,
} from "../../../components/InfoList/InfoListItemTitle"
import {
  TextTokenCount,
  TokenCountAsCurrency,
} from "../../../components/TextTokenCount"
import { useColors } from "../../../components/Themed/color"
import { useSpacing } from "../../../components/Themed/spacing"
import { TokenCount, TokenPresets } from "../../../components/TokenCount"
import { TokenName } from "../../../components/TokenName"
import { TooltippifiedText } from "../../../components/Tooltip"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { useSizeQuery } from "../../../utils/reactHelpers/useSizeQuery"
import {
  safeReadResource,
  SuspenseResource,
} from "../../../utils/SuspenseResource"
import { TokenInfo } from "../../../utils/TokenInfo"
import { TokenInfoPresets } from "../../../utils/TokenInfoPresets/TokenInfoPresets"
import { FullWidthPanelActionsArea, usePanelContainerGap } from "./misc"

interface RewardsToClaim {
  usdt: {
    token: TokenInfo
    amount: SuspenseResource<BigNumber>
    valueInUSD: SuspenseResource<BigNumber>
  }
  esUNW: {
    token: TokenInfo
    amount: SuspenseResource<BigNumber>
    valueInUSD: SuspenseResource<BigNumber>
    traderReward: SuspenseResource<BigNumber>
    stakeReward: SuspenseResource<BigNumber>
  }
  unw: {
    token: TokenInfo
    amount: SuspenseResource<BigNumber>
    valueInUSD: SuspenseResource<BigNumber>
  }
}

export interface RewardsToClaimPanelCoreProps {
  rewardsEstimatedUSD: SuspenseResource<BigNumber>

  rewardsToClaim: RewardsToClaim

  onClaim?: (compound?: boolean) => void
  canVest: boolean
  onVest?: () => void
}

export const RewardsToClaimPanel: FC<
  RewardsToClaimPanelCoreProps & {
    style?: StyleProp<ViewStyle>

    /**
     * @default 'row'
     */
    layout?: "col" | "row"
    enableCompound: boolean
  }
> = props => {
  const { $t } = useIntl()

  const spacing = useSpacing()
  const colors = useColors()

  const [layout = "row", onLayout] = useSizeQuery({
    0: "col",
    800: "row",
  } as const)

  const { vertical: verticalGap, horizontal: horizontalGap } =
    usePanelContainerGap()

  return (
    <CardBoxView style={props.style} padding={spacing(6)} onLayout={onLayout}>
      <View style={{ flexDirection: "row", alignItems: "center" }}>
        <Text style={{ fontSize: 18, color: colors("gray-900") }}>
          {$t(
            defineMessage({
              defaultMessage: "Rewards:",
              description: "EarnScreen/RewardsToClaim/title",
            }),
          )}
        </Text>

        <View style={{ marginLeft: spacing(2.5) }}>
          <TokenCountAsCurrency
            style={{
              fontSize: 30,
              color: colors("gray-900"),
            }}
            token={TokenPresets.USD}
            count={props.rewardsEstimatedUSD}
          />
        </View>
      </View>

      <CardInset
        style={[
          { marginTop: spacing(5) },
          layout === "row"
            ? {
                flexDirection: "row",
                alignItems: "center",
              }
            : {
                flexDirection: "column",
                alignItems: "stretch",
              },
        ]}
        padding={{
          paddingVertical: spacing(2.5),
          paddingHorizontal: spacing(3),
        }}
      >
        <InfoList
          style={{ flex: 1 }}
          direction={"row"}
          listItemDirection={"column"}
          renderInfoListItem={p => (
            <DefaultInfoListItem
              {...p}
              style={[p.style, { flex: 1, alignItems: "flex-start" }]}
            />
          )}
          renderInfoListItemTitleText={p => (
            <DefaultInfoListItemTitleText
              {...p}
              style={[
                p.style,
                {
                  color: colors("gray-500"),
                  fontSize: 14,
                },
              ]}
            />
          )}
          renderInfoListItemDetailText={p => (
            <DefaultInfoListItemDetailText
              {...p}
              style={[
                p.style,
                { fontSize: 18, fontWeight: "500", color: colors("black") },
              ]}
            />
          )}
        >
          <InfoListItem>
            <InfoListItemTitle>
              <InfoListItemTitleText>
                <TokenName token={TokenInfoPresets.MockUSDT} />
              </InfoListItemTitleText>
            </InfoListItemTitle>
            <InfoListItemDetail>
              <InfoListItemDetailText>
                <TooltippifiedText
                  content={$t(
                    defineMessage({
                      defaultMessage: "Your rewards from fee distribution",
                      description: "EarnScreen/RewardsToClaim/tooltip/usdt",
                    }),
                  )}
                >
                  <TokenCountAsCurrency
                    token={TokenPresets.USD}
                    count={props.rewardsToClaim.usdt.amount}
                  />
                </TooltippifiedText>
              </InfoListItemDetailText>
            </InfoListItemDetail>
          </InfoListItem>

          <RewardInfoListItem
            {...props.rewardsToClaim.esUNW}
            token={TokenInfoPresets.esUNW}
          />
        </InfoList>

        <FullWidthPanelActionsArea
          style={
            layout === "row"
              ? { flex: 1, marginLeft: horizontalGap, flexDirection: "row" }
              : { marginTop: verticalGap }
          }
          contentContainerStyle={{ flexDirection: "row" }}
          withoutHorizontalGap={true}
          withoutHorizontalPadding={layout === "col"}
        >
          <Button
            style={{ flex: 1 }}
            Variant={WhiteOutlineButtonVariant}
            disabled={props.onClaim == null}
            onPress={() => props.onClaim?.(false)}
          >
            {$t(
              defineMessage({
                defaultMessage: "Claim",
                description: "EarnScreen/RewardsToClaim/claim button",
              }),
            )}
          </Button>

          {props.enableCompound && (
            <Button
              style={{ flex: 1, marginLeft: 10 }}
              Variant={WhiteOutlineButtonVariant}
              disabled={props.onClaim == null}
              onPress={() => props.onClaim?.(true)}
            >
              {$t(
                defineMessage({
                  defaultMessage: "Compound",
                  description: "EarnScreen/RewardsToClaim/claim button",
                }),
              )}
            </Button>
          )}
        </FullWidthPanelActionsArea>
      </CardInset>
      <CardInset
        style={[
          { marginTop: spacing(5) },
          layout === "row"
            ? {
                flexDirection: "row",
                alignItems: "center",
              }
            : {
                flexDirection: "column",
                alignItems: "stretch",
              },
        ]}
        padding={{
          paddingVertical: spacing(2.5),
          paddingHorizontal: spacing(3),
        }}
      >
        <InfoList
          style={{ flex: 1 }}
          direction={"row"}
          listItemDirection={"column"}
          renderInfoListItem={p => (
            <DefaultInfoListItem
              {...p}
              style={[p.style, { flex: 1, alignItems: "flex-start" }]}
            />
          )}
          renderInfoListItemTitleText={p => (
            <DefaultInfoListItemTitleText
              {...p}
              style={[
                p.style,
                {
                  color: colors("gray-500"),
                  fontSize: 14,
                },
              ]}
            />
          )}
          renderInfoListItemDetailText={p => (
            <DefaultInfoListItemDetailText
              {...p}
              style={[
                p.style,
                { fontSize: 18, fontWeight: "500", color: colors("black") },
              ]}
            />
          )}
        >
          <InfoListItem>
            <InfoListItemTitle>
              <InfoListItemTitleText>
                <TokenName token={TokenInfoPresets.UNW} />
              </InfoListItemTitleText>
            </InfoListItemTitle>
            <InfoListItemDetail>
              <TooltippifiedText
                content={$t(
                  defineMessage({
                    defaultMessage: "Your UNW from esUNW Vest",
                    description: "EarnScreen/RewardsToClaim/tooltip/unw",
                  }),
                )}
              >
                <RewardInfoListItemDetailText
                  {...props.rewardsToClaim.unw}
                  token={TokenInfoPresets.UNW}
                />
              </TooltippifiedText>
            </InfoListItemDetail>
          </InfoListItem>
        </InfoList>

        <FullWidthPanelActionsArea
          style={
            layout === "row"
              ? { flex: 1, marginLeft: horizontalGap, flexDirection: "row" }
              : { marginTop: verticalGap }
          }
          contentContainerStyle={{ flexDirection: "row" }}
          withoutHorizontalGap={true}
          withoutHorizontalPadding={layout === "col"}
        >
          <Button
            disabled={!props.canVest}
            style={{ flex: 1 }}
            Variant={WhiteOutlineButtonVariant}
            onPress={() => props.onVest?.()}
          >
            {$t(
              defineMessage({
                defaultMessage: "Claim",
                description: "EarnScreen/RewardsToClaim/claim button",
              }),
            )}
          </Button>
        </FullWidthPanelActionsArea>
      </CardInset>
    </CardBoxView>
  )
}

const RewardInfoListItemDetailText: FC<{
  token: TokenInfo
  amount: SuspenseResource<BigNumber>
  valueInUSD: SuspenseResource<BigNumber>
}> = props => {
  const { $t } = useIntl()

  return (
    <InfoListItemDetailText>
      {$t(
        defineMessage({
          defaultMessage: "{rewardTokenAmount} ({rewardTokenUSDValue})",
          description: "EarnScreen/rewards panel",
        }),
        {
          rewardTokenAmount: (
            <TokenCount token={props.token} count={props.amount} />
          ),
          rewardTokenUSDValue: (
            <TokenCountAsCurrency
              token={TokenPresets.USD}
              count={props.valueInUSD}
            />
          ),
        },
      )}
    </InfoListItemDetailText>
  )
}

const RewardInfoListItem: FC<{
  token: TokenInfo
  amount: SuspenseResource<BigNumber>
  valueInUSD: SuspenseResource<BigNumber>
  traderReward: SuspenseResource<BigNumber>
  stakeReward: SuspenseResource<BigNumber>
}> = props => {
  const traderReward = safeReadResource(props.traderReward) ?? BigNumber.ZERO
  const stakeReward = safeReadResource(props.stakeReward) ?? BigNumber.ZERO

  const amountText = (
    <RewardInfoListItemDetailText
      token={props.token}
      amount={props.amount}
      valueInUSD={props.valueInUSD}
    />
  )

  return (
    <InfoListItem>
      <InfoListItemTitle>
        <InfoListItemTitleText>
          <TokenName token={props.token} />
        </InfoListItemTitleText>
      </InfoListItemTitle>
      <InfoListItemDetail>
        {BigNumber.isZero(traderReward) && BigNumber.isZero(stakeReward) ? (
          amountText
        ) : (
          <TooltippifiedText
            content={
              <RewardSourceTooltipContent
                rewardToken={props.token}
                traderRewardAmount={props.traderReward}
                stakeRewardAmount={props.stakeReward}
              />
            }
          >
            {amountText}
          </TooltippifiedText>
        )}
      </InfoListItemDetail>
    </InfoListItem>
  )
}

const RewardSourceTooltipContent: FC<{
  rewardToken: TokenInfo
  stakeRewardAmount: SuspenseResource<BigNumber>
  traderRewardAmount: SuspenseResource<BigNumber>
}> = props => {
  const spacing = useSpacing()
  const colors = useColors()
  const { $t } = useIntl()

  return (
    <InfoList
      style={{ width: 240 }}
      direction={"column"}
      listItemDirection={"row"}
      renderInfoListItem={p => (
        <DefaultInfoListItem
          {...p}
          style={[
            p.style,
            {
              alignItems: "center",
              justifyContent: "space-between",
            },
          ]}
        />
      )}
      renderInfoListItemDetailText={p => (
        <DefaultInfoListItemDetailText
          {...p}
          style={[p.style, { color: colors("white") }]}
        />
      )}
    >
      {BigNumber.isGtZero(
        safeReadResource(props.stakeRewardAmount) ?? BigNumber.ZERO,
      ) && (
        <InfoListItem>
          <InfoListItemTitle>
            {$t(
              defineMessage({
                defaultMessage: "Stake reward",
                description:
                  "EarnScreen/RewardsToClaim/reward tooltip/stake reward",
              }),
            )}
          </InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              <TextTokenCount
                token={props.rewardToken}
                count={props.stakeRewardAmount}
              />
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </InfoListItem>
      )}
      {BigNumber.isGtZero(
        safeReadResource(props.traderRewardAmount) ?? BigNumber.ZERO,
      ) && (
        <InfoListItem style={{ marginTop: spacing(1) }}>
          <InfoListItemTitle>
            {$t(
              defineMessage({
                defaultMessage: "Trader reward",
                description:
                  "EarnScreen/RewardsToClaim/reward tooltip/trader reward",
              }),
            )}
          </InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              <TextTokenCount
                token={props.rewardToken}
                count={props.traderRewardAmount}
              />
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </InfoListItem>
      )}
    </InfoList>
  )
}
