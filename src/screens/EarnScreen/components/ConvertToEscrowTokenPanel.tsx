import { FC, Fragment, ReactNode } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, Text, View, ViewStyle } from "react-native"
import { Path, Svg, SvgProps } from "react-native-svg"
import { WhiteOutlineButtonVariant } from "../../../components/Button/WhiteOutlineButtonVariant"
import { Button } from "../../../components/ButtonFramework/Button"
import { PercentNumber } from "../../../components/PercentNumber"
import { TextTokenCount } from "../../../components/TextTokenCount"
import { useColors } from "../../../components/Themed/color"
import { useSpacing } from "../../../components/Themed/spacing"
import { TokenIcon } from "../../../components/TokenIcon"
import { arrayJoin } from "../../../utils/arrayHelpers"
import { FormattedMessageOutsideText } from "../../../utils/intlHelpers"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { useSizeQuery } from "../../../utils/reactHelpers/useSizeQuery"
import { SuspenseResource } from "../../../utils/SuspenseResource"
import { TokenInfo } from "../../../utils/TokenInfo"
import { isNotNull } from "../../../utils/typeHelpers"
import { APRBadge } from "./APRBadge"
import {
  FullWidthPanelActionsArea,
  PanelContainer,
  usePanelContainerGap,
} from "./misc"

export interface ConvertToEscrowTokenPanelProps {
  style?: StyleProp<ViewStyle>
  token: TokenInfo
  esToken: TokenInfo
  tokenEmissionDistributeRate: BigNumber
  esTokenFeeDistributeRate: BigNumber
  esTokenEmissionDistributeRate: BigNumber
  esTokenApr: SuspenseResource<BigNumber>
  onConvert: () => void
}

export const ConvertToEscrowTokenPanel: FC<
  ConvertToEscrowTokenPanelProps
> = props => {
  const spacing = useSpacing()
  const colors = useColors()
  const { $t } = useIntl()

  const { horizontal: horizontalGap, vertical: verticalGap } =
    usePanelContainerGap()

  const [layout = "row", onLayout] = useSizeQuery({
    0: "col",
    600: "thin-row",
    1000: "row",
  } as const)

  return (
    <PanelContainer
      style={[
        props.style,
        layout === "row" || layout === "thin-row"
          ? {
              flexDirection: "row",
              alignItems: "center",
            }
          : {},
      ]}
      padding={{ paddingVertical: spacing(3) }}
      onLayout={onLayout}
    >
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <TokenFragment
          token={props.token}
          emissionDistributeRate={props.tokenEmissionDistributeRate}
        />
        <ArrowIcon
          style={{ flexShrink: 0, marginHorizontal: spacing(2.5) }}
          fill={colors("pink-500")}
        />
        <TokenFragment
          token={props.esToken}
          topRightArea={<APRBadge apr={props.esTokenApr} />}
          feeRevenueDistributeRate={props.esTokenFeeDistributeRate}
          emissionDistributeRate={props.esTokenEmissionDistributeRate}
        />
      </View>

      <FullWidthPanelActionsArea
        style={
          layout === "row"
            ? { flex: 1, marginLeft: horizontalGap }
            : layout === "thin-row"
            ? { marginLeft: horizontalGap }
            : { marginTop: verticalGap }
        }
        withoutHorizontalGap={true}
        withoutHorizontalPadding={layout === "col"}
      >
        <Button
          style={{
            flex: 1,
            flexDirection: "row",
            alignItems: "center",
          }}
          Variant={WhiteOutlineButtonVariant}
          onPress={props.onConvert}
        >
          <FormattedMessageOutsideText WrapText={Button.Text}>
            {$t(
              defineMessage({
                defaultMessage: "Convert",
                description:
                  "EarnScreen/ConvertToEscrowTokenPanel/convert button text",
              }),
            )}
          </FormattedMessageOutsideText>
        </Button>
      </FullWidthPanelActionsArea>
    </PanelContainer>
  )
}

const TokenFragment: FC<{
  style?: StyleProp<ViewStyle>
  token: TokenInfo
  feeRevenueDistributeRate?: BigNumber
  emissionDistributeRate?: BigNumber
  topRightArea?: ReactNode
}> = props => {
  const spacing = useSpacing()
  const colors = useColors()
  const { $t } = useIntl()

  return (
    <View style={[props.style, { flexDirection: "row", alignItems: "center" }]}>
      <TokenIcon
        style={{ marginRight: spacing(2.5) }}
        token={props.token}
        size={32}
      />

      <View>
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <TextTokenCount
            style={{
              marginRight: spacing(2.5),
              color: colors("gray-900"),
              fontSize: 14,
              fontWeight: "600",
            }}
            token={props.token}
            count={1}
          />
          {props.topRightArea}
        </View>

        <Text
          style={{
            color: colors("gray-900"),
            fontSize: 12,
            fontWeight: "400",
          }}
        >
          {arrayJoin(
            ({ index: idx }) => (
              <Fragment key={`sep-${idx}`}> + </Fragment>
            ),
            [
              props.feeRevenueDistributeRate == null ? undefined : (
                <Fragment key={"1"}>
                  {$t(
                    defineMessage({
                      defaultMessage: "{rate} Fee Dist.",
                      description: "EarnScreen/ConvertToEscrowTokenPanel",
                    }),
                    {
                      rate: (
                        <PercentNumber
                          number={props.feeRevenueDistributeRate}
                        />
                      ),
                    },
                  )}
                </Fragment>
              ),
              props.emissionDistributeRate == null ? undefined : (
                <Fragment key={"2"}>
                  {$t(
                    defineMessage({
                      defaultMessage: "{rate} Emission Dist.",
                      description: "EarnScreen/ConvertToEscrowTokenPanel",
                    }),
                    {
                      rate: (
                        <PercentNumber number={props.emissionDistributeRate} />
                      ),
                    },
                  )}
                </Fragment>
              ),
            ].filter(isNotNull),
          )}
        </Text>
      </View>
    </View>
  )
}

const ArrowIcon: FC<SvgProps> = props => (
  <Svg width="16" height="16" viewBox="0 0 16 16" fill="#EC4899" {...props}>
    <Path d="M9.72041 3.32214L9.78497 3.24387C10.0621 2.94724 10.5021 2.91762 10.8118 3.17476L10.8849 3.24387L14.7722 7.40566C15.0493 7.70233 15.077 8.17334 14.8367 8.50482L14.7722 8.58226L10.8857 12.7491C10.5964 13.0662 10.1258 13.0852 9.81503 12.7922C9.50428 12.4993 9.46234 11.9972 9.71963 11.6499L9.78419 11.5716L12.3428 8.82874H1.77791C1.38353 8.82868 1.05159 8.51272 1.00545 8.09347L1 7.99604C1 7.57389 1.29522 7.21857 1.6869 7.16918L1.77791 7.16335H12.3451L9.78497 4.4213C9.50785 4.12463 9.48018 3.65363 9.72041 3.32214L9.78497 3.24387L9.72041 3.32214Z" />
  </Svg>
)
