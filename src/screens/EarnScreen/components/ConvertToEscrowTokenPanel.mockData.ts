import { OptionalizeEventListeners } from "../../../../.storybook/utils"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../utils/TokenInfoPresets/TokenInfoPresets"
import { ConvertToEscrowTokenPanelProps } from "./ConvertToEscrowTokenPanel"

export const convertToEscrowTokenPanelProps: OptionalizeEventListeners<ConvertToEscrowTokenPanelProps> =
  {
    token: TokenInfoPresets.UNW,
    esToken: TokenInfoPresets.esUNW,
    tokenEmissionDistributeRate: BigNumber.from(0.05),
    esTokenFeeDistributeRate: BigNumber.from(0.4),
    esTokenEmissionDistributeRate: BigNumber.from(0.15),
    esTokenApr: BigNumber.from(0.3),
  }
