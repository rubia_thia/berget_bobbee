import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, ViewStyle } from "react-native"
import { SmartLoadableButton } from "../../../components/ButtonFramework/LoadableButton"
import { CardInset } from "../../../components/CardBox/CardInset"
import {
  CardBoxModalContent,
  TitleBar,
  TitleBarText,
} from "../../../components/CardBoxModalContent/CardBoxModalContent"
import { InfoList } from "../../../components/InfoList/InfoList"
import { InfoListItem } from "../../../components/InfoList/InfoListItem"
import { InfoListItemDetail } from "../../../components/InfoList/InfoListItemDetail"
import {
  InfoListItemTitle,
  InfoListItemTitleText,
} from "../../../components/InfoList/InfoListItemTitle"
import {
  NoteParagraph,
  NoteParagraphText,
} from "../../../components/NoteParagraph/NoteParagraph"
import { Spensor } from "../../../components/Spensor"
import { TextTokenCount } from "../../../components/TextTokenCount"
import { useSpacing } from "../../../components/Themed/spacing"
import { BalanceTopArea } from "../../../components/TokenInput/BalanceTopArea"
import { TokenInput } from "../../../components/TokenInput/TokenInput"
import { TokenName } from "../../../components/TokenName"
import { formatDuration } from "../../../utils/datetimeHelpers"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import {
  readResource,
  safeReadResource,
  SuspenseResource,
} from "../../../utils/SuspenseResource"
import { TokenInfo } from "../../../utils/TokenInfo"
import { TokenInfoPresets } from "../../../utils/TokenInfoPresets/TokenInfoPresets"

export interface ConvertToEscrowTokenFormModelContentProps {
  style?: StyleProp<ViewStyle>
  onDismiss: () => void

  esToken: TokenInfo
  token: TokenInfo
  availableTokenAmount: SuspenseResource<BigNumber>
  tokenAmount: SuspenseResource<null | BigNumber>
  onTokenAmountChange: (newAmount: null | BigNumber) => void
  onPressMax: SuspenseResource<() => void>

  willReceiveEsTokenAmount: SuspenseResource<BigNumber>
  vestPeriod: SuspenseResource<Duration>

  onConvert: SuspenseResource<undefined | (() => void | Promise<void>)>
}

export const ConvertToEscrowTokenFormModelContent: FC<
  ConvertToEscrowTokenFormModelContentProps
> = props => {
  const spacing = useSpacing()
  const intl = useIntl()
  const { $t } = intl

  const onConvert = safeReadResource(props.onConvert)

  return (
    <CardBoxModalContent
      className="space-y-5"
      style={props.style}
      padding={spacing(4)}
      onClose={props.onDismiss}
    >
      <TitleBar>
        <TitleBarText>
          {$t(
            defineMessage({
              defaultMessage: "Convert {token} to {token}",
              description:
                "EarnScreen/Convert To Escrow Token Form Modal/title",
            }),
            {
              token: <TokenName token={props.token} />,
              esToken: <TokenName token={props.esToken} />,
            },
          )}
        </TitleBarText>
      </TitleBar>

      <TokenInput
        availableTokens={[]}
        token={props.token}
        value={props.tokenAmount}
        onValueChange={props.onTokenAmountChange}
        topArea={
          <BalanceTopArea
            titleText={$t(
              defineMessage({
                defaultMessage: "Convert",
                description:
                  "EarnScreen/Convert To Escrow Token Form Modal/label",
              }),
            )}
            token={props.token}
            balance={props.availableTokenAmount}
          />
        }
        placeholder={"0.0"}
        onPressMax={props.onPressMax}
      />

      <CardInset padding={spacing(3)}>
        <InfoList direction={"column"} listItemDirection={"row"}>
          <InfoListItem>
            <InfoListItemTitle>
              {$t(
                defineMessage({
                  defaultMessage: "You will receive",
                  description:
                    "EarnScreen/Lock escrow token form modal content/info plate/title",
                }),
              )}
            </InfoListItemTitle>
            <InfoListItemDetail>
              <InfoListItemTitleText>
                <TextTokenCount
                  token={props.esToken}
                  count={props.willReceiveEsTokenAmount}
                />
              </InfoListItemTitleText>
            </InfoListItemDetail>
          </InfoListItem>
        </InfoList>
      </CardInset>

      <SmartLoadableButton
        disabled={onConvert == null}
        onPress={async () => {
          if (onConvert == null) return
          await onConvert()
          props.onDismiss()
        }}
      >
        {$t(
          defineMessage({
            defaultMessage: "Convert",
            description:
              "EarnScreen/Convert To Escrow Token Form Modal/submit button",
          }),
        )}
      </SmartLoadableButton>

      <NoteParagraph>
        <NoteParagraphText>
          {$t(
            defineMessage({
              defaultMessage:
                "Please note converting {token} to {token} is subject {vestDuration} linear vesting.",
              description:
                "EarnScreen/Convert To Escrow Token Form Modal/bottom tip text",
            }),
            {
              esToken: <TokenName token={TokenInfoPresets.esUNW} />,
              token: <TokenName token={TokenInfoPresets.UNW} />,
              vestDuration: (
                <Spensor fallback={"-"}>
                  {() => (
                    <>{formatDuration(intl, readResource(props.vestPeriod))}</>
                  )}
                </Spensor>
              ),
            },
          )}
        </NoteParagraphText>
      </NoteParagraph>
    </CardBoxModalContent>
  )
}
