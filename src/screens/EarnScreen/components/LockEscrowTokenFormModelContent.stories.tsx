import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../utils/TokenInfoPresets/TokenInfoPresets"
import { LockEscrowTokenFormModelContent } from "./LockEscrowTokenFormModelContent"

export default {
  title: "Page/EarnScreen/LockEscrowTokenFormModelContent",
  component: LockEscrowTokenFormModelContent,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof LockEscrowTokenFormModelContent>

const template = withTemplate(LockEscrowTokenFormModelContent, {
  style: {
    margin: 10,
  },
  token: TokenInfoPresets.UNW,
  esToken: TokenInfoPresets.esUNW,
  availableEsTokenAmount: BigNumber.from(1000),
  esTokenAmount: BigNumber.from(1011),
  willBeVestedTokenCount: BigNumber.from(1111),
  vestPeriod: { months: 6 },
  vestingDetailsLink: "https://google.com",
})

export const Normal = template()
