import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import {
  ActivityIndicator,
  Image,
  ImageSourcePropType,
  StyleProp,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import {
  CardBoxModalContent,
  TitleBar,
  TitleBarText,
} from "../../../../components/CardBoxModalContent/CardBoxModalContent"
import { Spensor } from "../../../../components/Spensor"
import { useColors } from "../../../../components/Themed/color"
import { useSpacing } from "../../../../components/Themed/spacing"
import { TokenName } from "../../../../components/TokenName"
import {
  HrefLink,
  HrefLinkComponentProps,
} from "../../../../utils/reactNavigationHelpers/HrefLink"
import {
  readResource,
  SuspenseResource,
} from "../../../../utils/SuspenseResource"
import { TokenInfo } from "../../../../utils/TokenInfo"

export interface BuyUNWModalContentProps {
  style?: StyleProp<ViewStyle>
  onDismiss: () => void
  buyingToken: TokenInfo
  thenaLink: SuspenseResource<string>
  pancakeLink: SuspenseResource<string>
}

export const BuyUNWModalContent: FC<BuyUNWModalContentProps> = props => {
  const spacing = useSpacing()
  const { $t } = useIntl()

  return (
    <CardBoxModalContent
      style={props.style}
      padding={spacing(4)}
      onClose={props.onDismiss}
    >
      <TitleBar>
        <TitleBarText>
          {$t(
            defineMessage({
              defaultMessage: "Buy {token} from",
              description: "EarnScreen/BuyTokenModal/title",
            }),
            {
              token: <TokenName token={props.buyingToken} />,
            },
          )}
        </TitleBarText>
      </TitleBar>

      <View style={{ paddingTop: spacing(5), paddingBottom: spacing(2.5) }}>
        <Spensor fallback={<ActivityIndicator className={"m-auto"} />}>
          {() => (
            <>
              <HrefLink href={readResource(props.thenaLink)}>
                {p => (
                  <EntryContainer
                    {...p}
                    imageSource={require("./thena.png")}
                    aspectRatio={300 / 62}
                  />
                )}
              </HrefLink>

              <HrefLink href={readResource(props.pancakeLink)}>
                {p => (
                  <EntryContainer
                    {...p}
                    style={{ marginTop: spacing(2.5) }}
                    imageSource={require("./pancake.png")}
                    aspectRatio={480 / 75}
                  />
                )}
              </HrefLink>
            </>
          )}
        </Spensor>
      </View>
    </CardBoxModalContent>
  )
}

const EntryContainer: FC<
  HrefLinkComponentProps & {
    style?: StyleProp<ViewStyle>
    imageSource: ImageSourcePropType
    aspectRatio: number
  }
> = props => {
  const spacing = useSpacing()
  const colors = useColors()

  const { style, imageSource, ...restProps } = props

  const height = 20

  return (
    <TouchableOpacity
      style={[
        style,
        {
          padding: spacing(5),
          borderRadius: 4,
          borderWidth: 1,
          borderColor: colors("gray-200"),
          backgroundColor: colors("blue-50"),
        },
      ]}
      {...restProps}
    >
      <Image
        style={{
          marginHorizontal: "auto",
          width: height * props.aspectRatio,
          height: height,
        }}
        source={imageSource}
      />
    </TouchableOpacity>
  )
}
