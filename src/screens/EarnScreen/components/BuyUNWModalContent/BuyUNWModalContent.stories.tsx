import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { TokenInfoPresets } from "../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { BuyUNWModalContent } from "./BuyUNWModalContent"

export default {
  title: "Page/EarnScreen/BuyUNWModalContent",
  component: BuyUNWModalContent,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof BuyUNWModalContent>

const template = withTemplate(BuyUNWModalContent, {
  style: {
    margin: 10,
  },
  buyingToken: TokenInfoPresets.UNW,
  thenaLink: "https://google.com",
  pancakeLink: "https://twitter.com",
})

export const Normal = template()
