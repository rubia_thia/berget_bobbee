import { FC, useState } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, View, ViewStyle } from "react-native"
import { SmartLoadableButton } from "../../../components/ButtonFramework/LoadableButton"
import {
  CardBoxModalContent,
  TitleBar,
  TitleBarText,
} from "../../../components/CardBoxModalContent/CardBoxModalContent"
import { useSpacing } from "../../../components/Themed/spacing"
import { BalanceTopArea } from "../../../components/TokenInput/BalanceTopArea"
import { TokenInput } from "../../../components/TokenInput/TokenInput"
import { TokenName } from "../../../components/TokenName"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { mathIs } from "../../../utils/numberHelpers/bigNumberExpressionParser"
import {
  safeReadResource,
  SuspenseResource,
} from "../../../utils/SuspenseResource"
import { TokenInfo } from "../../../utils/TokenInfo"

export interface StakeFormModalContentProps {
  style?: StyleProp<ViewStyle>
  onDismiss: () => void

  tokenToStake: TokenInfo
  tokenAmountAvailableToStake: SuspenseResource<BigNumber>

  onStake: (amount: BigNumber) => void | Promise<void>
}

export const StakeFormModalContent: FC<StakeFormModalContentProps> = props => {
  const spacing = useSpacing()
  const { $t } = useIntl()

  const [amount, setAmount] = useState<BigNumber>()

  return (
    <CardBoxModalContent
      style={props.style}
      padding={spacing(4)}
      onClose={props.onDismiss}
    >
      <View className="space-y-5">
        <TitleBar>
          <TitleBarText>
            {$t(
              defineMessage({
                defaultMessage: "Stake {token}",
                description: "EarnScreen/StakeFormModal/title",
              }),
              {
                token: <TokenName token={props.tokenToStake} />,
              },
            )}
          </TitleBarText>
        </TitleBar>

        <TokenInput
          availableTokens={[]}
          token={props.tokenToStake}
          value={amount ?? null}
          onValueChange={a => setAmount(a ?? undefined)}
          topArea={
            <BalanceTopArea
              titleText={$t(
                defineMessage({
                  defaultMessage: "Stake",
                  description: "EarnScreen/StakeFormModal/label",
                }),
              )}
              token={props.tokenToStake}
              balance={props.tokenAmountAvailableToStake}
            />
          }
          placeholder={"0.0"}
          onPressMax={() => {
            setAmount(safeReadResource(props.tokenAmountAvailableToStake))
          }}
        />

        <SmartLoadableButton
          disabled={
            amount == null ||
            mathIs`${amount} <= ${0} || ${amount} > ${
              safeReadResource(props.tokenAmountAvailableToStake) ??
              Number.MAX_VALUE
            }`
          }
          onPress={async () => {
            await props.onStake(amount!)
            props.onDismiss()
          }}
        >
          {$t(
            defineMessage({
              defaultMessage: "Stake",
              description: "EarnScreen/StakeFormModal/stakeButton",
            }),
          )}
        </SmartLoadableButton>
      </View>
    </CardBoxModalContent>
  )
}
