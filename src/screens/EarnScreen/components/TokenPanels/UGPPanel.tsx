import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, View, ViewStyle } from "react-native"
import { WhiteOutlineButtonVariant } from "../../../../components/Button/WhiteOutlineButtonVariant"
import { SmartLoadableButton } from "../../../../components/ButtonFramework/LoadableButton"
import { InfoList } from "../../../../components/InfoList/InfoList"
import {
  DefaultInfoListItem,
  InfoListItem,
} from "../../../../components/InfoList/InfoListItem"
import {
  InfoListItemDetail,
  InfoListItemDetailText,
  InfoListItemDetailTextStyleProvider,
} from "../../../../components/InfoList/InfoListItemDetail"
import {
  InfoListItemTitle,
  InfoListItemTitleTextStyleProvider,
} from "../../../../components/InfoList/InfoListItemTitle"
import { PercentNumber } from "../../../../components/PercentNumber"
import { Spensor } from "../../../../components/Spensor"
import { useColors } from "../../../../components/Themed/color"
import { useSpacing } from "../../../../components/Themed/spacing"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import {
  readResource,
  SuspenseResource,
} from "../../../../utils/SuspenseResource"
import { TokenInfoPresets } from "../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { BreakdownList } from "../BreakdownList"
import { PanelContainer, PanelInset, PanelInsetGroup } from "../misc"
import { TokenPanelHeader } from "./components/TokenPanelHeader/TokenPanelHeader"
import CheckIcon from "./_/check.svg"

export enum CannotStakeReason {
  NotEnoughBalance = "NotEnoughBalance",
  AlreadyStaked = "AlreadyStaked",
}

export interface UGPPanelsProps {
  style?: StyleProp<ViewStyle>

  ugpStakedIndex: SuspenseResource<string>

  tradingFeeDiscountPercentage: SuspenseResource<BigNumber>
  spreadReductionPercentage: SuspenseResource<BigNumber>
  boostEarningPercentage: SuspenseResource<BigNumber>

  cannotStakeReason?: CannotStakeReason
  onStake?: () => Promise<void> | void
  onUnstake?: () => Promise<void> | void
  onPressTokenIcon?: () => void
}

export const UGPPanel: FC<UGPPanelsProps> = props => {
  const spacing = useSpacing()
  const colors = useColors()
  const { $t } = useIntl()

  return (
    <PanelContainer style={props.style}>
      <TokenPanelHeader
        token={TokenInfoPresets.NFT_UGP}
        bottomText={$t(
          defineMessage({
            defaultMessage: "Stake your Genesis Pass to activate your benefits",
            description: "EarnScreen/UGPPanel",
          }),
        )}
      />

      <PanelInsetGroup style={{ marginTop: spacing(5), flex: 1 }}>
        {({ gap }) => (
          <>
            <PanelInset>
              <BreakdownList
                headInfoListItem={
                  <InfoListItem>
                    <InfoListItemTitle>
                      {$t(
                        defineMessage({
                          defaultMessage: "Your Active Staking",
                          description: "EarnScreen/UGPPanel",
                        }),
                      )}
                    </InfoListItemTitle>
                    <InfoListItemDetail>
                      <InfoListItemDetailText>
                        <Spensor fallback="-">
                          {() => readResource(props.ugpStakedIndex)}
                        </Spensor>
                      </InfoListItemDetailText>
                    </InfoListItemDetail>
                  </InfoListItem>
                }
              />
            </PanelInset>
            <PanelInset style={{ marginTop: gap }}>
              <InfoList
                direction={"column"}
                listItemDirection={"row"}
                renderInfoListItem={p => (
                  <DefaultInfoListItem
                    {...p}
                    style={[{ marginTop: spacing(1) }, p.style]}
                  />
                )}
              >
                <InfoListItemTitleTextStyleProvider
                  style={{ fontSize: 12, color: colors("gray-500") }}
                >
                  <InfoListItemDetailTextStyleProvider
                    style={{ fontSize: 12, color: colors("gray-500") }}
                  >
                    <InfoListItem>
                      <InfoListItemTitle>
                        {$t(
                          defineMessage({
                            defaultMessage: "Trading Fee Discount",
                            description: "EarnScreen/UGPPanel/label",
                          }),
                        )}
                      </InfoListItemTitle>
                      <InfoListItemDetail className="flex-row space-x-1 items-center">
                        <Spensor>
                          {() =>
                            readResource(props.ugpStakedIndex) ? (
                              <CheckIcon />
                            ) : null
                          }
                        </Spensor>
                        <InfoListItemDetailText>
                          -
                          <PercentNumber
                            number={props.tradingFeeDiscountPercentage}
                          />
                        </InfoListItemDetailText>
                      </InfoListItemDetail>
                    </InfoListItem>
                    <InfoListItem>
                      <InfoListItemTitle>
                        {$t(
                          defineMessage({
                            defaultMessage: "Spread Reduction",
                            description: "EarnScreen/UGPPanel/label",
                          }),
                        )}
                      </InfoListItemTitle>
                      <InfoListItemDetail className="flex-row space-x-1 items-center">
                        <Spensor>
                          {() =>
                            readResource(props.ugpStakedIndex) ? (
                              <CheckIcon />
                            ) : null
                          }
                        </Spensor>
                        <InfoListItemDetailText>
                          -
                          <PercentNumber
                            number={props.spreadReductionPercentage}
                          />
                        </InfoListItemDetailText>
                      </InfoListItemDetail>
                    </InfoListItem>
                  </InfoListItemDetailTextStyleProvider>
                </InfoListItemTitleTextStyleProvider>
              </InfoList>
            </PanelInset>
          </>
        )}
      </PanelInsetGroup>

      <View style={{ marginTop: spacing(5), flexDirection: "row" }}>
        <SmartLoadableButton
          style={{ flex: 1 }}
          disabled={props.onStake == null}
          onPress={props.onStake}
        >
          {$t(
            props.cannotStakeReason === CannotStakeReason.AlreadyStaked
              ? defineMessage({
                  defaultMessage: "Already Staked",
                  description: "EarnScreen/UGPPanel/button text",
                })
              : defineMessage({
                  defaultMessage: "Stake",
                  description: "EarnScreen/UGPPanel/button text",
                }),
          )}
        </SmartLoadableButton>

        <SmartLoadableButton
          style={{ flex: 1, marginLeft: spacing(5) }}
          Variant={WhiteOutlineButtonVariant}
          disabled={props.onUnstake == null}
          onPress={props.onUnstake}
        >
          {$t(
            defineMessage({
              defaultMessage: "Unstake",
              description: "EarnScreen/UGPPanel/button text",
            }),
          )}
        </SmartLoadableButton>
      </View>
    </PanelContainer>
  )
}
