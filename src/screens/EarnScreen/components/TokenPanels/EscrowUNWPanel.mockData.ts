import { OptionalizeEventListeners } from "../../../../../.storybook/utils"
import { noop } from "../../../../utils/fnHelpers"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { EscrowUNWPanelProps } from "./EscrowUNWPanel"

export const escrowUnwPanelProps: OptionalizeEventListeners<EscrowUNWPanelProps> =
  {
    esUnwWalletBalance: BigNumber.from(123),
    esUnwPrice: BigNumber.from(43.1),
    nftBurstingPercentage: BigNumber.from(0.15),
    esUnwStakingAmount: BigNumber.from(21.35),
    rewardsValueEstimatedInUSD: BigNumber.from(0.35),
    rewardsApr: BigNumber.from(0.163),
    emissionAPR: BigNumber.from(0.163),
    revenueAPR: BigNumber.from(0.163),
    totalStaked: BigNumber.from(111.11111111),
    rewards: [
      {
        token: TokenInfoPresets.MockBUSD,
        amount: BigNumber.from(0.03),
        valueEstimatedInUSD: BigNumber.from(0.24),
      },
      {
        token: TokenInfoPresets.MockUSDC,
        amount: BigNumber.from(0.03),
        valueEstimatedInUSD: BigNumber.from(0.24),
      },
    ],
    onStake: noop,
    onUnstake: undefined,
  }
