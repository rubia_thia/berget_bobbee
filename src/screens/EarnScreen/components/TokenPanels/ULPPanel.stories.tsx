import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { ULPPanel } from "./ULPPanel"
import { ulpPanelProps } from "./ULPPanel.mockData"

export default {
  title: "Page/EarnScreen/ULPPanel",
  component: ULPPanel,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof ULPPanel>

const template = withTemplate(ULPPanel, {
  style: {
    margin: 10,
  },
  ...ulpPanelProps,
})

export const Normal = template()
