import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, Text, View, ViewStyle } from "react-native"
import { WhiteOutlineButtonVariant } from "../../../../components/Button/WhiteOutlineButtonVariant"
import { Button } from "../../../../components/ButtonFramework/Button"
import { InfoListItem } from "../../../../components/InfoList/InfoListItem"
import {
  InfoListItemDetail,
  InfoListItemDetailText,
} from "../../../../components/InfoList/InfoListItemDetail"
import {
  InfoListItemTitle,
  InfoListItemTitleText,
} from "../../../../components/InfoList/InfoListItemTitle"
import {
  NoteParagraph,
  NoteParagraphText,
} from "../../../../components/NoteParagraph/NoteParagraph"
import { PercentNumber } from "../../../../components/PercentNumber"
import { TokenCountAsCurrency } from "../../../../components/TextTokenCount"
import { useColors } from "../../../../components/Themed/color"
import { useSpacing } from "../../../../components/Themed/spacing"
import { TokenCount, TokenPresets } from "../../../../components/TokenCount"
import { TokenName } from "../../../../components/TokenName"
import { TooltippifiedText } from "../../../../components/Tooltip"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { NavLink } from "../../../../utils/reactNavigationHelpers/NavLink"
import { NavLinkInfo } from "../../../../utils/reactNavigationHelpers/reactNavigationHelpers"
import { SuspenseResource } from "../../../../utils/SuspenseResource"
import { TokenInfo } from "../../../../utils/TokenInfo"
import { TokenInfoPresets } from "../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { APRTooltipContent } from "../APRTooltipContent"
import { BreakdownList } from "../BreakdownList"
import {
  PanelContainer,
  PanelInset,
  PanelInsetGroup,
  PanelOutlineInset,
} from "../misc"
import { HeaderAPR } from "./components/HeaderAPR"
import { TokenPanelHeader } from "./components/TokenPanelHeader/TokenPanelHeader"

export interface ULPPanelProps {
  style?: StyleProp<ViewStyle>
  buyUlpNavLinkInfo: NavLinkInfo<any>
  redeemUlpNavLinkInfo: NavLinkInfo<any>
  ulpPrice: SuspenseResource<BigNumber>
  nftBurstingPercentage?: SuspenseResource<BigNumber>
  ulpStakingAmount: SuspenseResource<BigNumber>
  rewardsValueEstimatedInUSD: SuspenseResource<BigNumber>
  rewardsApr: SuspenseResource<BigNumber>
  revenueAPR: SuspenseResource<BigNumber>
  emissionAPR: SuspenseResource<BigNumber>
  rewards: {
    token: TokenInfo
    amount: SuspenseResource<BigNumber>
    valueEstimatedInUSD: SuspenseResource<BigNumber>
  }[]
}

export const ULPPanel: FC<ULPPanelProps> = props => {
  const spacing = useSpacing()
  const colors = useColors()
  const { $t } = useIntl()

  return (
    <PanelContainer style={props.style}>
      <TokenPanelHeader
        token={TokenInfoPresets.ULP}
        tokenAmountText={
          <TokenCountAsCurrency
            token={TokenInfoPresets.ULP}
            count={props.ulpPrice}
          />
        }
        bottomText={
          props.nftBurstingPercentage != null &&
          $t(
            defineMessage({
              defaultMessage: "NFT Boosting: {nftBurstingPercentage}+",
              description: "EarnScreen/ULPPanel",
            }),
            {
              nftBurstingPercentage: (
                <PercentNumber number={props.nftBurstingPercentage} />
              ),
            },
          )
        }
        rightSide={
          <TooltippifiedText
            content={
              <APRTooltipContent
                revenueAPR={props.revenueAPR}
                emissionAPR={props.emissionAPR}
              />
            }
          >
            <HeaderAPR apr={props.rewardsApr} />
          </TooltippifiedText>
        }
      />

      <PanelInsetGroup style={{ marginTop: spacing(5) }}>
        {({ gap }) => (
          <>
            <PanelInset>
              <BreakdownList
                headInfoListItem={
                  <InfoListItem>
                    <InfoListItemTitle>
                      {$t(
                        defineMessage({
                          defaultMessage: "Your Active Staking",
                          description: "EarnScreen/ULPPanel",
                        }),
                      )}
                    </InfoListItemTitle>
                    <InfoListItemDetail>
                      <InfoListItemDetailText>
                        <TokenCount
                          token={TokenInfoPresets.ULP}
                          count={props.ulpStakingAmount}
                        />
                      </InfoListItemDetailText>
                    </InfoListItemDetail>
                  </InfoListItem>
                }
              />
            </PanelInset>
            <PanelInset style={{ marginTop: gap }}>
              <BreakdownList
                collapsable={true}
                headInfoListItem={
                  <InfoListItem>
                    <InfoListItemTitle>
                      <InfoListItemTitleText>
                        {$t(
                          defineMessage({
                            defaultMessage: "Rewards",
                            description: "EarnScreen/ULPPanel",
                          }),
                        )}
                      </InfoListItemTitleText>
                    </InfoListItemTitle>
                    <InfoListItemDetail>
                      <InfoListItemDetailText>
                        <TokenCountAsCurrency
                          token={TokenPresets.USD}
                          count={props.rewardsValueEstimatedInUSD}
                        />
                      </InfoListItemDetailText>
                    </InfoListItemDetail>
                  </InfoListItem>
                }
                detailInfoListItems={props.rewards.map((reward, idx) => (
                  <InfoListItem key={idx}>
                    <InfoListItemTitle>
                      <InfoListItemTitleText>
                        <TokenName token={reward.token} />
                      </InfoListItemTitleText>
                    </InfoListItemTitle>
                    <InfoListItemDetail>
                      <InfoListItemDetailText>
                        {$t(
                          defineMessage({
                            defaultMessage: "{tokenAmount} ({tokenUSDValue})",
                            description: "EarnScreen/ULPPanel/rewords section",
                          }),
                          {
                            tokenAmount: (
                              <TokenCount
                                token={reward.token}
                                count={reward.amount}
                              />
                            ),
                            tokenUSDValue: (
                              <TokenCountAsCurrency
                                token={TokenPresets.USD}
                                count={reward.valueEstimatedInUSD}
                              />
                            ),
                          },
                        )}
                      </InfoListItemDetailText>
                    </InfoListItemDetail>
                  </InfoListItem>
                ))}
              />
            </PanelInset>
          </>
        )}
      </PanelInsetGroup>

      <View style={{ marginTop: spacing(5), flex: 1 }}>
        <PanelOutlineInset>
          <Text
            style={{
              textAlign: "center",
              fontSize: 14,
              color: colors("gray-900"),
            }}
          >
            {$t(
              defineMessage({
                defaultMessage: "Stake {ulpToken}",
                description: "EarnScreen/ULPPanel",
              }),
              {
                ulpToken: <TokenName token={TokenInfoPresets.ULP} />,
              },
            )}
          </Text>
          <NoteParagraph style={{ marginTop: spacing(1) }}>
            <NoteParagraphText>
              {$t(
                defineMessage({
                  defaultMessage:
                    "{ulpToken} will be automatically staked upon minting. You can redeem any time.",
                  description: "EarnScreen/ULPPanel",
                }),
                {
                  ulpToken: <TokenName token={TokenInfoPresets.ULP} />,
                },
              )}
            </NoteParagraphText>
          </NoteParagraph>
        </PanelOutlineInset>
      </View>

      <View
        style={{
          marginTop: spacing(5),
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <NavLink {...props.buyUlpNavLinkInfo}>
          {linkProps => (
            <Button {...linkProps} style={{ flex: 1 }}>
              <Button.Text>
                {$t(
                  defineMessage({
                    defaultMessage: "Buy {lpToken}",
                    description: "EarnScreen/ULPPanel/buy button",
                  }),
                  { lpToken: <TokenName token={TokenInfoPresets.ULP} /> },
                )}
              </Button.Text>
            </Button>
          )}
        </NavLink>

        <NavLink {...props.redeemUlpNavLinkInfo}>
          {linkProps => (
            <Button
              {...linkProps}
              style={{ flex: 1, marginLeft: spacing(2.5) }}
              Variant={WhiteOutlineButtonVariant}
            >
              <Button.Text>
                {$t(
                  defineMessage({
                    defaultMessage: "Redeem",
                    description: "EarnScreen/ULPPanel/redeem button",
                  }),
                )}
              </Button.Text>
            </Button>
          )}
        </NavLink>
      </View>
    </PanelContainer>
  )
}
