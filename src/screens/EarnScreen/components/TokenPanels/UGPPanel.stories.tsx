import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { UGPPanel } from "./UGPPanel"
import { ugpPanelProps } from "./UGPPanel.mockData"

export default {
  title: "Page/EarnScreen/UGPPanel",
  component: UGPPanel,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof UGPPanel>

const template = withTemplate(UGPPanel, {
  style: {
    margin: 10,
  },
  ...ugpPanelProps,
})

export const Normal = template()
