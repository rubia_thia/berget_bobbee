import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { mockNavigation } from "../../../../utils/reactNavigationHelpers/linkTestHelpers"
import { TokenInfoPresets } from "../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { ULPPanelProps } from "./ULPPanel"

export const ulpPanelProps: ULPPanelProps = {
  redeemUlpNavLinkInfo: {} as any,
  buyUlpNavLinkInfo: {
    navigation: mockNavigation(),
    to: { screen: "Liquidity" },
  },
  ulpPrice: BigNumber.from(43.1),
  nftBurstingPercentage: BigNumber.from(0.15),
  ulpStakingAmount: BigNumber.from(21.35),
  rewardsValueEstimatedInUSD: BigNumber.from(0.35),
  rewardsApr: BigNumber.from(0.163),
  revenueAPR: BigNumber.from(0.163),
  emissionAPR: BigNumber.from(0.163),
  rewards: [
    {
      token: TokenInfoPresets.MockBUSD,
      amount: BigNumber.from(0.03),
      valueEstimatedInUSD: BigNumber.from(0.24),
    },
    {
      token: TokenInfoPresets.MockUSDC,
      amount: BigNumber.from(0.03),
      valueEstimatedInUSD: BigNumber.from(0.24),
    },
  ],
}
