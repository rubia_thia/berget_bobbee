import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, Text, ViewStyle } from "react-native"
import { PercentNumber } from "../../../../../components/PercentNumber"
import { useColors } from "../../../../../components/Themed/color"
import { BigNumber } from "../../../../../utils/numberHelpers/BigNumber"
import { SuspenseResource } from "../../../../../utils/SuspenseResource"

export const HeaderAPR: FC<{
  style?: StyleProp<ViewStyle>
  apr: SuspenseResource<BigNumber>
}> = props => {
  const colors = useColors()
  const { $t } = useIntl()

  return (
    <Text
      style={{
        alignSelf: "flex-end",
        fontSize: 14,
        fontWeight: "500",
        color: colors("green-600"),
      }}
    >
      {$t(
        defineMessage({
          defaultMessage: "{apr} APR",
          description: "EarnScreen/Panel Header APR",
        }),
        {
          apr: <PercentNumber number={props.apr} />,
        },
      )}
    </Text>
  )
}
