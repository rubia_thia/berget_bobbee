import { FC, ReactNode } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, Text, View, ViewStyle } from "react-native"
import { Path, Svg } from "react-native-svg"
import { ColorfulButtonVariant } from "../../../../../../components/Button/ColorfulButtonVariant"
import { Button } from "../../../../../../components/ButtonFramework/Button"
import { ButtonVariantProps } from "../../../../../../components/ButtonFramework/_/ButtonVariant"
import { useColors } from "../../../../../../components/Themed/color"
import { useSpacing } from "../../../../../../components/Themed/spacing"
import { TokenName } from "../../../../../../components/TokenName"
import {
  TextInsideTooltip,
  Tooltip,
} from "../../../../../../components/Tooltip"
import { TokenInfo } from "../../../../../../utils/TokenInfo"
import { PanelHeader, PanelHeaderTitleText } from "../../../misc"
import MetamaskIcon from "./metamask.svg"

export const TokenPanelHeader: FC<{
  style?: StyleProp<ViewStyle>
  token: TokenInfo
  tokenAmountText?: ReactNode
  bottomText?: ReactNode
  rightSide?: ReactNode
  onAddToWallet?: () => void
}> = props => {
  const spacing = useSpacing()
  const colors = useColors()
  const { $t } = useIntl()

  return (
    <PanelHeader
      title={
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <View>
            <PanelHeaderTitleText>
              <TokenName token={props.token} />
              {props.tokenAmountText != null && (
                <>&nbsp;({props.tokenAmountText})</>
              )}
            </PanelHeaderTitleText>
          </View>
          {props.onAddToWallet != null && (
            <Tooltip
              content={
                <TextInsideTooltip>
                  {$t(
                    defineMessage({
                      defaultMessage: "Add {tokenName} to wallet",
                      description:
                        "EarnScreen/Panel Header/add token to wallet",
                    }),
                    { tokenName: <TokenName token={props.token} /> },
                  )}
                </TextInsideTooltip>
              }
            >
              <Button
                style={{ marginLeft: spacing(2.5) }}
                Variant={MetamaskButtonVariant}
                onPress={props.onAddToWallet}
              />
            </Tooltip>
          )}
        </View>
      }
      bottomText={
        props.bottomText != null && (
          <Text style={{ color: colors("green-600") }}>{props.bottomText}</Text>
        )
      }
      rightSide={props.rightSide}
    />
  )
}

const MetamaskButtonVariant: FC<ButtonVariantProps> = props => {
  const colors = useColors()
  const spacing = useSpacing()

  return (
    <ColorfulButtonVariant
      {...props}
      style={[
        props.style,
        {
          flexDirection: "row",
          alignItems: "center",
        },
      ]}
      padding={{
        paddingVertical: spacing(1),
        paddingHorizontal: spacing(2),
      }}
      bgColor={{
        normal: colors("blue-50"),
        hovering: colors("blue-100"),
        pressing: colors("blue-200"),
        disabled: colors("blue-50"),
      }}
    >
      <Svg width="16" height="16" viewBox="0 0 16 16" fill="none">
        <Path d="M14 7H9V2H7V7H2V9H7V14H9V9H14V7Z" fill="#9CA3AF" />
      </Svg>
      <MetamaskIcon style={{ marginLeft: spacing(1) }} />
    </ColorfulButtonVariant>
  )
}
