import { OptionalizeEventListeners } from "../../../../../.storybook/utils"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { UGPPanelsProps } from "./UGPPanel"

export const ugpPanelProps: OptionalizeEventListeners<UGPPanelsProps> = {
  ugpStakedIndex: "#123",
  tradingFeeDiscountPercentage: BigNumber.from(0.15),
  spreadReductionPercentage: BigNumber.from(0.15),
  boostEarningPercentage: BigNumber.from(0.15),
}
