import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { EscrowUNWPanel } from "./EscrowUNWPanel"
import { escrowUnwPanelProps } from "./EscrowUNWPanel.mockData"

export default {
  title: "Page/EarnScreen/EscrowUNWPanel",
  component: EscrowUNWPanel,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof EscrowUNWPanel>

const template = withTemplate(EscrowUNWPanel, {
  style: {
    margin: 10,
  },
  ...escrowUnwPanelProps,
})

export const Normal = template()
