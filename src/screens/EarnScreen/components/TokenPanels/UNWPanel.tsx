import { FC, ReactNode } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, Text, View, ViewStyle } from "react-native"
import { WhiteOutlineButtonVariant } from "../../../../components/Button/WhiteOutlineButtonVariant"
import { Button } from "../../../../components/ButtonFramework/Button"
import { InfoListItem } from "../../../../components/InfoList/InfoListItem"
import {
  InfoListItemDetail,
  InfoListItemDetailText,
} from "../../../../components/InfoList/InfoListItemDetail"
import {
  InfoListItemTitle,
  InfoListItemTitleText,
} from "../../../../components/InfoList/InfoListItemTitle"
import { PercentNumber } from "../../../../components/PercentNumber"
import { TokenCountAsCurrency } from "../../../../components/TextTokenCount"
import { useColors } from "../../../../components/Themed/color"
import { useSpacing } from "../../../../components/Themed/spacing"
import { TokenCount, TokenPresets } from "../../../../components/TokenCount"
import { TokenName } from "../../../../components/TokenName"
import { TooltippifiedText } from "../../../../components/Tooltip"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { HrefLink } from "../../../../utils/reactNavigationHelpers/HrefLink"
import {
  safeReadResource,
  SuspenseResource,
} from "../../../../utils/SuspenseResource"
import { TokenInfo } from "../../../../utils/TokenInfo"
import { TokenInfoPresets } from "../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { BreakdownList } from "../BreakdownList"
import { PanelContainer, PanelInset, PanelInsetGroup } from "../misc"
import { HeaderAPR } from "./components/HeaderAPR"
import { TokenPanelHeader } from "./components/TokenPanelHeader/TokenPanelHeader"

export interface UNWPanelProps {
  style?: StyleProp<ViewStyle>
  unwPrice: SuspenseResource<BigNumber>
  nftBurstingPercentage?: SuspenseResource<BigNumber>
  unwStakingAmount: SuspenseResource<BigNumber>
  unwWalletBalance: SuspenseResource<BigNumber>
  rewardsValueEstimatedInUSD: SuspenseResource<BigNumber>
  rewardsApr: SuspenseResource<BigNumber>

  totalStaked: SuspenseResource<BigNumber>
  rewards: {
    token: TokenInfo
    amount: SuspenseResource<BigNumber>
    valueEstimatedInUSD: SuspenseResource<BigNumber>
  }[]

  onAddToWallet?: () => void
  onPressBuyUNW: () => void
  onStake: SuspenseResource<undefined | (() => void)>
  onUnstake: SuspenseResource<undefined | (() => void)>
}

export const UNWPanel: FC<UNWPanelProps> = props => {
  const spacing = useSpacing()
  const colors = useColors()
  const { $t } = useIntl()

  return (
    <PanelContainer style={props.style}>
      <TokenPanelHeader
        onAddToWallet={props.onAddToWallet}
        token={TokenInfoPresets.UNW}
        tokenAmountText={
          <TokenCountAsCurrency
            token={TokenInfoPresets.UNW}
            count={props.unwPrice}
          />
        }
        bottomText={
          props.nftBurstingPercentage != null &&
          $t(
            defineMessage({
              defaultMessage: "NFT Boosting: {nftBurstingPercentage}+",
              description: "EarnScreen/UNWPanel",
            }),
            {
              nftBurstingPercentage: (
                <PercentNumber number={props.nftBurstingPercentage} />
              ),
            },
          )
        }
        rightSide={
          <TooltippifiedText
            content={
              <Text className="text-[12px] text-white">
                {$t<ReactNode>(
                  defineMessage({
                    defaultMessage:
                      "7Day APR is moving average of each block APR. <learnMoreLink>Learn more ></learnMoreLink>",
                    description: "EarnScreen/EsUNW Pannel/APR",
                  }),
                  {
                    learnMoreLink: content => (
                      <HrefLink
                        href={
                          "https://docs.uniwhale.co/tokenomics/revenue-distribution-and-emission#apr"
                        }
                      >
                        <Text style={{ color: colors("blue-600") }}>
                          {content}
                        </Text>
                      </HrefLink>
                    ),
                  },
                )}
              </Text>
            }
          >
            <HeaderAPR apr={props.rewardsApr} />
          </TooltippifiedText>
        }
      />

      <PanelInsetGroup style={{ marginTop: spacing(5), flex: 1 }}>
        {({ gap }) => (
          <>
            <PanelInset>
              <BreakdownList
                headInfoListItem={
                  <InfoListItem>
                    <InfoListItemTitle>
                      {$t(
                        defineMessage({
                          defaultMessage: "Your Active Staking",
                          description: "EarnScreen/UNWPanel",
                        }),
                      )}
                    </InfoListItemTitle>
                    <InfoListItemDetail>
                      <InfoListItemDetailText>
                        <TokenCount
                          token={TokenInfoPresets.UNW}
                          count={props.unwStakingAmount}
                        />
                      </InfoListItemDetailText>
                    </InfoListItemDetail>
                  </InfoListItem>
                }
                detailInfoListItems={
                  <>
                    <InfoListItem>
                      <InfoListItemTitle>
                        {$t(
                          defineMessage({
                            defaultMessage: "Wallet Balance",
                            description: "EarnScreen/UNWPanel",
                          }),
                        )}
                      </InfoListItemTitle>
                      <InfoListItemDetail>
                        <InfoListItemDetailText>
                          <TokenCount
                            token={TokenInfoPresets.UNW}
                            count={props.unwWalletBalance}
                          />
                        </InfoListItemDetailText>
                      </InfoListItemDetail>
                    </InfoListItem>
                    <InfoListItem>
                      <InfoListItemTitle>
                        {$t(
                          defineMessage({
                            defaultMessage: "Total Staked",
                            description: "EarnScreen/UNWPanel",
                          }),
                        )}
                      </InfoListItemTitle>
                      <InfoListItemDetail>
                        <InfoListItemDetailText>
                          <TokenCount
                            token={TokenInfoPresets.UNW}
                            count={props.totalStaked}
                          />
                        </InfoListItemDetailText>
                      </InfoListItemDetail>
                    </InfoListItem>
                  </>
                }
              />
            </PanelInset>
            <PanelInset style={{ marginTop: gap }}>
              <BreakdownList
                collapsable={true}
                headInfoListItem={
                  <InfoListItem>
                    <InfoListItemTitle>
                      <InfoListItemTitleText>
                        {$t(
                          defineMessage({
                            defaultMessage: "Rewards",
                            description: "EarnScreen/UNWPanel",
                          }),
                        )}
                      </InfoListItemTitleText>
                    </InfoListItemTitle>
                    <InfoListItemDetail>
                      <InfoListItemDetailText>
                        <TokenCountAsCurrency
                          token={TokenPresets.USD}
                          count={props.rewardsValueEstimatedInUSD}
                        />
                      </InfoListItemDetailText>
                    </InfoListItemDetail>
                  </InfoListItem>
                }
                detailInfoListItems={props.rewards.map((reward, idx) => (
                  <InfoListItem key={idx}>
                    <InfoListItemTitle>
                      <InfoListItemTitleText>
                        <TokenName token={reward.token} />
                      </InfoListItemTitleText>
                    </InfoListItemTitle>
                    <InfoListItemDetail>
                      <InfoListItemDetailText>
                        {$t(
                          defineMessage({
                            defaultMessage: "{tokenAmount} ({tokenUSDValue})",
                            description: "EarnScreen/UNWPanel/rewords section",
                          }),
                          {
                            tokenAmount: (
                              <TokenCount
                                token={reward.token}
                                count={reward.amount}
                              />
                            ),
                            tokenUSDValue: (
                              <TokenCountAsCurrency
                                token={TokenPresets.USD}
                                count={reward.valueEstimatedInUSD}
                              />
                            ),
                          },
                        )}
                      </InfoListItemDetailText>
                    </InfoListItemDetail>
                  </InfoListItem>
                ))}
              />
            </PanelInset>
          </>
        )}
      </PanelInsetGroup>

      <View
        style={{
          marginTop: spacing(5),
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <Button
          style={{ flex: 1, marginRight: spacing(4) }}
          onPress={props.onPressBuyUNW}
        >
          <Button.Text>
            {$t(
              defineMessage({
                defaultMessage: "Buy {token}",
                description: "EarnScreen/UNWPanel/buy button",
              }),
              { token: <TokenName token={TokenInfoPresets.UNW} /> },
            )}
          </Button.Text>
        </Button>

        <Button
          style={{ flex: 1, marginRight: spacing(4) }}
          onPress={safeReadResource(props.onStake)}
          disabled={safeReadResource(props.onStake) == null}
        >
          <Button.Text>
            {$t(
              defineMessage({
                defaultMessage: "Stake {token}",
                description: "EarnScreen/UNWPanel/stake button",
              }),
              { token: <TokenName token={TokenInfoPresets.UNW} /> },
            )}
          </Button.Text>
        </Button>

        <Button
          style={{ flex: 1 }}
          Variant={WhiteOutlineButtonVariant}
          onPress={safeReadResource(props.onUnstake)}
          disabled={safeReadResource(props.onUnstake) == null}
        >
          <Button.Text>
            {$t(
              defineMessage({
                defaultMessage: "Unstake",
                description: "EarnScreen/UNWPanel/unstake button",
              }),
            )}
          </Button.Text>
        </Button>
      </View>
    </PanelContainer>
  )
}
