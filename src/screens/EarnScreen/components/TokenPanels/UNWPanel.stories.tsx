import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { UNWPanel } from "./UNWPanel"
import { unwPanelProps } from "./UNWPanel.mockData"

export default {
  title: "Page/EarnScreen/UNWPanel",
  component: UNWPanel,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof UNWPanel>

const template = withTemplate(UNWPanel, {
  style: {
    margin: 10,
  },
  ...unwPanelProps,
})

export const Normal = template()
