import { OptionalizeEventListeners } from "../../../../../.storybook/utils"
import { noop } from "../../../../utils/fnHelpers"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { UNWPanelProps } from "./UNWPanel"

export const unwPanelProps: OptionalizeEventListeners<UNWPanelProps> = {
  unwPrice: BigNumber.from(43.1),
  nftBurstingPercentage: BigNumber.from(0.15),
  unwStakingAmount: BigNumber.from(21.35),
  unwWalletBalance: BigNumber.from(21.35),
  rewardsValueEstimatedInUSD: BigNumber.from(0.35),
  rewardsApr: BigNumber.from(0.163),
  totalStaked: BigNumber.from(111.11111111),
  rewards: [
    {
      token: TokenInfoPresets.MockBUSD,
      amount: BigNumber.from(0.03),
      valueEstimatedInUSD: BigNumber.from(0.24),
    },
    {
      token: TokenInfoPresets.MockUSDC,
      amount: BigNumber.from(0.03),
      valueEstimatedInUSD: BigNumber.from(0.24),
    },
  ],
  onStake: undefined,
  onUnstake: noop,
}
