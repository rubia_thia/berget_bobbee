import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, View, ViewStyle } from "react-native"
import { WhiteOutlineButtonVariant } from "../../../../components/Button/WhiteOutlineButtonVariant"
import { Button } from "../../../../components/ButtonFramework/Button"
import { InfoListItem } from "../../../../components/InfoList/InfoListItem"
import {
  InfoListItemDetail,
  InfoListItemDetailText,
} from "../../../../components/InfoList/InfoListItemDetail"
import {
  InfoListItemTitle,
  InfoListItemTitleText,
} from "../../../../components/InfoList/InfoListItemTitle"
import { PercentNumber } from "../../../../components/PercentNumber"
import { TokenCountAsCurrency } from "../../../../components/TextTokenCount"
import { useSpacing } from "../../../../components/Themed/spacing"
import { TokenCount, TokenPresets } from "../../../../components/TokenCount"
import { TokenName } from "../../../../components/TokenName"
import { TooltippifiedText } from "../../../../components/Tooltip"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import {
  safeReadResource,
  SuspenseResource,
} from "../../../../utils/SuspenseResource"
import { TokenInfo } from "../../../../utils/TokenInfo"
import { TokenInfoPresets } from "../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { APRTooltipContent } from "../APRTooltipContent"
import { BreakdownList } from "../BreakdownList"
import { PanelContainer, PanelInset, PanelInsetGroup } from "../misc"
import { HeaderAPR } from "./components/HeaderAPR"
import { TokenPanelHeader } from "./components/TokenPanelHeader/TokenPanelHeader"

export interface EscrowUNWPanelProps {
  style?: StyleProp<ViewStyle>
  esUnwPrice: SuspenseResource<BigNumber>
  nftBurstingPercentage?: SuspenseResource<BigNumber>
  esUnwStakingAmount: SuspenseResource<BigNumber>
  esUnwWalletBalance: SuspenseResource<BigNumber>
  rewardsValueEstimatedInUSD: SuspenseResource<BigNumber>
  rewardsApr: SuspenseResource<BigNumber>
  revenueAPR: SuspenseResource<BigNumber>
  emissionAPR: SuspenseResource<BigNumber>
  totalStaked: SuspenseResource<BigNumber>
  rewards: {
    token: TokenInfo
    amount: SuspenseResource<BigNumber>
    valueEstimatedInUSD: SuspenseResource<BigNumber>
  }[]
  onAddToWallet?: () => void
  onStake: SuspenseResource<undefined | (() => void)>
  onUnstake: SuspenseResource<undefined | (() => void)>
}

export const EscrowUNWPanel: FC<EscrowUNWPanelProps> = props => {
  const spacing = useSpacing()
  const { $t } = useIntl()

  return (
    <PanelContainer style={props.style}>
      <TokenPanelHeader
        onAddToWallet={props.onAddToWallet}
        token={TokenInfoPresets.esUNW}
        tokenAmountText={
          <TokenCountAsCurrency
            token={TokenInfoPresets.esUNW}
            count={props.esUnwPrice}
          />
        }
        bottomText={
          props.nftBurstingPercentage != null &&
          $t(
            defineMessage({
              defaultMessage: "NFT Boosting: {nftBurstingPercentage}+",
              description: "EarnScreen/esUNWPanel",
            }),
            {
              nftBurstingPercentage: (
                <PercentNumber number={props.nftBurstingPercentage} />
              ),
            },
          )
        }
        rightSide={
          <TooltippifiedText
            content={
              <APRTooltipContent
                revenueAPR={props.revenueAPR}
                emissionAPR={props.emissionAPR}
              />
            }
          >
            <HeaderAPR apr={props.rewardsApr} />
          </TooltippifiedText>
        }
      />

      <PanelInsetGroup style={{ marginTop: spacing(5), flex: 1 }}>
        {({ gap }) => (
          <>
            <PanelInset>
              <BreakdownList
                headInfoListItem={
                  <InfoListItem>
                    <InfoListItemTitle>
                      {$t(
                        defineMessage({
                          defaultMessage: "Your Active Staking",
                          description: "EarnScreen/EscrowUNWPanel",
                        }),
                      )}
                    </InfoListItemTitle>
                    <InfoListItemDetail>
                      <InfoListItemDetailText>
                        <TokenCount
                          token={TokenInfoPresets.esUNW}
                          count={props.esUnwStakingAmount}
                        />
                      </InfoListItemDetailText>
                    </InfoListItemDetail>
                  </InfoListItem>
                }
                detailInfoListItems={
                  <>
                    <InfoListItem>
                      <InfoListItemTitle>
                        {$t(
                          defineMessage({
                            defaultMessage: "Wallet Balance",
                            description: "EarnScreen/esUNWPanel",
                          }),
                        )}
                      </InfoListItemTitle>
                      <InfoListItemDetail>
                        <InfoListItemDetailText>
                          <TokenCount
                            token={TokenInfoPresets.esUNW}
                            count={props.esUnwWalletBalance}
                          />
                        </InfoListItemDetailText>
                      </InfoListItemDetail>
                    </InfoListItem>

                    <InfoListItem>
                      <InfoListItemTitle>
                        {$t(
                          defineMessage({
                            defaultMessage: "Total Staked",
                            description: "EarnScreen/esUNWPanel",
                          }),
                        )}
                      </InfoListItemTitle>
                      <InfoListItemDetail>
                        <InfoListItemDetailText>
                          <TokenCount
                            token={TokenInfoPresets.esUNW}
                            count={props.totalStaked}
                          />
                        </InfoListItemDetailText>
                      </InfoListItemDetail>
                    </InfoListItem>
                  </>
                }
              />
            </PanelInset>
            <PanelInset style={{ marginTop: gap }}>
              <BreakdownList
                collapsable={true}
                headInfoListItem={
                  <InfoListItem>
                    <InfoListItemTitle>
                      <InfoListItemTitleText>
                        {$t(
                          defineMessage({
                            defaultMessage: "Rewards",
                            description: "EarnScreen/EscrowUNWPanel",
                          }),
                        )}
                      </InfoListItemTitleText>
                    </InfoListItemTitle>
                    <InfoListItemDetail>
                      <InfoListItemDetailText>
                        <TokenCountAsCurrency
                          token={TokenPresets.USD}
                          count={props.rewardsValueEstimatedInUSD}
                        />
                      </InfoListItemDetailText>
                    </InfoListItemDetail>
                  </InfoListItem>
                }
                detailInfoListItems={props.rewards.map((reward, idx) => (
                  <InfoListItem key={idx}>
                    <InfoListItemTitle>
                      <InfoListItemTitleText>
                        <TokenName token={reward.token} />
                      </InfoListItemTitleText>
                    </InfoListItemTitle>
                    <InfoListItemDetail>
                      <InfoListItemDetailText>
                        {$t(
                          defineMessage({
                            defaultMessage: "{tokenAmount} ({tokenUSDValue})",
                            description:
                              "EarnScreen/esUNWPanel/rewords section",
                          }),
                          {
                            tokenAmount: (
                              <TokenCount
                                token={reward.token}
                                count={reward.amount}
                              />
                            ),
                            tokenUSDValue: (
                              <TokenCountAsCurrency
                                token={TokenPresets.USD}
                                count={reward.valueEstimatedInUSD}
                              />
                            ),
                          },
                        )}
                      </InfoListItemDetailText>
                    </InfoListItemDetail>
                  </InfoListItem>
                ))}
              />
            </PanelInset>
          </>
        )}
      </PanelInsetGroup>

      <View
        style={{
          marginTop: spacing(5),
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <Button
          style={{ flex: 1, marginRight: spacing(4) }}
          onPress={safeReadResource(props.onStake)}
          disabled={safeReadResource(props.onStake) == null}
        >
          <Button.Text>
            {$t(
              defineMessage({
                defaultMessage: "Stake {token}",
                description: "EarnScreen/esUNWPanel/stake button",
              }),
              { token: <TokenName token={TokenInfoPresets.esUNW} /> },
            )}
          </Button.Text>
        </Button>

        <Button
          style={{ flex: 1 }}
          Variant={WhiteOutlineButtonVariant}
          onPress={safeReadResource(props.onUnstake)}
          disabled={safeReadResource(props.onUnstake) == null}
        >
          <Button.Text>
            {$t(
              defineMessage({
                defaultMessage: "Unstake",
                description: "EarnScreen/esUNWPanel/unstake button",
              }),
            )}
          </Button.Text>
        </Button>
      </View>
    </PanelContainer>
  )
}
