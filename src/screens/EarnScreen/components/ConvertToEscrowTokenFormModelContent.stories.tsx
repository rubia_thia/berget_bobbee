import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { noop } from "../../../utils/fnHelpers"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../utils/TokenInfoPresets/TokenInfoPresets"
import { ConvertToEscrowTokenFormModelContent } from "./ConvertToEscrowTokenFormModelContent"

export default {
  title: "Page/EarnScreen/ConvertToEscrowTokenFormModelContent",
  component: ConvertToEscrowTokenFormModelContent,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof ConvertToEscrowTokenFormModelContent>

const template = withTemplate(ConvertToEscrowTokenFormModelContent, {
  style: {
    margin: 10,
  },
  token: TokenInfoPresets.UNW,
  esToken: TokenInfoPresets.esUNW,
  availableTokenAmount: BigNumber.from(1000),
  tokenAmount: BigNumber.from(1011),
  willReceiveEsTokenAmount: BigNumber.from(1111),
  vestPeriod: { months: 6 },
  onPressMax: noop,
  onConvert: noop,
  onDismiss: noop,
  onTokenAmountChange: noop,
})

export const Normal = template()
