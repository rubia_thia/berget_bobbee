import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { ClaimConfirmModalContent } from "./ClaimConfirmModalContent"
import { claimConfirmModalContentProps } from "./ClaimConfirmModalContent.mockData"

export default {
  title: "Page/EarnScreen/ClaimConfirmModalContent",
  component: ClaimConfirmModalContent,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof ClaimConfirmModalContent>

const template = withTemplate(ClaimConfirmModalContent, {
  style: {
    margin: 10,
  },
  ...claimConfirmModalContentProps,
})

export const Normal = template()
