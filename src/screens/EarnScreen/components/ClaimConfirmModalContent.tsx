import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { ScrollView, StyleProp, Text, View, ViewStyle } from "react-native"
import { SmartLoadableButton } from "../../../components/ButtonFramework/LoadableButton"
import {
  CardBoxModalContent,
  TitleBar,
} from "../../../components/CardBoxModalContent/CardBoxModalContent"
import { Spensor } from "../../../components/Spensor"
import { TokenCountAsCurrency } from "../../../components/TextTokenCount"
import { useColors } from "../../../components/Themed/color"
import { useSpacing } from "../../../components/Themed/spacing"
import { TokenCount, TokenPresets } from "../../../components/TokenCount"
import { TokenIcon } from "../../../components/TokenIcon"
import { TokenName } from "../../../components/TokenName"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { readResource, SuspenseResource } from "../../../utils/SuspenseResource"
import { TokenInfo } from "../../../utils/TokenInfo"

export interface RewardToClaim {
  token: TokenInfo
  amount: SuspenseResource<BigNumber>
  estimatedUSD: SuspenseResource<BigNumber>
}

export interface ClaimConfirmModalContentProps {
  style?: StyleProp<ViewStyle>
  onDismiss: () => void

  rewardsToClaim: RewardToClaim[]

  onClaim: () => void | Promise<void>
}

export const ClaimConfirmModalContent: FC<
  ClaimConfirmModalContentProps
> = props => {
  const spacing = useSpacing()
  const { $t } = useIntl()

  return (
    <CardBoxModalContent
      style={props.style}
      padding={spacing(4)}
      onClose={props.onDismiss}
    >
      <TitleBar>
        {$t(
          defineMessage({
            defaultMessage: "Claim Rewards",
            description: "EarnScreen/ClaimConfirmModal/title",
          }),
        )}
      </TitleBar>

      <Spensor>
        {() => (
          <ScrollView
            horizontal={true}
            style={{
              marginVertical: spacing(6),
              marginHorizontal: -spacing(4),
            }}
            contentContainerStyle={{ flexGrow: 1, justifyContent: "center" }}
            showsHorizontalScrollIndicator={false}
          >
            <View style={{ width: spacing(4) }} />
            {props.rewardsToClaim
              .filter(r => BigNumber.isGtZero(readResource(r.amount)))
              .map((reward, i) => (
                <RecordCard
                  key={i}
                  style={[
                    { minWidth: 140 },
                    i !== 0 && { marginLeft: spacing(2.5) },
                  ]}
                  reward={reward}
                />
              ))}
            <View style={{ width: spacing(4) }} />
          </ScrollView>
        )}
      </Spensor>

      <SmartLoadableButton onPress={props.onClaim}>
        {$t(
          defineMessage({
            defaultMessage: "Claim",
            description: "EarnScreen/ClaimConfirmModal/claimButton",
          }),
        )}
      </SmartLoadableButton>
    </CardBoxModalContent>
  )
}

const RecordCard: FC<{
  style?: StyleProp<ViewStyle>
  reward: RewardToClaim
}> = props => {
  const spacing = useSpacing()
  const colors = useColors()

  return (
    <View
      style={[
        props.style,
        {
          alignItems: "center",
          borderWidth: 1,
          borderColor: colors("gray-200"),
          borderRadius: 8,
          paddingTop: spacing(5),
          paddingBottom: spacing(4),
          paddingHorizontal: spacing(3),
        },
      ]}
    >
      <TokenName
        style={{ fontSize: 16, fontWeight: "600", color: colors("gray-900") }}
        token={props.reward.token}
      />

      <TokenIcon
        style={{ marginTop: spacing(2), marginBottom: spacing(4) }}
        token={props.reward.token}
        size={56}
      />

      <TokenCount
        style={{ fontSize: 18, fontWeight: "600", color: colors("gray-900") }}
        token={props.reward.token}
        count={props.reward.amount}
      />

      <Text
        style={{ fontSize: 14, fontWeight: "400", color: colors("gray-500") }}
      >
        (
        <TokenCountAsCurrency
          token={TokenPresets.USD}
          count={props.reward.estimatedUSD}
        />
        )
      </Text>
    </View>
  )
}
