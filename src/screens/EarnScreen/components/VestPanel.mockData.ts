import { OptionalizeEventListeners } from "../../../../.storybook/utils"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { VestPanelProps } from "./VestPanel"

export const vestPanelProps: OptionalizeEventListeners<VestPanelProps> = {
  vestingDetailsLink: "https://www.google.com",
  vestPeriod: { months: 6 },
  activeVestingAmount: BigNumber.from(333.311111111111),
  canLock: true,
  canUnlock: true,
  claimableVest: {
    fromEsUNIAmount: BigNumber.from(21.222222),
    toUNWAmount: BigNumber.from(21.222222),
  },
}
