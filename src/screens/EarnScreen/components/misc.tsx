import { FC, ReactNode } from "react"
import { StyleProp, Text, View, ViewProps, ViewStyle } from "react-native"
import { CardBoxView } from "../../../components/CardBox/CardBox"
import { CardInset } from "../../../components/CardBox/CardInset"
import { useColors } from "../../../components/Themed/color"
import { useSpacing } from "../../../components/Themed/spacing"
import { FCC } from "../../../utils/reactHelpers/types"
import { styleGetters } from "../../../utils/reactHelpers/withProps/styleGetters"
import { withProps } from "../../../utils/reactHelpers/withProps/withProps"
import {
  PaddingStyle,
  useNormalizePaddingStyle,
} from "../../../utils/styleHelpers/PaddingStyle"

export const usePanelContainerGap = (): {
  horizontal: number
  vertical: number
} => {
  const spacing = useSpacing()

  return {
    horizontal: spacing(2.5),
    vertical: spacing(2.5),
  }
}

const usePanelContainerPadding = (
  overridePadding?: number | PaddingStyle,
): PaddingStyle => {
  const spacing = useSpacing()

  const defaultPadding = useNormalizePaddingStyle(spacing(6))
  const paddingStyle = useNormalizePaddingStyle(overridePadding)

  return {
    ...defaultPadding,
    ...paddingStyle,
  }
}

export const PanelContainer: FCC<{
  style?: StyleProp<ViewStyle>
  padding?: number | PaddingStyle
  onLayout?: ViewProps["onLayout"]
}> = props => {
  const paddingStyle = usePanelContainerPadding(props.padding)

  return (
    <CardBoxView
      style={props.style}
      padding={paddingStyle}
      onLayout={props.onLayout}
    >
      {props.children}
    </CardBoxView>
  )
}

export const PanelHeaderTitleText = withProps(
  styleGetters(({ colors }) => ({
    fontSize: 20,
    color: colors("gray-900"),
  })),
  Text,
)

export const PanelHeader: FC<{
  style?: StyleProp<ViewStyle>
  renderIcon?: (renderProps: { size: number }) => ReactNode
  title?: ReactNode
  bottomText?: ReactNode
  rightSide?: ReactNode
}> = props => {
  const colors = useColors()
  const spacing = useSpacing()

  const gap = spacing(2.5)

  return (
    <View style={[props.style, { flexDirection: "row", alignItems: "center" }]}>
      {props.renderIcon?.({ size: 48 })}

      <View style={{ marginLeft: props.renderIcon == null ? 0 : gap, flex: 1 }}>
        <View>{props.title}</View>

        {props.bottomText != null && (
          <Text style={{ fontSize: 14, color: colors("gray-500") }}>
            {props.bottomText}
          </Text>
        )}
      </View>

      {props.rightSide != null && (
        <View style={{ marginLeft: gap }}>{props.rightSide}</View>
      )}
    </View>
  )
}

export const PanelInset: FCC<{ style?: StyleProp<ViewStyle> }> = props => {
  const spacing = useSpacing()

  return (
    <CardInset
      style={[props.style, { borderRadius: 4 }]}
      padding={spacing(2.5)}
    >
      {props.children}
    </CardInset>
  )
}
export const PanelInsetGroup: FC<{
  style?: StyleProp<ViewStyle>
  children: (renderProps: { gap: number }) => ReactNode
}> = props => {
  const spacing = useSpacing()
  return (
    <View style={[{ flexDirection: "column" }, props.style]}>
      {props.children({ gap: spacing(1) })}
    </View>
  )
}
export const PanelInsetText = withProps(
  styleGetters(({ colors }) => ({ fontSize: 16, color: colors("gray-900") })),
  Text,
)

export const PanelOutlineInset: FCC<{
  style?: StyleProp<ViewStyle>
}> = props => {
  const spacing = useSpacing()
  const colors = useColors()
  return (
    <View
      style={[
        props.style,
        {
          paddingVertical: spacing(2),
          paddingHorizontal: spacing(4),
          borderWidth: 1,
          borderColor: colors("gray-300"),
          borderRadius: 6,
        },
      ]}
    >
      {props.children}
    </View>
  )
}

export const FullWidthPanelActionsArea: FCC<{
  style?: StyleProp<ViewStyle>
  contentContainerStyle?: StyleProp<ViewStyle>
  containerPadding?: number | PaddingStyle
  withoutHorizontalGap?: boolean
  withoutHorizontalPadding?: boolean
}> = props => {
  const { horizontal } = usePanelContainerGap()

  const containerPaddingStyle = usePanelContainerPadding(props.containerPadding)

  return (
    <View style={props.style}>
      <View
        style={[
          {
            width: "100%",
            height: "100%",
            paddingLeft:
              ((props.withoutHorizontalPadding
                ? 0
                : containerPaddingStyle.paddingLeft) ?? 0) +
              (props.withoutHorizontalGap ? 0 : horizontal) / 2,
          },
          props.contentContainerStyle,
        ]}
      >
        {props.children}
      </View>
    </View>
  )
}
