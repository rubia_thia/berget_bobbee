import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { noop } from "../../../utils/fnHelpers"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../utils/TokenInfoPresets/TokenInfoPresets"
import { UnstakeFormModalContent } from "./UnstakeFormModalContent"

export default {
  title: "Page/EarnScreen/UnstakeFormModalContent",
  component: UnstakeFormModalContent,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof UnstakeFormModalContent>

const template = withTemplate(UnstakeFormModalContent, {
  style: {
    margin: 10,
  },
  tokenToUnstake: TokenInfoPresets.UNW,
  tokenAmountAvailableToUnstake: BigNumber.from(1000),
  onUnstake: noop,
  onDismiss: noop,
})

export const Normal = template()
