import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, Text, View, ViewStyle } from "react-native"
import { confirm$t } from "../../../../commonIntlMessages"
import { SmartLoadableButton } from "../../../../components/ButtonFramework/LoadableButton"
import {
  CardBoxModalContent,
  TitleBar,
} from "../../../../components/CardBoxModalContent/CardBoxModalContent"
import { Divider } from "../../../../components/Divider"
import { PercentNumber } from "../../../../components/PercentNumber"
import { TextTokenCount } from "../../../../components/TextTokenCount"
import { useSpacing } from "../../../../components/Themed/spacing"
import { TokenName } from "../../../../components/TokenName"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { SuspenseResource } from "../../../../utils/SuspenseResource"
import { TokenInfo } from "../../../../utils/TokenInfo"
import { CompoundStrategy } from "../../types"
import ArrowIcon from "./assets/arrow.svg"
import { Checkbox } from "./Checkbox"

export interface CompoundModalContentProps {
  style?: StyleProp<ViewStyle>
  onDismiss: () => void

  strategy: CompoundStrategy
  onStrategyChange: (newStrategy: CompoundStrategy) => void

  usdtTokenInfo: TokenInfo
  usdtAmount: SuspenseResource<BigNumber>
  usdtStakeAsULPAPR: SuspenseResource<BigNumber>

  unwTokenInfo: TokenInfo
  unwAmount: SuspenseResource<BigNumber>
  uwnStakeAPR: SuspenseResource<BigNumber>
  uwnStakeAsEsUNWAPR: SuspenseResource<BigNumber>

  esUNWTokenInfo: TokenInfo
  esUNWAmount: SuspenseResource<BigNumber>
  esUNWStakeAPR: SuspenseResource<BigNumber>

  onSubmit: () => void | Promise<void>
}

export const CompoundModalContent: FC<CompoundModalContentProps> = props => {
  const spacing = useSpacing()
  const { $t } = useIntl()

  return (
    <CardBoxModalContent
      style={props.style}
      padding={spacing(4)}
      onClose={props.onDismiss}
    >
      <TitleBar>
        {$t(
          defineMessage({
            defaultMessage: "Claim / Compound Rewards",
            description: "EarnScreen/Compound Modal",
          }),
        )}
      </TitleBar>

      <View className={"space-y-2.5 my-4"}>
        <View className="pl-10 flex-row">
          <Text className="flex-1 text-[12px] text-gray-500">
            {$t(
              defineMessage({
                defaultMessage: "Type",
                description: "EarnScreen/Compound Modal",
              }),
            )}
          </Text>
          <Text className="flex-1 text-[12px] text-gray-500">
            {$t(
              defineMessage({
                defaultMessage: "Amount / APR",
                description: "EarnScreen/Compound Modal",
              }),
            )}
          </Text>
        </View>
        <Divider />
        <View className="flex-row items-center">
          <Checkbox
            checked={props.strategy.usdt != null}
            disabled={true}
            // disabled={props.strategy.usdt === "stake"}
            onCheckedChanged={on =>
              props.onStrategyChange({
                ...props.strategy,
                usdt: on ? "claim" : null,
              })
            }
            className="mr-4"
          />
          <Text className="flex-1 text-[14px] text-gray-900">
            {$t(
              defineMessage({
                defaultMessage: "Claim {token} rewards",
                description: "EarnScreen/Compound Modal",
              }),
              {
                token: <TokenName token={props.usdtTokenInfo} />,
              },
            )}
          </Text>
          <Text className="flex-1 text-gray-900 font-semibold">
            <TextTokenCount
              token={props.usdtTokenInfo}
              count={props.usdtAmount}
            />
          </Text>
        </View>
        <View className="flex-row items-center">
          <Checkbox
            disabled={props.strategy.usdt == null}
            checked={props.strategy.usdt === "stake"}
            onCheckedChanged={on =>
              props.onStrategyChange({
                ...props.strategy,
                usdt: on ? "stake" : "claim",
              })
            }
            className="mr-4"
          />
          <View className="flex-1 items-center flex-row space-x-2">
            <ArrowIcon />
            <Text className="flex-1 text-[14px] text-gray-900">
              {$t(
                defineMessage({
                  defaultMessage: "Stake {token} rewards",
                  description: "EarnScreen/Compound Modal",
                }),
                {
                  token: <TokenName token={props.usdtTokenInfo} />,
                },
              )}
            </Text>
          </View>
          <Text className="flex-1 text-green-600 font-medium">
            {$t(
              defineMessage({
                defaultMessage: "APR {apr}",
                description: "EarnScreen/Compound Modal",
              }),
              { apr: <PercentNumber number={props.usdtStakeAsULPAPR} /> },
            )}
          </Text>
        </View>
        <Divider />
        <View className="flex-row items-center">
          <Checkbox
            checked={props.strategy.unw != null}
            // disabled={
            //   props.strategy.unw != null && props.strategy.unw !== "claim"
            // }
            disabled={true}
            onCheckedChanged={on =>
              props.onStrategyChange({
                ...props.strategy,
                unw: on ? "claim" : null,
              })
            }
            className="mr-4"
          />
          <Text className="flex-1 text-[14px] text-gray-900">
            {$t(
              defineMessage({
                defaultMessage: "Claim {token} rewards",
                description: "EarnScreen/Compound Modal",
              }),
              {
                token: <TokenName token={props.unwTokenInfo} />,
              },
            )}
          </Text>
          <Text className="flex-1 text-gray-900 font-semibold">
            <TextTokenCount
              token={props.unwTokenInfo}
              count={props.unwAmount}
            />
          </Text>
        </View>
        <View className="flex-row items-center">
          <Checkbox
            disabled={props.strategy.unw == null}
            checked={props.strategy.unw === "stakeAsEsUnw"}
            onCheckedChanged={on =>
              props.onStrategyChange({
                ...props.strategy,
                unw: on ? "stakeAsEsUnw" : "claim",
              })
            }
            className="mr-4"
          />
          <View className="flex-1 items-center flex-row space-x-2">
            <ArrowIcon />
            <Text className="flex-1 text-[14px] text-gray-900">
              {$t(
                defineMessage({
                  defaultMessage: "Convert to {esToken} and stake",
                  description: "EarnScreen/Compound Modal",
                }),
                {
                  esToken: <TokenName token={props.esUNWTokenInfo} />,
                },
              )}
            </Text>
          </View>
          <Text className="flex-1 text-green-600 font-medium">
            {$t(
              defineMessage({
                defaultMessage: "APR {apr}",
                description: "EarnScreen/Compound Modal",
              }),
              { apr: <PercentNumber number={props.uwnStakeAsEsUNWAPR} /> },
            )}
          </Text>
        </View>
        <View className="flex-row items-center">
          <Checkbox
            disabled={props.strategy.unw == null}
            checked={props.strategy.unw === "stake"}
            onCheckedChanged={on =>
              props.onStrategyChange({
                ...props.strategy,
                unw: on ? "stake" : "claim",
              })
            }
            className="mr-4"
          />
          <View className="flex-1 items-center flex-row space-x-2">
            <ArrowIcon />
            <Text className="flex-1 text-[14px] text-gray-900">
              {$t(
                defineMessage({
                  defaultMessage: "Stake {token} rewards",
                  description: "EarnScreen/Compound Modal",
                }),
                {
                  token: <TokenName token={props.unwTokenInfo} />,
                },
              )}
            </Text>
          </View>
          <Text className="flex-1 text-green-600 font-medium">
            {$t(
              defineMessage({
                defaultMessage: "APR {apr}",
                description: "EarnScreen/Compound Modal",
              }),
              { apr: <PercentNumber number={props.uwnStakeAPR} /> },
            )}
          </Text>
        </View>
        <Divider />
        <View className="flex-row items-center">
          <Checkbox
            checked={props.strategy.esUNW != null}
            disabled={props.strategy.esUNW === "stake"}
            onCheckedChanged={on =>
              props.onStrategyChange({
                ...props.strategy,
                esUNW: on ? "claim" : null,
              })
            }
            className="mr-4"
          />
          <Text className="flex-1 text-[14px] text-gray-900">
            {$t(
              defineMessage({
                defaultMessage: "Claim {token} rewards",
                description: "EarnScreen/Compound Modal",
              }),
              {
                token: <TokenName token={props.esUNWTokenInfo} />,
              },
            )}
          </Text>
          <Text className="flex-1 text-gray-900 font-semibold">
            <TextTokenCount
              token={props.esUNWTokenInfo}
              count={props.esUNWAmount}
            />
          </Text>
        </View>
        <View className="flex-row items-center">
          <Checkbox
            disabled={props.strategy.esUNW == null}
            checked={props.strategy.esUNW === "stake"}
            onCheckedChanged={on =>
              props.onStrategyChange({
                ...props.strategy,
                esUNW: on ? "stake" : "claim",
              })
            }
            className="mr-4"
          />
          <View className="flex-1 items-center flex-row space-x-2">
            <ArrowIcon />
            <Text className="flex-1 text-[14px] text-gray-900">
              {$t(
                defineMessage({
                  defaultMessage: "Stake {token}",
                  description: "EarnScreen/Compound Modal",
                }),
                {
                  token: <TokenName token={props.esUNWTokenInfo} />,
                },
              )}
            </Text>
          </View>
          <Text className="flex-1 text-green-600 font-medium">
            {$t(
              defineMessage({
                defaultMessage: "APR {apr}",
                description: "EarnScreen/Compound Modal",
              }),
              { apr: <PercentNumber number={props.esUNWStakeAPR} /> },
            )}
          </Text>
        </View>
        <Divider />
      </View>

      <SmartLoadableButton
        disabled={
          props.strategy.usdt == null &&
          props.strategy.unw == null &&
          props.strategy.esUNW == null
        }
        onPress={props.onSubmit}
      >
        {$t(confirm$t)}
      </SmartLoadableButton>
    </CardBoxModalContent>
  )
}
