import { TouchableOpacity } from "react-native"
import Svg, { G, Rect } from "react-native-svg"
import { FCS } from "../../../../utils/reactHelpers/types"

type CheckboxProps = {
  checked: boolean
  disabled?: boolean
  onCheckedChanged: (checked: boolean) => void
}

export const Checkbox: FCS<CheckboxProps> = props => {
  return (
    <TouchableOpacity
      disabled={props.disabled}
      onPress={() => props.onCheckedChanged(!props.checked)}
      style={props.style}
    >
      <Svg width="24" height="24" viewBox="0 0 24 24" fill="none">
        <Rect
          x="0.5"
          y="0.5"
          width="23"
          height="23"
          rx="3.5"
          fill={
            props.disabled
              ? "#F3F4F6"
              : props.checked
              ? "#DBEAFE"
              : "transparent"
          }
        />
        {props.checked && (
          <G opacity={props.disabled ? 0.3 : 1}>
            <path
              d="M10.7885 16.3269L6.75 12.047L7.87996 10.8924L10.7671 13.9103L16.0752 8.25L17.25 9.38319L10.7885 16.3269Z"
              fill="black"
            />
          </G>
        )}
        <Rect
          x="0.5"
          y="0.5"
          width="23"
          height="23"
          rx="3.5"
          stroke="#CBD5E1"
        />
      </Svg>
    </TouchableOpacity>
  )
}
