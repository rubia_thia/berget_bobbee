import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { CompoundModalContent } from "./CompoundModalContent"

export default {
  title: "Page/EarnScreen/CompoundModalContent",
  component: CompoundModalContent,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof CompoundModalContent>

const template = withTemplate(CompoundModalContent, {
  style: {
    margin: 10,
  },
  strategy: {
    unw: "claim",
    usdt: "claim",
    esUNW: "claim",
  },
  usdtTokenInfo: TokenInfoPresets.MockUSDT,
  uwnStakeAPR: BigNumber.from(12),
  uwnStakeAsEsUNWAPR: BigNumber.from(21),
  esUNWAmount: BigNumber.from(123),
  esUNWStakeAPR: BigNumber.from(123),
  esUNWTokenInfo: TokenInfoPresets.esUNW,
  unwAmount: BigNumber.from(123),
  unwTokenInfo: TokenInfoPresets.UNW,
  usdtStakeAsULPAPR: BigNumber.from(12),
  usdtAmount: BigNumber.from(1231),
})

export const Normal = template()
