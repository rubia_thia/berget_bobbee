import { OptionalizeEventListeners } from "../../../../.storybook/utils"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../utils/TokenInfoPresets/TokenInfoPresets"
import { ClaimConfirmModalContentProps } from "./ClaimConfirmModalContent"

export const claimConfirmModalContentProps: OptionalizeEventListeners<ClaimConfirmModalContentProps> =
  {
    rewardsToClaim: [
      {
        token: TokenInfoPresets.MockBUSD,
        amount: BigNumber.from(3.587165282),
        estimatedUSD: BigNumber.from(3.587165282),
      },
      {
        token: TokenInfoPresets.MockUSDT,
        amount: BigNumber.from(3.587165282),
        estimatedUSD: BigNumber.from(3.587165282),
      },
    ],
  }
