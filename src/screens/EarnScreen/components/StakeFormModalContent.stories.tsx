import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { noop } from "../../../utils/fnHelpers"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../utils/TokenInfoPresets/TokenInfoPresets"
import { StakeFormModalContent } from "./StakeFormModalContent"

export default {
  title: "Page/EarnScreen/StakeFormModalContent",
  component: StakeFormModalContent,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof StakeFormModalContent>

const template = withTemplate(StakeFormModalContent, {
  style: {
    margin: 10,
  },
  tokenToStake: TokenInfoPresets.UNW,
  tokenAmountAvailableToStake: BigNumber.from(1000),
  onStake: noop,
  onDismiss: noop,
})

export const Normal = template()
