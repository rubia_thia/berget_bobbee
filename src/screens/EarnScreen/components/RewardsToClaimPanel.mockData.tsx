import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../utils/TokenInfoPresets/TokenInfoPresets"
import { RewardsToClaimPanelCoreProps } from "./RewardsToClaimPanel"

export const rewardsToClaimProps: RewardsToClaimPanelCoreProps = {
  canVest: true,
  rewardsEstimatedUSD: BigNumber.from(16.89),
  rewardsToClaim: {
    esUNW: {
      amount: BigNumber.from(16.43),
      valueInUSD: BigNumber.from(20.222),
      token: TokenInfoPresets.esUNW,
      stakeReward: BigNumber.from(20.222),
      traderReward: BigNumber.from(20.222),
    },
    usdt: {
      token: TokenInfoPresets.MockUSDT,
      amount: BigNumber.from(16.43),
      valueInUSD: BigNumber.from(20.222),
    },
    unw: {
      token: TokenInfoPresets.UNW,
      amount: BigNumber.from(16.43),
      valueInUSD: BigNumber.from(20.222),
    },
  },
}
