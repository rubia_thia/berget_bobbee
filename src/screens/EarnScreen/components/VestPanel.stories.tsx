import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { VestPanel } from "./VestPanel"
import { vestPanelProps } from "./VestPanel.mockData"

export default {
  title: "Page/EarnScreen/VestPanel",
  component: VestPanel,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof VestPanel>

const template = withTemplate(VestPanel, {
  style: {
    margin: 10,
  },
  ...vestPanelProps,
})

export const Normal = template()
