import { FC, ReactNode, useState } from "react"
import { StyleProp, TextStyle, TouchableOpacity, ViewProps } from "react-native"
import { Path, Svg } from "react-native-svg"
import { InfoList } from "../../../components/InfoList/InfoList"
import { DefaultInfoListItem } from "../../../components/InfoList/InfoListItem"
import { InfoListItemDetailTextStyleProvider } from "../../../components/InfoList/InfoListItemDetail"
import {
  DefaultInfoListItemTitle,
  InfoListItemTitleTextStyleProvider,
} from "../../../components/InfoList/InfoListItemTitle"
import { useColors } from "../../../components/Themed/color"
import { useSpacing } from "../../../components/Themed/spacing"
import { FCC } from "../../../utils/reactHelpers/types"
import { styleGetters } from "../../../utils/reactHelpers/withProps/styleGetters"
import { withProps } from "../../../utils/reactHelpers/withProps/withProps"

export interface BreakdownListProps {
  collapsable?: boolean
  headInfoListItem?: ReactNode
  detailInfoListItems?: ReactNode
}

export const BreakdownList: FC<BreakdownListProps> = props => {
  const colors = useColors()

  const [collapsed, setCollapsed] = useState(props.collapsable ? true : false)

  const headTextStyle: TextStyle = {
    fontSize: 16,
    color: colors("gray-900"),
  }

  const detailTextStyle: TextStyle = {
    fontSize: 12,
    color: colors("gray-500"),
  }

  return (
    <>
      {props.headInfoListItem != null && (
        <InfoList
          direction={"column"}
          listItemDirection={"row"}
          renderInfoListItemTitle={p => (
            <HeadInfoListItemTitle
              {...p}
              collapsed={collapsed}
              onToggleCollapse={
                props.collapsable ? () => setCollapsed(v => !v) : undefined
              }
            />
          )}
        >
          <InfoListItemTitleTextStyleProvider style={headTextStyle}>
            <InfoListItemDetailTextStyleProvider style={headTextStyle}>
              {props.headInfoListItem}
            </InfoListItemDetailTextStyleProvider>
          </InfoListItemTitleTextStyleProvider>
        </InfoList>
      )}

      {props.detailInfoListItems != null && !collapsed && (
        <InfoList
          direction={"column"}
          listItemDirection={"row"}
          renderInfoListItem={p => <DetailInfoListItem {...p} />}
        >
          <InfoListItemTitleTextStyleProvider style={detailTextStyle}>
            <InfoListItemDetailTextStyleProvider style={detailTextStyle}>
              {props.detailInfoListItems}
            </InfoListItemDetailTextStyleProvider>
          </InfoListItemTitleTextStyleProvider>
        </InfoList>
      )}
    </>
  )
}

const HeadInfoListItemTitle: FCC<{
  style?: StyleProp<ViewProps>
  collapsed: boolean
  onToggleCollapse?: () => void
}> = props => {
  const spacing = useSpacing()

  if (props.onToggleCollapse == null) {
    return (
      <DefaultInfoListItemTitle style={props.style}>
        {props.children}
      </DefaultInfoListItemTitle>
    )
  }

  return (
    <DefaultInfoListItemTitle style={props.style}>
      <TouchableOpacity
        style={{ flexDirection: "row", alignItems: "center" }}
        onPress={props.onToggleCollapse}
      >
        {props.children}

        <Svg
          style={{
            marginLeft: spacing(1),
            transform: [{ rotate: props.collapsed ? "180deg" : "0deg" }],
          }}
          width="16"
          height="16"
          viewBox="0 0 16 16"
          fill="black"
        >
          <Path d="M8.27276 4.61766L2.45715 10.3902L3.39996 11.333L8.27315 6.45984L13.1456 11.3323L14.0884 10.3895L8.27276 4.61766Z" />
        </Svg>
      </TouchableOpacity>
    </DefaultInfoListItemTitle>
  )
}

const DetailInfoListItem = withProps(
  styleGetters(({ spacing }) => ({
    marginTop: spacing(1),
  })),
  DefaultInfoListItem,
)
