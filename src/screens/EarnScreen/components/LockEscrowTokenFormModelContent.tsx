import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, Text, TouchableOpacity, ViewStyle } from "react-native"
import { SmartLoadableButton } from "../../../components/ButtonFramework/LoadableButton"
import { CardInset } from "../../../components/CardBox/CardInset"
import {
  CardBoxModalContent,
  TitleBar,
  TitleBarText,
} from "../../../components/CardBoxModalContent/CardBoxModalContent"
import { InfoList } from "../../../components/InfoList/InfoList"
import { InfoListItem } from "../../../components/InfoList/InfoListItem"
import { InfoListItemDetail } from "../../../components/InfoList/InfoListItemDetail"
import {
  InfoListItemTitle,
  InfoListItemTitleText,
} from "../../../components/InfoList/InfoListItemTitle"
import {
  NoteParagraph,
  NoteParagraphText,
} from "../../../components/NoteParagraph/NoteParagraph"
import { Spensor } from "../../../components/Spensor"
import { TextTokenCount } from "../../../components/TextTokenCount"
import { useColors } from "../../../components/Themed/color"
import { useSpacing } from "../../../components/Themed/spacing"
import { BalanceTopArea } from "../../../components/TokenInput/BalanceTopArea"
import { TokenInput } from "../../../components/TokenInput/TokenInput"
import { TokenName } from "../../../components/TokenName"
import { formatDuration } from "../../../utils/datetimeHelpers"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { mathIs } from "../../../utils/numberHelpers/bigNumberExpressionParser"
import { HrefLink } from "../../../utils/reactNavigationHelpers/HrefLink"
import {
  readResource,
  safeReadResource,
  SuspenseResource,
} from "../../../utils/SuspenseResource"
import { TokenInfo } from "../../../utils/TokenInfo"

export interface LockEscrowTokenFormModelContentProps {
  style?: StyleProp<ViewStyle>
  onDismiss: () => void

  esToken: TokenInfo
  token: TokenInfo
  availableEsTokenAmount: SuspenseResource<BigNumber>

  esTokenAmount: SuspenseResource<BigNumber>
  onEsTokenAmountChange: (newAmount: null | BigNumber) => void
  onPressMax: () => void

  willBeVestedTokenCount: SuspenseResource<BigNumber>
  vestPeriod: SuspenseResource<Duration>
  vestingDetailsLink: string

  onLock: () => void | Promise<void>
}

export const LockEscrowTokenFormModelContent: FC<
  LockEscrowTokenFormModelContentProps
> = props => {
  const spacing = useSpacing()
  const colors = useColors()
  const intl = useIntl()

  const { $t } = intl

  const amount = safeReadResource(props.esTokenAmount)

  return (
    <CardBoxModalContent
      className="space-y-5"
      style={props.style}
      padding={spacing(4)}
      onClose={props.onDismiss}
    >
      <TitleBar>
        <TitleBarText>
          {$t(
            defineMessage({
              defaultMessage: "Lock to vest",
              description: "EarnScreen/Lock Escrow Token Form Modal/title",
            }),
            {
              token: <TokenName token={props.esToken} />,
            },
          )}
        </TitleBarText>
      </TitleBar>

      <TokenInput
        availableTokens={[]}
        token={props.esToken}
        value={props.esTokenAmount}
        onValueChange={props.onEsTokenAmountChange}
        topArea={
          <BalanceTopArea
            titleText={$t(
              defineMessage({
                defaultMessage: "Lock",
                description: "EarnScreen/Lock Escrow Token Form Modal/label",
              }),
            )}
            token={props.esToken}
            balance={props.availableEsTokenAmount}
          />
        }
        placeholder={"0.0"}
        onPressMax={props.onPressMax}
      />

      <CardInset padding={spacing(3)}>
        <InfoList direction={"column"} listItemDirection={"row"}>
          <InfoListItem>
            <InfoListItemTitle>
              {$t(
                defineMessage({
                  defaultMessage: "Vest",
                  description:
                    "EarnScreen/Lock escrow token form modal content/info plate/title",
                }),
              )}
            </InfoListItemTitle>
            <InfoListItemDetail>
              <InfoListItemTitleText>
                {$t(
                  defineMessage({
                    defaultMessage: "{esTokenCount} → {tokenCount}",
                    description:
                      "EarnScreen/Lock escrow token form modal content/info plate/detail",
                  }),
                  {
                    esTokenCount: (
                      <TextTokenCount
                        token={props.esToken}
                        count={props.esTokenAmount}
                      />
                    ),
                    tokenCount: (
                      <TextTokenCount
                        token={props.token}
                        count={props.willBeVestedTokenCount}
                      />
                    ),
                  },
                )}
              </InfoListItemTitleText>
            </InfoListItemDetail>
          </InfoListItem>
          <InfoListItem style={{ marginTop: spacing(1) }}>
            <InfoListItemTitle>
              {$t(
                defineMessage({
                  defaultMessage: "Vesting duration",
                  description:
                    "EarnScreen/Lock escrow token form modal content/info plate/title",
                }),
              )}
            </InfoListItemTitle>
            <InfoListItemDetail>
              <InfoListItemTitleText>
                <Spensor>
                  {() => (
                    <>{formatDuration(intl, readResource(props.vestPeriod))}</>
                  )}
                </Spensor>
              </InfoListItemTitleText>
            </InfoListItemDetail>
          </InfoListItem>
        </InfoList>
      </CardInset>

      <SmartLoadableButton
        disabled={
          amount == null ||
          mathIs`${amount} <= ${0} || ${amount} > ${
            safeReadResource(props.availableEsTokenAmount) ?? Number.MAX_VALUE
          }`
        }
        onPress={async () => {
          await props.onLock()
          props.onDismiss()
        }}
      >
        {$t(
          defineMessage({
            defaultMessage: "Lock to vest",
            description:
              "EarnScreen/Lock Escrow Token Form Modal/submit button",
          }),
        )}
      </SmartLoadableButton>

      <NoteParagraph>
        <NoteParagraphText>
          {$t(
            defineMessage({
              defaultMessage:
                "Locking {token} to convert to {token} is subject {vestDuration} linear vesting. You can unlock {token} any time. <vestingDetailsLink>Details ></vestingDetailsLink>",
              description:
                "EarnScreen/Lock Escrow Token Form Modal/bottom message",
            }),
            {
              esToken: <TokenName token={props.esToken} />,
              token: <TokenName token={props.token} />,
              vestDuration: (
                <Spensor fallback={"-"}>
                  {() => (
                    <>{formatDuration(intl, readResource(props.vestPeriod))}</>
                  )}
                </Spensor>
              ),
              vestingDetailsLink: contents => (
                <HrefLink href={props.vestingDetailsLink}>
                  {p => (
                    <TouchableOpacity {...p}>
                      <Text style={{ color: colors("blue-600") }}>
                        {contents}
                      </Text>
                    </TouchableOpacity>
                  )}
                </HrefLink>
              ),
            },
          )}
        </NoteParagraphText>
      </NoteParagraph>
    </CardBoxModalContent>
  )
}
