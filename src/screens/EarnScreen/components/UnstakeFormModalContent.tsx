import { FC, useState } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, View, ViewStyle } from "react-native"
import { SmartLoadableButton } from "../../../components/ButtonFramework/LoadableButton"
import {
  CardBoxModalContent,
  TitleBar,
  TitleBarText,
} from "../../../components/CardBoxModalContent/CardBoxModalContent"
import { useSpacing } from "../../../components/Themed/spacing"
import { BalanceTopArea } from "../../../components/TokenInput/BalanceTopArea"
import { TokenInput } from "../../../components/TokenInput/TokenInput"
import { TokenName } from "../../../components/TokenName"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { mathIs } from "../../../utils/numberHelpers/bigNumberExpressionParser"
import {
  safeReadResource,
  SuspenseResource,
} from "../../../utils/SuspenseResource"
import { TokenInfo } from "../../../utils/TokenInfo"

export interface UnstakeFormModalContentProps {
  style?: StyleProp<ViewStyle>
  onDismiss: () => void

  tokenToUnstake: TokenInfo
  tokenAmountAvailableToUnstake: SuspenseResource<BigNumber>

  onUnstake: (amount: BigNumber) => void | Promise<void>
}

export const UnstakeFormModalContent: FC<
  UnstakeFormModalContentProps
> = props => {
  const spacing = useSpacing()
  const { $t } = useIntl()

  const [amount, setAmount] = useState<BigNumber>()

  return (
    <CardBoxModalContent
      style={props.style}
      padding={spacing(4)}
      onClose={props.onDismiss}
    >
      <View className="space-y-5">
        <TitleBar>
          <TitleBarText>
            {$t(
              defineMessage({
                defaultMessage: "Unstake {token}",
                description: "EarnScreen/UnstakeConfirmModal/title",
              }),
              {
                token: <TokenName token={props.tokenToUnstake} />,
              },
            )}
          </TitleBarText>
        </TitleBar>

        <TokenInput
          availableTokens={[]}
          token={props.tokenToUnstake}
          value={amount ?? null}
          onValueChange={a => setAmount(a ?? undefined)}
          topArea={
            <BalanceTopArea
              titleText={$t(
                defineMessage({
                  defaultMessage: "Unstake",
                  description: "EarnScreen/UnstakeConfirmModal/label",
                }),
              )}
              token={props.tokenToUnstake}
              balance={props.tokenAmountAvailableToUnstake}
            />
          }
          placeholder={"0.0"}
          onPressMax={() => {
            setAmount(safeReadResource(props.tokenAmountAvailableToUnstake))
          }}
        />

        <SmartLoadableButton
          disabled={
            amount == null ||
            mathIs`${amount} <= ${0} || ${amount} > ${
              safeReadResource(props.tokenAmountAvailableToUnstake) ??
              Number.MAX_VALUE
            }`
          }
          onPress={async () => {
            await props.onUnstake(amount!)
            props.onDismiss()
          }}
        >
          {$t(
            defineMessage({
              defaultMessage: "Unstake",
              description: "EarnScreen/UnstakeConfirmModal/unstake button",
            }),
          )}
        </SmartLoadableButton>
      </View>
    </CardBoxModalContent>
  )
}
