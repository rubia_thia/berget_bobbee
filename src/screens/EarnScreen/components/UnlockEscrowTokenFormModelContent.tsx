import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, Text, ViewStyle } from "react-native"
import { SmartLoadableButton } from "../../../components/ButtonFramework/LoadableButton"
import { CardInset } from "../../../components/CardBox/CardInset"
import {
  CardBoxModalContent,
  TitleBar,
  TitleBarText,
} from "../../../components/CardBoxModalContent/CardBoxModalContent"
import { InfoList } from "../../../components/InfoList/InfoList"
import { InfoListItem } from "../../../components/InfoList/InfoListItem"
import { InfoListItemDetail } from "../../../components/InfoList/InfoListItemDetail"
import {
  InfoListItemTitle,
  InfoListItemTitleText,
} from "../../../components/InfoList/InfoListItemTitle"
import {
  NoteParagraph,
  NoteParagraphText,
} from "../../../components/NoteParagraph/NoteParagraph"
import { TextTokenCount } from "../../../components/TextTokenCount"
import { useSpacing } from "../../../components/Themed/spacing"
import { BalanceTopArea } from "../../../components/TokenInput/BalanceTopArea"
import { TokenInput } from "../../../components/TokenInput/TokenInput"
import { TokenName } from "../../../components/TokenName"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { mathIs } from "../../../utils/numberHelpers/bigNumberExpressionParser"
import {
  safeReadResource,
  SuspenseResource,
} from "../../../utils/SuspenseResource"
import { TokenInfo } from "../../../utils/TokenInfo"

export interface UnlockEscrowTokenFormModelContentProps {
  style?: StyleProp<ViewStyle>
  onDismiss: () => void

  esToken: TokenInfo
  availableEsTokenAmount: SuspenseResource<BigNumber>

  esTokenAmount: SuspenseResource<BigNumber>
  onEsTokenAmountChange: (newAmount: null | BigNumber) => void
  onPressMax: () => void

  esTokenAmountBeforeUnlock: SuspenseResource<BigNumber>
  esTokenAmountAfterUnlock: SuspenseResource<BigNumber>

  onUnlock: () => void | Promise<void>
}

export const UnlockEscrowTokenFormModelContent: FC<
  UnlockEscrowTokenFormModelContentProps
> = props => {
  const spacing = useSpacing()
  const intl = useIntl()

  const { $t } = intl

  const amount = safeReadResource(props.esTokenAmount)

  return (
    <CardBoxModalContent
      className="space-y-5"
      style={props.style}
      padding={spacing(4)}
      onClose={props.onDismiss}
    >
      <TitleBar>
        <TitleBarText>
          {$t(
            defineMessage({
              defaultMessage: "Unlock {token}",
              description: "EarnScreen/Unlock Escrow Token Form Modal/title",
            }),
            {
              token: <TokenName token={props.esToken} />,
            },
          )}
        </TitleBarText>
      </TitleBar>

      <TokenInput
        availableTokens={[]}
        token={props.esToken}
        value={props.esTokenAmount}
        onValueChange={props.onEsTokenAmountChange}
        topArea={
          <BalanceTopArea
            titleText={$t(
              defineMessage({
                defaultMessage: "Unlock",
                description: "EarnScreen/Unlock Escrow Token Form Modal/label",
              }),
            )}
            token={props.esToken}
            balance={props.availableEsTokenAmount}
          />
        }
        placeholder={"0.0"}
        onPressMax={props.onPressMax}
      />

      <CardInset padding={spacing(3)}>
        <InfoList direction={"column"} listItemDirection={"row"}>
          <InfoListItem>
            <InfoListItemTitle>
              <Text>
                {$t(
                  defineMessage({
                    defaultMessage: "Locked {token}",
                    description:
                      "EarnScreen/Lock escrow token form modal content/info plate/title",
                  }),
                  { token: <TokenName token={props.esToken} /> },
                )}
              </Text>
            </InfoListItemTitle>
            <InfoListItemDetail>
              <InfoListItemTitleText>
                {$t(
                  defineMessage({
                    defaultMessage:
                      "{tokenCountBeforeUnlock} → {tokenCountAfterUnlock}",
                    description:
                      "EarnScreen/Lock escrow token form modal content/info plate/detail",
                  }),
                  {
                    tokenCountBeforeUnlock: (
                      <TextTokenCount
                        token={props.esToken}
                        count={props.esTokenAmountBeforeUnlock}
                      />
                    ),
                    tokenCountAfterUnlock: (
                      <TextTokenCount
                        token={props.esToken}
                        count={props.esTokenAmountAfterUnlock}
                      />
                    ),
                  },
                )}
              </InfoListItemTitleText>
            </InfoListItemDetail>
          </InfoListItem>
        </InfoList>
      </CardInset>

      <SmartLoadableButton
        disabled={
          amount == null ||
          mathIs`${amount} <= ${0} || ${amount} > ${
            safeReadResource(props.availableEsTokenAmount) ?? Number.MAX_VALUE
          }`
        }
        onPress={async () => {
          await props.onUnlock()
          props.onDismiss()
        }}
      >
        {$t(
          defineMessage({
            defaultMessage: "Unlock",
            description:
              "EarnScreen/Unlock Escrow Token Form Modal/submit button",
          }),
        )}
      </SmartLoadableButton>

      <NoteParagraph>
        <NoteParagraphText>
          {$t(
            defineMessage({
              defaultMessage:
                "The {token} withdrawn can continue to earn rewards through staking.",
              description:
                "EarnScreen/Unlock Escrow Token Form Modal/bottom message",
            }),
            {
              token: <TokenName token={props.esToken} />,
            },
          )}
        </NoteParagraphText>
      </NoteParagraph>
    </CardBoxModalContent>
  )
}
