import clsx from "clsx"
import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import {
  ActivityIndicator,
  ScrollView,
  StyleProp,
  Text,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import { SmartLoadableButton } from "../../../components/ButtonFramework/LoadableButton"
import { CardInset } from "../../../components/CardBox/CardInset"
import {
  CardBoxModalContent,
  TitleBar,
} from "../../../components/CardBoxModalContent/CardBoxModalContent"
import { Spensor } from "../../../components/Spensor"
import { useSpacing } from "../../../components/Themed/spacing"
import { TruncatedAddressText } from "../../../components/TruncatedAddress"
import {
  readResource,
  safeReadResource,
  SuspenseResource,
} from "../../../utils/SuspenseResource"
import CheckIcon from "./assets/check.svg"

export interface StakeConfirmModalContentProps {
  style?: StyleProp<ViewStyle>
  onDismiss: () => void
  ownedNFTs: SuspenseResource<
    {
      contractAddress: string
      nftIndex: number
    }[]
  >
  onSelectNFT: (contractAddress: string, nftIndex: number) => void
  selectedNFT: SuspenseResource<null | {
    contractAddress: string
    nftIndex: number
  }>

  onStake: () => void | Promise<void>
}

export const StakeNFTConfirmModalContent: FC<
  StakeConfirmModalContentProps
> = props => {
  const spacing = useSpacing()
  const { $t } = useIntl()

  return (
    <CardBoxModalContent
      style={props.style}
      padding={spacing(4)}
      onClose={props.onDismiss}
    >
      <View className="space-y-4">
        <TitleBar>
          {$t(
            defineMessage({
              defaultMessage: "Stake NFT",
              description: "EarnScreen/StakeConfirmModal",
            }),
          )}
        </TitleBar>

        <Text className="text-gray-900 font-medium">
          {$t(
            defineMessage({
              defaultMessage: "Choose one NFT to stake:",
              description: "EarnScreen/StakeConfirmModal",
            }),
          )}
        </Text>

        <ScrollView className="h-[150px] border border-gray-200 rounded">
          <View className="flex-row border-b border-gray-200 p-3">
            <Text className="text-gray-500 text-xs w-[100px]">NFT Number</Text>
            <Text className="text-gray-500 text-xs">Contract</Text>
          </View>
          <Spensor fallback={<ActivityIndicator className="mx-auto mt-20" />}>
            {() => {
              const selectedNft = readResource(props.selectedNFT)
              return (
                <>
                  {readResource(props.ownedNFTs).map(nft => {
                    const isSelected =
                      selectedNft != null &&
                      selectedNft.nftIndex === nft.nftIndex &&
                      selectedNft.contractAddress === nft.contractAddress
                    return (
                      <TouchableOpacity
                        key={`${nft.contractAddress}-${nft.nftIndex}`}
                        onPress={() =>
                          props.onSelectNFT(nft.contractAddress, nft.nftIndex)
                        }
                        className={clsx(
                          "flex-row border-b border-gray-200 px-3 h-10 items-center",
                          isSelected && "bg-blue-100",
                        )}
                      >
                        <Text className="text-gray-900 text-xs w-[100px]">
                          #{nft.nftIndex}
                        </Text>
                        <Text className="text-gray-900 text-xs flex-1">
                          <TruncatedAddressText address={nft.contractAddress} />
                        </Text>
                        {isSelected && <CheckIcon />}
                      </TouchableOpacity>
                    )
                  })}
                </>
              )
            }}
          </Spensor>
        </ScrollView>

        <CardInset
          padding={{
            paddingHorizontal: spacing(3),
            paddingVertical: spacing(2.5),
          }}
        >
          <View className="flex-row justify-between">
            <Text className="text-xs text-gray-500">
              {$t(
                defineMessage({
                  defaultMessage: "Stake NFT",
                  description: "EarnScreen/StakeConfirmModal",
                }),
              )}
            </Text>
            <Text className="text-gray-900 text-sm">
              <Spensor fallback="-">
                {() => readResource(props.selectedNFT)?.nftIndex || "-"}
              </Spensor>
            </Text>
          </View>
          <View className="flex-row justify-between">
            <Text className="text-xs text-gray-500">
              {$t(
                defineMessage({
                  defaultMessage: "NFT Contract",
                  description: "EarnScreen/StakeConfirmModal",
                }),
              )}
            </Text>
            <Text className="text-gray-900 text-sm">
              <Spensor fallback="-">
                {() => {
                  const address = readResource(
                    props.selectedNFT,
                  )?.contractAddress
                  if (address) {
                    return <TruncatedAddressText address={address} />
                  }
                  return "-"
                }}
              </Spensor>
            </Text>
          </View>
        </CardInset>
        <SmartLoadableButton
          onPress={props.onStake}
          disabled={safeReadResource(props.selectedNFT) == null}
        >
          {$t(
            defineMessage({
              defaultMessage: "Stake",
              description: "EarnScreen/StakeConfirmModal",
            }),
          )}
        </SmartLoadableButton>
      </View>
    </CardBoxModalContent>
  )
}
