import { FC, ReactNode } from "react"
import { defineMessage, useIntl } from "react-intl"
import { Text, View } from "react-native"
import { InfoList } from "../../../components/InfoList/InfoList"
import {
  DefaultInfoListItem,
  InfoListItem,
} from "../../../components/InfoList/InfoListItem"
import {
  DefaultInfoListItemDetail,
  DefaultInfoListItemDetailText,
  InfoListItemDetail,
  InfoListItemDetailText,
} from "../../../components/InfoList/InfoListItemDetail"
import { InfoListItemTitle } from "../../../components/InfoList/InfoListItemTitle"
import { PercentNumber } from "../../../components/PercentNumber"
import { useColors } from "../../../components/Themed/color"
import { useSpacing } from "../../../components/Themed/spacing"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { HrefLink } from "../../../utils/reactNavigationHelpers/HrefLink"
import { SuspenseResource } from "../../../utils/SuspenseResource"

export const APRTooltipContent: FC<{
  revenueAPR: SuspenseResource<BigNumber>
  emissionAPR: SuspenseResource<BigNumber>
}> = props => {
  const spacing = useSpacing()
  const colors = useColors()
  const { $t } = useIntl()

  return (
    <View>
      <InfoList
        style={{ minWidth: 240 }}
        direction={"column"}
        listItemDirection={"row"}
        renderInfoListItem={p => (
          <DefaultInfoListItem
            {...p}
            style={[
              p.style,
              {
                alignItems: "center",
                justifyContent: "space-between",
              },
            ]}
          />
        )}
        renderInfoListItemDetail={p => (
          <DefaultInfoListItemDetail {...p} alignSelf={"right"} />
        )}
        renderInfoListItemDetailText={p => (
          <DefaultInfoListItemDetailText
            {...p}
            style={[p.style, { color: colors("white") }]}
          />
        )}
      >
        <InfoListItem>
          <InfoListItemTitle>
            {$t(
              defineMessage({
                defaultMessage: "Fee APR",
                description: "EarnScreen/EsUNW Pannel/APR",
              }),
            )}
          </InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              <PercentNumber number={props.revenueAPR} />
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </InfoListItem>
        <InfoListItem style={{ marginTop: spacing(1) }}>
          <InfoListItemTitle>
            {$t(
              defineMessage({
                defaultMessage: "Emission APR",
                description: "EarnScreen/EsUNW Pannel/APR",
              }),
            )}
          </InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              <PercentNumber number={props.emissionAPR} />
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </InfoListItem>
      </InfoList>
      <Text className="mt-2 text-[12px] text-white">
        {$t<ReactNode>(
          defineMessage({
            defaultMessage:
              "7Day APR is moving average of each block APR. <learnMoreLink>Learn more ></learnMoreLink>",
            description: "EarnScreen/EsUNW Pannel/APR",
          }),
          {
            learnMoreLink: content => (
              <HrefLink
                href={
                  "https://docs.uniwhale.co/tokenomics/revenue-distribution-and-emission#apr"
                }
              >
                <Text style={{ color: colors("blue-600") }}>{content}</Text>
              </HrefLink>
            ),
          },
        )}
      </Text>
    </View>
  )
}
