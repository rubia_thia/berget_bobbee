import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { ConvertToEscrowTokenPanel } from "./ConvertToEscrowTokenPanel"
import { convertToEscrowTokenPanelProps } from "./ConvertToEscrowTokenPanel.mockData"

export default {
  title: "Page/EarnScreen/ConvertToEscrowTokenPanel",
  component: ConvertToEscrowTokenPanel,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof ConvertToEscrowTokenPanel>

const template = withTemplate(ConvertToEscrowTokenPanel, {
  style: {
    margin: 10,
  },
  ...convertToEscrowTokenPanelProps,
})

export const Normal = template()
