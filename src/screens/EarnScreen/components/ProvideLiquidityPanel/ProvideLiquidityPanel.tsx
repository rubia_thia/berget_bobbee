import { FC, ReactNode } from "react"
import { defineMessage, useIntl } from "react-intl"
import {
  Image,
  ImageSourcePropType,
  StyleProp,
  Text,
  View,
  ViewStyle,
} from "react-native"
import { WhiteOutlineButtonVariant } from "../../../../components/Button/WhiteOutlineButtonVariant"
import { Button } from "../../../../components/ButtonFramework/Button"
import { CardBoxView } from "../../../../components/CardBox/CardBox"
import { Divider } from "../../../../components/Divider"
import { useColors } from "../../../../components/Themed/color"
import { useSpacing } from "../../../../components/Themed/spacing"
import { arrayJoin } from "../../../../utils/arrayHelpers"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { HrefLink } from "../../../../utils/reactNavigationHelpers/HrefLink"
import { checkNever, isNotNull } from "../../../../utils/typeHelpers"
import { OneOrMore } from "../../../../utils/types"
import { APRBadge } from "../APRBadge"
import { FullWidthPanelActionsArea } from "../misc"

export interface PoolInfo {
  type: "thena" | "pancake"
  link: string
  apr?: BigNumber
}

export interface ProvideLiquidityPanelProps {
  style?: StyleProp<ViewStyle>
  pools: Readonly<OneOrMore<PoolInfo>>
}

export const ProvideLiquidityPanel: FC<ProvideLiquidityPanelProps> = props => {
  const spacing = useSpacing()

  return (
    <CardBoxView
      style={props.style}
      padding={{
        paddingVertical: spacing(3),
        paddingHorizontal: spacing(6),
      }}
    >
      {arrayJoin(
        ({ index }) => (
          <Divider
            key={`sep-${index}`}
            style={{ marginVertical: spacing(2.5) }}
          />
        ),
        props.pools
          .map((p, idx) => {
            switch (p.type) {
              case "pancake":
                return (
                  <PoolEntry
                    key={`pool-${idx}`}
                    image={require("./pancake.png")}
                    nameText={"UNW/BNB"}
                    poolLink={p.link}
                    apr={p.apr}
                  />
                )
              case "thena":
                return (
                  <PoolEntry
                    key={`pool-${idx}`}
                    image={require("./thena.png")}
                    nameText={"UNW/THE"}
                    poolLink={p.link}
                    apr={p.apr}
                  />
                )
              default:
                checkNever(p.type)
                return null
            }
          })
          .filter(isNotNull),
      )}
    </CardBoxView>
  )
}

const PoolEntry: FC<{
  style?: StyleProp<ViewStyle>
  image: ImageSourcePropType
  nameText: ReactNode
  apr?: BigNumber
  poolLink: string
}> = props => {
  const spacing = useSpacing()
  const colors = useColors()
  const { $t } = useIntl()

  return (
    <View
      style={[
        props.style,
        {
          flexDirection: "row",
          alignItems: "center",
        },
      ]}
    >
      <View style={{ flex: 1, flexDirection: "row", alignItems: "center" }}>
        <Image
          style={{ width: 32, height: 32 }}
          source={props.image}
          resizeMode={"contain"}
        />

        <Text
          style={{
            marginLeft: spacing(2.5),
            fontSize: 14,
            color: colors("gray-900"),
          }}
        >
          {props.nameText}
        </Text>

        {props.apr != null && (
          <APRBadge style={{ marginLeft: spacing(2.5) }} apr={props.apr} />
        )}
      </View>

      <FullWidthPanelActionsArea style={{ flex: 1 }}>
        <HrefLink href={props.poolLink}>
          {p => (
            <Button {...p} Variant={WhiteOutlineButtonVariant}>
              {$t(
                defineMessage({
                  defaultMessage: "Provide Liquidity",
                  description: "EarnScreen/RewardsToClaim/title",
                }),
              )}
            </Button>
          )}
        </HrefLink>
      </FullWidthPanelActionsArea>
    </View>
  )
}
