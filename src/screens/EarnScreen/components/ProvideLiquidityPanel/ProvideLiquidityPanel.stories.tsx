import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { ProvideLiquidityPanel } from "./ProvideLiquidityPanel"

export default {
  title: "Page/EarnScreen/ProvideLiquidityPanel",
  component: ProvideLiquidityPanel,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof ProvideLiquidityPanel>

const template = withTemplate(ProvideLiquidityPanel, {
  style: {
    margin: 10,
  },
  pools: [
    { type: "thena", link: "https://google.com", apr: BigNumber.from(0.3) },
    { type: "pancake", link: "https://twitter.com", apr: BigNumber.from(0.3) },
  ] as const,
})

export const Normal = template()
