import { StackScreenProps } from "@react-navigation/stack"
import { reaction } from "mobx"
import React, { FC, Suspense, useEffect } from "react"
import { View } from "react-native"
import { formatPercentNumber } from "../../components/PercentNumber"
import { Stack } from "../../components/Stack"
import { useResponsiveValue } from "../../components/Themed/breakpoints"
import { useSpacing } from "../../components/Themed/spacing"
import { formatTokenCount, TokenPresets } from "../../components/TokenCount"
import {
  TopLevelNavigatorParamList,
  TopLevelNavigatorScreenProps,
} from "../../navigation/navigators/TopLevelNavigatorTypes"
import { AirdropStoreProvider } from "../../stores/AirdropStore/useAirdropStore"
import { PriceSymbol } from "../../stores/AppEnvStore/PythModule.service"
import { useAppEnvStore } from "../../stores/AppEnvStore/useAppEnvStore"
import { useCurrencyStore } from "../../stores/CurrencyStore/useCurrencyStore"
import {
  TradeStoreContextBridgeSymbol,
  TradeStoreProvider,
  useCheckedPriceSymbol,
  useTradeStore,
} from "../../stores/TradeStore/useTradeStore"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import {
  math,
  mathIs,
} from "../../utils/numberHelpers/bigNumberExpressionParser"
import { FCS } from "../../utils/reactHelpers/types"
import { useContextBridge } from "../../utils/reactHelpers/useContextBridge"
import { suspenseResource } from "../../utils/SuspenseResource"
import { TokenInfoPresets } from "../../utils/TokenInfoPresets/TokenInfoPresets"
import { HideScreenOnBlur } from "../components/HideScreenOnBlur"
import { WiredChartPanel } from "./components/ChartPanel/WiredChartPanel"
import { HorizontalScrollableContainer } from "./components/HorizontalScrollableContainer"
import { MarketSelector } from "./components/MarketSelector"
import { MarketSummaryBar } from "./components/MarketSummaryBar/MarketSummaryBar"
import { MyAssetsPanel } from "./components/MyAssetsPanel/MyAssetsPanel"
import { TradePromote } from "./components/TradePromote/TradePromote"
import WiredOrderCreationPanel from "./wiredComponents/WiredOrderCreationPanel"
import { WiredOrderHistory } from "./wiredComponents/WiredOrderHistory"

const WiredMarketSelector: FCS<{ closeSelector: () => void }> = ({
  style,
  closeSelector,
}) => {
  const appEnv = useAppEnvStore()
  const approvedPriceSymbols = appEnv.onChainConfigs$.approvedPriceSymbols
  const trade = useTradeStore()
  const currency = useCurrencyStore()
  return (
    <MarketSelector
      style={style}
      tokenParis={approvedPriceSymbols.map(symbol => ({
        value: symbol,
        pair: {
          baseToken: currency.baseTokenInfo$(symbol),
          quoteToken: trade.anchorTokenInfo$,
        },
      }))}
      onSelect={(symbol: PriceSymbol) => {
        trade.selectPriceSymbol(symbol)
        closeSelector()
      }}
    />
  )
}

const WiredMarketSummaryBar: FCS<
  Pick<TopLevelNavigatorScreenProps<"Trade">, "navigation">
> = ({ style, navigation }) => {
  const trade = useTradeStore()
  const ContextBridge = useContextBridge(TradeStoreContextBridgeSymbol)
  useEffect(
    () =>
      reaction(
        () => ({
          market: trade.baseTokenInfo$,
          price: trade.chart.currentPrice$,
          changeIn24Hours:
            trade.chart.priceChangeIn24Hours$.priceDeltaPercentage,
        }),
        data => {
          if (data == null) {
            return
          }
          navigation.setOptions({
            title: `${data.market.displayName} $${formatTokenCount(
              TokenPresets.USD,
              data.price,
              { padDecimals: true },
            )} ${
              mathIs`${data.changeIn24Hours} >= ${0}` ? "▲" : "▼"
            }${formatPercentNumber(data.changeIn24Hours)} - Uniwhale`,
          })
        },
        {
          fireImmediately: true,
        },
      ),
    [navigation, trade],
  )
  return (
    <MarketSummaryBar
      style={style}
      tokenSelector={({ close }) => (
        <ContextBridge>
          <WiredMarketSelector closeSelector={close} />
        </ContextBridge>
      )}
      tokenPair={{
        baseToken: trade.baseTokenInfo$,
        quoteToken: trade.anchorTokenInfo$,
      }}
      currentPrice={suspenseResource(() => trade.chart.currentPrice$)}
      currentPriceChangeDirection={suspenseResource(
        () => trade.chart.currentPriceChangeDirection.value$,
      )}
      priceChangeIn24Hours={suspenseResource(
        () => trade.chart.priceChangeIn24Hours$,
      )}
      priceHighestIn24Hours={suspenseResource(
        () => trade.chart.priceHighestIn24Hours$,
      )}
      priceLowestIn24Hours={suspenseResource(
        () => trade.chart.priceLowestIn24Hours$,
      )}
      openLong={suspenseResource(
        () => trade.currentSelectedMarketPriceIdInfo$.totalLong,
      )}
      maxLong={suspenseResource(
        () => trade.currentSelectedMarketPriceIdInfo$.maxLong,
      )}
      openShort={suspenseResource(
        () => trade.currentSelectedMarketPriceIdInfo$.totalShort,
      )}
      maxShort={suspenseResource(
        () => trade.currentSelectedMarketPriceIdInfo$.maxShort,
      )}
      fundingLongHourlyRate={suspenseResource(
        () => trade.longFundingFeeHourlyRate$,
      )}
      fundingShortHourlyRate={suspenseResource(
        () => trade.shortFundingFeeHourlyRate$,
      )}
      rolloverRate={suspenseResource(() => trade.rolloverFeeHourlyRate$)}
      // openInterest={{
      //   long: suspenseResource(
      //     () => trade.currentPriceIdInfo$.totalLong,
      //   ),
      //   short: suspenseResource(
      //     () => trade.currentPriceIdInfo$.totalShort,
      //   ),
      //   total: suspenseResource(() => {
      //     const { totalLong, totalShort } =
      //       trade.currentPriceIdInfo$
      //     return math`${totalLong} + ${totalShort}`
      //   }),
      // }}
    />
  )
}

const WiredMyAssetsPanel: FCS = ({ style }) => {
  const app = useAppEnvStore()
  const currency = useCurrencyStore()
  const trade = useTradeStore()
  return (
    <MyAssetsPanel
      style={style}
      totalBalanceInUSD={suspenseResource(() => {
        const stableCoins = trade.tradeInfo.allSupportedStableToken$
          .map(token => currency.balance$(token))
          .reduce((acc, cur) => math`${acc} + ${cur}`, BigNumber.from(0))
        const native = currency.ethBalance.value$
        const price = app.pyth.priceFor(app.pyth.priceIdFor("BNB_USD")).value$
          .price
        return math`${native} x ${price} + ${stableCoins}`
      })}
      marginInUSD={suspenseResource(
        () => trade.orderHistory.totalOpenPositionMargin$,
      )}
      unrealizedPNL={{
        trendPercentage: suspenseResource(
          () => trade.orderHistory.unrealizedPnlPercentage$,
        ),
        valueInUSD: suspenseResource(() => trade.orderHistory.unrealizedPnl$),
      }}
      balances={[
        {
          token: currency.baseTokenInfo$("BNB_USD"),
          balance: suspenseResource(() => currency.ethBalance.value$),
        },
        ...trade.tradeInfo.allSupportedStableToken$.map(token => ({
          token: currency.tokenInfo$(token),
          balance: suspenseResource(() => currency.balance$(token)),
        })),
      ]}
    />
  )
}

const WiredTradePromote: FCS<{
  navigation: StackScreenProps<
    TopLevelNavigatorParamList,
    "Trade"
  >["navigation"]
}> = ({ style, navigation }) => {
  const { tradePromote } = useTradeStore()

  return (
    <TradePromote
      style={style}
      detailNavLinkItem={{
        type: "href",
        href: "https://medium.com/uniwhale/trade-and-earn-your-scorecard-e1b4687d036",
      }}
      emissionTokenInfo={{
        ...TokenInfoPresets.esUNW,
        precision: 0,
      }}
      emissionTokenAmountPerDay={suspenseResource(
        () => tradePromote.emissionPerDay$,
      )}
      totalTradingVolTokenAmount={suspenseResource(
        () => tradePromote.totalTradingVolume$,
      )}
      myTradingVolumeInUSD={suspenseResource(() => tradePromote.myTradeVolume$)}
      myCurrentEmissionPercentage={suspenseResource(
        () => tradePromote.myEmissionPercentage$,
      )}
      myCurrentEmissionAmount={suspenseResource(
        () => tradePromote.myEstimatedEmission$,
      )}
      myCurrentEmissionTokenInfo={{
        ...TokenInfoPresets.esUNW,
        precision: 0,
      }}
      blocksPerRun={suspenseResource(() => tradePromote.blocksPerRun$)}
      remainingBlock={suspenseResource(() => tradePromote.remainingBlock$)}
    />
  )
}

const WiredTradeScreenContent: FC<{
  navigation: StackScreenProps<
    TopLevelNavigatorParamList,
    "Trade"
  >["navigation"]
}> = props => {
  const spacing = useSpacing()

  const gap = spacing(2.5)

  const layoutMode =
    useResponsiveValue({
      _: "1col" as const,
      md: "2col" as const,
    }) ?? "1col"

  const horizontalMargin = layoutMode === "1col" ? gap : undefined

  const orderCreationPanel = (
    <WiredOrderCreationPanel
      gap={spacing(5)}
      style={{ marginHorizontal: horizontalMargin }}
    />
  )

  const myAssetsPanel = (
    <WiredMyAssetsPanel style={{ marginHorizontal: horizontalMargin }} />
  )

  return (
    <View
      style={[
        layoutMode === "2col"
          ? {
              marginVertical: spacing(5),
              marginHorizontal: "auto",
              paddingHorizontal: spacing(5),
            }
          : { marginVertical: gap },
        { width: "100%", flexDirection: "row" },
      ]}
    >
      <Stack style={{ flex: 1 }} space={gap}>
        <WiredTradePromote
          style={{ marginHorizontal: horizontalMargin }}
          navigation={props.navigation}
        />
        {/*<HorizontalScrollableContainer*/}
        {/*  horizontalMargin={horizontalMargin}*/}
        {/*  scrollable={layoutMode === "1col"}*/}
        {/*>*/}
        {/*  <FavMarketPriceBar tokenPairs={pairs} />*/}
        {/*</HorizontalScrollableContainer>*/}

        <HorizontalScrollableContainer
          contentContainerStyle={{ minWidth: "100%" }}
          horizontalMargin={horizontalMargin}
          scrollable={layoutMode === "1col"}
        >
          <WiredMarketSummaryBar
            style={{ minWidth: "100%" }}
            navigation={props.navigation}
          />
        </HorizontalScrollableContainer>

        <WiredChartPanel style={{ marginHorizontal: horizontalMargin }} />

        {layoutMode === "1col" && orderCreationPanel}

        {layoutMode === "1col" && myAssetsPanel}

        <WiredOrderHistory
          horizontalMargin={horizontalMargin}
          layoutMode={layoutMode}
        />
      </Stack>

      {layoutMode === "2col" && (
        <Stack style={{ width: 360, marginLeft: gap }} space={gap}>
          {orderCreationPanel}
          {myAssetsPanel}
        </Stack>
      )}
    </View>
  )
}

export const TradeScreen: FC<TopLevelNavigatorScreenProps<"Trade">> = props => {
  const appEnv = useAppEnvStore()
  return (
    <HideScreenOnBlur>
      <TradeStoreProvider
        priceSymbol={useCheckedPriceSymbol(
          props.route.params?.symbol ?? appEnv.previousMarket,
        )}
      >
        <AirdropStoreProvider>
          <Suspense>
            <WiredTradeScreenContent navigation={props.navigation} />
          </Suspense>
        </AirdropStoreProvider>
      </TradeStoreProvider>
    </HideScreenOnBlur>
  )
}
