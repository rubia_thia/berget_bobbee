import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { noop } from "../../../utils/fnHelpers"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../utils/TokenInfoPresets/TokenInfoPresets"
import { MarginManipulation, OrderDirection } from "../types"
import { EditMarginModalContent } from "./EditMarginModalContent"
import { EditMarginModalFormErrorType } from "./EditMarginModalContent.types"

export default {
  title: "Page/TradeScreen/EditMarginModalContent",
  component: EditMarginModalContent,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof EditMarginModalContent>

const tpl = withTemplate(EditMarginModalContent, {
  style: { margin: 10 },
  onDismiss: noop,
  onSubmit: noop,

  baseToken: TokenInfoPresets.MockBTC,
  quoteToken: TokenInfoPresets.MockUSDC,
  orderDirection: OrderDirection.Long,
  marginManipulation: MarginManipulation.Decrease,
  onChangeMarginManipulation: noop,

  positionSize: BigNumber.from(100),
  marginSize: [BigNumber.from(8.818342152), BigNumber.from(5)],
  leverage: [BigNumber.from(11.34), BigNumber.from(20)],
  markPrice: BigNumber.from(1641.28),
  liquidationPrice: [BigNumber.from(20867.12), BigNumber.from(10867.12)],
  executionFee: BigNumber.from(0.0075),

  maxAvailableTokenToOperate: BigNumber.from(10000.00001),
  marginQuoteTokenAmount: BigNumber.from(5),
  onMarginQuoteTokenAmountChange: noop,
  onPressMaxMarginQuoteTokenAmount: noop,
})

export const Normal = tpl()

export const Error = tpl(p => {
  p.error = {
    type: EditMarginModalFormErrorType.InsufficientQuoteTokenBalance,
  }
})
