import { FC, ReactNode } from "react"
import { StyleProp, Text, ViewStyle } from "react-native"
import { useColors } from "../../../components/Themed/color"
import { useSpacing } from "../../../components/Themed/spacing"

export interface BadgeProps {
  style?: StyleProp<ViewStyle>

  textColor: string

  backgroundColor: string

  text: ReactNode
}

export const Badge: FC<BadgeProps> = props => {
  const spacing = useSpacing()

  return (
    <Text
      style={[
        props.style,
        {
          paddingHorizontal: spacing(2),
          paddingVertical: spacing(0.5),
          fontSize: 14,
          lineHeight: 20,
          fontWeight: "500",
          color: props.textColor,
          borderRadius: 9999,
          backgroundColor: props.backgroundColor,
        },
      ]}
    >
      {props.text}
    </Text>
  )
}

export interface BadgeVariantProps
  extends Omit<BadgeProps, "textColor" | "backgroundColor"> {
  textColor?: string

  backgroundColor?: string

  text: ReactNode
}

export const RedBadge: FC<BadgeVariantProps> = props => {
  const colors = useColors()

  return (
    <Badge
      style={props.style}
      textColor={props.textColor ?? colors("red-500")}
      backgroundColor={props.backgroundColor ?? colors("red-100")}
      text={props.text}
    />
  )
}

export const IndigoBadge: FC<BadgeVariantProps> = props => {
  const colors = useColors()

  return (
    <Badge
      style={props.style}
      textColor={props.textColor ?? colors("blue-700")}
      backgroundColor={props.backgroundColor ?? colors("indigo-100")}
      text={props.text}
    />
  )
}

export const GreenBadge: FC<BadgeVariantProps> = props => {
  const colors = useColors()

  return (
    <Badge
      style={props.style}
      textColor={props.textColor ?? colors("green-700")}
      backgroundColor={props.backgroundColor ?? colors("green-300")}
      text={props.text}
    />
  )
}

export const GrayBadge: FC<BadgeVariantProps> = props => {
  const colors = useColors()

  return (
    <Badge
      style={props.style}
      textColor={props.textColor ?? colors("gray-400")}
      backgroundColor={props.backgroundColor ?? colors("gray-300")}
      text={props.text}
    />
  )
}
