export enum ClosePositionModalFormErrorType {
  OrderNotFound,
  EnterPrice,
  EnterPosition,
  InsufficientPositionSize,
}

export type ClosePositionModalFormError = {
  type: ClosePositionModalFormErrorType
}
