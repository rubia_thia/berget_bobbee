import { FC, useCallback, useMemo, useRef } from "react"
import { defineMessage, IntlShape, useIntl } from "react-intl"
import { StyleProp, Text, TextStyle, View, ViewStyle } from "react-native"
import { BlueButtonVariant } from "../../../../components/Button/BlueButtonVariant"
import {
  ColorfulButtonVariant,
  ColorfulButtonVariantColorSet,
} from "../../../../components/Button/ColorfulButtonVariant"
import { PlainIconButtonVariant } from "../../../../components/Button/PlainIconButtonVariant"
import { RedButtonVariant } from "../../../../components/Button/RedButtonVariant"
import { Button } from "../../../../components/ButtonFramework/Button"
import { SmartLoadableButton } from "../../../../components/ButtonFramework/LoadableButton"
import { CardBoxView } from "../../../../components/CardBox/CardBox"
import { CardInset } from "../../../../components/CardBox/CardInset"
import {
  CardBoxModalContent,
  TitleBar,
} from "../../../../components/CardBoxModalContent/CardBoxModalContent"
import {
  DropdownMenu,
  DropdownMenuItem,
} from "../../../../components/DropdownMenu"
import { InfoList } from "../../../../components/InfoList/InfoList"
import { InfoListItem } from "../../../../components/InfoList/InfoListItem"
import {
  InfoListItemDetail,
  InfoListItemDetailText,
} from "../../../../components/InfoList/InfoListItemDetail"
import { InfoListItemTitle } from "../../../../components/InfoList/InfoListItemTitle"
import { PercentNumber } from "../../../../components/PercentNumber"
import {
  PopoverContent,
  PopoverRoot,
  PopoverTriggerRaw,
  PopoverTriggerRawRenderProps,
} from "../../../../components/Popover/Popover"
import { Stack } from "../../../../components/Stack"
import { useColors } from "../../../../components/Themed/color"
import { useSpacing } from "../../../../components/Themed/spacing"
import { oneOf } from "../../../../utils/arrayHelpers"
import { noop } from "../../../../utils/fnHelpers"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { FCC } from "../../../../utils/reactHelpers/types"
import { useIsFocusing } from "../../../../utils/reactHelpers/useIsFocusing"
import { useLatestValueRef } from "../../../../utils/reactHelpers/useLatestValueRef"
import {
  readResource,
  safeReadResource,
  SuspenseResource,
} from "../../../../utils/SuspenseResource"
import { TokenInfo } from "../../../../utils/TokenInfo"
import { checkNever, checkObjEmpty } from "../../../../utils/typeHelpers"
import { OrderDirection } from "../../types"
import { NumberChanging, NumberChangingText } from "../NumberChangingText"
import {
  LeftAreaLabelText,
  OneLineTokenInput,
  OneLineTokenInputRefValue,
} from "../OneLineTokenInput"
import { OrderDirectionBadge } from "../OrderDirectionBadge"
import { TokenNameStack } from "../TokenNameStack"
import { commonErrorMessage$t } from "../_/commonIntlMessages"
import {
  ClosePositionModalFormError,
  ClosePositionModalFormErrorType,
} from "./ClosePositionModalContent.types"
import SelectorArrowDownIcon from "./_/selectorArrowDown.svg"

export type ClosePositionModalFormPrice =
  | {
      type: "marketPrice"
      price: SuspenseResource<BigNumber>
    }
  | {
      type: "limitPrice"
      price: SuspenseResource<BigNumber | null>
    }

export type ClosePositionModalFormPricingPatch =
  | {
      type: "marketPrice"
    }
  | {
      type: "limitPrice"
      price: null | BigNumber
    }

export type ClosePositionModalFormPositionSize = null | {
  percentage: BigNumber
  amount: BigNumber
}
export type ClosePositionModalFormPositionSizePatch =
  | null
  | { type: "amount"; amount: BigNumber }
  | { type: "percentage"; percentage: BigNumber }

export interface ClosePositionModalContentProps {
  style?: StyleProp<ViewStyle>
  onDismiss: () => void
  onSubmit: () => void | Promise<void>

  error: SuspenseResource<undefined | ClosePositionModalFormError>

  baseToken: TokenInfo
  quoteToken: TokenInfo
  leverage: BigNumber
  orderDirection: OrderDirection
  positionSizeChanging: NumberChanging

  price?: ClosePositionModalFormPrice
  onPriceChange?: (pricing: ClosePositionModalFormPricingPatch) => void

  positionSize: ClosePositionModalFormPositionSize
  onPositionSizeChange: (
    newValue: ClosePositionModalFormPositionSizePatch,
  ) => void
}

export const ClosePositionModalContent: FC<
  ClosePositionModalContentProps
> = props => {
  const spacing = useSpacing()
  const colors = useColors()

  const intl = useIntl()
  const { $t } = intl

  const error = safeReadResource(props.error)

  return (
    <CardBoxModalContent
      style={props.style}
      padding={spacing(4)}
      onClose={props.onDismiss}
    >
      <Stack space={spacing(5)}>
        <TitleBar>
          {$t(
            defineMessage({
              defaultMessage: "Close Position",
              description: "TradeScreen/Close position modal/modal title",
            }),
          )}
        </TitleBar>

        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <Text
            style={{
              marginRight: spacing(2.5),
              fontSize: 18,
              fontWeight: "700",
              lineHeight: 28,
              color: colors("gray-900"),
            }}
          >
            <TokenNameStack
              baseToken={props.baseToken}
              quoteToken={props.quoteToken}
            />
          </Text>

          <Text
            style={{
              marginRight: spacing(2.5),
              fontSize: 18,
              lineHeight: 28,
              color: colors("gray-500"),
            }}
          >
            {Number(BigNumber.toNumber(props.leverage).toFixed(2))}x
          </Text>

          <OrderDirectionBadge
            style={{ marginRight: spacing(2.5) }}
            direction={props.orderDirection}
          />
        </View>

        <Stack space={spacing(2.5)}>
          {props.price != null && props.onPriceChange != null && (
            <OneLineTokenInput
              token={props.quoteToken}
              leftArea={
                <PricingTypeSelector
                  readonly={true}
                  price={props.price}
                  onPriceChange={props.onPriceChange}
                />
              }
              count={props.price.price}
              onCountChange={
                props.price.type === "marketPrice"
                  ? undefined
                  : n => props.onPriceChange!({ type: "limitPrice", price: n })
              }
            />
          )}

          <PositionInput
            quotaToken={props.quoteToken}
            value={props.positionSize}
            onValueChange={props.onPositionSizeChange}
          />

          <CardInset>
            <InfoList>
              <InfoListItem>
                <InfoListItemTitle>
                  {$t(
                    defineMessage({
                      defaultMessage: "Position",
                      description:
                        "TradeScreen/Close position modal/info list label",
                    }),
                  )}
                </InfoListItemTitle>

                <InfoListItemDetail>
                  <InfoListItemDetailText>
                    <NumberChangingText
                      token={props.quoteToken}
                      changing={props.positionSizeChanging}
                    />
                  </InfoListItemDetailText>
                </InfoListItemDetail>
              </InfoListItem>
            </InfoList>
          </CardInset>
        </Stack>

        <SmartLoadableButton
          disabled={error != null}
          Variant={
            error != null &&
            !oneOf(
              ClosePositionModalFormErrorType.EnterPosition,
              ClosePositionModalFormErrorType.EnterPrice,
              ClosePositionModalFormErrorType.OrderNotFound,
            )(error.type)
              ? RedButtonVariant
              : BlueButtonVariant
          }
          onPress={props.onSubmit}
        >
          <Button.Text>
            {renderButtonErrorMessageText(intl, error) ??
              $t(
                defineMessage({
                  defaultMessage: "Close Position",
                  description:
                    'TradeScreen/Close position modal/"Close Position" button',
                }),
              )}
          </Button.Text>
        </SmartLoadableButton>
      </Stack>
    </CardBoxModalContent>
  )
}

const renderButtonErrorMessageText = (
  intl: IntlShape,
  error?: ClosePositionModalFormError,
): null | JSX.Element => {
  if (error == null) return null

  if (error.type === ClosePositionModalFormErrorType.OrderNotFound) {
    return <>{intl.$t(commonErrorMessage$t.positionNotFound)}</>
  }

  if (error.type === ClosePositionModalFormErrorType.EnterPrice) {
    return <>{intl.$t(commonErrorMessage$t.enterPrice)}</>
  }

  if (error.type === ClosePositionModalFormErrorType.EnterPosition) {
    return <>{intl.$t(commonErrorMessage$t.enterPosition)}</>
  }

  if (error.type === ClosePositionModalFormErrorType.InsufficientPositionSize) {
    return <>{intl.$t(commonErrorMessage$t.insufficientPositionSize)}</>
  }

  checkNever(error.type)
  return null
}

const PricingTypeSelector: FC<{
  readonly?: boolean
  price: ClosePositionModalFormPrice
  onPriceChange: (pricing: ClosePositionModalFormPricingPatch) => void
}> = props => {
  const { $t } = useIntl()

  return (
    <PopoverRoot triggerMethod={"press"} contentPlacement={"bottom"}>
      <PopoverTriggerRaw>
        {p => (
          <PriceTypeSelectorTrigger
            readonly={props.readonly}
            price={props.price}
            popoverTriggerRawRenderProps={p}
          />
        )}
      </PopoverTriggerRaw>

      <PopoverContent>
        <DropdownMenu>
          <DropdownMenuItem
            disabled={props.price.type === "limitPrice"}
            onPress={() =>
              props.onPriceChange({
                type: "limitPrice",
                price: readResource(props.price.price),
              })
            }
          >
            {$t(pricingType$t.limitPrice)}
          </DropdownMenuItem>
          <DropdownMenuItem
            disabled={props.price.type === "marketPrice"}
            onPress={() =>
              props.onPriceChange({
                type: "marketPrice",
              })
            }
          >
            {$t(pricingType$t.marketPrice)}
          </DropdownMenuItem>
        </DropdownMenu>
      </PopoverContent>
    </PopoverRoot>
  )
}

const PriceTypeSelectorTrigger: FC<{
  readonly?: boolean
  price: ClosePositionModalFormPrice
  popoverTriggerRawRenderProps: PopoverTriggerRawRenderProps
}> = props => {
  const {
    ref,
    isOpen,
    onPress,
    onHoverOut,
    onHoverIn,
    onLayout,
    ...popoverTriggerRawRenderPropsRest
  } = props.popoverTriggerRawRenderProps ?? {}
  checkObjEmpty(popoverTriggerRawRenderPropsRest)

  const { $t } = useIntl()
  const colors = useColors()

  const textStyle: TextStyle = { color: colors("gray-900") }

  if (props.readonly) {
    return (
      <LeftAreaLabelText style={textStyle}>
        {props.price.type === "marketPrice"
          ? $t(pricingType$t.marketPrice)
          : $t(pricingType$t.limitPrice)}
      </LeftAreaLabelText>
    )
  }

  return (
    <Button
      Variant={p => (
        <PlainIconButtonVariant
          {...p}
          ref={ref}
          textStyle={textStyle}
          iconRight={<SelectorArrowDownIcon width={16} height={16} />}
          onHoverIn={onHoverIn}
          onHoverOut={onHoverOut}
          onLayout={onLayout}
        />
      )}
      onPress={onPress}
    >
      <LeftAreaLabelText>
        {props.price.type === "marketPrice"
          ? $t(pricingType$t.marketPrice)
          : $t(pricingType$t.limitPrice)}
      </LeftAreaLabelText>
    </Button>
  )
}

namespace pricingType$t {
  export const marketPrice = defineMessage({
    defaultMessage: "Price",
    description: "TradeScreen/Close position modal/form label: market price",
  })

  export const limitPrice = defineMessage({
    defaultMessage: "Limit Price",
    description: "TradeScreen/Close position modal/form label: limit price",
  })
}

const PositionInput: FC<{
  quotaToken: TokenInfo
  value: ClosePositionModalFormPositionSize
  onValueChange: (newValue: ClosePositionModalFormPositionSizePatch) => void
}> = props => {
  const { $t } = useIntl()

  const [isFocusing, _isFocusingEventListeners] = useIsFocusing()
  const isFocusingRef = useLatestValueRef(isFocusing)
  const isFocusingEventListeners = {
    ..._isFocusingEventListeners,
    onBlur: () => {
      setTimeout(() => {
        if (!isFocusingRef.current) {
          _isFocusingEventListeners.onBlur()
        }
      })
    },
  }

  const inputRef = useRef<OneLineTokenInputRefValue>(null)

  const propsOnValueChange = props.onValueChange
  const onSelectedPercentageChange = useCallback(
    (newValue: ClosePositionModalFormPositionSizePatch) => {
      propsOnValueChange(newValue)
      setTimeout(() => {
        console.log("input ref", inputRef.current)
        inputRef.current?.focus()
      })
    },
    [propsOnValueChange],
  )

  return (
    <>
      <OneLineTokenInput
        ref={inputRef}
        token={props.quotaToken}
        leftArea={
          <LeftAreaLabelText>
            {$t(
              defineMessage({
                defaultMessage: "Position",
                description: "TradeScreen/Close position modal/form label",
              }),
            )}
          </LeftAreaLabelText>
        }
        count={props.value?.amount ?? null}
        onCountChange={count =>
          props.onValueChange(
            count == null ? null : { type: "amount", amount: count },
          )
        }
        {...isFocusingEventListeners}
      />
      <View className="self-end">
        <PositionPercentageSelector
          value={props.value}
          onChange={onSelectedPercentageChange}
        />
      </View>
    </>
  )
}

const PositionPercentageSelector: FC<{
  value: ClosePositionModalFormPositionSize
  onChange: (newValue: ClosePositionModalFormPositionSizePatch) => void
}> = props => {
  const spacing = useSpacing()

  const availableValues = useMemo(
    () => [
      BigNumber.from(0.1),
      BigNumber.from(0.25),
      BigNumber.from(0.5),
      BigNumber.from(0.75),
      BigNumber.from(1),
    ],
    [],
  )

  return (
    <CardBoxView style={{ flexDirection: "row" }} padding={spacing(1)}>
      <Stack horizontal={true} space={1}>
        {availableValues.map((value, idx) => (
          <PositionPercentageSelectorItem
            key={idx}
            active={
              props.value == null
                ? false
                : BigNumber.isEq(value, props.value.percentage)
            }
            onPress={() =>
              props.onChange({ type: "percentage", percentage: value })
            }
          >
            <PercentNumber number={value} />
          </PositionPercentageSelectorItem>
        ))}
      </Stack>
    </CardBoxView>
  )
}
const PositionPercentageSelectorItem: FCC<{
  active?: boolean
  onPress: () => void
}> = props => {
  const spacing = useSpacing()
  const colors = useColors()

  const bgColor: ColorfulButtonVariantColorSet = useMemo(
    () =>
      props.active
        ? {
            normal: colors("blue-200"),
            hovering: colors("blue-300"),
            pressing: colors("blue-400"),
            disabled: colors("blue-200"),
          }
        : {
            normal: colors("blue-100"),
            hovering: colors("blue-200"),
            pressing: colors("blue-300"),
            disabled: colors("blue-100"),
          },
    [colors, props.active],
  )

  const textColor: ColorfulButtonVariantColorSet = useMemo(
    () => ({
      normal: colors("blue-600"),
      hovering: colors("blue-600"),
      pressing: colors("blue-600"),
      disabled: colors("blue-600"),
    }),
    [colors],
  )

  return (
    <Button
      Variant={p => (
        <ColorfulButtonVariant
          {...p}
          style={[p.style, { width: 48 }]}
          textStyle={{
            fontSize: 12,
            lineHeight: 16,
          }}
          padding={{
            paddingHorizontal: spacing(2),
            paddingVertical: spacing(1),
          }}
          borderRadius={0}
          textColor={textColor}
          bgColor={bgColor}
        />
      )}
      onPress={props.active ? noop : props.onPress}
    >
      <Button.Text>{props.children}</Button.Text>
    </Button>
  )
}
