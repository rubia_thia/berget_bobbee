import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { OrderDirection } from "../../types"
import { ClosePositionModalContent } from "./ClosePositionModalContent"

export default {
  title: "Page/TradeScreen/ClosePositionModalContent",
  component: ClosePositionModalContent,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof ClosePositionModalContent>

const tpl = withTemplate(ClosePositionModalContent, {
  style: { margin: 10 },

  baseToken: TokenInfoPresets.MockBTC,
  quoteToken: TokenInfoPresets.MockUSDC,
  leverage: BigNumber.from(10),
  orderDirection: OrderDirection.Short,

  price: {
    type: "limitPrice",
    price: BigNumber.from(20867.12),
  },

  positionSize: {
    percentage: BigNumber.from(0.51),
    amount: BigNumber.from(100.56),
  },

  positionSizeChanging: [BigNumber.from(812.56), BigNumber.from(12.45)],
  error: undefined,
})

export const Normal = tpl()

export const MarketPrice = tpl(p => {
  p.price = {
    type: "marketPrice",
    price: BigNumber.from(20867.12),
  }
})
