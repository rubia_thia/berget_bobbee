import { FC, ReactNode } from "react"
import { StyleProp, Text, TextInputProps, TextStyle, View } from "react-native"
import { NumberInput } from "../../../components/NumberInput/NumberInput"
import { NumberInputTextStyle } from "../../../components/NumberInput/types"
import { useSpacing } from "../../../components/Themed/spacing"
import { InputStyleGuide } from "../../../components/TokenInput/InputStyleGuide"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { useIsFocusing } from "../../../utils/reactHelpers/useIsFocusing"
import {
  PaddingStyle,
  useNormalizePaddingStyle,
} from "../../../utils/styleHelpers/PaddingStyle"
import {
  safeReadResource,
  SuspenseResource,
} from "../../../utils/SuspenseResource"
import { checkObjEmpty } from "../../../utils/typeHelpers"

export interface FormattedNumberInputProps {
  style?: TextInputProps["style"]
  textStyle?: StyleProp<NumberInputTextStyle>
  padding?: number | PaddingStyle
  error?: boolean
  textAlign?: TextInputProps["textAlign"]
  placeholder?: string

  /**
   * @default 2
   */
  precision?: number
  /**
   * @default false
   */
  multiline?: boolean

  valueFormattedText?: SuspenseResource<ReactNode>
  value: SuspenseResource<null | BigNumber>
  onValueChange?: (newValue: null | BigNumber) => void
}

export const FormattedNumberInput: FC<FormattedNumberInputProps> = props => {
  const spacing = useSpacing()

  const [isFocusing, isFocusingEventListeners] = useIsFocusing()

  const paddingStyle = useNormalizePaddingStyle(props.padding)

  const { multiline = false, precision = 2 } = props

  return (
    <InputStyleGuide
      hasValue={safeReadResource(props.value) != null}
      readonly={props.onValueChange == null}
      error={!!props.error}
    >
      {({
        textColor,
        borderColor,
        backgroundColor,
        onFocus,
        onBlur,
        ...rest
      }) => {
        checkObjEmpty(rest)
        const { onValueChange } = props

        const numberTextStyle: TextStyle = {
          paddingLeft: spacing(3),
          paddingRight: spacing(3),
          paddingTop: spacing(2),
          paddingBottom: spacing(2),
          fontSize: 16,
          lineHeight: 24,
          textAlign: props.textAlign ?? "center",
          color: textColor,
          ...paddingStyle,
        }

        return (
          <View
            style={[
              props.style,
              {
                overflow: "hidden",
                borderRadius: 6,
                borderWidth: 1,
                borderColor,
                backgroundColor,
              },
            ]}
          >
            {onValueChange != null && (
              <NumberInput
                style={[
                  numberTextStyle,
                  props.textStyle,
                  { outline: "none" } as any,
                  { opacity: isFocusing ? 1 : 0 },
                  {
                    position: "absolute",
                    top: 0,
                    right: 0,
                    bottom: 0,
                    left: 0,
                  },
                ]}
                {...isFocusingEventListeners}
                onFocus={() => {
                  onFocus()
                  isFocusingEventListeners.onFocus()
                }}
                onBlur={() => {
                  onBlur()
                  isFocusingEventListeners.onBlur()
                }}
                precision={precision}
                multiline={props.multiline}
                placeholder={props.placeholder}
                value={safeReadResource(props.value) ?? BigNumber.ZERO}
                onChange={onValueChange}
              />
            )}
            <Text
              numberOfLines={multiline ? undefined : 1}
              style={[
                numberTextStyle,
                props.textStyle,
                { opacity: isFocusing ? 0 : 1 },
              ]}
            >
              {safeReadResource(props.valueFormattedText) ??
                props.placeholder ?? <>&nbsp;</>}
            </Text>
          </View>
        )
      }}
    </InputStyleGuide>
  )
}
