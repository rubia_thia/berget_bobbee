import { FC } from "react"
import { defineMessage, IntlShape, useIntl } from "react-intl"
import { StyleProp, Text, ViewStyle } from "react-native"
import { BlueButtonVariant } from "../../../components/Button/BlueButtonVariant"
import { RedButtonVariant } from "../../../components/Button/RedButtonVariant"
import { Button } from "../../../components/ButtonFramework/Button"
import { SmartLoadableButton } from "../../../components/ButtonFramework/LoadableButton"
import { CardInset } from "../../../components/CardBox/CardInset"
import {
  CardBoxModalContent,
  TitleBar,
} from "../../../components/CardBoxModalContent/CardBoxModalContent"
import { InfoList } from "../../../components/InfoList/InfoList"
import { InfoListItem } from "../../../components/InfoList/InfoListItem"
import {
  InfoListItemDetail,
  InfoListItemDetailText,
} from "../../../components/InfoList/InfoListItemDetail"
import { InfoListItemTitle } from "../../../components/InfoList/InfoListItemTitle"
import { Segment, SegmentControl } from "../../../components/SegmentControl"
import { Stack } from "../../../components/Stack"
import { TokenCountAsCurrency } from "../../../components/TextTokenCount"
import { useSpacing } from "../../../components/Themed/spacing"
import { BalanceTopArea } from "../../../components/TokenInput/BalanceTopArea"
import { TokenInput } from "../../../components/TokenInput/TokenInput"
import { TokenName } from "../../../components/TokenName"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { SuspenseResource } from "../../../utils/SuspenseResource"
import { TokenInfo } from "../../../utils/TokenInfo"
import { checkNever } from "../../../utils/typeHelpers"
import { MarginManipulation, OrderDirection } from "../types"
import {
  EditMarginModalFormError,
  EditMarginModalFormErrorType,
} from "./EditMarginModalContent.types"
import { NumberChanging, NumberChangingText } from "./NumberChangingText"
import {
  commonErrorMessage$t,
  marginManipulation$t,
} from "./_/commonIntlMessages"

export interface EditMarginModalContentProps extends InsetInfoListCoreData {
  style?: StyleProp<ViewStyle>
  onDismiss: () => void
  onSubmit: () => void | Promise<void>

  error?: EditMarginModalFormError

  baseToken: TokenInfo
  quoteToken: TokenInfo
  orderDirection: OrderDirection
  marginManipulation: MarginManipulation
  onChangeMarginManipulation: (newDirection: MarginManipulation) => void

  maxAvailableTokenToOperate: SuspenseResource<BigNumber>
  marginQuoteTokenAmount: SuspenseResource<null | BigNumber>
  onMarginQuoteTokenAmountChange: (newValue: null | BigNumber) => void
  onPressMaxMarginQuoteTokenAmount: SuspenseResource<(() => void) | undefined>
}

export const EditMarginModalContent: FC<
  EditMarginModalContentProps
> = props => {
  const spacing = useSpacing()

  const intl = useIntl()
  const { $t } = intl

  return (
    <CardBoxModalContent
      style={props.style}
      padding={spacing(4)}
      onClose={props.onDismiss}
    >
      <Stack space={spacing(5)}>
        <TitleBar>
          <Text>
            {$t(
              defineMessage({
                defaultMessage:
                  "Edit {orderDirection, select, long {Long} other {Short}} {baseToken}",
                description: "TradeScreen/Edit collateral modal/modal title",
              }),
              {
                orderDirection: props.orderDirection,
                baseToken: <TokenName token={props.baseToken} />,
              },
            )}
          </Text>
        </TitleBar>

        <SegmentControl>
          <Segment
            active={props.marginManipulation === MarginManipulation.Increase}
            onPress={() =>
              props.onChangeMarginManipulation(MarginManipulation.Increase)
            }
          >
            {$t(marginManipulation$t.increase)}
          </Segment>
          <Segment
            active={props.marginManipulation === MarginManipulation.Decrease}
            onPress={() =>
              props.onChangeMarginManipulation(MarginManipulation.Decrease)
            }
          >
            {$t(marginManipulation$t.decrease)}
          </Segment>
        </SegmentControl>

        <Stack space={spacing(2.5)}>
          <TokenInput
            error={
              props.error?.type ===
              EditMarginModalFormErrorType.InsufficientQuoteTokenBalance
            }
            topArea={
              <BalanceTopArea
                titleText={marginManipulation$t.fromMarginManipulation(
                  intl,
                  props.marginManipulation,
                )}
                token={props.quoteToken}
                balance={
                  props.marginManipulation === MarginManipulation.Increase
                    ? props.maxAvailableTokenToOperate
                    : undefined
                }
              />
            }
            availableTokens={[]}
            token={props.quoteToken}
            value={props.marginQuoteTokenAmount}
            onValueChange={props.onMarginQuoteTokenAmountChange}
            onPressMax={props.onPressMaxMarginQuoteTokenAmount}
          />

          <CardInset>
            <InsetInfoList
              quoteToken={props.quoteToken}
              positionSize={props.positionSize}
              marginSize={props.marginSize}
              leverage={props.leverage}
              markPrice={props.markPrice}
              liquidationPrice={props.liquidationPrice}
              executionFee={props.executionFee}
            />
          </CardInset>
        </Stack>

        <SmartLoadableButton
          disabled={props.error != null}
          Variant={
            props.error != null &&
            props.error.type !== EditMarginModalFormErrorType.EnterAmount
              ? RedButtonVariant
              : BlueButtonVariant
          }
          onPress={props.onSubmit}
        >
          <Button.Text>
            {(props.error == null
              ? null
              : renderButtonErrorMessageText(
                  intl,
                  props.quoteToken,
                  props.error,
                )) ??
              marginManipulation$t.fromMarginManipulation(
                intl,
                props.marginManipulation,
              )}
          </Button.Text>
        </SmartLoadableButton>
      </Stack>
    </CardBoxModalContent>
  )
}

const renderButtonErrorMessageText = (
  intl: IntlShape,
  quoteToken: TokenInfo,
  error: EditMarginModalFormError,
): null | JSX.Element => {
  if (
    error.type === EditMarginModalFormErrorType.InsufficientQuoteTokenBalance
  ) {
    return <>{commonErrorMessage$t.insufficientBalance(intl, quoteToken)}</>
  }

  if (error.type === EditMarginModalFormErrorType.EnterAmount) {
    return <>{intl.$t(commonErrorMessage$t.enterAmount)}</>
  }

  if (error.type === EditMarginModalFormErrorType.LeverageTooHigh) {
    return <>{intl.$t(commonErrorMessage$t.leverageTooHigh)}</>
  }

  if (error.type === EditMarginModalFormErrorType.LeverageTooLow) {
    return <>{intl.$t(commonErrorMessage$t.leverageTooLow)}</>
  }

  if (error.type === EditMarginModalFormErrorType.ExceedMargin) {
    return <>{intl.$t(commonErrorMessage$t.exceededMargin)}</>
  }

  checkNever(error.type)
  return null
}

interface InsetInfoListCoreData {
  positionSize: BigNumber
  marginSize: NumberChanging
  leverage: NumberChanging
  markPrice: SuspenseResource<BigNumber>
  liquidationPrice: NumberChanging
  executionFee: SuspenseResource<BigNumber>
}

const InsetInfoList: FC<
  {
    quoteToken: TokenInfo
  } & InsetInfoListCoreData
> = props => {
  const { $t } = useIntl()

  const spacing = useSpacing()

  return (
    <InfoList direction={"column"} listItemDirection={"row"}>
      <Stack space={spacing(1)}>
        <InfoListItem>
          <InfoListItemTitle>
            {$t(
              defineMessage({
                defaultMessage: "Position",
                description: "TradeScreen/Edit collateral modal/label",
              }),
            )}
          </InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              <TokenCountAsCurrency
                token={props.quoteToken}
                count={props.positionSize}
              />
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </InfoListItem>

        <InfoListItem>
          <InfoListItemTitle>
            {$t(
              defineMessage({
                defaultMessage: "Collateral",
                description: "TradeScreen/Edit collateral modal/label",
              }),
            )}
          </InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              <NumberChangingText
                token={props.quoteToken}
                changing={props.marginSize}
              />
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </InfoListItem>

        <InfoListItem>
          <InfoListItemTitle>
            {$t(
              defineMessage({
                defaultMessage: "Leverage",
                description: "TradeScreen/Edit collateral modal/label",
              }),
            )}
          </InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              <NumberChangingText
                token={props.quoteToken}
                changing={props.leverage}
                NumberFormatter={({ value }) => (
                  <>{`${BigNumber.toNumber(value).toFixed(2)}x`}</>
                )}
              />
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </InfoListItem>

        <InfoListItem>
          <InfoListItemTitle>
            {$t(
              defineMessage({
                defaultMessage: "Mark Price",
                description: "TradeScreen/Edit collateral modal/label",
              }),
            )}
          </InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              <TokenCountAsCurrency
                token={props.quoteToken}
                count={props.markPrice}
              />
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </InfoListItem>

        <InfoListItem>
          <InfoListItemTitle>
            {$t(
              defineMessage({
                defaultMessage: "Liq. Price",
                description: "TradeScreen/Edit collateral modal/label",
              }),
            )}
          </InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              <NumberChangingText
                token={props.quoteToken}
                changing={props.liquidationPrice}
              />
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </InfoListItem>

        <InfoListItem>
          <InfoListItemTitle>
            {$t(
              defineMessage({
                defaultMessage: "Execution Fee",
                description: "TradeScreen/Edit collateral modal/label",
              }),
            )}
          </InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              <TokenCountAsCurrency
                token={props.quoteToken}
                count={props.executionFee}
              />
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </InfoListItem>
      </Stack>
    </InfoList>
  )
}
