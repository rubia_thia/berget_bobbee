import { noop } from "../../../../utils/fnHelpers"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { OrderDirection, OrderType } from "../../types"
import { OrderCreationFrameProps } from "./OrderCreationFrame"

export const orderCreationFrameProps: OrderCreationFrameProps = {
  anchorToken: TokenInfoPresets.MockUSDT,
  swapRate: BigNumber.from(0.995),
  swapSlippage: null,
  onSwapSlippagePressed: noop,
  baseToken: TokenInfoPresets.MockBTC,
  quoteToken: TokenInfoPresets.MockUSDC,
  orderDirection: OrderDirection.Long,
  orderType: OrderType.Market,
  leverage: BigNumber.from(10),
  positionSize: BigNumber.from(100),
  liquidationPrice: BigNumber.from(1387.62),
  fees: BigNumber.from(3.2),
  stopLoseTrigger: null,
  takeProfitTrigger: {
    sourceType: "price",
    percentage: BigNumber.from(1),
    price: BigNumber.from(1695.1),
  },
  onChangeOrderType: noop,
  onConnectWallet: noop,
  onSubmit: noop,
}
