import { values } from "ramda"
import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import {
  StyleProp,
  Text,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import { Path, Svg } from "react-native-svg"
import { connectWallet$t, wrongNetwork$t } from "../../../../commonIntlMessages"
import { BlueButtonVariant } from "../../../../components/Button/BlueButtonVariant"
import { GreenButtonVariant } from "../../../../components/Button/GreenButtonVariant"
import { PinkButtonVariant } from "../../../../components/Button/PinkButtonVariant"
import { RedButtonVariant } from "../../../../components/Button/RedButtonVariant"
import { Button } from "../../../../components/ButtonFramework/Button"
import { SmartLoadableButton } from "../../../../components/ButtonFramework/LoadableButton"
import { CardBoxView } from "../../../../components/CardBox/CardBox"
import { CardInset } from "../../../../components/CardBox/CardInset"
import { InfoList } from "../../../../components/InfoList/InfoList"
import { InfoListItem } from "../../../../components/InfoList/InfoListItem"
import {
  InfoListItemDetail,
  InfoListItemDetailText,
} from "../../../../components/InfoList/InfoListItemDetail"
import { InfoListItemTitle } from "../../../../components/InfoList/InfoListItemTitle"
import {
  NoteParagraph,
  NoteParagraphText,
  WarnIcon,
} from "../../../../components/NoteParagraph/NoteParagraph"
import {
  formatPercentNumber,
  PercentNumber,
} from "../../../../components/PercentNumber"
import { Segment, SegmentControl } from "../../../../components/SegmentControl"
import { Spensor } from "../../../../components/Spensor"
import { Stack } from "../../../../components/Stack"
import { TabBar, TabbarItem } from "../../../../components/TabBar"
import {
  TextTokenCount,
  TokenCountAsCurrency,
} from "../../../../components/TextTokenCount"
import { useColors } from "../../../../components/Themed/color"
import { useSpacing } from "../../../../components/Themed/spacing"
import { oneOf } from "../../../../utils/arrayHelpers"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { FCC } from "../../../../utils/reactHelpers/types"
import { withProps } from "../../../../utils/reactHelpers/withProps/withProps"
import {
  readResource,
  safeReadResource,
  SuspenseResource,
} from "../../../../utils/SuspenseResource"
import { TokenInfo } from "../../../../utils/TokenInfo"
import { OrderDirection, OrderType } from "../../types"
import { orderDirection$t, orderType$t } from "../_/commonIntlMessages"
import {
  ActionTrigger,
  OrderCreationFormError,
  OrderCreationFormErrorType,
} from "./types"

const StyledTabbarItem = withProps(
  {
    textStyle: {
      fontSize: 14,
      fontWeight: "600",
    },
  },
  TabbarItem,
)

export interface OrderCreationFrameProps extends OrderInfoPropsMainPart {
  style?: StyleProp<ViewStyle>

  baseToken: TokenInfo
  quoteToken: TokenInfo

  orderDirection: OrderDirection
  onChangeOrderDirection?: (newMode: OrderDirection) => void

  orderType: OrderType
  onChangeOrderType: (newType: OrderType) => void

  formError?: SuspenseResource<OrderCreationFormError | undefined>
  onConnectWallet: () => void
  onSubmit: SuspenseResource<() => void | Promise<void>>
}

export const OrderCreationFrame: FCC<OrderCreationFrameProps> = props => {
  const intl = useIntl()
  const { $t } = intl

  const spacing = useSpacing()
  const colors = useColors()

  const error = safeReadResource(props.formError)
  const formErrorContent = useFormErrorContent({
    anchorToken: props.quoteToken,
    formError: error,
  })

  return (
    <CardBoxView style={[props.style, { padding: spacing(4) }]}>
      <Stack space={spacing(5)}>
        <SegmentControl>
          <Segment
            icon={
              <Svg width="16" height="16" viewBox="0 0 16 16" fill="white">
                <Path d="M7.03101 5.34513L9.27224 7.60946L12.28 4.59472H10.4437V3H15V7.56636H13.4515V5.82232L9.40924 9.94074L7.01023 7.51864L2.0552 12.4806L1 11.3823L7.03101 5.34513Z" />
              </Svg>
            }
            active={props.orderDirection === OrderDirection.Long}
            onPress={() => props.onChangeOrderDirection?.(OrderDirection.Long)}
          >
            {$t(orderDirection$t.buy)}
          </Segment>
          <Segment
            icon={
              <Svg width="16" height="16" viewBox="0 0 16 16" fill="#111827">
                <Path d="M7.53101 10.1353L9.77224 7.87101L12.78 10.8857H10.9437V12.4805H15.5V7.91411H13.9515V9.65815L9.90924 5.53973L7.51023 7.96183L2.5552 2.99988L1.5 4.09817L7.53101 10.1353Z" />
              </Svg>
            }
            active={props.orderDirection === OrderDirection.Short}
            onPress={() => props.onChangeOrderDirection?.(OrderDirection.Short)}
          >
            {$t(orderDirection$t.sell)}
          </Segment>
        </SegmentControl>

        <TabBar
          tabs={[
            {
              tab: ({ isSelected }) => (
                <StyledTabbarItem isSelected={isSelected}>
                  {$t(orderType$t.market)}
                </StyledTabbarItem>
              ),
              value: OrderType.Market,
            },
            {
              tab: ({ isSelected }) => (
                <StyledTabbarItem isSelected={isSelected}>
                  {$t(orderType$t.limit)}
                </StyledTabbarItem>
              ),
              value: OrderType.Limit,
            },
          ]}
          selectedTab={{
            tabValue: props.orderType,
            onChange: tab => props.onChangeOrderType?.(tab.value),
          }}
          separator={false}
        >
          {props.children}
        </TabBar>

        {formErrorContent != null && (
          <CardInset>
            <NoteParagraph textColor={colors("red-500")} Icon={WarnIcon}>
              <NoteParagraphText style={{ color: colors("red-500") }}>
                {formErrorContent}
              </NoteParagraphText>
            </NoteParagraph>
          </CardInset>
        )}

        {error?.type === OrderCreationFormErrorType.ConnectWalletRequired ? (
          <Button Variant={BlueButtonVariant} onPress={props.onConnectWallet}>
            {$t(connectWallet$t)}
          </Button>
        ) : error?.type ===
          OrderCreationFormErrorType.ConnectedToTheWrongNetwork ? (
          <Button
            disabled
            Variant={RedButtonVariant}
            onPress={props.onConnectWallet}
          >
            {$t(wrongNetwork$t)}
          </Button>
        ) : (
          <Spensor
            fallback={
              <Button Variant={BlueButtonVariant} disabled={true}>
                {orderDirection$t.fromOrderDirection(
                  intl,
                  props.orderDirection,
                )}
              </Button>
            }
          >
            {() => (
              <SmartLoadableButton
                Variant={
                  props.orderDirection === OrderDirection.Short
                    ? PinkButtonVariant
                    : GreenButtonVariant
                }
                disabled={
                  error != null &&
                  oneOf(...values(OrderCreationFormErrorType))(error.type)
                }
                onPress={readResource(props.onSubmit)}
              >
                {orderDirection$t.fromOrderDirection(
                  intl,
                  props.orderDirection,
                )}
              </SmartLoadableButton>
            )}
          </Spensor>
        )}

        <OrderInfo
          onSwapSlippagePressed={props.onSwapSlippagePressed}
          swapSlippage={props.swapSlippage}
          quoteToken={props.quoteToken}
          anchorToken={props.anchorToken}
          swapRate={props.swapRate}
          leverage={props.leverage}
          positionSize={props.positionSize}
          liquidationPrice={props.liquidationPrice}
          fees={props.fees}
          stopLoseTrigger={props.stopLoseTrigger}
          takeProfitTrigger={props.takeProfitTrigger}
        />
      </Stack>
    </CardBoxView>
  )
}

interface OrderInfoPropsMainPart {
  leverage: SuspenseResource<BigNumber>
  positionSize: SuspenseResource<BigNumber>
  liquidationPrice: SuspenseResource<BigNumber>
  fees: SuspenseResource<BigNumber>
  swapSlippage: SuspenseResource<BigNumber | null>
  swapRate: SuspenseResource<BigNumber | null>
  anchorToken: TokenInfo
  onSwapSlippagePressed: () => void
  stopLoseTrigger: SuspenseResource<null | ActionTrigger>
  takeProfitTrigger: SuspenseResource<null | ActionTrigger>
}
interface OrderInfoProps extends OrderInfoPropsMainPart {
  quoteToken: TokenInfo
}
const OrderInfo: FC<OrderInfoProps> = props => {
  const { $t } = useIntl()
  const spacing = useSpacing()

  return (
    <InfoList>
      <View className="space-y-2">
        <InfoListItem>
          <InfoListItemTitle>
            {$t(
              defineMessage({
                defaultMessage: "Leverage",
                description: "TradeScreen/Order creation/field name",
              }),
            )}
          </InfoListItemTitle>
          <InfoListItemDetail>
            <Text>
              <Spensor fallback={"-"}>
                {() => `${readResource(props.leverage).toString()}x`}
              </Spensor>
            </Text>
          </InfoListItemDetail>
        </InfoListItem>

        <InfoListItem>
          <InfoListItemTitle>
            {$t(
              defineMessage({
                defaultMessage: "Position Size",
                description: "TradeScreen/Order creation/field name",
              }),
            )}
          </InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              <TokenCountAsCurrency
                token={props.quoteToken}
                count={props.positionSize}
              />
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </InfoListItem>

        <InfoListItem>
          <InfoListItemTitle>
            {$t(
              defineMessage({
                defaultMessage: "Liq. Price",
                description: "TradeScreen/Order creation/field name",
              }),
            )}
          </InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              <TokenCountAsCurrency
                token={props.quoteToken}
                count={props.liquidationPrice}
              />
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </InfoListItem>

        <InfoListItem>
          <InfoListItemTitle>
            {$t(
              defineMessage({
                defaultMessage: "Fees",
                description: "TradeScreen/Order creation/field name",
              }),
            )}
          </InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              <TokenCountAsCurrency
                token={props.quoteToken}
                count={props.fees}
              />
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </InfoListItem>

        <Spensor>
          {() => {
            const swapRate = readResource(props.swapRate)
            if (!swapRate) {
              return null
            }
            return (
              <InfoListItem className="mt-2">
                <InfoListItemTitle>
                  {$t(
                    defineMessage({
                      defaultMessage: "Swap Rate",
                      description:
                        "LiquidityScreen/ActionPanel/info list label",
                    }),
                  )}
                </InfoListItemTitle>
                <InfoListItemDetail>
                  <InfoListItemDetailText>
                    <TextTokenCount
                      count={swapRate}
                      token={{
                        ...props.anchorToken,
                        precision: 4,
                      }}
                    />
                  </InfoListItemDetailText>
                </InfoListItemDetail>
              </InfoListItem>
            )
          }}
        </Spensor>
        <Spensor>
          {() => {
            const slippage = readResource(props.swapSlippage)
            if (slippage == null) {
              return null
            }
            return (
              <InfoListItem className="mt-2">
                <InfoListItemTitle>
                  {$t(
                    defineMessage({
                      defaultMessage: "Swap Slippage",
                      description: "TradeScreen/Order creation/field name",
                    }),
                  )}
                </InfoListItemTitle>
                <TouchableOpacity
                  className="ml-auto flex-row items-center"
                  onPress={props.onSwapSlippagePressed}
                >
                  <Svg
                    width="12"
                    height="12"
                    viewBox="0 0 12 12"
                    style={{ marginRight: spacing(1) }}
                  >
                    <Path
                      d="M9.39894 5.09131L3.86806 10.6209L0.75 11.25L1.37912 8.1315L6.90956 2.6015L9.39894 5.09131V5.09131ZM10.0176 4.47269L11.25 3.23894L8.76019 0.75L7.52819 1.98244L10.0176 4.47269V4.47269Z"
                      fill="#9CA3AF"
                    />
                  </Svg>
                  <InfoListItemDetail>
                    <InfoListItemDetailText>
                      <PercentNumber number={slippage} />
                    </InfoListItemDetailText>
                  </InfoListItemDetail>
                </TouchableOpacity>
              </InfoListItem>
            )
          }}
        </Spensor>

        <InfoListItem>
          <InfoListItemTitle>
            {$t(
              defineMessage({
                defaultMessage: "Stop Loss",
                description: "TradeScreen/Order creation/field name",
              }),
            )}
          </InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              <Spensor fallback={"-"}>
                {() => (
                  <SLTPInfoText
                    quoteToken={props.quoteToken}
                    info={readResource(props.stopLoseTrigger)}
                  />
                )}
              </Spensor>
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </InfoListItem>

        <InfoListItem>
          <InfoListItemTitle>
            {$t(
              defineMessage({
                defaultMessage: "Take Profit",
                description: "TradeScreen/Order creation/field name",
              }),
            )}
          </InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              <Spensor fallback={"-"}>
                {() => (
                  <SLTPInfoText
                    quoteToken={props.quoteToken}
                    info={readResource(props.takeProfitTrigger)}
                  />
                )}
              </Spensor>
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </InfoListItem>
      </View>
    </InfoList>
  )
}

const SLTPInfoText: FC<{
  quoteToken: TokenInfo
  info: null | ActionTrigger
}> = props => {
  const { $t } = useIntl()
  return (
    <>
      {props.info == null ? (
        $t(
          defineMessage({
            defaultMessage: "None",
            description: "TradeScreen/Order creation/take profit value",
          }),
        )
      ) : (
        <>
          <PercentNumber number={props.info.percentage} />
          &nbsp; (
          <TokenCountAsCurrency
            token={props.quoteToken}
            count={props.info.price}
          />
          )
        </>
      )}
    </>
  )
}

const useFormErrorContent = (props: {
  anchorToken: TokenInfo
  formError?: OrderCreationFormError
}): null | undefined | JSX.Element => {
  const { $t } = useIntl()

  if (
    props.formError?.type ===
    OrderCreationFormErrorType.InsufficientQuoteTokenBalance
  ) {
    return (
      <>
        {$t(
          defineMessage({
            defaultMessage: "Insufficient Balance",
            description: "TradeScreen/Create Order Panel/error messages",
          }),
        )}
      </>
    )
  }

  if (props.formError?.type === OrderCreationFormErrorType.SlippageOutOfRange) {
    return (
      <>
        {$t(
          defineMessage({
            defaultMessage: "Slippage can only be between {min} and {max}",
            description: "TradeScreen/Create Order Panel/error messages",
          }),
          {
            min: formatPercentNumber(props.formError.min),
            max: formatPercentNumber(props.formError.max),
          },
        )}
      </>
    )
  }
  if (props.formError?.type === OrderCreationFormErrorType.PythPriceOutOfDate) {
    return (
      <>
        {$t(
          defineMessage({
            defaultMessage: "Pyth price is out of date",
            description: "TradeScreen/Create Order Panel/error messages",
          }),
        )}
      </>
    )
  }
  if (props.formError?.type === OrderCreationFormErrorType.PositionIsTooMall) {
    return (
      <>
        {$t(
          defineMessage({
            defaultMessage: "Min position size is {min}",
            description: "TradeScreen/Create Order Panel/error messages",
          }),
          {
            min: (
              <TokenCountAsCurrency
                token={props.anchorToken}
                count={props.formError.minPosition}
              />
            ),
          },
        )}
      </>
    )
  }

  return null
}
