import { noop } from "../../../../utils/fnHelpers"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { LimitOrderCreationContentProps } from "./LimitOrderCreationContent"

export const limitOrderCreationContentProps: LimitOrderCreationContentProps = {
  baseToken: TokenInfoPresets.MockBTC,
  quoteToken: TokenInfoPresets.MockUSDC,

  quoteTokenBalance: BigNumber.from(136.3),
  onSelectQuoteToken: noop,
  availableQuoteTokens: [TokenInfoPresets.MockUSDC, TokenInfoPresets.MockBUSD],

  leverageRange: [BigNumber.from(1.5), BigNumber.from(200)],
  leverage: BigNumber.from(2),
  onLeverageChange: noop,

  price: BigNumber.from(1_541.8),

  stopLossTriggerPercentageOptions: [
    { value: null },
    { value: BigNumber.from(0.1) },
    { value: BigNumber.from(0.25) },
    { value: BigNumber.from(0.5) },
    { value: BigNumber.from(0.75) },
  ],
  stopLossTrigger: null,
  onStopLossTriggerChange: noop,

  takeProfitTriggerPercentageOptions: [
    { value: BigNumber.from(0.25) },
    { value: BigNumber.from(0.5) },
    { value: BigNumber.from(0.75) },
    { value: BigNumber.from(1) },
    { value: BigNumber.from(9) },
  ],
  takeProfitTrigger: {
    sourceType: "percentage",
    percentage: BigNumber.from(0.5),
    price: BigNumber.from(888.8),
  },
  onTakeProfitTriggerChange: noop,

  value: null,
  onValueChange: noop,
  onPressMax: noop,
}
