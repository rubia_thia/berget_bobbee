import { FC, ReactNode } from "react"
import { StyleProp, Text, TextStyle } from "react-native"
import { useColors } from "../../../../../components/Themed/color"

export const FieldLabel: FC<{
  style?: StyleProp<TextStyle>
  contentText: ReactNode
}> = props => {
  const colors = useColors()

  return (
    <Text
      style={[
        {
          color: colors("gray-900"),
          fontSize: 14,
          lineHeight: 20,
        },
        props.style,
      ]}
    >
      {props.contentText}
    </Text>
  )
}
