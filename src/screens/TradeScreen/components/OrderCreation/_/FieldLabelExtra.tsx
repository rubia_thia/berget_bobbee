import { FC, ReactNode } from "react"
import { StyleProp, Text, TextStyle } from "react-native"
import { useColors } from "../../../../../components/Themed/color"

export const FieldLabelExtra: FC<{
  style?: StyleProp<TextStyle>
  contentText: ReactNode
}> = props => {
  const colors = useColors()

  return (
    <Text
      style={[
        {
          color: colors("gray-500"),
          fontSize: 12,
          lineHeight: 16,
        },
        props.style,
      ]}
    >
      {props.contentText}
    </Text>
  )
}
