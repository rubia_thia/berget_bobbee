import { FC } from "react"
import { useIntl } from "react-intl"
import { ActivityIndicator, StyleProp, View, ViewStyle } from "react-native"
import { SpensorR } from "../../../../../components/Spensor"
import { TextNumber } from "../../../../../components/TextNumber"
import { useSpacing } from "../../../../../components/Themed/spacing"
import { hasAny } from "../../../../../utils/arrayHelpers"
import { BigNumber } from "../../../../../utils/numberHelpers/BigNumber"
import { marginCenter } from "../../../../../utils/styleHelpers/styleHelpers"
import {
  safeReadResource,
  SuspenseResource,
} from "../../../../../utils/SuspenseResource"
import { OneOrMore } from "../../../../../utils/types"
import { FormattedNumberInput } from "../../FormattedNumberInput"
import {
  InlineNumberSegmentControl,
  InlineNumberSegmentControlItem,
} from "../../InlineNumberSegmentControl"
import {
  placeholderPrice$t,
  specialPriceNone$t,
} from "../../_/commonIntlMessages"
import {
  ActionTrigger,
  ActionTriggerPatch,
  ActionTriggerPercentageOption,
} from "../types"

export const PercentageNumberControl: FC<{
  style?: StyleProp<ViewStyle>
  error?: boolean
  percentageOptions: SuspenseResource<ActionTriggerPercentageOption[]>
  renderLabelNumber: (num: BigNumber) => JSX.Element
  trigger: SuspenseResource<null | ActionTrigger>
  onTriggerUpdated: (trigger: null | ActionTriggerPatch) => void
  precision?: number
}> = props => {
  const trigger = safeReadResource(props.trigger)

  const { $t } = useIntl()
  const spacing = useSpacing()

  const segmentControlValue =
    trigger == null
      ? { value: null }
      : trigger.sourceType === "percentage"
      ? { value: trigger.percentage }
      : null

  const inputValue =
    trigger != null && trigger.sourceType === "price" ? trigger.price : null

  const textStyle = {
    fontSize: 12,
    lineHeight: 16,
  }
  const verticalPaddingStyle = {
    paddingVertical: spacing(2.5),
  }

  return (
    <View style={[{ flexDirection: "row", alignItems: "center" }, props.style]}>
      <SpensorR
        fallback={<ActivityIndicator style={marginCenter} />}
        read={{ options: props.percentageOptions }}
      >
        {({ options }) =>
          !hasAny(options) ? null : (
            <InlineNumberSegmentControl
              style={{ flex: 1 }}
              textStyle={textStyle}
              itemPadding={verticalPaddingStyle}
              items={
                options.map(option => ({
                  labelText:
                    option.value == null
                      ? $t(specialPriceNone$t)
                      : props.renderLabelNumber(option.value),
                  value: option.value,
                  disabled: option.disabled,
                })) as OneOrMore<
                  InlineNumberSegmentControlItem<null | BigNumber>
                >
              }
              selectedValue={segmentControlValue}
              onSelectedValueChange={v => {
                props.onTriggerUpdated(
                  v.value == null
                    ? null
                    : { sourceType: "percentage", percentage: v.value },
                )
              }}
              valueEquals={(a, b) => {
                if (a === b) return true
                return a !== null && b !== null && BigNumber.isEq(a, b)
              }}
            />
          )
        }
      </SpensorR>

      <FormattedNumberInput
        style={{ marginLeft: spacing(2.5), width: 100 }}
        textStyle={textStyle}
        padding={verticalPaddingStyle}
        error={props.error}
        placeholder={$t(placeholderPrice$t)}
        valueFormattedText={
          inputValue == null ? null : <TextNumber number={inputValue} />
        }
        value={inputValue}
        onValueChange={v =>
          props.onTriggerUpdated(
            v == null ? null : { sourceType: "price", price: v },
          )
        }
        precision={props.precision}
      />
    </View>
  )
}
