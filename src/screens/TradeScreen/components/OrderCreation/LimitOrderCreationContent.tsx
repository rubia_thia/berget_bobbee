import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, View, ViewStyle } from "react-native"
import { Divider } from "../../../../components/Divider"
import { InfoList } from "../../../../components/InfoList/InfoList"
import {
  DefaultInfoListItem,
  InfoListItem,
} from "../../../../components/InfoList/InfoListItem"
import { InfoListItemDetail } from "../../../../components/InfoList/InfoListItemDetail"
import {
  DefaultInfoListItemTitle,
  InfoListItemTitle,
} from "../../../../components/InfoList/InfoListItemTitle"
import { PercentNumber } from "../../../../components/PercentNumber"
import { Slider } from "../../../../components/Slider"
import { Spensor, SpensorR } from "../../../../components/Spensor"
import { Stack } from "../../../../components/Stack"
import { TextNumber } from "../../../../components/TextNumber"
import { useColors } from "../../../../components/Themed/color"
import { useSpacing } from "../../../../components/Themed/spacing"
import { TokenCount } from "../../../../components/TokenCount"
import { BalanceTopArea } from "../../../../components/TokenInput/BalanceTopArea"
import { TokenInput } from "../../../../components/TokenInput/TokenInput"
import { oneOf } from "../../../../utils/arrayHelpers"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import {
  readResource,
  safeReadResource,
  suspenseResource,
  SuspenseResource,
} from "../../../../utils/SuspenseResource"
import { TokenInfo } from "../../../../utils/TokenInfo"
import {
  ActionTriggerConditionNumberDisplayingText,
  percentageLimitInRange100,
} from "../ActionTriggerConditionNumberDisplayingText"
import { FormattedNumberInput } from "../FormattedNumberInput"
import { Colors } from "../_/designTokens"
import {
  ActionTrigger,
  ActionTriggerPatch,
  ActionTriggerPercentageOption,
  OrderCreationFormError,
  OrderCreationFormErrorType,
} from "./types"
import { FieldLabel } from "./_/FieldLabel"
import { FieldLabelExtra } from "./_/FieldLabelExtra"
import { PercentageNumberControl } from "./_/PercentageNumberControl"

export interface LimitOrderCreationContentProps {
  style?: StyleProp<ViewStyle>
  gap?: number

  formError?: SuspenseResource<OrderCreationFormError | undefined>

  baseToken: TokenInfo
  quoteToken: SuspenseResource<TokenInfo>
  availableQuoteTokens: TokenInfo[]
  onSelectQuoteToken: (newToken: TokenInfo) => void

  quoteTokenBalance: SuspenseResource<BigNumber>

  leverageRange: SuspenseResource<[min: BigNumber, max: BigNumber]>
  leverage: SuspenseResource<null | BigNumber>
  onLeverageChange: (leverage: null | BigNumber) => void

  price: SuspenseResource<null | BigNumber>
  onPriceChange?: (price: null | BigNumber) => void

  stopLossTriggerPercentageOptions: SuspenseResource<
    ActionTriggerPercentageOption[]
  >
  stopLossTrigger: SuspenseResource<null | ActionTrigger>
  onStopLossTriggerChange: (newTrigger: null | ActionTriggerPatch) => void
  takeProfitTriggerPercentageOptions: SuspenseResource<
    ActionTriggerPercentageOption[]
  >
  takeProfitTrigger: SuspenseResource<null | ActionTrigger>
  onTakeProfitTriggerChange: (newTrigger: null | ActionTriggerPatch) => void

  value: SuspenseResource<null | BigNumber>
  onValueChange: (value: null | BigNumber) => void
  onPressMax: SuspenseResource<() => void>
}

export const LimitOrderCreationContent: FC<
  LimitOrderCreationContentProps
> = props => {
  const { $t } = useIntl()
  const spacing = useSpacing()
  const colors = useColors()

  return (
    <Stack style={props.style} space={props.gap}>
      <TokenInput
        error={oneOf(
          OrderCreationFormErrorType.MarginAmountIsEmpty,
          OrderCreationFormErrorType.InsufficientQuoteTokenBalance,
        )(safeReadResource(props.formError)?.type)}
        topArea={
          <BalanceTopArea
            titleText={$t(
              defineMessage({
                defaultMessage: "Collateral",
                description: "TradeScreen/Market Order Creation/input label",
              }),
            )}
            token={props.quoteToken}
            balance={props.quoteTokenBalance}
          />
        }
        placeholder={"0.0"}
        availableTokens={props.availableQuoteTokens}
        token={props.quoteToken}
        onTokenChange={props.onSelectQuoteToken}
        value={props.value}
        onValueChange={props.onValueChange}
        onPressMax={safeReadResource(props.onPressMax)}
      />

      <View>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            marginBottom: spacing(1),
          }}
        >
          <FieldLabel
            contentText={$t<JSX.Element>(
              defineMessage({
                defaultMessage:
                  "Leverage <extraContentStyle>({leverageRange})</extraContentStyle>",
                description: "TradeScreen/Market Order Creation/label",
              }),
              {
                leverageRange: (
                  <Spensor fallback={"-"}>
                    {() => {
                      const [min, max] = readResource(props.leverageRange)
                      return `${min}x - ${max}x`
                    }}
                  </Spensor>
                ),
                extraContentStyle: content => (
                  <FieldLabelExtra contentText={content} />
                ),
              },
            )}
          />

          <FormattedNumberInput
            style={{ width: 100 }}
            error={oneOf(
              OrderCreationFormErrorType.LeverageOutOfRange,
              OrderCreationFormErrorType.LeverageIsEmpty,
            )(safeReadResource(props.formError)?.type)}
            multiline={true}
            valueFormattedText={suspenseResource(() =>
              readResource(props.leverage) == null ? null : (
                <>
                  <TextNumber number={readResource(props.leverage)!} />x
                </>
              ),
            )}
            value={props.leverage}
            onValueChange={props.onLeverageChange}
          />
        </View>

        <SpensorR
          read={{
            leverage: props.leverage,
            leverageRange: props.leverageRange,
          }}
        >
          {result => (
            <Slider
              value={BigNumber.toNumber(result.leverage ?? BigNumber.ZERO)}
              onChange={n =>
                props.onLeverageChange(BigNumber.from(n.toFixed(2)))
              }
              min={BigNumber.toNumber(result.leverageRange[0])}
              max={BigNumber.toNumber(result.leverageRange[1])}
              segments={[
                { endingPosition: 0.08, endingValue: 2, labelText: "2" },
                { endingPosition: 0.22, endingValue: 5, labelText: "5" },
                { endingPosition: 0.36, endingValue: 10, labelText: "10" },
                { endingPosition: 0.5, endingValue: 20, labelText: "20" },
                { endingPosition: 0.64, endingValue: 30, labelText: "30" },
                { endingPosition: 0.78, endingValue: 50, labelText: "50" },
                { endingPosition: 0.92, endingValue: 150, labelText: "150" },
                {},
              ]}
            />
          )}
        </SpensorR>
      </View>

      <InfoList
        direction={"row"}
        listItemDirection={"column"}
        renderInfoListItem={p => (
          <DefaultInfoListItem {...p} style={[p.style, { flex: 1 }]} />
        )}
        renderInfoListItemTitle={p => (
          <DefaultInfoListItemTitle
            {...p}
            style={[p.style, { flex: 1, marginBottom: spacing(2) }]}
          />
        )}
      >
        <InfoListItem>
          <InfoListItemTitle>
            <FieldLabel
              contentText={
                <Spensor
                  fallback={$t<JSX.Element>(
                    defineMessage({
                      defaultMessage: "Price",
                      description: "TradeScreen/Market Order Creation/label",
                    }),
                  )}
                >
                  {() =>
                    $t<JSX.Element>(
                      defineMessage({
                        defaultMessage: "Price",
                        description: "TradeScreen/Market Order Creation/label",
                      }),
                    )
                  }
                </Spensor>
              }
            />
          </InfoListItemTitle>

          <InfoListItemDetail>
            <FormattedNumberInput
              error={oneOf(OrderCreationFormErrorType.PriceIsEmpty)(
                safeReadResource(props.formError)?.type,
              )}
              precision={
                safeReadResource(props.quoteToken) != null
                  ? TokenInfo.getPrecision(safeReadResource(props.quoteToken))
                  : undefined
              }
              value={props.price}
              valueFormattedText={suspenseResource(() => {
                const price = readResource(props.price)
                return price == null ? null : (
                  <TokenCount
                    token={props.quoteToken}
                    count={readResource(props.price)!}
                  />
                )
              })}
              onValueChange={props.onPriceChange}
            />
          </InfoListItemDetail>
        </InfoListItem>
      </InfoList>

      <Divider color={colors("gray-200")} />

      <InfoList direction={"column"} listItemDirection={"column"}>
        <Stack space={props.gap}>
          <InfoListItem>
            <InfoListItemTitle>
              <FieldLabel
                contentText={$t<JSX.Element>(
                  defineMessage({
                    defaultMessage:
                      "Stop Loss <extraContentStyle>({stopLossPrice})</extraContentStyle>",
                    description: "TradeScreen/Market Order Creation/label",
                  }),
                  {
                    stopLossPrice: (
                      <ActionTriggerConditionNumberDisplayingText
                        quoteToken={props.quoteToken}
                        trigger={props.stopLossTrigger}
                        percentageLimitInRange={percentageLimitInRange100}
                      />
                    ),
                    extraContentStyle: content => (
                      <FieldLabelExtra
                        style={{ color: Colors.lossColor }}
                        contentText={content}
                      />
                    ),
                  },
                )}
              />
            </InfoListItemTitle>

            <InfoListItemDetail style={{ marginTop: spacing(1) }}>
              <PercentageNumberControl
                error={oneOf(
                  OrderCreationFormErrorType.StopLossPriceOutOfRange,
                )(safeReadResource(props.formError)?.type)}
                percentageOptions={props.stopLossTriggerPercentageOptions}
                renderLabelNumber={n => (
                  <PercentNumber number={BigNumber.neg(n)} />
                )}
                trigger={props.stopLossTrigger}
                onTriggerUpdated={props.onStopLossTriggerChange}
                precision={
                  safeReadResource(props.quoteToken) != null
                    ? TokenInfo.getPrecision(safeReadResource(props.quoteToken))
                    : undefined
                }
              />
            </InfoListItemDetail>
          </InfoListItem>

          <InfoListItem>
            <InfoListItemTitle>
              <FieldLabel
                contentText={$t<JSX.Element>(
                  defineMessage({
                    defaultMessage:
                      "Take Profit <extraContentStyle>({takeProfitPrice})</extraContentStyle>",
                    description: "TradeScreen/Market Order Creation/label",
                  }),
                  {
                    takeProfitPrice: (
                      <ActionTriggerConditionNumberDisplayingText
                        quoteToken={props.quoteToken}
                        trigger={props.takeProfitTrigger}
                      />
                    ),
                    extraContentStyle: content => (
                      <FieldLabelExtra
                        style={{ color: Colors.profitColor }}
                        contentText={content}
                      />
                    ),
                  },
                )}
              />
            </InfoListItemTitle>

            <InfoListItemDetail style={{ marginTop: spacing(1) }}>
              <PercentageNumberControl
                error={oneOf(
                  OrderCreationFormErrorType.TakeProfitPriceOutOfRange,
                )(safeReadResource(props.formError)?.type)}
                percentageOptions={props.takeProfitTriggerPercentageOptions}
                renderLabelNumber={n => <PercentNumber number={n} />}
                trigger={props.takeProfitTrigger}
                onTriggerUpdated={props.onTakeProfitTriggerChange}
                precision={
                  safeReadResource(props.quoteToken) != null
                    ? TokenInfo.getPrecision(safeReadResource(props.quoteToken))
                    : undefined
                }
              />
            </InfoListItemDetail>
          </InfoListItem>
        </Stack>
      </InfoList>

      <Divider color={colors("gray-200")} />
    </Stack>
  )
}
