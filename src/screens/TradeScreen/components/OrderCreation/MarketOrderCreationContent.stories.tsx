import { ComponentMeta } from "@storybook/react"
import {
  BackgroundColor,
  CardContainer,
} from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { MarketOrderCreationContent } from "./MarketOrderCreationContent"
import { marketOrderCreationContentProps } from "./MarketOrderCreationContent.mockData"
import { OrderCreationFormErrorType } from "./types"

export default {
  title: "Page/TradeScreen/OrderCreation/MarketOrderCreationContent",
  component: MarketOrderCreationContent,
  decorators: [CardContainer({ margin: 10, padding: 10 }), BackgroundColor()],
} as ComponentMeta<typeof MarketOrderCreationContent>

const template = withTemplate(MarketOrderCreationContent, {
  gap: 20,
  ...marketOrderCreationContentProps,
})

export const Normal = template()

export const SuperSmallTakeProfitRate = template(p => {
  p.takeProfitTrigger = {
    sourceType: "percentage",
    price: BigNumber.from(0.00001),
    percentage: BigNumber.from(0.001),
  }
})

export const ErrorLeverageOutOfRange = template(p => {
  p.formError = {
    type: OrderCreationFormErrorType.LeverageOutOfRange,
    min: BigNumber.from(1.5),
    max: BigNumber.from(100),
  }
})

export const ErrorLeverageIsEmpty = template(p => {
  p.formError = {
    type: OrderCreationFormErrorType.LeverageIsEmpty,
  }
})

export const ErrorMarginAmountIsEmpty = template(p => {
  p.formError = {
    type: OrderCreationFormErrorType.MarginAmountIsEmpty,
  }
})

export const ErrorInsufficientQuoteTokenBalance = template(p => {
  p.formError = {
    type: OrderCreationFormErrorType.InsufficientQuoteTokenBalance,
  }
})

export const ErrorSlippageIsEmpty = template(p => {
  p.formError = {
    type: OrderCreationFormErrorType.SlippageIsEmpty,
  }
})

export const ErrorSlippageOutOfRange = template(p => {
  p.formError = {
    type: OrderCreationFormErrorType.SlippageOutOfRange,
    min: BigNumber.from(0.0001),
    max: BigNumber.from(0.49),
  }
})

export const ErrorStopLossPriceOutOfRange = template(p => {
  p.formError = {
    type: OrderCreationFormErrorType.StopLossPriceOutOfRange,
  }
})

export const ErrorTakeProfitPriceOutOfRange = template(p => {
  p.formError = {
    type: OrderCreationFormErrorType.TakeProfitPriceOutOfRange,
  }
})
