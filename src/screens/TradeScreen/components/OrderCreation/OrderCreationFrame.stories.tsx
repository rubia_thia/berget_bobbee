import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { OrderDirection } from "../../types"
import { OrderCreationFrame } from "./OrderCreationFrame"
import { orderCreationFrameProps } from "./OrderCreationFrame.mockData"
import { OrderCreationFormErrorType } from "./types"

export default {
  title: "Page/TradeScreen/OrderCreation/OrderCreationFrame",
  component: OrderCreationFrame,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof OrderCreationFrame>

const template = withTemplate(OrderCreationFrame, {
  style: { margin: 10 },
  ...orderCreationFrameProps,
})

export const Long = template()

export const Short = template(p => {
  p.orderDirection = OrderDirection.Short
})

export const ConnectWallet = template(p => {
  p.formError = {
    type: OrderCreationFormErrorType.ConnectWalletRequired,
  }
})

export const ErrorInsufficientBalance = template(p => {
  p.formError = {
    type: OrderCreationFormErrorType.InsufficientQuoteTokenBalance,
  }
})

export const ErrorLeverageOutOfRange = template(p => {
  p.formError = {
    type: OrderCreationFormErrorType.LeverageOutOfRange,
    min: BigNumber.from(1.5),
    max: BigNumber.from(100),
  }
})

export const ErrorSlippageOutOfRange = template(p => {
  p.formError = {
    type: OrderCreationFormErrorType.SlippageOutOfRange,
    min: BigNumber.from(0.0001),
    max: BigNumber.from(0.49),
  }
})
