import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, Text, View, ViewStyle } from "react-native"
import { Path, Rect, Svg } from "react-native-svg"
import { Divider } from "../../../../components/Divider"
import { InfoList } from "../../../../components/InfoList/InfoList"
import {
  DefaultInfoListItem,
  InfoListItem,
} from "../../../../components/InfoList/InfoListItem"
import { InfoListItemDetail } from "../../../../components/InfoList/InfoListItemDetail"
import {
  DefaultInfoListItemTitle,
  InfoListItemTitle,
} from "../../../../components/InfoList/InfoListItemTitle"
import { PercentNumber } from "../../../../components/PercentNumber"
import { Slider } from "../../../../components/Slider"
import { Spensor, SpensorR } from "../../../../components/Spensor"
import { Stack } from "../../../../components/Stack"
import { TextNumber } from "../../../../components/TextNumber"
import { useColors } from "../../../../components/Themed/color"
import { useSpacing } from "../../../../components/Themed/spacing"
import { TokenCount } from "../../../../components/TokenCount"
import { BalanceTopArea } from "../../../../components/TokenInput/BalanceTopArea"
import { TokenInput } from "../../../../components/TokenInput/TokenInput"
import {
  TextInsideTooltip,
  TooltippifiedText,
} from "../../../../components/Tooltip"
import { oneOf } from "../../../../utils/arrayHelpers"
import { FormattedMessageOutsideText } from "../../../../utils/intlHelpers"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { HrefLink } from "../../../../utils/reactNavigationHelpers/HrefLink"
import {
  readResource,
  safeReadResource,
  suspenseResource,
  SuspenseResource,
} from "../../../../utils/SuspenseResource"
import { TokenInfo } from "../../../../utils/TokenInfo"
import {
  ActionTriggerConditionNumberDisplayingText,
  percentageLimitInRange100,
} from "../ActionTriggerConditionNumberDisplayingText"
import { FormattedNumberInput } from "../FormattedNumberInput"
import { Colors } from "../_/designTokens"
import {
  ActionTrigger,
  ActionTriggerPatch,
  ActionTriggerPercentageOption,
  OrderCreationFormError,
  OrderCreationFormErrorType,
} from "./types"
import { FieldLabel } from "./_/FieldLabel"
import { FieldLabelExtra } from "./_/FieldLabelExtra"
import { PercentageNumberControl } from "./_/PercentageNumberControl"

export interface MarketOrderCreationContentProps {
  style?: StyleProp<ViewStyle>
  gap?: number

  formError?: SuspenseResource<OrderCreationFormError | undefined>

  baseToken: TokenInfo
  quoteToken: SuspenseResource<TokenInfo>
  availableQuoteTokens: TokenInfo[]
  onSelectQuoteToken: (newToken: TokenInfo) => void

  quoteTokenBalance: SuspenseResource<BigNumber>

  leverageRange: SuspenseResource<[min: BigNumber, max: BigNumber]>
  leverage: SuspenseResource<null | BigNumber>
  onLeverageChange: (leverage: null | BigNumber) => void

  priceSpreadRate: SuspenseResource<BigNumber>
  marketPrice: SuspenseResource<BigNumber>
  markPrice: SuspenseResource<BigNumber>
  entryPrice: SuspenseResource<undefined | BigNumber>
  exitPrice: SuspenseResource<undefined | BigNumber>

  slippagePercentage: SuspenseResource<null | BigNumber>
  onSlippageChange: (newSlippage: null | BigNumber) => void

  stopLossTriggerPercentageOptions: SuspenseResource<
    ActionTriggerPercentageOption[]
  >
  stopLossTrigger: SuspenseResource<null | ActionTrigger>
  onStopLossTriggerChange: (newTrigger: null | ActionTriggerPatch) => void
  takeProfitTriggerPercentageOptions: SuspenseResource<
    ActionTriggerPercentageOption[]
  >
  takeProfitTrigger: SuspenseResource<null | ActionTrigger>
  onTakeProfitTriggerChange: (newTrigger: null | ActionTriggerPatch) => void

  value: SuspenseResource<null | BigNumber>
  onValueChange: (value: null | BigNumber) => void
  onPressMax: SuspenseResource<() => void>
}

export const MarketOrderCreationContent: FC<
  MarketOrderCreationContentProps
> = props => {
  const { $t } = useIntl()
  const spacing = useSpacing()
  const colors = useColors()

  return (
    <Stack style={props.style} space={props.gap}>
      <TokenInput
        error={oneOf(
          OrderCreationFormErrorType.MarginAmountIsEmpty,
          OrderCreationFormErrorType.InsufficientQuoteTokenBalance,
        )(safeReadResource(props.formError)?.type)}
        topArea={
          <BalanceTopArea
            titleText={$t(
              defineMessage({
                defaultMessage: "Collateral",
                description: "TradeScreen/Market Order Creation/input label",
              }),
            )}
            token={props.quoteToken}
            balance={props.quoteTokenBalance}
          />
        }
        placeholder={"0.0"}
        availableTokens={props.availableQuoteTokens}
        token={props.quoteToken}
        onTokenChange={props.onSelectQuoteToken}
        value={props.value}
        onValueChange={props.onValueChange}
        onPressMax={safeReadResource(props.onPressMax)}
      />

      <View>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            marginBottom: spacing(1),
          }}
        >
          <FieldLabel
            contentText={$t<JSX.Element>(
              defineMessage({
                defaultMessage:
                  "Leverage <extraContentStyle>({leverageRange})</extraContentStyle>",
                description: "TradeScreen/Market Order Creation/label",
              }),
              {
                leverageRange: (
                  <Spensor fallback={"-"}>
                    {() => {
                      const [min, max] = readResource(props.leverageRange)
                      return `${min}x - ${max}x`
                    }}
                  </Spensor>
                ),
                extraContentStyle: content => (
                  <FieldLabelExtra contentText={content} />
                ),
              },
            )}
          />

          <FormattedNumberInput
            style={{ width: 100 }}
            error={oneOf(
              OrderCreationFormErrorType.LeverageOutOfRange,
              OrderCreationFormErrorType.LeverageIsEmpty,
            )(safeReadResource(props.formError)?.type)}
            multiline={true}
            valueFormattedText={suspenseResource(() =>
              readResource(props.leverage) == null ? null : (
                <>
                  <TextNumber number={readResource(props.leverage)!} />x
                </>
              ),
            )}
            value={props.leverage}
            onValueChange={props.onLeverageChange}
          />
        </View>

        <SpensorR
          read={{
            leverage: props.leverage,
            leverageRange: props.leverageRange,
          }}
        >
          {result => (
            <Slider
              value={BigNumber.toNumber(result.leverage ?? BigNumber.ZERO)}
              onChange={n =>
                props.onLeverageChange(BigNumber.from(n.toFixed(2)))
              }
              min={BigNumber.toNumber(result.leverageRange[0])}
              max={BigNumber.toNumber(result.leverageRange[1])}
              segments={[
                { endingPosition: 0.08, endingValue: 2, labelText: "2" },
                { endingPosition: 0.22, endingValue: 5, labelText: "5" },
                { endingPosition: 0.36, endingValue: 10, labelText: "10" },
                { endingPosition: 0.5, endingValue: 20, labelText: "20" },
                { endingPosition: 0.64, endingValue: 30, labelText: "30" },
                { endingPosition: 0.78, endingValue: 50, labelText: "50" },
                { endingPosition: 0.92, endingValue: 150, labelText: "150" },
                {},
              ]}
            />
          )}
        </SpensorR>
      </View>

      <View>
        <InfoList
          direction={"row"}
          listItemDirection={"column"}
          renderInfoListItem={p => (
            <DefaultInfoListItem {...p} style={[p.style, { flex: 1 }]} />
          )}
          renderInfoListItemTitle={p => (
            <DefaultInfoListItemTitle
              {...p}
              style={[p.style, { flex: 1, marginBottom: spacing(2) }]}
            />
          )}
        >
          <InfoListItem>
            <InfoListItemTitle>
              <FieldLabel
                contentText={
                  <Spensor
                    fallback={$t<JSX.Element>(
                      defineMessage({
                        defaultMessage: "Price",
                        description: "TradeScreen/Market Order Creation/label",
                      }),
                    )}
                  >
                    {() =>
                      $t<JSX.Element>(
                        defineMessage({
                          defaultMessage: "Price",
                          description:
                            "TradeScreen/Market Order Creation/label",
                        }),
                      )
                    }
                  </Spensor>
                }
              />
            </InfoListItemTitle>

            <InfoListItemDetail>
              <FormattedNumberInput
                error={oneOf(OrderCreationFormErrorType.PriceIsEmpty)(
                  safeReadResource(props.formError)?.type,
                )}
                precision={
                  safeReadResource(props.quoteToken) != null
                    ? TokenInfo.getPrecision(safeReadResource(props.quoteToken))
                    : undefined
                }
                value={props.marketPrice}
                valueFormattedText={suspenseResource(() => (
                  <TokenCount
                    token={props.quoteToken}
                    count={readResource(props.marketPrice)!}
                  />
                ))}
              />
            </InfoListItemDetail>
          </InfoListItem>

          <InfoListItem style={{ marginLeft: spacing(2.5) }}>
            <InfoListItemTitle>
              <FieldLabel
                contentText={$t<JSX.Element>(
                  defineMessage({
                    defaultMessage:
                      "<tooltip>Tolerance</tooltip> <sub>(%)</sub>",
                    description: "TradeScreen/Market Order Creation/label",
                  }),
                  {
                    sub: content => <FieldLabelExtra contentText={content} />,
                    tooltip: content => (
                      <TooltippifiedText
                        dismissMethod={"hover-outside"}
                        content={
                          <FormattedMessageOutsideText>
                            {$t(
                              defineMessage({
                                defaultMessage:
                                  "<formula>Market Impact = {spread} Click to <learnMoreLink>Learn More</learnMoreLink></formula>" +
                                  "<desc>There may be slippage due to price move between tranasction submission and its confirmation.</desc>",
                                description:
                                  "TradeScreen/Market Order Creation/tolerance tooltip",
                              }),
                              {
                                spread: (
                                  <PercentNumber
                                    number={props.priceSpreadRate}
                                  />
                                ),
                                formula: content => (
                                  <TextInsideTooltip>
                                    {content}
                                  </TextInsideTooltip>
                                ),
                                desc: content => (
                                  <TextInsideTooltip marginTop={true}>
                                    {content}
                                  </TextInsideTooltip>
                                ),
                                learnMoreLink: content => (
                                  <HrefLink
                                    href={
                                      "https://docs.uniwhale.co/trading#tolerance-setting"
                                    }
                                  >
                                    <Text
                                      style={{
                                        color: colors("blue-600"),
                                        textDecorationLine: "underline",
                                      }}
                                    >
                                      {content}
                                    </Text>
                                  </HrefLink>
                                ),
                              },
                            )}
                          </FormattedMessageOutsideText>
                        }
                      >
                        {content}
                      </TooltippifiedText>
                    ),
                  },
                )}
              />
            </InfoListItemTitle>

            <InfoListItemDetail>
              <FormattedNumberInput
                error={oneOf(
                  OrderCreationFormErrorType.SlippageIsEmpty,
                  OrderCreationFormErrorType.SlippageOutOfRange,
                )(safeReadResource(props.formError)?.type)}
                valueFormattedText={suspenseResource(() => {
                  const slippage = readResource(props.slippagePercentage)
                  return slippage == null ? null : (
                    <TextNumber number={BigNumber.mul(slippage, 100)} />
                  )
                })}
                value={suspenseResource(() => {
                  const slippage = readResource(props.slippagePercentage)
                  return slippage == null ? null : BigNumber.mul(slippage, 100)
                })}
                onValueChange={n =>
                  props.onSlippageChange(
                    n == null ? null : BigNumber.div(n, 100),
                  )
                }
              />
            </InfoListItemDetail>
          </InfoListItem>
        </InfoList>

        <View
          style={{
            marginTop: spacing(2.5),
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <FormattedMessageOutsideText>
            {$t(
              defineMessage({
                defaultMessage: "{icon} <tooltip>Mark Price:</tooltip> {price}",
                description: "TradeScreen/Market Order Creation/label",
              }),
              {
                icon: (
                  <Svg
                    width="12"
                    height="12"
                    viewBox="0 0 12 12"
                    fill={colors("gray-500")}
                  >
                    <Path d="M3.75 0.75H11.25L8.25 3.75L11.25 6.75H3.75V0.75Z" />
                    <Rect x="1.5" y="0.75" width="1.5" height="10.5" />
                  </Svg>
                ),
                tooltip: content => (
                  <TooltippifiedText
                    style={{
                      marginHorizontal: spacing(1),
                    }}
                    textStyle={{
                      color: colors("gray-500"),
                      fontSize: 12,
                    }}
                    content={
                      <TextInsideTooltip>
                        {$t(
                          defineMessage({
                            defaultMessage:
                              'Mark Price is the latest mid price as reported by the Oracle, which is used as a reference point for "Liquidations" and "Unrealized PnL"',
                            description:
                              "TradeScreen/Market Order Creation/mark price tooltip",
                          }),
                        )}
                      </TextInsideTooltip>
                    }
                  >
                    {content}
                  </TooltippifiedText>
                ),
                price: (
                  <TokenCount
                    style={{ fontSize: 12 }}
                    color={colors("gray-500")}
                    token={props.quoteToken}
                    count={props.markPrice}
                  />
                ),
              },
            )}
          </FormattedMessageOutsideText>
        </View>

        <SpensorR read={{ entryPrice: props.entryPrice }}>
          {({ entryPrice }) =>
            entryPrice != null && (
              <Text
                style={{
                  color: colors("gray-500"),
                  fontSize: 12,
                }}
              >
                {$t(
                  defineMessage({
                    defaultMessage: "Entry Price: {price}",
                    description: "TradeScreen/Market Order Creation/label",
                  }),
                  {
                    price: (
                      <TokenCount token={props.quoteToken} count={entryPrice} />
                    ),
                  },
                )}
              </Text>
            )
          }
        </SpensorR>

        <SpensorR read={{ exitPrice: props.exitPrice }}>
          {({ exitPrice }) =>
            exitPrice != null && (
              <Text
                style={{
                  color: colors("gray-500"),
                  fontSize: 12,
                }}
              >
                {$t(
                  defineMessage({
                    defaultMessage: "Exit Price: {price}",
                    description: "TradeScreen/Market Order Creation/label",
                  }),
                  {
                    price: (
                      <TokenCount token={props.quoteToken} count={exitPrice} />
                    ),
                  },
                )}
              </Text>
            )
          }
        </SpensorR>
      </View>

      <Divider color={colors("gray-200")} />

      <InfoList direction={"column"} listItemDirection={"column"}>
        <Stack space={props.gap}>
          <InfoListItem>
            <InfoListItemTitle>
              <FieldLabel
                contentText={$t<JSX.Element>(
                  defineMessage({
                    defaultMessage:
                      "Stop Loss <extraContentStyle>({stopLossPrice})</extraContentStyle>",
                    description: "TradeScreen/Market Order Creation/label",
                  }),
                  {
                    stopLossPrice: (
                      <ActionTriggerConditionNumberDisplayingText
                        quoteToken={props.quoteToken}
                        trigger={props.stopLossTrigger}
                        percentageLimitInRange={percentageLimitInRange100}
                      />
                    ),
                    extraContentStyle: content => (
                      <FieldLabelExtra
                        style={{ color: Colors.lossColor }}
                        contentText={content}
                      />
                    ),
                  },
                )}
              />
            </InfoListItemTitle>

            <InfoListItemDetail style={{ marginTop: spacing(1) }}>
              <PercentageNumberControl
                error={oneOf(
                  OrderCreationFormErrorType.StopLossPriceOutOfRange,
                )(safeReadResource(props.formError)?.type)}
                percentageOptions={props.stopLossTriggerPercentageOptions}
                renderLabelNumber={n => (
                  <PercentNumber number={BigNumber.neg(n)} />
                )}
                trigger={props.stopLossTrigger}
                onTriggerUpdated={props.onStopLossTriggerChange}
                precision={
                  safeReadResource(props.quoteToken) != null
                    ? TokenInfo.getPrecision(safeReadResource(props.quoteToken))
                    : undefined
                }
              />
            </InfoListItemDetail>
          </InfoListItem>

          <InfoListItem>
            <InfoListItemTitle>
              <FieldLabel
                contentText={$t<JSX.Element>(
                  defineMessage({
                    defaultMessage:
                      "Take Profit <extraContentStyle>({takeProfitPrice})</extraContentStyle>",
                    description: "TradeScreen/Market Order Creation/label",
                  }),
                  {
                    takeProfitPrice: (
                      <ActionTriggerConditionNumberDisplayingText
                        quoteToken={props.quoteToken}
                        trigger={props.takeProfitTrigger}
                      />
                    ),
                    extraContentStyle: content => (
                      <FieldLabelExtra
                        style={{ color: Colors.profitColor }}
                        contentText={content}
                      />
                    ),
                  },
                )}
              />
            </InfoListItemTitle>

            <InfoListItemDetail style={{ marginTop: spacing(1) }}>
              <PercentageNumberControl
                error={oneOf(
                  OrderCreationFormErrorType.TakeProfitPriceOutOfRange,
                )(safeReadResource(props.formError)?.type)}
                percentageOptions={props.takeProfitTriggerPercentageOptions}
                renderLabelNumber={n => <PercentNumber number={n} />}
                trigger={props.takeProfitTrigger}
                onTriggerUpdated={props.onTakeProfitTriggerChange}
                precision={
                  safeReadResource(props.quoteToken) != null
                    ? TokenInfo.getPrecision(safeReadResource(props.quoteToken))
                    : undefined
                }
              />
            </InfoListItemDetail>
          </InfoListItem>
        </Stack>
      </InfoList>

      <Divider color={colors("gray-200")} />
    </Stack>
  )
}
