import { noop } from "../../../../utils/fnHelpers"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { MarketOrderCreationContentProps } from "./MarketOrderCreationContent"

export const marketOrderCreationContentProps: MarketOrderCreationContentProps =
  {
    baseToken: TokenInfoPresets.MockBTC,
    quoteToken: TokenInfoPresets.MockUSDC,
    availableQuoteTokens: [
      TokenInfoPresets.MockUSDC,
      TokenInfoPresets.MockBUSD,
    ],
    onSelectQuoteToken: noop,

    quoteTokenBalance: BigNumber.from(136.3),

    leverageRange: [BigNumber.from(1.5), BigNumber.from(200)],
    leverage: BigNumber.from(2),
    onLeverageChange: noop,

    priceSpreadRate: BigNumber.from(0.0012),
    marketPrice: BigNumber.from(1_541.8),
    markPrice: BigNumber.from(1_522.8),

    slippagePercentage: BigNumber.from(0.01),
    onSlippageChange: noop,

    stopLossTriggerPercentageOptions: [
      { value: null },
      { value: BigNumber.from(0.1) },
      { value: BigNumber.from(0.25) },
      { value: BigNumber.from(0.5) },
      { value: BigNumber.from(0.75) },
    ],
    stopLossTrigger: null,
    onStopLossTriggerChange: noop,

    takeProfitTriggerPercentageOptions: [
      { value: BigNumber.from(0.25) },
      { value: BigNumber.from(0.5) },
      { value: BigNumber.from(0.75) },
      { value: BigNumber.from(1) },
      { value: BigNumber.from(9) },
    ],
    takeProfitTrigger: {
      sourceType: "percentage",
      percentage: BigNumber.from(0.5),
      price: BigNumber.from(888.8),
    },
    onTakeProfitTriggerChange: noop,

    value: null,
    onValueChange: noop,
    onPressMax: noop,
    entryPrice: BigNumber.from(0.123),
    exitPrice: BigNumber.from(1),
  }
