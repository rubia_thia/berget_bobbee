import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"

export interface ActionTrigger {
  sourceType: "percentage" | "price"
  percentage: BigNumber
  price: BigNumber
}

export type ActionTriggerPatch =
  | ActionTriggerPatch.Percentage
  | ActionTriggerPatch.Price
export namespace ActionTriggerPatch {
  export interface Price {
    sourceType: "price"
    price: BigNumber
  }

  export interface Percentage {
    sourceType: "percentage"
    percentage: BigNumber
  }
}

export enum OrderCreationFormErrorType {
  ConnectWalletRequired = "ConnectWalletRequired",
  ConnectedToTheWrongNetwork = "ConnectedToTheWrongNetwork",
  MarginAmountIsEmpty = "MarginAmountIsEmpty",
  PositionIsTooMall = "PositionIsTooMall",
  InsufficientQuoteTokenBalance = "InsufficientQuoteTokenBalance",
  PriceIsEmpty = "PriceIsEmpty",
  SlippageIsEmpty = "SlippageIsEmpty",
  SlippageOutOfRange = "SlippageOutOfRange",
  LeverageIsEmpty = "LeverageIsEmpty",
  LeverageOutOfRange = "LeverageOutOfRange",
  StopLossPriceOutOfRange = "StopLossPriceOutOfRange",
  TakeProfitPriceOutOfRange = "TakeProfitPriceOutOfRange",
  PythPriceOutOfDate = "PythPriceOutOfDate",
}

export type OrderCreationFormError =
  | OrderCreationFormError.Common
  | OrderCreationFormError.LeverageOutOfRange
  | OrderCreationFormError.SlippageOutOfRange
  | OrderCreationFormError.PositionOutOfRange

export namespace OrderCreationFormError {
  export interface Common {
    type: Exclude<
      OrderCreationFormErrorType,
      | OrderCreationFormErrorType.LeverageOutOfRange
      | OrderCreationFormErrorType.SlippageOutOfRange
      | OrderCreationFormErrorType.PositionIsTooMall
    >
  }

  export interface LeverageOutOfRange {
    type: OrderCreationFormErrorType.LeverageOutOfRange
    min: BigNumber
    max: BigNumber
  }

  export interface SlippageOutOfRange {
    type: OrderCreationFormErrorType.SlippageOutOfRange
    min: BigNumber
    max: BigNumber
  }

  export interface PositionOutOfRange {
    type: OrderCreationFormErrorType.PositionIsTooMall
    minPosition: BigNumber
  }
}

export interface ActionTriggerPercentageOption {
  value: null | BigNumber
  disabled?: boolean
}
