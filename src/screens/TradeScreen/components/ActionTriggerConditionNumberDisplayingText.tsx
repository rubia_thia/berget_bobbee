import { FC } from "react"
import { useIntl } from "react-intl"
import { StyleProp, Text, TextStyle } from "react-native"
import { PercentNumber } from "../../../components/PercentNumber"
import { SpensorR } from "../../../components/Spensor"
import { TokenCount } from "../../../components/TokenCount"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { getPrecisionFloor } from "../../../utils/numberHelpers/getPrecisionFloor"
import {
  safeReadResource,
  SuspenseResource,
} from "../../../utils/SuspenseResource"
import { TokenInfo } from "../../../utils/TokenInfo"
import { ActionTrigger } from "./OrderCreation/types"
import { specialPriceNone$t } from "./_/commonIntlMessages"

export const percentageLimitInRange100 = [
  BigNumber.ZERO,
  BigNumber.from(1),
] as const

export const ActionTriggerConditionNumberDisplayingText: FC<{
  style?: StyleProp<TextStyle>

  quoteToken: SuspenseResource<TokenInfo>

  trigger: SuspenseResource<null | ActionTrigger>

  percentageLimitInRange?: readonly [min: BigNumber, max: BigNumber]
}> = props => {
  const { $t } = useIntl()

  const tokenPrecisionFloor = getPrecisionFloor(
    TokenInfo.getPrecision(safeReadResource(props.quoteToken)),
  )

  return (
    <SpensorR read={{ trigger: props.trigger }} fallback={"-"}>
      {({ trigger }) => {
        if (trigger === null) {
          return <Text style={props.style}>{$t(specialPriceNone$t)}</Text>
        }

        if (trigger.sourceType === "percentage") {
          return BigNumber.isLt(trigger.price, tokenPrecisionFloor) ? (
            <>
              &lt;
              <TokenCount
                style={props.style}
                token={props.quoteToken}
                count={tokenPrecisionFloor}
              />
            </>
          ) : (
            <TokenCount
              style={props.style}
              token={props.quoteToken}
              count={trigger.price}
            />
          )
        }

        if (props.percentageLimitInRange != null) {
          if (
            BigNumber.isLt(trigger.percentage, props.percentageLimitInRange[0])
          ) {
            return (
              <>
                &lt;
                <PercentNumber
                  style={props.style}
                  number={props.percentageLimitInRange[0]}
                />
              </>
            )
          }

          if (
            BigNumber.isGt(trigger.percentage, props.percentageLimitInRange[1])
          ) {
            return (
              <>
                &gt;
                <PercentNumber
                  style={props.style}
                  number={props.percentageLimitInRange[1]}
                />
              </>
            )
          }
        }

        return <PercentNumber style={props.style} number={trigger.percentage} />
      }}
    </SpensorR>
  )
}
