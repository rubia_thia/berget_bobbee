import { ComponentMeta, Story } from "@storybook/react"
import { useState } from "react"
import {
  BackgroundColor,
  CardContainer,
} from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { TextNumber } from "../../../components/TextNumber"
import { noop } from "../../../utils/fnHelpers"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { FormattedNumberInput } from "./FormattedNumberInput"

export default {
  title: "Page/TradeScreen/FormattedNumberInput",
  component: FormattedNumberInput,
  decorators: [CardContainer({ margin: 10, padding: 10 }), BackgroundColor()],
} as ComponentMeta<typeof FormattedNumberInput>

const tpl = withTemplate(FormattedNumberInput, {
  placeholder: "Price",
  valueFormattedText: "100%",
  value: BigNumber.from(1),
  onValueChange: noop,
})

export const Normal: Story = () => {
  const [value, setValue] = useState<null | BigNumber>(null)

  return (
    <FormattedNumberInput
      placeholder="Price"
      valueFormattedText={
        value == null ? null : (
          <>
            <TextNumber number={value} />x
          </>
        )
      }
      value={value}
      onValueChange={setValue}
    />
  )
}

export const Readonly = tpl(p => {
  p.onValueChange = undefined
})

export const Error = tpl(p => {
  p.error = true
})
