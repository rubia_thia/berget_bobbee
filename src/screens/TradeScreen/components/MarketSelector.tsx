import { StyleProp, ViewStyle } from "react-native"
import { PlainIconButtonVariantLayout } from "../../../components/Button/PlainIconButtonVariant"
import { CardBoxView } from "../../../components/CardBox/CardBox"
import {
  DropdownMenu,
  DropdownMenuItem,
} from "../../../components/DropdownMenu"
import { TokenPair } from "../types"
import { TokenPairName } from "./TokenPairName"

export const MarketSelector = <T extends string>({
  style,
  tokenParis,
  onSelect,
}: {
  style?: StyleProp<ViewStyle>
  tokenParis: { pair: TokenPair; value: T }[]
  onSelect: (value: T) => void
}): JSX.Element => {
  return (
    <CardBoxView style={style}>
      <DropdownMenu>
        {tokenParis.map(({ pair, value }) => {
          return (
            <DropdownMenuItem key={value} onPress={() => onSelect(value)}>
              <PlainIconButtonVariantLayout>
                <TokenPairName
                  className="font-medium text-center text-gray-900"
                  pair={pair}
                />
              </PlainIconButtonVariantLayout>
            </DropdownMenuItem>
          )
        })}
      </DropdownMenu>
    </CardBoxView>
  )
}
