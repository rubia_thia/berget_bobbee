export enum EditMarginModalFormErrorType {
  EnterAmount = "EnterAmount",
  InsufficientQuoteTokenBalance = "InsufficientQuoteTokenBalance",
  LeverageTooHigh = "LeverageTooHigh",
  ExceedMargin = "ExceedMargin",
  LeverageTooLow = "LeverageTooLow",
}

export type EditMarginModalFormError = {
  type: EditMarginModalFormErrorType
}
