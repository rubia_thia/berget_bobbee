import { ReactNode } from "react"
import { defineMessage, IntlShape } from "react-intl"
import { PercentNumber } from "../../../components/PercentNumber"
import { TokenCountAsCurrency } from "../../../components/TextTokenCount"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { TokenInfo } from "../../../utils/TokenInfo"
import { checkNever } from "../../../utils/typeHelpers"

export enum UpdateSLTPTriggerModalFormErrorType {
  TakeProfitPercentageOutOfRange = "TakeProfitPercentageOutOfRange",
  StopLossPercentageOutOfRange = "StopLossPercentageOutOfRange",
  StopLossShouldGteLiquidationPrice = "StopLossShouldGteLiquidationPrice",
  StopLossShouldLteLiquidationPrice = "StopLossShouldLteLiquidationPrice",
}

export type UpdateSLTPTriggerModalFormError =
  | {
      type: Exclude<
        UpdateSLTPTriggerModalFormErrorType,
        | UpdateSLTPTriggerModalFormErrorType.TakeProfitPercentageOutOfRange
        | UpdateSLTPTriggerModalFormErrorType.StopLossPercentageOutOfRange
      >
    }
  | UpdateSLTPTriggerModalFormError.StopLossPercentageOutOfRange
  | UpdateSLTPTriggerModalFormError.TakeProfitPriceOutOfRange
export namespace UpdateSLTPTriggerModalFormError {
  export interface StopLossPercentageOutOfRange {
    type: UpdateSLTPTriggerModalFormErrorType.StopLossPercentageOutOfRange
    maxStopLossPercentage: BigNumber
    minStopLossPercentage: BigNumber
  }

  export interface TakeProfitPriceOutOfRange {
    type: UpdateSLTPTriggerModalFormErrorType.TakeProfitPercentageOutOfRange
    maxTakeProfitPercentage: BigNumber
    minTakeProfitPercentage: BigNumber
  }
}

export namespace updateSLTPTriggerModalFormErrorMessages$t {
  export const fromError = (
    intl: IntlShape,
    error: UpdateSLTPTriggerModalFormError,
    quoteToken: TokenInfo,
    liquidationPrice: BigNumber,
  ): ReactNode => {
    switch (error.type) {
      case UpdateSLTPTriggerModalFormErrorType.StopLossShouldGteLiquidationPrice:
        return stopLossShouldGteLiquidationPrice(
          intl,
          quoteToken,
          liquidationPrice,
        )
      case UpdateSLTPTriggerModalFormErrorType.StopLossShouldLteLiquidationPrice:
        return stopLossShouldLteLiquidationPrice(
          intl,
          quoteToken,
          liquidationPrice,
        )
      case UpdateSLTPTriggerModalFormErrorType.StopLossPercentageOutOfRange:
        return stopLossPercentageOutOfRange(intl, quoteToken, error)
      case UpdateSLTPTriggerModalFormErrorType.TakeProfitPercentageOutOfRange:
        return takeProfitPercentageOutOfRange(intl, quoteToken, error)
      default:
        checkNever(error)
        return null
    }
  }

  export const stopLossShouldGteLiquidationPrice = (
    intl: IntlShape,
    quoteToken: TokenInfo,
    liquidationPrice: BigNumber,
  ): ReactNode => {
    return intl.$t<JSX.Element>(
      defineMessage({
        defaultMessage:
          "Stop loss price should be greater than or equal to liquidation price ({liquidationPrice})",
        description: "TradeScreen/UpdateSLTPTriggerModalContent/error message",
      }),
      {
        liquidationPrice: (
          <TokenCountAsCurrency token={quoteToken} count={liquidationPrice} />
        ),
      },
    )
  }

  export const stopLossShouldLteLiquidationPrice = (
    intl: IntlShape,
    quoteToken: TokenInfo,
    liquidationPrice: BigNumber,
  ): ReactNode => {
    return intl.$t<JSX.Element>(
      defineMessage({
        defaultMessage:
          "Stop loss price should be less than or equal to liquidation price ({liquidationPrice})",
        description: "TradeScreen/UpdateSLTPTriggerModalContent/error message",
      }),
      {
        liquidationPrice: (
          <TokenCountAsCurrency token={quoteToken} count={liquidationPrice} />
        ),
      },
    )
  }

  export const stopLossPercentageOutOfRange = (
    intl: IntlShape,
    quoteToken: TokenInfo,
    err: UpdateSLTPTriggerModalFormError.StopLossPercentageOutOfRange,
  ): ReactNode => {
    return intl.$t<JSX.Element>(
      defineMessage({
        defaultMessage:
          "Stop loss percentage should be between {min} and {max}",
        description: "TradeScreen/UpdateSLTPTriggerModalContent/error message",
      }),
      {
        min: <PercentNumber number={err.minStopLossPercentage} />,
        max: <PercentNumber number={err.maxStopLossPercentage} />,
      },
    )
  }

  export const takeProfitPercentageOutOfRange = (
    intl: IntlShape,
    quoteToken: TokenInfo,
    err: UpdateSLTPTriggerModalFormError.TakeProfitPriceOutOfRange,
  ): ReactNode => {
    return intl.$t<JSX.Element>(
      defineMessage({
        defaultMessage:
          "Take profit percentage should be between {min} and {max}",
        description: "TradeScreen/UpdateSLTPTriggerModalContent/error message",
      }),
      {
        min: <PercentNumber number={err.minTakeProfitPercentage} />,
        max: <PercentNumber number={err.maxTakeProfitPercentage} />,
      },
    )
  }
}
