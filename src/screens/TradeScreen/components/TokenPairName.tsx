import { FC } from "react"
import { StyleProp, Text, TextStyle } from "react-native"
import { TokenName } from "../../../components/TokenName"
import { TokenPair } from "../types"

export interface TokenPairNameProps {
  style?: StyleProp<TextStyle>
  pair: TokenPair
}

export const TokenPairName: FC<TokenPairNameProps> = props => {
  return (
    <Text style={props.style}>
      <TokenName token={props.pair.baseToken} />/
      <TokenName token={props.pair.quoteToken} />
    </Text>
  )
}
