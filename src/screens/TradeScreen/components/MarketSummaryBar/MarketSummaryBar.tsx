import { FormatXMLElementFn } from "intl-messageformat/src/formatters"
import { FC, ReactNode } from "react"
import { defineMessage, useIntl } from "react-intl"
import { ScrollView, StyleProp, Text, ViewStyle } from "react-native"
import { CardBoxView } from "../../../../components/CardBox/CardBox"
import { Divider } from "../../../../components/Divider"
import { InfoList } from "../../../../components/InfoList/InfoList"
import { InfoListItem } from "../../../../components/InfoList/InfoListItem"
import {
  InfoListItemDetail,
  InfoListItemDetailText,
} from "../../../../components/InfoList/InfoListItemDetail"
import {
  InfoListItemTitle,
  InfoListItemTitleText,
} from "../../../../components/InfoList/InfoListItemTitle"
import { OutlineDropdownTrigger } from "../../../../components/OutlineDropdownTrigger/OutlineDropdownTrigger"
import { PercentNumber } from "../../../../components/PercentNumber"
import {
  PopoverContent,
  PopoverRoot,
  PopoverTrigger,
} from "../../../../components/Popover/Popover"
import { PopoverContentChildrenRenderer } from "../../../../components/Popover/_/PopoverContent"
import { SimplifiedNumber } from "../../../../components/SimplifiedNumber"
import { Spensor } from "../../../../components/Spensor"
import { Stack } from "../../../../components/Stack"
import { useColors } from "../../../../components/Themed/color"
import { useSpacing } from "../../../../components/Themed/spacing"
import { TokenCount } from "../../../../components/TokenCount"
import {
  TextInsideTooltip,
  TextWithTooltippifiedStyle,
  Tooltip,
} from "../../../../components/Tooltip"
import { FormattedMessageOutsideText } from "../../../../utils/intlHelpers"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import {
  mergeStyle,
  withProps,
} from "../../../../utils/reactHelpers/withProps/withProps"
import {
  readResource,
  safeReadResource,
  suspenseResource,
  SuspenseResource,
} from "../../../../utils/SuspenseResource"
import { PriceChangeDirection, TokenPair } from "../../types"
import { TokenPairName } from "../TokenPairName"
import { Colors } from "../_/designTokens"

const infoListItemDetailTextFontSize = 14
const StyledInfoListItemDetailText = withProps(
  mergeStyle({ fontSize: infoListItemDetailTextFontSize }),
  InfoListItemDetailText,
)

export interface MarketSummaryBarProps {
  style?: StyleProp<ViewStyle>
  tokenSelector: ReactNode | PopoverContentChildrenRenderer
  tokenPair: TokenPair
  currentPrice: SuspenseResource<BigNumber>
  currentPriceChangeDirection: SuspenseResource<PriceChangeDirection>
  priceChangeIn24Hours?: SuspenseResource<{
    priceDelta: BigNumber
    priceDeltaPercentage: BigNumber
  }>
  priceLowestIn24Hours?: SuspenseResource<BigNumber>
  priceHighestIn24Hours?: SuspenseResource<BigNumber>
  openInterest?: {
    long: SuspenseResource<BigNumber>
    short: SuspenseResource<BigNumber>
    total: SuspenseResource<BigNumber>
  }
  openLong?: SuspenseResource<BigNumber>
  maxLong?: SuspenseResource<BigNumber>
  openShort?: SuspenseResource<BigNumber>
  maxShort?: SuspenseResource<BigNumber>
  fundingLongHourlyRate?: SuspenseResource<BigNumber>
  fundingShortHourlyRate?: SuspenseResource<BigNumber>
  rolloverRate?: SuspenseResource<BigNumber>
}

export const MarketSummaryBar: FC<MarketSummaryBarProps> = props => {
  const spacing = useSpacing()
  const color = useColors()
  const { $t } = useIntl()

  return (
    <CardBoxView
      style={[
        props.style,
        {
          flexDirection: "row",
          paddingHorizontal: spacing(2.5),
          paddingVertical: spacing(2),
        },
      ]}
    >
      <Stack space={spacing(4)} align={"center"} horizontal>
        <PopoverRoot triggerMethod="press" dismissMethod={"press-outside"}>
          <PopoverTrigger>
            <OutlineDropdownTrigger>
              <TokenPairName
                style={{
                  color: color("gray-900"),
                  fontSize: 16,
                  fontWeight: "600",
                }}
                pair={props.tokenPair}
              />
            </OutlineDropdownTrigger>
          </PopoverTrigger>
          <PopoverContent>{props.tokenSelector}</PopoverContent>
        </PopoverRoot>
        <TokenCount
          style={{ fontSize: 16 }}
          color={Colors.fromPriceChangeDirection(
            safeReadResource(props.currentPriceChangeDirection) ??
              PriceChangeDirection.Keep,
          )}
          token={props.tokenPair.quoteToken}
          count={props.currentPrice}
          padDecimals={true}
        />
      </Stack>

      <Divider style={{ marginLeft: spacing(6) }} direction={"vertical"} />

      <ScrollView
        style={{ flex: 1 }}
        contentContainerStyle={{ paddingLeft: spacing(6) }}
        horizontal={true}
      >
        <InfoList
          style={{ justifyContent: "center" }}
          listItemDirection={"column"}
        >
          <Stack space={spacing(4)} align={"center"} horizontal>
            {props.priceChangeIn24Hours && (
              <Spensor
                fallback={
                  <PriceChangeIn24HoursCell
                    tokenPair={props.tokenPair}
                    priceChangeIn24Hours={{
                      priceDelta: BigNumber.ZERO,
                      priceDeltaPercentage: BigNumber.ZERO,
                    }}
                  />
                }
              >
                {() => (
                  <PriceChangeIn24HoursCell
                    tokenPair={props.tokenPair}
                    priceChangeIn24Hours={readResource(
                      props.priceChangeIn24Hours!,
                    )}
                  />
                )}
              </Spensor>
            )}

            {props.priceHighestIn24Hours != null && (
              <InfoListItem>
                <InfoListItemTitle>
                  {$t(
                    defineMessage({
                      defaultMessage: "24h High",
                      description: "trade screen/summary bar",
                    }),
                  )}
                </InfoListItemTitle>
                <InfoListItemDetail>
                  <StyledInfoListItemDetailText>
                    <TokenCount
                      token={props.tokenPair.quoteToken}
                      count={props.priceHighestIn24Hours}
                      padDecimals={true}
                    />
                  </StyledInfoListItemDetailText>
                </InfoListItemDetail>
              </InfoListItem>
            )}

            {props.priceLowestIn24Hours != null && (
              <InfoListItem>
                <InfoListItemTitle>
                  {$t(
                    defineMessage({
                      defaultMessage: "24h Low",
                      description: "trade screen/summary bar",
                    }),
                  )}
                </InfoListItemTitle>
                <InfoListItemDetail>
                  <StyledInfoListItemDetailText>
                    <TokenCount
                      token={props.tokenPair.quoteToken}
                      count={props.priceLowestIn24Hours}
                      padDecimals={true}
                    />
                  </StyledInfoListItemDetailText>
                </InfoListItemDetail>
              </InfoListItem>
            )}

            {props.openInterest != null && (
              <InfoListItem>
                <InfoListItemTitle>
                  {$t(
                    defineMessage({
                      defaultMessage: "Open Interest (L)",
                      description: "trade screen/summary bar",
                    }),
                  )}
                </InfoListItemTitle>
                <InfoListItemDetail>
                  <StyledInfoListItemDetailText>
                    <SimplifiedNumber number={props.openInterest.long} />/
                    <SimplifiedNumber number={props.openInterest.total} />
                  </StyledInfoListItemDetailText>
                </InfoListItemDetail>
              </InfoListItem>
            )}

            {props.openInterest != null && (
              <InfoListItem>
                <InfoListItemTitle>
                  <InfoListItemTitleText>
                    {$t(
                      defineMessage({
                        defaultMessage: "Open Interest (S)",
                        description: "trade screen/summary bar",
                      }),
                    )}
                  </InfoListItemTitleText>
                </InfoListItemTitle>
                <InfoListItemDetail>
                  <StyledInfoListItemDetailText>
                    <SimplifiedNumber number={props.openInterest.short} />/
                    <SimplifiedNumber number={props.openInterest.total} />
                  </StyledInfoListItemDetailText>
                </InfoListItemDetail>
              </InfoListItem>
            )}

            {props.openLong != null && (
              <InfoListItem>
                <InfoListItemTitle>
                  <InfoListItemTitleText>
                    {$t(
                      defineMessage({
                        defaultMessage: "Open Long",
                        description: "trade screen/summary bar",
                      }),
                    )}
                  </InfoListItemTitleText>
                </InfoListItemTitle>
                <InfoListItemDetail>
                  <StyledInfoListItemDetailText>
                    <SimplifiedNumber number={props.openLong} />
                    {props.maxLong != null && (
                      <Text>
                        {" "}
                        / <SimplifiedNumber number={props.maxLong} />
                      </Text>
                    )}
                  </StyledInfoListItemDetailText>
                </InfoListItemDetail>
              </InfoListItem>
            )}
            {props.openShort != null && (
              <InfoListItem>
                <InfoListItemTitle>
                  <InfoListItemTitleText>
                    {$t(
                      defineMessage({
                        defaultMessage: "Open Short",
                        description: "trade screen/summary bar",
                      }),
                    )}
                  </InfoListItemTitleText>
                </InfoListItemTitle>
                <InfoListItemDetail>
                  <StyledInfoListItemDetailText>
                    <SimplifiedNumber number={props.openShort} />
                    {props.maxShort != null && (
                      <Text>
                        {" "}
                        / <SimplifiedNumber number={props.maxShort} />
                      </Text>
                    )}
                  </StyledInfoListItemDetailText>
                </InfoListItemDetail>
              </InfoListItem>
            )}
            {props.fundingLongHourlyRate != null && (
              <FeeRateDisplayCell
                title={$t(
                  defineMessage({
                    defaultMessage: "Funding (l)",
                    description: "trade screen/summary bar",
                  }),
                )}
                hoverContentText={
                  <FormattedMessageOutsideText>
                    {$t<JSX.Element>(
                      defineMessage({
                        defaultMessage:
                          "<desc>Funding Fee (longs) hourly rate, charged per block on the position size.</desc>" +
                          "<legend><green>Green</green> = reward, <red>Red</red> = fee</legend>",
                        description: "trade screen/summary bar",
                      }),
                      { ...feeRateCommonStylishOptionValues },
                    )}
                  </FormattedMessageOutsideText>
                }
                rate={props.fundingLongHourlyRate}
                ratePrecision={4}
              />
            )}
            {props.fundingShortHourlyRate != null && (
              <FeeRateDisplayCell
                title={$t(
                  defineMessage({
                    defaultMessage: "Funding (s)",
                    description: "trade screen/summary bar",
                  }),
                )}
                hoverContentText={
                  <FormattedMessageOutsideText>
                    {$t<JSX.Element>(
                      defineMessage({
                        defaultMessage:
                          "<desc>Funding Fee (short) hourly rate, charged per block on the position size.</desc>" +
                          "<legend><green>Green</green> = reward, <red>Red</red> = fee</legend>",
                        description: "trade screen/summary bar",
                      }),
                      { ...feeRateCommonStylishOptionValues },
                    )}
                  </FormattedMessageOutsideText>
                }
                rate={props.fundingShortHourlyRate}
                ratePrecision={4}
              />
            )}
            {props.rolloverRate != null && (
              <FeeRateDisplayCell
                title={$t(
                  defineMessage({
                    defaultMessage: "Rollover Fee",
                    description: "trade screen/summary bar",
                  }),
                )}
                hoverContentText={
                  <FormattedMessageOutsideText>
                    {$t<JSX.Element>(
                      defineMessage({
                        defaultMessage:
                          "<desc>Rollover Fee hourly rate, charged per block on the collateral only.</desc>",
                        description: "trade screen/summary bar",
                      }),
                      { ...feeRateCommonStylishOptionValues },
                    )}
                  </FormattedMessageOutsideText>
                }
                rate={props.rolloverRate}
                ratePrecision={4}
              />
            )}
          </Stack>
        </InfoList>
      </ScrollView>
    </CardBoxView>
  )
}

const feeRateCommonStylishOptionValues: Record<
  string,
  FormatXMLElementFn<JSX.Element>
> = {
  green: content => (
    <Text style={{ color: Colors.increaseColor }}>{content}</Text>
  ),
  red: content => (
    <Text style={{ color: Colors.decreaseColor }}>{content}</Text>
  ),
  desc: content => <TextInsideTooltip>{content}</TextInsideTooltip>,
  legend: content => (
    <TextInsideTooltip marginTop={true}>{content}</TextInsideTooltip>
  ),
}

const FeeRateDisplayCell: FC<{
  title: string
  rate: SuspenseResource<BigNumber>
  ratePrecision?: number
  hoverContentText: ReactNode
  reverseColor?: boolean
}> = props => {
  const currentRate = safeReadResource(props.rate)
  return (
    <Tooltip content={props.hoverContentText}>
      <InfoListItem>
        <InfoListItemTitle>
          <InfoListItemTitleText>
            <TextWithTooltippifiedStyle>
              {props.title}
            </TextWithTooltippifiedStyle>
          </InfoListItemTitleText>
        </InfoListItemTitle>

        <InfoListItemDetail>
          <StyledInfoListItemDetailText
            style={{
              color:
                currentRate != null && BigNumber.isNegative(currentRate)
                  ? Colors.profitColor
                  : Colors.lossColor,
            }}
          >
            <PercentNumber
              number={suspenseResource(() =>
                BigNumber.abs(readResource(props.rate)),
              )}
              precision={props.ratePrecision}
              padDecimals={true}
            />
          </StyledInfoListItemDetailText>
        </InfoListItemDetail>
      </InfoListItem>
    </Tooltip>
  )
}

const PriceChangeIn24HoursCell: FC<{
  tokenPair: TokenPair
  priceChangeIn24Hours: {
    priceDelta: BigNumber
    priceDeltaPercentage: BigNumber
  }
}> = props => {
  const { $t } = useIntl()

  return (
    <InfoListItem>
      <InfoListItemTitle>
        {$t(
          defineMessage({
            defaultMessage: "24h Change",
            description: "trade screen/summary bar",
          }),
        )}
      </InfoListItemTitle>
      <InfoListItemDetail>
        <StyledInfoListItemDetailText
          style={{
            color: BigNumber.isNegative(
              props.priceChangeIn24Hours.priceDeltaPercentage,
            )
              ? Colors.decreaseColor
              : Colors.increaseColor,
          }}
        >
          <TokenCount
            token={props.tokenPair.quoteToken}
            count={props.priceChangeIn24Hours.priceDelta}
            padDecimals={true}
          />
          &nbsp;
          <PercentNumber
            number={props.priceChangeIn24Hours.priceDeltaPercentage}
            padDecimals={true}
          />
        </StyledInfoListItemDetailText>
      </InfoListItemDetail>
    </InfoListItem>
  )
}
