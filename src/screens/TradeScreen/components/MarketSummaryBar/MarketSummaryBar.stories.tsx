import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { MarketSummaryBar } from "./MarketSummaryBar"
import { marketSummaryBarProps } from "./MarketSummaryBar.mockData"

export default {
  title: "Page/TradeScreen/MarketSummaryBar",
  component: MarketSummaryBar,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof MarketSummaryBar>

const template = withTemplate(MarketSummaryBar, {
  style: { margin: 10, alignSelf: "stretch" },
  ...marketSummaryBarProps,
})

export const Normal = template()
