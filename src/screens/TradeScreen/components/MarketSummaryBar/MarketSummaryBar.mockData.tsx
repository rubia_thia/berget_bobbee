import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { PriceChangeDirection } from "../../types"
import { MarketSummaryBarProps } from "./MarketSummaryBar"

export const marketSummaryBarProps: MarketSummaryBarProps = {
  tokenSelector: null,
  tokenPair: {
    baseToken: TokenInfoPresets.MockBUSD,
    quoteToken: TokenInfoPresets.MockUSDC,
  },
  currentPrice: BigNumber.from(1213.13),
  currentPriceChangeDirection: PriceChangeDirection.Down,
  priceChangeIn24Hours: {
    priceDelta: BigNumber.from(0.0032),
    priceDeltaPercentage: BigNumber.from(-0.0095),
  },
  priceLowestIn24Hours: BigNumber.from(1013.13),
  priceHighestIn24Hours: BigNumber.from(1513.13),
  openInterest: {
    long: BigNumber.from(3.4 * 1_000_000),
    short: BigNumber.from(692.8 * 1000),
    total: BigNumber.from(3.4 * 1_000_000 + 692.8 * 1000),
  },
}
