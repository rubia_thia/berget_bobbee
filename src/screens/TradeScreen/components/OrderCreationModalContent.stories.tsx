import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { noop } from "../../../utils/fnHelpers"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../utils/TokenInfoPresets/TokenInfoPresets"
import { OrderDirection, OrderType } from "../types"
import { OrderCreationModalContent } from "./OrderCreationModalContent"

export default {
  title: "Page/TradeScreen/OrderCreationModalContent",
  component: OrderCreationModalContent,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof OrderCreationModalContent>

const tpl = withTemplate(OrderCreationModalContent, {
  style: { margin: 10 },
  onCancel: noop,
  onSubmit: noop,

  baseToken: TokenInfoPresets.MockBTC,
  quoteToken: TokenInfoPresets.MockUSDC,
  orderType: OrderType.Limit,
  orderDirection: OrderDirection.Long,

  margin: BigNumber.from(100),
  marginEstimatedUSD: BigNumber.from(106.8888),
  baseTokenPositionSize: BigNumber.from(1.473),
  baseTokenPositionSizeEstimatedUSD: BigNumber.from(23568.8332472139),

  leverage: BigNumber.from(11.111111),
  liquidationPrice: BigNumber.from(20867.12),
  entryPrice: BigNumber.from(20867.12),
  spreadRate: BigNumber.from(0.0001),
  allowedSlippage: BigNumber.from(0.0001),
  fees: BigNumber.from(0.0001),
})

export const Long = tpl()

export const Short = tpl(p => {
  p.orderDirection = OrderDirection.Short
})
