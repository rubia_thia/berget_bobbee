import { ComponentMeta, Story } from "@storybook/react"
import { useState } from "react"
import {
  BackgroundColor,
  CardContainer,
} from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { noop } from "../../../utils/fnHelpers"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../utils/TokenInfoPresets/TokenInfoPresets"
import { LeftAreaLabelText, OneLineTokenInput } from "./OneLineTokenInput"

export default {
  title: "Page/TradeScreen/OneLineTokenInput",
  component: OneLineTokenInput,
  decorators: [CardContainer({ margin: 10, padding: 10 }), BackgroundColor()],
} as ComponentMeta<typeof OneLineTokenInput>

const tpl = withTemplate(OneLineTokenInput, {
  leftArea: <LeftAreaLabelText>Position</LeftAreaLabelText>,
  token: TokenInfoPresets.MockUSDC,
  count: BigNumber.from(1),
  onCountChange: noop,
})

export const Normal: Story = () => {
  const [value, setValue] = useState<null | BigNumber>(BigNumber.from(1))

  return (
    <OneLineTokenInput
      leftArea={<LeftAreaLabelText>Position</LeftAreaLabelText>}
      token={TokenInfoPresets.MockUSDC}
      count={value}
      onCountChange={setValue}
    />
  )
}

export const Readonly = tpl(p => {
  p.onCountChange = undefined
})

export const Error = tpl(p => {
  p.error = true
})
