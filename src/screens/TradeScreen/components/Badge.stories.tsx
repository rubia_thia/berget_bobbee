import { ComponentMeta } from "@storybook/react"
import {
  BackgroundColor,
  CardContainer,
} from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { Badge, RedBadge } from "./Badge"

export default {
  title: "Page/TradeScreen/Badge",
  component: Badge,
  decorators: [CardContainer({ margin: 10, padding: 10 }), BackgroundColor()],
} as ComponentMeta<typeof Badge>

const tpl = withTemplate(Badge, {
  text: "hello",
  textColor: "#22c55e",
  backgroundColor: "#dcfce7",
})

export const Normal = tpl()

export const Red = withTemplate(RedBadge, {
  text: "hello",
})()
