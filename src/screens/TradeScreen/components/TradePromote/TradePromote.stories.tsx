import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { TradePromote } from "./TradePromote"

export default {
  title: "Page/TradeScreen/TradePromote",
  component: TradePromote,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof TradePromote>

const tpl = withTemplate(TradePromote, {
  style: { margin: 10 },
  emissionTokenInfo: TokenInfoPresets.UNW,
  detailNavLinkItem: {
    type: "href",
    href: "https://www.google.com",
  },
  blocksPerRun: 123123,
  emissionTokenAmountPerDay: BigNumber.from(1_000_000),
  totalTradingVolTokenAmount: BigNumber.from(1_000_000),
  myCurrentEmissionPercentage: BigNumber.from(1_000_000),
  myTradingVolumeInUSD: BigNumber.from(1_000_000),
  myCurrentEmissionAmount: BigNumber.from(1_000),
  myCurrentEmissionTokenInfo: TokenInfoPresets.esUNW,
})

export const Normal = tpl()
