import { FC, ReactNode } from "react"
import { defineMessage, useIntl } from "react-intl"
import { ScrollView, StyleProp, Text, View, ViewStyle } from "react-native"
import { Path, Svg } from "react-native-svg"
import { Divider } from "../../../../components/Divider"
import { LinearGradientContainer } from "../../../../components/LinearGradientContainer"
import { PercentNumber } from "../../../../components/PercentNumber"
import { Spensor } from "../../../../components/Spensor"
import { TextNumber } from "../../../../components/TextNumber"
import {
  TextTokenCount,
  TokenCountAsCurrency,
} from "../../../../components/TextTokenCount"
import { useColors } from "../../../../components/Themed/color"
import { useSpacing } from "../../../../components/Themed/spacing"
import { TokenPresets } from "../../../../components/TokenCount"
import { TooltippifiedText } from "../../../../components/Tooltip"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { FCS } from "../../../../utils/reactHelpers/types"
import { HrefLink } from "../../../../utils/reactNavigationHelpers/HrefLink"
import { NavLink } from "../../../../utils/reactNavigationHelpers/NavLink"
import { UniversalLinkInfo } from "../../../../utils/reactNavigationHelpers/reactNavigationHelpers"
import {
  readResource,
  SuspenseResource,
} from "../../../../utils/SuspenseResource"
import { TokenInfo } from "../../../../utils/TokenInfo"

interface TradeAndEarnProps {
  style?: StyleProp<ViewStyle>
  airdropTokenInfoPerDay: TokenInfo
  airdropTokenAmountPerDay: SuspenseResource<BigNumber>
  detailNavLinkItem: UniversalLinkInfo
}
const TradeAndEarn: FC<TradeAndEarnProps> = props => {
  const { $t } = useIntl()
  return (
    <View style={props.style}>
      <Text className="text-[18px] leading-7 font-semibold text-blue-600">
        {$t(
          defineMessage({
            defaultMessage: "Trade and Earn!",
            description: "TradeScreen/TradePromote",
          }),
        )}
      </Text>
      <Text className="text-[12px] text-gray-500" numberOfLines={1}>
        {$t<ReactNode>(
          defineMessage({
            defaultMessage:
              "{emissionTokenAmountPerDay} per day <detailLink>Detail></detailLink>",
            description: "TradeScreen/TradePromote",
          }),
          {
            emissionTokenAmountPerDay: (
              <TextTokenCount
                token={props.airdropTokenInfoPerDay}
                count={props.airdropTokenAmountPerDay}
              />
            ),
            detailLink: content =>
              props.detailNavLinkItem.type === "href" ? (
                <HrefLink href={props.detailNavLinkItem.href}>
                  <Text className="text-[12px] leading-4 font-normal text-gray-500 underline">
                    {content}
                  </Text>
                </HrefLink>
              ) : (
                <NavLink {...props.detailNavLinkItem}>
                  <Text className="text-[12px] leading-4 font-normal text-gray-500 underline">
                    {content}
                  </Text>
                </NavLink>
              ),
          },
        )}
      </Text>
    </View>
  )
}

interface CurrentTradeAirdropProps {
  style?: StyleProp<ViewStyle>
  totalTradingVol: SuspenseResource<BigNumber>
  blocksPerRun: SuspenseResource<number>
}
const TotalTradingVol: FCS<CurrentTradeAirdropProps> = props => {
  const { $t } = useIntl()
  return (
    <View style={props.style}>
      <Text className="text-[12px] text-gray-500">
        {$t(
          defineMessage({
            defaultMessage: "24h Total Trading Vol.",
            description: "TradeScreen/TradePromote",
          }),
        )}
      </Text>
      <TooltippifiedText
        content={
          <Text className="text-white">
            {$t(
              defineMessage({
                defaultMessage:
                  "Trade and Earn rewards are calculated for every {blocksPerRun} blocks",
                description: "TradeScreen/TradePromote",
              }),
              {
                blocksPerRun: (
                  <Spensor fallback="-">
                    {() => <Text>{readResource(props.blocksPerRun)}</Text>}
                  </Spensor>
                ),
              },
            )}
          </Text>
        }
      >
        <TokenCountAsCurrency
          className="text-[16px] text-blue-600 font-semibold"
          token={TokenPresets.USD}
          count={props.totalTradingVol}
        />
      </TooltippifiedText>
    </View>
  )
}

interface PersonalTradingVolProps {
  style?: StyleProp<ViewStyle>
  myTradingVolumeInUSD: SuspenseResource<BigNumber>
  blocksPerRun: SuspenseResource<number>
}
const PersonalTradingVol: FCS<PersonalTradingVolProps> = props => {
  const { $t } = useIntl()
  return (
    <View style={props.style}>
      <Text className="text-[12px] text-gray-500">
        {$t(
          defineMessage({
            defaultMessage: "24h Your Trading Vol.",
            description: "TradeScreen/TradePromote",
          }),
        )}
      </Text>
      <TooltippifiedText
        content={
          <Text className="text-white">
            {$t(
              defineMessage({
                defaultMessage:
                  "Trade and Earn rewards are calculated for every {blocksPerRun} blocks",
                description: "TradeScreen/TradePromote",
              }),
              {
                blocksPerRun: (
                  <Spensor fallback="-">
                    {() => <Text>{readResource(props.blocksPerRun)}</Text>}
                  </Spensor>
                ),
              },
            )}
          </Text>
        }
      >
        <TokenCountAsCurrency
          className="text-[16px] text-blue-600 font-semibold text-base"
          token={TokenPresets.USD}
          count={props.myTradingVolumeInUSD}
        />
      </TooltippifiedText>
    </View>
  )
}

interface PersonalEarningProps {
  style?: StyleProp<ViewStyle>
  myCurrentEmissionTokenInfo: TokenInfo
  myCurrentEmissionAmount: SuspenseResource<BigNumber>
  myCurrentEmissionPercentage: SuspenseResource<BigNumber>
}
const PersonalEarning: FCS<PersonalEarningProps> = props => {
  const { $t } = useIntl()
  return (
    <View style={props.style}>
      <Text className="text-[12px] text-gray-500">
        {$t(
          defineMessage({
            defaultMessage: "Your Trader Earning (%)",
            description: "TradeScreen/TradePromote",
          }),
        )}
      </Text>
      <View className="flex-row items-center space-x-2">
        <PercentNumber
          className="text-[16px] text-blue-600 font-semibold"
          number={props.myCurrentEmissionPercentage}
        />
        <Text className="text-gray-400 text-[12px]">
          (Est.{" "}
          <TextTokenCount
            token={props.myCurrentEmissionTokenInfo}
            count={props.myCurrentEmissionAmount}
          />
          )
        </Text>
      </View>
    </View>
  )
}

const CycleBlockCountdown: FCS<{
  remainingBlock: SuspenseResource<number>
  blocksPerRun: SuspenseResource<number>
}> = props => {
  const colors = useColors()
  const spacing = useSpacing()
  const { $t } = useIntl()

  return (
    <View
      style={[
        props.style,
        {
          paddingVertical: spacing(1),
          paddingHorizontal: spacing(2.5),
          borderWidth: 1,
          borderColor: colors("gray-400"),
          borderRadius: 4,
        },
      ]}
    >
      <Text style={{ fontSize: 12, color: colors("gray-500") }}>
        {$t(
          defineMessage({
            defaultMessage: "24h Remaining Block",
            description: "TradeScreen/Cycle Block Countdown",
          }),
        )}
      </Text>

      <View style={{ flexDirection: "row", alignItems: "center" }}>
        <Svg
          width="16"
          height="17"
          viewBox="0 0 16 17"
          fill={colors("gray-400")}
        >
          <Path d="M8 1.83333C11.676 1.83333 14.6667 4.824 14.6667 8.5C14.6667 12.176 11.676 15.1667 8 15.1667C4.324 15.1667 1.33333 12.176 1.33333 8.5C1.33333 4.824 4.324 1.83333 8 1.83333ZM8 0.5C3.582 0.5 0 4.082 0 8.5C0 12.918 3.582 16.5 8 16.5C12.418 16.5 16 12.918 16 8.5C16 4.082 12.418 0.5 8 0.5ZM8.66667 8.5V4.5H7.33333V9.83333H12V8.5H8.66667Z" />
        </Svg>

        <Text
          style={{
            marginLeft: spacing(1),
            fontSize: 14,
            color: colors("blue-800"),
          }}
        >
          {$t(
            defineMessage({
              defaultMessage: "Block #{remainingBlock} of #{blockPerRun}",
              description: "TradeScreen/Cycle Block Countdown",
            }),
            {
              remainingBlock: <TextNumber number={props.remainingBlock} />,
              blockPerRun: <TextNumber number={props.blocksPerRun} />,
            },
          )}
        </Text>
      </View>
    </View>
  )
}

export interface TradePromoteProps {
  detailNavLinkItem: UniversalLinkInfo
  emissionTokenInfo: TokenInfo
  emissionTokenAmountPerDay: SuspenseResource<BigNumber>

  totalTradingVolTokenAmount: SuspenseResource<BigNumber>
  myTradingVolumeInUSD: SuspenseResource<BigNumber>

  blocksPerRun: SuspenseResource<number>
  remainingBlock: SuspenseResource<number>

  myCurrentEmissionPercentage: SuspenseResource<BigNumber>
  myCurrentEmissionTokenInfo: TokenInfo
  myCurrentEmissionAmount: SuspenseResource<BigNumber>
}

export const TradePromote: FCS<TradePromoteProps> = props => {
  const spacing = useSpacing()

  return (
    <LinearGradientContainer
      style={props.style}
      className="min-h-[65px]"
      borderRadius={8}
      direction={"horizontal"}
      colors={[
        { color: "#CCE2FF", offset: 0 },
        { color: "#EFF6FF", offset: 0.276 },
        { color: "#FFFFFF", offset: 1 },
      ]}
    >
      <ScrollView
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{
          paddingHorizontal: spacing(4),
          paddingVertical: spacing(2),
          alignItems: "center",
        }}
      >
        <TradeAndEarn
          className={"mr-5"}
          style={{ minWidth: 180 }}
          airdropTokenInfoPerDay={props.emissionTokenInfo}
          airdropTokenAmountPerDay={props.emissionTokenAmountPerDay}
          detailNavLinkItem={props.detailNavLinkItem}
        />

        <CycleBlockCountdown
          className={"mr-5"}
          remainingBlock={props.remainingBlock}
          blocksPerRun={props.blocksPerRun}
        />

        <Divider className={"mr-5"} direction={"vertical"} />

        <TotalTradingVol
          className={"mr-5"}
          blocksPerRun={props.blocksPerRun}
          totalTradingVol={props.totalTradingVolTokenAmount}
        />

        <Divider className={"mr-5"} direction={"vertical"} />

        <PersonalTradingVol
          className={"mr-5"}
          myTradingVolumeInUSD={props.myTradingVolumeInUSD}
          blocksPerRun={props.blocksPerRun}
        />

        <Divider className={"mr-5"} direction={"vertical"} />

        <PersonalEarning
          myCurrentEmissionTokenInfo={props.myCurrentEmissionTokenInfo}
          myCurrentEmissionAmount={props.myCurrentEmissionAmount}
          myCurrentEmissionPercentage={props.myCurrentEmissionPercentage}
        />
      </ScrollView>
    </LinearGradientContainer>
  )
}
