import { FC, ReactNode, useMemo } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, Text, TextStyle, ViewStyle } from "react-native"
import { Path, Svg } from "react-native-svg"
import {
  ColorfulButtonVariant,
  ColorfulButtonVariantColorSet,
} from "../../../components/Button/ColorfulButtonVariant"
import { PlainIconButtonVariantLayout } from "../../../components/Button/PlainIconButtonVariant"
import {
  Button,
  ButtonTextStyleConsumer,
} from "../../../components/ButtonFramework/Button"
import { SmartLoadableButton } from "../../../components/ButtonFramework/LoadableButton"
import { CardInset } from "../../../components/CardBox/CardInset"
import {
  CardBoxModalContent,
  TitleBar,
} from "../../../components/CardBoxModalContent/CardBoxModalContent"
import { InfoList } from "../../../components/InfoList/InfoList"
import { InfoListItem } from "../../../components/InfoList/InfoListItem"
import {
  InfoListItemDetail,
  InfoListItemDetailText,
} from "../../../components/InfoList/InfoListItemDetail"
import { InfoListItemTitle } from "../../../components/InfoList/InfoListItemTitle"
import { PercentNumber } from "../../../components/PercentNumber"
import { Stack } from "../../../components/Stack"
import {
  TextTokenCount,
  TokenCountAsCurrency,
} from "../../../components/TextTokenCount"
import { useColors } from "../../../components/Themed/color"
import { useSpacing } from "../../../components/Themed/spacing"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { TokenInfo } from "../../../utils/TokenInfo"
import { OrderDirection, OrderType } from "../types"
import { orderDirection$t, orderType$t } from "./_/commonIntlMessages"
import { Colors } from "./_/designTokens"

export interface OrderCreationModalContentProps {
  style?: StyleProp<ViewStyle>
  onCancel: () => void
  onSubmit: () => void | Promise<void>

  baseToken: TokenInfo
  quoteToken: TokenInfo
  orderType: OrderType
  orderDirection: OrderDirection

  margin: BigNumber
  marginEstimatedUSD: BigNumber
  baseTokenPositionSize: BigNumber
  baseTokenPositionSizeEstimatedUSD: BigNumber

  leverage: BigNumber
  spreadRate: BigNumber
  liquidationPrice: BigNumber
  entryPrice: BigNumber
  allowedSlippage: BigNumber
  fees: BigNumber
}

export const OrderCreationModalContent: FC<
  OrderCreationModalContentProps
> = props => {
  const spacing = useSpacing()

  const intl = useIntl()
  const { $t } = intl

  return (
    <CardBoxModalContent
      style={props.style}
      padding={spacing(4)}
      onClose={props.onCancel}
    >
      <Stack space={spacing(5)}>
        <TitleBar>
          {$t(
            defineMessage({
              defaultMessage:
                "Confirm {orderDirection, select, long {Long} other {Short}}",
              description: "TradeScreen/confirm order modal/modal title",
            }),
            {
              orderDirection:
                props.orderDirection === OrderDirection.Long ? "long" : "short",
            },
          )}
        </TitleBar>

        <PayPanel
          orderType={props.orderType}
          orderDirection={props.orderDirection}
          baseToken={props.baseToken}
          quoteToken={props.quoteToken}
          margin={props.margin}
          marginEstimatedUSD={props.marginEstimatedUSD}
          baseTokenPositionSize={props.baseTokenPositionSize}
          baseTokenPositionSizeEstimatedUSD={
            props.baseTokenPositionSizeEstimatedUSD
          }
        />

        <CardInset>
          <InfoList direction={"column"} listItemDirection={"row"}>
            <Stack space={spacing(1)}>
              <InfoListItem>
                <InfoListItemTitle>
                  {$t(
                    defineMessage({
                      defaultMessage: "Collateral",
                      description: "TradeScreen/order creation panel/label",
                    }),
                  )}
                </InfoListItemTitle>
                <InfoListItemDetail>
                  <InfoListItemDetailText>
                    <TokenCountAsCurrency
                      token={props.quoteToken}
                      count={props.margin}
                    />
                  </InfoListItemDetailText>
                </InfoListItemDetail>
              </InfoListItem>

              <InfoListItem>
                <InfoListItemTitle>
                  {$t(
                    defineMessage({
                      defaultMessage: "Leverage",
                      description: "TradeScreen/order creation panel/label",
                    }),
                  )}
                </InfoListItemTitle>
                <InfoListItemDetail>
                  {BigNumber.toString(props.leverage)}x
                </InfoListItemDetail>
              </InfoListItem>

              <InfoListItem>
                <InfoListItemTitle>
                  {$t(
                    defineMessage({
                      defaultMessage: "Liq. Price",
                      description: "TradeScreen/order creation panel/label",
                    }),
                  )}
                </InfoListItemTitle>
                <InfoListItemDetail>
                  <InfoListItemDetailText>
                    <TokenCountAsCurrency
                      token={props.quoteToken}
                      count={props.liquidationPrice}
                    />
                  </InfoListItemDetailText>
                </InfoListItemDetail>
              </InfoListItem>

              <InfoListItem>
                <InfoListItemTitle>
                  {$t(
                    defineMessage({
                      defaultMessage: "Spread",
                      description: "TradeScreen/order creation panel/label",
                    }),
                  )}
                </InfoListItemTitle>
                <InfoListItemDetail>
                  <InfoListItemDetailText>
                    <PercentNumber number={props.spreadRate} />
                  </InfoListItemDetailText>
                </InfoListItemDetail>
              </InfoListItem>

              <InfoListItem>
                <InfoListItemTitle>
                  {$t(
                    defineMessage({
                      defaultMessage: "Entry Price",
                      description: "TradeScreen/order creation panel/label",
                    }),
                  )}
                </InfoListItemTitle>
                <InfoListItemDetail>
                  <InfoListItemDetailText>
                    <TokenCountAsCurrency
                      token={props.quoteToken}
                      count={props.entryPrice}
                    />
                  </InfoListItemDetailText>
                </InfoListItemDetail>
              </InfoListItem>

              <InfoListItem>
                <InfoListItemTitle>
                  {$t(
                    defineMessage({
                      defaultMessage: "Allowed Slippage",
                      description: "TradeScreen/order creation panel/label",
                    }),
                  )}
                </InfoListItemTitle>
                <InfoListItemDetail>
                  <InfoListItemDetailText>
                    <PercentNumber number={props.allowedSlippage} />
                  </InfoListItemDetailText>
                </InfoListItemDetail>
              </InfoListItem>

              <InfoListItem>
                <InfoListItemTitle>
                  {$t(
                    defineMessage({
                      defaultMessage: "Fees",
                      description: "TradeScreen/order creation panel/label",
                    }),
                  )}
                </InfoListItemTitle>
                <InfoListItemDetail>
                  <InfoListItemDetailText>
                    <TokenCountAsCurrency
                      token={props.quoteToken}
                      count={props.fees}
                    />
                  </InfoListItemDetailText>
                </InfoListItemDetail>
              </InfoListItem>
            </Stack>
          </InfoList>
        </CardInset>

        <SubmitButton
          orderDirection={props.orderDirection}
          onPress={props.onSubmit}
        />
      </Stack>
    </CardBoxModalContent>
  )
}

const PayPanel: FC<{
  style?: StyleProp<ViewStyle>
  orderType: OrderType
  orderDirection: OrderDirection
  baseToken: TokenInfo
  quoteToken: TokenInfo
  margin: BigNumber
  marginEstimatedUSD: BigNumber
  baseTokenPositionSize: BigNumber
  baseTokenPositionSizeEstimatedUSD: BigNumber
}> = props => {
  const colors = useColors()
  const spacing = useSpacing()

  const intl = useIntl()
  const { $t } = intl

  const textStyle: TextStyle = {
    fontSize: 16,
    lineHeight: 24,
    color: colors("gray-900"),
  }

  return (
    <CardInset style={props.style}>
      <Stack align={"center"} space={spacing(1)}>
        <Text style={textStyle}>
          {$t<ReactNode>(
            defineMessage({
              defaultMessage:
                "<strong>Pay {collateral}</strong> (${estimatedUSD, number, ::.00})",
              description: "TradeScreen/confirm order modal/text",
            }),
            {
              strong: children => (
                <Text style={{ fontWeight: "700" }}>{children}</Text>
              ),
              collateral: (
                <TextTokenCount token={props.quoteToken} count={props.margin} />
              ),
              estimatedUSD: BigNumber.toNumber(props.marginEstimatedUSD),
            },
          )}
        </Text>
        <Svg width="16" height="16" viewBox="0 0 16 16" fill="black">
          <Path d="M12.6779 9.72041L12.7561 9.78497C13.0528 10.0621 13.0824 10.5021 12.8252 10.8118L12.7561 10.8849L8.59433 14.7722C8.29767 15.0493 7.82666 15.077 7.49518 14.8367L7.41774 14.7722L3.25095 10.8857C2.93383 10.5964 2.91485 10.1258 3.20776 9.81503C3.50068 9.50428 4.00283 9.46234 4.3501 9.71963L4.42838 9.78419L7.17126 12.3428L7.17126 1.77791C7.17132 1.38353 7.48728 1.05159 7.90653 1.00545L8.00396 1C8.42611 1 8.78143 1.29522 8.83082 1.6869L8.83665 1.77791L8.83665 12.3451L11.5787 9.78497C11.8754 9.50785 12.3464 9.48018 12.6779 9.72041L12.7561 9.78497L12.6779 9.72041Z" />
        </Svg>
        <Text style={textStyle}>
          {$t<ReactNode>(
            defineMessage({
              defaultMessage:
                "<strong>{orderTypeAndDirection} {baseTokenPositionSize}</strong> (${estimatedUSD, number, ::.00})",
              description: "TradeScreen/confirm order modal/text",
            }),
            {
              strong: children => (
                <Text style={{ fontWeight: "700" }}>{children}</Text>
              ),
              orderTypeAndDirection: (
                <Text
                  style={{
                    color:
                      props.orderDirection === OrderDirection.Short
                        ? Colors.shortColor
                        : Colors.longColor,
                  }}
                >
                  {orderType$t.fromOrderType(intl, props.orderType)}
                  {" / "}
                  {orderDirection$t.fromOrderDirection(
                    intl,
                    props.orderDirection,
                  )}
                </Text>
              ),
              baseTokenPositionSize: (
                <TextTokenCount
                  token={props.baseToken}
                  count={props.baseTokenPositionSize}
                />
              ),
              estimatedUSD: BigNumber.toNumber(
                props.baseTokenPositionSizeEstimatedUSD,
              ),
            },
          )}
        </Text>
      </Stack>
    </CardInset>
  )
}

const SubmitButton: FC<{
  orderDirection: OrderDirection
  onPress: () => void | Promise<void>
}> = props => {
  const colors = useColors()

  const intl = useIntl()

  const buttonColor: ColorfulButtonVariantColorSet = useMemo(
    () =>
      props.orderDirection === OrderDirection.Long
        ? {
            normal: colors("green-500"),
            hovering: colors("green-600"),
            pressing: colors("green-700"),
            disabled: colors("green-500"),
          }
        : {
            normal: colors("red-500"),
            hovering: colors("red-600"),
            pressing: colors("red-700"),
            disabled: colors("red-500"),
          },
    [colors, props.orderDirection],
  )

  const iconPath =
    props.orderDirection === OrderDirection.Short ? (
      <Path d="M7.53101 10.1353L9.77224 7.87101L12.78 10.8857H10.9437V12.4805H15.5V7.91411H13.9515V9.65815L9.90924 5.53973L7.51023 7.96183L2.5552 2.99988L1.5 4.09817L7.53101 10.1353Z" />
    ) : (
      <Path d="M7.03101 5.34513L9.27224 7.60946L12.28 4.59472H10.4437V3H15V7.56636H13.4515V5.82232L9.40924 9.94074L7.01023 7.51864L2.0552 12.4806L1 11.3823L7.03101 5.34513Z" />
    )

  return (
    <SmartLoadableButton
      Variant={p => <ColorfulButtonVariant {...p} bgColor={buttonColor} />}
      onPress={props.onPress}
    >
      <ButtonTextStyleConsumer>
        {style => (
          <PlainIconButtonVariantLayout
            color={style.color}
            iconLeft={
              <Svg width="16" height="16" viewBox="0 0 16 16" fill="#111827">
                {iconPath}
              </Svg>
            }
          >
            <Button.Text>
              {orderDirection$t.fromOrderDirection(intl, props.orderDirection)}
            </Button.Text>
          </PlainIconButtonVariantLayout>
        )}
      </ButtonTextStyleConsumer>
    </SmartLoadableButton>
  )
}
