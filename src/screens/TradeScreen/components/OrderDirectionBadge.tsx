import { FC } from "react"
import { useIntl } from "react-intl"
import { StyleProp, ViewStyle } from "react-native"
import { checkNever } from "../../../utils/typeHelpers"
import { OrderDirection } from "../types"
import { GreenBadge, RedBadge } from "./Badge"
import { orderDirection$t } from "./_/commonIntlMessages"

export const OrderDirectionBadge: FC<{
  style?: StyleProp<ViewStyle>
  direction: OrderDirection
}> = props => {
  const { $t } = useIntl()

  if (props.direction === OrderDirection.Long) {
    return <GreenBadge style={props.style} text={$t(orderDirection$t.buy)} />
  } else if (props.direction === OrderDirection.Short) {
    return <RedBadge style={props.style} text={$t(orderDirection$t.sell)} />
  } else {
    checkNever(props.direction)
    return null
  }
}
