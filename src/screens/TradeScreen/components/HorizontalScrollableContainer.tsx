import { ScrollView, StyleProp, View, ViewStyle } from "react-native"
import { FCC } from "../../../utils/reactHelpers/types"

export interface HorizontalScrollableContainerProps {
  style?: StyleProp<ViewStyle>
  contentContainerStyle?: StyleProp<ViewStyle>
  scrollable: boolean
  horizontalMargin?: number
}

export const HorizontalScrollableContainer: FCC<
  HorizontalScrollableContainerProps
> = props => {
  if (props.scrollable) {
    return (
      <ScrollView
        style={props.style}
        contentContainerStyle={[
          { marginHorizontal: props.horizontalMargin },
          props.contentContainerStyle,
        ]}
        horizontal={true}
      >
        {props.children}
      </ScrollView>
    )
  } else {
    return (
      <View
        style={[
          { marginHorizontal: props.horizontalMargin },
          props.style,
          props.contentContainerStyle,
        ]}
      >
        {props.children}
      </View>
    )
  }
}
