import { FC } from "react"
import { TokenName } from "../../../components/TokenName"
import { TokenInfo } from "../../../utils/TokenInfo"

export interface TokenNameStackProps {
  baseToken: TokenInfo
  quoteToken: TokenInfo
}

export const TokenNameStack: FC<TokenNameStackProps> = props => {
  return (
    <>
      <TokenName token={props.baseToken} />/
      <TokenName token={props.quoteToken} />
    </>
  )
}
