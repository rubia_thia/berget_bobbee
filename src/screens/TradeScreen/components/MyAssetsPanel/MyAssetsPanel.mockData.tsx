import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { MyAssetsPanelProps } from "./MyAssetsPanel"

export const myAssetsPanelProps: MyAssetsPanelProps = {
  totalBalanceInUSD: BigNumber.from(456789.12),
  marginInUSD: BigNumber.from(9100.12),
  unrealizedPNL: {
    valueInUSD: BigNumber.from(388.68),
    trendPercentage: BigNumber.from(0.0853),
  },
  balances: [
    { token: TokenInfoPresets.MockBTC, balance: BigNumber.from(123.45678) },
    { token: TokenInfoPresets.MockUSDC, balance: BigNumber.from(123.45678) },
  ],
}
