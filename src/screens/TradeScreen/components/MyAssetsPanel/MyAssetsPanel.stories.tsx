import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { MyAssetsPanel } from "./MyAssetsPanel"
import { myAssetsPanelProps } from "./MyAssetsPanel.mockData"

export default {
  title: "Page/TradeScreen/MyAssetsPanel",
  component: MyAssetsPanel,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof MyAssetsPanel>

const template = withTemplate(MyAssetsPanel, {
  style: { margin: 10, width: "auto", alignSelf: "stretch" },
  ...myAssetsPanelProps,
})

export const Normal = template()
