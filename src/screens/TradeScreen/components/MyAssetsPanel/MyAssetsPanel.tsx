import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, Text, TextStyle, View, ViewStyle } from "react-native"
import { CardBox } from "../../../../components/CardBox/CardBox"
import { CardInset } from "../../../../components/CardBox/CardInset"
import { Collapsable } from "../../../../components/Collapsable/Collapsable"
import { DefaultTriggerButton } from "../../../../components/Collapsable/DefaultTriggerButton"
import { InfoList } from "../../../../components/InfoList/InfoList"
import { InfoListItem } from "../../../../components/InfoList/InfoListItem"
import {
  InfoListItemDetail,
  InfoListItemDetailText,
} from "../../../../components/InfoList/InfoListItemDetail"
import {
  InfoListItemTitle,
  InfoListItemTitleText,
} from "../../../../components/InfoList/InfoListItemTitle"
import { NoteParagraph } from "../../../../components/NoteParagraph/NoteParagraph"
import { PercentNumber } from "../../../../components/PercentNumber"
import { Spensor } from "../../../../components/Spensor"
import { Stack } from "../../../../components/Stack"
import { useColors } from "../../../../components/Themed/color"
import { useSpacing } from "../../../../components/Themed/spacing"
import { TokenCount, TokenPresets } from "../../../../components/TokenCount"
import { TokenName } from "../../../../components/TokenName"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import {
  readResource,
  SuspenseResource,
} from "../../../../utils/SuspenseResource"
import { TokenInfo } from "../../../../utils/TokenInfo"
import { checkObjEmpty } from "../../../../utils/typeHelpers"
import { Colors } from "../_/designTokens"

export interface MyAssetsPanelProps {
  style?: StyleProp<ViewStyle>
  totalBalanceInUSD: SuspenseResource<BigNumber>
  marginInUSD: SuspenseResource<BigNumber>
  unrealizedPNL: {
    valueInUSD: SuspenseResource<BigNumber>
    trendPercentage: SuspenseResource<BigNumber>
  }
  balances: { token: TokenInfo; balance: SuspenseResource<BigNumber> }[]
}

export const MyAssetsPanel: FC<MyAssetsPanelProps> = props => {
  const spacing = useSpacing()
  const colors = useColors()
  const { $t } = useIntl()

  const assetTableHeader: TextStyle = {
    fontSize: 12,
    color: colors("gray-400"),
  }

  return (
    <CardBox>
      {({ style, ...rest }) => {
        checkObjEmpty(rest)

        return (
          <Stack
            style={[props.style, { width: "auto", padding: spacing(4) }, style]}
            space={spacing(5)}
          >
            <Text
              style={{
                color: colors("gray-900"),
                fontSize: 14,
              }}
            >
              {$t(
                defineMessage({
                  defaultMessage: "My Asset",
                  description: "Trade Screen/My Asset Panel/Title",
                }),
              )}
            </Text>

            <InfoList>
              <Stack space={spacing(1)}>
                <InfoListItem>
                  <InfoListItemTitle>
                    {$t(
                      defineMessage({
                        defaultMessage: "Total Balance",
                        description: "Trade Screen/My Asset Panel/field title",
                      }),
                    )}
                  </InfoListItemTitle>
                  <InfoListItemDetail>
                    <InfoListItemDetailText>
                      ~$
                      <TokenCount
                        token={TokenPresets.USD}
                        count={props.totalBalanceInUSD}
                      />
                    </InfoListItemDetailText>
                  </InfoListItemDetail>
                </InfoListItem>

                <InfoListItem>
                  <InfoListItemTitle>
                    {$t(
                      defineMessage({
                        defaultMessage: "Collateral",
                        description: "Trade Screen/My Asset Panel/field title",
                      }),
                    )}
                  </InfoListItemTitle>
                  <InfoListItemDetail>
                    <InfoListItemDetailText>
                      ~$
                      <TokenCount
                        token={TokenPresets.USD}
                        count={props.marginInUSD}
                      />
                    </InfoListItemDetailText>
                  </InfoListItemDetail>
                </InfoListItem>

                <InfoListItem>
                  <InfoListItemTitle>
                    {$t(
                      defineMessage({
                        defaultMessage: "Unrealized PNL",
                        description: "Trade Screen/My Asset Panel/field title",
                      }),
                    )}
                  </InfoListItemTitle>
                  <InfoListItemDetail>
                    <Spensor>
                      {() => {
                        const percentage = readResource(
                          props.unrealizedPNL.trendPercentage,
                        )
                        const value = readResource(
                          props.unrealizedPNL.valueInUSD,
                        )
                        return (
                          <InfoListItemDetailText
                            style={{
                              color: BigNumber.isNegative(percentage)
                                ? Colors.decreaseColor
                                : Colors.increaseColor,
                            }}
                          >
                            {BigNumber.isNegative(percentage) ? "-" : "+"}
                            $
                            <TokenCount
                              token={TokenPresets.USD}
                              count={BigNumber.abs(value)}
                            />
                            &nbsp; (
                            {BigNumber.isNegative(percentage) ? "-" : "+"}
                            <PercentNumber number={BigNumber.abs(percentage)} />
                            )
                          </InfoListItemDetailText>
                        )
                      }}
                    </Spensor>
                  </InfoListItemDetail>
                </InfoListItem>
              </Stack>
            </InfoList>

            <Collapsable TriggerButton={DefaultTriggerButton}>
              <CardInset>
                <View
                  style={{
                    marginBottom: spacing(2),
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <Text style={assetTableHeader}>
                    {$t(
                      defineMessage({
                        defaultMessage: "Asset",
                        description: "Trade Screen/My Asset Panel/field title",
                      }),
                    )}
                  </Text>

                  <Text style={assetTableHeader}>
                    {$t(
                      defineMessage({
                        defaultMessage: "Balance",
                        description: "Trade Screen/My Asset Panel/field title",
                      }),
                    )}
                  </Text>
                </View>

                <InfoList>
                  <Stack space={spacing(1)}>
                    {props.balances.map(({ token, balance }, idx) => (
                      <InfoListItem key={token.id ?? idx}>
                        <InfoListItemTitle>
                          <InfoListItemTitleText>
                            <TokenName token={token} />
                          </InfoListItemTitleText>
                        </InfoListItemTitle>

                        <InfoListItemDetail>
                          <InfoListItemDetailText>
                            <TokenCount token={token} count={balance} />
                          </InfoListItemDetailText>
                        </InfoListItemDetail>
                      </InfoListItem>
                    ))}
                  </Stack>
                </InfoList>
              </CardInset>
            </Collapsable>

            <NoteParagraph>
              {$t(
                defineMessage({
                  defaultMessage: "Display Stable Assets > $1 Only",
                  description: "Trade Screen/My Asset Panel/Note",
                }),
              )}
            </NoteParagraph>
          </Stack>
        )
      }}
    </CardBox>
  )
}
