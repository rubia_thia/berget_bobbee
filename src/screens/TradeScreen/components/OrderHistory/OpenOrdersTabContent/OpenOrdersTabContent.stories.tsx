import { ComponentMeta } from "@storybook/react"
import { View } from "react-native"
import { BackgroundColor } from "../../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../../.storybook/utils"
import { OpenOrdersTabContent } from "./OpenOrdersTabContent"
import { openOrderRecords } from "./OpenOrdersTabContent.mockData"

export default {
  title: "Page/TradeScreen/Operation History Panel/OpenOrdersTabContent",
  component: OpenOrdersTabContent,
  decorators: [
    Story => (
      <View style={{ margin: 10 }}>
        <Story />
      </View>
    ),
    BackgroundColor("#F3F4F6"),
  ],
} as ComponentMeta<typeof OpenOrdersTabContent>

const template = withTemplate(OpenOrdersTabContent, {
  horizontalScrollable: false,
  horizontalMargin: 0,
  records: openOrderRecords,
  paginationInfo: {
    pageSize: 20,
    current: 3,
    total: 1034,
  },
})

export const Normal = template()

export const HorizontalScrollable = template(p => {
  p.horizontalScrollable = true
  p.horizontalMargin = 10
})
