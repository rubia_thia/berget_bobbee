import { BigNumber } from "../../../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { OpenOrderRecord, OrderDirection, OrderType } from "../../../types"

export const openOrderRecords: OpenOrderRecord[] = [
  {
    orderHash: "1",
    quoteToken: TokenInfoPresets.MockUSDC,
    baseToken: TokenInfoPresets.MockBTC,
    orderType: OrderType.Limit,
    orderDirection: OrderDirection.Short,
    leverage: BigNumber.from(10),
    createdAt: new Date(),
    margin: BigNumber.from(40.45),
    position: BigNumber.from(404.5),
    askPrice: BigNumber.from(0.4165),
    isCanceling: false,
  },
  {
    orderHash: "2",
    quoteToken: TokenInfoPresets.MockUSDC,
    baseToken: TokenInfoPresets.MockBTC,
    orderType: OrderType.Limit,
    orderDirection: OrderDirection.Long,
    leverage: BigNumber.from(10),
    createdAt: new Date(),
    margin: BigNumber.from(40.45),
    position: BigNumber.from(404.5),
    askPrice: BigNumber.from(0.4165),
    isCanceling: false,
  },
]
