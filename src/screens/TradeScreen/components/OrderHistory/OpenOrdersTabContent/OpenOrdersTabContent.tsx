import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { ActivityIndicator } from "react-native"
import { cancel$t } from "../../../../../commonIntlMessages"
import { ButtonText } from "../../../../../components/ButtonFramework/Button"
import { LoadableButton } from "../../../../../components/ButtonFramework/LoadableButton"
import { EmptyState } from "../../../../../components/EmptyState/EmptyState"
import {
  InfoListItemDetail,
  InfoListItemDetailText,
} from "../../../../../components/InfoList/InfoListItemDetail"
import { InfoListItemTitle } from "../../../../../components/InfoList/InfoListItemTitle"
import {
  PaginationInfo,
  PaginationProps,
} from "../../../../../components/Pagination/Pagination"
import { Spensor } from "../../../../../components/Spensor"
import { TokenCountAsCurrency } from "../../../../../components/TextTokenCount"
import { useColors } from "../../../../../components/Themed/color"
import { useSpacing } from "../../../../../components/Themed/spacing"
import { BigNumber } from "../../../../../utils/numberHelpers/BigNumber"
import {
  readResource,
  safeReadResource,
  SuspenseResource,
} from "../../../../../utils/SuspenseResource"
import { OpenOrderRecord, OrderDirection } from "../../../types"
import { HorizontalScrollableContainer } from "../../HorizontalScrollableContainer"
import { TokenNameStack } from "../../TokenNameStack"
import { orderDirection$t, orderType$t } from "../../_/commonIntlMessages"
import { Colors } from "../../_/designTokens"
import { CancelButtonVariant } from "../_/CancelButtonVariant"
import { fields$t } from "../_/commonIntlMessages"
import {
  ActionsCell,
  CellTiles,
  CreateDateCellContent,
  FieldCell,
  HeadCell,
  RecordRowContainer,
  RecordTableContainerWithPagination,
} from "../_/RecordRow"

export const OpenOrdersTabContent: FC<{
  horizontalScrollable: boolean
  horizontalMargin: number
  records: SuspenseResource<OpenOrderRecord[]>
  paginationInfo: SuspenseResource<PaginationInfo>
  onPaginationChange: PaginationProps["onChange"]
  onCancelOrder: (record: OpenOrderRecord) => Promise<void>
}> = props => {
  const spacing = useSpacing()

  return (
    <>
      <Spensor fallback={<ActivityIndicator />}>
        {() =>
          readResource(props.records).length <= 0 ? (
            <EmptyState />
          ) : (
            <RecordTableContainerWithPagination
              paginationInfo={props.paginationInfo}
              onPaginationChange={props.onPaginationChange}
            >
              {readResource(props.records).map((r, idx) => (
                <HorizontalScrollableContainer
                  key={idx}
                  style={{ marginBottom: spacing(1) }}
                  scrollable={props.horizontalScrollable}
                  horizontalMargin={props.horizontalMargin}
                >
                  <RowTemplate
                    mainPartMaxWidth={
                      props.horizontalScrollable ? 700 : undefined
                    }
                    record={r}
                    onCancelOrder={props.onCancelOrder}
                  />
                </HorizontalScrollableContainer>
              ))}
            </RecordTableContainerWithPagination>
          )
        }
      </Spensor>
    </>
  )
}

const RowTemplate: FC<{
  mainPartMaxWidth?: number
  record: OpenOrderRecord
  onCancelOrder: (record: OpenOrderRecord) => Promise<void>
}> = props => {
  const colors = useColors()

  const intl = useIntl()
  const { $t } = intl

  const { record } = props

  return (
    <RecordRowContainer>
      <HeadCell>
        <InfoListItemDetail>
          <InfoListItemDetailText
            style={{
              fontSize: 14,
              fontWeight: "600",
              color: colors("gray-900"),
            }}
          >
            <TokenNameStack
              baseToken={record.baseToken}
              quoteToken={record.quoteToken}
            />
          </InfoListItemDetailText>
          <InfoListItemDetailText
            style={{
              fontSize: 14,
              fontWeight: "500",
              color: colors("gray-500"),
            }}
          >
            {BigNumber.toString(
              BigNumber.round({ precision: 2 }, record.leverage),
            )}
            x
          </InfoListItemDetailText>
        </InfoListItemDetail>
      </HeadCell>

      <CellTiles style={{ maxWidth: props.mainPartMaxWidth }}>
        <FieldCell>
          <InfoListItemTitle>{$t(fields$t.orderType)}</InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText
              style={{
                fontSize: 12,
                color:
                  record.orderDirection === OrderDirection.Short
                    ? Colors.shortColor
                    : Colors.longColor,
              }}
            >
              {orderDirection$t.fromOrderDirection(intl, record.orderDirection)}{" "}
              / {orderType$t.fromOrderType(intl, record.orderType)}
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </FieldCell>

        <FieldCell>
          <InfoListItemTitle>{$t(fields$t.margin)}</InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              <TokenCountAsCurrency
                token={record.quoteToken}
                count={record.margin}
              />
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </FieldCell>

        <FieldCell>
          <InfoListItemTitle>
            {$t(
              defineMessage({
                defaultMessage: "Position",
                description:
                  "TradeScreen/Operation History Panel/Open Orders/field title",
              }),
            )}
          </InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              <TokenCountAsCurrency
                token={record.quoteToken}
                count={record.position}
              />
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </FieldCell>

        <FieldCell>
          <InfoListItemTitle>
            {$t(
              defineMessage({
                defaultMessage: "Ask Price",
                description:
                  "TradeScreen/Operation History Panel/Open Orders/field title",
              }),
            )}
          </InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              <TokenCountAsCurrency
                token={record.quoteToken}
                count={record.askPrice}
              />
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </FieldCell>

        <FieldCell>
          <CreateDateCellContent
            createdAt={props.record.createdAt}
            fadeoutTime={true}
          />
        </FieldCell>
      </CellTiles>

      <ActionsCell>
        <InfoListItemDetail style={{ justifyContent: "flex-end" }}>
          <LoadableButton
            loading={safeReadResource(props.record.isCanceling)}
            Variant={CancelButtonVariant}
            onPress={() => props.onCancelOrder(props.record)}
          >
            <ButtonText>{$t(cancel$t)}</ButtonText>
          </LoadableButton>
        </InfoListItemDetail>
      </ActionsCell>
    </RecordRowContainer>
  )
}
