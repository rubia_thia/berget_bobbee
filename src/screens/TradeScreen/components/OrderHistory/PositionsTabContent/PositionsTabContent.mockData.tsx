import { noop } from "../../../../../utils/fnHelpers"
import { BigNumber } from "../../../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { OrderDirection, PositionRecord } from "../../../types"

export const positionRecords: PositionRecord[] = [
  {
    openedAt: new Date(),
    direction: OrderDirection.Long,
    id: "a",
    quoteToken: TokenInfoPresets.MockUSDC,
    baseToken: TokenInfoPresets.MockBTC,
    onCancel: noop,
    onEditMargin: noop,
    onEditActionTriggers: noop,

    leverage: BigNumber.from(10),
    positionSize: BigNumber.from(400.25),
    margin: BigNumber.from(40.45),
    entryPrice: BigNumber.from(1052.35),
    markPrice: BigNumber.from(1051.35),
    liquidityPrice: BigNumber.from(874.34),
    canceling: true,

    stopLossPrice: BigNumber.from(0.4169),
    takeProfitPrice: BigNumber.from(0.4169),

    pnl: {
      delta: BigNumber.from(-10.86),
      deltaPercentage: BigNumber.from(-0.0853),
    },

    fees: {
      total: BigNumber.from(-0.22),
      open: BigNumber.from(-1.2),
      funding: BigNumber.from(0.2),
      rollover: BigNumber.from(-0.3),
    },
  },
  {
    openedAt: new Date(),
    direction: OrderDirection.Long,
    id: "b",
    quoteToken: TokenInfoPresets.MockUSDC,
    baseToken: TokenInfoPresets.MockBTC,
    onCancel: noop,
    onEditMargin: noop,
    onEditActionTriggers: noop,

    leverage: BigNumber.from(10),
    positionSize: BigNumber.from(400.25),
    margin: BigNumber.from(40.45),
    entryPrice: BigNumber.from(1052.35),
    markPrice: BigNumber.from(1051.35),
    liquidityPrice: BigNumber.from(874.34),

    stopLossPrice: null,
    takeProfitPrice: BigNumber.from(0.4169),
    canceling: false,

    pnl: {
      delta: BigNumber.from(-10.86),
      deltaPercentage: BigNumber.from(-0.0853),
    },

    fees: {
      total: BigNumber.from(-0.22),
      open: BigNumber.from(-1.2),
      funding: BigNumber.from(0.2),
      rollover: BigNumber.from(-0.3),
    },
  },
  {
    openedAt: new Date(),
    direction: OrderDirection.Short,
    id: "c",
    quoteToken: TokenInfoPresets.MockUSDC,
    baseToken: TokenInfoPresets.MockBTC,
    onCancel: noop,

    leverage: BigNumber.from(10),
    positionSize: BigNumber.from(400.25),
    margin: BigNumber.from(40.45),
    entryPrice: BigNumber.from(1052.35),
    markPrice: BigNumber.from(1051.35),
    liquidityPrice: BigNumber.from(874.34),

    stopLossPrice: null,
    takeProfitPrice: null,
    canceling: false,

    pnl: {
      delta: BigNumber.from(-10.86),
      deltaPercentage: BigNumber.from(-0.0853),
    },

    fees: {
      total: BigNumber.from(-0.22),
      open: BigNumber.from(-1.2),
      funding: BigNumber.from(0.2),
      rollover: BigNumber.from(-0.3),
    },
  },
]
