import { ComponentMeta } from "@storybook/react"
import { View } from "react-native"
import { BackgroundColor } from "../../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../../.storybook/utils"
import { MessageProvider } from "../../../../../components/MessageProvider/MessageProvider"
import { PositionsTabContent } from "./PositionsTabContent"
import { positionRecords } from "./PositionsTabContent.mockData"

export default {
  title: "Page/TradeScreen/Operation History Panel/PositionsTabContent",
  component: PositionsTabContent,
  decorators: [
    Story => (
      <MessageProvider>
        <View style={{ margin: 10 }}>
          <Story />
        </View>
      </MessageProvider>
    ),
    BackgroundColor("#F3F4F6"),
  ],
} as ComponentMeta<typeof PositionsTabContent>

const template = withTemplate(PositionsTabContent, {
  horizontalScrollable: false,
  horizontalMargin: 0,
  records: positionRecords,
  paginationInfo: {
    pageSize: 20,
    current: 3,
    total: 1034,
  },
})

export const Normal = template()

export const HorizontalScrollable = template(p => {
  p.horizontalScrollable = true
  p.horizontalMargin = 10
})
