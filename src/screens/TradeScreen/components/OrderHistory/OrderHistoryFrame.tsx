import { FC, ReactNode } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, View, ViewStyle } from "react-native"
import { CardBoxView } from "../../../../components/CardBox/CardBox"
import {
  TabBar,
  TabbarItem,
  TabbarItemVerticalPaddingUnit,
} from "../../../../components/TabBar"
import { useColors } from "../../../../components/Themed/color"
import { useSpacing } from "../../../../components/Themed/spacing"
import { SuspenseResource } from "../../../../utils/SuspenseResource"

export enum HistoryContentType {
  PositionOrders,
  OpenOrders,
  TradeHistory,
}

export const OrderHistoryFrame: FC<{
  style?: StyleProp<ViewStyle>
  positionOrderCount: SuspenseResource<number>
  openOrderCount: SuspenseResource<number>
  tradeHistoryCount: SuspenseResource<number>
  children: (info: {
    value: HistoryContentType
    paddingHorizontal: number
  }) => ReactNode | ReactNode[]
}> = props => {
  const { $t } = useIntl()

  const colors = useColors()
  const spacing = useSpacing()

  const paddingHorizontal = spacing(4)

  return (
    <CardBoxView
      style={props.style}
      padding={{
        paddingTop: spacing(4) - spacing(TabbarItemVerticalPaddingUnit),
        paddingBottom: spacing(4),
      }}
      backgroundColor={colors("gray-100")}
    >
      <TabBar
        style={{ marginHorizontal: paddingHorizontal }}
        tabs={[
          {
            tab: (
              <TabbarItem count={props.positionOrderCount}>
                {$t(
                  defineMessage({
                    defaultMessage: `Positions`,
                    description:
                      "Page/TradeScreen/Operation History Panel/tab name",
                  }),
                )}
              </TabbarItem>
            ),
            value: HistoryContentType.PositionOrders,
          },
          {
            tab: (
              <TabbarItem count={props.openOrderCount}>
                {$t(
                  defineMessage({
                    defaultMessage: `Open Orders`,
                    description:
                      "Page/TradeScreen/Operation History Panel/tab name",
                  }),
                )}
              </TabbarItem>
            ),
            value: HistoryContentType.OpenOrders,
          },
          {
            tab: (
              <TabbarItem count={props.tradeHistoryCount}>
                {$t(
                  defineMessage({
                    defaultMessage: `Trade History`,
                    description: "Page/TradeScreen/TradeHistory/tab name",
                  }),
                )}
              </TabbarItem>
            ),
            value: HistoryContentType.TradeHistory,
          },
        ]}
      >
        {info => (
          <View style={{ minHeight: 160, justifyContent: "center" }}>
            {props.children({ ...info, paddingHorizontal })}
          </View>
        )}
      </TabBar>
    </CardBoxView>
  )
}
