import { BigNumber } from "../../../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { OrderDirection, OrderType, TradeRecord } from "../../../types"

const getCommonData = (
  input: Partial<TradeRecord.Common> = {},
): TradeRecord.Common => {
  return {
    transactionHash: "0xasdfsadfhipoenwfandsd",
    orderType: OrderType.Market,
    orderDirection: OrderDirection.Short,
    createdAt: new Date(),
    quoteToken: TokenInfoPresets.MockUSDC,
    baseToken: TokenInfoPresets.MockBTC,
    leverage: BigNumber.from(10),
    margin: BigNumber.from(40.45),
    position: BigNumber.from(404.5),
    ...input,
  }
}

export const tradeRecords: TradeRecord[] = [
  {
    type: TradeRecord.Type.CreateOrder,
    ...getCommonData(),
    price: BigNumber.from(0.4165),
    fee: BigNumber.from(0.0001),
  },
  {
    type: TradeRecord.Type.CancelOrder,
    ...getCommonData(),
  },
  {
    type: TradeRecord.Type.IncreaseMargin,
    ...getCommonData(),
    marginDelta: BigNumber.from(10),
  },
  {
    type: TradeRecord.Type.DecreaseMargin,
    ...getCommonData(),
    marginDelta: BigNumber.from(10),
  },
  {
    type: TradeRecord.Type.ClosePosition,
    ...getCommonData({
      orderDirection: OrderDirection.Short,
    }),
    grossPnl: BigNumber.from(11.110001),
    price: BigNumber.from(0.4165),
    fees: {
      total: BigNumber.from(-0.032),
      close: BigNumber.from(-0.0001),
      funding: BigNumber.from(0.0001),
      rollover: BigNumber.from(-0.0001),
    },
  },
  {
    type: TradeRecord.Type.ClosePosition,
    ...getCommonData({
      orderDirection: OrderDirection.Long,
    }),
    grossPnl: BigNumber.from(-11.110001),
    price: BigNumber.from(0.4165),
    fees: {
      total: BigNumber.from(-0.032),
      close: BigNumber.from(-0.0001),
      funding: BigNumber.from(0.0001),
      rollover: BigNumber.from(-0.0001),
    },
  },
  {
    type: TradeRecord.Type.Liquidation,
    ...getCommonData({}),
    grossPnl: BigNumber.from(11.110001),
    price: BigNumber.from(0.4165),
    amount: BigNumber.from(0.4265),
    fees: {
      total: BigNumber.from(-0.032),
      close: BigNumber.from(-0.0001),
      funding: BigNumber.from(0.0001),
      rollover: BigNumber.from(-0.0001),
    },
  },
]
