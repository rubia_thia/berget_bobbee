import { ComponentMeta } from "@storybook/react"
import { View } from "react-native"
import { BackgroundColor } from "../../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../../.storybook/utils"
import { MessageProviderDecorator } from "../../../../../components/MessageProvider/MessageProvider.storiesHelpers"
import { TradeHistoryTabContent } from "./TradeHistoryTabContent"
import { tradeRecords } from "./TradeHistoryTabContent.mockData"

export default {
  title: "Page/TradeScreen/Operation History Panel/TradeHistoryTabContent",
  component: TradeHistoryTabContent,
  decorators: [
    Story => (
      <View style={{ margin: 10 }}>
        <Story />
      </View>
    ),
    BackgroundColor("#F3F4F6"),
    MessageProviderDecorator(),
  ],
} as ComponentMeta<typeof TradeHistoryTabContent>

const template = withTemplate(TradeHistoryTabContent, {
  horizontalScrollable: false,
  horizontalMargin: 0,
  records: tradeRecords,
  paginationInfo: {
    pageSize: 20,
    current: 3,
    total: 1034,
  },
})

export const Normal = template()

export const HorizontalScrollable = template(p => {
  p.horizontalScrollable = true
  p.horizontalMargin = 10
})
