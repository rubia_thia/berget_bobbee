import * as Clipboard from "expo-clipboard"
import { identity, omit } from "ramda"
import { FC } from "react"
import { useIntl } from "react-intl"
import { ActivityIndicator, Text } from "react-native"
import { copied$t } from "../../../../../commonIntlMessages"
import { PlainIconButtonVariant } from "../../../../../components/Button/PlainIconButtonVariant"
import { Button } from "../../../../../components/ButtonFramework/Button"
import { ButtonTextStyleConsumer } from "../../../../../components/ButtonFramework/_/ButtonText"
import { EmptyState } from "../../../../../components/EmptyState/EmptyState"
import {
  InfoListItemDetail,
  InfoListItemDetailText,
} from "../../../../../components/InfoList/InfoListItemDetail"
import { InfoListItemTitle } from "../../../../../components/InfoList/InfoListItemTitle"
import { useMessage } from "../../../../../components/MessageProvider/MessageProvider"
import {
  PaginationInfo,
  PaginationProps,
} from "../../../../../components/Pagination/Pagination"
import { Spensor } from "../../../../../components/Spensor"
import { TokenCountAsCurrency } from "../../../../../components/TextTokenCount"
import { useColors } from "../../../../../components/Themed/color"
import { useSpacing } from "../../../../../components/Themed/spacing"
import { TooltippifiedText } from "../../../../../components/Tooltip"
import { TruncatedAddressText } from "../../../../../components/TruncatedAddress"
import { BigNumber } from "../../../../../utils/numberHelpers/BigNumber"
import {
  readResource,
  SuspenseResource,
} from "../../../../../utils/SuspenseResource"
import { OrderDirection, TradeRecord } from "../../../types"
import { HorizontalScrollableContainer } from "../../HorizontalScrollableContainer"
import { TokenNameStack } from "../../TokenNameStack"
import { orderDirection$t, orderType$t } from "../../_/commonIntlMessages"
import { Colors } from "../../_/designTokens"
import { fields$t, tradeType$t } from "../_/commonIntlMessages"
import CopyIcon from "../_/copyIcon.svg"
import { FeesTooltipContent } from "../_/FeesTooltipContent"
import {
  CellTiles,
  CreateDateCellContent,
  FieldCell,
  HeadCell,
  RecordRowContainer,
  RecordTableContainerWithPagination,
} from "../_/RecordRow"

export const TradeHistoryTabContent: FC<{
  horizontalScrollable: boolean
  horizontalMargin: number
  records: SuspenseResource<TradeRecord[]>
  paginationInfo: SuspenseResource<PaginationInfo>
  onPaginationChange: PaginationProps["onChange"]
}> = props => {
  const spacing = useSpacing()

  return (
    <>
      <Spensor fallback={<ActivityIndicator />}>
        {() =>
          readResource(props.records).length <= 0 ? (
            <EmptyState />
          ) : (
            <RecordTableContainerWithPagination
              paginationInfo={props.paginationInfo}
              onPaginationChange={props.onPaginationChange}
            >
              {readResource(props.records).map((r, idx) => (
                <HorizontalScrollableContainer
                  key={idx}
                  style={{ marginBottom: spacing(1) }}
                  scrollable={props.horizontalScrollable}
                  horizontalMargin={props.horizontalMargin}
                >
                  <RowTemplate
                    mainPartMaxWidth={
                      props.horizontalScrollable ? 700 : undefined
                    }
                    record={r}
                  />
                </HorizontalScrollableContainer>
              ))}
            </RecordTableContainerWithPagination>
          )
        }
      </Spensor>
    </>
  )
}

const RowTemplate: FC<{
  mainPartMaxWidth?: number
  record: TradeRecord
}> = props => {
  const colors = useColors()
  const message = useMessage()

  const intl = useIntl()
  const { $t } = intl

  const { record } = props

  const onCopyTxHash = async (): Promise<void> => {
    await Clipboard.setStringAsync(record.transactionHash)
    message.show({ message: $t(copied$t) })
  }

  return (
    <RecordRowContainer>
      <HeadCell>
        <CreateDateCellContent createdAt={props.record.createdAt} />
      </HeadCell>

      <CellTiles style={{ maxWidth: props.mainPartMaxWidth }}>
        <FieldCell>
          <InfoListItemTitle>{$t(fields$t.tradeType)}</InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              {tradeType$t.fromTradeType(intl, record.type)}
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </FieldCell>

        <FieldCell>
          <InfoListItemTitle>{$t(fields$t.pair)}</InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              <TokenNameStack
                baseToken={record.baseToken}
                quoteToken={record.quoteToken}
              />
              &nbsp;
              <Text style={{ color: colors("gray-500") }}>
                {Number(BigNumber.toNumber(record.leverage).toFixed(2))}x
              </Text>
              &nbsp;
              <Text
                style={{
                  color:
                    record.orderDirection === OrderDirection.Short
                      ? Colors.shortColor
                      : Colors.longColor,
                }}
              >
                {orderDirection$t.fromOrderDirection(
                  intl,
                  record.orderDirection,
                )}
                /{orderType$t.fromOrderType(intl, record.orderType)}
              </Text>
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </FieldCell>

        <FieldCell>
          <InfoListItemTitle>{$t(fields$t.margin)}</InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              {"marginDelta" in record ? (
                <TokenCountAsCurrency
                  token={record.quoteToken}
                  count={(record.type === TradeRecord.Type.DecreaseMargin
                    ? BigNumber.neg
                    : identity)(record.marginDelta)}
                />
              ) : (
                <TokenCountAsCurrency
                  token={record.quoteToken}
                  count={record.margin}
                />
              )}
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </FieldCell>

        {"stopLossPrice" in record && (
          <FieldCell>
            <InfoListItemTitle>{$t(fields$t.stopLossPrice)}</InfoListItemTitle>
            <InfoListItemDetail>
              <InfoListItemDetailText>
                {record.stopLossPrice == null ? (
                  "-"
                ) : (
                  <TokenCountAsCurrency
                    token={record.quoteToken}
                    count={record.stopLossPrice}
                  />
                )}
              </InfoListItemDetailText>
            </InfoListItemDetail>
          </FieldCell>
        )}

        {"takeProfitPrice" in record && (
          <FieldCell>
            <InfoListItemTitle>
              {$t(fields$t.takeProfitPrice)}
            </InfoListItemTitle>
            <InfoListItemDetail>
              <InfoListItemDetailText>
                {record.takeProfitPrice == null ? (
                  "-"
                ) : (
                  <TokenCountAsCurrency
                    token={record.quoteToken}
                    count={record.takeProfitPrice}
                  />
                )}
              </InfoListItemDetailText>
            </InfoListItemDetail>
          </FieldCell>
        )}

        {"price" in record && (
          <FieldCell>
            <InfoListItemTitle>{$t(fields$t.price)}</InfoListItemTitle>
            <InfoListItemDetail>
              <InfoListItemDetailText>
                <TokenCountAsCurrency
                  token={record.quoteToken}
                  count={record.price}
                />
              </InfoListItemDetailText>
            </InfoListItemDetail>
          </FieldCell>
        )}

        {"amount" in record && (
          <FieldCell>
            <InfoListItemTitle>{$t(fields$t.amount)}</InfoListItemTitle>
            <InfoListItemDetail>
              <InfoListItemDetailText>
                <TokenCountAsCurrency
                  token={record.quoteToken}
                  count={record.amount}
                />
              </InfoListItemDetailText>
            </InfoListItemDetail>
          </FieldCell>
        )}

        {"fee" in record && (
          <FieldCell>
            <InfoListItemTitle>{$t(fields$t.fee)}</InfoListItemTitle>
            <InfoListItemDetail>
              <InfoListItemDetailText>
                <TokenCountAsCurrency
                  token={record.quoteToken}
                  count={record.fee}
                />
              </InfoListItemDetailText>
            </InfoListItemDetail>
          </FieldCell>
        )}

        {"fees" in record && (
          <FieldCell>
            <InfoListItemTitle>{$t(fields$t.fee)}</InfoListItemTitle>
            <InfoListItemDetail>
              <InfoListItemDetailText numberOfLines={1}>
                <TooltippifiedText
                  content={
                    <FeesTooltipContent
                      quoteToken={record.quoteToken}
                      total={record.fees.total}
                      close={record.fees.close}
                      funding={record.fees.funding}
                      rollover={record.fees.rollover}
                    />
                  }
                >
                  <Spensor fallback={"-"}>
                    {() => (
                      <TokenCountAsCurrency
                        token={record.quoteToken}
                        count={readResource(record.fees).total}
                      />
                    )}
                  </Spensor>
                </TooltippifiedText>
              </InfoListItemDetailText>
            </InfoListItemDetail>
          </FieldCell>
        )}

        {"grossPnl" in record && (
          <FieldCell>
            <InfoListItemTitle>{$t(fields$t.grossPnl)}</InfoListItemTitle>
            <InfoListItemDetail>
              <InfoListItemDetailText>
                <TokenCountAsCurrency
                  style={{
                    color: BigNumber.isNegative(record.grossPnl)
                      ? Colors.lossColor
                      : Colors.profitColor,
                  }}
                  token={record.quoteToken}
                  count={record.grossPnl}
                />
              </InfoListItemDetailText>
            </InfoListItemDetail>
          </FieldCell>
        )}

        <FieldCell>
          <InfoListItemTitle>{$t(fields$t.txHash)}</InfoListItemTitle>
          <InfoListItemDetail>
            <Button
              Variant={p => (
                <PlainIconButtonVariant {...p} iconRight={<CopyIcon />} />
              )}
              onPress={onCopyTxHash}
            >
              <ButtonTextStyleConsumer>
                {style => (
                  <InfoListItemDetailText style={omit(["fontSize"], style)}>
                    <TruncatedAddressText address={record.transactionHash} />
                  </InfoListItemDetailText>
                )}
              </ButtonTextStyleConsumer>
            </Button>
          </InfoListItemDetail>
        </FieldCell>
      </CellTiles>
    </RecordRowContainer>
  )
}
