import { FC, ReactNode } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, View, ViewStyle } from "react-native"
import { InfoList } from "../../../../../components/InfoList/InfoList"
import { InfoListItem } from "../../../../../components/InfoList/InfoListItem"
import {
  InfoListItemDetail,
  InfoListItemDetailText,
} from "../../../../../components/InfoList/InfoListItemDetail"
import {
  InfoListItemTitle,
  InfoListItemTitleText,
} from "../../../../../components/InfoList/InfoListItemTitle"
import { Spensor } from "../../../../../components/Spensor"
import { Stack } from "../../../../../components/Stack"
import { TokenCountAsCurrency } from "../../../../../components/TextTokenCount"
import { useColors } from "../../../../../components/Themed/color"
import { useSpacing } from "../../../../../components/Themed/spacing"
import { TextInsideTooltip } from "../../../../../components/Tooltip"
import { FormattedMessageOutsideText } from "../../../../../utils/intlHelpers"
import { BigNumber } from "../../../../../utils/numberHelpers/BigNumber"
import { styleGetters } from "../../../../../utils/reactHelpers/withProps/styleGetters"
import { withProps } from "../../../../../utils/reactHelpers/withProps/withProps"
import { HrefLink } from "../../../../../utils/reactNavigationHelpers/HrefLink"
import {
  readResource,
  SuspenseResource,
} from "../../../../../utils/SuspenseResource"
import { TokenInfo } from "../../../../../utils/TokenInfo"

export interface FeesTooltipContentProps {
  style?: StyleProp<ViewStyle>
  quoteToken: TokenInfo
  total: SuspenseResource<BigNumber>
  open?: SuspenseResource<BigNumber>
  close?: SuspenseResource<BigNumber>
  funding?: SuspenseResource<BigNumber>
  rollover?: SuspenseResource<BigNumber>
}

export const FeesTooltipContent: FC<FeesTooltipContentProps> = props => {
  const spacing = useSpacing()
  const colors = useColors()

  const { $t } = useIntl()

  const gap = spacing(1)

  return (
    <Stack style={[props.style, { minWidth: 200 }]} space={gap}>
      <TextInsideTooltip style={{ color: colors("gray-200"), fontSize: 14 }}>
        {$t(
          defineMessage({
            defaultMessage: "Total Fees: {fees}",
            description: "TradeScreen/Operation History Panel/fee tooltip",
          }),
          {
            fees: <FeeNumber token={props.quoteToken} fee={props.total} />,
          },
        )}
      </TextInsideTooltip>

      <InfoList direction={"column"} listItemDirection={"row"}>
        <Stack space={gap}>
          {props.open != null && (
            <InfoListItem>
              <InfoListItemTitle>
                <InfoListItemTitleText>
                  {$t(
                    defineMessage({
                      defaultMessage: "Open Fee",
                      description:
                        "TradeScreen/Operation History Panel/fee tooltip",
                    }),
                  )}
                </InfoListItemTitleText>
              </InfoListItemTitle>
              <InfoListItemDetail>
                <TooltipInfoListItemDetailText>
                  <FeeNumber token={props.quoteToken} fee={props.open} />
                </TooltipInfoListItemDetailText>
              </InfoListItemDetail>
            </InfoListItem>
          )}

          {props.close != null && (
            <InfoListItem>
              <InfoListItemTitle>
                <InfoListItemTitleText>
                  {$t(
                    defineMessage({
                      defaultMessage: "Close Fee",
                      description:
                        "TradeScreen/Operation History Panel/fee tooltip",
                    }),
                  )}
                </InfoListItemTitleText>
              </InfoListItemTitle>
              <InfoListItemDetail>
                <TooltipInfoListItemDetailText>
                  <FeeNumber token={props.quoteToken} fee={props.close} />
                </TooltipInfoListItemDetailText>
              </InfoListItemDetail>
            </InfoListItem>
          )}

          {props.funding != null && (
            <InfoListItem>
              <InfoListItemTitle>
                <InfoListItemTitleText>
                  {$t(
                    defineMessage({
                      defaultMessage: "Funding Fee",
                      description:
                        "TradeScreen/Operation History Panel/fee tooltip",
                    }),
                  )}
                </InfoListItemTitleText>
              </InfoListItemTitle>
              <InfoListItemDetail>
                <TooltipInfoListItemDetailText>
                  <FeeNumber token={props.quoteToken} fee={props.funding} />
                </TooltipInfoListItemDetailText>
              </InfoListItemDetail>
            </InfoListItem>
          )}

          {props.rollover && (
            <InfoListItem>
              <InfoListItemTitle>
                <InfoListItemTitleText>
                  {$t(
                    defineMessage({
                      defaultMessage: "Rollover Fee",
                      description:
                        "TradeScreen/Operation History Panel/fee tooltip",
                    }),
                  )}
                </InfoListItemTitleText>
              </InfoListItemTitle>
              <InfoListItemDetail>
                <TooltipInfoListItemDetailText>
                  <FeeNumber token={props.quoteToken} fee={props.rollover} />
                </TooltipInfoListItemDetailText>
              </InfoListItemDetail>
            </InfoListItem>
          )}
        </Stack>
      </InfoList>

      {props.close != null && (
        <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
          <FormattedMessageOutsideText>
            {$t<ReactNode>(
              defineMessage({
                defaultMessage:
                  "<tooltipText>Close fee includes any liquidation charges.</tooltipText> <learnMoreLink>Learn more ></learnMoreLink>",
                description: "TradeScreen/Operation History Panel/fee tooltip",
              }),
              {
                tooltipText: content => (
                  <TextInsideTooltip style={{ color: colors("gray-400") }}>
                    {content}
                  </TextInsideTooltip>
                ),
                learnMoreLink: content => (
                  <HrefLink
                    href={"https://docs.uniwhale.co/trading#liquidation"}
                  >
                    <TextInsideTooltip style={{ color: colors("blue-600") }}>
                      {content}
                    </TextInsideTooltip>
                  </HrefLink>
                ),
              },
            )}
          </FormattedMessageOutsideText>
        </View>
      )}
    </Stack>
  )
}

const FeeNumber: FC<{
  token: TokenInfo
  fee: SuspenseResource<BigNumber>
}> = props => {
  return (
    <Spensor fallback={"-"}>
      {() => {
        const fee = readResource(props.fee)
        return (
          <>
            {BigNumber.isGte(fee, 0) ? "" : "-"}
            <TokenCountAsCurrency
              token={props.token}
              count={BigNumber.abs(fee)}
            />
          </>
        )
      }}
    </Spensor>
  )
}

const TooltipInfoListItemDetailText = withProps(
  styleGetters(({ colors }) => ({
    color: colors("gray-200"),
  })),
  InfoListItemDetailText,
)
