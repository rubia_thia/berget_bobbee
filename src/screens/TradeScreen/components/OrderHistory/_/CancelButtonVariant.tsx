import { LightBlueRoundedButtonVariant } from "../../../../../components/Button/LightBlueRoundedButtonVariant"
import { ButtonVariantProps } from "../../../../../components/ButtonFramework/Button"
import { useSpacing } from "../../../../../components/Themed/spacing"
import { FCC } from "../../../../../utils/reactHelpers/types"

export interface CancelButtonVariantProps extends ButtonVariantProps {}

export const CancelButtonVariant: FCC<CancelButtonVariantProps> = props => {
  const spacing = useSpacing()

  return (
    <LightBlueRoundedButtonVariant
      {...props}
      textStyle={{
        fontSize: 12,
        lineHeight: 16,
      }}
      padding={{
        padding: undefined,
        paddingVertical: spacing(1),
        paddingHorizontal: spacing(3),
      }}
    />
  )
}
