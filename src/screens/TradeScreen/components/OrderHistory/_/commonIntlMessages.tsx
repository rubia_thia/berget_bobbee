import { ReactNode } from "react"
import { defineMessage, IntlFormatters } from "react-intl"
import { checkNever } from "../../../../../utils/typeHelpers"
import { TradeRecord } from "../../../types"

export namespace tradeType$t {
  export const createOrder = defineMessage({
    defaultMessage: "Create Order",
    description: "TradeScreen/Operation History Panel/trade type",
  })

  export const cancelOrder = defineMessage({
    defaultMessage: "Cancel Order",
    description: "TradeScreen/Operation History Panel/trade type",
  })

  export const updateActionTrigger = defineMessage({
    defaultMessage: "SL/TP",
    description: "TradeScreen/Operation History Panel/trade type",
  })

  export const increaseMargin = defineMessage({
    defaultMessage: "Increase Collateral",
    description: "TradeScreen/Operation History Panel/trade type",
  })

  export const decreaseMargin = defineMessage({
    defaultMessage: "Decrease Collateral",
    description: "TradeScreen/Operation History Panel/trade type",
  })

  export const closePosition = defineMessage({
    defaultMessage: "Close Position",
    description: "TradeScreen/Operation History Panel/trade type",
  })

  export const takeProfit = defineMessage({
    defaultMessage: "Take Profit",
    description: "TradeScreen/Operation History Panel/trade type",
  })

  export const stopLoss = defineMessage({
    defaultMessage: "Stop Loss",
    description: "TradeScreen/Operation History Panel/trade type",
  })

  export const liquidation = defineMessage({
    defaultMessage: "Liquidation",
    description: "TradeScreen/Operation History Panel/trade type",
  })

  export const fromTradeType = (
    { $t }: IntlFormatters<ReactNode>,
    type: TradeRecord.Type,
  ): ReactNode => {
    switch (type) {
      case TradeRecord.Type.CreateOrder:
        return $t(createOrder)
      case TradeRecord.Type.CancelOrder:
        return $t(cancelOrder)
      case TradeRecord.Type.IncreaseMargin:
        return $t(increaseMargin)
      case TradeRecord.Type.DecreaseMargin:
        return $t(decreaseMargin)
      case TradeRecord.Type.ClosePosition:
        return $t(closePosition)
      case TradeRecord.Type.TakeProfit:
        return $t(takeProfit)
      case TradeRecord.Type.StopLoss:
        return $t(stopLoss)
      case TradeRecord.Type.Liquidation:
        return $t(liquidation)
      case TradeRecord.Type.UpdateActionTrigger:
        return $t(updateActionTrigger)
      default:
        return checkNever(type)
    }
  }
}

export namespace fields$t {
  export const orderHash = defineMessage({
    defaultMessage: "Order ID",
    description: "TradeScreen/Operation History Panel/field title: trade type",
  })

  export const txHash = defineMessage({
    defaultMessage: "TX ID",
    description: "TradeScreen/Operation History Panel/field title: trade type",
  })

  export const address = defineMessage({
    defaultMessage: "Address",
    description: "TradeScreen/Operation History Panel/field title: trade type",
  })

  export const tradeType = defineMessage({
    defaultMessage: "Type",
    description: "TradeScreen/Operation History Panel/field title: trade type",
  })

  export const positionSize = defineMessage({
    defaultMessage: "Size",
    description: "TradeScreen/Operation History Panel/field title",
  })

  export const orderType = defineMessage({
    defaultMessage: "Type",
    description: "TradeScreen/Operation History Panel/field title: order type",
  })

  export const pair = defineMessage({
    defaultMessage: "Pair",
    description:
      "TradeScreen/Operation History Panel/field title: order token pair",
  })

  export const margin = defineMessage({
    defaultMessage: "Collateral",
    description: "TradeScreen/Operation History Panel/field title",
  })

  export const price = defineMessage({
    defaultMessage: "Price",
    description: "TradeScreen/Operation History Panel/field title",
  })

  export const stopLossPrice = defineMessage({
    defaultMessage: "Stop Loss",
    description: "TradeScreen/Operation History Panel/field title",
  })

  export const takeProfitPrice = defineMessage({
    defaultMessage: "Take Profit",
    description: "TradeScreen/Operation History Panel/field title",
  })

  export const amount = defineMessage({
    defaultMessage: "Amount",
    description: "TradeScreen/Operation History Panel/field title",
  })

  export const fee = defineMessage({
    defaultMessage: "Fee",
    description: "TradeScreen/Operation History Panel/field title",
  })

  export const grossPnl = defineMessage({
    defaultMessage: "Gross PnL",
    description: "TradeScreen/Operation History Panel/field title",
  })

  export const entryPrice = defineMessage({
    defaultMessage: "Entry Price",
    description: "TradeScreen/Operation History Panel/field title",
  })

  export const markPrice = defineMessage({
    defaultMessage: "Mark Price",
    description: "TradeScreen/Operation History Panel/field title",
  })

  export const liquidityPrice = defineMessage({
    defaultMessage: "Liq. Price",
    description: "TradeScreen/Operation History Panel/field title",
  })

  export const pnl = defineMessage({
    defaultMessage: "PnL (ROE%)",
    description: "TradeScreen/Operation History Panel/field title",
  })

  export const fees = defineMessage({
    defaultMessage: "Fee",
    description: "TradeScreen/Operation History Panel/field title",
  })

  export const triggerCondition = defineMessage({
    defaultMessage: "Trigger Condition",
    description: "TradeScreen/Operation History Panel/field title",
  })

  export const createDate = defineMessage({
    defaultMessage: "Date",
    description: "TradeScreen/Operation History Panel/field title",
  })

  export const openDate = defineMessage({
    defaultMessage: "Open Date",
    description: "TradeScreen/Operation History Panel/field title",
  })

  export const lastUpdatedDate = defineMessage({
    defaultMessage: "Last Updated",
    description: "TradeScreen/Operation History Panel/field title",
  })

  export const status = defineMessage({
    defaultMessage: "Status",
    description: "TradeScreen/Operation History Panel/field title",
  })

  export const adjustedMarkPrice = defineMessage({
    defaultMessage: "Adjusted Mark Price",
    description: "TradeScreen/Operation History Panel/field title",
  })
}
