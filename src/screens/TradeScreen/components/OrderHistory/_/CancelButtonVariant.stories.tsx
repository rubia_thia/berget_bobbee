import { ComponentMeta } from "@storybook/react"
import { withTemplate } from "../../../../../../.storybook/utils"
import { Button } from "../../../../../components/ButtonFramework/Button"
import { CancelButtonVariant } from "./CancelButtonVariant"

export default {
  title: "Page/TradeScreen/Operation History Panel/CancelButtonVariant",
  component: CancelButtonVariant,
} as ComponentMeta<typeof CancelButtonVariant>

const template = withTemplate(Button, {
  style: {
    margin: 10,
  },
  Variant: CancelButtonVariant,
})

export const NormalButton = template(p => {
  p.children = "Normal Size Button"
})

export const DisabledButton = template(p => {
  p.children = "Disabled Button"
  p.disabled = true
})
