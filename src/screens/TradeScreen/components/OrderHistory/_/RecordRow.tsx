import { format } from "date-fns"
import { omit } from "ramda"
import { Children, FC, isValidElement, ReactNode } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { PlainIconButtonVariant } from "../../../../../components/Button/PlainIconButtonVariant"
import { Button } from "../../../../../components/ButtonFramework/Button"
import { ButtonTextStyleConsumer } from "../../../../../components/ButtonFramework/_/ButtonText"
import { InfoList } from "../../../../../components/InfoList/InfoList"
import { InfoListItem } from "../../../../../components/InfoList/InfoListItem"
import {
  InfoListItemDetail,
  InfoListItemDetailText,
} from "../../../../../components/InfoList/InfoListItemDetail"
import {
  InfoListItemTitle,
  InfoListItemTitleText,
} from "../../../../../components/InfoList/InfoListItemTitle"
import {
  Pagination,
  PaginationInfo,
  PaginationProps,
} from "../../../../../components/Pagination/Pagination"
import {
  createSizeTuner,
  Transformer,
} from "../../../../../components/SizeTuner"
import { Spensor } from "../../../../../components/Spensor"
import { TokenCountAsCurrency } from "../../../../../components/TextTokenCount"
import { useColors } from "../../../../../components/Themed/color"
import { useSpacing } from "../../../../../components/Themed/spacing"
import { BigNumber } from "../../../../../utils/numberHelpers/BigNumber"
import { FCC } from "../../../../../utils/reactHelpers/types"
import { usePersistFn } from "../../../../../utils/reactHelpers/usePersistFn"
import {
  readResource,
  SuspenseResource,
} from "../../../../../utils/SuspenseResource"
import { TokenInfo } from "../../../../../utils/TokenInfo"
import { fields$t } from "./commonIntlMessages"
import EditIcon from "./editIcon.svg"

const infoListItemSpacingUnit = 2.5
const cellMinWidth = 120

interface SizeTunerTransformerData {
  widthRatio: number
  heightRatio: number
}
const { SizeTuner, SizeConsumer } = createSizeTuner<SizeTunerTransformerData>()

export const RecordTableContainer: FCC = props => {
  const transformer: Transformer<SizeTunerTransformerData> = usePersistFn(
    allSize => {
      return {
        width: Math.max(...allSize.map(s => s.width), cellMinWidth, 0),
        height: Math.max(...allSize.map(s => s.height), 0),
      }
    },
  )

  return <SizeTuner transformer={transformer}>{props.children}</SizeTuner>
}

export const RecordTableContainerWithPagination: FCC<{
  paginationInfo: SuspenseResource<PaginationInfo>
  onPaginationChange: PaginationProps["onChange"]
}> = props => {
  const spacing = useSpacing()

  return (
    <>
      <RecordTableContainer>{props.children}</RecordTableContainer>

      <Spensor>
        {() => (
          <Pagination
            style={{ marginTop: spacing(2.5), marginLeft: "auto" }}
            {...readResource(props.paginationInfo)}
            onChange={props.onPaginationChange}
          />
        )}
      </Spensor>
    </>
  )
}

export const RecordRowContainer: FCC<{
  style?: StyleProp<ViewStyle>
}> = props => {
  const colors = useColors()
  const spacing = useSpacing()
  return (
    <InfoList
      style={[
        props.style,
        {
          borderRadius: spacing(1),
          paddingVertical: spacing(2.5),
          paddingHorizontal: spacing(4),
          backgroundColor: colors("white"),
        },
      ]}
      direction={"row"}
      listItemDirection={"column"}
    >
      {props.children}
    </InfoList>
  )
}

export const CellTiles: FCC<{ style?: StyleProp<ViewStyle> }> = props => {
  const spacing = useSpacing()

  const children = Children.toArray(props.children).map((child, idx) => (
    <View
      key={(isValidElement(child) ? child.key : undefined) ?? idx}
      style={{ marginBottom: spacing(infoListItemSpacingUnit) }}
    >
      {child}
    </View>
  ))

  return (
    <View
      style={[
        props.style,
        {
          marginBottom:
            children.length > 0 ? -spacing(infoListItemSpacingUnit) : 0,
          flex: 1,
          flexDirection: "row",
          flexWrap: "wrap",
        },
      ]}
    >
      {children}
    </View>
  )
}

export const HeadCell: FCC = props => (
  <SizeConsumer
    style={{ flexShrink: 0 }}
    data={{ widthRatio: 1, heightRatio: 1 }}
  >
    <InfoListItem>{props.children}</InfoListItem>
  </SizeConsumer>
)

export const FieldCell: FCC = props => {
  const spacing = useSpacing()
  return (
    <SizeConsumer
      style={{ marginLeft: spacing(infoListItemSpacingUnit) }}
      data={{ widthRatio: 1, heightRatio: 1 }}
    >
      <InfoListItem>{props.children}</InfoListItem>
    </SizeConsumer>
  )
}

export const ActionsCell: FCC = props => {
  const spacing = useSpacing()
  return (
    <SizeConsumer
      style={{ flexShrink: 0, marginLeft: spacing(infoListItemSpacingUnit) }}
      containerStyle={{ right: 0 }}
      data={{ widthRatio: 1, heightRatio: 1 }}
    >
      <InfoListItem>{props.children}</InfoListItem>
    </SizeConsumer>
  )
}

export const CreateDateCellContent: FC<{
  titleText?: ReactNode
  createdAt: Date
  fadeoutTime?: boolean
}> = props => {
  const { $t } = useIntl()
  const colors = useColors()
  return (
    <>
      <InfoListItemTitle>
        {props.titleText ?? $t(fields$t.createDate)}
      </InfoListItemTitle>
      <InfoListItemDetail>
        <InfoListItemDetailText
          style={{ color: props.fadeoutTime ? colors("gray-500") : undefined }}
          numberOfLines={1}
        >
          {format(props.createdAt, "yyyy-MM-dd HH:mm:ss")}
        </InfoListItemDetailText>
      </InfoListItemDetail>
    </>
  )
}

export const ActionTriggerCell: FC<{
  quoteToken: TokenInfo
  takeProfitPrice: null | BigNumber
  stopLossPrice: null | BigNumber
  onEditActionTriggers?: () => void
}> = props => {
  const { $t } = useIntl()

  const { onEditActionTriggers } = props

  const tpDetailText = $t(
    defineMessage({
      defaultMessage: `TP: {takeProfitPrice}`,
      description: "TradeScreen/Operation History Panel/field value",
    }),
    {
      takeProfitPrice:
        props.takeProfitPrice == null ? (
          "-"
        ) : (
          <TokenCountAsCurrency
            token={props.quoteToken}
            count={props.takeProfitPrice}
          />
        ),
    },
  )

  const slDetailText = $t(
    defineMessage({
      defaultMessage: `SL: {stopLossPrice}`,
      description: "TradeScreen/Operation History Panel/field value",
    }),
    {
      stopLossPrice:
        props.stopLossPrice == null ? (
          "-"
        ) : (
          <TokenCountAsCurrency
            token={props.quoteToken}
            count={props.stopLossPrice}
          />
        ),
    },
  )

  const content = (style: TextStyle = {}): JSX.Element => (
    <>
      <InfoListItemDetailText style={style}>
        {tpDetailText}
      </InfoListItemDetailText>
      <InfoListItemDetailText style={style}>
        {slDetailText}
      </InfoListItemDetailText>
    </>
  )

  return (
    <FieldCell>
      <InfoListItemTitle>
        <InfoListItemTitleText numberOfLines={1}>
          {$t(
            defineMessage({
              defaultMessage: "TP/SL For Position",
              description: "TradeScreen/Operation History Panel/field title",
            }),
          )}
        </InfoListItemTitleText>
      </InfoListItemTitle>
      <InfoListItemDetail>
        {onEditActionTriggers == null ? (
          content()
        ) : (
          <Button
            Variant={p => (
              <PlainIconButtonVariant
                {...p}
                iconRight={<EditIcon style={{ flexShrink: 0 }} />}
              />
            )}
            onPress={onEditActionTriggers}
          >
            <ButtonTextStyleConsumer>
              {style => content(omit(["fontSize"], style))}
            </ButtonTextStyleConsumer>
          </Button>
        )}
      </InfoListItemDetail>
    </FieldCell>
  )
}
