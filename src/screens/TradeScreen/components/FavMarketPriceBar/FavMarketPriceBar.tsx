import { FC } from "react"
import { ScrollView, StyleProp, ViewStyle } from "react-native"
import { CardBox } from "../../../../components/CardBox/CardBox"
import { Divider } from "../../../../components/Divider"
import { Inline, Stack } from "../../../../components/Stack"
import { useSpacing } from "../../../../components/Themed/spacing"
import { TokenCount } from "../../../../components/TokenCount"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { checkObjEmpty } from "../../../../utils/typeHelpers"
import { PriceChangeDirection, TokenPair } from "../../types"
import { TokenPairName } from "../TokenPairName"
import { Colors } from "../_/designTokens"
import StarIcon from "./_/star.svg"

export interface FavMarketInfo {
  tokenPair: TokenPair
  price: BigNumber
  priceChangeDirection: PriceChangeDirection
}

export const FavMarketPriceBar: FC<{
  style?: StyleProp<ViewStyle>
  tokenPairs: FavMarketInfo[]
}> = props => {
  const spacing = useSpacing()

  return (
    <CardBox>
      {({ style, ...rest }) => {
        checkObjEmpty(rest)
        return (
          <ScrollView style={[props.style, style, { flexGrow: 0 }]} horizontal>
            <Stack
              paddingX={spacing(4)}
              paddingY={spacing(3)}
              space={spacing(4)}
              divider={<Divider style={{ flex: 1 }} direction={"vertical"} />}
              align={"center"}
              horizontal
            >
              <StarIcon style={{ marginTop: -3 }} />
              {props.tokenPairs.map((tokenPair, index) => (
                <TokenPriceUnit key={index} info={tokenPair} />
              ))}
            </Stack>
          </ScrollView>
        )
      }}
    </CardBox>
  )
}

const TokenPriceUnit: FC<{ info: FavMarketInfo }> = props => {
  const spacing = useSpacing()
  const fontSize = 12

  return (
    <Inline alignY={"center"} space={spacing(2.5)}>
      <TokenPairName pair={props.info.tokenPair} />
      <TokenCount
        style={{ fontSize }}
        color={Colors.fromPriceChangeDirection(props.info.priceChangeDirection)}
        token={props.info.tokenPair.quoteToken}
        count={props.info.price}
      />
    </Inline>
  )
}
