import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { FavMarketPriceBar } from "./FavMarketPriceBar"
import { pairs } from "./FavMarketPriceBar.mockData"

export default {
  title: "Page/TradeScreen/FavMarketPriceBar",
  component: FavMarketPriceBar,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof FavMarketPriceBar>

const template = withTemplate(FavMarketPriceBar, {
  style: { margin: 10, alignSelf: "stretch" },
  tokenPairs: pairs,
})

export const Normal = template()

export const EmptyState = template(p => {
  p.tokenPairs = []
})
