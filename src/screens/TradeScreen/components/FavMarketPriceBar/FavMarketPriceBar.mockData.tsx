import { times } from "ramda"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { PriceChangeDirection } from "../../types"
import { FavMarketInfo } from "./FavMarketPriceBar"

export const pairs = times(
  (): FavMarketInfo => ({
    tokenPair: {
      baseToken: TokenInfoPresets.MockBTC,
      quoteToken: TokenInfoPresets.MockUSDC,
    },
    price: BigNumber.from(20243.97 * (1 - Math.random() * 0.01)),
    priceChangeDirection:
      Math.random() > 0.3
        ? PriceChangeDirection.Up
        : Math.random() < 0.3
        ? PriceChangeDirection.Down
        : PriceChangeDirection.Keep,
  }),
  20,
)
