import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { noop } from "../../../utils/fnHelpers"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { EditSlippageModalContent } from "./EditSlippageModalContent"

export default {
  title: "Page/TradeScreen/EditSlippageModalContent",
  component: EditSlippageModalContent,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof EditSlippageModalContent>

const tpl = withTemplate(EditSlippageModalContent, {
  style: { margin: 10 },
  onDismiss: noop,
  onSubmit: noop,
  recommendedSlippage: BigNumber.from(0.005),
  slippage: BigNumber.from(0.005),
})

export const Normal = tpl()
