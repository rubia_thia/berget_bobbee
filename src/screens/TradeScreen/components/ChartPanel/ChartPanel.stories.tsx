import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { priceChanges } from "../../../../components/LightweightCharts/LightweightCharts.mockData"
import { TokenInfoPresets } from "../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { ChartPanel } from "./ChartPanel"

export default {
  title: "Page/TradeScreen/ChartPanel",
  component: ChartPanel,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof ChartPanel>

const template = withTemplate(ChartPanel, {
  style: { margin: 10, height: 300 },
  quoteToken: TokenInfoPresets.MockBUSD,
  priceChanges,
  isSpreadRangeVisible: true,
})

export const Normal = template()
