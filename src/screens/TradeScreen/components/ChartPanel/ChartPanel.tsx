import { FC, useRef } from "react"
import { ActivityIndicator, StyleProp, ViewStyle } from "react-native"
import { Observable } from "rxjs"
import { AspectRatio } from "../../../../components/AspectRatio"
import { CardBoxView } from "../../../../components/CardBox/CardBox"
import { LightweightCharts } from "../../../../components/LightweightCharts/LightweightCharts"
import {
  OnHoveringPriceChangeCallback,
  PriceChange,
} from "../../../../components/LightweightCharts/LightweightCharts.types"
import { ScopedLoadingBoundary } from "../../../../components/LoadingBoundary/ScopedLoadingBoundary"
import { RenderChildrenFn } from "../../../../components/Spensor"
import { useResponsiveValue } from "../../../../components/Themed/breakpoints"
import { useSpacing } from "../../../../components/Themed/spacing"
import {
  readResource,
  SuspenseResource,
} from "../../../../utils/SuspenseResource"
import { TokenInfo } from "../../../../utils/TokenInfo"
import { Colors } from "../_/designTokens"

export const ChartPanel: FC<{
  style?: StyleProp<ViewStyle>
  isSpreadRangeVisible: boolean
  quoteToken: SuspenseResource<TokenInfo>
  priceChanges: SuspenseResource<Observable<PriceChange[]>>
  onHoveringPriceChange?: OnHoveringPriceChangeCallback
}> = props => {
  const spacing = useSpacing()

  const hasPadding =
    useResponsiveValue({
      _: false,
      md: true,
    }) ?? false

  const ratio =
    useResponsiveValue({
      _: 370 / 200,
      md: 1000 / 400,
    }) ?? 370 / 200

  const lastPriceChangesObservableRef = useRef<null | Observable<
    PriceChange[]
  >>(null)
  const chartKeyRef = useRef(0)

  return (
    <AspectRatio style={props.style} ratio={ratio}>
      <CardBoxView style={{ flex: 1, overflow: "hidden" }}>
        <ScopedLoadingBoundary
          style={[
            { flex: 1 },
            hasPadding && {
              paddingHorizontal: spacing(4),
              paddingVertical: spacing(2),
            },
          ]}
          loadingIndicator={<ActivityIndicator />}
        >
          <RenderChildrenFn>
            {() => {
              const currPriceChangesObservable = readResource(
                props.priceChanges,
              )

              // re-mount lightweight chart when price changes observable changes
              if (
                currPriceChangesObservable !==
                lastPriceChangesObservableRef.current
              ) {
                chartKeyRef.current += 1
                lastPriceChangesObservableRef.current =
                  currPriceChangesObservable
              }

              return (
                <LightweightCharts
                  key={`chart-${chartKeyRef.current}`}
                  style={{ flex: 1 }}
                  isSpreadRangeVisible={props.isSpreadRangeVisible}
                  token={readResource(props.quoteToken)}
                  priceChanges={currPriceChangesObservable}
                  initialColors={{
                    increase: Colors.increaseColor,
                    decrease: Colors.decreaseColor,
                    buy: Colors.longColor,
                    sell: Colors.shortColor,
                  }}
                  onHoveringPriceChange={props.onHoveringPriceChange}
                />
              )
            }}
          </RenderChildrenFn>
        </ScopedLoadingBoundary>
      </CardBoxView>
    </AspectRatio>
  )
}
