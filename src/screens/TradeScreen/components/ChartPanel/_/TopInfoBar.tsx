import { FC, useEffect, useState } from "react"
import { defineMessage, useIntl } from "react-intl"
import {
  StyleProp,
  Text,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import { Observable } from "rxjs"
import {
  OnHoveringPriceChangeHoveringData,
  PriceChange,
} from "../../../../../components/LightweightCharts/LightweightCharts.types"
import { Spensor } from "../../../../../components/Spensor"
import { useColors } from "../../../../../components/Themed/color"
import { useSpacing } from "../../../../../components/Themed/spacing"
import { TokenCount } from "../../../../../components/TokenCount"
import {
  allTimeWindows,
  HistoricalPriceTimeWindow,
} from "../../../../../stores/TradeStore/PriceChartModule.hasura"
import { BigNumber } from "../../../../../utils/numberHelpers/BigNumber"
import {
  readResource,
  SuspenseResource,
} from "../../../../../utils/SuspenseResource"
import { TokenInfo } from "../../../../../utils/TokenInfo"
import { Colors } from "../../_/designTokens"

export const TopInfoBar: FC<{
  style?: StyleProp<ViewStyle>
  isInfoBarVisible: boolean
  quoteToken: SuspenseResource<TokenInfo>
  latestPriceChange: SuspenseResource<
    [prev: undefined | PriceChange, curr: PriceChange]
  >
  hoveredPriceChangeObservable: Observable<null | OnHoveringPriceChangeHoveringData>
  selectedHistoricalTimeWindow: HistoricalPriceTimeWindow
  onSelectedHistoricalTimeWindow: (
    timeWindow: HistoricalPriceTimeWindow,
  ) => void
}> = props => {
  const spacing = useSpacing()
  const colors = useColors()
  const { $t } = useIntl()

  const [hoveredPriceChange, setHoveredPriceChange] =
    useState<null | OnHoveringPriceChangeHoveringData>(null)

  useEffect(() => {
    const sub = props.hoveredPriceChangeObservable.subscribe(
      setHoveredPriceChange,
    )
    return () => sub.unsubscribe()
  }, [props.hoveredPriceChangeObservable])

  return (
    <View className="flex-row items-center" style={props.style}>
      <View
        className={
          "flex-row items-center bg-white shadow px-3 py-1 space-x-2 rounded-full"
        }
      >
        {allTimeWindows.map(timeWindow => (
          <TouchableOpacity
            key={timeWindow}
            onPress={() => {
              props.onSelectedHistoricalTimeWindow(timeWindow)
            }}
          >
            <Text
              className={`text-sm ${
                timeWindow === props.selectedHistoricalTimeWindow
                  ? "font-bold"
                  : "font-regular"
              }`}
            >
              {timeWindow.toUpperCase()}
            </Text>
          </TouchableOpacity>
        ))}
      </View>

      {props.isInfoBarVisible && (
        <Spensor>
          {() => {
            const quoteToken = readResource(props.quoteToken)
            const [prevPriceChange, currPriceChange] = readResource(
              props.latestPriceChange,
            )

            const showingPriceChange: OnHoveringPriceChangeHoveringData =
              hoveredPriceChange ?? {
                currPoint: currPriceChange,
                prevPoint: prevPriceChange,
                nextPoint: undefined,
              }

            return (
              <Text
                style={{
                  marginLeft: spacing(2.5),
                  fontSize: 10,
                  fontWeight: "600",
                  color: colors("gray-700"),
                }}
              >
                {$t(
                  defineMessage({
                    defaultMessage:
                      "<fragment>O: {open}</fragment>" +
                      "<fragment>H: {high}</fragment>" +
                      "<fragment>L: {low}</fragment>" +
                      "<fragment>C: {close}</fragment>",
                    description: "TradeScreen/ChartPanel/price change title",
                  }),
                  {
                    fragment: contents => (
                      <Text style={{ marginHorizontal: spacing(1.5) }}>
                        {contents}
                      </Text>
                    ),
                    open: (
                      <TokenCountWithDirection
                        token={quoteToken}
                        price={showingPriceChange.currPoint.start}
                        prevPrice={showingPriceChange.prevPoint?.start}
                      />
                    ),
                    close: (
                      <TokenCountWithDirection
                        token={quoteToken}
                        price={showingPriceChange.currPoint.end}
                        prevPrice={showingPriceChange.prevPoint?.end}
                      />
                    ),
                    high: (
                      <TokenCountWithDirection
                        token={quoteToken}
                        price={showingPriceChange.currPoint.high}
                        prevPrice={showingPriceChange.prevPoint?.high}
                      />
                    ),
                    low: (
                      <TokenCountWithDirection
                        token={quoteToken}
                        price={showingPriceChange.currPoint.low}
                        prevPrice={showingPriceChange.prevPoint?.low}
                      />
                    ),
                    ask: (
                      <TokenCountWithDirection
                        token={quoteToken}
                        price={showingPriceChange.currPoint.highestAsk}
                        prevPrice={showingPriceChange.prevPoint?.highestAsk}
                      />
                    ),
                    bid: (
                      <TokenCountWithDirection
                        token={quoteToken}
                        price={showingPriceChange.currPoint.lowestBid}
                        prevPrice={showingPriceChange.prevPoint?.lowestBid}
                      />
                    ),
                  },
                )}
              </Text>
            )
          }}
        </Spensor>
      )}
    </View>
  )
}

const TokenCountWithDirection: FC<{
  token: TokenInfo
  price: BigNumber
  prevPrice?: BigNumber
}> = props => {
  const color =
    props.prevPrice == null || BigNumber.isEq(props.price, props.prevPrice)
      ? ""
      : BigNumber.isGt(props.price, props.prevPrice)
      ? Colors.increaseColor
      : Colors.decreaseColor

  return (
    <TokenCount style={{ color }} token={props.token} count={props.price} />
  )
}
