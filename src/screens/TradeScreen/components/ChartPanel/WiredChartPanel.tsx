import { action } from "mobx"
import { FC } from "react"
import { StyleProp, View, ViewStyle } from "react-native"
import { Subject, throttleTime } from "rxjs"
import { OnHoveringPriceChangeHoveringData } from "../../../../components/LightweightCharts/LightweightCharts.types"
import { useAppEnvStore } from "../../../../stores/AppEnvStore/useAppEnvStore"
import { useTradeStore } from "../../../../stores/TradeStore/useTradeStore"
import { useCreation } from "../../../../utils/reactHelpers/useCreation"
import { suspenseResource } from "../../../../utils/SuspenseResource"
import { ChartPanel } from "./ChartPanel"
import { TopInfoBar } from "./_/TopInfoBar"

export const WiredChartPanel: FC<{
  style?: StyleProp<ViewStyle>
}> = props => {
  const store = useTradeStore()
  const appEnv = useAppEnvStore()

  const hoveredPriceChangeObservable = useCreation(
    () => new Subject<null | OnHoveringPriceChangeHoveringData>(),
    [],
  )

  return (
    <View style={props.style}>
      <ChartPanel
        key={store.currentPriceId$ + store.chart.historicalTimeWindow}
        isSpreadRangeVisible={appEnv.appEnv$.trade_ShowSpreadRange}
        quoteToken={suspenseResource(() => store.anchorTokenInfo$)}
        priceChanges={suspenseResource(() => store.chart.priceChange$)}
        onHoveringPriceChange={data => {
          hoveredPriceChangeObservable.next(data)
        }}
      />

      <TopInfoBar
        className={"absolute top-2 left-2 mr-[100px]"}
        isInfoBarVisible={appEnv.appEnv$.trade_ShowSpreadRange}
        hoveredPriceChangeObservable={hoveredPriceChangeObservable.pipe(
          throttleTime(100), // without this, the page might stale and freeze for a few seconds
        )}
        latestPriceChange={suspenseResource(() => [
          store.chart.lastFragmentPriceChange.value$,
          store.chart.currPriceChange.value$,
        ])}
        quoteToken={suspenseResource(() => store.anchorTokenInfo$)}
        selectedHistoricalTimeWindow={store.chart.historicalTimeWindow}
        onSelectedHistoricalTimeWindow={action(timeWindow => {
          store.chart.historicalTimeWindow = timeWindow
        })}
      />
    </View>
  )
}
