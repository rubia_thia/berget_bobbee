import clsx from "clsx"
import { FC, useState } from "react"
import { defineMessage, useIntl } from "react-intl"
import {
  StyleProp,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import Svg, { Path } from "react-native-svg"
import { BlueButtonVariant } from "../../../components/Button/BlueButtonVariant"
import { RedButtonVariant } from "../../../components/Button/RedButtonVariant"
import { Button } from "../../../components/ButtonFramework/Button"
import {
  CardBoxModalContent,
  TitleBar,
} from "../../../components/CardBoxModalContent/CardBoxModalContent"
import { PercentNumber } from "../../../components/PercentNumber"
import { useColors } from "../../../components/Themed/color"
import { useSpacing } from "../../../components/Themed/spacing"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { FCS } from "../../../utils/reactHelpers/types"
import {
  safeReadResource,
  SuspenseResource,
} from "../../../utils/SuspenseResource"

export interface EditSlippageModalContentProps {
  style?: StyleProp<ViewStyle>
  slippage: SuspenseResource<BigNumber>
  recommendedSlippage: BigNumber
  onDismiss: () => void
  onSubmit: (slippage: BigNumber) => void
}

const RecommendedCell: FCS<{
  selected: boolean
  slippage: BigNumber
  onSelect: () => void
}> = ({ slippage, selected, onSelect, style }) => {
  const { $t } = useIntl()
  return (
    <TouchableOpacity
      onPress={onSelect}
      className={clsx(
        "flex-row items-center px-3 py-2.5 rounded-lg",
        selected ? "border-2 border-blue-600" : "border border-gray-200",
      )}
      style={style}
    >
      <View className="flex-1">
        <Text className="text-gray-900">
          {$t(
            defineMessage({
              defaultMessage: "Recommended",
              description: "TradeScreen/Edit slippage modal",
            }),
          )}
        </Text>
        <Text className="text-xs font-medium text-gray-500">
          {$t(
            defineMessage({
              defaultMessage: "99% of users select this setting",
              description: "TradeScreen/Edit slippage modal",
            }),
          )}
        </Text>
      </View>
      <PercentNumber
        className="text-gray-900 text-xl font-semibold"
        number={slippage}
      />
    </TouchableOpacity>
  )
}

const CustomizeCell: FCS<{
  selected: boolean
  error: boolean
  text: string
  onTextChange: (text: string) => void
  onFocus: () => void
}> = ({ onFocus, onTextChange, text, selected, error, style }) => {
  const { $t } = useIntl()
  const colors = useColors()
  return (
    <View
      className={clsx(
        "flex-row items-center px-3 py-2.5 rounded-lg",
        error
          ? "border-2 border-red-600 bg-red-50"
          : selected
          ? "border-2 border-blue-600"
          : "border border-gray-200",
      )}
      style={style}
    >
      <Text className="text-gray-900">
        {$t(
          defineMessage({
            defaultMessage: "Customize",
            description: "TradeScreen/Edit slippage modal",
          }),
        )}
      </Text>
      <TextInput
        defaultValue={text}
        placeholder="0.0%"
        placeholderTextColor={colors("gray-300")}
        keyboardType="decimal-pad"
        className={clsx(
          "flex-1 text-right text-gray-900 text-xl font-semibold ml-2",
          error && "text-red-600",
        )}
        style={{ borderWidth: 0, outline: 0 } as any}
        onFocus={onFocus}
        onChangeText={onTextChange}
      />
    </View>
  )
}

export const EditSlippageModalContent: FC<
  EditSlippageModalContentProps
> = props => {
  const spacing = useSpacing()

  const slippage = safeReadResource(props.slippage) ?? props.recommendedSlippage
  const [isRecommended, setIsRecommended] = useState(
    BigNumber.isEq(slippage, props.recommendedSlippage),
  )
  const [customized, setCustomized] = useState(
    isRecommended ? "" : BigNumber.toString(BigNumber.mul(100)(slippage)),
  )

  const { $t } = useIntl()

  const error = Boolean(
    !isRecommended &&
      customized &&
      (Number.isNaN(Number(customized)) ||
        Number(customized) >= 100 ||
        Number(customized) < 0),
  )
  return (
    <CardBoxModalContent
      style={props.style}
      padding={spacing(4)}
      onClose={props.onDismiss}
    >
      <View className="space-y-2">
        <TitleBar>
          <Text>
            {$t(
              defineMessage({
                defaultMessage: "Transaction Setting",
                description: "TradeScreen/Edit slippage modal/modal title",
              }),
            )}
          </Text>
        </TitleBar>
        <Text className="text-sm leading-5">
          {$t(
            defineMessage({
              defaultMessage: "Slippage Tolerance",
              description: "TradeScreen/Edit slippage modal/modal title",
            }),
          )}
        </Text>
        <RecommendedCell
          onSelect={() => setIsRecommended(true)}
          selected={isRecommended}
          slippage={props.recommendedSlippage}
        />
        <CustomizeCell
          selected={!isRecommended}
          text={customized}
          error={error}
          onFocus={() => setIsRecommended(false)}
          onTextChange={setCustomized}
        />
        {error && (
          <View className="items-center space-x-1 flex-row">
            <Svg width="16" height="16" viewBox="0 0 16 16">
              <Path
                d="M8 0C3.582 0 0 3.582 0 8C0 12.418 3.582 16 8 16C12.418 16 16 12.418 16 8C16 3.582 12.418 0 8 0ZM11.0647 11.9693L8.004 8.936L4.96733 12L4.03067 11.0633L7.062 8.002L4 4.96733L4.93667 4.03067L7.99533 7.05933L11.0253 4L11.9693 4.93533L8.93867 7.99333L12 11.0253L11.0647 11.9693Z"
                fill="#EF4444"
              />
            </Svg>
            <Text className="text-sm text-red-500">
              {$t(
                defineMessage({
                  defaultMessage: "Enter a valid slippage percentage",
                  description: "TradeScreen/Edit slippage modal/modal title",
                }),
              )}
            </Text>
          </View>
        )}
        <Button
          disabled={error || (!isRecommended && !customized)}
          Variant={error ? RedButtonVariant : BlueButtonVariant}
          onPress={() => {
            props.onSubmit(
              isRecommended
                ? props.recommendedSlippage
                : BigNumber.from(Number(customized) / 100),
            )
          }}
        >
          {$t(
            defineMessage({
              defaultMessage: "Confirm",
              description: "TradeScreen/Edit slippage modal/modal title",
            }),
          )}
        </Button>
      </View>
    </CardBoxModalContent>
  )
}
