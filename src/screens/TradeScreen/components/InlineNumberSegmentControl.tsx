import { ReactNode } from "react"
import {
  StyleProp,
  Text,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import { useColors } from "../../../components/Themed/color"
import { useSpacing } from "../../../components/Themed/spacing"
import {
  PaddingStyle,
  useNormalizePaddingStyle,
} from "../../../utils/styleHelpers/PaddingStyle"
import {
  safeReadResource,
  SuspenseResource,
} from "../../../utils/SuspenseResource"
import { OneOrMore } from "../../../utils/types"

export interface InlineNumberSegmentControlItem<T> {
  labelText: ReactNode
  value: T
  disabled?: boolean
}

// eslint-disable-next-line @typescript-eslint/no-unnecessary-type-constraint
export const InlineNumberSegmentControl = <T extends any>(props: {
  style?: StyleProp<ViewStyle>
  textStyle?: StyleProp<TextStyle>
  itemPadding?: number | PaddingStyle
  items: OneOrMore<InlineNumberSegmentControlItem<T>>
  selectedValue: SuspenseResource<null | { value: T }>
  onSelectedValueChange: (newValue: InlineNumberSegmentControlItem<T>) => void
  valueEquals?: (a: T, b: T) => boolean
}): JSX.Element => {
  const colors = useColors()
  const spacing = useSpacing()

  const valueEquals = props.valueEquals ?? Object.is
  const selectedValue = safeReadResource(props.selectedValue) ?? null

  const itemPaddingStyle = useNormalizePaddingStyle(props.itemPadding)

  return (
    <View
      style={[
        props.style,
        {
          flexDirection: "row",
          borderRadius: 6,
          paddingHorizontal: spacing(1),
          borderWidth: 1,
          borderColor: colors("gray-200"),
          backgroundColor: colors("blue-50"),
        },
      ]}
    >
      {props.items.map((item, idx) => {
        const isSelected =
          selectedValue == null
            ? false
            : valueEquals(item.value, selectedValue.value)

        return (
          <TouchableOpacity
            key={idx}
            style={[
              {
                flex: 1,
                alignItems: "center",
                paddingTop: spacing(2.5),
                paddingBottom: spacing(2.5),
              },
              itemPaddingStyle,
            ]}
            disabled={item.disabled}
            onPress={() => props.onSelectedValueChange(item)}
          >
            <Text
              style={[
                {
                  fontSize: 12,
                  lineHeight: 16,
                  fontWeight: "600",
                  color: isSelected ? colors("blue-600") : colors("gray-500"),
                },
                props.textStyle,
                item.disabled && { opacity: 0.3 },
              ]}
            >
              {item.labelText}
            </Text>
          </TouchableOpacity>
        )
      })}
    </View>
  )
}
