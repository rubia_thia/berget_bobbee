import { ComponentType } from "react"
import { Text } from "react-native"
import { Spensor } from "../../../components/Spensor"
import { TokenCountAsCurrency } from "../../../components/TextTokenCount"
import { useSpacing } from "../../../components/Themed/spacing"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { readResource, SuspenseResource } from "../../../utils/SuspenseResource"
import { TokenInfo } from "../../../utils/TokenInfo"

export type NumberChanging = [
  from: SuspenseResource<BigNumber>,
  to: SuspenseResource<BigNumber>,
]

export function NumberChangingText(props: {
  token: TokenInfo
  changing: NumberChanging
  NumberFormatter?: ComponentType<{ value: BigNumber }>
}): JSX.Element {
  const spacing = useSpacing()

  const [from, to] = props.changing

  const NumberFormatter =
    props.NumberFormatter ??
    ((p: { value: BigNumber }): JSX.Element => (
      <TokenCountAsCurrency token={props.token} count={p.value} />
    ))

  return (
    <>
      <Text style={{ opacity: 0.8 }}>
        <Spensor fallback="--">
          {() => <NumberFormatter value={readResource(from)} />}
        </Spensor>
      </Text>
      <Text style={{ marginHorizontal: spacing(1) }}>→</Text>
      <Text>
        <Spensor fallback="--">
          {() => <NumberFormatter value={readResource(to)} />}
        </Spensor>
      </Text>
    </>
  )
}
