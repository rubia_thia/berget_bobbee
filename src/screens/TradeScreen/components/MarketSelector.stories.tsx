import { ComponentMeta } from "@storybook/react"
import {
  BackgroundColor,
  CardContainer,
} from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { noop } from "../../../utils/fnHelpers"
import { TokenInfoPresets } from "../../../utils/TokenInfoPresets/TokenInfoPresets"
import { MarketSelector } from "./MarketSelector"

export default {
  title: "Page/TradeScreen/MarketSelector",
  component: MarketSelector,
  decorators: [CardContainer({ margin: 10, padding: 10 }), BackgroundColor()],
} as ComponentMeta<typeof MarketSelector>

const tpl = withTemplate(MarketSelector, {
  tokenParis: [
    {
      value: "ETH_USD",
      pair: {
        baseToken: TokenInfoPresets.MockBUSD,
        quoteToken: TokenInfoPresets.MockUSDC,
      },
    },
    {
      value: "BTC_USD",
      pair: {
        baseToken: TokenInfoPresets.MockBTC,
        quoteToken: TokenInfoPresets.MockUSDC,
      },
    },
  ],
  onSelect: noop,
})

export const Normal = tpl()
