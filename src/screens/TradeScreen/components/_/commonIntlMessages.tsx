import { ReactNode } from "react"
import { defineMessage, IntlFormatters } from "react-intl"
import { formatTokenCount } from "../../../../components/TokenCount"
import { TokenName } from "../../../../components/TokenName"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { TokenInfo } from "../../../../utils/TokenInfo"
import { checkNever } from "../../../../utils/typeHelpers"
import { MarginManipulation, OrderDirection, OrderType } from "../../types"

export const specialPriceNone$t = defineMessage({
  defaultMessage: "None",
  description: "TradeScreen/Special price: None",
})

export const specialPriceMarketPrice$t = defineMessage({
  defaultMessage: "Market Price",
  description: "TradeScreen/Special price: Market Price",
})

export const avbl$t = defineMessage({
  defaultMessage: "Avbl:",
  description: "TradeScreen/Common/token balance label",
})

export const placeholderPrice$t = defineMessage({
  defaultMessage: "Price",
  description: "TradeScreen/Order Creation/SL TP price input placeholder",
})

export namespace marginManipulation$t {
  export const increase = defineMessage({
    defaultMessage: "Deposit",
    description:
      "TradeScreen/Common/collateral manipulation direction: Increase",
  })

  export const decrease = defineMessage({
    defaultMessage: "Withdraw",
    description:
      "TradeScreen/Common/collateral manipulation direction: Decrease",
  })

  export const fromMarginManipulation = (
    { $t }: IntlFormatters<ReactNode>,
    manipulation: MarginManipulation,
  ): ReactNode => {
    switch (manipulation) {
      case MarginManipulation.Increase:
        return $t(increase)
      case MarginManipulation.Decrease:
        return $t(decrease)
      default:
        return checkNever(manipulation)
    }
  }
}

export namespace orderDirection$t {
  export const buy = defineMessage({
    defaultMessage: "Long",
    description: "TradeScreen/Common/order direction",
  })

  export const sell = defineMessage({
    defaultMessage: "Short",
    description: "TradeScreen/Common/order direction",
  })

  export const fromOrderDirection = (
    { $t }: IntlFormatters<ReactNode>,
    dir: OrderDirection,
  ): string => {
    switch (dir) {
      case OrderDirection.Long:
        return $t(buy)
      case OrderDirection.Short:
        return $t(sell)
      default:
        checkNever(dir)
        return $t(buy)
    }
  }
}

export namespace orderType$t {
  export const limit = defineMessage({
    defaultMessage: "Limit",
    description: 'TradeScreen/Common/order type "limit"',
  })

  export const market = defineMessage({
    defaultMessage: "Market",
    description: 'TradeScreen/Common/order type "market"',
  })

  export const stopLimit = defineMessage({
    defaultMessage: "Stop Limit",
    description: 'TradeScreen/Common/order type "stop limit"',
  })

  export const stopDashLimit = defineMessage({
    defaultMessage: "Stop-Limit",
    description: 'TradeScreen/Common/order type "stop-limit"',
  })

  export const fromOrderType = (
    { $t }: IntlFormatters<ReactNode>,
    type: OrderType,
  ): string => {
    switch (type) {
      case OrderType.Market:
        return $t(market)
      case OrderType.Limit:
        return $t(limit)
      default:
        checkNever(type)
        return $t(market)
    }
  }
}

export namespace commonErrorMessage$t {
  export const insufficientBalance = (
    { $t }: IntlFormatters<ReactNode>,
    token: TokenInfo,
  ): ReactNode =>
    $t(
      defineMessage({
        defaultMessage: "Insufficient {token} balance",
        description: "TradeScreen/Common/error message",
      }),
      {
        token: <TokenName token={token} />,
      },
    )

  export const enterAmount = defineMessage({
    defaultMessage: "Enter amount",
    description: "TradeScreen/Common/error message",
  })

  export const enterPrice = defineMessage({
    defaultMessage: "Enter price",
    description: "TradeScreen/Common/error message",
  })

  export const enterPosition = defineMessage({
    defaultMessage: "Enter position",
    description: "TradeScreen/Common/error message",
  })

  export const insufficientPositionSize = defineMessage({
    defaultMessage: "Insufficient position",
    description: "TradeScreen/Common/error message",
  })

  export const takeProfitPriceExceedMaxPnl = (
    { $t }: IntlFormatters<ReactNode>,
    token: TokenInfo,
    orderDirection: OrderDirection,
    maxPrice: BigNumber,
  ): ReactNode =>
    $t(
      defineMessage({
        defaultMessage:
          "Take Profit price should be {orderDirection, select, long {less} other {more}} than {maxPrice}",
        description: "TradeScreen/Common/error message",
      }),
      { orderDirection, maxPrice: formatTokenCount(token, maxPrice) },
    )

  export const leverageTooHigh = defineMessage({
    defaultMessage: "Leverage too high",
    description: "TradeScreen/Common/error message",
  })

  export const leverageTooLow = defineMessage({
    defaultMessage: "Leverage too low",
    description: "TradeScreen/Common/error message",
  })

  export const exceededMargin = defineMessage({
    defaultMessage: "Exceeded Collateral",
    description: "TradeScreen/Common/error message",
  })

  export const positionNotFound = defineMessage({
    defaultMessage: "Position is no longer existed",
    description: "TradeScreen/Common/error message",
  })
}
