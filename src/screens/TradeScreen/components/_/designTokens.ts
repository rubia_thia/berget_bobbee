import { PriceChangeDirection } from "../../types"

export namespace Colors {
  export function fromPriceChangeDirection(
    direction: PriceChangeDirection,
  ): undefined | string {
    return direction === PriceChangeDirection.Down
      ? Colors.lossColor
      : Colors.profitColor
  }

  const greenColor = "#059669"
  const redColor = "#EF4444"

  export const profitColor = greenColor
  export const lossColor = redColor

  export const depositColor = greenColor
  export const withdrawColor = redColor

  export const increaseColor = greenColor
  export const decreaseColor = redColor

  export const longColor = greenColor
  export const shortColor = redColor
}
