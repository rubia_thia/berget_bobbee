import { forwardRef, ReactNode, Ref, useImperativeHandle, useRef } from "react"
import {
  LayoutChangeEvent,
  StyleProp,
  Text,
  TextInputProps,
  TextStyle,
  View,
} from "react-native"
import {
  NumberInput,
  NumberInputRefValue,
} from "../../../components/NumberInput/NumberInput"
import { SpensorR } from "../../../components/Spensor"
import { useColors } from "../../../components/Themed/color"
import { useSpacing } from "../../../components/Themed/spacing"
import { TokenCount } from "../../../components/TokenCount"
import { InputStyleGuide } from "../../../components/TokenInput/InputStyleGuide"
import { TokenName } from "../../../components/TokenName"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { FCC } from "../../../utils/reactHelpers/types"
import {
  PaddingStyle,
  useNormalizePaddingStyle,
} from "../../../utils/styleHelpers/PaddingStyle"
import {
  safeReadResource,
  SuspenseResource,
} from "../../../utils/SuspenseResource"
import { TokenInfo } from "../../../utils/TokenInfo"
import { checkObjEmpty } from "../../../utils/typeHelpers"

export interface OneLineTokenInputRefValue {
  focus(): void
}

export interface OneLineTokenInputProps {
  containerRef?: Ref<View>

  style?: TextInputProps["style"]
  padding?: number | PaddingStyle
  error?: boolean

  token: TokenInfo

  leftArea: ReactNode
  rightAreaText?: ReactNode

  onFocus?: () => void
  onBlur?: () => void
  onLayout?: (e: LayoutChangeEvent) => void

  count: SuspenseResource<null | BigNumber>
  onCountChange?: (newValue: null | BigNumber) => void
}

export const OneLineTokenInput = forwardRef<
  OneLineTokenInputRefValue,
  OneLineTokenInputProps
>((props, ref) => {
  const spacing = useSpacing()
  const colors = useColors()

  const paddingStyle = useNormalizePaddingStyle(props.padding)

  const tokenCountInputRef = useRef<NumberInputRefValue>(null)

  const token = safeReadResource(props.token)
  const tokenPrecision =
    token == null ? TokenInfo.fallbackPrecision : TokenInfo.getPrecision(token)

  useImperativeHandle(ref, () => ({
    focus() {
      tokenCountInputRef.current?.focus()
    },
  }))

  return (
    <InputStyleGuide
      error={!!props.error}
      readonly={props.onCountChange == null}
      hasValue={props.count != null}
    >
      {({
        textColor,
        borderColor,
        backgroundColor,
        onFocus,
        onBlur,
        ...rest
      }) => {
        checkObjEmpty(rest)
        const numberTextStyle = {
          color: textColor,
          fontSize: 24,
          lineHeight: 32,
          textAlign: "right",
        } as const
        return (
          <View
            ref={props.containerRef}
            style={[
              props.style,
              {
                paddingLeft: spacing(3),
                paddingRight: spacing(3),
                paddingTop: spacing(2.5),
                paddingBottom: spacing(2.5),
                flexDirection: "row",
                alignItems: "center",
                borderWidth: 1,
                borderColor,
                borderRadius: 4,
                backgroundColor,
              },
              paddingStyle,
            ]}
            onLayout={props.onLayout}
          >
            <View style={{ marginRight: spacing(2.5) }}>{props.leftArea}</View>

            <View style={{ flex: 1 }}>
              {props.onCountChange == null ? (
                <Text style={numberTextStyle}>
                  <SpensorR read={{ count: props.count }}>
                    {({ count }) =>
                      count == null ? null : (
                        <TokenCount token={props.token} count={count} />
                      )
                    }
                  </SpensorR>
                </Text>
              ) : (
                <NumberInput
                  ref={tokenCountInputRef}
                  style={[numberTextStyle, { outline: "none" }]}
                  placeholder={"0.0"}
                  precision={tokenPrecision}
                  value={props.count}
                  onChange={props.onCountChange}
                  onFocus={() => {
                    onFocus()
                    props.onFocus?.()
                  }}
                  onBlur={() => {
                    onBlur()
                    props.onBlur?.()
                  }}
                />
              )}
            </View>

            <Text
              style={{
                marginLeft: spacing(2.5),
                fontSize: 14,
                lineHeight: 20,
                color: colors("gray-500"),
              }}
            >
              {props.rightAreaText ?? <TokenName token={props.token} />}
            </Text>
          </View>
        )
      }}
    </InputStyleGuide>
  )
})

export const LeftAreaLabelText: FCC<{
  style?: StyleProp<TextStyle>
}> = props => {
  const colors = useColors()
  return (
    <Text
      style={[
        { fontSize: 16, lineHeight: 24, color: colors("gray-900") },
        props.style,
      ]}
    >
      {props.children}
    </Text>
  )
}
