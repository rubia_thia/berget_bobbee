import { FC, ReactNode } from "react"
import { defineMessage, IntlShape, useIntl } from "react-intl"
import { StyleProp, Text, TextStyle, View, ViewStyle } from "react-native"
import { BlueButtonVariant } from "../../../components/Button/BlueButtonVariant"
import { RedButtonVariant } from "../../../components/Button/RedButtonVariant"
import { Button } from "../../../components/ButtonFramework/Button"
import { SmartLoadableButton } from "../../../components/ButtonFramework/LoadableButton"
import { CardInset } from "../../../components/CardBox/CardInset"
import {
  CardBoxModalContent,
  TitleBar,
} from "../../../components/CardBoxModalContent/CardBoxModalContent"
import { SpensorR } from "../../../components/Spensor"
import { Stack } from "../../../components/Stack"
import { TokenCountAsCurrency } from "../../../components/TextTokenCount"
import { useColors } from "../../../components/Themed/color"
import { useSpacing } from "../../../components/Themed/spacing"
import { TokenCount } from "../../../components/TokenCount"
import { hasAny } from "../../../utils/arrayHelpers"
import { findError } from "../../../utils/errorHelpers"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { FCC } from "../../../utils/reactHelpers/types"
import {
  safeReadResource,
  SuspenseResource,
} from "../../../utils/SuspenseResource"
import { TokenInfo } from "../../../utils/TokenInfo"
import { OrderDirection } from "../types"
import {
  ActionTriggerConditionNumberDisplayingText,
  percentageLimitInRange100,
} from "./ActionTriggerConditionNumberDisplayingText"
import { LeftAreaLabelText, OneLineTokenInput } from "./OneLineTokenInput"
import { ActionTrigger, ActionTriggerPatch } from "./OrderCreation/types"
import { OrderDirectionBadge } from "./OrderDirectionBadge"
import { TokenNameStack } from "./TokenNameStack"
import {
  UpdateSLTPTriggerModalFormError,
  updateSLTPTriggerModalFormErrorMessages$t,
  UpdateSLTPTriggerModalFormErrorType,
} from "./UpdateSLTPTriggerModalContent.types"
import { Colors } from "./_/designTokens"

export interface UpdateSLTPTriggerModalContentProps {
  style?: StyleProp<ViewStyle>
  onDismiss: () => void
  onSubmit: () => void | Promise<void>

  errors: SuspenseResource<undefined | UpdateSLTPTriggerModalFormError[]>
  baseToken: TokenInfo
  quoteToken: TokenInfo
  leverage: BigNumber
  orderDirection: OrderDirection
  liquidationPrice: BigNumber

  stopLossTrigger: null | ActionTrigger
  stopLossTriggerEstimatedPnl: SuspenseResource<BigNumber>
  stopLossPricePatch: null | ActionTriggerPatch.Price
  onStopLossPriceChange: (newValue: null | ActionTriggerPatch.Price) => void

  takeProfitTrigger: null | ActionTrigger
  takeProfitTriggerEstimatedPnl: SuspenseResource<BigNumber>
  takeProfitPricePatch: null | ActionTriggerPatch.Price
  onTakeProfitPriceChange: (newValue: null | ActionTriggerPatch.Price) => void
}

export const UpdateSLTPTriggerModalContent: FC<
  UpdateSLTPTriggerModalContentProps
> = props => {
  const spacing = useSpacing()
  const colors = useColors()

  const intl = useIntl()
  const { $t } = intl

  const errors = safeReadResource(props.errors)

  const stopLossError = findError(errors, [
    UpdateSLTPTriggerModalFormErrorType.StopLossPercentageOutOfRange,
    UpdateSLTPTriggerModalFormErrorType.StopLossShouldGteLiquidationPrice,
    UpdateSLTPTriggerModalFormErrorType.StopLossShouldLteLiquidationPrice,
  ] as const)
  const stopLossErrorText =
    stopLossError != null &&
    updateSLTPTriggerModalFormErrorMessages$t.fromError(
      intl,
      stopLossError,
      props.quoteToken,
      props.liquidationPrice,
    )

  const takeProfitError = findError(errors, [
    UpdateSLTPTriggerModalFormErrorType.TakeProfitPercentageOutOfRange,
  ] as const)
  const takeProfitErrorText =
    takeProfitError != null &&
    updateSLTPTriggerModalFormErrorMessages$t.fromError(
      intl,
      takeProfitError,
      props.quoteToken,
      props.liquidationPrice,
    )

  return (
    <CardBoxModalContent
      style={props.style}
      padding={spacing(4)}
      onClose={props.onDismiss}
    >
      <Stack space={spacing(5)}>
        <TitleBar>
          {$t(
            defineMessage({
              defaultMessage: "Update SL/TP",
              description: "TradeScreen/Update SLTP modal/modal title",
            }),
          )}
        </TitleBar>

        <Stack align={"center"} horizontal={true} space={spacing(2.5)}>
          <Text
            style={{
              fontSize: 18,
              fontWeight: "700",
              lineHeight: 28,
              color: colors("gray-900"),
            }}
          >
            <TokenNameStack
              baseToken={props.baseToken}
              quoteToken={props.quoteToken}
            />
          </Text>

          <Text
            style={{
              fontSize: 18,
              lineHeight: 28,
              color: colors("gray-500"),
            }}
          >
            {Number(BigNumber.toNumber(props.leverage).toFixed(2))}x
          </Text>

          <OrderDirectionBadge direction={props.orderDirection} />
        </Stack>

        <View>
          <OneLineTokenInput
            padding={{ paddingVertical: spacing(1.5) }}
            error={!!stopLossError}
            token={props.quoteToken}
            leftArea={
              <LeftAreaLabelText style={{ maxWidth: 180 }}>
                {$t(
                  defineMessage({
                    defaultMessage: "Stop Loss {detail}",
                    description:
                      "TradeScreen/Update SLTP modal/SL setting label",
                  }),
                  {
                    detail: (
                      <LeftAreaLabelText style={{ color: Colors.lossColor }}>
                        (
                        <ActionTriggerConditionNumberDisplayingText
                          quoteToken={props.quoteToken}
                          trigger={props.stopLossTrigger}
                          percentageLimitInRange={percentageLimitInRange100}
                        />
                        )
                      </LeftAreaLabelText>
                    ),
                  },
                )}
              </LeftAreaLabelText>
            }
            rightAreaText={<RightAreaMarketPriceText />}
            count={props.stopLossPricePatch?.price ?? null}
            onCountChange={n => {
              props.onStopLossPriceChange(
                n == null ? null : { sourceType: "price", price: n },
              )
            }}
          />

          {stopLossErrorText && (
            <ActionTriggerErrorText style={{ marginTop: spacing(2.5) }}>
              {stopLossErrorText}
            </ActionTriggerErrorText>
          )}

          {props.stopLossTrigger != null && !stopLossErrorText && (
            <ActionTriggerDescribeText style={{ marginTop: spacing(2.5) }}>
              {$t<ReactNode>(
                defineMessage({
                  defaultMessage:
                    "When <strong>Mark Price</strong> reaches <strong>{stopLossPrice}</strong>, it will trigger Stop Loss Market order to close this position. Estimated PnL will be <strong>{estimatedPnl}</strong>",
                  description: "TradeScreen/Update SLTP modal/tips text",
                }),
                {
                  strong: children => (
                    <Text style={{ color: colors("gray-900") }}>
                      {children}
                    </Text>
                  ),
                  stopLossPrice: (
                    <TokenCountAsCurrency
                      token={props.quoteToken}
                      count={props.stopLossTrigger.price}
                    />
                  ),
                  estimatedPnl: (
                    <SpensorR
                      read={{
                        stopLossTriggerEstimatedPnl:
                          props.stopLossTriggerEstimatedPnl,
                      }}
                    >
                      {({ stopLossTriggerEstimatedPnl }) => (
                        <TokenCountAsCurrency
                          style={{
                            color: BigNumber.isNegative(
                              stopLossTriggerEstimatedPnl,
                            )
                              ? Colors.lossColor
                              : Colors.profitColor,
                          }}
                          token={props.quoteToken}
                          count={stopLossTriggerEstimatedPnl}
                        />
                      )}
                    </SpensorR>
                  ),
                },
              )}
            </ActionTriggerDescribeText>
          )}
        </View>

        <View>
          <OneLineTokenInput
            padding={{ paddingVertical: spacing(1.5) }}
            error={!!takeProfitError}
            token={props.quoteToken}
            leftArea={
              <LeftAreaLabelText style={{ maxWidth: 180 }}>
                {$t(
                  defineMessage({
                    defaultMessage: "Take Profit {detail}",
                    description:
                      "TradeScreen/Update SLTP modal/TP setting label",
                  }),
                  {
                    detail: (
                      <LeftAreaLabelText style={{ color: Colors.profitColor }}>
                        (
                        <ActionTriggerConditionNumberDisplayingText
                          quoteToken={props.quoteToken}
                          trigger={props.takeProfitTrigger}
                        />
                        )
                      </LeftAreaLabelText>
                    ),
                  },
                )}
              </LeftAreaLabelText>
            }
            rightAreaText={<RightAreaMarketPriceText />}
            count={props.takeProfitPricePatch?.price ?? null}
            onCountChange={n => {
              props.onTakeProfitPriceChange(
                n == null ? null : { sourceType: "price", price: n },
              )
            }}
          />

          {takeProfitErrorText && (
            <ActionTriggerErrorText style={{ marginTop: spacing(2.5) }}>
              {takeProfitErrorText}
            </ActionTriggerErrorText>
          )}

          {props.takeProfitTrigger != null && !takeProfitErrorText && (
            <ActionTriggerDescribeText style={{ marginTop: spacing(2.5) }}>
              {$t<ReactNode>(
                defineMessage({
                  defaultMessage:
                    "When <strong>Mark Price</strong> reaches <strong>{takeProfitPrice}</strong>, it will trigger Take Profit Market order to close this position. Estimated PnL will be <strong>{estimatedPnl}</strong>",
                  description: "TradeScreen/Update SLTP modal/tips text",
                }),
                {
                  strong: children => (
                    <Text style={{ color: colors("gray-900") }}>
                      {children}
                    </Text>
                  ),
                  takeProfitPrice: (
                    <TokenCountAsCurrency
                      token={props.quoteToken}
                      count={props.takeProfitTrigger.price}
                    />
                  ),
                  estimatedPnl: (
                    <SpensorR
                      read={{
                        takeProfitTriggerEstimatedPnl:
                          props.takeProfitTriggerEstimatedPnl,
                      }}
                    >
                      {({ takeProfitTriggerEstimatedPnl }) => (
                        <TokenCountAsCurrency
                          style={{
                            color: BigNumber.isNegative(
                              takeProfitTriggerEstimatedPnl,
                            )
                              ? Colors.lossColor
                              : Colors.profitColor,
                          }}
                          token={props.quoteToken}
                          count={takeProfitTriggerEstimatedPnl}
                        />
                      )}
                    </SpensorR>
                  ),
                },
              )}
            </ActionTriggerDescribeText>
          )}
        </View>

        <CardInset style={{ alignItems: "center" }}>
          <Text>
            {$t(
              defineMessage({
                defaultMessage: "Liquidation price: {price}",
                description: "TradeScreen/Update SLTP modal/liquidation price",
              }),
              {
                price: (
                  <TokenCount
                    color={colors("red-500")}
                    token={props.quoteToken}
                    count={props.liquidationPrice}
                  />
                ),
              },
            )}
          </Text>
        </CardInset>

        <SmartLoadableButton
          disabled={errors != null}
          Variant={
            findError(errors, [
              UpdateSLTPTriggerModalFormErrorType.TakeProfitPercentageOutOfRange,
              UpdateSLTPTriggerModalFormErrorType.StopLossPercentageOutOfRange,
              UpdateSLTPTriggerModalFormErrorType.StopLossShouldGteLiquidationPrice,
              UpdateSLTPTriggerModalFormErrorType.StopLossShouldLteLiquidationPrice,
            ] as const)
              ? RedButtonVariant
              : BlueButtonVariant
          }
          onPress={props.onSubmit}
        >
          <Button.Text>
            {renderButtonErrorMessageText(
              intl,
              props.quoteToken,
              props.orderDirection,
              errors,
            ) ??
              $t(
                defineMessage({
                  defaultMessage: "Update",
                  description: 'TradeScreen/Update SLTP modal/"Update" button',
                }),
              )}
          </Button.Text>
        </SmartLoadableButton>
      </Stack>
    </CardBoxModalContent>
  )
}

const RightAreaMarketPriceText: FC = () => {
  const { $t } = useIntl()
  return (
    <Text style={{ fontSize: 12, maxWidth: 45 }} numberOfLines={2}>
      {$t(
        defineMessage({
          defaultMessage: "Price",
          description: "TradeScreen/Update SLTP modal/input suffix: price",
        }),
      )}
    </Text>
  )
}

const ActionTriggerErrorText: FCC<{
  style?: StyleProp<ViewStyle>
}> = props => {
  const colors = useColors()
  return (
    <ActionTriggerDescribeText
      style={[{ color: colors("red-500") }, props.style]}
    >
      {props.children}
    </ActionTriggerDescribeText>
  )
}

const ActionTriggerDescribeText: FCC<{
  style?: StyleProp<TextStyle>
}> = props => {
  const colors = useColors()
  return (
    <Text
      style={[
        {
          fontSize: 12,
          color: colors("gray-400"),
        },
        props.style,
      ]}
    >
      {props.children}
    </Text>
  )
}

const renderButtonErrorMessageText = (
  intl: IntlShape,
  quoteToken: TokenInfo,
  orderDirection: OrderDirection,
  errors?: UpdateSLTPTriggerModalFormError[],
): null | JSX.Element => {
  if (
    errors == null ||
    !hasAny(errors) ||
    findError(errors, [
      UpdateSLTPTriggerModalFormErrorType.StopLossPercentageOutOfRange,
      UpdateSLTPTriggerModalFormErrorType.StopLossShouldGteLiquidationPrice,
      UpdateSLTPTriggerModalFormErrorType.StopLossShouldLteLiquidationPrice,
      UpdateSLTPTriggerModalFormErrorType.TakeProfitPercentageOutOfRange,
    ] as const)
  ) {
    return null
  }

  return null
}
