import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { noop } from "../../../utils/fnHelpers"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../utils/TokenInfoPresets/TokenInfoPresets"
import { OrderDirection } from "../types"
import { UpdateSLTPTriggerModalContent } from "./UpdateSLTPTriggerModalContent"

export default {
  title: "Page/TradeScreen/UpdateSLTPTriggerModalContent",
  component: UpdateSLTPTriggerModalContent,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof UpdateSLTPTriggerModalContent>

const tpl = withTemplate(UpdateSLTPTriggerModalContent, {
  style: { margin: 10 },
  errors: undefined,
  baseToken: TokenInfoPresets.MockBTC,
  quoteToken: TokenInfoPresets.MockUSDC,
  leverage: BigNumber.from(10),
  orderDirection: OrderDirection.Long,
  liquidationPrice: BigNumber.from(20867.12),

  stopLossTrigger: {
    sourceType: "price",
    price: BigNumber.from(20867.12),
    percentage: BigNumber.from(0.1),
  },
  stopLossTriggerEstimatedPnl: BigNumber.from(-10),
  stopLossPricePatch: null,
  onStopLossPriceChange: noop,

  takeProfitTrigger: {
    sourceType: "price",
    price: BigNumber.from(20867.12),
    percentage: BigNumber.from(0.1),
  },
  takeProfitTriggerEstimatedPnl: BigNumber.from(10),
  takeProfitPricePatch: null,
  onTakeProfitPriceChange: noop,

  onSubmit: noop,
  onDismiss: noop,
})

export const Normal = tpl()
