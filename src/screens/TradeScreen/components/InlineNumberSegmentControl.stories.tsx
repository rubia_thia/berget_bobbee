import { ComponentMeta, Story } from "@storybook/react"
import { ReactNode, useMemo, useState } from "react"
import {
  BackgroundColor,
  CardContainer,
} from "../../../../.storybook/decorators"
import { PercentNumber } from "../../../components/PercentNumber"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { OneOrMore } from "../../../utils/types"
import { InlineNumberSegmentControl } from "./InlineNumberSegmentControl"

export default {
  title: "Page/TradeScreen/InlineNumberSegmentControl",
  component: InlineNumberSegmentControl,
  decorators: [CardContainer({ margin: 10, padding: 10 }), BackgroundColor()],
} as ComponentMeta<typeof InlineNumberSegmentControl>

export const Normal: Story = () => {
  const [value, setValue] = useState<null | BigNumber>(BigNumber.from(0.1))

  const availableOptions: OneOrMore<{
    labelText: ReactNode
    value: null | BigNumber
  }> = useMemo(
    () => [
      {
        labelText: "None",
        value: null,
      },
      {
        labelText: <PercentNumber number={-0.1} />,
        value: BigNumber.from(0.1),
      },
      {
        labelText: <PercentNumber number={-0.25} />,
        value: BigNumber.from(0.25),
      },
      {
        labelText: <PercentNumber number={-0.5} />,
        value: BigNumber.from(0.5),
      },
      {
        labelText: <PercentNumber number={-0.75} />,
        value: BigNumber.from(0.75),
      },
    ],
    [],
  )

  return (
    <InlineNumberSegmentControl
      style={{ flex: 1 }}
      items={availableOptions}
      selectedValue={{ value }}
      onSelectedValueChange={v => {
        setValue(v.value)
      }}
    />
  )
}
