import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { useTransactionNotifier } from "../../../components/TransactionNotifier/WiredTransactionNotifier"
import { useTradeStore } from "../../../stores/TradeStore/useTradeStore"
import { suspenseResource } from "../../../utils/SuspenseResource"
import { checkNever } from "../../../utils/typeHelpers"
import { OpenOrdersTabContent } from "../components/OrderHistory/OpenOrdersTabContent/OpenOrdersTabContent"
import {
  HistoryContentType,
  OrderHistoryFrame,
} from "../components/OrderHistory/OrderHistoryFrame"
import { PositionsTabContent } from "../components/OrderHistory/PositionsTabContent/PositionsTabContent"
import { TradeHistoryTabContent } from "../components/OrderHistory/TradeHistoryTabContent/TradeHistoryTabContent"
import { WiredCancelOrderModal } from "./WiredCancelOrderModal"
import { WiredOrderEditMarginModal } from "./WiredOrderEditMarginModal"
import { WiredOrderEditStopsModal } from "./WiredOrderEditStopsModal"

export const WiredOrderHistory: FC<{
  horizontalMargin?: number
  layoutMode: "1col" | "2col"
}> = ({ horizontalMargin, layoutMode }) => {
  const store = useTradeStore()
  const notifier = useTransactionNotifier()
  const { $t } = useIntl()
  const { orderHistory } = store
  return (
    <>
      <OrderHistoryFrame
        style={{ marginHorizontal: horizontalMargin }}
        positionOrderCount={suspenseResource(
          () => orderHistory.openPositionsLength$,
        )}
        tradeHistoryCount={suspenseResource(
          () => orderHistory.allTradeEventsCount$,
        )}
        openOrderCount={suspenseResource(
          () => orderHistory.limitBookEventsCount$,
        )}
      >
        {({ value, paddingHorizontal }) =>
          value === HistoryContentType.PositionOrders ? (
            <PositionsTabContent
              horizontalScrollable={layoutMode === "1col"}
              horizontalMargin={paddingHorizontal}
              records={suspenseResource(
                () => orderHistory.openPositionsPaginationStore.records$,
              )}
              paginationInfo={suspenseResource(
                () => orderHistory.openPositionsPaginationStore.paginationInfo$,
              )}
              onPaginationChange={p =>
                orderHistory.openPositionsPaginationStore.onPaginationChange(p)
              }
            />
          ) : value === HistoryContentType.OpenOrders ? (
            <OpenOrdersTabContent
              horizontalScrollable={layoutMode === "1col"}
              horizontalMargin={paddingHorizontal}
              records={suspenseResource(
                () => orderHistory.limitBookEventsPaginationStore.records$,
              )}
              paginationInfo={suspenseResource(
                () =>
                  orderHistory.limitBookEventsPaginationStore.paginationInfo$,
              )}
              onPaginationChange={p =>
                orderHistory.limitBookEventsPaginationStore.onPaginationChange(
                  p,
                )
              }
              onCancelOrder={async record => {
                const event = orderHistory.limitBookEvents$.find(
                  e => e.trade.orderHash === record.orderHash,
                )
                if (event == null) return
                orderHistory.closingLimitOrderHashes.add(record.orderHash)
                try {
                  await notifier.broadcastAndShowConfirmation(
                    event.cancelOrder(),
                    $t(
                      defineMessage({
                        defaultMessage: "Close open order",
                        description: "TradeScreen/NotificationTitle",
                      }),
                    ),
                  )
                } finally {
                  orderHistory.closingLimitOrderHashes.delete(record.orderHash)
                }
              }}
            />
          ) : value === HistoryContentType.TradeHistory ? (
            <TradeHistoryTabContent
              horizontalScrollable={layoutMode === "1col"}
              horizontalMargin={paddingHorizontal}
              records={suspenseResource(
                () => orderHistory.allTradeEventsPaginationStore.records$,
              )}
              paginationInfo={suspenseResource(
                () =>
                  orderHistory.allTradeEventsPaginationStore.paginationInfo$,
              )}
              onPaginationChange={p =>
                orderHistory.allTradeEventsPaginationStore.onPaginationChange(p)
              }
            />
          ) : (
            checkNever(value)
          )
        }
      </OrderHistoryFrame>
      <WiredCancelOrderModal />
      <WiredOrderEditMarginModal />
      <WiredOrderEditStopsModal />
    </>
  )
}
