import { action, runInAction } from "mobx"
import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { Dialog } from "../../../components/DialogProvider/Dialog"
import { useTransactionNotifier } from "../../../components/TransactionNotifier/WiredTransactionNotifier"
import { useTradeStore } from "../../../stores/TradeStore/useTradeStore"
import { Result } from "../../../stores/utils/Result"
import { waitFor } from "../../../stores/utils/waitFor"
import { math } from "../../../utils/numberHelpers/bigNumberExpressionParser"
import { suspenseResource } from "../../../utils/SuspenseResource"
import { checkNever } from "../../../utils/typeHelpers"
import { ClosePositionModalContent } from "../components/ClosePositionModalContent/ClosePositionModalContent"

export const WiredCancelOrderModal: FC = () => {
  const { $t } = useIntl()
  const { orderHistory, anchorTokenInfo$ } = useTradeStore()
  const { closingOrder } = orderHistory
  const notifier = useTransactionNotifier()
  if (closingOrder == null) {
    return null
  }
  const { trade, positionSize, closingPrice } = closingOrder
  const onDismiss = action(() => (orderHistory.closingOrder = undefined))
  return (
    <Dialog visible={true} onClose={onDismiss}>
      <ClosePositionModalContent
        error={suspenseResource(() =>
          Result.maybeError(closingOrder.cancelForm$),
        )}
        onDismiss={onDismiss}
        onSubmit={async () => {
          const data = Result.maybeValue(
            await waitFor(() => closingOrder.cancelForm$),
          )
          if (data == null) {
            return
          }
          runInAction(() =>
            orderHistory.closingOrderHashes.push(data.orderHash),
          )
          const operationType = $t(
            defineMessage({
              defaultMessage: "Close position",
              description: "TradeScreen/NotificationTitle",
            }),
          )
          const transaction = await notifier.broadcast(
            closingOrder.cancelOrder(data),
            operationType,
          )
          onDismiss()
          void notifier
            .showConfirmationProgress(transaction, operationType)
            .finally(() => {
              runInAction(
                () =>
                  (orderHistory.closingOrderHashes =
                    orderHistory.closingOrderHashes.filter(
                      a => a !== data.orderHash,
                    )),
              )
            })
        }}
        baseToken={trade.baseTokenInfo$}
        quoteToken={anchorTokenInfo$}
        leverage={trade.leverage}
        orderDirection={trade.orderDirection}
        positionSizeChanging={[
          trade.remainingPosition,
          math`${trade.remainingPosition} - ${positionSize?.amount ?? 0}`,
        ]}
        price={closingPrice}
        onPriceChange={i => closingOrder.setClosingPrice(i)}
        positionSize={positionSize ?? null}
        onPositionSizeChange={action(x => {
          if (closingOrder == null) return
          if (x == null) {
            closingOrder.positionSize = undefined
          } else if (x.type === "percentage") {
            closingOrder.positionSize = {
              percentage: x.percentage,
              amount: math`${trade.remainingPosition} * ${x.percentage}`,
            }
          } else if (x.type === "amount") {
            closingOrder.positionSize = {
              percentage: math`${x.amount} / ${trade.remainingPosition}`,
              amount: x.amount,
            }
          } else {
            checkNever(x)
          }
        })}
      />
    </Dialog>
  )
}
