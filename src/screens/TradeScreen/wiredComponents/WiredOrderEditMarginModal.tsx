import { action } from "mobx"
import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { Dialog } from "../../../components/DialogProvider/Dialog"
import { useTransactionNotifier } from "../../../components/TransactionNotifier/WiredTransactionNotifier"
import { useTradeStore } from "../../../stores/TradeStore/useTradeStore"
import { Result } from "../../../stores/utils/Result"
import { safelyGet, waitFor } from "../../../stores/utils/waitFor"
import { suspenseResource } from "../../../utils/SuspenseResource"
import { EditMarginModalContent } from "../components/EditMarginModalContent"
import { MarginManipulation, OrderDirection } from "../types"

export const WiredOrderEditMarginModal: FC = () => {
  const { $t } = useIntl()
  const { orderHistory } = useTradeStore()
  const { editMargin } = orderHistory
  const notifier = useTransactionNotifier()
  if (editMargin == null) {
    return null
  }
  const onDismiss = action(() => (orderHistory.editMargin = undefined))
  const { trade } = editMargin
  return (
    <Dialog visible={true} onClose={onDismiss}>
      <EditMarginModalContent
        onDismiss={onDismiss}
        error={safelyGet(() => Result.maybeError(editMargin.formData$))}
        onSubmit={async () => {
          const data = await waitFor(() =>
            Result.maybeValue(editMargin.formData$),
          )
          if (data == null) {
            return
          }
          void notifier.broadcastAndShowConfirmation(
            editMargin.changeMargin(data),
            data.direction === MarginManipulation.Increase
              ? $t(
                  defineMessage({
                    defaultMessage: "Deposit Collateral",
                    description: "TradeScreen/NotificationTitle/edit margin",
                  }),
                )
              : $t(
                  defineMessage({
                    defaultMessage: "Withdraw Collateral",
                    description: "TradeScreen/NotificationTitle/edit margin",
                  }),
                ),
          )
        }}
        baseToken={editMargin.trade.baseTokenInfo$}
        quoteToken={editMargin.trade.quoteTokenInfo$}
        orderDirection={
          trade.isLong ? OrderDirection.Long : OrderDirection.Short
        }
        marginManipulation={editMargin.direction}
        onChangeMarginManipulation={action(x => (editMargin.direction = x))}
        positionSize={trade.remainingPosition}
        marginSize={[
          trade.remainingMargin,
          suspenseResource(() => editMargin.margin$),
        ]}
        leverage={[
          trade.leverage,
          suspenseResource(() => editMargin.leverage$),
        ]}
        markPrice={suspenseResource(() => trade.currentMarkPrice$)}
        liquidationPrice={[
          suspenseResource(() => editMargin.liquidationPrice$),
          suspenseResource(() => editMargin?.newLiquidationPrice$),
        ]}
        executionFee={suspenseResource(() => editMargin.executionFee$)}
        maxAvailableTokenToOperate={suspenseResource(() =>
          editMargin.direction === MarginManipulation.Increase
            ? editMargin.maxAmountToDeposit$
            : editMargin.maxAmountToWithdraw$,
        )}
        marginQuoteTokenAmount={suspenseResource(
          () => editMargin.amount.get() ?? null,
        )}
        onMarginQuoteTokenAmountChange={x =>
          editMargin.amount.set(x ?? undefined)
        }
        onPressMaxMarginQuoteTokenAmount={suspenseResource(() => {
          const balance =
            editMargin.direction === MarginManipulation.Increase
              ? editMargin.maxAmountToDeposit$
              : editMargin.maxAmountToWithdraw$
          return () => editMargin.amount.set(balance)
        })}
      />
    </Dialog>
  )
}
