import { action } from "mobx"
import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { Dialog } from "../../../components/DialogProvider/Dialog"
import {
  MessageItemBodyText,
  MessageItemTitleText,
} from "../../../components/MessageProvider/MessageItem"
import { Spensor } from "../../../components/Spensor"
import { TokenCount } from "../../../components/TokenCount"
import { TransactionMessageItem } from "../../../components/TransactionNotifier/TransactionMessageItem"
import {
  RenderExecutedMessageItemFn,
  useTransactionNotifier,
} from "../../../components/TransactionNotifier/WiredTransactionNotifier"
import { useAppEnvStore } from "../../../stores/AppEnvStore/useAppEnvStore"
import { useAuthStore } from "../../../stores/AuthStore/useAuthStore"
import { StableToken } from "../../../stores/CurrencyStore/CurrencyStore.service"
import { useTradeStore } from "../../../stores/TradeStore/useTradeStore"
import { Result } from "../../../stores/utils/Result"
import { safelyGet, waitFor } from "../../../stores/utils/waitFor"
import { noop } from "../../../utils/fnHelpers"
import { FCS } from "../../../utils/reactHelpers/types"
import { suspenseResource } from "../../../utils/SuspenseResource"
import { assertNever } from "../../../utils/types"
import { EditSlippageModalContent } from "../components/EditSlippageModalContent"
import { LimitOrderCreationContent } from "../components/OrderCreation/LimitOrderCreationContent"
import { MarketOrderCreationContent } from "../components/OrderCreation/MarketOrderCreationContent"
import { OrderCreationFrame } from "../components/OrderCreation/OrderCreationFrame"
import { TokenNameStack } from "../components/TokenNameStack"
import {
  orderDirection$t,
  orderType$t,
} from "../components/_/commonIntlMessages"
import { Colors } from "../components/_/designTokens"
import { OrderDirection, OrderType } from "../types"

const WiredOrderCreationContent: FCS<{ gap?: number }> = ({ style, gap }) => {
  const appEnvStore = useAppEnvStore()
  const store = useTradeStore()
  const { orderCreation, baseTokenInfo$ } = store
  if (orderCreation.type === OrderType.Market) {
    return (
      <MarketOrderCreationContent
        style={style}
        gap={gap}
        quoteToken={suspenseResource(() => orderCreation.quoteTokenInfo$)}
        availableQuoteTokens={orderCreation.allAvailableQuoteTokenInfo$}
        onSelectQuoteToken={token =>
          orderCreation.setQuoteToken(token.id as StableToken)
        }
        baseToken={baseTokenInfo$}
        formError={suspenseResource(() => orderCreation.formError$)}
        quoteTokenBalance={suspenseResource(
          () => orderCreation.quoteTokenBalance$,
        )}
        leverageRange={suspenseResource(() => orderCreation.leverageRange$)}
        leverage={suspenseResource(() => orderCreation.leverage.get() ?? null)}
        onLeverageChange={action(leverage =>
          orderCreation.leverage.set(leverage ?? undefined),
        )}
        entryPrice={suspenseResource(() =>
          !appEnvStore.appEnv$.trade_ShowMorePrice
            ? undefined
            : orderCreation.entryPrice$,
        )}
        exitPrice={suspenseResource(() =>
          !appEnvStore.appEnv$.trade_ShowMorePrice
            ? undefined
            : orderCreation.exitPrice$,
        )}
        marketPrice={suspenseResource(() => orderCreation.marketPrice$)}
        markPrice={suspenseResource(() => orderCreation.markPrice$)}
        priceSpreadRate={suspenseResource(() => orderCreation.priceSpreadRate$)}
        slippagePercentage={suspenseResource(
          () => orderCreation.slippage.get() ?? null,
        )}
        onSlippageChange={action(s =>
          orderCreation.slippage.set(s ?? undefined),
        )}
        value={suspenseResource(() => orderCreation.inputMargin.get() ?? null)}
        onValueChange={amount =>
          orderCreation.inputMargin.set(amount ?? undefined)
        }
        onPressMax={suspenseResource(() => {
          const balance$ = orderCreation.quoteTokenBalance$
          return () => orderCreation.inputMargin.set(balance$)
        })}
        stopLossTriggerPercentageOptions={suspenseResource(
          () => orderCreation.stopLossTriggerPercentageOptions$,
        )}
        stopLossTrigger={suspenseResource(
          () => orderCreation.stopLossTriggerDisplay$,
        )}
        onStopLossTriggerChange={action(
          patch => (orderCreation.stopLossTrigger = patch ?? undefined),
        )}
        takeProfitTriggerPercentageOptions={suspenseResource(
          () => orderCreation.takeProfitTriggerPercentageOptions$,
        )}
        takeProfitTrigger={suspenseResource(
          () => orderCreation.takeProfitTriggerDisplay$,
        )}
        onTakeProfitTriggerChange={action(
          patch =>
            (orderCreation.takeProfitTrigger =
              patch ?? orderCreation.defaultTakeProfitTrigger),
        )}
      />
    )
  }
  if (orderCreation.type === OrderType.Limit) {
    return (
      <LimitOrderCreationContent
        style={style}
        gap={gap}
        quoteToken={suspenseResource(() => orderCreation.quoteTokenInfo$)}
        availableQuoteTokens={orderCreation.allAvailableQuoteTokenInfo$}
        onSelectQuoteToken={token =>
          orderCreation.setQuoteToken(token.id as StableToken)
        }
        baseToken={baseTokenInfo$}
        formError={suspenseResource(() => orderCreation.formError$)}
        quoteTokenBalance={suspenseResource(
          () => orderCreation.quoteTokenBalance$,
        )}
        leverageRange={suspenseResource(() => orderCreation.leverageRange$)}
        leverage={suspenseResource(() => orderCreation.leverage.get() ?? null)}
        onLeverageChange={action(leverage =>
          orderCreation.leverage.set(leverage ?? undefined),
        )}
        price={suspenseResource(
          () => safelyGet(() => orderCreation.executePrice$) ?? null,
        )}
        onPriceChange={action(price => {
          orderCreation.inputLimitPriceEdited = true
          orderCreation.inputLimitPrice.set(price ?? undefined)
        })}
        value={suspenseResource(() => orderCreation.inputMargin.get() ?? null)}
        onValueChange={amount =>
          orderCreation.inputMargin.set(amount ?? undefined)
        }
        onPressMax={suspenseResource(() => {
          const balance$ = orderCreation.quoteTokenBalance$
          return () => orderCreation.inputMargin.set(balance$)
        })}
        stopLossTriggerPercentageOptions={suspenseResource(
          () => orderCreation.stopLossTriggerPercentageOptions$,
        )}
        stopLossTrigger={suspenseResource(
          () => orderCreation.stopLossTriggerDisplay$,
        )}
        onStopLossTriggerChange={action(
          patch => (orderCreation.stopLossTrigger = patch ?? undefined),
        )}
        takeProfitTriggerPercentageOptions={suspenseResource(
          () => orderCreation.takeProfitTriggerPercentageOptions$,
        )}
        takeProfitTrigger={suspenseResource(
          () => orderCreation.takeProfitTriggerDisplay$,
        )}
        onTakeProfitTriggerChange={action(
          patch =>
            (orderCreation.takeProfitTrigger =
              patch ?? orderCreation.defaultTakeProfitTrigger),
        )}
      />
    )
  }
  assertNever(orderCreation.type)
}

const WiredSlippageModal: FC = () => {
  const creation = useTradeStore().orderCreation
  const onClose = action(() => (creation.showSwapSlippageEditingModal = false))
  return (
    <Dialog visible={creation.showSwapSlippageEditingModal} onClose={onClose}>
      <EditSlippageModalContent
        slippage={creation.swapSlippage.read$}
        recommendedSlippage={creation.recommendedSwapSlippage}
        onDismiss={onClose}
        onSubmit={slippage => {
          creation.swapSlippage.set(slippage)
          onClose()
        }}
      />
    </Dialog>
  )
}

const WiredOrderCreationPanel: FCS<{ gap?: number }> = ({ style, gap }) => {
  const auth = useAuthStore()
  const { orderCreation, baseTokenInfo$, anchorTokenInfo$ } = useTradeStore()
  const notifier = useTransactionNotifier()
  const intl = useIntl()
  const { $t } = intl
  return (
    <Spensor>
      {() => (
        <OrderCreationFrame
          style={style}
          quoteToken={orderCreation.quoteTokenInfo$}
          baseToken={baseTokenInfo$}
          anchorToken={anchorTokenInfo$}
          swapRate={suspenseResource(() => orderCreation.exchangeRate$)}
          orderDirection={orderCreation.direction}
          onChangeOrderDirection={action(
            direction => (orderCreation.direction = direction),
          )}
          orderType={orderCreation.type}
          onChangeOrderType={action(type => (orderCreation.type = type))}
          swapSlippage={suspenseResource(() =>
            orderCreation.isAnchorToken$
              ? null
              : orderCreation.swapSlippage.read$,
          )}
          onSwapSlippagePressed={action(
            () => (orderCreation.showSwapSlippageEditingModal = true),
          )}
          takeProfitTrigger={suspenseResource(
            () => orderCreation.takeProfitTriggerDisplay$,
          )}
          stopLoseTrigger={suspenseResource(
            () => orderCreation.stopLossTriggerDisplay$,
          )}
          leverage={suspenseResource(() => orderCreation.leverage.read$)}
          positionSize={suspenseResource(() => orderCreation.positionSize$)}
          liquidationPrice={suspenseResource(
            () => orderCreation.liquidationPrice$,
          )}
          fees={suspenseResource(() => orderCreation.fees$)}
          formError={suspenseResource(() => orderCreation.formError$)}
          onConnectWallet={action(() => {
            auth.showConnectWalletModal = true
          })}
          onSubmit={suspenseResource(() => {
            const data = Result.maybeValue(orderCreation.formData$)
            if (data == null) {
              return noop
            }
            return async () => {
              const needApprove = await waitFor(
                () => orderCreation.needApproveFirst$,
              )
              if (needApprove) {
                const operationType = $t(
                  defineMessage({
                    defaultMessage: "Approve token",
                    description:
                      "TradeScreen/NotificationTitle/Approve operate wallet stable coin",
                  }),
                )
                const tx = await notifier.broadcast(
                  orderCreation.approveToken(),
                  operationType,
                )
                await notifier.showConfirmationProgress(tx, operationType)
              }
              const operationType = $t(
                defineMessage({
                  defaultMessage: "{orderType} {orderDirection}",
                  description: "TradeScreen/NotificationTitle/Create order",
                }),
                {
                  orderType: orderType$t.fromOrderType(intl, data.type),
                  orderDirection: orderDirection$t.fromOrderDirection(
                    intl,
                    data.direction,
                  ),
                },
              )
              const renderExecutedMessageItem: RenderExecutedMessageItemFn =
                props => (
                  <TransactionMessageItem
                    titleText={
                      <MessageItemTitleText
                        style={{
                          color:
                            data.direction === OrderDirection.Short
                              ? Colors.shortColor
                              : Colors.longColor,
                        }}
                      >
                        {operationType}
                      </MessageItemTitleText>
                    }
                    content={
                      <MessageItemBodyText>
                        <TokenNameStack
                          baseToken={baseTokenInfo$}
                          quoteToken={orderCreation.quoteTokenInfo$}
                        />
                        @
                        <TokenCount
                          token={orderCreation.quoteTokenInfo$}
                          count={data.executionPrice}
                        />
                      </MessageItemBodyText>
                    }
                    transactionExplorerUrl={props.explorerUrl}
                  />
                )
              const latestData = Result.maybeValue(orderCreation.formData$)
              if (latestData == null) {
                return
              }
              await notifier.broadcastAndShowConfirmation(
                orderCreation.submit(latestData),
                operationType,
                {
                  renderExecutedMessageItem,
                },
              )
            }
          })}
        >
          <WiredOrderCreationContent gap={gap} />
          <WiredSlippageModal />
        </OrderCreationFrame>
      )}
    </Spensor>
  )
}

export default WiredOrderCreationPanel
