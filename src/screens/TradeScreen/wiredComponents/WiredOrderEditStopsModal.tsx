import { action } from "mobx"
import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { ActivityIndicator } from "react-native"
import { CardBoxModalContent } from "../../../components/CardBoxModalContent/CardBoxModalContent"
import { Dialog } from "../../../components/DialogProvider/Dialog"
import { Spensor } from "../../../components/Spensor"
import { useSpacing } from "../../../components/Themed/spacing"
import { useTransactionNotifier } from "../../../components/TransactionNotifier/WiredTransactionNotifier"
import { useTradeStore } from "../../../stores/TradeStore/useTradeStore"
import { Result } from "../../../stores/utils/Result"
import { safelyGet, waitFor } from "../../../stores/utils/waitFor"
import { suspenseResource } from "../../../utils/SuspenseResource"
import { UpdateSLTPTriggerModalContent } from "../components/UpdateSLTPTriggerModalContent"
import { OrderDirection } from "../types"

export const WiredOrderEditStopsModal: FC = () => {
  const { $t } = useIntl()
  const { orderHistory } = useTradeStore()
  const { editStops } = orderHistory
  const notifier = useTransactionNotifier()
  const spacing = useSpacing()
  if (editStops == null) {
    return null
  }
  const onDismiss = action(() => (orderHistory.editStops = undefined))
  const { trade } = editStops
  return (
    <Dialog visible={true} onClose={onDismiss}>
      <Spensor
        fallback={
          <CardBoxModalContent padding={spacing(4)}>
            <ActivityIndicator className="m-auto" />
          </CardBoxModalContent>
        }
      >
        {() => (
          <UpdateSLTPTriggerModalContent
            onDismiss={onDismiss}
            errors={safelyGet(() => Result.maybeError(editStops.formData$))}
            onSubmit={async () => {
              const data = await waitFor(() =>
                Result.maybeValue(editStops.formData$),
              )
              if (data == null) return
              void notifier.broadcastAndShowConfirmation(
                editStops.updateStops(data),
                $t(
                  defineMessage({
                    defaultMessage: "Update Stop Loss/Take Profit",
                    description: "TradeScreen/NotificationTitle/update stops",
                  }),
                ),
              )
            }}
            baseToken={trade.baseTokenInfo$}
            quoteToken={trade.quoteTokenInfo$}
            orderDirection={
              trade.isLong ? OrderDirection.Long : OrderDirection.Short
            }
            leverage={trade.leverage}
            liquidationPrice={trade.trade.liquidationPrice}
            stopLossTrigger={editStops.stopLossTriggerDisplay$}
            takeProfitTrigger={editStops.takeProfitTriggerDisplay$}
            stopLossTriggerEstimatedPnl={suspenseResource(
              () => editStops.stopLossTriggerEstimatedPnl$,
            )}
            takeProfitTriggerEstimatedPnl={suspenseResource(
              () => editStops.takeProfitTriggerEstimatedPnl$,
            )}
            stopLossPricePatch={editStops.stopLossTrigger ?? null}
            onStopLossPriceChange={action(
              x => (editStops.stopLossTrigger = x ?? undefined),
            )}
            takeProfitPricePatch={editStops.takeProfitTrigger ?? null}
            onTakeProfitPriceChange={action(
              x => (editStops.takeProfitTrigger = x ?? undefined),
            )}
          />
        )}
      </Spensor>
    </Dialog>
  )
}
