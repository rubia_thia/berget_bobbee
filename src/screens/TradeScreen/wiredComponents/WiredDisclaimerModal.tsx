import { action } from "mobx"
import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { Text, useWindowDimensions, View } from "react-native"
import { accept$t } from "../../../commonIntlMessages"
import { Button } from "../../../components/ButtonFramework/Button"
import {
  CardBoxModalContent,
  TitleBar,
} from "../../../components/CardBoxModalContent/CardBoxModalContent"
import { Dialog } from "../../../components/DialogProvider/Dialog"
import { Stack } from "../../../components/Stack"
import { useSpacing } from "../../../components/Themed/spacing"
import { useAppEnvStore } from "../../../stores/AppEnvStore/useAppEnvStore"

const content = `IN ACCESSING AND/OR USING UNIWHALE EXCHANGE, YOU ACKNOWLEDGE AND AGREE THAT :

(a) UNIWHALE EXCHANGE IS/ARE PROVIDED ON AN “AS-IS” AND “AS AVAILABLE” BASIS, AND UNIWHALE EXCHANGE COMMUNITY (“OPERATOR”) AND ITS AFFILIATES (SAVE TO THE EXTENT PROHIBITED BY APPLICABLE LAWS) EXPRESSLY DISCLAIM ANY AND ALL REPRESENTATIONS, WARRANTIES AND/OR CONDITIONS OF ANY KIND IN RESPECT THEREOF, WHETHER EXPRESS, IMPLIED, OR STATUTORY, INCLUDING ALL WARRANTIES OR CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, QUIET ENJOYMENT, ACCURACY, OR NON-INFRINGEMENT.

(b) OPERATOR AND ITS AFFILIATES HAS NOT MADE AND MAKES NO REPRESENTATION, WARRANTY AND/OR CONDITION OF ANY KIND THAT UNIWHALE EXCHANGE WILL MEET YOUR REQUIREMENTS, OR WILL BE AVAILABLE ON AN UNINTERRUPTED, TIMELY, SECURE, OR ERROR-FREE BASIS, OR WILL BE ACCURATE, RELIABLE, FREE OF VIRUSES OR OTHER HARMFUL CODE, COMPLETE, LEGAL, OR SAFE.

(c) YOU SHALL HAVE NO CLAIM AGAINST OPERATOR AND/OR ITS AFFILIATES IN RESPECT OF ANY LOSS SUFFERED BY YOU IN RELATION TO OR ARISING FROM YOUR ACCESS AND/OR USE OF UNIWHALE EXCHANGE.`

export const WiredDisclaimerModal: FC = () => {
  const app = useAppEnvStore()
  const onClose = action(() => (app.acceptedDisclaimers = true))
  const spacing = useSpacing()
  const { $t } = useIntl()
  const { width } = useWindowDimensions()
  return (
    <Dialog
      visible={!app.acceptedDisclaimers}
      onClose={onClose}
      width={Math.min(640, width - 40)}
    >
      <CardBoxModalContent padding={spacing(4)} onClose={onClose}>
        <Stack space={spacing(5)}>
          <TitleBar>
            {$t(
              defineMessage({
                defaultMessage: "Uniwhale Disclaimer",
                description: "Disclaimer Title",
              }),
            )}
          </TitleBar>
          <View className="border p-5 border-gray-300">
            <Text className="text-xs">{content}</Text>
          </View>
          <Button onPress={onClose}>{$t(accept$t)}</Button>
        </Stack>
      </CardBoxModalContent>
    </Dialog>
  )
}
