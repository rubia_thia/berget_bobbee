import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { SuspenseResource } from "../../utils/SuspenseResource"
import { TokenInfo } from "../../utils/TokenInfo"

export interface TokenPair {
  baseToken: TokenInfo
  quoteToken: TokenInfo
}

export enum PriceChangeDirection {
  Up = "up",
  Down = "down",
  Keep = "keep",
}

export enum OrderType {
  Market = "market",
  Limit = "limit",
}

export enum OrderDirection {
  Long = "long",
  Short = "short",
}

export enum MarginManipulation {
  Increase = "increase",
  Decrease = "decrease",
}

export interface PositionRecord {
  openedAt: Date
  id: string
  baseToken: TokenInfo
  quoteToken: TokenInfo
  canceling?: SuspenseResource<boolean>
  onCancel?: () => void
  onEditMargin?: () => void
  onEditActionTriggers?: () => void

  leverage: BigNumber
  positionSize: BigNumber
  margin: BigNumber
  entryPrice: BigNumber
  markPrice: SuspenseResource<BigNumber>
  liquidityPrice: BigNumber

  direction: OrderDirection

  stopLossPrice: null | BigNumber
  takeProfitPrice: null | BigNumber

  pnl: SuspenseResource<{
    delta: BigNumber
    deltaPercentage: BigNumber
  }>

  fees: {
    total: SuspenseResource<BigNumber>
    open: SuspenseResource<BigNumber>
    funding: SuspenseResource<BigNumber>
    rollover: SuspenseResource<BigNumber>
  }
}

export type OpenOrderRecord = OpenOrderRecord.Limit | never

export namespace OpenOrderRecord {
  export interface Common {
    orderHash: string
    createdAt: Date
    baseToken: TokenInfo
    quoteToken: TokenInfo
    leverage: BigNumber
    isCanceling: SuspenseResource<boolean>
    orderDirection: OrderDirection
    margin: BigNumber
    position: BigNumber
  }

  export interface Limit extends Common {
    orderType: OrderType.Limit

    askPrice: BigNumber
  }
}

export type TradeRecord =
  | TradeRecord.CreateOrder
  | TradeRecord.CancelOrder
  | TradeRecord.UpdateActionTrigger
  | TradeRecord.IncreaseMargin
  | TradeRecord.DecreaseMargin
  | TradeRecord.ClosePosition
  | TradeRecord.Liquidation
export namespace TradeRecord {
  export enum Type {
    CreateOrder = "CreateOrder",
    CancelOrder = "CancelOrder",
    UpdateActionTrigger = "UpdateActionTrigger",
    IncreaseMargin = "IncreaseMargin",
    DecreaseMargin = "DecreaseMargin",
    ClosePosition = "ClosePosition",
    TakeProfit = "TakeProfit",
    StopLoss = "StopLoss",
    Liquidation = "Liquidation",
  }

  export interface Common {
    transactionHash: string
    orderType: OrderType
    orderDirection: OrderDirection
    createdAt: Date
    baseToken: TokenInfo
    quoteToken: TokenInfo
    leverage: BigNumber
    margin: BigNumber
    position: BigNumber
  }

  export interface CreateOrder extends Common {
    type: Type.CreateOrder
    price: BigNumber
    fee: BigNumber
  }

  export interface CancelOrder extends Common {
    type: Type.CancelOrder
  }

  export interface UpdateActionTrigger extends Common {
    type: Type.UpdateActionTrigger
    stopLossPrice: null | BigNumber
    takeProfitPrice: null | BigNumber
  }

  export interface IncreaseMargin extends Common {
    type: Type.IncreaseMargin
    marginDelta: BigNumber
  }

  export interface DecreaseMargin extends Common {
    type: Type.DecreaseMargin
    marginDelta: BigNumber
  }

  export interface ClosePosition extends Common {
    type: Type.ClosePosition | Type.TakeProfit | Type.StopLoss
    price: BigNumber
    grossPnl: BigNumber
    fees: {
      total: BigNumber
      close: BigNumber
      funding: BigNumber
      rollover: BigNumber
    }
  }

  export interface Liquidation extends Common {
    type: Type.Liquidation
    price: BigNumber
    amount: BigNumber
    grossPnl: BigNumber
    fees: {
      total: BigNumber
      close: BigNumber
      funding: BigNumber
      rollover: BigNumber
    }
  }
}

export enum OrderStatus {
  Matching = "matching",
  Matched = "matched",
  Settled = "settled",
  Expired = "expired",
  Canceled = "canceled",
}
