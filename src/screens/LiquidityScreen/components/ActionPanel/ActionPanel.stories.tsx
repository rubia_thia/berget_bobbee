import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { LiquidityMutationDirection } from "../../types"
import { ActionPanel } from "./ActionPanel"

export default {
  title: "Page/LiquidityScreen/ActionPanel",
  component: ActionPanel,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof ActionPanel>

const tpl = withTemplate(ActionPanel, {
  style: { margin: 10 },
  anchorToken: TokenInfoPresets.MockUSDT,
  swapRate: BigNumber.from(0.995),
  supportedStableTokens: [TokenInfoPresets.MockUSDC, TokenInfoPresets.MockBTC],
  direction: LiquidityMutationDirection.Deposit,
  stableToken: TokenInfoPresets.MockUSDC,
  stableTokenBalance: BigNumber.from(126.533222222),
  stableTokenCount: BigNumber.from(1134.864321),
  lpToken: TokenInfoPresets.MockBUSD,
  lpTokenBalance: BigNumber.from(126.533222222),
  lpTokenCount: BigNumber.from(22.2222222222),
  feeRate: BigNumber.from(0.0013),
  swapSlippage: BigNumber.from(0.02),
})

export const Normal = tpl()

export const WithoutTokenSelector = tpl(p => {
  p.onSelectStableToken = undefined
})
