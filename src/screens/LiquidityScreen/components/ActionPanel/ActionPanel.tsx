import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, TouchableOpacity, ViewStyle } from "react-native"
import { Path, Svg } from "react-native-svg"
import { connectWallet$t } from "../../../../commonIntlMessages"
import { SmartLoadableButton } from "../../../../components/ButtonFramework/LoadableButton"
import { CardBoxView } from "../../../../components/CardBox/CardBox"
import { InfoList } from "../../../../components/InfoList/InfoList"
import { InfoListItem } from "../../../../components/InfoList/InfoListItem"
import {
  InfoListItemDetail,
  InfoListItemDetailText,
} from "../../../../components/InfoList/InfoListItemDetail"
import { InfoListItemTitle } from "../../../../components/InfoList/InfoListItemTitle"
import { PercentNumber } from "../../../../components/PercentNumber"
import { Segment, SegmentControl } from "../../../../components/SegmentControl"
import { Spensor } from "../../../../components/Spensor"
import { TextTokenCount } from "../../../../components/TextTokenCount"
import { useSpacing } from "../../../../components/Themed/spacing"
import { BalanceTopArea } from "../../../../components/TokenInput/BalanceTopArea"
import {
  BlockGroup,
  BlockGroupDownArrowIcon,
} from "../../../../components/TokenInput/BlockGroup"
import { TokenInput } from "../../../../components/TokenInput/TokenInput"
import { noop } from "../../../../utils/fnHelpers"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import {
  readResource,
  safeReadResource,
  SuspenseResource,
} from "../../../../utils/SuspenseResource"
import { TokenInfo } from "../../../../utils/TokenInfo"
import { LiquidityMutationDirection } from "../../types"
import {
  ActionPanelFormError,
  ActionPanelFormErrorType,
} from "./ActionPanel.types"

export const ActionPanel: FC<{
  style?: StyleProp<ViewStyle>
  error?: SuspenseResource<undefined | ActionPanelFormError>

  supportedStableTokens: TokenInfo[]
  onSelectStableToken?: (token: TokenInfo) => void

  direction: LiquidityMutationDirection
  onDirectionChange: (direction: LiquidityMutationDirection) => void

  anchorToken: TokenInfo
  stableToken: TokenInfo
  stableTokenBalance: SuspenseResource<BigNumber>
  stableTokenCount: SuspenseResource<null | BigNumber>
  onStableTokenCountChange: (stableTokenCount: null | BigNumber) => void
  onStableTokenCountPressMax?: SuspenseResource<(() => void) | undefined>

  lpToken: TokenInfo
  lpTokenBalance: SuspenseResource<BigNumber>
  lpTokenCount: SuspenseResource<null | BigNumber>
  onLpTokenCountChange: (lpTokenCount: null | BigNumber) => void
  onLpTokenCountPressMax?: SuspenseResource<(() => void) | undefined>

  feeRate: SuspenseResource<BigNumber>
  swapSlippage: SuspenseResource<BigNumber | undefined>
  swapRate: SuspenseResource<BigNumber | undefined>
  onSwapSlippagePressed: () => void

  onConnectWallet: () => void
  onSubmit: () => void | Promise<void>
}> = props => {
  const { $t } = useIntl()
  const spacing = useSpacing()

  const error = safeReadResource(props.error)

  const gap = spacing(5)

  return (
    <CardBoxView style={[props.style, { padding: spacing(4) }]}>
      <SegmentControl style={{ marginBottom: gap }}>
        <Segment
          active={props.direction === LiquidityMutationDirection.Deposit}
          onPress={() => {
            props.onDirectionChange(LiquidityMutationDirection.Deposit)
          }}
        >
          {$t(
            defineMessage({
              defaultMessage: "Provide Liquidity",
              description:
                "LiquidtyScreen/ActionPanel/Provide Liquidity segment",
            }),
          )}
        </Segment>
        <Segment
          active={props.direction === LiquidityMutationDirection.Withdraw}
          onPress={() => {
            props.onDirectionChange(LiquidityMutationDirection.Withdraw)
          }}
        >
          {$t(
            defineMessage({
              defaultMessage: "Redeem Liquidity",
              description:
                "LiquidtyScreen/ActionPanel/Provide Liquidity segment",
            }),
          )}
        </Segment>
      </SegmentControl>

      {props.direction === LiquidityMutationDirection.Deposit ? (
        <DepositInputFields
          style={{ marginBottom: gap }}
          insufficientBalance={
            error?.type === ActionPanelFormErrorType.InsufficientBalance
          }
          supportedStableTokens={props.supportedStableTokens}
          onSelectStableToken={props.onSelectStableToken}
          stableToken={props.stableToken}
          stableTokenBalance={props.stableTokenBalance}
          stableTokenCount={props.stableTokenCount}
          onStableTokenCountChange={props.onStableTokenCountChange}
          onStableTokenCountPressMax={props.onStableTokenCountPressMax}
          lpToken={props.lpToken}
          lpTokenBalance={props.lpTokenBalance}
          lpTokenCount={props.lpTokenCount}
        />
      ) : (
        <WithdrawInputFields
          style={{ marginBottom: gap }}
          insufficientBalance={
            error?.type === ActionPanelFormErrorType.InsufficientBalance
          }
          supportedStableTokens={props.supportedStableTokens}
          onSelectStableToken={props.onSelectStableToken}
          stableToken={props.stableToken}
          stableTokenBalance={props.stableTokenBalance}
          stableTokenCount={props.stableTokenCount}
          lpToken={props.lpToken}
          lpTokenBalance={props.lpTokenBalance}
          lpTokenCount={props.lpTokenCount}
          onLpTokenCountChange={props.onLpTokenCountChange}
          onLpTokenCountPressMax={props.onLpTokenCountPressMax}
        />
      )}

      <InfoList
        style={{ marginBottom: gap }}
        direction={"column"}
        listItemDirection={"row"}
      >
        <InfoListItem>
          <InfoListItemTitle>
            {$t(
              defineMessage({
                defaultMessage: "Fees",
                description: "LiquidityScreen/ActionPanel/info list label",
              }),
            )}
          </InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              <PercentNumber number={props.feeRate} />
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </InfoListItem>
        <Spensor>
          {() => {
            const swapRate = readResource(props.swapRate)
            if (!swapRate) {
              return null
            }
            return (
              <InfoListItem>
                <InfoListItemTitle>
                  {$t(
                    defineMessage({
                      defaultMessage: "Swap Rate",
                      description:
                        "LiquidityScreen/ActionPanel/info list label",
                    }),
                  )}
                </InfoListItemTitle>
                <InfoListItemDetail>
                  <InfoListItemDetailText>
                    <TextTokenCount
                      count={swapRate}
                      token={{
                        ...props.anchorToken,
                        precision: 4,
                      }}
                    />
                  </InfoListItemDetailText>
                </InfoListItemDetail>
              </InfoListItem>
            )
          }}
        </Spensor>
        <Spensor>
          {() => {
            const slippage = readResource(props.swapSlippage)
            if (!slippage) {
              return null
            }
            return (
              <InfoListItem>
                <InfoListItemTitle>
                  {$t(
                    defineMessage({
                      defaultMessage: "Swap Slippage Tolerance",
                      description:
                        "LiquidityScreen/ActionPanel/info list label",
                    }),
                  )}
                </InfoListItemTitle>
                <TouchableOpacity
                  className="ml-auto flex-row items-center"
                  onPress={props.onSwapSlippagePressed}
                >
                  <Svg
                    width="12"
                    height="12"
                    viewBox="0 0 12 12"
                    style={{ marginRight: spacing(1) }}
                  >
                    <Path
                      d="M9.39894 5.09131L3.86806 10.6209L0.75 11.25L1.37912 8.1315L6.90956 2.6015L9.39894 5.09131V5.09131ZM10.0176 4.47269L11.25 3.23894L8.76019 0.75L7.52819 1.98244L10.0176 4.47269V4.47269Z"
                      fill="#9CA3AF"
                    />
                  </Svg>
                  <InfoListItemDetail>
                    <InfoListItemDetailText>
                      <PercentNumber number={slippage} />
                    </InfoListItemDetailText>
                  </InfoListItemDetail>
                </TouchableOpacity>
              </InfoListItem>
            )
          }}
        </Spensor>
      </InfoList>

      {error?.type === ActionPanelFormErrorType.ConnectWallet ? (
        <SmartLoadableButton onPress={props.onConnectWallet}>
          {$t(connectWallet$t)}
        </SmartLoadableButton>
      ) : (
        <SmartLoadableButton disabled={error != null} onPress={props.onSubmit}>
          {$t(
            defineMessage({
              defaultMessage: "Submit",
              description: "LiquidityScreen/ActionPanel/Submit button text",
            }),
          )}
        </SmartLoadableButton>
      )}
    </CardBoxView>
  )
}

const DepositInputFields: FC<{
  style?: StyleProp<ViewStyle>
  insufficientBalance?: boolean

  supportedStableTokens: TokenInfo[]
  onSelectStableToken?: (token: TokenInfo) => void

  stableToken: TokenInfo
  stableTokenBalance: SuspenseResource<BigNumber>
  stableTokenCount: SuspenseResource<null | BigNumber>
  onStableTokenCountChange: (stableTokenCount: null | BigNumber) => void
  onStableTokenCountPressMax?: SuspenseResource<(() => void) | undefined>

  lpToken: TokenInfo
  lpTokenBalance: SuspenseResource<BigNumber>
  lpTokenCount: SuspenseResource<null | BigNumber>
}> = props => {
  const { $t } = useIntl()

  return (
    <BlockGroup
      style={props.style}
      firstBlock={
        <TokenInput
          topArea={
            <BalanceTopArea
              titleText={$t(
                defineMessage({
                  defaultMessage: "Pay:",
                  description:
                    "LiquidityScreen/ActionPanel/Deposit/input top label",
                }),
              )}
              token={props.stableToken}
              balance={props.stableTokenBalance}
            />
          }
          error={props.insufficientBalance}
          availableTokens={props.supportedStableTokens}
          onTokenChange={props.onSelectStableToken}
          token={props.stableToken}
          value={props.stableTokenCount}
          onValueChange={props.onStableTokenCountChange}
          onPressMax={props.onStableTokenCountPressMax}
        />
      }
      secondBlock={
        <TokenInput
          topArea={
            <BalanceTopArea
              titleText={$t(
                defineMessage({
                  defaultMessage: "Receive:",
                  description:
                    "LiquidityScreen/ActionPanel/Deposit/input top label",
                }),
              )}
              token={props.lpToken}
              balance={props.lpTokenBalance}
            />
          }
          readonly={true}
          availableTokens={[]}
          token={props.lpToken}
          value={props.lpTokenCount}
          onValueChange={noop}
        />
      }
      icon={<BlockGroupDownArrowIcon />}
    />
  )
}

const WithdrawInputFields: FC<{
  style?: StyleProp<ViewStyle>
  insufficientBalance?: boolean

  supportedStableTokens: TokenInfo[]
  onSelectStableToken?: (token: TokenInfo) => void

  stableToken: TokenInfo
  stableTokenBalance: SuspenseResource<BigNumber>
  stableTokenCount: SuspenseResource<null | BigNumber>

  lpToken: TokenInfo
  lpTokenBalance: SuspenseResource<BigNumber>
  lpTokenCount: SuspenseResource<null | BigNumber>
  onLpTokenCountChange: (lpTokenCount: null | BigNumber) => void
  onLpTokenCountPressMax?: SuspenseResource<(() => void) | undefined>
}> = props => {
  const { $t } = useIntl()

  return (
    <BlockGroup
      style={props.style}
      firstBlock={
        <TokenInput
          topArea={
            <BalanceTopArea
              titleText={$t(
                defineMessage({
                  defaultMessage: "Pay:",
                  description:
                    "LiquidityScreen/ActionPanel/Redeem/input top label",
                }),
              )}
              token={props.lpToken}
              balance={props.lpTokenBalance}
            />
          }
          error={props.insufficientBalance}
          availableTokens={[]}
          token={props.lpToken}
          value={props.lpTokenCount}
          onValueChange={props.onLpTokenCountChange}
          onPressMax={props.onLpTokenCountPressMax}
        />
      }
      secondBlock={
        <TokenInput
          topArea={
            <BalanceTopArea
              titleText={$t(
                defineMessage({
                  defaultMessage: "Receive:",
                  description:
                    "LiquidityScreen/ActionPanel/Redeem/input top label",
                }),
              )}
              token={props.stableToken}
              balance={props.stableTokenBalance}
            />
          }
          readonly={true}
          availableTokens={props.supportedStableTokens}
          onTokenChange={props.onSelectStableToken}
          token={props.stableToken}
          value={props.stableTokenCount}
          onValueChange={noop}
        />
      }
      icon={<BlockGroupDownArrowIcon />}
    />
  )
}
