export enum ActionPanelFormErrorType {
  ConnectWallet = "ConnectWallet",
  EnterAmount = "EnterAmount",
  InsufficientBalance = "InsufficientBalance",
}

export type ActionPanelFormError = {
  type: ActionPanelFormErrorType
}

export namespace ActionPanelFormError {}
