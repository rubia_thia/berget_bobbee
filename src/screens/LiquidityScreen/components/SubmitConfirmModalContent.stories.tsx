import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../utils/TokenInfoPresets/TokenInfoPresets"
import { LiquidityMutationDirection } from "../types"
import { SubmitConfirmModalContent } from "./SubmitConfirmModalContent"

export default {
  title: "Page/LiquidityScreen/SubmitConfirmModalContent",
  component: SubmitConfirmModalContent,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof SubmitConfirmModalContent>

const tpl = withTemplate(SubmitConfirmModalContent, {
  style: {
    margin: 10,
  },
  liquidityTransferDirection: LiquidityMutationDirection.Deposit,
  pay: {
    token: TokenInfoPresets.MockUSDC,
    count: BigNumber.from(111.11111111),
  },
  receive: {
    token: TokenInfoPresets.MockUSDC,
    count: BigNumber.from(111.11111111),
  },
  fee: {
    token: TokenInfoPresets.MockUSDC,
    count: BigNumber.from(111.11111111),
  },
  feeRate: BigNumber.from(0.013),
  lpToken: TokenInfoPresets.ULP,
  baseStableToken: TokenInfoPresets.MockBUSD,
})

export const Normal = tpl()
