import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { LiquidityScreenContent } from "./LiquidityScreenContent"

export default {
  title: "Page/LiquidityScreen/LiquidityScreenContent",
  component: LiquidityScreenContent,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof LiquidityScreenContent>

const tpl = withTemplate(LiquidityScreenContent, {})

export const Normal = tpl()
