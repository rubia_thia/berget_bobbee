import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, Text, View, ViewStyle } from "react-native"
import {
  cancel$t,
  confirm$t,
  confirmDialogTitleText$t,
} from "../../../commonIntlMessages"
import { WhiteOutlineButtonVariant } from "../../../components/Button/WhiteOutlineButtonVariant"
import { Button } from "../../../components/ButtonFramework/Button"
import { SmartLoadableButton } from "../../../components/ButtonFramework/LoadableButton"
import { CardInset } from "../../../components/CardBox/CardInset"
import {
  ActionRowColumns,
  CardBoxModalContent,
  TitleBar,
} from "../../../components/CardBoxModalContent/CardBoxModalContent"
import { InfoList } from "../../../components/InfoList/InfoList"
import { InfoListItem } from "../../../components/InfoList/InfoListItem"
import { InfoListItemDetail } from "../../../components/InfoList/InfoListItemDetail"
import { InfoListItemTitle } from "../../../components/InfoList/InfoListItemTitle"
import {
  NoteParagraph,
  NoteParagraphText,
} from "../../../components/NoteParagraph/NoteParagraph"
import { PercentNumber } from "../../../components/PercentNumber"
import { TextTokenCount } from "../../../components/TextTokenCount"
import { useColors } from "../../../components/Themed/color"
import { useSpacing } from "../../../components/Themed/spacing"
import { TokenName } from "../../../components/TokenName"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { styleGetters } from "../../../utils/reactHelpers/withProps/styleGetters"
import { withProps } from "../../../utils/reactHelpers/withProps/withProps"
import { TokenInfo } from "../../../utils/TokenInfo"
import { LiquidityMutationDirection } from "../types"

export interface PriceInfo {
  token: TokenInfo
  count: BigNumber
}

export interface SubmitConfirmModalContentProps {
  style?: StyleProp<ViewStyle>
  onDismiss: () => void
  onConfirm: () => void | Promise<void>

  liquidityTransferDirection: LiquidityMutationDirection
  lpToken: TokenInfo
  baseStableToken: TokenInfo

  pay: PriceInfo
  receive: PriceInfo
  fee: PriceInfo
  feeRate: BigNumber
}

export const SubmitConfirmModalContent: FC<
  SubmitConfirmModalContentProps
> = props => {
  const { $t } = useIntl()

  const spacing = useSpacing()
  const colors = useColors()

  return (
    <CardBoxModalContent
      style={props.style}
      padding={spacing(5)}
      onClose={props.onDismiss}
    >
      <TitleBar>{$t(confirmDialogTitleText$t)}</TitleBar>

      <View className={"space-y-5"} style={{ marginTop: spacing(6) }}>
        <Text style={{ fontSize: 14, color: colors("gray-900") }}>
          {$t(
            props.liquidityTransferDirection ===
              LiquidityMutationDirection.Deposit
              ? defineMessage({
                  defaultMessage: "Provide Liquidity",
                  description: "LiquidityScreen/Confirm Modal/text",
                })
              : defineMessage({
                  defaultMessage: "Redeem Liquidity",
                  description: "LiquidityScreen/Confirm Modal/text",
                }),
          )}
        </Text>

        <CardInset>
          <InfoList
            className={"space-y-1"}
            direction={"column"}
            listItemDirection={"row"}
          >
            <InfoListItem>
              <InfoListItemTitle>
                {$t(
                  defineMessage({
                    defaultMessage: "Pay",
                    description: "LiquidityScreen/Confirm Modal/label text",
                  }),
                )}
              </InfoListItemTitle>
              <InfoListItemDetail>
                <InfoListDetailText>
                  <TextTokenCount
                    token={props.pay.token}
                    count={props.pay.count}
                  />
                </InfoListDetailText>
              </InfoListItemDetail>
            </InfoListItem>

            <InfoListItem>
              <InfoListItemTitle>
                {$t(
                  defineMessage({
                    defaultMessage: "Receive",
                    description: "LiquidityScreen/Confirm Modal/label text",
                  }),
                )}
              </InfoListItemTitle>
              <InfoListItemDetail>
                <InfoListDetailText>
                  <TextTokenCount
                    token={props.receive.token}
                    count={props.receive.count}
                  />
                </InfoListDetailText>
              </InfoListItemDetail>
            </InfoListItem>

            <InfoListItem>
              <InfoListItemTitle>
                {$t(
                  defineMessage({
                    defaultMessage: "Fees",
                    description: "LiquidityScreen/Confirm Modal/label text",
                  }),
                )}
              </InfoListItemTitle>
              <InfoListItemDetail>
                <InfoListDetailText>
                  <PercentNumber number={props.feeRate} />
                  &nbsp; (
                  <TextTokenCount
                    token={props.fee.token}
                    count={props.fee.count}
                  />
                  )
                </InfoListDetailText>
              </InfoListItemDetail>
            </InfoListItem>
          </InfoList>
        </CardInset>

        <ActionRowColumns>
          <Button Variant={WhiteOutlineButtonVariant} onPress={props.onDismiss}>
            {$t(cancel$t)}
          </Button>
          <SmartLoadableButton onPress={props.onConfirm}>
            {$t(confirm$t)}
          </SmartLoadableButton>
        </ActionRowColumns>

        {props.liquidityTransferDirection ===
          LiquidityMutationDirection.Deposit && (
          <NoteParagraph>
            <NoteParagraphText>
              {$t(
                defineMessage({
                  defaultMessage:
                    "{lpToken} will be automatically swapped into {stableToken} and staked once you provide liquidity. Staked {lpToken} can be withdrawn and redeemed at any time.",
                  description: "LiquidityScreen/Confirm Modal/note text",
                }),
                {
                  lpToken: <TokenName token={props.lpToken} />,
                  stableToken: <TokenName token={props.baseStableToken} />,
                },
              )}
            </NoteParagraphText>
          </NoteParagraph>
        )}

        {props.liquidityTransferDirection ===
          LiquidityMutationDirection.Withdraw && (
          <NoteParagraph>
            <NoteParagraphText>
              {$t(
                defineMessage({
                  defaultMessage:
                    "Once redeemed, {lpToken} token will be burned and no longer receive interests.",
                  description: "LiquidityScreen/Confirm Modal/note text",
                }),
                {
                  lpToken: <TokenName token={props.lpToken} />,
                },
              )}
            </NoteParagraphText>
          </NoteParagraph>
        )}
      </View>
    </CardBoxModalContent>
  )
}

const InfoListDetailText = withProps(
  styleGetters(({ colors }) => ({
    fontSize: 16,
    color: colors("gray-900"),
  })),
  Text,
)
