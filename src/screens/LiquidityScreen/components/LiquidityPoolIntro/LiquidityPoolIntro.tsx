import { FC, ReactNode } from "react"
import { defineMessage, useIntl } from "react-intl"
import { Image, StyleProp, Text, View, ViewStyle } from "react-native"
import { CardBoxView } from "../../../../components/CardBox/CardBox"
import { InfoList } from "../../../../components/InfoList/InfoList"
import { useResponsiveValue } from "../../../../components/Themed/breakpoints"
import { useColors } from "../../../../components/Themed/color"
import { useSpacing } from "../../../../components/Themed/spacing"
import { TokenName } from "../../../../components/TokenName"
import { arrayJoin } from "../../../../utils/arrayHelpers"
import { TokenInfo } from "../../../../utils/TokenInfo"
import { OneOrMore } from "../../../../utils/types"
import LPTokenIcon from "./_/lpTokenLargeIcon.svg"

import { InfoListItem } from "../../../../components/InfoList/InfoListItem"
import { InfoListItemDetail } from "../../../../components/InfoList/InfoListItemDetail"
import { InfoListItemTitle } from "../../../../components/InfoList/InfoListItemTitle"
import { PercentNumber } from "../../../../components/PercentNumber"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { styleGetters } from "../../../../utils/reactHelpers/withProps/styleGetters"
import { withProps } from "../../../../utils/reactHelpers/withProps/withProps"
import { SuspenseResource } from "../../../../utils/SuspenseResource"
import sideImageSrc from "./_/sideImage.png"

export interface LiquidityPoolIntroProps {
  style?: StyleProp<ViewStyle>

  lpToken: TokenInfo
  stableToken: TokenInfo
  yieldTokens: OneOrMore<TokenInfo>

  supportedStableTokens: OneOrMore<TokenInfo>

  accruesTradingFeePercentage: SuspenseResource<BigNumber>
}

export const LiquidityPoolIntro: FC<LiquidityPoolIntroProps> = props => {
  const { $t } = useIntl()
  const colors = useColors()
  const spacing = useSpacing()

  const summaryCardLayout =
    useResponsiveValue({
      _: "col",
      md: "row",
    } as const) ?? "col"

  return (
    <CardBoxView
      style={[
        props.style,
        { flexDirection: summaryCardLayout === "row" ? "row" : "column" },
      ]}
      padding={spacing(6)}
    >
      <View
        className={"gap-y-[50px]"}
        style={[
          { justifyContent: "space-between" },
          summaryCardLayout === "row" && { flex: 1 },
          summaryCardLayout === "row"
            ? { marginRight: 50 }
            : { marginBottom: 20 },
        ]}
      >
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <LPTokenIcon
            style={{ marginRight: 20, flexShrink: 0 }}
            width={64}
            height={64}
          />

          <View style={{ flex: 1 }}>
            <Text
              style={{
                marginBottom: spacing(1),
                fontSize: 20,
                fontWeight: "500",
                color: "#019A63",
              }}
            >
              {$t<ReactNode>(
                defineMessage({
                  defaultMessage: "Introducing {lpToken}",
                  description:
                    "LiquidityScreen/Liquidity Pool Intro Panel/text",
                }),
                {
                  lpToken: <TokenName token={props.lpToken} />,
                },
              )}
            </Text>
            <NormalText>
              {$t<ReactNode>(
                defineMessage({
                  defaultMessage: "The platform's liquidity pool token",
                  description:
                    "LiquidityScreen/Liquidity Pool Intro Panel/text",
                }),
              )}
            </NormalText>
            <NormalText>
              {$t<ReactNode>(
                defineMessage({
                  defaultMessage:
                    "Earn upto {percentage} of platform fees distributed in stablecoin",
                  description:
                    "LiquidityScreen/Liquidity Pool Intro Panel/text",
                }),
                {
                  percentage: (
                    <PercentNumber number={props.accruesTradingFeePercentage} />
                  ),
                },
              )}
            </NormalText>
            <NormalText numberOfLines={3}>
              {$t<ReactNode>(
                defineMessage({
                  defaultMessage:
                    "USD real passive yield without market volatility",
                  description:
                    "LiquidityScreen/Liquidity Pool Intro Panel/text",
                }),
              )}
            </NormalText>
          </View>
        </View>

        <InfoList direction={"row"} listItemDirection={"column"}>
          <InfoListItem style={{ flex: 1 }}>
            <InfoListItemTitle>
              <InfoListItemTitleText>
                {$t<ReactNode>(
                  defineMessage({
                    defaultMessage: "Real Yield",
                    description:
                      "LiquidityScreen/Liquidity Pool Intro Panel/label",
                  }),
                )}
              </InfoListItemTitleText>
            </InfoListItemTitle>
            <InfoListItemDetail>
              <InfoListItemDetailText>
                {arrayJoin<ReactNode>(
                  () => "+",
                  props.yieldTokens.map((token, idx) => (
                    <TokenName key={idx} token={token} />
                  )),
                )}
              </InfoListItemDetailText>
            </InfoListItemDetail>
          </InfoListItem>

          <InfoListItem style={{ flex: 1 }}>
            <InfoListItemTitle>
              <InfoListItemTitleText>
                {$t<ReactNode>(
                  defineMessage({
                    defaultMessage: "Impermanent Loss",
                    description:
                      "LiquidityScreen/Liquidity Pool Intro Panel/label",
                  }),
                )}
              </InfoListItemTitleText>
            </InfoListItemTitle>
            <InfoListItemDetail>
              <InfoListItemDetailText>
                {$t<ReactNode>(
                  defineMessage({
                    defaultMessage: "Free",
                    description:
                      "LiquidityScreen/Liquidity Pool Intro Panel/Impermanent Loss field value",
                  }),
                )}
              </InfoListItemDetailText>
            </InfoListItemDetail>
          </InfoListItem>

          <InfoListItem style={{ flex: 1 }}>
            <InfoListItemTitle>
              <InfoListItemTitleText>
                {$t<ReactNode>(
                  defineMessage({
                    defaultMessage: "Redeem to {withdrawToken}",
                    description:
                      "LiquidityScreen/Liquidity Pool Intro Panel/label",
                  }),
                  {
                    withdrawToken: <TokenName token={props.stableToken} />,
                  },
                )}
              </InfoListItemTitleText>
            </InfoListItemTitle>
            <InfoListItemDetail>
              <InfoListItemDetailText>
                {$t<ReactNode>(
                  defineMessage({
                    defaultMessage: "Anytime",
                    description:
                      "LiquidityScreen/Liquidity Pool Intro Panel/Redeem to token field value",
                  }),
                )}
              </InfoListItemDetailText>
            </InfoListItemDetail>
          </InfoListItem>
        </InfoList>

        <Text style={{ fontSize: 12, color: colors("black"), opacity: 0.4 }}>
          {$t<ReactNode>(
            defineMessage({
              defaultMessage: "Support {acceptedTokenList} to join",
              description: "LiquidityScreen/Liquidity Pool Intro Panel/text",
            }),
            {
              acceptedTokenList: arrayJoin<ReactNode>(
                () => "/",
                props.supportedStableTokens.map((t, idx) => (
                  <TokenName key={idx} token={t} />
                )),
              ),
            },
          )}
        </Text>
      </View>

      <View style={{ minWidth: 360 }}>
        <Image
          style={{
            width: "100%",
            height: "100%",
            minHeight: 278,
            borderRadius: 8,
            overflow: "hidden",
          }}
          resizeMode={"cover"}
          source={sideImageSrc}
        />
      </View>
    </CardBoxView>
  )
}

const InfoListItemTitleText = withProps(
  styleGetters(({ colors }) => ({
    fontSize: 14,
    color: colors("black"),
  })),
  Text,
)
const InfoListItemDetailText = withProps(
  styleGetters(({ colors }) => ({
    fontSize: 18,
    fontWeight: "500",
    color: colors("gray-900"),
  })),
  Text,
)

const NormalText = withProps(
  styleGetters(({ colors }) => ({
    fontSize: 14,
    color: colors("gray-500"),
  })),
  Text,
)
