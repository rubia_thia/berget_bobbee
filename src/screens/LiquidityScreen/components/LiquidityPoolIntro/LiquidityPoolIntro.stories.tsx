import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { LiquidityPoolIntro } from "./LiquidityPoolIntro"

export default {
  title: "Page/LiquidityScreen/LiquidityPoolIntro",
  component: LiquidityPoolIntro,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof LiquidityPoolIntro>

const tpl = withTemplate(LiquidityPoolIntro, {
  lpToken: TokenInfoPresets.MockBUSD,
  stableToken: TokenInfoPresets.MockUSDC,
  supportedStableTokens: [TokenInfoPresets.MockUSDC, TokenInfoPresets.MockBTC],
  yieldTokens: [TokenInfoPresets.MockUSDC, TokenInfoPresets.MockBTC],
  accruesTradingFeePercentage: BigNumber.from(0.6),
})

export const Normal = tpl()
