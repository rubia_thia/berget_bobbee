import { defineMessage, IntlShape } from "react-intl"
import { checkNever } from "../../../../utils/typeHelpers"
import { LiquidityMutationDirection } from "../../types"

export const approveLP$t = defineMessage({
  defaultMessage: "Approve LP",
  description:
    "LiquidityScreen/NotificationTitle/Approve operate wallet LP token",
})

export const mintLP$t = defineMessage({
  defaultMessage: "Mint LP",
  description: "LiquidityScreen/NotificationTitle/Mint LP token",
})

export const redeemLP$t = defineMessage({
  defaultMessage: "Redeem LP",
  description: "LiquidityScreen/NotificationTitle/Redeem LP token",
})

export const fromLiquidityMutationDirection$t = (
  intl: IntlShape,
  direction: LiquidityMutationDirection,
): string => {
  switch (direction) {
    case LiquidityMutationDirection.Deposit:
      return intl.formatMessage(mintLP$t)
    case LiquidityMutationDirection.Withdraw:
      return intl.formatMessage(redeemLP$t)
    default:
      checkNever(direction)
      return intl.formatMessage(mintLP$t)
  }
}
