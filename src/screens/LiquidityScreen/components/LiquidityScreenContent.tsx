import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, View, ViewStyle } from "react-native"
import { useResponsiveValue } from "../../../components/Themed/breakpoints"
import { useSpacing } from "../../../components/Themed/spacing"
import { noop } from "../../../utils/fnHelpers"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../utils/TokenInfoPresets/TokenInfoPresets"
import {
  PageContainer,
  PageTitleMainText,
} from "../../components/pageLayoutPresets"
import { LiquidityMutationDirection } from "../types"
import { ActionPanel } from "./ActionPanel/ActionPanel"
import { ChartPanel } from "./ChartPanel/ChartPanel"
import { LiquidityPoolIntro } from "./LiquidityPoolIntro/LiquidityPoolIntro"

export interface LiquidityScreenContentProps {
  style?: StyleProp<ViewStyle>
}

export const LiquidityScreenContent: FC<
  LiquidityScreenContentProps
> = props => {
  const spacing = useSpacing()

  const { $t } = useIntl()

  const gap = spacing(5)

  const bottomAreaLayout =
    useResponsiveValue({
      _: "col",
      md: "row",
    } as const) ?? "row"

  return (
    <PageContainer style={props.style}>
      <PageTitleMainText>
        {$t(
          defineMessage({
            defaultMessage: "Liquidity",
            description: "LiquidityScreen/title",
          }),
        )}
      </PageTitleMainText>

      <LiquidityPoolIntro
        style={{ marginTop: gap }}
        supportedStableTokens={[
          TokenInfoPresets.MockUSDC,
          TokenInfoPresets.MockBTC,
        ]}
        lpToken={TokenInfoPresets.ULP}
        stableToken={TokenInfoPresets.MockUSDC}
        yieldTokens={[TokenInfoPresets.MockUSDC, TokenInfoPresets.MockBTC]}
        accruesTradingFeePercentage={BigNumber.from(0.6)}
      />

      <View
        style={{
          marginTop: gap,
          flexDirection: bottomAreaLayout === "row" ? "row" : "column",
        }}
      >
        <ActionPanel
          anchorToken={TokenInfoPresets.MockUSDT}
          onSwapSlippagePressed={noop}
          swapSlippage={undefined}
          swapRate={undefined}
          style={
            bottomAreaLayout === "row"
              ? { marginRight: gap, width: 400 }
              : { marginBottom: gap }
          }
          supportedStableTokens={[]}
          direction={LiquidityMutationDirection.Deposit}
          onDirectionChange={noop}
          stableToken={TokenInfoPresets.MockUSDC}
          stableTokenBalance={BigNumber.from(126.533222222)}
          stableTokenCount={BigNumber.from(1134.864321)}
          onStableTokenCountChange={noop}
          onStableTokenCountPressMax={noop}
          lpToken={TokenInfoPresets.MockBUSD}
          lpTokenBalance={BigNumber.from(126.533222222)}
          lpTokenCount={BigNumber.from(22.2222222222)}
          onLpTokenCountChange={noop}
          feeRate={BigNumber.from(0.0013)}
          onSubmit={noop}
          onConnectWallet={noop}
        />

        <ChartPanel
          style={{ flex: 1 }}
          lpToken={TokenInfoPresets.MockBUSD}
          esToken={TokenInfoPresets.esUNW}
          stableToken={TokenInfoPresets.MockUSDC}
          lpTokenPriceInStableToken={BigNumber.from(1.01)}
          apr={BigNumber.from(0.44)}
          revenueAPR={BigNumber.from(0.123)}
          emissionAPR={BigNumber.from(123)}
          direction={LiquidityMutationDirection.Deposit}
          currentWithdrawLimit={BigNumber.from(276822222.21387)}
          accumulatedFeeIn7Days={BigNumber.from(276822222.21387)}
          distributedFeeInTotal={BigNumber.from(276822222.21387)}
          feeDistributedRates={{
            ulpHolder: BigNumber.from(0.5),
            esUNWHolder: BigNumber.from(0.4),
            reserved: BigNumber.from(0.1),
          }}
          poolAnalyticsData={{
            stableTokenCounts: [
              {
                token: TokenInfoPresets.MockBUSD,
                amountInUSD: BigNumber.from(276822222.21387),
              },
              {
                token: TokenInfoPresets.MockBTC,
                amountInUSD: BigNumber.from(276822222.21387),
              },
              {
                token: TokenInfoPresets.MockUSDC,
                amountInUSD: BigNumber.from(276822222.21387),
              },
              {
                token: TokenInfoPresets.MockUSDC,
                amountInUSD: BigNumber.from(276822222.21387),
              },
            ],
            marketCapacityInUSD: BigNumber.from(276822222.21387),
            totalSuppliedLpTokenAmount: BigNumber.from(276822222.21387),
          }}
        />
      </View>
    </PageContainer>
  )
}
