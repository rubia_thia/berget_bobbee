import { FC, ReactNode } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, Text, View, ViewStyle } from "react-native"
import * as S from "react-native-svg"
import { LinearGradient, Stop } from "react-native-svg"
import { VictoryThemeDefinition } from "victory-core/src/victory-theme/types"
import { VictoryPie, VictoryTheme } from "victory-native"
import { CardBoxView } from "../../../../components/CardBox/CardBox"
import { CardInset } from "../../../../components/CardBox/CardInset"
import { Divider } from "../../../../components/Divider"
import { InfoList } from "../../../../components/InfoList/InfoList"
import { InfoListItem } from "../../../../components/InfoList/InfoListItem"
import {
  InfoListItemDetail,
  InfoListItemDetail as _InfoListItemDetail,
  InfoListItemDetailText,
} from "../../../../components/InfoList/InfoListItemDetail"
import { InfoListItemTitle } from "../../../../components/InfoList/InfoListItemTitle"
import { PercentNumber } from "../../../../components/PercentNumber"
import { Spensor } from "../../../../components/Spensor"
import { Stack } from "../../../../components/Stack"
import {
  TextTokenCount,
  TokenCountAsCurrency,
} from "../../../../components/TextTokenCount"
import { useColors } from "../../../../components/Themed/color"
import { useSpacing } from "../../../../components/Themed/spacing"
import { formatTokenName, TokenName } from "../../../../components/TokenName"
import {
  TextInsideTooltip,
  TooltippifiedText,
} from "../../../../components/Tooltip"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { math } from "../../../../utils/numberHelpers/bigNumberExpressionParser"
import { useSizeQuery } from "../../../../utils/reactHelpers/useSizeQuery"
import { styleGetters } from "../../../../utils/reactHelpers/withProps/styleGetters"
import { withProps } from "../../../../utils/reactHelpers/withProps/withProps"
import { idFactory } from "../../../../utils/stringHelpers"
import {
  readResource,
  suspenseResource,
  SuspenseResource,
} from "../../../../utils/SuspenseResource"
import { TokenInfo } from "../../../../utils/TokenInfo"
import { APRTooltipContent } from "../../../EarnScreen/components/APRTooltipContent"
import { LiquidityMutationDirection } from "../../types"

export const ChartPanel: FC<{
  style?: StyleProp<ViewStyle>
  esToken: TokenInfo
  lpToken: TokenInfo
  stableToken: TokenInfo
  lpTokenPriceInStableToken: SuspenseResource<BigNumber>
  direction: LiquidityMutationDirection
  apr: SuspenseResource<BigNumber>
  revenueAPR: SuspenseResource<BigNumber>
  emissionAPR: SuspenseResource<BigNumber>

  accumulatedFeeIn7Days: SuspenseResource<BigNumber>
  distributedFeeInTotal: SuspenseResource<BigNumber>
  feeDistributedRates: SuspenseResource<{
    ulpHolder: BigNumber
    esUNWHolder: BigNumber
    reserved: BigNumber
  }>
  currentWithdrawLimit: SuspenseResource<BigNumber>
  poolAnalyticsData: {
    stableTokenCounts: {
      token: TokenInfo
      amountInUSD: SuspenseResource<BigNumber>
    }[]
    marketCapacityInUSD: SuspenseResource<BigNumber>
    totalSuppliedLpTokenAmount: SuspenseResource<BigNumber>
  }
}> = props => {
  const { $t } = useIntl()

  const spacing = useSpacing()
  const colors = useColors()

  const [chartLayout = "row", onContainerLayout] = useSizeQuery({
    0: "col",
    500: "row",
  } as const)

  const gap = spacing(4)

  const totalStableTokenPriceInUSD = suspenseResource(() =>
    BigNumber.sum(
      props.poolAnalyticsData.stableTokenCounts.map(info =>
        readResource(info.amountInUSD),
      ),
    ),
  )

  return (
    <CardBoxView
      style={props.style}
      padding={spacing(4)}
      onLayout={onContainerLayout}
    >
      <CardInset padding={spacing(5)} backgroundColor={"#F9FAFB"}>
        <View style={{ minHeight: 50, justifyContent: "center" }}>
          {props.direction === LiquidityMutationDirection.Deposit ? (
            <InfoList direction={"row"} listItemDirection={"column"}>
              <InfoListItem style={{ flex: 1 }}>
                <InfoListItemTitle>
                  <TopInfoListTitleText>
                    {$t(
                      defineMessage({
                        defaultMessage: "{lpToken} Price",
                        description:
                          "LiquidityScreen/ChartPanel/top info list item title",
                      }),
                      {
                        lpToken: <TokenName token={props.lpToken} />,
                      },
                    )}
                  </TopInfoListTitleText>
                </InfoListItemTitle>
                <TopInfoListItemDetail>
                  <TopInfoListDetailText>
                    <TextTokenCount
                      token={{ ...props.stableToken, precision: 4 }}
                      count={props.lpTokenPriceInStableToken}
                    />
                  </TopInfoListDetailText>
                </TopInfoListItemDetail>
              </InfoListItem>

              <InfoListItem style={{ flex: 1 }}>
                <InfoListItemTitle>
                  <TopInfoListTitleText>
                    {$t(
                      defineMessage({
                        defaultMessage: "APR",
                        description:
                          "LiquidityScreen/ChartPanel/top info list item title",
                      }),
                    )}
                  </TopInfoListTitleText>
                </InfoListItemTitle>
                <TopInfoListItemDetail>
                  <TooltippifiedText
                    style={{ alignItems: "flex-start" }}
                    content={
                      <APRTooltipContent
                        revenueAPR={props.revenueAPR}
                        emissionAPR={props.emissionAPR}
                      />
                    }
                  >
                    <TopInfoListDetailText>
                      <PercentNumber number={props.apr} />
                    </TopInfoListDetailText>
                  </TooltippifiedText>
                </TopInfoListItemDetail>
              </InfoListItem>

              <InfoListItem style={{ flex: 1 }}>
                <InfoListItemTitle>
                  <TopInfoListTitleText>
                    {$t(
                      defineMessage({
                        defaultMessage: "7Days {stableToken} fee",
                        description:
                          "LiquidityScreen/ChartPanel/top info list item title",
                      }),
                      {
                        stableToken: <TokenName token={props.stableToken} />,
                      },
                    )}
                  </TopInfoListTitleText>
                </InfoListItemTitle>
                <TopInfoListItemDetail>
                  <TooltippifiedText
                    style={{ alignItems: "flex-start" }}
                    content={$t<ReactNode>(
                      defineMessage({
                        defaultMessage:
                          "<listItem>- {ulpHolderDistributeRate} will be distributed to the {ulpToken} holders</listItem>" +
                          "<listItem>- {esUNWHolderDistributeRate} will be distributed to the {esUNWToken} holders</listItem>" +
                          "<listItem>- {reservedRate} distributed to the Reserve Fund</listItem>",
                        description: "LiquidityScreen/ChartPanel/Fee tooltip",
                      }),
                      {
                        listItem: contents => (
                          <TextInsideTooltip>{contents}</TextInsideTooltip>
                        ),
                        esUNWToken: <TokenName token={props.esToken} />,
                        ulpToken: <TokenName token={props.lpToken} />,
                        esUNWHolderDistributeRate: (
                          <PercentNumber
                            number={suspenseResource(
                              () =>
                                readResource(props.feeDistributedRates)
                                  .esUNWHolder,
                            )}
                            precision={4}
                          />
                        ),
                        ulpHolderDistributeRate: (
                          <PercentNumber
                            number={suspenseResource(
                              () =>
                                readResource(props.feeDistributedRates)
                                  .ulpHolder,
                            )}
                            precision={4}
                          />
                        ),
                        reservedRate: (
                          <PercentNumber
                            number={suspenseResource(
                              () =>
                                readResource(props.feeDistributedRates)
                                  .reserved,
                            )}
                            precision={4}
                          />
                        ),
                      },
                    )}
                  >
                    <TopInfoListDetailText>
                      <TokenCountAsCurrency
                        token={props.stableToken}
                        count={props.accumulatedFeeIn7Days}
                      />
                    </TopInfoListDetailText>
                  </TooltippifiedText>
                </TopInfoListItemDetail>
              </InfoListItem>
              <InfoListItem style={{ flex: 1 }}>
                <InfoListItemTitle>
                  <TopInfoListTitleText>
                    {$t(
                      defineMessage({
                        defaultMessage: "Distributed fee",
                        description:
                          "LiquidityScreen/ChartPanel/top info list item title",
                      }),
                      {
                        stableToken: <TokenName token={props.stableToken} />,
                      },
                    )}
                  </TopInfoListTitleText>
                </InfoListItemTitle>
                <TopInfoListItemDetail>
                  <TopInfoListDetailText>
                    <TokenCountAsCurrency
                      token={props.stableToken}
                      count={props.distributedFeeInTotal}
                    />
                  </TopInfoListDetailText>
                </TopInfoListItemDetail>
              </InfoListItem>
            </InfoList>
          ) : (
            <Stack
              horizontal
              space={spacing(2.5)}
              align={"center"}
              style={{ flexDirection: "row" }}
            >
              <TopInfoListTitleText>
                {$t(
                  defineMessage({
                    defaultMessage: "Block Avai. {lpToken} Redeem",
                    description:
                      "LiquidityScreen/ChartPanel/top info list item title",
                  }),
                  {
                    lpToken: <TokenName token={props.lpToken} />,
                  },
                )}
              </TopInfoListTitleText>
              <Text className="text-gray-900 text-lg font-semibold">
                <TokenCountAsCurrency
                  token={props.stableToken}
                  count={props.currentWithdrawLimit}
                />
              </Text>
            </Stack>
          )}
        </View>

        <Divider style={{ marginVertical: gap }} />

        <TopInfoListTitleText>
          {$t(
            defineMessage({
              defaultMessage: "Pool Percentage",
              description: "LiquidityScreen/ChartPanel/chart title",
            }),
          )}
        </TopInfoListTitleText>
        <View
          style={
            chartLayout === "row"
              ? {
                  flexDirection: "row",
                  minHeight: 260,
                  alignItems: "center",
                  justifyContent: "center",
                }
              : {
                  flexDirection: "column",
                  alignItems: "center",
                }
          }
        >
          <PieChart
            style={{ marginHorizontal: 22 }}
            size={138}
            pieWidth={20}
            lpToken={props.lpToken}
            totalStableTokenAmountInUSD={totalStableTokenPriceInUSD}
            data={props.poolAnalyticsData.stableTokenCounts}
          />

          <InfoList
            style={{
              flex: 1,
              marginTop: chartLayout !== "col" ? 0 : spacing(5),
              marginLeft: chartLayout !== "row" ? 0 : spacing(5),
              width: chartLayout === "row" ? "auto" : "100%",
            }}
            direction={"column"}
            listItemDirection={"row"}
          >
            <Stack space={spacing(2.5)}>
              {props.poolAnalyticsData.stableTokenCounts.map((info, idx) => (
                <InfoListItem
                  key={idx}
                  style={{ marginTop: idx === 0 ? 0 : spacing(2.5) }}
                >
                  <InfoListItemTitle
                    style={{ flexDirection: "row", alignItems: "center" }}
                  >
                    <View
                      style={{
                        marginRight: spacing(1),
                        width: spacing(4),
                        height: spacing(4),
                        borderRadius: 999,
                        backgroundColor: pieChartTheme.pie!.colorScale![idx],
                      }}
                    />
                    <ChartTokenLegendText>
                      <TokenName token={info.token} />
                    </ChartTokenLegendText>
                  </InfoListItemTitle>
                  <InfoListItemDetail>
                    <ChartTokenLegendText>
                      <TokenCountAsCurrency
                        token={info.token}
                        count={info.amountInUSD}
                      />
                    </ChartTokenLegendText>
                  </InfoListItemDetail>
                </InfoListItem>
              ))}

              <Divider lineStyle={"dotted"} color={colors("gray-400")} />

              <InfoListItem>
                <InfoListItemTitle>
                  <ChartSummaryLegendText>
                    {$t(
                      defineMessage({
                        defaultMessage: "Market Cap",
                        description: "LiquidityScreen/ChartPanel/market cap",
                      }),
                    )}
                  </ChartSummaryLegendText>
                </InfoListItemTitle>
                <InfoListItemDetail>
                  <ChartSummaryLegendText>
                    <TokenCountAsCurrency
                      token={props.stableToken}
                      count={props.poolAnalyticsData.marketCapacityInUSD}
                    />
                  </ChartSummaryLegendText>
                </InfoListItemDetail>
              </InfoListItem>
              <InfoListItem>
                <InfoListItemTitle>
                  <ChartSummaryLegendText>
                    {$t(
                      defineMessage({
                        defaultMessage: "Total Supply",
                        description: "LiquidityScreen/ChartPanel/total supply",
                      }),
                    )}
                  </ChartSummaryLegendText>
                </InfoListItemTitle>
                <InfoListItemDetail>
                  <ChartSummaryLegendText>
                    <TextTokenCount
                      token={props.lpToken}
                      count={props.poolAnalyticsData.totalSuppliedLpTokenAmount}
                    />
                  </ChartSummaryLegendText>
                </InfoListItemDetail>
              </InfoListItem>
            </Stack>
          </InfoList>
        </View>
      </CardInset>
    </CardBoxView>
  )
}

const ChartTokenLegendText = withProps(
  styleGetters(({ colors }) => ({
    fontSize: 18,
    fontWeight: "600",
    color: colors("gray-900"),
  })),
  Text,
)
const ChartSummaryLegendText = withProps(
  styleGetters(({ colors }) => ({
    fontSize: 14,
    fontWeight: "500",
    color: colors("gray-900"),
  })),
  Text,
)

const chatIdFactory = idFactory("PieChart-linear-gradient", "-")

const PieChart: FC<{
  style?: StyleProp<ViewStyle>
  size: number
  pieWidth: number
  lpToken: TokenInfo
  data: { token: TokenInfo; amountInUSD: SuspenseResource<BigNumber> }[]
  totalStableTokenAmountInUSD: SuspenseResource<BigNumber>
}> = props => {
  const spacing = useSpacing()
  const { $t } = useIntl()

  const centreAreaSize = props.size - props.pieWidth * 2

  const chartId = chatIdFactory()

  return (
    <View style={props.style}>
      <S.Svg width={props.size} height={props.size}>
        <LinearGradient
          id={chartId}
          y1="0.578125"
          x1="0.078125"
          y2="0.578125"
          x2="1.078125"
        >
          <Stop stopColor="#019A63" />
          <Stop offset="1" stopColor="#29C28B" />
        </LinearGradient>

        <Spensor>
          {() => (
            <VictoryPie
              style={{
                data: { fill: `url(#${chartId})` },
              }}
              padding={0}
              width={props.size}
              height={props.size}
              standalone={false}
              innerRadius={(props.size - props.pieWidth * 2) / 2}
              labelComponent={<></>}
              theme={pieChartTheme}
              data={props.data.map(info => {
                const total = readResource(props.totalStableTokenAmountInUSD)
                return {
                  x: formatTokenName(info.token),
                  y: BigNumber.isZero(total)
                    ? 0
                    : BigNumber.toNumber(
                        math`${readResource(
                          info.amountInUSD,
                        )} / ${total} * ${100}`,
                      ),
                }
              })}
            />
          )}
        </Spensor>
      </S.Svg>

      <Text
        style={{
          position: "absolute",
          top: props.pieWidth,
          left: props.pieWidth,
          right: props.pieWidth,
          bottom: props.pieWidth,
          display: "flex",
          padding: spacing(1),
          width: centreAreaSize,
          height: centreAreaSize,
          alignItems: "center",
          justifyContent: "center",
          textAlign: "center",
          fontSize: 18,
          fontWeight: "600",
        }}
      >
        {$t(
          defineMessage({
            defaultMessage: "{lpToken} Pool",
            description: "LiquidityScreen/ChartPanel/pie chart title",
          }),
          {
            lpToken: formatTokenName(props.lpToken),
          },
        )}
      </Text>
    </View>
  )
}

const pieChartTheme: VictoryThemeDefinition = {
  ...VictoryTheme.grayscale,
  pie: {
    ...VictoryTheme.grayscale.pie,
    colorScale: ["#019A63", "#0EB675", "#46E7AA", "#60FDC1"],
  },
}

const TopInfoListItemDetail = withProps(
  styleGetters(({ spacing }) => ({
    marginTop: spacing(0.5),
  })),
  _InfoListItemDetail,
)

const TopInfoListTitleText = withProps(
  styleGetters(({ colors }) => ({
    color: "#019A63",
    fontSize: 12,
    fontWeight: "500",
  })),
  InfoListItemDetailText,
)
const TopInfoListDetailText = withProps(
  styleGetters(({ colors }) => ({
    color: colors("gray-900"),
    fontSize: 18,
    fontWeight: "600",
  })),
  InfoListItemDetailText,
)
