import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { LiquidityMutationDirection } from "../../types"
import { ChartPanel } from "./ChartPanel"

export default {
  title: "Page/LiquidityScreen/ChartPanel",
  component: ChartPanel,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof ChartPanel>

const tpl = withTemplate(ChartPanel, {
  style: { margin: 10 },
  direction: LiquidityMutationDirection.Deposit,
  currentWithdrawLimit: BigNumber.from(10),
  lpToken: TokenInfoPresets.MockBUSD,
  esToken: TokenInfoPresets.esUNW,
  stableToken: TokenInfoPresets.MockUSDC,
  lpTokenPriceInStableToken: BigNumber.from(1.01),
  distributedFeeInTotal: BigNumber.from(12312),
  feeDistributedRates: {
    ulpHolder: BigNumber.from(0.5),
    esUNWHolder: BigNumber.from(0.4),
    reserved: BigNumber.from(0.1),
  },
  apr: BigNumber.from(0.44),
  revenueAPR: BigNumber.from(0.44),
  emissionAPR: BigNumber.from(0.44),
  accumulatedFeeIn7Days: BigNumber.from(276822222.21387),
  poolAnalyticsData: {
    stableTokenCounts: [
      {
        token: TokenInfoPresets.MockBUSD,
        amountInUSD: BigNumber.from(276822222.21387),
      },
      {
        token: TokenInfoPresets.MockBTC,
        amountInUSD: BigNumber.from(276822222.21387),
      },
      {
        token: TokenInfoPresets.MockUSDC,
        amountInUSD: BigNumber.from(276822222.21387),
      },
      {
        token: TokenInfoPresets.MockUSDC,
        amountInUSD: BigNumber.from(276822222.21387),
      },
    ],
    marketCapacityInUSD: BigNumber.from(276822222.21387),
    totalSuppliedLpTokenAmount: BigNumber.from(276822222.21387),
  },
})

export const Normal = tpl()
