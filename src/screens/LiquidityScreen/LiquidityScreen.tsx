import { action } from "mobx"
import { FC, Suspense } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, View, ViewStyle } from "react-native"
import { Dialog } from "../../components/DialogProvider/Dialog"
import { Spensor } from "../../components/Spensor"
import { useResponsiveValue } from "../../components/Themed/breakpoints"
import { useSpacing } from "../../components/Themed/spacing"
import { useTransactionNotifier } from "../../components/TransactionNotifier/WiredTransactionNotifier"
import { TopLevelNavigatorScreenProps } from "../../navigation/navigators/TopLevelNavigatorTypes"
import { useAuthStore } from "../../stores/AuthStore/useAuthStore"
import {
  LiquidityStoreProvider,
  useLiquidityStore,
} from "../../stores/LiquidityStore/useLiquidityStore"
import { Result } from "../../stores/utils/Result"
import { waitFor } from "../../stores/utils/waitFor"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { suspenseResource } from "../../utils/SuspenseResource"
import { TokenInfo } from "../../utils/TokenInfo"
import { TokenInfoPresets } from "../../utils/TokenInfoPresets/TokenInfoPresets"
import { OneOrMore } from "../../utils/types"
import { HideScreenOnBlur } from "../components/HideScreenOnBlur"
import {
  PageContainer,
  PageTitleMainText,
} from "../components/pageLayoutPresets"
import { EditSlippageModalContent } from "../TradeScreen/components/EditSlippageModalContent"
import { ActionPanel } from "./components/ActionPanel/ActionPanel"
import { ChartPanel } from "./components/ChartPanel/ChartPanel"
import { LiquidityPoolIntro } from "./components/LiquidityPoolIntro/LiquidityPoolIntro"
import { SubmitConfirmModalContent } from "./components/SubmitConfirmModalContent"
import {
  approveLP$t,
  fromLiquidityMutationDirection$t,
} from "./components/_/commonIntlMessages"
import { LiquidityMutationDirection } from "./types"

const WiredLiquidityPoolIntro: FC<{ style?: StyleProp<ViewStyle> }> = props => {
  const liquidity = useLiquidityStore()
  return (
    <LiquidityPoolIntro
      style={props.style}
      supportedStableTokens={liquidity.allTokenInfo$ as OneOrMore<TokenInfo>}
      lpToken={liquidity.liquidityTokenInfo$}
      stableToken={liquidity.anchorTokenInfo$}
      yieldTokens={[liquidity.anchorTokenInfo$]}
      accruesTradingFeePercentage={suspenseResource(
        () => liquidity.currentInfo$.ulpFeeRebateFactor,
      )}
    />
  )
}

const WiredActionConfirmation: FC = () => {
  const liquidity = useLiquidityStore()
  const onClose = action(() => (liquidity.showConfirmation = false))
  const notifier = useTransactionNotifier()
  const intl = useIntl()
  return (
    <Dialog visible={liquidity.showConfirmation} onClose={onClose}>
      <Spensor>
        {() => (
          <SubmitConfirmModalContent
            lpToken={liquidity.liquidityTokenInfo$}
            baseStableToken={liquidity.anchorTokenInfo$}
            onDismiss={onClose}
            feeRate={liquidity.feeRate$}
            fee={{
              count: liquidity.fee$,
              token: liquidity.selectedTokenInfo$,
            }}
            liquidityTransferDirection={liquidity.direction}
            pay={{
              token:
                liquidity.direction === LiquidityMutationDirection.Deposit
                  ? liquidity.selectedTokenInfo$
                  : liquidity.liquidityTokenInfo$,
              count:
                liquidity.direction === LiquidityMutationDirection.Deposit
                  ? liquidity.amountToDeposit.read$
                  : liquidity.amountToWithdraw.read$,
            }}
            receive={{
              token:
                liquidity.direction === LiquidityMutationDirection.Deposit
                  ? liquidity.liquidityTokenInfo$
                  : liquidity.selectedTokenInfo$,
              count:
                liquidity.direction === LiquidityMutationDirection.Deposit
                  ? liquidity.ulpMintAmount$
                  : liquidity.anchorWithdrawAmount$,
            }}
            onConfirm={async () => {
              const form = Result.maybeValue(
                await waitFor(() => liquidity.formData$),
              )
              if (form == null) return
              const needApprove = await waitFor(() => liquidity.needApprove$)
              if (needApprove) {
                const approveTx = await notifier.broadcast(
                  liquidity.approve(),
                  intl.$t(approveLP$t),
                )
                await notifier.showConfirmationProgress(
                  approveTx,
                  intl.$t(approveLP$t),
                )
              }
              const operationType = fromLiquidityMutationDirection$t(
                intl,
                liquidity.direction,
              )
              const tx = await notifier.broadcast(
                liquidity.submit(form),
                operationType,
              )
              onClose()
              void notifier.showConfirmationProgress(tx, operationType)
            }}
          />
        )}
      </Spensor>
    </Dialog>
  )
}

const WiredSlippageEditor: FC = () => {
  const liquidity = useLiquidityStore()
  const onClose = action(() => (liquidity.showSwapSlippageEditingModal = false))
  return (
    <Dialog visible={liquidity.showSwapSlippageEditingModal} onClose={onClose}>
      <EditSlippageModalContent
        slippage={liquidity.swapSlippage.read$}
        recommendedSlippage={liquidity.recommendedSwapSlippage}
        onDismiss={onClose}
        onSubmit={slippage => {
          liquidity.swapSlippage.set(slippage)
          onClose()
        }}
      />
    </Dialog>
  )
}

const WiredActionPanel: FC<{ style?: StyleProp<ViewStyle> }> = props => {
  const liquidity = useLiquidityStore()
  const auth = useAuthStore()
  return (
    <ActionPanel
      swapRate={suspenseResource(() =>
        liquidity.isUsingAnchorToken$ ? undefined : liquidity.exchangeRate$,
      )}
      swapSlippage={suspenseResource(() =>
        liquidity.isUsingAnchorToken$
          ? undefined
          : liquidity.swapSlippage.read$,
      )}
      onSwapSlippagePressed={action(
        () => (liquidity.showSwapSlippageEditingModal = true),
      )}
      style={props.style}
      supportedStableTokens={liquidity.allTokenInfo$}
      onSelectStableToken={a => liquidity.selected(a)}
      direction={liquidity.direction}
      onDirectionChange={action(a => (liquidity.direction = a))}
      anchorToken={liquidity.anchorTokenInfo$}
      stableToken={liquidity.selectedTokenInfo$}
      stableTokenBalance={suspenseResource(
        () => liquidity.selectedTokenBalance$,
      )}
      stableTokenCount={suspenseResource(() =>
        liquidity.direction === LiquidityMutationDirection.Deposit
          ? liquidity.amountToDeposit.get() ?? null
          : liquidity.anchorWithdrawAmount$,
      )}
      onStableTokenCountChange={a =>
        liquidity.amountToDeposit.set(a ?? undefined)
      }
      onStableTokenCountPressMax={suspenseResource(() => {
        if (
          liquidity.amountToDeposit?.get() != null &&
          BigNumber.isEq(liquidity.amountToDeposit.read$)(
            liquidity.selectedTokenBalance$,
          )
        ) {
          return
        }
        return () =>
          liquidity.amountToDeposit.set(liquidity.selectedTokenBalance$)
      })}
      lpToken={liquidity.liquidityTokenInfo$}
      lpTokenBalance={suspenseResource(() => liquidity.liquidityBalance$)}
      lpTokenCount={suspenseResource(() =>
        liquidity.direction === LiquidityMutationDirection.Withdraw
          ? liquidity.amountToWithdraw.get() ?? null
          : liquidity.ulpMintAmount$,
      )}
      onLpTokenCountChange={a => liquidity.amountToWithdraw.set(a ?? undefined)}
      onLpTokenCountPressMax={suspenseResource(() => {
        if (
          liquidity.amountToWithdraw?.get() != null &&
          BigNumber.isEq(liquidity.amountToWithdraw.read$)(
            liquidity.liquidityBalance$,
          )
        ) {
          return
        }
        return () => liquidity.amountToWithdraw.set(liquidity.liquidityBalance$)
      })}
      feeRate={suspenseResource(() => liquidity.feeRate$)}
      error={suspenseResource(() => Result.maybeError(liquidity.formData$))}
      onSubmit={action(() => {
        liquidity.showConfirmation = true
      })}
      onConnectWallet={action(() => (auth.showConnectWalletModal = true))}
    />
  )
}

const WiredChartPanel: FC<{ style?: StyleProp<ViewStyle> }> = props => {
  const liquidity = useLiquidityStore()
  return (
    <ChartPanel
      style={props.style}
      lpToken={liquidity.liquidityTokenInfo$}
      esToken={TokenInfoPresets.esUNW}
      stableToken={liquidity.anchorTokenInfo$}
      lpTokenPriceInStableToken={suspenseResource(() => liquidity.ulpPrice$)}
      apr={suspenseResource(() => liquidity.apr$)}
      revenueAPR={suspenseResource(() => liquidity.feeAPR$)}
      emissionAPR={suspenseResource(() => liquidity.emissionAPR$)}
      accumulatedFeeIn7Days={suspenseResource(
        () => liquidity.accumulatedFeeIn7Days$,
      )}
      distributedFeeInTotal={suspenseResource(
        () => liquidity.distributedFeeInTotal$,
      )}
      feeDistributedRates={suspenseResource(
        () => liquidity.feeDistributedRates$,
      )}
      direction={liquidity.direction}
      currentWithdrawLimit={suspenseResource(
        () => liquidity.currentWithdrawLimit$,
      )}
      poolAnalyticsData={{
        stableTokenCounts: [
          {
            token: liquidity.anchorTokenInfo$,
            amountInUSD: suspenseResource(() => liquidity.anchorLiquidity$),
          },
        ],
        marketCapacityInUSD: suspenseResource(() => liquidity.anchorLiquidity$),
        totalSuppliedLpTokenAmount: suspenseResource(
          () => liquidity.totalSuppliedLpTokenAmount$,
        ),
      }}
    />
  )
}

const WiredLiquidityScreenContent: FC<
  TopLevelNavigatorScreenProps<"Liquidity">
> = () => {
  const { $t } = useIntl()
  const spacing = useSpacing()
  const gap = spacing(5)
  const bottomAreaLayout =
    useResponsiveValue({
      _: "col",
      lg: "row",
    } as const) ?? "row"
  return (
    <PageContainer>
      <PageTitleMainText>
        {$t(
          defineMessage({
            defaultMessage: "Liquidity",
            description: "Liquidity/Page Title",
          }),
        )}
      </PageTitleMainText>
      <WiredLiquidityPoolIntro style={{ marginTop: spacing(5) }} />
      <View
        style={{
          marginTop: gap,
          flexDirection: bottomAreaLayout === "row" ? "row" : "column",
        }}
      >
        <WiredActionPanel
          style={
            bottomAreaLayout === "row"
              ? { marginRight: gap, width: 400 }
              : { marginBottom: gap }
          }
        />
        <WiredSlippageEditor />
        <WiredActionConfirmation />
        <WiredChartPanel style={{ flex: 1 }} />
      </View>
    </PageContainer>
  )
}

export const LiquidityScreen: FC<
  TopLevelNavigatorScreenProps<"Liquidity">
> = props => {
  return (
    <HideScreenOnBlur>
      <LiquidityStoreProvider>
        <Suspense>
          <WiredLiquidityScreenContent {...props} />
        </Suspense>
      </LiquidityStoreProvider>
    </HideScreenOnBlur>
  )
}
