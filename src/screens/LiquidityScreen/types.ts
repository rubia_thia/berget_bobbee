export enum LiquidityMutationDirection {
  Deposit = "Deposit",
  Withdraw = "Withdraw",
}
