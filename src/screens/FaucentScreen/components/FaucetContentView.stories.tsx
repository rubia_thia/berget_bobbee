import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { noop } from "../../../utils/fnHelpers"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../utils/TokenInfoPresets/TokenInfoPresets"
import { FaucetContentView } from "./FaucetContentView"

export default {
  title: "Page/Faucet",
  component: FaucetContentView,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof FaucetContentView>

const tpl = withTemplate(FaucetContentView, {
  availableTokens: [],
  isWalletConnected: true,
  onConnectWallet: noop,
  bnbFaucetLink: "https://testnet.bscscan.com",
  bnbFaucetBackupLink: "https://testnet.help/en/bnbfaucet/testnet",
  bnbTokenBalance: BigNumber.from(10),
  bnbTokenInfo: TokenInfoPresets.MockBTC,
  onExchange: noop,
  rate: 1000,
  testTokenBalance: BigNumber.from(20),
  testTokenInfo: TokenInfoPresets.MockUSDC,
  onStartTesting: noop,
  learnMoreLink: "https://twitter.com/UniwhaleEx/status/1610085596629995520",
})

export const Standard = tpl()

export const LackBNB = tpl(a => {
  a.bnbTokenBalance = BigNumber.from(0)
  a.testTokenBalance = BigNumber.from(0)
})

export const LackTestToken = tpl(a => {
  a.testTokenBalance = BigNumber.from(0)
})

export const ConnectWalletFirst = tpl(a => {
  a.isWalletConnected = false
})
