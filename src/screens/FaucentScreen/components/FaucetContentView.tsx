import { FC } from "react"
import { Image, Text, View } from "react-native"
import { Button, ButtonText } from "../../../components/ButtonFramework/Button"
import {
  LoadableButton,
  SmartLoadableButton,
} from "../../../components/ButtonFramework/LoadableButton"
import { Spensor } from "../../../components/Spensor"
import { TextTokenCount } from "../../../components/TextTokenCount"
import { TokenName } from "../../../components/TokenName"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { mathIs } from "../../../utils/numberHelpers/bigNumberExpressionParser"
import { HrefLink } from "../../../utils/reactNavigationHelpers/HrefLink"
import { readResource, SuspenseResource } from "../../../utils/SuspenseResource"
import { TokenInfo } from "../../../utils/TokenInfo"
import ArrowIcon from "../assets/arrow.svg"
import GiftIcon from "../assets/gift.svg"
import ArrowIndicator from "../assets/indicator.svg"
import InfoIcon from "../assets/info.svg"
import LinkIcon from "../assets/linkIcon.svg"

const Separator: FC = () => (
  <View className="self-center my-2 mx-5">
    <ArrowIcon />
  </View>
)

export type FaucetContentViewProps = {
  availableTokens: TokenInfo[]
  isWalletConnected: boolean
  onConnectWallet: () => void
  bnbFaucetLink: string
  bnbFaucetBackupLink: string
  onExchange: (token: TokenInfo) => void | Promise<void>
  bnbTokenInfo: TokenInfo
  bnbTokenBalance: SuspenseResource<BigNumber>
  testTokenInfo: TokenInfo
  testTokenBalance: SuspenseResource<BigNumber>
  rate: number
  onStartTesting: () => void
  learnMoreLink: string
}

export const FaucetContentView: FC<FaucetContentViewProps> = props => (
  <View className="w-full max-w-5xl mx-auto px-4">
    <Text className="text-3xl leading-9 font-normal my-10 text-gray-900">
      Get Test Tokens
    </Text>
    <View className="flex-col md:flex-row">
      <View className="bg-white rounded p-4 space-y-5 flex-1">
        <Text className="text-blue-600 text-xl leading-7">Step 1</Text>
        <View className="bg-blue-50 p-2.5 justify-center items-center rounded">
          <Image
            style={{ height: 64 }}
            resizeMode={"contain"}
            source={require("../assets/step1.png")}
          />
        </View>
        <Button
          className="flex-row space-x-2"
          onPress={() => {
            window.open(props.bnbFaucetLink, "_blank")
          }}
        >
          <ButtonText>Get testnet BNB</ButtonText>
          <LinkIcon />
        </Button>
        <View className="bg-gray-100 py-2.5 px-3 rounded space-y-1">
          <Text className="text-gray-500 text-sm leading-5">
            Backup test BNB faucet address:
          </Text>
          <HrefLink href={props.bnbFaucetBackupLink}>
            <Text className="text-blue-600">{props.bnbFaucetBackupLink}</Text>
          </HrefLink>
        </View>
        <View className="space-x-2 flex-row">
          <InfoIcon />
          <Text className="flex-1 text-xs leading-4 text-gray-500">
            Test BNB are provided by the official fuacet of the BNB Fork
            Testnet.
          </Text>
        </View>
      </View>
      <Separator />
      <View className="bg-white rounded p-4 space-y-5 flex-1">
        <Text className="text-blue-600 text-xl leading-7">Step 2</Text>
        <View className="bg-blue-50 p-2.5 justify-center items-center rounded">
          <Image
            style={{ height: 64 }}
            resizeMode={"contain"}
            source={require("../assets/step2.png")}
          />
        </View>
        <View>
          <Spensor
            fallback={
              props.isWalletConnected ? (
                <LoadableButton loading={true} />
              ) : (
                <Button onPress={props.onConnectWallet}>Connect Wallet</Button>
              )
            }
          >
            {() => {
              const balance = readResource(props.bnbTokenBalance)
              if (mathIs`${balance} > ${0.1}`) {
                return (
                  <View className="space-y-1">
                    {props.availableTokens.map(token => (
                      <SmartLoadableButton
                        className="flex-row space-x-2"
                        onPress={() => props.onExchange(token)}
                      >
                        <ButtonText>
                          Get <TokenName token={token} />
                        </ButtonText>
                        <ArrowIndicator />
                      </SmartLoadableButton>
                    ))}
                  </View>
                )
              }
              return (
                <Button className="flex-row space-x-2" disabled>
                  <ButtonText>Get BNB first</ButtonText>
                  <ArrowIndicator />
                </Button>
              )
            }}
          </Spensor>
        </View>
        <View className="bg-gray-100 py-2.5 px-3 rounded space-y-1">
          <Text className="text-gray-500 text-sm leading-5">Your Balance</Text>
          <View className="flex-row justify-between">
            <TokenName
              className="text-xs text-gray-500"
              token={props.bnbTokenInfo}
            />
            <TextTokenCount
              className="text-xs"
              token={props.bnbTokenInfo}
              count={props.bnbTokenBalance}
            />
          </View>
          <View className="flex-row justify-between">
            <TokenName
              className="text-xs text-gray-500"
              token={props.testTokenInfo}
            />
            <TextTokenCount
              className="text-xs"
              token={props.testTokenInfo}
              count={props.testTokenBalance}
            />
          </View>
        </View>
        <View className="space-x-2 flex-row">
          <InfoIcon />
          <Text className="flex-1 text-xs leading-4 text-gray-500">
            To acquire {props.rate} test USD, it will cost 0.1 test BNB per
            transaction.
          </Text>
        </View>
      </View>
      <Separator />
      <View className="bg-white rounded p-4 space-y-5 flex-1">
        <Text className="text-blue-600 text-xl leading-7">Step 3</Text>
        <View className="bg-blue-50 p-2.5 justify-center items-center rounded">
          <Image
            style={{ height: 64 }}
            resizeMode={"contain"}
            source={require("../assets/step3.png")}
          />
        </View>
        <View>
          <Spensor
            fallback={
              props.isWalletConnected ? (
                <LoadableButton loading={true} />
              ) : (
                <Button onPress={props.onConnectWallet}>Connect Wallet</Button>
              )
            }
          >
            {() => {
              const balance = readResource(props.testTokenBalance)
              return (
                <Button
                  className="flex-row space-x-2"
                  onPress={props.onStartTesting}
                  disabled={mathIs`${balance} == ${0}`}
                >
                  <ButtonText>Start to test</ButtonText>
                  <LinkIcon />
                </Button>
              )
            }}
          </Spensor>
        </View>
        <View className="bg-gray-100 py-2.5 px-3 rounded space-y-1">
          <View className="flex-row items-center">
            <GiftIcon />
            <Text className="ml-2 leading-6 font-medium text-pink-600">
              Airdrop
            </Text>
          </View>
          <Text className="flex-1 text-xs leading-4 text-gray-900">
            You will get airdrop by completing the test task.
            <Text
              className="text-blue-600"
              onPress={() => {
                window.open(props.learnMoreLink, "_blank")
              }}
            >
              {" Learn more >"}
            </Text>
          </Text>
        </View>
      </View>
    </View>
  </View>
)
