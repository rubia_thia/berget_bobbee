import { action } from "mobx"
import { FC } from "react"
import { TopLevelNavigatorScreenProps } from "../../navigation/navigators/TopLevelNavigatorTypes"
import { useAuthStore } from "../../stores/AuthStore/useAuthStore"
import { useContractStore } from "../../stores/contracts/useContractStore"
import {
  ERC20Tokens,
  StableToken,
} from "../../stores/CurrencyStore/CurrencyStore.service"
import { useCurrencyStore } from "../../stores/CurrencyStore/useCurrencyStore"
import { useTradeInfoStore } from "../../stores/TradeStore/useTradeInfoStore"
import { fromNativeToContract } from "../../utils/contractHelpers/bigNumberContracts"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { suspenseResource } from "../../utils/SuspenseResource"
import { TokenInfo } from "../../utils/TokenInfo"
import { HideScreenOnBlur } from "../components/HideScreenOnBlur"
import { FaucetContentView } from "./components/FaucetContentView"

export const FaucetScreen: FC<TopLevelNavigatorScreenProps<"Faucet">> = ({
  navigation,
}) => {
  const currency = useCurrencyStore()
  const contracts = useContractStore()
  const auth = useAuthStore()
  const tradeInfo = useTradeInfoStore()
  return (
    <HideScreenOnBlur>
      <FaucetContentView
        isWalletConnected={auth.connected}
        availableTokens={tradeInfo.allSupportedStableToken$.map(
          currency.tokenInfo$,
        )}
        onConnectWallet={action(() => (auth.showConnectWalletModal = true))}
        bnbFaucetLink={"https://testnet.bnbchain.org/faucet-smart"}
        bnbFaucetBackupLink="https://testnet.help/en/bnbfaucet/testnet"
        bnbTokenBalance={suspenseResource(() => currency.ethBalance.value$)}
        bnbTokenInfo={currency.baseTokenInfo$("BNB_USD")}
        onExchange={async (token: TokenInfo) => {
          const token$ = contracts.mockToken$(token.id as StableToken)
          const decimals = await token$.decimals()
          await token$
            .deposit({
              value: fromNativeToContract(BigNumber.from(0.1), { decimals }),
            })
            .then(a => a.wait())
        }}
        rate={1000}
        testTokenBalance={suspenseResource(() =>
          currency.balance$(ERC20Tokens.AnchorToken),
        )}
        testTokenInfo={currency.tokenInfo$(ERC20Tokens.AnchorToken)}
        onStartTesting={() => {
          navigation.navigate("Trade", {})
        }}
        learnMoreLink="https://twitter.com/UniwhaleEx/status/1610085596629995520"
      />
    </HideScreenOnBlur>
  )
}
