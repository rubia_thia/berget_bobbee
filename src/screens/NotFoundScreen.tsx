import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { View } from "react-native"
import { Button } from "../components/ButtonFramework/Button"
import {
  ActionButtonVariant,
  DetailText,
  FullPageErrorMessage,
} from "../components/FullPageErrorMessage/FullPageErrorMessage"
import { TopLevelNavigatorScreenProps } from "../navigation/navigators/TopLevelNavigatorTypes"

export const NotFoundScreen: FC<
  TopLevelNavigatorScreenProps<"NotFound">
> = props => {
  const { navigation } = props
  const { $t } = useIntl()

  return (
    <View style={{ flex: 1, alignSelf: "stretch" }}>
      <FullPageErrorMessage
        titleText={"404"}
        subtitleText={$t(
          defineMessage({
            defaultMessage: "Oops! Where is my whale?",
            description: "NotFoundScreen/sub title",
          }),
        )}
        detail={
          <DetailText>
            {$t(
              defineMessage({
                defaultMessage:
                  "The page you are looking for doesn't exist or has been moved",
                description: "NotFoundScreen/detail text",
              }),
            )}
          </DetailText>
        }
        actionArea={
          <Button
            Variant={ActionButtonVariant}
            onPress={() => {
              navigation.navigate("Trade")
            }}
          >
            {$t(
              defineMessage({
                defaultMessage: "Go Home",
                description: "NotFoundScreen/go home button text",
              }),
            )}
          </Button>
        }
      />
    </View>
  )
}
