import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { TokenInfoPresets } from "../../../../utils/TokenInfoPresets/TokenInfoPresets"
import { OrderDirection } from "../../../TradeScreen/types"
import { DebugPositionRecord, DebugPositionStatus } from "./types"

export const positionRecords: DebugPositionRecord[] = [
  {
    status: DebugPositionStatus.open,
    adjustedMarkPriceOrClosing: BigNumber.from(1051.35),
    lastUpdatedAt: new Date(),
    openedAt: new Date(),
    user: "0x0000",
    direction: OrderDirection.Long,
    id: "a",
    quoteToken: TokenInfoPresets.MockUSDC,
    baseToken: TokenInfoPresets.MockBTC,

    leverage: BigNumber.from(10),
    positionSize: BigNumber.from(400.25),
    margin: BigNumber.from(40.45),
    entryPrice: BigNumber.from(1052.35),
    markPrice: BigNumber.from(1051.35),
    liquidityPrice: BigNumber.from(874.34),

    stopLossPrice: BigNumber.from(0.4169),
    takeProfitPrice: BigNumber.from(0.4169),

    pnl: {
      delta: BigNumber.from(-10.86),
      deltaPercentage: BigNumber.from(-0.0853),
    },

    fees: {
      total: BigNumber.from(-0.22),
      open: BigNumber.from(-1.2),
      funding: BigNumber.from(0.2),
      rollover: BigNumber.from(-0.3),
    },
  },
  {
    status: DebugPositionStatus.open,
    adjustedMarkPriceOrClosing: BigNumber.from(1051.35),
    lastUpdatedAt: new Date(),
    openedAt: new Date(),
    direction: OrderDirection.Long,
    user: "0x0000",
    id: "b",
    quoteToken: TokenInfoPresets.MockUSDC,
    baseToken: TokenInfoPresets.MockBTC,

    leverage: BigNumber.from(10),
    positionSize: BigNumber.from(400.25),
    margin: BigNumber.from(40.45),
    entryPrice: BigNumber.from(1052.35),
    markPrice: BigNumber.from(1051.35),
    liquidityPrice: BigNumber.from(874.34),

    stopLossPrice: null,
    takeProfitPrice: BigNumber.from(0.4169),

    pnl: {
      delta: BigNumber.from(-10.86),
      deltaPercentage: BigNumber.from(-0.0853),
    },

    fees: {
      total: BigNumber.from(-0.22),
      open: BigNumber.from(-1.2),
      funding: BigNumber.from(0.2),
      rollover: BigNumber.from(-0.3),
    },
  },
  {
    status: DebugPositionStatus.open,
    adjustedMarkPriceOrClosing: BigNumber.from(1051.35),
    lastUpdatedAt: new Date(),
    openedAt: new Date(),
    user: "0x0000",
    direction: OrderDirection.Short,
    id: "c",
    quoteToken: TokenInfoPresets.MockUSDC,
    baseToken: TokenInfoPresets.MockBTC,

    leverage: BigNumber.from(10),
    positionSize: BigNumber.from(400.25),
    margin: BigNumber.from(40.45),
    entryPrice: BigNumber.from(1052.35),
    markPrice: BigNumber.from(1051.35),
    liquidityPrice: BigNumber.from(874.34),

    stopLossPrice: null,
    takeProfitPrice: null,

    pnl: {
      delta: BigNumber.from(-10.86),
      deltaPercentage: BigNumber.from(-0.0853),
    },

    fees: {
      total: BigNumber.from(-0.22),
      open: BigNumber.from(-1.2),
      funding: BigNumber.from(0.2),
      rollover: BigNumber.from(-0.3),
    },
  },
]
