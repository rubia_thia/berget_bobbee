import { format } from "date-fns"
import * as Clipboard from "expo-clipboard"
import { omit } from "ramda"
import { FC } from "react"
import { useIntl } from "react-intl"
import { ActivityIndicator, Text, useWindowDimensions } from "react-native"
import { copied$t } from "../../../../commonIntlMessages"
import { PlainIconButtonVariant } from "../../../../components/Button/PlainIconButtonVariant"
import {
  Button,
  ButtonTextStyleConsumer,
} from "../../../../components/ButtonFramework/Button"
import { EmptyState } from "../../../../components/EmptyState/EmptyState"
import {
  InfoListItemDetail,
  InfoListItemDetailText,
} from "../../../../components/InfoList/InfoListItemDetail"
import { InfoListItemTitle } from "../../../../components/InfoList/InfoListItemTitle"
import { useMessage } from "../../../../components/MessageProvider/MessageProvider"
import {
  PaginationInfo,
  PaginationProps,
} from "../../../../components/Pagination/Pagination"
import { PercentNumber } from "../../../../components/PercentNumber"
import { Spensor } from "../../../../components/Spensor"
import { TokenCountAsCurrency } from "../../../../components/TextTokenCount"
import { useColors } from "../../../../components/Themed/color"
import { useSpacing } from "../../../../components/Themed/spacing"
import { TooltippifiedText } from "../../../../components/Tooltip"
import { TruncatedAddressText } from "../../../../components/TruncatedAddress"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import {
  readResource,
  SuspenseResource,
} from "../../../../utils/SuspenseResource"
import { HorizontalScrollableContainer } from "../../../TradeScreen/components/HorizontalScrollableContainer"
import { fields$t } from "../../../TradeScreen/components/OrderHistory/_/commonIntlMessages"
import { FeesTooltipContent } from "../../../TradeScreen/components/OrderHistory/_/FeesTooltipContent"
import {
  ActionTriggerCell,
  CellTiles,
  FieldCell,
  HeadCell,
  RecordRowContainer,
  RecordTableContainerWithPagination,
} from "../../../TradeScreen/components/OrderHistory/_/RecordRow"
import { TokenNameStack } from "../../../TradeScreen/components/TokenNameStack"
import { orderDirection$t } from "../../../TradeScreen/components/_/commonIntlMessages"
import { Colors } from "../../../TradeScreen/components/_/designTokens"
import { OrderDirection } from "../../../TradeScreen/types"
import CopyIcon from "./assets/copyIcon.svg"
import { DebugPositionRecord } from "./types"

export const DebugPositionsTabContent: FC<{
  horizontalScrollable: boolean
  horizontalMargin: number
  records: SuspenseResource<DebugPositionRecord[]>
  paginationInfo: SuspenseResource<PaginationInfo>
  onPaginationChange: PaginationProps["onChange"]
}> = props => {
  const spacing = useSpacing()

  const { width } = useWindowDimensions()
  return (
    <>
      <Spensor fallback={<ActivityIndicator />}>
        {() =>
          readResource(props.records).length <= 0 ? (
            <EmptyState />
          ) : (
            <RecordTableContainerWithPagination
              paginationInfo={props.paginationInfo}
              onPaginationChange={props.onPaginationChange}
            >
              {readResource(props.records).map(r => (
                <HorizontalScrollableContainer
                  key={r.id}
                  style={{ marginBottom: spacing(1) }}
                  scrollable={props.horizontalScrollable}
                  horizontalMargin={props.horizontalMargin}
                >
                  <RowTemplate mainPartMaxWidth={width - 200} record={r} />
                </HorizontalScrollableContainer>
              ))}
            </RecordTableContainerWithPagination>
          )
        }
      </Spensor>
    </>
  )
}

const RowTemplate: FC<{
  mainPartMaxWidth?: number
  record: DebugPositionRecord
}> = props => {
  const colors = useColors()

  const intl = useIntl()
  const { $t } = intl

  const { record } = props
  const message = useMessage()
  return (
    <RecordRowContainer>
      <HeadCell>
        <InfoListItemDetail>
          <InfoListItemDetailText
            style={{
              fontSize: 14,
              fontWeight: "600",
              color: colors("gray-900"),
            }}
          >
            <TokenNameStack
              baseToken={record.baseToken}
              quoteToken={record.quoteToken}
            />
          </InfoListItemDetailText>
          <InfoListItemDetailText
            style={{
              fontSize: 14,
              fontWeight: "500",
              color: colors("gray-500"),
            }}
          >
            {BigNumber.toString(
              BigNumber.round({ precision: 2 }, record.leverage),
            )}
            x&nbsp;
            <Text
              style={{
                color:
                  record.direction === OrderDirection.Short
                    ? Colors.shortColor
                    : Colors.longColor,
              }}
            >
              {orderDirection$t.fromOrderDirection(intl, record.direction)}
            </Text>
          </InfoListItemDetailText>
        </InfoListItemDetail>
      </HeadCell>

      <CellTiles style={{ maxWidth: props.mainPartMaxWidth }}>
        <FieldCell>
          <InfoListItemTitle>{$t(fields$t.status)}</InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              {props.record.status}
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </FieldCell>

        <FieldCell>
          <InfoListItemTitle>{$t(fields$t.openDate)}</InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              {format(props.record.openedAt, "yyyy-MM-dd HH:mm:ss")}
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </FieldCell>

        <FieldCell>
          <InfoListItemTitle>{$t(fields$t.lastUpdatedDate)}</InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              {format(props.record.lastUpdatedAt, "yyyy-MM-dd HH:mm:ss")}
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </FieldCell>

        <FieldCell>
          <InfoListItemTitle>{$t(fields$t.address)}</InfoListItemTitle>
          <InfoListItemDetail>
            <Button
              Variant={p => (
                <PlainIconButtonVariant {...p} iconRight={<CopyIcon />} />
              )}
              onPress={async (): Promise<void> => {
                await Clipboard.setStringAsync(record.user)
                message.show({ message: $t(copied$t) })
              }}
            >
              <ButtonTextStyleConsumer>
                {style => (
                  <InfoListItemDetailText style={omit(["fontSize"], style)}>
                    <TruncatedAddressText address={record.user} />
                  </InfoListItemDetailText>
                )}
              </ButtonTextStyleConsumer>
            </Button>
          </InfoListItemDetail>
        </FieldCell>

        <FieldCell>
          <InfoListItemTitle>{$t(fields$t.positionSize)}</InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              <TokenCountAsCurrency
                token={record.quoteToken}
                count={record.positionSize}
              />
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </FieldCell>

        <FieldCell>
          <InfoListItemTitle>{$t(fields$t.margin)}</InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              <TokenCountAsCurrency
                token={record.quoteToken}
                count={record.margin}
              />
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </FieldCell>

        <FieldCell>
          <InfoListItemTitle>{$t(fields$t.entryPrice)}</InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              <TokenCountAsCurrency
                token={record.quoteToken}
                count={record.entryPrice}
              />
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </FieldCell>

        <FieldCell>
          <InfoListItemTitle>{$t(fields$t.markPrice)}</InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              <TokenCountAsCurrency
                token={record.quoteToken}
                count={record.markPrice}
              />
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </FieldCell>

        <FieldCell>
          <InfoListItemTitle>
            {$t(fields$t.adjustedMarkPrice)}
          </InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              <TokenCountAsCurrency
                token={record.quoteToken}
                count={record.adjustedMarkPriceOrClosing}
              />
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </FieldCell>

        <FieldCell>
          <InfoListItemTitle>{$t(fields$t.liquidityPrice)}</InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText>
              <TokenCountAsCurrency
                token={record.quoteToken}
                count={record.liquidityPrice}
              />
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </FieldCell>

        <ActionTriggerCell
          quoteToken={props.record.quoteToken}
          takeProfitPrice={record.takeProfitPrice}
          stopLossPrice={record.stopLossPrice}
        />

        <FieldCell>
          <InfoListItemTitle>{$t(fields$t.pnl)}</InfoListItemTitle>

          <InfoListItemDetail>
            <Spensor>
              {() => (
                <InfoListItemDetailText
                  style={{
                    color: BigNumber.isNegative(readResource(record.pnl).delta)
                      ? Colors.decreaseColor
                      : Colors.increaseColor,
                  }}
                  numberOfLines={1}
                >
                  <TokenCountAsCurrency
                    token={record.quoteToken}
                    count={readResource(record.pnl).delta}
                  />
                  &nbsp; (
                  <PercentNumber
                    number={readResource(record.pnl).deltaPercentage}
                  />
                  )
                </InfoListItemDetailText>
              )}
            </Spensor>
          </InfoListItemDetail>
        </FieldCell>

        <FieldCell>
          <InfoListItemTitle>{$t(fields$t.fees)}</InfoListItemTitle>
          <InfoListItemDetail>
            <InfoListItemDetailText numberOfLines={1}>
              <TooltippifiedText
                content={
                  <FeesTooltipContent
                    quoteToken={record.quoteToken}
                    total={record.fees.total}
                    open={record.fees.open}
                    funding={record.fees.funding}
                    rollover={record.fees.rollover}
                  />
                }
              >
                <Spensor fallback={"-"}>
                  {() => (
                    <TokenCountAsCurrency
                      token={record.quoteToken}
                      count={readResource(record.fees).total}
                    />
                  )}
                </Spensor>
              </TooltippifiedText>
            </InfoListItemDetailText>
          </InfoListItemDetail>
        </FieldCell>

        <FieldCell>
          <InfoListItemTitle>{$t(fields$t.orderHash)}</InfoListItemTitle>
          <InfoListItemDetail>
            <Button
              Variant={p => (
                <PlainIconButtonVariant {...p} iconRight={<CopyIcon />} />
              )}
              onPress={async (): Promise<void> => {
                await Clipboard.setStringAsync(record.id)
                message.show({ message: $t(copied$t) })
              }}
            >
              <ButtonTextStyleConsumer>
                {style => (
                  <InfoListItemDetailText style={omit(["fontSize"], style)}>
                    <TruncatedAddressText address={record.id} />
                  </InfoListItemDetailText>
                )}
              </ButtonTextStyleConsumer>
            </Button>
          </InfoListItemDetail>
        </FieldCell>
      </CellTiles>
    </RecordRowContainer>
  )
}
