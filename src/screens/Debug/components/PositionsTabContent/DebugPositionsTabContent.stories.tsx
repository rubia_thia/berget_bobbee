import { ComponentMeta } from "@storybook/react"
import { View } from "react-native"
import { BackgroundColor } from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { MessageProvider } from "../../../../components/MessageProvider/MessageProvider"
import { DebugPositionsTabContent } from "./DebugPositionsTabContent"
import { positionRecords } from "./DebugPositionsTabContent.mockData"

export default {
  title: "Page/TradeScreen/Operation History Panel/PositionsTabContent",
  component: DebugPositionsTabContent,
  decorators: [
    Story => (
      <MessageProvider>
        <View style={{ margin: 10 }}>
          <Story />
        </View>
      </MessageProvider>
    ),
    BackgroundColor("#F3F4F6"),
  ],
} as ComponentMeta<typeof DebugPositionsTabContent>

const template = withTemplate(DebugPositionsTabContent, {
  horizontalScrollable: false,
  horizontalMargin: 0,
  records: positionRecords,
  paginationInfo: {
    pageSize: 20,
    current: 3,
    total: 1034,
  },
})

export const Normal = template()

export const HorizontalScrollable = template(p => {
  p.horizontalScrollable = true
  p.horizontalMargin = 10
})
