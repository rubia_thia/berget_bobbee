import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { SuspenseResource } from "../../../../utils/SuspenseResource"
import { TokenInfo } from "../../../../utils/TokenInfo"
import { OrderDirection } from "../../../TradeScreen/types"

export enum DebugPositionStatus {
  open = "open",
  closedByUser = "closed by user",
  closedByLiquidation = "closed by liquidation",
  closedByStopLoss = "closed by stop loss",
  closedByTakeProfit = "closed by take profit",
}

export interface DebugPositionRecord {
  openedAt: Date
  lastUpdatedAt: Date
  status: DebugPositionStatus

  id: string
  user: string
  baseToken: TokenInfo
  quoteToken: TokenInfo

  leverage: BigNumber
  positionSize: BigNumber
  margin: BigNumber
  entryPrice: BigNumber
  markPrice: SuspenseResource<BigNumber>
  adjustedMarkPriceOrClosing: SuspenseResource<BigNumber>
  liquidityPrice: BigNumber

  direction: OrderDirection

  stopLossPrice: null | BigNumber
  takeProfitPrice: null | BigNumber

  pnl: SuspenseResource<{
    delta: BigNumber
    deltaPercentage: BigNumber
  }>

  fees: {
    total: SuspenseResource<BigNumber>
    open: SuspenseResource<BigNumber>
    funding: SuspenseResource<BigNumber>
    rollover: SuspenseResource<BigNumber>
  }
}
