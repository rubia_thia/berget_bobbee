import { FC } from "react"
import { Spensor } from "../../components/Spensor"
import { useSpacing } from "../../components/Themed/spacing"
import { DebugNavigatorScreenProps } from "../../navigation/navigators/DebugScreenNavigatorTypes"
import { useDebugStore } from "../../stores/DebugStore/useDebugStore"
import { suspenseResource } from "../../utils/SuspenseResource"
import { DebugPositionsTabContent } from "./components/PositionsTabContent/DebugPositionsTabContent"

export const LatestTxScreen: FC<DebugNavigatorScreenProps<"Latest">> = () => {
  const debug = useDebugStore()
  const spacing = useSpacing()
  return (
    <Spensor>
      {() => (
        <DebugPositionsTabContent
          horizontalScrollable
          horizontalMargin={spacing(1)}
          records={suspenseResource(
            () => debug.openPositionsPaginationStore.records$,
          )}
          paginationInfo={suspenseResource(
            () => debug.openPositionsPaginationStore.paginationInfo$,
          )}
          onPaginationChange={p =>
            debug.openPositionsPaginationStore.onPaginationChange(p)
          }
        />
      )}
    </Spensor>
  )
}
