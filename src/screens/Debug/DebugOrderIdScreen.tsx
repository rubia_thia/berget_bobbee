import { FC } from "react"
import { Spensor } from "../../components/Spensor"
import { useSpacing } from "../../components/Themed/spacing"
import { DebugNavigatorScreenProps } from "../../navigation/navigators/DebugScreenNavigatorTypes"
import { useDebugStore } from "../../stores/DebugStore/useDebugStore"
import { suspenseResource } from "../../utils/SuspenseResource"
import { DebugPositionsTabContent } from "./components/PositionsTabContent/DebugPositionsTabContent"

export const DebugOrderIdScreen: FC<
  DebugNavigatorScreenProps<"OrderDetail">
> = ({
  route: {
    params: { orderId },
  },
}) => {
  const debug = useDebugStore()
  const spacing = useSpacing()
  const store = debug.orderDetail(orderId).paginationStore
  return (
    <Spensor>
      {() => (
        <DebugPositionsTabContent
          horizontalScrollable
          horizontalMargin={spacing(1)}
          records={suspenseResource(() => store.records$)}
          paginationInfo={suspenseResource(() => store.paginationInfo$)}
          onPaginationChange={p => store.onPaginationChange(p)}
        />
      )}
    </Spensor>
  )
}
