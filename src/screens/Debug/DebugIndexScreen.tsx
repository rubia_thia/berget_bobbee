import { useIsFocused } from "@react-navigation/native"
import { ErrorBoundary } from "@sentry/react-native"
import { action, runInAction } from "mobx"
import { FC, useEffect, useMemo, useState } from "react"
import { ScrollView, Text, TextInput, View } from "react-native"
import { from } from "rxjs"
import { Button } from "../../components/ButtonFramework/Button"
import { SmartLoadableButton } from "../../components/ButtonFramework/LoadableButton"
import { Spensor } from "../../components/Spensor"
import { DebugNavigatorScreenProps } from "../../navigation/navigators/DebugScreenNavigatorTypes"
import { useAppEnvStore } from "../../stores/AppEnvStore/useAppEnvStore"
import { useAuthStore } from "../../stores/AuthStore/useAuthStore"
import { useContractStore } from "../../stores/contracts/useContractStore"
import {
  DebugStoreProvider,
  useDebugStore,
} from "../../stores/DebugStore/useDebugStore"
import { TradeStoreProvider } from "../../stores/TradeStore/useTradeStore"
import { LazyValue } from "../../stores/utils/LazyValue"
import { waitFor } from "../../stores/utils/waitFor"
import { fromContractToNative } from "../../utils/contractHelpers/bigNumberContracts"
import { DisplayableError } from "../../utils/errorHelpers"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { FCS } from "../../utils/reactHelpers/types"

const SetOrigin: FC = () => {
  const appEnv = useAppEnvStore()
  const [origin, setOrigin] = useState(appEnv.origin)
  return (
    <>
      <TextInput
        placeholderTextColor="gray"
        className="p-3 bg-gray-300 rounded mb-2"
        value={origin}
        onChangeText={setOrigin}
        placeholder="Origin to set"
      />
      <Button onPress={action(() => (appEnv.origin = origin))}>
        Set Origin
      </Button>
    </>
  )
}

const MockUser: FCS = ({ style }) => {
  const appEnv = useAppEnvStore()
  const [mockUser, setMockUser] = useState(appEnv.mockUser ?? "")
  return (
    <View className="space-y-2" style={style}>
      <TextInput
        placeholderTextColor="gray"
        className="p-3 bg-gray-300 rounded my-2"
        value={mockUser}
        onChangeText={setMockUser}
        placeholder="Address to Mock"
      />
      <Button
        className="my-2"
        onPress={action(() => (appEnv.mockUser = mockUser))}
      >
        Mock User
      </Button>
      <Button
        onPress={() => {
          setMockUser("")
          runInAction(() => {
            appEnv.mockUser = undefined
          })
        }}
      >
        Clear Mock
      </Button>
    </View>
  )
}

const CurrentBlock: FC = () => {
  const block$ = useAppEnvStore().currentBlockObservable$
  const [block, setBlock] = useState<number>()
  const isFocused = useIsFocused()
  useEffect(() => {
    if (!isFocused) {
      return
    }
    const sub = block$.subscribe({ next: setBlock })
    return () => {
      sub.unsubscribe()
    }
  }, [block$, isFocused])
  return <Text>{block}</Text>
}

const PoolLiquidity: FC = () => {
  const contracts = useContractStore()
  const lpBalance = useMemo(
    () =>
      new LazyValue(
        () => contracts.liquidityPool$,
        lp => from(lp.getTotalStaked().then(fromContractToNative)),
      ),
    [contracts],
  )
  const stakeUNWBalance = useMemo(
    () =>
      new LazyValue(
        () => ({
          rev: contracts.revenuePool$,
          lp: contracts.liquidityPool$,
        }),
        ({ rev, lp }) =>
          from(rev["balance(address)"](lp.address).then(fromContractToNative)),
      ),
    [contracts],
  )
  return (
    <View>
      <Text>
        Current Block:
        <Spensor>{() => <CurrentBlock />}</Spensor>
        {"\n"}Total staked LP:{" "}
        <Spensor fallback="--">
          {() => <Text>{BigNumber.toString(lpBalance.value$)}</Text>}
        </Spensor>
        {"\n"}StakingHelper UNW balance:{" "}
        <Spensor fallback="--">
          {() => <Text>{BigNumber.toString(stakeUNWBalance.value$)}</Text>}
        </Spensor>
      </Text>
    </View>
  )
}
const TestMyFee: FCS = ({ style }) => {
  const account = useAuthStore()
  const { registryCore$ } = useContractStore()
  return (
    <Spensor>
      {() => (
        <Button
          style={style}
          onPress={() => {
            void Promise.all([
              registryCore$
                .getOpenFee(account.account$)
                .then(a => fromContractToNative(a.fee)),
              registryCore$
                .getCloseFee(account.account$)
                .then(a => fromContractToNative(a.fee)),
            ]).then(a =>
              alert(
                [
                  `Open Fee: ${BigNumber.toString(a[0])}`,
                  `Close Fee: ${BigNumber.toString(a[1])}`,
                ].join(", "),
              ),
            )
          }}
        >
          Test My Fee Rate
        </Button>
      )}
    </Spensor>
  )
}

const MintGenesisNFT: FCS = ({ style }) => {
  const contracts = useContractStore()
  const auth = useAuthStore()
  return (
    <View className="space-y-2" style={style}>
      <Button
        onPress={async () => {
          await contracts.genesisPass$["safeMint()"]()
        }}
      >
        Mint UGP NFT
      </Button>
    </View>
  )
}

const FakeCrashErrorButton: FCS = ({ style }) => {
  const [err, setErr] = useState<Error>()

  if (err != null) throw err

  return (
    <Button
      style={style}
      onPress={() => {
        try {
          ;(globalThis as any).undefinedMethod()
        } catch (e) {
          setErr(new DisplayableError("It's a testing error", { cause: e }))
        }
      }}
    >
      Fake crash (to test Sentry)
    </Button>
  )
}

const TroubleShoot: FCS<
  Pick<DebugNavigatorScreenProps<"Index">, "navigation">
> = ({ navigation, style }) => {
  const [mockUser, setMockUser] = useState("")
  const [txIdOrOrderID, setTxIdOrOrderID] = useState("")
  const debug = useDebugStore()
  return (
    <View className="space-y-2 mt-3" style={style}>
      <Text>Watch Transactions</Text>
      <Button
        onPress={() => {
          navigation.push("Latest")
        }}
      >
        Latest Tx
      </Button>
      <TextInput
        placeholderTextColor="gray"
        className="p-3 bg-gray-300 rounded my-2"
        value={mockUser}
        onChangeText={setMockUser}
        placeholder="Address to Watch"
      />
      <Button
        disabled={!mockUser}
        onPress={() => {
          navigation.push("Wallet", { address: mockUser })
        }}
      >
        Watch Address
      </Button>
      <TextInput
        placeholderTextColor="gray"
        className="p-3 bg-gray-300 rounded my-2"
        value={txIdOrOrderID}
        onChangeText={setTxIdOrOrderID}
        placeholder="Order ID or Tx Hash"
      />
      <SmartLoadableButton
        disabled={!txIdOrOrderID}
        onPress={async () => {
          const orderId = await debug.getOrderHashesByTxHash(txIdOrOrderID)
          navigation.push("OrderDetail", { orderId: orderId ?? txIdOrOrderID })
        }}
      >
        Read Order
      </SmartLoadableButton>
    </View>
  )
}

const AddTokensToWallet: FCS = ({ style }) => {
  const contracts = useContractStore()
  const auth = useAuthStore()
  return (
    <View style={style} className="space-y-2">
      <Button
        onPress={async () => {
          const lib = await waitFor(() => auth.library$)
          const ugp = await waitFor(() => contracts.genesisPass$)
          await lib.send("wallet_watchAsset", {
            type: "ERC20",
            options: {
              address: ugp.address,
              symbol: "UGP",
              decimals: 0,
              image: "https://example.com/token-image.png",
            },
          } as any)
        }}
      >
        Add UGP To Wallet
      </Button>
      <Button
        onPress={async () => {
          const lib = await waitFor(() => auth.library$)
          const unw = await waitFor(() => contracts.uniwhaleToken$)
          await lib.send("wallet_watchAsset", {
            type: "ERC20",
            options: {
              address: unw.address,
              symbol: "UNW",
              decimals: 18,
            },
          } as any)
        }}
      >
        Add UNW To Wallet
      </Button>
      <Button
        onPress={async () => {
          const lib = await waitFor(() => auth.library$)
          const esUnw = await waitFor(() => contracts.esUniwhaleToken$)
          await lib.send("wallet_watchAsset", {
            type: "ERC20",
            options: {
              address: esUnw.address,
              symbol: "esUNW",
              decimals: 18,
            },
          } as any)
        }}
      >
        Add esUNW To Wallet
      </Button>
      <Button
        onPress={async () => {
          const lib = await waitFor(() => auth.library$)
          const ulp = await waitFor(() => contracts.liquidityPool$)
          await lib.send("wallet_watchAsset", {
            type: "ERC20",
            options: {
              address: ulp.address,
              symbol: "ULP",
              decimals: 18,
            },
          } as any)
        }}
      >
        Add ULP To Wallet
      </Button>
    </View>
  )
}

const DebugIndexScreen: FC<DebugNavigatorScreenProps<"Index">> = ({
  navigation,
}) => {
  return (
    <ScrollView className="space-y-2" contentContainerStyle={{ padding: 16 }}>
      <Text>Version: {process.env["CF_PAGES_COMMIT_SHA"] || "Dev"}</Text>
      <SetOrigin />
      <FakeCrashErrorButton />
      <ErrorBoundary>
        <View className="space-y-2">
          <MockUser />
          <TestMyFee />
          <MintGenesisNFT />
          <DebugStoreProvider>
            <TroubleShoot navigation={navigation} />
          </DebugStoreProvider>
          <AddTokensToWallet />
          <TradeStoreProvider>
            <PoolLiquidity />
          </TradeStoreProvider>
        </View>
      </ErrorBoundary>
    </ScrollView>
  )
}

export default DebugIndexScreen
