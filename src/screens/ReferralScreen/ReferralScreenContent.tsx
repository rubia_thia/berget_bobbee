import { FC, ReactNode, useState } from "react"
import { defineMessage, useIntl } from "react-intl"
import {
  ActivityIndicator,
  StyleProp,
  Text,
  View,
  ViewStyle,
} from "react-native"
import { Path, Svg } from "react-native-svg"
import { connectWallet$t } from "../../commonIntlMessages"
import { BlueButtonVariant } from "../../components/Button/BlueButtonVariant"
import { useCompactButtonVariant } from "../../components/Button/useCompactButtonVariant"
import {
  Button,
  ButtonTextStyleConsumer,
} from "../../components/ButtonFramework/Button"
import { GradientText } from "../../components/GradientText"
import { LinearGradientContainer } from "../../components/LinearGradientContainer"
import { PercentNumber } from "../../components/PercentNumber"
import { Spensor, SpensorR } from "../../components/Spensor"
import { useColors } from "../../components/Themed/color"
import { useSpacing } from "../../components/Themed/spacing"
import { FormattedMessageOutsideText } from "../../utils/intlHelpers"
import { FCS } from "../../utils/reactHelpers/types"
import { HrefLink } from "../../utils/reactNavigationHelpers/HrefLink"
import { readResource, SuspenseResource } from "../../utils/SuspenseResource"
import {
  PageContainer,
  PageSectionTitleText,
  PageTitle,
  PageTitleDescriptionLinkText,
  PageTitleMainText,
} from "../components/pageLayoutPresets"
import { HorizontalScrollableContainer } from "../TradeScreen/components/HorizontalScrollableContainer"
import { TopPanelFrame } from "./components/topPanel/TopPanelFrame"
import { TopPanelTab } from "./components/types"

export const ReferralScreenContent: FC<{
  style?: StyleProp<ViewStyle>
  topTraderPanelContent: (props: { style?: StyleProp<ViewStyle> }) => ReactNode
  topMyCodePanelContent: (props: { style?: StyleProp<ViewStyle> }) => ReactNode
  myCodeTable: (props: { style?: StyleProp<ViewStyle> }) => ReactNode
  isCreatedReferralCode: SuspenseResource<boolean>
  onCreateCode: SuspenseResource<undefined | (() => void)>
  onConnectWallet?: () => void
}> = props => {
  const spacing = useSpacing()
  const { $t } = useIntl()

  const CompactBlueButtonVariant = useCompactButtonVariant(BlueButtonVariant)

  const [topPanelTab, setTopPanelTab] = useState(TopPanelTab.Traders)

  return (
    <PageContainer style={props.style}>
      {pcp => (
        <>
          <PageTitle
            style={{ marginHorizontal: pcp.horizontalMargin }}
            descriptionText={$t<ReactNode>(
              defineMessage({
                defaultMessage:
                  "Get trading fee discounts and earn referral rebates through the Uniwhale Referral Program. For more information, please read the <detailLink>referral program details ></detailLink>.",
                description: "ReferralScreen/title description",
              }),
              {
                detailLink: content => (
                  <HrefLink
                    href={
                      "https://medium.com/uniwhale/uniwhale-referral-program-41e7a44ace6f"
                    }
                  >
                    <PageTitleDescriptionLinkText>
                      {content}
                    </PageTitleDescriptionLinkText>
                  </HrefLink>
                ),
              },
            )}
          >
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <FormattedMessageOutsideText>
                {$t<ReactNode>(
                  defineMessage({
                    defaultMessage:
                      "<title>Uniwhale Referral Program</title> {rebateRatePromote}",
                    description: "ReferralScreen/title",
                  }),
                  {
                    title: content => (
                      <PageTitleMainText>{content}</PageTitleMainText>
                    ),
                    rebateRatePromote: (
                      <RebateRatePromote
                        style={{ marginLeft: spacing(4) }}
                        uptoRebateRate={0.4}
                      />
                    ),
                  },
                )}
              </FormattedMessageOutsideText>
            </View>
          </PageTitle>

          <TopPanelFrame
            style={{
              marginTop: spacing(5),
              marginHorizontal: pcp.horizontalMargin,
            }}
            currentTab={topPanelTab}
            onCurrentTabChange={setTopPanelTab}
          >
            <Spensor
              fallback={
                props.onConnectWallet != null ? (
                  <WalletNotConnected onConnect={props.onConnectWallet} />
                ) : (
                  <ActivityIndicator className={"m-auto"} />
                )
              }
            >
              {() =>
                topPanelTab === TopPanelTab.Traders
                  ? props.topTraderPanelContent({})
                  : props.topMyCodePanelContent({})
              }
            </Spensor>
          </TopPanelFrame>

          <Spensor>
            {() =>
              topPanelTab === TopPanelTab.MyCode &&
              readResource(props.isCreatedReferralCode) && (
                <>
                  <View
                    style={{
                      marginTop: spacing(8),
                      marginHorizontal: pcp.horizontalMargin,
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "space-between",
                    }}
                  >
                    <PageSectionTitleText>
                      {$t(
                        defineMessage({
                          defaultMessage: "Referral Codes",
                          description: 'ReferralScreen/"My Code" Table/title',
                        }),
                      )}
                    </PageSectionTitleText>

                    <SpensorR read={{ onCreateCode: props.onCreateCode }}>
                      {({ onCreateCode }) =>
                        onCreateCode != null && (
                          <Button
                            renderVariant={p => (
                              <CompactBlueButtonVariant
                                {...p}
                                style={[p.style, { flexDirection: "row" }]}
                              />
                            )}
                            onPress={onCreateCode}
                          >
                            {$t(
                              defineMessage({
                                defaultMessage: "{plusIcon} Create Code",
                                description:
                                  'ReferralScreen/"My Code" Table/Create Code button',
                              }),
                              {
                                plusIcon: (
                                  <ButtonTextStyleConsumer>
                                    {style => (
                                      <Svg
                                        width="16"
                                        height="16"
                                        viewBox="0 0 16 16"
                                        fill={style.color}
                                      >
                                        <Path d="M14 7H9V2H7V7H2V9H7V14H9V9H14V7Z" />
                                      </Svg>
                                    )}
                                  </ButtonTextStyleConsumer>
                                ),
                              },
                            )}
                          </Button>
                        )
                      }
                    </SpensorR>
                  </View>

                  <HorizontalScrollableContainer
                    style={{ width: "100%", marginTop: spacing(4) }}
                    contentContainerStyle={{ minWidth: "100%" }}
                    scrollable={pcp.horizontalScrollable}
                  >
                    <View
                      style={{
                        flex: 1,
                        minWidth: 640,
                        paddingHorizontal: pcp.horizontalMargin,
                      }}
                    >
                      {props.myCodeTable({
                        style: { flex: 1 },
                      })}
                    </View>
                  </HorizontalScrollableContainer>
                </>
              )
            }
          </Spensor>
        </>
      )}
    </PageContainer>
  )
}

const RebateRatePromote: FC<{
  style?: StyleProp<ViewStyle>
  uptoRebateRate: number
}> = props => {
  const spacing = useSpacing()
  const { $t } = useIntl()

  return (
    <LinearGradientContainer
      style={[
        props.style,
        {
          paddingVertical: 4,
          paddingHorizontal: 12,
          borderTopLeftRadius: 8,
          borderBottomRightRadius: 8,
          borderTopRightRadius: 2,
          borderBottomLeftRadius: 2,
          overflow: "hidden",
        },
      ]}
      colors={[
        { color: "#006AF9", offset: 0.4982 },
        { color: "#00BDF9", offset: 1 },
      ]}
    >
      <View style={{ flexDirection: "row", alignItems: "center" }}>
        <FormattedMessageOutsideText>
          {$t(
            defineMessage({
              defaultMessage: "<gradientText>Upto</gradientText> {rebateRate}",
              description: "ReferralScreen/RebateRatePromote/text",
            }),
            {
              gradientText: content => (
                <GradientText
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    zIndex: 0,
                  }}
                  textStyle={{
                    fontSize: 12,
                    fontWeight: "600",
                  }}
                  gradient={{
                    direction: 270.65,
                    colors: [
                      { color: "#FFFFFF", offset: 0.8578 },
                      { color: "#006AF9", offset: 1.1244 },
                    ],
                  }}
                >
                  {content}
                </GradientText>
              ),
              rebateRate: (
                <PercentNumber
                  style={{
                    marginLeft: spacing(1),
                    color: "#fff",
                    fontSize: 16,
                    fontWeight: "700",
                  }}
                  number={props.uptoRebateRate}
                />
              ),
            },
          )}
        </FormattedMessageOutsideText>
      </View>
    </LinearGradientContainer>
  )
}

const WalletNotConnected: FCS<{ onConnect: () => void }> = props => {
  const spacing = useSpacing()
  const colors = useColors()
  const { $t } = useIntl()
  return (
    <View
      style={[
        {
          padding: spacing(5),
          alignItems: "center",
          justifyContent: "center",
          borderRadius: 8,
          borderWidth: 1,
          borderColor: colors("gray-200"),
        },
        props.style,
      ]}
    >
      <Text
        style={{
          color: colors("gray-900"),
          fontSize: 18,
          fontWeight: "500",
        }}
      >
        {$t(
          defineMessage({
            defaultMessage: "Enter Referral Code",
            description: "ReferralScreen/Top Panel/Traders Tab",
          }),
        )}
      </Text>
      <Text
        style={{
          color: colors("gray-900"),
          marginTop: spacing(1),
          fontSize: 14,
        }}
      >
        {$t(
          defineMessage({
            defaultMessage:
              "Link a referral code and get trading fee discounts.",
            description: "ReferralScreen/Top Panel/Traders Tab",
          }),
        )}
      </Text>
      <Button
        style={{ marginTop: spacing(5), minWidth: 180 }}
        onPress={props.onConnect}
      >
        {$t(connectWallet$t)}
      </Button>
    </View>
  )
}
