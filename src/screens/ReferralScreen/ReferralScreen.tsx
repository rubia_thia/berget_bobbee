import { action } from "mobx"
import { FC, Suspense } from "react"
import { defineMessage, useIntl } from "react-intl"
import { Dialog } from "../../components/DialogProvider/Dialog"
import { Spensor } from "../../components/Spensor"
import { useTransactionNotifier } from "../../components/TransactionNotifier/WiredTransactionNotifier"
import { TopLevelNavigatorScreenProps } from "../../navigation/navigators/TopLevelNavigatorTypes"
import { useAuthStore } from "../../stores/AuthStore/useAuthStore"
import {
  ReferralStoreProvider,
  useReferralStore,
} from "../../stores/ReferralStore/useReferralStore"
import { Result } from "../../stores/utils/Result"
import { suspenseResource } from "../../utils/SuspenseResource"
import { HideScreenOnBlur } from "../components/HideScreenOnBlur"
import {
  EnterReferralCodeModalContent,
  EnterReferralCodeModalContentLoading,
} from "./components/EnterReferralCodeModalContent"
import {
  GenerateReferralCodeModalContent,
  GenerateReferralCodeModalContentLoading,
} from "./components/GenerateReferralCodeModalContent"
import { MyCodeTablePanel } from "./components/MyCodeTablePanel"
import { MyCodeContentPanelForUser } from "./components/topPanel/MyCodeContentPanelForUser"
import { MyCodePanelContentForMultiCodeUser } from "./components/topPanel/MyCodePanelContentForMultiCodeUser"
import { MyCodePanelContentInitial } from "./components/topPanel/MyCodePanelContentInitial"
import { TradersPanelContentActivated } from "./components/topPanel/TradersPanelContentActivated"
import { TradersPanelContentInitial } from "./components/topPanel/TradersPanelContentInitial"
import { ReferralScreenContent } from "./ReferralScreenContent"

const WiredEnterReferralCodeModal: FC<{ code?: string }> = () => {
  const { $t } = useIntl()

  const store = useReferralStore()
  const noti = useTransactionNotifier()
  const authStore = useAuthStore()

  return (
    <Dialog
      visible={store.switchCodeModule.get() != null}
      onClose={() => store.onCloseSwitchCode()}
    >
      <Spensor
        fallback={
          <EnterReferralCodeModalContentLoading
            onDismiss={() => store.onCloseSwitchCode()}
          />
        }
      >
        {() => (
          <EnterReferralCodeModalContent
            onConnectWallet={action(
              () => (authStore.showConnectWalletModal = true),
            )}
            onDismiss={() => store.onCloseSwitchCode()}
            errors={suspenseResource(() =>
              Result.maybeError(store.switchCodeModule.read$.formData$),
            )}
            referralCode={store.switchCodeModule.read$.inputtedCode.get() ?? ""}
            onReferralCodeChange={v =>
              store.switchCodeModule.read$.inputtedCode.set(v ?? "")
            }
            onSubmit={suspenseResource(() => {
              const data = Result.maybeValue(
                store.switchCodeModule.read$.formData$,
              )
              if (data == null) return
              return async () =>
                noti.broadcastAndShowConfirmation(
                  store.switchCodeModule.read$.submit(data),
                  $t(
                    defineMessage({
                      defaultMessage: "Activate Referral Code",
                      description:
                        "ReferralScreen/Activate Referral Code Modal/notification",
                    }),
                  ),
                )
            })}
          />
        )}
      </Spensor>
    </Dialog>
  )
}

const WiredGenerateReferralCodeModal: FC = () => {
  const { $t } = useIntl()

  const store = useReferralStore()
  const noti = useTransactionNotifier()

  return (
    <Dialog
      visible={store.createCodeModule.get() != null}
      onClose={() => store.onCloseCreateCode()}
    >
      <Spensor
        fallback={
          <GenerateReferralCodeModalContentLoading
            onDismiss={() => store.onCloseSwitchCode()}
          />
        }
      >
        {() => (
          <GenerateReferralCodeModalContent
            onDismiss={() => store.onCloseCreateCode()}
            errors={suspenseResource(() =>
              Result.maybeError(store.createCodeModule.read$.formData$),
            )}
            referralCode={store.createCodeModule.read$.inputtedCode.get() ?? ""}
            onReferralCodeChange={v =>
              store.createCodeModule.read$.inputtedCode.set(
                (v ?? "").toLowerCase(),
              )
            }
            onSubmit={async () => {
              const data = Result.maybeValue(
                store.createCodeModule.read$.formData$,
              )
              if (data == null) {
                return
              }
              return noti.broadcastAndShowConfirmation(
                store.createCodeModule.read$.submit(data),
                $t(
                  defineMessage({
                    defaultMessage: "Create Referral Code",
                    description:
                      "ReferralScreen/Create Referral Code Modal/notification",
                  }),
                ),
              )
            }}
          />
        )}
      </Spensor>
    </Dialog>
  )
}

const Content: FC = () => {
  const store = useReferralStore()
  const authStore = useAuthStore()

  return (
    <ReferralScreenContent
      topTraderPanelContent={p => {
        const m = store.traderModule$
        return m == null ? (
          <TradersPanelContentInitial
            style={p.style}
            onChangeActiveReferralCode={() => store.onStartSwitchCode()}
          />
        ) : (
          <TradersPanelContentActivated
            style={p.style}
            activeReferralCode={m.referral.code}
            discountRate={m.referral.discountRate}
            totalTradingVolumeInUSD={suspenseResource(
              () => m.totalTradingVolumeInUSD$,
            )}
            totalDiscountVolumeInUSD={suspenseResource(
              () => m.totalDiscountVolumeInUSD$,
            )}
            onChangeActiveReferralCode={() => store.onStartSwitchCode()}
          />
        )
      }}
      topMyCodePanelContent={p => {
        const m = store.myCodeModule$
        return m == null ? (
          <MyCodePanelContentInitial
            style={p.style}
            onGenerateNewCode={() => store.onStartCreateCode()}
          />
        ) : !m.isMultiCodeUser ? (
          <MyCodeContentPanelForUser
            style={p.style}
            rebateRate={suspenseResource(() => m.rebateRate$)}
            totalRebateVolumeInUSD={suspenseResource(
              () => m.totalRebateVolumeInUSD$,
            )}
            totalTradingVolumeInUSD={suspenseResource(
              () => m.totalTradingVolumeInUSD$,
            )}
            totalReferredCount={suspenseResource(() => m.totalReferredCount$)}
          />
        ) : (
          <MyCodePanelContentForMultiCodeUser
            style={p.style}
            totalTradingVolumeInUSD={suspenseResource(
              () => m.totalTradingVolumeInUSD$,
            )}
            totalReferredCount={suspenseResource(() => m.totalReferredCount$)}
            distributionDetailsLink={suspenseResource(
              () => m.distributionDetailsLink$,
            )}
          />
        )
      }}
      myCodeTable={p => {
        const m = store.myCodeModule$
        return m == null ? (
          <>{null}</>
        ) : (
          <MyCodeTablePanel
            style={p.style}
            pagination={suspenseResource(
              () => m.referralCodesPagination.paginationInfo$,
            )}
            records={suspenseResource(() => m.referralCodesPagination.records$)}
            onPaginationChange={patch =>
              m.referralCodesPagination.onPaginationChange(patch)
            }
          />
        )
      }}
      isCreatedReferralCode={suspenseResource(
        () => store.isCurrentAccountCreatedReferralCode$,
      )}
      onCreateCode={suspenseResource(() =>
        store.myCodeModule$?.canCreateNewReferralCode$
          ? () => store.onStartCreateCode()
          : undefined,
      )}
      onConnectWallet={
        authStore.connected
          ? undefined
          : () => authStore.triggerConnectWalletModal()
      }
    />
  )
}

export const ReferralScreen: FC<
  TopLevelNavigatorScreenProps<"Referral">
> = props => {
  return (
    <HideScreenOnBlur>
      <ReferralStoreProvider
        receivedReferralCode={props.route.params?.referralCode}
      >
        <Suspense>
          <Content />
          <WiredEnterReferralCodeModal />
          <WiredGenerateReferralCodeModal />
        </Suspense>
      </ReferralStoreProvider>
    </HideScreenOnBlur>
  )
}
