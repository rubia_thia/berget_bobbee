export enum EnterReferralCodeModalContentErrorType {
  WalletNotConnected = "WalletNotConnected",
  CodeEmpty = "CodeEmpty",
  CodeNotExists = "CodeNotExists",
  CodeInvalid = "CodeInvalid",
  UseSelfGeneratedCode = "UseSelfGeneratedCode",
}

export interface EnterReferralCodeModalContentError {
  type: EnterReferralCodeModalContentErrorType
}
