import { FC, ReactNode } from "react"
import {
  StyleProp,
  Text,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native"
import { useColors } from "../../../components/Themed/color"
import { useSpacing } from "../../../components/Themed/spacing"

export interface Item<T> {
  key: string
  labelText: ReactNode
  value: T
}

export interface TextOnlySegmentControlProps<T> {
  style?: StyleProp<ViewStyle>

  items: Item<T>[]

  selectedItemKey: string

  onSelectedItem: (item: Item<T>) => void
}

export function TextOnlySegmentControl<T>(
  props: TextOnlySegmentControlProps<T>,
): JSX.Element {
  return (
    <View style={[props.style, { flexDirection: "row", alignItems: "center" }]}>
      {props.items.map((i, idx) => (
        <SegmentItem
          key={i.key}
          isFirstItem={idx === 0}
          item={i}
          isSelected={props.selectedItemKey === i.key}
          onSelect={props.onSelectedItem}
        />
      ))}
    </View>
  )
}

const SegmentItem: FC<{
  item: Item<any>
  isFirstItem: boolean
  isSelected: boolean
  onSelect: (item: Item<any>) => void
}> = props => {
  const colors = useColors()
  const spacing = useSpacing()

  return (
    <View
      style={{
        flexDirection: "row",
        alignItems: "center",
      }}
    >
      {!props.isFirstItem && (
        <View
          style={{
            marginVertical: 4,
            marginHorizontal: 2,
            width: 1,
            height: 20,
            backgroundColor: colors("gray-200"),
          }}
        />
      )}
      <TouchableOpacity
        style={{
          marginHorizontal: spacing(2.5),
          marginLeft: props.isFirstItem ? 0 : undefined,
        }}
        onPress={() => {
          props.onSelect(props.item)
        }}
      >
        <Text
          style={{
            fontSize: 16,
            fontWeight: "500",
            color: props.isSelected ? colors("gray-900") : colors("gray-400"),
          }}
        >
          {props.item.labelText}
        </Text>
      </TouchableOpacity>
    </View>
  )
}
