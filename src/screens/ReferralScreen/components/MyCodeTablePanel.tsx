import * as Clipboard from "expo-clipboard"
import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { ActivityIndicator, StyleProp, View, ViewStyle } from "react-native"
import { CardBoxView } from "../../../components/CardBox/CardBox"
import {
  Pagination,
  PaginationInfo,
  PaginationPatch,
} from "../../../components/Pagination/Pagination"
import { PaginationCountText } from "../../../components/Pagination/PaginationCountText"
import { Spensor, SpensorR } from "../../../components/Spensor"
import { TextNumber } from "../../../components/TextNumber"
import { TokenCountAsCurrency } from "../../../components/TextTokenCount"
import { useSpacing } from "../../../components/Themed/spacing"
import { TokenPresets } from "../../../components/TokenCount"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { useSizeQuery } from "../../../utils/reactHelpers/useSizeQuery"
import { HrefLink } from "../../../utils/reactNavigationHelpers/HrefLink"
import { readResource, SuspenseResource } from "../../../utils/SuspenseResource"
import CopyIcon from "./assets/copyIcon.svg"
import TwitterIcon from "./assets/twitterIcon.svg"

import { copied$t, copy$t, share$t } from "../../../commonIntlMessages"
import { BlueButtonVariant } from "../../../components/Button/BlueButtonVariant"
import { PlainIconButtonVariantLayout } from "../../../components/Button/PlainIconButtonVariant"
import { useCompactButtonVariant } from "../../../components/Button/useCompactButtonVariant"
import { WhiteOutlineButtonVariant } from "../../../components/Button/WhiteOutlineButtonVariant"
import { Button } from "../../../components/ButtonFramework/Button"
import { useMessage } from "../../../components/MessageProvider/MessageProvider"
import {
  TableBodyRow,
  TableBodyText,
  TableHeaderRow,
  TableHeaderText,
} from "../../../components/StyledTable"

export interface MyCodeTableRecord {
  code: string
  link: string
  referredTraderCount: SuspenseResource<number>
  totalVolumeInUSD: SuspenseResource<BigNumber>
}

export interface MyCodeTablePanelProps {
  style?: StyleProp<ViewStyle>
  records: SuspenseResource<MyCodeTableRecord[]>
  pagination: SuspenseResource<PaginationInfo>
  onPaginationChange: (patch: PaginationPatch) => void
}

export const MyCodeTablePanel: FC<MyCodeTablePanelProps> = props => {
  const spacing = useSpacing()
  const { $t } = useIntl()

  const [paginationLayout = "row", onLayout] = useSizeQuery({
    0: "column",
    400: "row",
  })

  const message = useMessage()

  const CompactBlueButton = useCompactButtonVariant(BlueButtonVariant)
  const CompactWhiteOutlineButton = useCompactButtonVariant(
    WhiteOutlineButtonVariant,
  )

  return (
    <CardBoxView style={props.style} padding={spacing(6)}>
      <View>
        <TableHeaderRow>
          <TableHeaderText style={{ flex: 300 }}>
            {$t(
              defineMessage({
                defaultMessage: "Referral Code",
                description: 'ReferralScreen/"My Code" table/table header',
              }),
            )}
          </TableHeaderText>
          <TableHeaderText style={{ flex: 370 }}>
            {$t(
              defineMessage({
                defaultMessage: "Trader Referred",
                description: 'ReferralScreen/"My Code" table/table header',
              }),
            )}
          </TableHeaderText>
          <TableHeaderText style={{ flex: 370 }}>
            {$t(
              defineMessage({
                defaultMessage: "Total Volume",
                description: 'ReferralScreen/"My Code" table/table header',
              }),
            )}
          </TableHeaderText>
        </TableHeaderRow>
        <Spensor
          fallback={
            <View className={"min-h-[200px]"}>
              <ActivityIndicator className={"m-auto"} />
            </View>
          }
        >
          {() => (
            <>
              {readResource(props.records).map(r => (
                <TableBodyRow key={r.code}>
                  <View
                    className="flex-row items-center space-x-2"
                    style={{ flex: 300 }}
                  >
                    <TableBodyText className="mr-auto">{r.code}</TableBodyText>
                    <Button
                      Variant={CompactBlueButton}
                      onPress={async () => {
                        await Clipboard.setStringAsync(r.link)
                        message.show({ message: $t(copied$t) })
                      }}
                    >
                      <PlainIconButtonVariantLayout iconLeft={CopyIcon}>
                        <Button.Text>{$t(copy$t)}</Button.Text>
                      </PlainIconButtonVariantLayout>
                    </Button>
                    <HrefLink href={composeTweetLink(r.link)}>
                      {linkProps => (
                        <Button
                          {...linkProps}
                          Variant={CompactWhiteOutlineButton}
                          className="mr-10"
                        >
                          <PlainIconButtonVariantLayout iconLeft={TwitterIcon}>
                            <Button.Text>{$t(share$t)}</Button.Text>
                          </PlainIconButtonVariantLayout>
                        </Button>
                      )}
                    </HrefLink>
                  </View>
                  <TableBodyText style={{ flex: 370 }}>
                    <TextNumber number={r.referredTraderCount} />
                  </TableBodyText>
                  <TableBodyText style={{ flex: 370 }}>
                    <TokenCountAsCurrency
                      token={TokenPresets.USD}
                      count={r.totalVolumeInUSD}
                    />
                  </TableBodyText>
                </TableBodyRow>
              ))}
            </>
          )}
        </Spensor>
      </View>
      <SpensorR read={{ p: props.pagination }}>
        {({ p }) => (
          <View
            className={"flex-wrap items-center justify-between mt-2.5"}
            style={paginationLayout === "row" && { flexDirection: "row" }}
            onLayout={onLayout}
          >
            {paginationLayout === "row" && (
              <PaginationCountText className={"mr-2.5"} pagination={p} />
            )}
            <Pagination {...p} onChange={props.onPaginationChange} />
          </View>
        )}
      </SpensorR>
    </CardBoxView>
  )
}

const composeTweetLink = (referralLink: string): string => {
  const tweet = `Trade on @UniwhaleEx with 200x leverage on likes of $BTC, $ETH, $BNB, and more🥳

For fee discount 👉 ${referralLink}`
  return `https://twitter.com/intent/tweet?text=${encodeURIComponent(
    tweet,
  )}&url=${encodeURIComponent(referralLink)}`
}
