import { FC, ReactNode } from "react"
import { defineMessage, useIntl } from "react-intl"
import {
  ActivityIndicator,
  StyleProp,
  Text,
  View,
  ViewStyle,
} from "react-native"
import { connectWallet$t, submit$t } from "../../../commonIntlMessages"
import { BlueButtonVariant } from "../../../components/Button/BlueButtonVariant"
import { RedButtonVariant } from "../../../components/Button/RedButtonVariant"
import { Button } from "../../../components/ButtonFramework/Button"
import { SmartLoadableButton } from "../../../components/ButtonFramework/LoadableButton"
import {
  CardBoxModalContent,
  TitleBar,
} from "../../../components/CardBoxModalContent/CardBoxModalContent"
import {
  NoteParagraph,
  NoteParagraphText,
} from "../../../components/NoteParagraph/NoteParagraph"
import { SpensorR } from "../../../components/Spensor"
import { TextInput } from "../../../components/TextInput"
import { useColors } from "../../../components/Themed/color"
import { useSpacing } from "../../../components/Themed/spacing"
import {
  safeReadResource,
  SuspenseResource,
} from "../../../utils/SuspenseResource"
import GiftIcon from "./assets/gift.svg"
import PiggyIcon from "./assets/piggy.svg"
import {
  EnterReferralCodeModalContentError,
  EnterReferralCodeModalContentErrorType,
} from "./EnterReferralCodeModalContent.types"

export const EnterReferralCodeModalContentLoading: FC<{
  style?: StyleProp<ViewStyle>
  onDismiss: () => void
}> = props => {
  const spacing = useSpacing()

  return (
    <CardBoxModalContent
      style={props.style}
      padding={spacing(4)}
      onClose={props.onDismiss}
    >
      <ActivityIndicator
        style={{
          marginTop: "auto",
          marginBottom: "auto",
          marginLeft: "auto",
          marginRight: "auto",
        }}
      />
    </CardBoxModalContent>
  )
}

export interface EnterReferralCodeModalContentProps {
  style?: StyleProp<ViewStyle>
  errors?: SuspenseResource<undefined | EnterReferralCodeModalContentError[]>
  referralCode: string
  onReferralCodeChange: (newReferralCode: null | string) => void
  onConnectWallet: () => void
  onSubmit: SuspenseResource<undefined | (() => void | Promise<void>)>
  onDismiss: () => void
}

export const EnterReferralCodeModalContent: FC<
  EnterReferralCodeModalContentProps
> = props => {
  const spacing = useSpacing()
  const colors = useColors()
  const { $t } = useIntl()

  const errors = safeReadResource(props.errors)
  const hasError =
    errors != null &&
    errors.length > 0 &&
    !(
      errors.length === 1 &&
      findError(errors, EnterReferralCodeModalContentErrorType.CodeEmpty) !=
        null
    )
  const needToConnectWallet =
    errors != null &&
    errors.length > 0 &&
    errors.length === 1 &&
    findError(
      errors,
      EnterReferralCodeModalContentErrorType.WalletNotConnected,
    ) != null

  const hasDisabled = errors != null && !hasError

  return (
    <CardBoxModalContent
      style={props.style}
      padding={spacing(4)}
      onClose={props.onDismiss}
    >
      <TitleBar style={{ marginBottom: spacing(2) }}>
        {$t(
          defineMessage({
            defaultMessage: "Enter Referral Code",
            description: "ReferralScreen/Enter Referral Code Modal/title",
          }),
        )}
      </TitleBar>

      <View>
        <View className="flex-row p-5 justify-between space-x-2 items-center">
          <GiftIcon />
          <Text className="flex-1 text-[14px] text-gray-900 text-center">
            {$t<ReactNode>(
              defineMessage({
                defaultMessage:
                  "Bind the referral code now,{br} enjoy <strong>15% OFF</strong> the price now!",
                description: "ReferralScreen/Enter Referral Code Modal/label",
              }),
              {
                br: <Text>{"\n"}</Text>,
                strong: contents => (
                  <Text className="text-[18px] font-extrabold text-green-600">
                    {contents}
                  </Text>
                ),
              },
            )}
          </Text>
          <PiggyIcon />
        </View>

        <View className="p-5 rounded bg-green-100 mb-2.5">
          <TextInput
            error={hasError && !needToConnectWallet}
            placeholder={$t(
              defineMessage({
                defaultMessage: "Enter Referral Code",
                description:
                  "ReferralScreen/Enter Referral Code Modal/placeholder",
              }),
            )}
            value={props.referralCode}
            onValueChange={props.onReferralCodeChange}
            placeholderTextColor={colors("gray-400")}
          />
        </View>

        <SpensorR
          read={{ onSubmit: props.onSubmit }}
          fallback={
            <SmartLoadableButton loading={true}>
              {$t(submit$t)}
            </SmartLoadableButton>
          }
        >
          {({ onSubmit }) => {
            if (needToConnectWallet) {
              return (
                <Button onPress={props.onConnectWallet}>
                  {$t(connectWallet$t)}
                </Button>
              )
            }
            return (
              <SmartLoadableButton
                Variant={hasError ? RedButtonVariant : BlueButtonVariant}
                disabled={hasError || hasDisabled}
                onPress={onSubmit}
              >
                {$t(
                  findError(
                    errors,
                    EnterReferralCodeModalContentErrorType.CodeInvalid,
                  )
                    ? defineMessage({
                        defaultMessage: "Invalid referral code",
                        description:
                          "ReferralScreen/Enter Referral Code Modal/error message",
                      })
                    : findError(
                        errors,
                        EnterReferralCodeModalContentErrorType.CodeNotExists,
                      )
                    ? defineMessage({
                        defaultMessage: "Referral code does not exist",
                        description:
                          "ReferralScreen/Enter Referral Code Modal/error message",
                      })
                    : // : findError(
                      //     errors,
                      //     EnterReferralCodeModalContentErrorType.UseSelfGeneratedCode,
                      //   )
                      // ? defineMessage({
                      //     defaultMessage: "Not support use self",
                      //     description:
                      //       "ReferralScreen/Enter Referral Code Modal/error message",
                      //   })
                      submit$t,
                )}
              </SmartLoadableButton>
            )
          }}
        </SpensorR>

        <NoteParagraph className={"mt-4"}>
          <NoteParagraphText>
            {$t(
              defineMessage({
                defaultMessage:
                  "Referral discounts and rebates will not apply to those users with UGP staked, in case where the trading fee discount on UGP is higher than the referral discount.",
                description:
                  "ReferralScreen/Enter Referral Code Modal/tip text",
              }),
            )}
          </NoteParagraphText>
        </NoteParagraph>
      </View>
    </CardBoxModalContent>
  )
}

function findError(
  errors: undefined | EnterReferralCodeModalContentError[],
  error: EnterReferralCodeModalContentErrorType,
):
  | undefined
  | Extract<EnterReferralCodeModalContentError, { type: typeof error }> {
  if (errors == null || errors.length <= 0) return undefined
  return errors.find(e => e.type === error)
}
