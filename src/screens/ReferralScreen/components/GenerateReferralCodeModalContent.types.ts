export enum GenerateReferralCodeModalContentErrorType {
  CodeLengthInvalid = "CodeLengthInvalid",
  CodeInvalid = "CodeInvalid",
  CodeAlreadyExists = "CodeAlreadyExists",
}

export type GenerateReferralCodeModalContentError =
  | {
      type: Exclude<
        GenerateReferralCodeModalContentErrorType,
        GenerateReferralCodeModalContentErrorType.CodeInvalid
      >
    }
  | {
      type: GenerateReferralCodeModalContentErrorType.CodeInvalid
      validContent: string
    }
