import { FC, ReactNode } from "react"
import { defineMessage, useIntl } from "react-intl"
import {
  ActivityIndicator,
  StyleProp,
  Text,
  View,
  ViewStyle,
} from "react-native"
import { BlueButtonVariant } from "../../../components/Button/BlueButtonVariant"
import { RedButtonVariant } from "../../../components/Button/RedButtonVariant"
import { SmartLoadableButton } from "../../../components/ButtonFramework/LoadableButton"
import {
  CardBoxModalContent,
  TitleBar,
} from "../../../components/CardBoxModalContent/CardBoxModalContent"
import { TextInput } from "../../../components/TextInput"
import { useColors } from "../../../components/Themed/color"
import { useSpacing } from "../../../components/Themed/spacing"
import {
  safeReadResource,
  SuspenseResource,
} from "../../../utils/SuspenseResource"
import InviteIcon from "./assets/invite.svg"
import Step2Icon from "./assets/step2.svg"
import {
  GenerateReferralCodeModalContentError,
  GenerateReferralCodeModalContentErrorType,
} from "./GenerateReferralCodeModalContent.types"

export const GenerateReferralCodeModalContentLoading: FC<{
  style?: StyleProp<ViewStyle>
  onDismiss: () => void
}> = props => {
  const spacing = useSpacing()

  return (
    <CardBoxModalContent
      style={props.style}
      padding={spacing(4)}
      onClose={props.onDismiss}
    >
      <ActivityIndicator
        style={{
          marginTop: "auto",
          marginBottom: "auto",
          marginLeft: "auto",
          marginRight: "auto",
        }}
      />
    </CardBoxModalContent>
  )
}

export interface GenerateReferralCodeModalContentProps {
  style?: StyleProp<ViewStyle>
  errors?: SuspenseResource<undefined | GenerateReferralCodeModalContentError[]>
  referralCode: string
  onReferralCodeChange: (newReferralCode: null | string) => void
  onSubmit: () => Promise<void>
  onDismiss: () => void
}

export const GenerateReferralCodeModalContent: FC<
  GenerateReferralCodeModalContentProps
> = props => {
  const spacing = useSpacing()
  const colors = useColors()
  const { $t } = useIntl()

  const errors = safeReadResource(props.errors)
  const hasError =
    errors != null &&
    errors.length > 0 &&
    !(
      errors.length === 1 &&
      findError(
        errors,
        GenerateReferralCodeModalContentErrorType.CodeLengthInvalid,
      ) != null
    )
  const hasDisabled = errors != null && !hasError

  return (
    <CardBoxModalContent
      style={props.style}
      padding={spacing(4)}
      onClose={props.onDismiss}
    >
      <TitleBar style={{ marginBottom: spacing(2) }}>
        {$t(
          defineMessage({
            defaultMessage: "Generate Referral Code",
            description: "ReferralScreen/Generate Referral Code Modal/title",
          }),
        )}
      </TitleBar>

      <View>
        <View className="flex-row p-5 justify-between space-x-2 items-center">
          <InviteIcon />
          <Text className="text-[14px] text-gray-900 text-center flex-1">
            {$t<ReactNode>(
              defineMessage({
                defaultMessage:
                  "Looks like you don’t have a referral code to share. Generate one now and start earning referral rebates!",
                description:
                  "ReferralScreen/Generate Referral Code Modal/label",
              }),
            )}
          </Text>
          <Step2Icon />
        </View>
        <View className="p-5 rounded bg-green-100 mb-5">
          <TextInput
            placeholderTextColor={colors("gray-400")}
            error={hasError}
            placeholder={$t(
              defineMessage({
                defaultMessage: "Enter referral code",
                description:
                  "ReferralScreen/Generate Referral Code Modal/placeholder",
              }),
            )}
            value={props.referralCode}
            onValueChange={props.onReferralCodeChange}
            autoCapitalize="none"
          />
        </View>

        <SmartLoadableButton
          Variant={hasError ? RedButtonVariant : BlueButtonVariant}
          disabled={hasError || hasDisabled}
          onPress={hasError ? undefined : props.onSubmit}
        >
          {findError(
            errors,
            GenerateReferralCodeModalContentErrorType.CodeInvalid,
          )
            ? $t(
                defineMessage({
                  defaultMessage:
                    "Invalid referral code, code only support {validContent}",
                  description:
                    "ReferralScreen/Generate Referral Code Modal/button text",
                }),
                {
                  validContent: findError(
                    errors,
                    GenerateReferralCodeModalContentErrorType.CodeInvalid,
                  )!.validContent,
                },
              )
            : findError(
                errors,
                GenerateReferralCodeModalContentErrorType.CodeAlreadyExists,
              )
            ? $t(
                defineMessage({
                  defaultMessage: "Referral code already taken",
                  description:
                    "ReferralScreen/Generate Referral Code Modal/button text",
                }),
              )
            : $t(
                defineMessage({
                  defaultMessage: "Submit",
                  description:
                    "ReferralScreen/Generate Referral Code Modal/button text",
                }),
              )}
        </SmartLoadableButton>
      </View>
    </CardBoxModalContent>
  )
}

function findError<T extends GenerateReferralCodeModalContentErrorType>(
  errors: undefined | GenerateReferralCodeModalContentError[],
  error: T,
): undefined | Extract<GenerateReferralCodeModalContentError, { type: T }> {
  if (errors == null || errors.length <= 0) return undefined
  return errors.find(e => e.type === error) as any
}
