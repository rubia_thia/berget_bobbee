import { defineMessage } from "react-intl"

export enum TopPanelTab {
  Traders = "traders",
  MyCode = "mycode",
}

export namespace topPanelTab$t {
  export const traders = defineMessage({
    defaultMessage: "Traders",
    description: "TopPanel/tabs/Traders",
  })

  export const myCode = defineMessage({
    defaultMessage: "My Code",
    description: "TopPanel/tabs/MyCode",
  })
}
