import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { MessageProviderDecorator } from "../../../components/MessageProvider/MessageProvider.storiesHelpers"
import { MyCodeTablePanel } from "./MyCodeTablePanel"
import { myCodeTablePanelProps } from "./MyCodeTablePanel.mockData"

export default {
  title: "Page/ReferralScreen/MyCodeTablePanel",
  component: MyCodeTablePanel,
  decorators: [BackgroundColor(), MessageProviderDecorator()],
} as ComponentMeta<typeof MyCodeTablePanel>

const template = withTemplate(MyCodeTablePanel, {
  style: {
    margin: 10,
  },
  ...myCodeTablePanelProps,
})

export const Normal = template()
