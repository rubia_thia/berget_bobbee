import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { GenerateReferralCodeModalContent } from "./GenerateReferralCodeModalContent"
import { GenerateReferralCodeModalContentErrorType } from "./GenerateReferralCodeModalContent.types"

export default {
  title: "Page/ReferralScreen/GenerateReferralCodeModalContent",
  component: GenerateReferralCodeModalContent,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof GenerateReferralCodeModalContent>

const template = withTemplate(GenerateReferralCodeModalContent, {
  style: {
    margin: 10,
  },
  referralCode: "ABCD",
})

export const Normal = template()

export const ErrorCodeInvalid = template(p => {
  p.errors = [
    {
      type: GenerateReferralCodeModalContentErrorType.CodeInvalid,
      validContent: "a-z",
    },
  ]
})

export const ErrorCodeAlreadyExists = template(p => {
  p.errors = [
    { type: GenerateReferralCodeModalContentErrorType.CodeAlreadyExists },
  ]
})
