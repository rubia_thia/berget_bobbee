import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../../.storybook/decorators"
import { withTemplate } from "../../../../.storybook/utils"
import { noop } from "../../../utils/fnHelpers"
import { EnterReferralCodeModalContent } from "./EnterReferralCodeModalContent"
import { EnterReferralCodeModalContentErrorType } from "./EnterReferralCodeModalContent.types"

export default {
  title: "Page/ReferralScreen/EnterReferralCodeModalContent",
  component: EnterReferralCodeModalContent,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof EnterReferralCodeModalContent>

const template = withTemplate(EnterReferralCodeModalContent, {
  style: {
    margin: 10,
  },
  referralCode: "ABCD",
  onSubmit: noop,
})

export const Normal = template()

export const ErrorCodeInvalid = template(p => {
  p.errors = [{ type: EnterReferralCodeModalContentErrorType.CodeInvalid }]
})

export const ErrorCodeNotExists = template(p => {
  p.errors = [{ type: EnterReferralCodeModalContentErrorType.CodeNotExists }]
})

export const ErrorUseSelfGeneratedCode = template(p => {
  p.errors = [
    { type: EnterReferralCodeModalContentErrorType.UseSelfGeneratedCode },
  ]
})
