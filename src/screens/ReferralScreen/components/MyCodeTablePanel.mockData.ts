import { OptionalizeEventListeners } from "../../../../.storybook/utils"
import { BigNumber } from "../../../utils/numberHelpers/BigNumber"
import { MyCodeTablePanelProps } from "./MyCodeTablePanel"

export const myCodeTablePanelProps: OptionalizeEventListeners<MyCodeTablePanelProps> =
  {
    records: [
      {
        link: "https://www.google.com",
        code: "abcdef1",
        totalVolumeInUSD: BigNumber.from(0.1),
        referredTraderCount: 30,
      },
      {
        link: "https://www.google.com",
        code: "abcdef2",
        totalVolumeInUSD: BigNumber.from(0.1),
        referredTraderCount: 30,
      },
      {
        link: "https://www.google.com",
        code: "abcdef3",
        totalVolumeInUSD: BigNumber.from(0.1),
        referredTraderCount: 30,
      },
    ],
    pagination: {
      current: 2,
      pageSize: 20,
      total: 345,
    },
  }
