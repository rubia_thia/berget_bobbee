import { ComponentMeta } from "@storybook/react"
import {
  BackgroundColor,
  CardContainer,
} from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { TradersPanelContentActivated } from "./TradersPanelContentActivated"
import { tradersPanelContentActivatedProps } from "./TradersPanelContentActivated.mockData"

export default {
  title: "Page/ReferralScreen/TopPanel/TradersPanelContentActivated",
  component: TradersPanelContentActivated,
  decorators: [CardContainer(), BackgroundColor()],
} as ComponentMeta<typeof TradersPanelContentActivated>

const template = withTemplate(TradersPanelContentActivated, {
  style: {
    margin: 10,
  },
  ...tradersPanelContentActivatedProps,
})

export const Normal = template()
