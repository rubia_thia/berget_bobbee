import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, Text, View, ViewStyle } from "react-native"
import {
  NoteParagraph,
  NoteParagraphText,
} from "../../../../components/NoteParagraph/NoteParagraph"
import { SpensorR } from "../../../../components/Spensor"
import { TextNumber } from "../../../../components/TextNumber"
import { TokenCountAsCurrency } from "../../../../components/TextTokenCount"
import { useColors } from "../../../../components/Themed/color"
import { TokenPresets } from "../../../../components/TokenCount"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { HrefLink } from "../../../../utils/reactNavigationHelpers/HrefLink"
import { SuspenseResource } from "../../../../utils/SuspenseResource"
import { BodyText, InfoBlock, TitleText } from "./_/InfoBlock"
import { InfoBlockContainer } from "./_/InfoBlockContainer"

export interface MyCodePanelContentForMultiCodeUserProps {
  style?: StyleProp<ViewStyle>
  totalReferredCount: SuspenseResource<number>
  totalTradingVolumeInUSD: SuspenseResource<BigNumber>
  distributionDetailsLink: SuspenseResource<string>
}

export const MyCodePanelContentForMultiCodeUser: FC<
  MyCodePanelContentForMultiCodeUserProps
> = props => {
  const colors = useColors()
  const { $t } = useIntl()

  return (
    <View style={props.style}>
      <InfoBlockContainer>
        <InfoBlock
          style={{ flex: 1 }}
          title={$t(
            defineMessage({
              defaultMessage: "Total Referred",
              description: "ReferralScreen/Top Panel/My Code Tab",
            }),
          )}
        >
          <BodyText>
            <TextNumber number={props.totalReferredCount} />
          </BodyText>
        </InfoBlock>

        <InfoBlock
          style={{ flex: 1 }}
          title={
            <TitleText>
              {$t(
                defineMessage({
                  defaultMessage: "Total Trading Volume",
                  description: "ReferralScreen/Top Panel/My Code Tab",
                }),
              )}
            </TitleText>
          }
        >
          <BodyText>
            <TokenCountAsCurrency
              token={TokenPresets.USD}
              count={props.totalTradingVolumeInUSD}
            />
          </BodyText>
        </InfoBlock>

        <InfoBlock
          style={{ flex: 1 }}
          title={$t(
            defineMessage({
              defaultMessage: "Rebates",
              description: "ReferralScreen/Top Panel/My Code Tab",
            }),
          )}
        >
          <SpensorR read={{ link: props.distributionDetailsLink }}>
            {({ link }) => (
              <HrefLink
                style={{ flex: 1, justifyContent: "center" }}
                href={link}
              >
                <Text style={{ fontSize: 14, color: colors("blue-600") }}>
                  {$t(
                    defineMessage({
                      defaultMessage: "Distribution Details >",
                      description: "ReferralScreen/Top Panel/My Code Tab",
                    }),
                  )}
                </Text>
              </HrefLink>
            )}
          </SpensorR>
        </InfoBlock>
      </InfoBlockContainer>

      <NoteParagraph className={"mt-4 mx-auto"}>
        <NoteParagraphText>
          {$t(
            defineMessage({
              defaultMessage:
                "Referral discounts and rebates will not apply to those users with UGP staked, in case where the trading fee discount on UGP is higher than the referral discount.",
              description: "ReferralScreen/Top Panel/tip text",
            }),
          )}
        </NoteParagraphText>
      </NoteParagraph>
    </View>
  )
}
