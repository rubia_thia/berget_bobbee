import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, Text, View, ViewStyle } from "react-native"
import { Button } from "../../../../components/ButtonFramework/Button"
import { useColors } from "../../../../components/Themed/color"
import { useSpacing } from "../../../../components/Themed/spacing"

export interface MyCodePanelContentInitialProps {
  style?: StyleProp<ViewStyle>
  onGenerateNewCode: () => void
}

export const MyCodePanelContentInitial: FC<
  MyCodePanelContentInitialProps
> = props => {
  const spacing = useSpacing()
  const colors = useColors()
  const { $t } = useIntl()

  return (
    <View
      style={[
        {
          padding: spacing(5),
          alignItems: "center",
          justifyContent: "center",
          borderRadius: 8,
          borderWidth: 1,
          borderColor: colors("gray-200"),
        },
        props.style,
      ]}
    >
      <Text
        style={{
          color: colors("gray-900"),
          fontSize: 18,
          fontWeight: "500",
        }}
      >
        {$t(
          defineMessage({
            defaultMessage: "Generate Referral Code",
            description: "ReferralScreen/Top Panel/My Code Tab",
          }),
        )}
      </Text>
      <Text
        style={{
          color: colors("gray-900"),
          marginTop: spacing(1),
          textAlign: "center",
          maxWidth: 330,
          fontSize: 14,
        }}
      >
        {$t(
          defineMessage({
            defaultMessage:
              "Looks like you don’t have a referral code to share. Create one now and start earning referral rebates!",
            description: "ReferralScreen/Top Panel/My Code Tab",
          }),
        )}
      </Text>
      <Button
        style={{ marginTop: spacing(5), minWidth: 180 }}
        onPress={props.onGenerateNewCode}
      >
        {$t(
          defineMessage({
            defaultMessage: "Generate",
            description: "ReferralScreen/Top Panel/My Code Tab",
          }),
        )}
      </Button>
    </View>
  )
}
