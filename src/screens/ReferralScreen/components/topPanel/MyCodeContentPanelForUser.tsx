import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, Text, View, ViewStyle } from "react-native"
import {
  NoteParagraph,
  NoteParagraphText,
} from "../../../../components/NoteParagraph/NoteParagraph"
import { PercentNumber } from "../../../../components/PercentNumber"
import { TextNumber } from "../../../../components/TextNumber"
import { TokenCountAsCurrency } from "../../../../components/TextTokenCount"
import { useColors } from "../../../../components/Themed/color"
import { TokenPresets } from "../../../../components/TokenCount"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { SuspenseResource } from "../../../../utils/SuspenseResource"
import { BodyText, InfoBlock, TitleText } from "./_/InfoBlock"
import { InfoBlockContainer } from "./_/InfoBlockContainer"

export interface MyCodeContentPanelForUserProps {
  style?: StyleProp<ViewStyle>
  rebateRate: SuspenseResource<BigNumber>
  totalReferredCount: SuspenseResource<number>
  totalTradingVolumeInUSD: SuspenseResource<BigNumber>
  totalRebateVolumeInUSD: SuspenseResource<BigNumber>
}

export const MyCodeContentPanelForUser: FC<
  MyCodeContentPanelForUserProps
> = props => {
  const colors = useColors()
  const { $t } = useIntl()

  return (
    <View style={props.style}>
      <InfoBlockContainer>
        <InfoBlock
          style={{ flex: 1 }}
          title={$t(
            defineMessage({
              defaultMessage: "Total Referred",
              description: "ReferralScreen/Top Panel/My Code Tab",
            }),
          )}
        >
          <BodyText>
            <TextNumber number={props.totalReferredCount} />
          </BodyText>
        </InfoBlock>

        <InfoBlock
          style={{ flex: 1 }}
          title={
            <TitleText>
              {$t(
                defineMessage({
                  defaultMessage: "Total Trading Volume",
                  description: "ReferralScreen/Top Panel/My Code Tab",
                }),
              )}
            </TitleText>
          }
        >
          <BodyText>
            <TokenCountAsCurrency
              token={TokenPresets.USD}
              count={props.totalTradingVolumeInUSD}
            />
          </BodyText>
        </InfoBlock>

        <InfoBlock
          style={{ flex: 1 }}
          title={
            <TitleText>
              {$t(
                defineMessage({
                  defaultMessage: "Total Rebates",
                  description: "ReferralScreen/Top Panel/My Code Tab",
                }),
                {
                  highlightedText: content => (
                    <Text style={{ color: colors("green-600") }}>
                      {content}
                    </Text>
                  ),
                  rebateRate: <PercentNumber number={props.rebateRate} />,
                },
              )}
            </TitleText>
          }
        >
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <BodyText>
              <TokenCountAsCurrency
                token={TokenPresets.USD}
                count={props.totalRebateVolumeInUSD}
              />
            </BodyText>
          </View>
        </InfoBlock>
      </InfoBlockContainer>

      <NoteParagraph className={"mt-4 mx-auto"}>
        <NoteParagraphText>
          {$t(
            defineMessage({
              defaultMessage:
                "Referral discounts and rebates will not apply to those users with UGP staked, in case where the trading fee discount on UGP is higher than the referral discount.",
              description: "ReferralScreen/Top Panel/tip text",
            }),
          )}
        </NoteParagraphText>
      </NoteParagraph>
    </View>
  )
}
