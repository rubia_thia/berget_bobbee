import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, Text, View, ViewStyle } from "react-native"
import { useCompactButtonVariant } from "../../../../components/Button/useCompactButtonVariant"
import { WhiteOutlineButtonVariant } from "../../../../components/Button/WhiteOutlineButtonVariant"
import { Button } from "../../../../components/ButtonFramework/Button"
import {
  NoteParagraph,
  NoteParagraphText,
} from "../../../../components/NoteParagraph/NoteParagraph"
import { PercentNumber } from "../../../../components/PercentNumber"
import { Spensor } from "../../../../components/Spensor"
import { TokenCountAsCurrency } from "../../../../components/TextTokenCount"
import { useColors } from "../../../../components/Themed/color"
import { useSpacing } from "../../../../components/Themed/spacing"
import { TokenPresets } from "../../../../components/TokenCount"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import {
  readResource,
  SuspenseResource,
} from "../../../../utils/SuspenseResource"
import { BodyText, InfoBlock, TitleText } from "./_/InfoBlock"
import { InfoBlockContainer } from "./_/InfoBlockContainer"

export interface TradersPanelContentActivatedProps {
  style?: StyleProp<ViewStyle>
  discountRate: SuspenseResource<BigNumber>
  activeReferralCode: SuspenseResource<string>
  totalTradingVolumeInUSD: SuspenseResource<BigNumber>
  totalDiscountVolumeInUSD: SuspenseResource<BigNumber>
  onChangeActiveReferralCode: () => void
}

export const TradersPanelContentActivated: FC<
  TradersPanelContentActivatedProps
> = props => {
  const spacing = useSpacing()
  const colors = useColors()
  const { $t } = useIntl()

  const CompactWhiteOutlineButtonVariant = useCompactButtonVariant(
    WhiteOutlineButtonVariant,
  )

  return (
    <View style={props.style}>
      <InfoBlockContainer>
        <InfoBlock
          style={{ flex: 1 }}
          title={$t(
            defineMessage({
              defaultMessage: "Total Trading Volume",
              description: "ReferralScreen/Top Panel/Traders Tab",
            }),
          )}
        >
          <BodyText>
            <TokenCountAsCurrency
              token={TokenPresets.USD}
              count={props.totalTradingVolumeInUSD}
            />
          </BodyText>
        </InfoBlock>

        <InfoBlock
          style={{ flex: 1 }}
          title={
            <TitleText>
              {$t(
                defineMessage({
                  defaultMessage: "Total Discounts",
                  description: "ReferralScreen/Top Panel/Traders Tab",
                }),
                {
                  highlightedText: content => (
                    <Text style={{ color: colors("green-600") }}>
                      {content}
                    </Text>
                  ),
                  discountRate: <PercentNumber number={props.discountRate} />,
                },
              )}
            </TitleText>
          }
        >
          <BodyText>
            <TokenCountAsCurrency
              token={TokenPresets.USD}
              count={props.totalDiscountVolumeInUSD}
            />
          </BodyText>
        </InfoBlock>

        <InfoBlock
          style={{ flex: 1 }}
          title={$t(
            defineMessage({
              defaultMessage: "Active Referral Code",
              description: "ReferralScreen/Top Panel/Traders Tab",
            }),
          )}
        >
          <BodyText>
            <Spensor fallback={"-"}>
              {() => <>{readResource(props.activeReferralCode)}</>}
            </Spensor>
          </BodyText>
          <Button
            style={{ marginTop: spacing(2.5), alignSelf: "flex-start" }}
            Variant={CompactWhiteOutlineButtonVariant}
            onPress={props.onChangeActiveReferralCode}
          >
            {$t(
              defineMessage({
                defaultMessage: "Edit",
                description: "ReferralScreen/Top Panel/Traders Tab",
              }),
            )}
          </Button>
        </InfoBlock>
      </InfoBlockContainer>

      <NoteParagraph className={"mt-4 mx-auto"}>
        <NoteParagraphText>
          {$t(
            defineMessage({
              defaultMessage:
                "Referral discounts and rebates will not apply to those users with UGP staked, in case where the trading fee discount on UGP is higher than the referral discount.",
              description: "ReferralScreen/Top Panel/tip text",
            }),
          )}
        </NoteParagraphText>
      </NoteParagraph>
    </View>
  )
}
