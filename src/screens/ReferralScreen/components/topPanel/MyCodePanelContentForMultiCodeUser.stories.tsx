import { ComponentMeta } from "@storybook/react"
import {
  BackgroundColor,
  CardContainer,
} from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { MyCodePanelContentForMultiCodeUser } from "./MyCodePanelContentForMultiCodeUser"
import { myCodePanelContentForMultiCodeUserProps } from "./MyCodePanelContentForMultiCodeUser.mockData"

export default {
  title: "Page/ReferralScreen/TopPanel/MyCodePanelContentForMultiCodeUser",
  component: MyCodePanelContentForMultiCodeUser,
  decorators: [CardContainer(), BackgroundColor()],
} as ComponentMeta<typeof MyCodePanelContentForMultiCodeUser>

const template = withTemplate(MyCodePanelContentForMultiCodeUser, {
  style: {
    margin: 10,
  },
  ...myCodePanelContentForMultiCodeUserProps,
})

export const Normal = template()
