import { OptionalizeEventListeners } from "../../../../../.storybook/utils"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { MyCodePanelContentForMultiCodeUserProps } from "./MyCodePanelContentForMultiCodeUser"

export const myCodePanelContentForMultiCodeUserProps: OptionalizeEventListeners<MyCodePanelContentForMultiCodeUserProps> =
  {
    totalReferredCount: 20,
    totalTradingVolumeInUSD: BigNumber.from(100000),
    distributionDetailsLink: "https://www.google.com",
  }
