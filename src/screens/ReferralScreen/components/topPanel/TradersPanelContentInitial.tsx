import { FC } from "react"
import { defineMessage, useIntl } from "react-intl"
import { StyleProp, Text, View, ViewStyle } from "react-native"
import { Button } from "../../../../components/ButtonFramework/Button"
import { useColors } from "../../../../components/Themed/color"
import { useSpacing } from "../../../../components/Themed/spacing"

export interface TradersPanelContentInitialProps {
  style?: StyleProp<ViewStyle>
  onChangeActiveReferralCode: () => void
}

export const TradersPanelContentInitial: FC<
  TradersPanelContentInitialProps
> = props => {
  const spacing = useSpacing()
  const colors = useColors()
  const { $t } = useIntl()

  return (
    <View
      style={[
        {
          padding: spacing(5),
          alignItems: "center",
          justifyContent: "center",
          borderRadius: 8,
          borderWidth: 1,
          borderColor: colors("gray-200"),
        },
        props.style,
      ]}
    >
      <Text
        style={{
          color: colors("gray-900"),
          fontSize: 18,
          fontWeight: "500",
        }}
      >
        {$t(
          defineMessage({
            defaultMessage: "Enter Referral Code",
            description: "ReferralScreen/Top Panel/Traders Tab",
          }),
        )}
      </Text>
      <Text
        style={{
          color: colors("gray-900"),
          marginTop: spacing(1),
          fontSize: 14,
        }}
      >
        {$t(
          defineMessage({
            defaultMessage:
              "Link a referral code and get trading fee discounts.",
            description: "ReferralScreen/Top Panel/Traders Tab",
          }),
        )}
      </Text>
      <Button
        style={{ marginTop: spacing(5), minWidth: 180 }}
        onPress={props.onChangeActiveReferralCode}
      >
        {$t(
          defineMessage({
            defaultMessage: "Enter",
            description: "ReferralScreen/Top Panel/Traders Tab",
          }),
        )}
      </Button>
    </View>
  )
}
