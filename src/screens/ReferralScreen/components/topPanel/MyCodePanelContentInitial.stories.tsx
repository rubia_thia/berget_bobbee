import { ComponentMeta } from "@storybook/react"
import {
  BackgroundColor,
  CardContainer,
} from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { MyCodePanelContentInitial } from "./MyCodePanelContentInitial"
import { myCodePanelContentInitialProps } from "./MyCodePanelContentInitial.mockData"

export default {
  title: "Page/ReferralScreen/TopPanel/MyCodePanelContentInitial",
  component: MyCodePanelContentInitial,
  decorators: [CardContainer(), BackgroundColor()],
} as ComponentMeta<typeof MyCodePanelContentInitial>

const template = withTemplate(MyCodePanelContentInitial, {
  style: {
    margin: 10,
  },
  ...myCodePanelContentInitialProps,
})

export const Normal = template()
