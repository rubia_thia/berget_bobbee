import { ComponentMeta } from "@storybook/react"
import {
  BackgroundColor,
  CardContainer,
} from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { MyCodeContentPanelForUser } from "./MyCodeContentPanelForUser"
import { myCodePanelContentForUserProps } from "./MyCodeContentPanelForUser.mockData"

export default {
  title: "Page/ReferralScreen/TopPanel/MyCodeContentPanelForUser",
  component: MyCodeContentPanelForUser,
  decorators: [CardContainer(), BackgroundColor()],
} as ComponentMeta<typeof MyCodeContentPanelForUser>

const template = withTemplate(MyCodeContentPanelForUser, {
  style: {
    margin: 10,
  },
  ...myCodePanelContentForUserProps,
})

export const Normal = template()
