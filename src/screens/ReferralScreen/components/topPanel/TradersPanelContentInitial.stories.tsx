import { ComponentMeta } from "@storybook/react"
import {
  BackgroundColor,
  CardContainer,
} from "../../../../../.storybook/decorators"
import { withTemplate } from "../../../../../.storybook/utils"
import { TradersPanelContentInitial } from "./TradersPanelContentInitial"
import { tradersPanelContentInitialProps } from "./TradersPanelContentInitial.mockData"

export default {
  title: "Page/ReferralScreen/TopPanel/TradersPanelContentInitial",
  component: TradersPanelContentInitial,
  decorators: [CardContainer(), BackgroundColor()],
} as ComponentMeta<typeof TradersPanelContentInitial>

const template = withTemplate(TradersPanelContentInitial, {
  style: {
    margin: 10,
  },
  ...tradersPanelContentInitialProps,
})

export const Normal = template()
