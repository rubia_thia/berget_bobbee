import { useIntl } from "react-intl"
import { StyleProp, View, ViewStyle } from "react-native"
import { CardBoxView } from "../../../../components/CardBox/CardBox"
import { useSpacing } from "../../../../components/Themed/spacing"
import { FCC } from "../../../../utils/reactHelpers/types"
import { TextOnlySegmentControl } from "../TextOnlySegmentControl"
import { TopPanelTab, topPanelTab$t } from "../types"

export interface TopPanelFrameProps {
  style?: StyleProp<ViewStyle>
  currentTab: TopPanelTab
  onCurrentTabChange: (tab: TopPanelTab) => void
}

export const TopPanelFrame: FCC<TopPanelFrameProps> = props => {
  const spacing = useSpacing()
  const { $t } = useIntl()

  return (
    <CardBoxView
      style={props.style}
      className={"space-y-5"}
      padding={spacing(6)}
    >
      <TextOnlySegmentControl
        items={[
          {
            key: TopPanelTab.Traders,
            labelText: $t(topPanelTab$t.traders),
            value: TopPanelTab.Traders,
          },
          {
            key: TopPanelTab.MyCode,
            labelText: $t(topPanelTab$t.myCode),
            value: TopPanelTab.MyCode,
          },
        ]}
        selectedItemKey={props.currentTab}
        onSelectedItem={i => props.onCurrentTabChange(i.value)}
      />

      <View>{props.children}</View>
    </CardBoxView>
  )
}
