import { FC, ReactNode } from "react"
import { StyleProp, Text, View, ViewStyle } from "react-native"
import { useColors } from "../../../../../components/Themed/color"
import { useSpacing } from "../../../../../components/Themed/spacing"
import { wrapText } from "../../../../../utils/reactHelpers/childrenHelpers"
import { styleGetters } from "../../../../../utils/reactHelpers/withProps/styleGetters"
import { withProps } from "../../../../../utils/reactHelpers/withProps/withProps"

export interface InfoBlockProps {
  style?: StyleProp<ViewStyle>
  title: ReactNode
  children: ReactNode
}

export const InfoBlock: FC<InfoBlockProps> = props => {
  const spacing = useSpacing()
  const colors = useColors()
  return (
    <View
      style={[
        props.style,
        {
          padding: spacing(5),
          borderRadius: 8,
          borderWidth: 1,
          borderColor: colors("gray-200"),
        },
      ]}
    >
      <View>{wrapText(props.title, { Text: TitleText })}</View>
      <View style={{ marginTop: spacing(1), flex: 1 }}>
        {wrapText(props.children, { Text: BodyText })}
      </View>
    </View>
  )
}

export const TitleText = withProps(
  styleGetters(({ colors }) => ({
    color: colors("gray-900"),
    fontSize: 14,
  })),
  Text,
)

export const BodyText = withProps(
  styleGetters(({ colors }) => ({
    color: colors("gray-900"),
    fontSize: 30,
    fontWeight: "bold",
  })),
  Text,
)
