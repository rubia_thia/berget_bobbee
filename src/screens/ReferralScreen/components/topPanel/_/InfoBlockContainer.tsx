import { Children } from "react"
import { StyleProp, View, ViewStyle } from "react-native"
import { FCC } from "../../../../../utils/reactHelpers/types"
import { useSizeQuery } from "../../../../../utils/reactHelpers/useSizeQuery"

export interface InfoBlockContainerProps {
  style?: StyleProp<ViewStyle>
  infoBlockCount?: number
}

export const InfoBlockContainer: FCC<InfoBlockContainerProps> = props => {
  const infoBlockCount = Children.toArray(props.children).length

  const [layout = "column", onLayout] = useSizeQuery({
    0: "column",
    [infoBlockCount * 200]: "row",
  } as const)

  return (
    <View
      className={layout === "column" ? "space-y-5" : "space-x-5"}
      style={[{ flexDirection: layout, alignItems: "stretch" }, props.style]}
      onLayout={onLayout}
    >
      {props.children}
    </View>
  )
}
