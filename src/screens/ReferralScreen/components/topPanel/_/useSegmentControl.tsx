import { useIntl } from "react-intl"
import { TextOnlySegmentControl } from "../../TextOnlySegmentControl"
import { TopPanelTab, topPanelTab$t } from "../../types"

export const useSegmentControl = (
  currentTab: TopPanelTab,
  onSelectAnotherTab: () => void,
): JSX.Element => {
  const { $t } = useIntl()

  return (
    <TextOnlySegmentControl
      items={[
        {
          key: TopPanelTab.Traders,
          labelText: $t(topPanelTab$t.traders),
          value: TopPanelTab.Traders,
        },
        {
          key: TopPanelTab.MyCode,
          labelText: $t(topPanelTab$t.myCode),
          value: TopPanelTab.MyCode,
        },
      ]}
      selectedItemKey={currentTab}
      onSelectedItem={i => {
        if (i.value === currentTab) return
        onSelectAnotherTab()
      }}
    />
  )
}
