import { OptionalizeEventListeners } from "../../../../../.storybook/utils"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { MyCodeContentPanelForUserProps } from "./MyCodeContentPanelForUser"

export const myCodePanelContentForUserProps: OptionalizeEventListeners<MyCodeContentPanelForUserProps> =
  {
    rebateRate: BigNumber.from(0.1),
    totalTradingVolumeInUSD: BigNumber.from(100000),
    totalRebateVolumeInUSD: BigNumber.from(1000000),
    totalReferredCount: 20,
  }
