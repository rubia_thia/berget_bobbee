import { OptionalizeEventListeners } from "../../../../../.storybook/utils"
import { BigNumber } from "../../../../utils/numberHelpers/BigNumber"
import { TradersPanelContentActivatedProps } from "./TradersPanelContentActivated"

export const tradersPanelContentActivatedProps: OptionalizeEventListeners<TradersPanelContentActivatedProps> =
  {
    activeReferralCode: "abc123456",
    discountRate: BigNumber.from(0.1),
    totalDiscountVolumeInUSD: BigNumber.from(100000),
    totalTradingVolumeInUSD: BigNumber.from(1000000),
  }
