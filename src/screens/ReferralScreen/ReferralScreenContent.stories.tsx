import { ComponentMeta } from "@storybook/react"
import { BackgroundColor } from "../../../.storybook/decorators"
import { withTemplate } from "../../../.storybook/utils"
import { noop } from "../../utils/fnHelpers"
import { MyCodeTablePanel } from "./components/MyCodeTablePanel"
import { myCodeTablePanelProps } from "./components/MyCodeTablePanel.mockData"
import { MyCodeContentPanelForUser } from "./components/topPanel/MyCodeContentPanelForUser"
import { myCodePanelContentForUserProps } from "./components/topPanel/MyCodeContentPanelForUser.mockData"
import { TradersPanelContentActivated } from "./components/topPanel/TradersPanelContentActivated"
import { tradersPanelContentActivatedProps } from "./components/topPanel/TradersPanelContentActivated.mockData"
import { ReferralScreenContent } from "./ReferralScreenContent"

export default {
  title: "Page/ReferralScreen/ReferralScreenContent",
  component: ReferralScreenContent,
  decorators: [BackgroundColor()],
} as ComponentMeta<typeof ReferralScreenContent>

const template = withTemplate(ReferralScreenContent, {
  onCreateCode: noop,
  topTraderPanelContent: p => (
    <TradersPanelContentActivated
      {...p}
      {...tradersPanelContentActivatedProps}
      onChangeActiveReferralCode={noop}
    />
  ),
  topMyCodePanelContent: p => (
    <MyCodeContentPanelForUser {...myCodePanelContentForUserProps} {...p} />
  ),
  myCodeTable: p => (
    <MyCodeTablePanel
      {...p}
      {...myCodeTablePanelProps}
      onPaginationChange={noop}
    />
  ),
  isCreatedReferralCode: true,
})

export const Normal = template()
