import { InjectedConnector } from "@web3-react/injected-connector"
import { WalletConnectConnector } from "@web3-react/walletconnect-connector"
import { WalletLinkConnector } from "@web3-react/walletlink-connector"
import { APP_NAME } from "./appConstant"

const injected = new InjectedConnector({})

// we want to connect user first before redirecting them to the correct chain
const supportChainIds = [
  1, 3, 4, 42, 5, 100, 56, 137, 80001, 42161, 10, 43114, 250, 65656565656,
  987654321097, 97,
]

// const supportChainIds = [65656565656, 987654321097, 97, 1, 56]

const walletConnect = (
  chainId: number,
  rpcUrl: string,
): WalletConnectConnector =>
  new WalletConnectConnector({
    rpc: {
      [chainId]: rpcUrl,
    },
    supportedChainIds: supportChainIds,
    chainId: chainId,
    qrcode: true,
  })

const walletLinkConnector = (
  chainId: number,
  rpcUrl: string,
): WalletLinkConnector =>
  new WalletLinkConnector({
    url: rpcUrl,
    supportedChainIds: supportChainIds,
    appName: APP_NAME,
  })

export enum SupportLoginType {
  MetaMask = "MetaMask",
  WalletConnect = "WalletConnect",
  CoinbaseWallet = "CoinbaseWallet",
}

export const connectors = (chainId: number, rpcUrl: string) =>
  ({
    [SupportLoginType.MetaMask]: injected,
    [SupportLoginType.WalletConnect]: walletConnect(chainId, rpcUrl),
    [SupportLoginType.CoinbaseWallet]: walletLinkConnector(chainId, rpcUrl),
  } as const)
