import { useWeb3React, Web3ReactProvider } from "@web3-react/core"
import { ethers } from "ethers"
import { useEffect, useRef } from "react"
import { FCC } from "../../utils/reactHelpers/types"
import { useCreation } from "../../utils/reactHelpers/useCreation"
import { useAppEnvStore } from "../AppEnvStore/useAppEnvStore"
import { createStore } from "../utils/createStore"
import { AuthStore } from "./AuthStore"

const { Provider, useStore } = createStore<AuthStore>("AuthStore")

export const useAuthStore = useStore.bind(null)

function getLibrary(provider: any): any {
  return new ethers.providers.Web3Provider(provider)
}

export const WalletConnectProvider: FCC = props => (
  <Web3ReactProvider getLibrary={getLibrary}>
    {props.children}
  </Web3ReactProvider>
)

export const AuthStoreProvider: FCC = props => {
  const web3React = useWeb3React()
  const appEnvStore = useAppEnvStore()
  const initRef = useRef(web3React)
  const store = useCreation(
    () => new AuthStore(appEnvStore, initRef.current),
    [appEnvStore],
  )
  useEffect(() => {
    store.setWeb3Ref(web3React)
  }, [store, web3React])
  return <Provider store={store}>{props.children}</Provider>
}
