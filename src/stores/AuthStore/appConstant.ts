export const APP_NAME = "UniWhale"
export const APP_LOGO_URL = "https://uniwhales.io/images/favicon.png"
export const DEFAULT_ETH_JSONRPC_URL =
  "https://eth-mainnet.uniwhale.co/rzxgpTjAEI4gYa4b"
export const DEFAULT_CHAIN_ID = 1337
