import { JsonRpcSigner } from "@ethersproject/providers/src.ts/json-rpc-provider"
import type { useWeb3React } from "@web3-react/core"
import { ethers } from "ethers"
import { action, computed, makeObservable, observable } from "mobx"
import { asyncAction, runAsyncAction } from "../../utils/asyncAction"
import { AppEnvStore } from "../AppEnvStore/AppEnvStore"
import { hydrate, persist } from "../utils/persist"
import { waitFor, waitUntilExist$ } from "../utils/waitFor"
import { connectors, SupportLoginType } from "./AuthStore.service"

export type Web3RefType = ReturnType<
  typeof useWeb3React<ethers.providers.Web3Provider>
>

export class AuthStore {
  constructor(readonly appEnvStore: AppEnvStore, web3Ref: Web3RefType) {
    this.web3Ref = web3Ref
    makeObservable(this)
    void hydrate("io.uniwhale.authStore", this)
  }

  @observable.ref private web3Ref: Web3RefType
  @action setWeb3Ref(web3Ref: Web3RefType): void {
    this.web3Ref = web3Ref
  }

  @computed get connected(): boolean {
    return this.web3Ref.active
  }

  @computed get isMockAccount$(): boolean {
    return this.appEnvStore.mockUser != null
  }

  @computed get account$(): string {
    if (this.appEnvStore.mockUser) {
      return this.appEnvStore.mockUser
    }
    return waitUntilExist$(() => this.web3Ref.account)
  }

  @computed get chainId$(): number {
    return waitUntilExist$(() => this.web3Ref.chainId)
  }

  @computed get library$(): ethers.providers.Web3Provider {
    return waitUntilExist$(() => this.web3Ref.library)
  }

  @persist @observable connectedProvider?: SupportLoginType

  @computed({ keepAlive: true }) get signer$(): JsonRpcSigner {
    return this.library$.getSigner(this.account$)
  }

  @computed get signerOrLibrary$():
    | JsonRpcSigner
    | ethers.providers.JsonRpcProvider {
    // return this.appEnvStore.publicJSONRPCProvider$
    return this.connected && this.connectedToTheRightChain$
      ? this.signer$
      : this.appEnvStore.publicJSONRPCProvider$
  }

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  @computed({ keepAlive: true }) get connectors$() {
    return connectors(
      this.appEnvStore.appEnv$.chainId,
      this.appEnvStore.appEnv$.rpcUrl,
    )
  }

  @asyncAction async login(
    login: SupportLoginType,
    run = runAsyncAction,
  ): Promise<void> {
    const connectors = await run(waitFor(() => this.connectors$))
    await run(this.web3Ref.activate(connectors[login], undefined, true))
    this.connectedProvider = login
  }

  @asyncAction async disconnect(): Promise<void> {
    if (this.isMockAccount$) {
      this.appEnvStore.mockUser = undefined
    } else {
      this.web3Ref.deactivate()
      this.connectedProvider = undefined
    }
  }

  get isMetaMaskInstalled(): boolean {
    // @ts-ignore
    return Boolean(window.ethereum?.isMetaMask)
  }

  @observable showConnectWalletModal = false

  @action triggerConnectWalletModal(): void {
    this.showConnectWalletModal = true
  }

  @computed get connectedToTheRightChain$(): boolean {
    return this.chainId$ === this.appEnvStore.appEnv$.chainId
  }

  async switchToRightChain(): Promise<void> {
    const appEnv$ = await waitFor(() => this.appEnvStore.appEnv$)
    const library$ = await waitFor(() => this.library$)
    const chainId = "0x" + appEnv$.chainId.toString(16)
    try {
      await library$.send("wallet_switchEthereumChain", [{ chainId }])
    } catch {
      await library$.send("wallet_addEthereumChain", [
        {
          chainId: chainId,
          rpcUrls: [appEnv$.rpcUrl],
          chainName: appEnv$.chainName,
          nativeCurrency: appEnv$.nativeCurrency,
          blockExplorerUrls: [appEnv$.explorerUrl],
        },
      ])
    }
  }
}
