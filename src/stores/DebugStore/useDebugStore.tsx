import { FCC } from "../../utils/reactHelpers/types"
import { useCreation } from "../../utils/reactHelpers/useCreation"
import { useAppEnvStore } from "../AppEnvStore/useAppEnvStore"
import { useAuthStore } from "../AuthStore/useAuthStore"
import { useContractStore } from "../contracts/useContractStore"
import { useCurrencyStore } from "../CurrencyStore/useCurrencyStore"
import { useTradeInfoStore } from "../TradeStore/useTradeInfoStore"
import { createStore } from "../utils/createStore"
import { DebugStore } from "./DebugStore"

const { Provider, useStore, ContextBridgeSymbol } =
  createStore<DebugStore>("DebugStore")

export const DebugStoreContextBridgeSymbol = ContextBridgeSymbol

export const useDebugStore = useStore.bind(null)

export const DebugStoreProvider: FCC = props => {
  const appEnvStore = useAppEnvStore()
  const authStore = useAuthStore()
  const currencyStore = useCurrencyStore()
  const contracts = useContractStore()
  const tradeInfo = useTradeInfoStore()
  const store = useCreation(
    () =>
      new DebugStore(
        appEnvStore,
        authStore,
        currencyStore,
        contracts,
        tradeInfo,
      ),
    [appEnvStore, authStore, contracts, currencyStore, tradeInfo],
  )
  return <Provider store={store}>{props.children}</Provider>
}
