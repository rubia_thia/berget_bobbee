import { computed, makeObservable } from "mobx"
import { memoizeWith } from "ramda"
import { firstValueFrom } from "rxjs"
import { Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Bool_Exp } from "../../generated/graphql/graphql.generated"
import { DebugPositionRecord } from "../../screens/Debug/components/PositionsTabContent/types"
import { AppEnvStore } from "../AppEnvStore/AppEnvStore"
import { AuthStore } from "../AuthStore/AuthStore"
import { ContractStore } from "../contracts/ContractStore"
import { CurrencyStore } from "../CurrencyStore/CurrencyStore"
import {
  closeOrderEvents,
  openOrderEvents,
  updateOrderEvents,
} from "../TradeStore/OrderHistoryModule.marketOrder.service"
import { TradeCellModule } from "../TradeStore/TradeCellModule"
import { TradeInfoStore } from "../TradeStore/TradeInfoStore"
import { ethAddressToHasuraAddress } from "../utils/hasuraClient"
import { LazyValue } from "../utils/LazyValue"
import { PaginationStore } from "../utils/PaginationStore"
import { getOrderHashesByTxHash } from "./DebugStore.services"

class TransactionModule {
  constructor(
    readonly store: DebugStore,
    readonly where: Pick<
      Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Bool_Exp,
      "trade_user" | "evt_tx_hash" | "orderHash"
    >,
  ) {
    makeObservable(this)
  }
  #openOrderEvents = new LazyValue(
    () =>
      [
        this.store.appEnvStore.hasura$,
        this.store.appEnvStore.appOpenBlockHeight$,
        this.where,
      ] as const,
    ([tc, block, where]) => openOrderEvents(tc, block, where, 100),
  )

  #updateOrderEvents = new LazyValue(
    () =>
      [
        this.store.appEnvStore.hasura$,
        this.store.appEnvStore.appOpenBlockHeight$,
        this.where,
      ] as const,
    ([tc, block, where]) => updateOrderEvents(tc, block, where, 100),
  )

  #closeOrderEvents = new LazyValue(
    () =>
      [
        this.store.appEnvStore.hasura$,
        this.store.appEnvStore.appOpenBlockHeight$,
        this.where,
      ] as const,
    ([tc, block, where]) => closeOrderEvents(tc, block, where, 100),
  )

  @computed private get openPositions$(): TradeCellModule[] {
    return this.#openOrderEvents.value$.map(openEvent => {
      const closed = this.#closeOrderEvents.value$.filter(
        a => a.orderHash === openEvent.orderHash,
      )
      const updated = this.#updateOrderEvents.value$.filter(
        a => a.orderHash === openEvent.orderHash,
      )
      const latestEvent = [openEvent, ...closed, ...updated].sort(
        (a, b) => b.blockNumber - a.blockNumber,
      )[0]!
      return new TradeCellModule(
        latestEvent,
        this.store.tradeInfoStore,
        openEvent,
      )
    })
  }

  @computed
  get paginationStore(): PaginationStore<DebugPositionRecord> {
    return new PaginationStore(
      () => this.openPositions$.map(p => p.debugPositionRecord$),
      { pageSize: 5 },
    )
  }
}

export class DebugStore {
  constructor(
    readonly appEnvStore: AppEnvStore,
    readonly authStore: AuthStore,
    readonly currencyStore: CurrencyStore,
    readonly contracts: ContractStore,
    readonly tradeInfoStore: TradeInfoStore,
  ) {
    makeObservable(this)
  }

  all = new TransactionModule(this, {})

  wallet = memoizeWith(
    String,
    (address: string) =>
      new TransactionModule(this, {
        trade_user: { _eq: ethAddressToHasuraAddress(address) },
      }),
  )

  orderDetail = memoizeWith(
    String,
    (orderId: string) =>
      new TransactionModule(this, {
        orderHash: { _eq: ethAddressToHasuraAddress(orderId) },
      }),
  )

  @computed
  get openPositionsPaginationStore(): PaginationStore<DebugPositionRecord> {
    return this.all.paginationStore
  }

  async getOrderHashesByTxHash(txHash: string): Promise<string | null> {
    return firstValueFrom(
      getOrderHashesByTxHash(this.appEnvStore.hasura$, txHash),
    )
  }
}
