import { Client, gql } from "@urql/core"
import { Observable } from "rxjs"
import {
  LatestTxsQuery,
  LatestTxsQueryVariables,
  OrderHashesByTxHashQuery,
  OrderHashesByTxHashQueryVariables,
} from "../../generated/graphql/graphql.generated"
import {
  ethAddressToHasuraAddress,
  fromUrqlSource,
  hasuraAddressToEth,
} from "../utils/hasuraClient"

export namespace DebugOrder {
  export enum EventTypes {
    OpenMarketOrderEvent = "OpenMarketOrderEvent",
    UpdateOpenOrderEvent = "UpdateOpenOrderEvent",
    CloseMarketOrderEvent = "CloseMarketOrderEvent",
  }

  export type Event = {
    type: EventTypes
    txId: string
    user: string
    orderHash: string
    blockNumber: number
  }
}

export function fetchLatestOrderHashes(
  hasura: Client,
): Observable<DebugOrder.Event[]> {
  return fromUrqlSource(
    hasura.query<LatestTxsQuery, LatestTxsQueryVariables>(
      gql`
        query LatestTxs {
          uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent(
            order_by: { evt_block_number: desc }
            limit: 100
          ) {
            evt_tx_hash
            orderHash
            evt_block_number
            trade_user
          }
          uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent(
            order_by: { evt_block_number: desc }
            limit: 100
          ) {
            evt_tx_hash
            orderHash
            evt_block_number
            trade_user
          }
          uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent(
            order_by: { evt_block_number: desc }
            limit: 100
          ) {
            evt_tx_hash
            orderHash
            evt_block_number
            trade_user
          }
        }
      `,
      {},
    ),
    result => {
      return [
        ...result.data.uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent.map(
          (a): DebugOrder.Event => ({
            txId: hasuraAddressToEth(a.evt_tx_hash),
            orderHash: hasuraAddressToEth(a.orderHash),
            type: DebugOrder.EventTypes.OpenMarketOrderEvent,
            blockNumber: a.evt_block_number,
            user: hasuraAddressToEth(a.trade_user),
          }),
        ),
        ...result.data.uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent.map(
          (a): DebugOrder.Event => ({
            txId: hasuraAddressToEth(a.evt_tx_hash),
            orderHash: hasuraAddressToEth(a.orderHash),
            type: DebugOrder.EventTypes.UpdateOpenOrderEvent,
            blockNumber: a.evt_block_number,
            user: hasuraAddressToEth(a.trade_user),
          }),
        ),
        ...result.data.uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent.map(
          (a): DebugOrder.Event => ({
            txId: hasuraAddressToEth(a.evt_tx_hash),
            orderHash: hasuraAddressToEth(a.orderHash),
            type: DebugOrder.EventTypes.CloseMarketOrderEvent,
            blockNumber: a.evt_block_number,
            user: hasuraAddressToEth(a.trade_user),
          }),
        ),
      ]
    },
  )
}

export function getOrderHashesByTxHash(
  hasura: Client,
  txHash: string,
): Observable<string | null> {
  return fromUrqlSource(
    hasura.query<OrderHashesByTxHashQuery, OrderHashesByTxHashQueryVariables>(
      gql`
        query OrderHashesByTxHash($txHash: bytea!) {
          uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent(
            where: { evt_tx_hash: { _eq: $txHash } }
          ) {
            orderHash
          }
          uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent(
            where: { evt_tx_hash: { _eq: $txHash } }
          ) {
            orderHash
          }
          uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent(
            where: { evt_tx_hash: { _eq: $txHash } }
          ) {
            orderHash
          }
          uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent(
            where: { evt_tx_hash: { _eq: $txHash } }
          ) {
            orderHash
          }
          uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent(
            where: { evt_tx_hash: { _eq: $txHash } }
          ) {
            orderHash
          }
          uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent(
            where: { evt_tx_hash: { _eq: $txHash } }
          ) {
            orderHash
          }
          uniwhale_v1_bsc_LimitBook_evt_CloseLimitOrderEvent(
            where: { evt_tx_hash: { _eq: $txHash } }
          ) {
            orderHash
          }
        }
      `,
      { txHash: ethAddressToHasuraAddress(txHash) },
    ),
    ({ data }) => {
      const orderHash =
        data.uniwhale_v1_bsc_LimitBook_evt_CloseLimitOrderEvent[0]?.orderHash ??
        data.uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent[0]
          ?.orderHash ??
        data.uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent[0]
          ?.orderHash ??
        data.uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent[0]?.orderHash ??
        data.uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent[0]
          ?.orderHash ??
        data.uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent[0]
          ?.orderHash ??
        data.uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent[0]?.orderHash
      if (orderHash) {
        return hasuraAddressToEth(orderHash)
      }
      return null
    },
  )
}
