import { Client, gql } from "@urql/core"
import { Observable } from "rxjs"
import {
  GetTotalDiscountVolumeOfTraderQuery,
  GetTotalDiscountVolumeOfTraderQueryVariables,
  GetTotalTradingVolumeOfTraderQuery,
  GetTotalTradingVolumeOfTraderQueryVariables,
} from "../../generated/graphql/graphql.generated"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import {
  ethAddressToHasuraAddress,
  fromUrqlSource,
} from "../utils/hasuraClient"

export const getTotalTradingVolumeOfTrader = (
  hasura: Client,
  walletAddress: string,
): Observable<BigNumber> => {
  return fromUrqlSource(
    hasura.query(
      gql<
        GetTotalTradingVolumeOfTraderQuery,
        GetTotalTradingVolumeOfTraderQueryVariables
      >`
        query GetTotalTradingVolumeOfTrader($walletAddress: bytea) {
          cp_referee_summary(where: { trade_user: { _eq: $walletAddress } }) {
            trade_volume
          }
        }
      `,
      {
        walletAddress: ethAddressToHasuraAddress(walletAddress),
      },
    ),
    resp =>
      BigNumber.sum(resp.data.cp_referee_summary.map(d => d.trade_volume ?? 0)),
  )
}

export const getTotalDiscountVolumeOfTrader = (
  hasura: Client,
  walletAddress: string,
): Observable<BigNumber> => {
  return fromUrqlSource(
    hasura.query(
      gql<
        GetTotalDiscountVolumeOfTraderQuery,
        GetTotalDiscountVolumeOfTraderQueryVariables
      >`
        query GetTotalDiscountVolumeOfTrader($walletAddress: bytea) {
          cp_referee_summary(where: { trade_user: { _eq: $walletAddress } }) {
            referred_fee
          }
        }
      `,
      {
        walletAddress: ethAddressToHasuraAddress(walletAddress),
      },
    ),
    resp =>
      BigNumber.sum(resp.data.cp_referee_summary.map(s => s.referred_fee ?? 0)),
  )
}
