import { computed, makeObservable } from "mobx"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { AppEnvStore } from "../AppEnvStore/AppEnvStore"
import { LazyValue } from "../utils/LazyValue"
import { ActivatedReferral } from "./ReferralStore.service"
import {
  getTotalDiscountVolumeOfTrader,
  getTotalTradingVolumeOfTrader,
} from "./TraderModule.graphql"

export class TraderModule {
  constructor(
    private appEnvStore: AppEnvStore,
    private walletAddress: string,
    public referral: ActivatedReferral,
  ) {
    makeObservable(this)
  }

  get discountRate(): BigNumber {
    return this.referral.discountRate
  }

  private totalTradingVolume = new LazyValue(
    () => [this.appEnvStore.hasura$, this.walletAddress] as const,
    ([hasura, walletAddress]) =>
      getTotalTradingVolumeOfTrader(hasura, walletAddress),
  )
  @computed
  get totalTradingVolumeInUSD$(): BigNumber {
    return this.totalTradingVolume.value$
  }

  private totalDiscountVolume = new LazyValue(
    () => [this.appEnvStore.hasura$, this.walletAddress] as const,
    ([hasura, walletAddress]) =>
      getTotalDiscountVolumeOfTrader(hasura, walletAddress),
  )
  @computed
  get totalDiscountVolumeInUSD$(): BigNumber {
    return this.totalDiscountVolume.value$
  }
}
