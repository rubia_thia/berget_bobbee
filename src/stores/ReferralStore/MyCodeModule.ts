import { computed, makeObservable } from "mobx"
import { from } from "rxjs"
import { MyCodeTableRecord } from "../../screens/ReferralScreen/components/MyCodeTablePanel"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { DELAYED, readResource } from "../../utils/SuspenseResource"
import { AppEnvStore } from "../AppEnvStore/AppEnvStore"
import { ContractStore } from "../contracts/ContractStore"
import { LazyValue } from "../utils/LazyValue"
import { PaginationStore } from "../utils/PaginationStore"
import {
  getAllReferralCodeInfoOfReferer,
  getReferredCountOfReferer,
  getReferredCountOfReferralCode,
  getTotalRebateVolumeOfReferralCode,
  getTotalTradingVolumeOfReferer,
  getTotalTradingVolumeOfReferralCode,
} from "./MyCodeModule.graphql"
import {
  getReferralCodeDebateRate,
  getReferralSettingObservable,
} from "./ReferralStore.service"

export class MyCodeModuleForUser {
  constructor(
    private appEnvStore: AppEnvStore,
    private contracts: ContractStore,
    private contractsForEvent: ContractStore,
    private referralCode: string,
    private walletAddress: string,
  ) {
    makeObservable(this)
  }

  isMultiCodeUser = false as const

  @computed
  get canCreateNewReferralCode$(): boolean {
    return false
  }

  private totalRebateVolume = new LazyValue(
    () =>
      [
        this.appEnvStore.hasura$,
        this.walletAddress,
        this.referralCode,
      ] as const,
    ([hasura, walletAddress, referralCode]) =>
      getTotalRebateVolumeOfReferralCode(hasura, walletAddress, referralCode),
  )
  @computed
  get totalRebateVolumeInUSD$(): BigNumber {
    return this.totalRebateVolume.value$
  }

  private totalTradingVolume = new LazyValue(
    () =>
      [
        this.appEnvStore.hasura$,
        this.walletAddress,
        this.referralCode,
      ] as const,
    ([hasura, walletAddress, referralCode]) =>
      getTotalTradingVolumeOfReferralCode(hasura, walletAddress, referralCode),
  )
  @computed
  get totalTradingVolumeInUSD$(): BigNumber {
    return this.totalTradingVolume.value$
  }

  private totalReferredCount = new LazyValue(
    () =>
      [
        this.appEnvStore.hasura$,
        this.walletAddress,
        this.referralCode,
      ] as const,
    ([hasura, walletAddress, referralCode]) =>
      getReferredCountOfReferralCode(hasura, walletAddress, referralCode),
  )
  @computed
  get totalReferredCount$(): number {
    return this.totalReferredCount.value$
  }

  private referralCodeRebateRate = new LazyValue(
    () => [this.contracts.referrals$, this.referralCode] as const,
    ([referrals, referralCode]) =>
      from(getReferralCodeDebateRate(referrals, referralCode)),
  )
  @computed
  get rebateRate$(): BigNumber {
    if (this.referralCodeRebateRate.value$ == null) {
      return BigNumber.ZERO
    }

    return this.referralCodeRebateRate.value$.rebateRate
  }

  private referralCodeInfos = new LazyValue(
    () =>
      [
        this.contractsForEvent.referrals$,
        this.appEnvStore.hasura$,
        this.walletAddress,
      ] as const,
    ([referrals, hasura, walletAddress]) =>
      getAllReferralCodeInfoOfReferer(referrals, hasura, walletAddress),
  )
  referralCodesPagination = new PaginationStore<MyCodeTableRecord>(
    () => this.referralCodeInfos.value$,
  )
}

export class MyCodeModuleForMultiCodeUser {
  constructor(
    private appEnvStore: AppEnvStore,
    private contracts: ContractStore,
    private contractsForEvent: ContractStore,
    private walletAddress: string,
  ) {
    makeObservable(this)
  }

  isMultiCodeUser = true as const

  @computed
  get canCreateNewReferralCode$(): boolean {
    return this.referralSetting.value$.count < this.referralSetting.value$.max
  }

  referralSetting = new LazyValue(
    () =>
      [
        this.contracts.referrals$,
        this.contractsForEvent.referrals$,
        this.walletAddress,
      ] as const,
    ([referrals, referralsForEvent, walletAddress]) =>
      getReferralSettingObservable(referrals, referralsForEvent, walletAddress),
  )

  private referralCodeInfos = new LazyValue(
    () =>
      [
        this.contractsForEvent.referrals$,
        this.appEnvStore.hasura$,
        this.walletAddress,
      ] as const,
    ([referrals, hasura, walletAddress]) =>
      getAllReferralCodeInfoOfReferer(referrals, hasura, walletAddress),
  )
  referralCodesPagination = new PaginationStore<MyCodeTableRecord>(
    () => this.referralCodeInfos.value$,
  )

  private totalReferredCount = new LazyValue(
    () => [this.appEnvStore.hasura$, this.walletAddress] as const,
    ([hasura, walletAddress]) =>
      getReferredCountOfReferer(hasura, walletAddress),
  )
  @computed
  get totalReferredCount$(): number {
    return this.totalReferredCount.value$
  }

  private totalTradingVolume = new LazyValue(
    () => [this.appEnvStore.hasura$, this.walletAddress] as const,
    ([hasura, walletAddress]) =>
      getTotalTradingVolumeOfReferer(hasura, walletAddress),
  )
  @computed
  get totalTradingVolumeInUSD$(): BigNumber {
    return this.totalTradingVolume.value$
  }

  @computed
  get distributionDetailsLink$(): string {
    // TODO
    return readResource(DELAYED())
  }
}
