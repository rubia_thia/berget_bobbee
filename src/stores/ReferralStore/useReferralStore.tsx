import { FCC } from "../../utils/reactHelpers/types"
import { useCreation } from "../../utils/reactHelpers/useCreation"
import { useAppEnvStore } from "../AppEnvStore/useAppEnvStore"
import { useAuthStore } from "../AuthStore/useAuthStore"
import {
  useContractStore,
  useContractStoreForEventListen,
} from "../contracts/useContractStore"
import { useCurrencyStore } from "../CurrencyStore/useCurrencyStore"
import { createStore } from "../utils/createStore"
import { ReferralStore } from "./ReferralStore"

const { Provider, useStore, ContextBridgeSymbol } =
  createStore<ReferralStore>("ReferralStore")

export const ReferralStoreContextBridgeSymbol = ContextBridgeSymbol

export const useReferralStore = useStore.bind(null)

export const ReferralStoreProvider: FCC<{
  receivedReferralCode?: string
}> = props => {
  const appEnvStore = useAppEnvStore()
  const authStore = useAuthStore()
  const currencyStore = useCurrencyStore()
  const contracts = useContractStore()
  const contractsForEvent = useContractStoreForEventListen()
  const store = useCreation(
    () =>
      new ReferralStore(
        props.receivedReferralCode,
        appEnvStore,
        authStore,
        currencyStore,
        contracts,
        contractsForEvent,
      ),
    [
      appEnvStore,
      authStore,
      contracts,
      currencyStore,
      contractsForEvent,
      props.receivedReferralCode,
    ],
  )
  return <Provider store={store}>{props.children}</Provider>
}
