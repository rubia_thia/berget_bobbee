import { constants, ContractTransaction, ethers } from "ethers"
import { noop } from "lodash"
import memoizeOne from "memoize-one"
import {
  defer,
  filter,
  map,
  merge,
  Observable,
  shareReplay,
  switchMap,
} from "rxjs"
import { fromContractToNative } from "../../utils/contractHelpers/bigNumberContracts"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { fromContractEvent } from "../../utils/rxjsHelpers/fromContractEvent"
import { startWithObservable } from "../../utils/rxjsHelpers/rxjsHelpers"
import { Referrals } from "../contracts/generated"
import { IReferral } from "../contracts/generated/Referrals"

/**
 * https://github.com/uniwhale-io/uniwhale-v1/blob/d727219ee7b2af799e78d6f18bea4d829ee969f3/packages/contracts/core-v1/contracts/interfaces/IReferral.sol#L6-L11
 */
export interface ActivatedReferral {
  discountRate: BigNumber // % of fee
  rebateRate: BigNumber // % of fee
  referrer: string // address
  code: string
}
function contractReferralToActivatedReferral(
  r: IReferral.ReferralStructOutput,
): ActivatedReferral {
  return {
    discountRate: fromContractToNative(r.rebatePct),
    rebateRate: fromContractToNative(r.referralRebatePct),
    referrer: r.referrer,
    code: decodeReferralCode(r.referralCode),
  }
}
export const getActivatedReferralObservable = memoizeOne(
  (
    referrals: Referrals,
    referralsForEvent: Referrals,
    walletAddress: string,
  ): Observable<undefined | ActivatedReferral> => {
    return fromContractEvent(
      referrals,
      referrals.filters["UseReferralCodeEvent(address,bytes32)"](),
    ).pipe(
      filter(([referee]) => referee === walletAddress),
      switchMap(() => getActivatedReferral(referrals, walletAddress)),
      startWithObservable(
        defer(() => getActivatedReferral(referrals, walletAddress)),
      ),
      shareReplay(1),
    )
  },
)
export const getActivatedReferral = async (
  referrals: Referrals,
  walletAddress: string,
): Promise<undefined | ActivatedReferral> => {
  const referral = await referrals.getReferral(walletAddress)
  return referral.referrer === constants.AddressZero
    ? undefined
    : contractReferralToActivatedReferral(referral)
}

export const getFirstReferralCode = (
  referrals: Referrals,
  referralsForEvent: Referrals,
  walletAddress: string,
): Observable<undefined | string> => {
  const fetchData = async (): Promise<undefined | string> => {
    try {
      const code = await referrals.referralCodes(walletAddress, 0)
      return decodeReferralCode(code)
    } catch (err) {
      console.warn("[getFirstReferralCode]", err)
      return undefined
    }
  }

  return referralCodeCountMayChangedObservable(
    referralsForEvent,
    walletAddress,
  ).pipe(switchMap(fetchData), startWithObservable(defer(fetchData)))
}

export const getReferralCodeDebateRate = async (
  referrals: Referrals,
  referralCode: string,
): Promise<
  | undefined
  | {
      discountRate: BigNumber
      rebateRate: BigNumber
    }
> => {
  try {
    const pct = await referrals.getRebatePct(encodeReferralCode(referralCode))
    return {
      discountRate: fromContractToNative(pct.referredRebatePct),
      rebateRate: fromContractToNative(pct.referralRebatePct),
    }
  } catch (err) {
    console.warn("[getReferralCodeDebateRate]", err)
    return undefined
  }
}

export const getReferralSettingObservable = memoizeOne(
  (
    referrals: Referrals,
    referralsForEvent: Referrals,
    walletAddress: string,
  ): Observable<{ count: number; max: number }> => {
    return referralCodeCountMayChangedObservable(
      /**
       * We use referrals (instead of referralsForEvent) intentionally, cuz we
       * use referrals.getReferralSetting later
       */
      referrals,
      walletAddress,
    ).pipe(
      switchMap(() => getReferralSetting(referrals, walletAddress)),
      startWithObservable(
        defer(() => getReferralSetting(referrals, walletAddress)),
      ),
      shareReplay(1),
    )
  },
)
export const getReferralSetting = async (
  referrals: Referrals,
  walletAddress: string,
): Promise<{ count: number; max: number }> => {
  const res = await referrals.getReferralSetting(walletAddress)
  return { count: res.count.toNumber(), max: res.max.toNumber() }
}

export const referralCodeCountMayChangedObservable = memoizeOne(
  (referralsForEvent: Referrals, walletAddress: string): Observable<void> => {
    return merge(
      fromContractEvent(
        referralsForEvent,
        referralsForEvent.filters["CreateReferralCodeEvent(address,bytes32)"](),
      ).pipe(filter(([addr]) => addr === walletAddress)),
      fromContractEvent(
        referralsForEvent,
        referralsForEvent.filters["RemoveReferralCodeEvent(address,bytes32)"](),
      ).pipe(filter(([addr]) => addr === walletAddress)),
    ).pipe(map(noop), shareReplay(1))
  },
)

export const getReferer = async (
  referrals: Referrals,
  referralCode: string,
): Promise<undefined | string> => {
  let referer: string

  try {
    referer = await referrals.referrer(encodeReferralCode(referralCode))
  } catch {
    return
  }

  if (referer === constants.AddressZero) {
    return undefined
  }

  return referer
}

export const createReferralCode = async (
  referrals: Referrals,
  walletAddress: string,
  code: string,
): Promise<ContractTransaction> => {
  return referrals.createReferralCode(encodeReferralCode(code), {
    from: walletAddress,
  })
}

export const changeActivatedReferralCode = async (
  referrals: Referrals,
  walletAddress: string,
  code: string,
): Promise<ContractTransaction> => {
  return referrals.useReferralCode(encodeReferralCode(code), {
    from: walletAddress,
  })
}

export function encodeReferralCode(code: string): Uint8Array {
  // https://github.com/uniwhale-io/uniwhale-v1/blob/dc02a57f318eeb1bc7a23a65301cb213e832153e/packages/contracts/core-v1/test/trading-core/market-order.test.ts#L69
  return ethers.utils.zeroPad(Buffer.from(code, "ascii"), 32)
}

export function decodeReferralCode(code: string): string {
  return new Buffer(ethers.utils.stripZeros(code)).toString()
}
