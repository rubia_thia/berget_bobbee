import { Client, gql } from "@urql/core"
import { sum } from "ramda"
import { delay, Observable, switchMap } from "rxjs"
import {
  GetAllReferralCodeInfoOfRefererQuery,
  GetAllReferralCodeInfoOfRefererQueryVariables,
  GetReferredCountOfRefererQuery,
  GetReferredCountOfRefererQueryVariables,
  GetReferredCountOfReferralCodeQuery,
  GetReferredCountOfReferralCodeQueryVariables,
  GetTotalRebateVolumeOfRefererQuery,
  GetTotalRebateVolumeOfRefererQueryVariables,
  GetTotalRebateVolumeOfReferralCodeQuery,
  GetTotalRebateVolumeOfReferralCodeQueryVariables,
  GetTotalTradingVolumeOfRefererQuery,
  GetTotalTradingVolumeOfRefererQueryVariables,
  GetTotalTradingVolumeOfReferralCodeQuery,
  GetTotalTradingVolumeOfReferralCodeQueryVariables,
} from "../../generated/graphql/graphql.generated"
import { MyCodeTableRecord } from "../../screens/ReferralScreen/components/MyCodeTablePanel"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { startWithObservable } from "../../utils/rxjsHelpers/rxjsHelpers"
import { Referrals } from "../contracts/generated"
import {
  ethAddressToHasuraAddress,
  ethArrayBufferToHasuraAddress,
  fromUrqlSource,
  hasuraAddressToEth,
} from "../utils/hasuraClient"
import {
  decodeReferralCode,
  encodeReferralCode,
  referralCodeCountMayChangedObservable,
} from "./ReferralStore.service"

export const getReferredCountOfReferer = (
  hasura: Client,
  walletAddress: string,
): Observable<number> => {
  return fromUrqlSource(
    hasura.query(
      gql<
        GetReferredCountOfRefererQuery,
        GetReferredCountOfRefererQueryVariables
      >`
        query GetReferredCountOfReferer($walletAddress: bytea) {
          cp_referral_counter(where: { referrer: { _eq: $walletAddress } }) {
            count
          }
        }
      `,
      {
        walletAddress: ethAddressToHasuraAddress(walletAddress),
      },
    ),
    resp => {
      return sum(resp.data.cp_referral_counter.map(c => c.count ?? 0))
    },
  )
}

export const getReferredCountOfReferralCode = (
  hasura: Client,
  walletAddress: string,
  referralCode: string,
): Observable<number> => {
  return fromUrqlSource(
    hasura.query(
      gql<
        GetReferredCountOfReferralCodeQuery,
        GetReferredCountOfReferralCodeQueryVariables
      >`
        query GetReferredCountOfReferralCode(
          $walletAddress: bytea
          $referralCode: bytea
        ) {
          cp_referral_counter(
            where: {
              referrer: { _eq: $walletAddress }
              referralCode: { _eq: $referralCode }
            }
          ) {
            count
          }
        }
      `,
      {
        walletAddress: ethAddressToHasuraAddress(walletAddress),
        referralCode: ethArrayBufferToHasuraAddress(
          encodeReferralCode(referralCode),
        ),
      },
    ),
    resp => {
      return sum(resp.data.cp_referral_counter.map(d => d.count ?? 0))
    },
  )
}

export const getTotalRebateVolumeOfReferer = (
  hasura: Client,
  walletAddress: string,
): Observable<BigNumber> => {
  return fromUrqlSource(
    hasura.query(
      gql<
        GetTotalRebateVolumeOfRefererQuery,
        GetTotalRebateVolumeOfRefererQueryVariables
      >`
        query GetTotalRebateVolumeOfReferer($walletAddress: bytea) {
          cp_referrer_summary_aggregate(
            where: { fee_referrer: { _eq: $walletAddress } }
          ) {
            aggregate {
              sum {
                referral_fee
              }
            }
          }
        }
      `,
      {
        walletAddress: ethAddressToHasuraAddress(walletAddress),
      },
    ),
    resp =>
      BigNumber.from(
        resp.data.cp_referrer_summary_aggregate.aggregate?.sum?.referral_fee ??
          0,
      ),
  )
}

export const getTotalRebateVolumeOfReferralCode = (
  hasura: Client,
  walletAddress: string,
  referralCode: string,
): Observable<BigNumber> => {
  return fromUrqlSource(
    hasura.query(
      gql<
        GetTotalRebateVolumeOfReferralCodeQuery,
        GetTotalRebateVolumeOfReferralCodeQueryVariables
      >`
        query GetTotalRebateVolumeOfReferralCode(
          $walletAddress: bytea
          $referralCode: bytea
        ) {
          cp_referrer_summary_aggregate(
            where: {
              fee_referrer: { _eq: $walletAddress }
              fee_referralCode: { _eq: $referralCode }
            }
          ) {
            aggregate {
              sum {
                referral_fee
              }
            }
          }
        }
      `,
      {
        walletAddress: ethAddressToHasuraAddress(walletAddress),
        referralCode: ethArrayBufferToHasuraAddress(
          encodeReferralCode(referralCode),
        ),
      },
    ),
    resp =>
      BigNumber.from(
        resp.data.cp_referrer_summary_aggregate.aggregate?.sum?.referral_fee ??
          0,
      ),
  )
}

export const getTotalTradingVolumeOfReferer = (
  hasura: Client,
  walletAddress: string,
): Observable<BigNumber> => {
  return fromUrqlSource(
    hasura.query(
      gql<
        GetTotalTradingVolumeOfRefererQuery,
        GetTotalTradingVolumeOfRefererQueryVariables
      >`
        query GetTotalTradingVolumeOfReferer($walletAddress: bytea) {
          cp_referrer_summary_aggregate(
            where: { fee_referrer: { _eq: $walletAddress } }
          ) {
            aggregate {
              sum {
                trade_volume
              }
            }
          }
        }
      `,
      {
        walletAddress: ethAddressToHasuraAddress(walletAddress),
      },
    ),
    resp =>
      BigNumber.from(
        resp.data.cp_referrer_summary_aggregate.aggregate?.sum?.trade_volume ??
          0,
      ),
  )
}

export const getTotalTradingVolumeOfReferralCode = (
  hasura: Client,
  walletAddress: string,
  referralCode: string,
): Observable<BigNumber> => {
  return fromUrqlSource(
    hasura.query(
      gql<
        GetTotalTradingVolumeOfReferralCodeQuery,
        GetTotalTradingVolumeOfReferralCodeQueryVariables
      >`
        query GetTotalTradingVolumeOfReferralCode(
          $walletAddress: bytea
          $referralCode: bytea
        ) {
          cp_referrer_summary_aggregate(
            where: {
              fee_referrer: { _eq: $walletAddress }
              fee_referralCode: { _eq: $referralCode }
            }
          ) {
            aggregate {
              sum {
                trade_volume
              }
            }
          }
        }
      `,
      {
        walletAddress: ethAddressToHasuraAddress(walletAddress),
        referralCode: ethArrayBufferToHasuraAddress(
          encodeReferralCode(referralCode),
        ),
      },
    ),
    resp =>
      BigNumber.from(
        resp.data.cp_referrer_summary_aggregate.aggregate?.sum?.trade_volume ??
          0,
      ),
  )
}

export const getAllReferralCodeInfoOfReferer = (
  referrals: Referrals,
  hasura: Client,
  walletAddress: string,
): Observable<MyCodeTableRecord[]> => {
  const source = fromUrqlSource(
    hasura.query(
      gql<
        GetAllReferralCodeInfoOfRefererQuery,
        GetAllReferralCodeInfoOfRefererQueryVariables
      >`
        query GetAllReferralCodeInfoOfReferer($walletAddress: bytea) {
          cp_referrer_summary(
            where: { fee_referrer: { _eq: $walletAddress } }
          ) {
            fee_referralCode
            trade_volume
          }
          cp_referral_counter(where: { referrer: { _eq: $walletAddress } }) {
            referralCode
            count
          }
        }
      `,
      {
        walletAddress: ethAddressToHasuraAddress(walletAddress),
      },
    ),
    resp => {
      return resp.data.cp_referral_counter.flatMap(c => {
        if (c.referralCode == null) return []
        const code = decodeReferralCode(hasuraAddressToEth(c.referralCode))
        const link = `https://app.uniwhale.co/referral/${code}`
        return [
          {
            code,
            link,
            referredTraderCount: c.count ?? 0,
            totalVolumeInUSD: BigNumber.from(
              resp.data.cp_referrer_summary.find(
                s => s.fee_referralCode === c.referralCode,
              )?.trade_volume ?? 0,
            ),
          },
        ]
      })
    },
  )

  return referralCodeCountMayChangedObservable(
    /**
     * We use referrals (instead of referralsForEvent) intentionally, cuz the backend
     * use HTTP RPC node not WebSocket RPC
     */
    referrals,
    walletAddress,
  ).pipe(
    // our database sync data every 3 seconds for events which dependent by cp_referral_*
    delay(4000),
    switchMap(() => source),
    startWithObservable(source),
  )
}
