import { ContractTransaction } from "ethers"
import { action, computed, makeObservable } from "mobx"
import { from } from "rxjs"
import {
  EnterReferralCodeModalContentError,
  EnterReferralCodeModalContentErrorType,
} from "../../screens/ReferralScreen/components/EnterReferralCodeModalContent.types"
import {
  GenerateReferralCodeModalContentError,
  GenerateReferralCodeModalContentErrorType,
} from "../../screens/ReferralScreen/components/GenerateReferralCodeModalContent.types"
import { asyncAction, runAsyncAction } from "../../utils/asyncAction"
import { AppEnvStore } from "../AppEnvStore/AppEnvStore"
import { AuthStore } from "../AuthStore/AuthStore"
import { ContractStore } from "../contracts/ContractStore"
import { CurrencyStore } from "../CurrencyStore/CurrencyStore"
import { LazyValue } from "../utils/LazyValue"
import { Result } from "../utils/Result"
import { SuspenseObservable } from "../utils/SuspenseObservable"
import {
  MyCodeModuleForMultiCodeUser,
  MyCodeModuleForUser,
} from "./MyCodeModule"
import {
  changeActivatedReferralCode,
  createReferralCode,
  getActivatedReferralObservable,
  getFirstReferralCode,
  getReferer,
  getReferralSettingObservable,
} from "./ReferralStore.service"
import { TraderModule } from "./TraderModule"

export class ReferralStore {
  constructor(
    receivedReferralCode: string | undefined,
    readonly appEnvStore: AppEnvStore,
    readonly authStore: AuthStore,
    readonly currencyStore: CurrencyStore,
    readonly contracts: ContractStore,
    readonly contractsForEvent: ContractStore,
  ) {
    makeObservable(this)
    this.switchCodeModule = new SuspenseObservable<SwitchCodeModule>(
      receivedReferralCode == null
        ? undefined
        : new SwitchCodeModule(
            receivedReferralCode,
            this.contracts,
            this.authStore,
            () => this.onCloseSwitchCode(),
          ),
    )
  }

  @asyncAction
  async changeActivatedReferral(
    referralCode: string,
    run = runAsyncAction,
  ): Promise<ContractTransaction> {
    return await run(
      changeActivatedReferralCode(
        this.contracts.referrals$,
        this.authStore.account$,
        referralCode,
      ),
    )
  }

  switchCodeModule: SuspenseObservable<SwitchCodeModule>

  @action
  onStartSwitchCode(): void {
    this.switchCodeModule.set(
      new SwitchCodeModule("", this.contracts, this.authStore, () =>
        this.onCloseSwitchCode(),
      ),
    )
  }
  @action
  onCloseSwitchCode(): void {
    this.switchCodeModule.set(undefined)
  }

  createCodeModule = new SuspenseObservable<CreateCodeModule>()
  @action
  onStartCreateCode(): void {
    this.createCodeModule.set(
      new CreateCodeModule(this.contracts, this.authStore, () =>
        this.onCloseCreateCode(),
      ),
    )
  }
  @action
  onCloseCreateCode(): void {
    this.createCodeModule.set(undefined)
  }

  myActivatedReferral = new LazyValue(
    () =>
      [
        this.contracts.referrals$,
        this.contractsForEvent.referrals$,
        this.authStore.account$,
      ] as const,
    ([referrals, referralsForEvent, walletAddress]) =>
      getActivatedReferralObservable(
        referrals,
        referralsForEvent,
        walletAddress,
      ),
  )

  @computed({ keepAlive: true })
  get traderModule$(): undefined | TraderModule {
    if (this.myActivatedReferral.value$ == null) {
      return undefined
    }
    return new TraderModule(
      this.appEnvStore,
      this.authStore.account$,
      this.myActivatedReferral.value$,
    )
  }

  private myFirstReferralCode = new LazyValue(
    () =>
      [
        this.contracts.referrals$,
        this.contractsForEvent.referrals$,
        this.authStore.account$,
      ] as const,
    ([referrals, referralsForEvent, walletAddress]) =>
      getFirstReferralCode(referrals, referralsForEvent, walletAddress),
  )

  private referralSetting = new LazyValue(
    () =>
      [
        this.contracts.referrals$,
        this.contractsForEvent.referrals$,
        this.authStore.account$,
      ] as const,
    ([referrals, referralsForEvent, walletAddress]) =>
      getReferralSettingObservable(referrals, referralsForEvent, walletAddress),
  )

  private get isMultiCodeUser$(): boolean {
    return this.referralSetting.value$.max > 1
  }

  @computed
  get isCurrentAccountCreatedReferralCode$(): boolean {
    return this.referralSetting.value$.count > 0
  }

  @computed({ keepAlive: true })
  get myCodeModule$():
    | undefined
    | MyCodeModuleForMultiCodeUser
    | MyCodeModuleForUser {
    if (!this.isCurrentAccountCreatedReferralCode$) {
      return undefined
    } else if (this.isMultiCodeUser$) {
      return new MyCodeModuleForMultiCodeUser(
        this.appEnvStore,
        this.contracts,
        this.contractsForEvent,
        this.authStore.account$,
      )
    } else {
      return new MyCodeModuleForUser(
        this.appEnvStore,
        this.contracts,
        this.contractsForEvent,
        this.myFirstReferralCode.value$!,
        this.authStore.account$,
      )
    }
  }
}

interface SwitchCodeModuleFormData {
  referralCode: string
}
class SwitchCodeModule {
  constructor(
    defaultInputtedCode: string,
    private contracts: ContractStore,
    private authStore: AuthStore,
    private closeSelf: () => void,
  ) {
    this.inputtedCode = new SuspenseObservable(defaultInputtedCode)
  }

  inputtedCode: SuspenseObservable<string>

  referrer = new LazyValue(
    () => [this.contracts.referrals$, this.inputtedCode.read$] as const,
    ([referrals, code]) => from(getReferer(referrals, code)),
  )

  get formData$(): Result<
    SwitchCodeModuleFormData,
    EnterReferralCodeModalContentError[]
  > {
    if (!this.authStore.connected) {
      return Result.error([
        { type: EnterReferralCodeModalContentErrorType.WalletNotConnected },
      ])
    }
    if (this.inputtedCode.get() === "") {
      return Result.error([
        { type: EnterReferralCodeModalContentErrorType.CodeEmpty },
      ])
    }

    if (this.referrer.value$ == null) {
      return Result.error([
        { type: EnterReferralCodeModalContentErrorType.CodeNotExists },
      ])
    }

    // if (this.referrer.value$ === this.authStore.account$) {
    //   return Result.error([
    //     { type: EnterReferralCodeModalContentErrorType.UseSelfGeneratedCode },
    //   ])
    // }

    return Result.ok({
      referralCode: this.inputtedCode.read$,
    })
  }

  @asyncAction
  async submit(
    formData: SwitchCodeModuleFormData,
    run = runAsyncAction,
  ): Promise<ContractTransaction> {
    const res = await run(
      changeActivatedReferralCode(
        this.contracts.referrals$,
        this.authStore.account$,
        formData.referralCode,
      ),
    )
    this.closeSelf()
    return res
  }
}

interface CreateCodeModuleFormData {
  referralCode: string
}
class CreateCodeModule {
  constructor(
    private contracts: ContractStore,
    private authStore: AuthStore,
    private closeSelf: () => void,
  ) {}

  inputtedCode = new SuspenseObservable("")

  referrer = new LazyValue(
    () => [this.contracts.referrals$, this.inputtedCode.read$] as const,
    ([referrals, code]) => from(getReferer(referrals, code)),
  )

  get formData$(): Result<
    CreateCodeModuleFormData,
    GenerateReferralCodeModalContentError[]
  > {
    const inputtedCode = this.inputtedCode.get()

    if (
      inputtedCode == null ||
      inputtedCode.length < 3 ||
      inputtedCode.length >= 20
    ) {
      return Result.error([
        {
          type: GenerateReferralCodeModalContentErrorType.CodeLengthInvalid as const,
        },
      ])
    }

    if (this.referrer.value$ != null) {
      return Result.error([
        {
          type: GenerateReferralCodeModalContentErrorType.CodeAlreadyExists as const,
        },
      ])
    }

    if (!/^[a-z0-9]+$/.test(inputtedCode)) {
      return Result.error([
        {
          type: GenerateReferralCodeModalContentErrorType.CodeInvalid as const,
          validContent: "a-z,0-9",
        },
      ])
    }

    return Result.ok({
      referralCode: this.inputtedCode.read$,
    })
  }

  @asyncAction
  async submit(
    formData: SwitchCodeModuleFormData,
    run = runAsyncAction,
  ): Promise<ContractTransaction> {
    const res = await run(
      createReferralCode(
        this.contracts.referrals$,
        this.authStore.account$,
        formData.referralCode,
      ),
    )
    this.closeSelf()
    return res
  }
}
