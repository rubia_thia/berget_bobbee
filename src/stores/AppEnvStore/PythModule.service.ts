import { Client, gql } from "@urql/core"
import { Observable } from "rxjs"
import {
  PythPriceSubscription,
  PythPriceSubscriptionVariables,
} from "../../generated/graphql/graphql.generated"
import { DisplayableError } from "../../utils/errorHelpers"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import {
  ethAddressToHasuraAddress,
  fromUrqlSource,
  hasuraAddressToEth,
} from "../utils/hasuraClient"
import mainnetPriceIds from "./priceids.mainnet.json"
import testnetPriceIds from "./priceids.testnet.json"

export type PriceSymbol = keyof typeof mainnetPriceIds

export function isPriceSymbol(input: string): input is PriceSymbol {
  return input in mainnetPriceIds
}

export type PythEnv = "ETH_MAINNET" | "ETH_TESTNET"

export const getPriceId = (symbol: PriceSymbol, env: PythEnv): string => {
  if (env === "ETH_MAINNET") {
    return mainnetPriceIds[symbol].id
  }
  return testnetPriceIds[symbol].id
}

export const getTokenNames = (
  symbol: PriceSymbol,
  env: PythEnv,
): [baseTokenName: string, quoteTokenName: string] => {
  if (env === "ETH_MAINNET") {
    return mainnetPriceIds[symbol].name.split("/") as any
  }
  return testnetPriceIds[symbol].name.split("/") as any
}

export type PythPrice = {
  price: BigNumber
  conf: BigNumber
  expo: number
  publishTime: Date
}

interface PriceInfo {
  price: string
  conf: string
  expo: number
  publishTime: Date
}
export const mapToPythPrice = (priceInfo: PriceInfo): PythPrice => {
  const { price, conf, expo, publishTime } = priceInfo
  const shift = BigNumber.rightMoveDecimals(expo)
  return {
    price: shift(BigNumber.from(price)),
    conf: shift(BigNumber.from(conf)),
    expo: expo,
    publishTime,
  }
}

export const subscribeToPriceId = (
  hasura: Client,
  priceId: string,
): Observable<PythPrice & { priceData: string }> => {
  return fromUrqlSource(
    hasura.subscription<PythPriceSubscription, PythPriceSubscriptionVariables>(
      gql`
        subscription PythPrice($priceId: bytea!) {
          latest_pyth_prices_by_pk(price_id: $priceId) {
            price
            publish_time
            conf
            expo
            price_data
          }
        }
      `,
      { priceId: ethAddressToHasuraAddress(priceId) },
    ),
    result => {
      const price = result.data.latest_pyth_prices_by_pk
      if (price == null) {
        throw new DisplayableError(`Price id "${priceId}" not found`, {
          cause: "subscribeToPriceId",
        })
      }
      return {
        ...mapToPythPrice({
          publishTime: new Date(price.publish_time),
          expo: price.expo,
          price: String(price.price),
          conf: String(price.conf),
        }),
        priceData: hasuraAddressToEth(price.price_data!),
      }
    },
  )
}
