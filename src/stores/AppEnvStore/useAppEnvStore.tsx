import { FCC } from "../../utils/reactHelpers/types"
import { useCreation } from "../../utils/reactHelpers/useCreation"
import { createStore } from "../utils/createStore"
import { AppEnvStore } from "./AppEnvStore"

const { Provider, useStore } = createStore<AppEnvStore>("AppEnvStore")

export const useAppEnvStore = useStore.bind(null)

export const AppEnvStoreProvider: FCC = props => {
  const store = useCreation(() => new AppEnvStore(), [])
  return <Provider store={store}>{props.children}</Provider>
}
