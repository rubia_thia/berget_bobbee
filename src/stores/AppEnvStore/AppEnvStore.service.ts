import { captureMessage, withScope } from "@sentry/react-native"
import { identity, keys } from "ramda"
import { from, Observable } from "rxjs"
import { DisplayableError } from "../../utils/errorHelpers"
import { assertNotNull } from "../../utils/types"
import { UniwhaleConfig } from "../contracts/generated"
import { ONCHAIN_CONFIG_KEY } from "./ONCHAIN_CONFIG_KEY"
import { PriceSymbol, PythEnv } from "./PythModule.service"

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const defaultAppEnv = ({
  valueFor,
  optionalValueFor,
}: {
  valueFor: <T = string>(entryName: string, parser?: (input: string) => T) => T
  optionalValueFor: <T = string>(
    entryName: string,
    parser?: (input: string) => T,
  ) => T
}) => ({
  maintenanceMode: valueFor("Maintenance_Mode") === "true",
  uniwhaleConfigAddress: valueFor("UniwhaleConfig_addr"),
  rpcUrl: valueFor("Chain_RPC"),
  wsRPCUrl: valueFor("Chain_WS_RPC"),
  chainId: parseInt(valueFor("Chain_ID"), 10),
  chainName: valueFor("Chain_Name"),
  chainSwitcherName: valueFor("Chain_Switcher_Name"),
  nativeCurrency: {
    name: valueFor("Chain_Currency"),
    symbol: valueFor("Chain_Currency"),
    decimals: 18,
  },
  explorerUrl: valueFor("Chain_Explorer"),
  hasuraUrl: valueFor("Hasura_URL"),
  priceHasuraUrl: valueFor("Price_Hasura_URL"),
  airdropHasuraUrl: valueFor("Airdrop_Hasura_URL"),
  airdropEnableMainnetContributor:
    valueFor("Airdrop_EnableMainnetContributor") === "true",
  airdrop_MainnetContributor_LiquidityProvider_Stage1SnapshotBlockHeight:
    optionalValueFor(
      "Airdrop_MainnetContributor_LiquidityProvider_Stage1SnapshotBlockHeight",
      parseNumber,
    ),
  airdrop_MainnetContributor_LiquidityProvider_Stage1LoyaltyPhaseSnapshotBlockHeight:
    optionalValueFor(
      "Airdrop_MainnetContributor_LiquidityProvider_Stage1LoyaltyPhaseSnapshotBlockHeight_2023-03-17",
      parseNumber,
    ),
  airdrop_MainnetContributor_LiquidityProvider_Stage2SnapshotBlockHeight:
    optionalValueFor(
      "Airdrop_MainnetContributor_LiquidityProvider_Stage2SnapshotBlockHeight",
      parseNumber,
    ),
  airdrop_MainnetContributor_Trader_Stage1SnapshotBlockHeight: optionalValueFor(
    "Airdrop_MainnetContributor_Trader_Stage1SnapshotBlockHeight",
    parseNumber,
  ),
  airdrop_MainnetContributor_Trader_Stage2SnapshotBlockHeight: optionalValueFor(
    "Airdrop_MainnetContributor_Trader_Stage2SnapshotBlockHeight",
    parseNumber,
  ),
  trade_ShowMorePrice: optionalValueFor("Trade_ShowMorePrice", parseBoolean),
  trade_ShowSpreadRange: optionalValueFor(
    "Trade_ShowSpreadRange",
    parseBoolean,
  ),
  earn_thenaLiquidityPoolLink: optionalValueFor("Earn_Thena_LiquidityPoolUrl"),
  earn_thenaLiquidityPoolApr: optionalValueFor("Earn_Thena_LiquidityPoolApr"),
  earn_pancakeLiquidityPoolLink: optionalValueFor(
    "Earn_Pancake_LiquidityPoolUrl",
  ),
  earn_pancakeLiquidityPoolApr: optionalValueFor(
    "Earn_Pancake_LiquidityPoolApr",
    parseNumber,
  ),
  earn_enableCompound: optionalValueFor("Earn_EnableCompound", parseBoolean),
  showLPTab: __DEV__ || valueFor("Show_LP_Tab") === "true",
  showEarnTab: __DEV__ || valueFor("Show_Earn_Tab") === "true",
  showAirdropTab: __DEV__ || valueFor("Show_Airdrop_Tab") === "true",
  showReferralTab: __DEV__ || valueFor("Show_Referral_Tab") === "true",
  showFaucetTab: __DEV__ || valueFor("Show_Faucet_Tab") === "true",
})

const parseBoolean = (input: string): boolean => {
  return input === "true"
}

const parseNumber = (input: string): undefined | number => {
  const res = Number(input)
  if (Number.isNaN(res)) {
    return undefined
  } else {
    return res
  }
}

export type AppEnv = ReturnType<typeof defaultAppEnv> & {
  tradeMarketPrecision: { [key in PriceSymbol]?: number }
}

const configUrl = __DEV__ ? "https://uniwhale.tk/config" : "/config"
export async function fetchAppEnv(origin: string): Promise<AppEnv> {
  const fetchingURL = configUrl + "?origin=" + encodeURIComponent(origin)
  try {
    const result = await fetch(fetchingURL, {
      mode: "cors",
    }).then(r => r.json())
    const appEnv = defaultAppEnv({
      valueFor: (entryName, parser = identity as any) => {
        const value = parser(result[entryName])
        if (value == null) {
          throw new DisplayableError(
            `Field "${entryName}" not found in app env`,
            {
              cause: "fetchAppEnv",
            },
          )
        }
        return value
      },
      optionalValueFor: (entryName, parser = identity as any) => {
        return parser(result[entryName])
      },
    })
    const tradeMarketPrecision: { [key in PriceSymbol]?: number } = {}
    Object.keys(result)
      .filter(a => a.startsWith("Trade_Market_Precision"))
      .forEach(a => {
        const market = a.split(".")[1]
        const value = Number(result[a])
        if (market && !isNaN(value)) {
          tradeMarketPrecision[market as PriceSymbol] = value
        }
      })
    return {
      ...appEnv,
      tradeMarketPrecision,
    }
  } catch (err) {
    console.error("fetchAppEnv", err)

    withScope(scope => {
      scope.setExtra("error", err)
      scope.setExtra("origin", origin)
      scope.setExtra("fetchingUrl", fetchingURL)
      captureMessage("Request app config failed")
    })

    return { maintenanceMode: true } as Pick<
      AppEnv,
      "maintenanceMode"
    > as AppEnv
  }
}

type Parser<T> = { key: ONCHAIN_CONFIG_KEY; parse: (value?: string) => T }

function parseString<T extends string = string>(
  key: ONCHAIN_CONFIG_KEY,
): Parser<T>
function parseString<U, T extends string = string>(
  key: ONCHAIN_CONFIG_KEY,
  map: (value: T) => U,
): Parser<U>
function parseString(
  key: ONCHAIN_CONFIG_KEY,
  map?: (value: any) => any,
): Parser<any> {
  return {
    key,
    parse: (value?: string) => {
      assertNotNull(value)
      if (map != null) {
        return map(value)
      }
      return value
    },
  }
}

const configParser = {
  genesisBlock: parseString(ONCHAIN_CONFIG_KEY.GENESIS_BLOCK),
  tradingCore: parseString(ONCHAIN_CONFIG_KEY.TRADING_CORE),
  tradingCoreWithRouter: parseString(
    ONCHAIN_CONFIG_KEY.TRADING_CORE_WITH_ROUTER,
  ),
  tradingCoreLib: parseString(ONCHAIN_CONFIG_KEY.TRADING_CORE_LIB),
  limitBook: parseString(ONCHAIN_CONFIG_KEY.LIMIT_BOOK),
  liquidityPool: parseString(ONCHAIN_CONFIG_KEY.LIQUIDITY_POOL),
  swapRouter: parseString(ONCHAIN_CONFIG_KEY.SWAP_ROUTER),
  registryCore: parseString(ONCHAIN_CONFIG_KEY.REGISTRY_CORE),
  registryReader: parseString(ONCHAIN_CONFIG_KEY.REGISTRY_READER),
  genesisPass: parseString(ONCHAIN_CONFIG_KEY.GENESIS_PASS),
  pyth: parseString(ONCHAIN_CONFIG_KEY.PYTH),
  anchorToken: parseString(ONCHAIN_CONFIG_KEY.ANCHOR_TOKEN),
  anchorTokenSymbol: parseString(ONCHAIN_CONFIG_KEY.ANCHOR_TOKEN_SYMBOL),
  tBUSDToken: parseString(ONCHAIN_CONFIG_KEY.TBUSD_TOKEN),
  tBUSDTokenSymbol: parseString(ONCHAIN_CONFIG_KEY.TBUSD_TOKEN_SYMBOL),
  tUSDTToken: parseString(ONCHAIN_CONFIG_KEY.TUSDT_TOKEN),
  tUSDTTokenSymbol: parseString(ONCHAIN_CONFIG_KEY.TUSDT_TOKEN_SYMBOL),
  BUSDToken: parseString(ONCHAIN_CONFIG_KEY.BUSD_TOKEN),
  BUSDTokenSymbol: parseString(ONCHAIN_CONFIG_KEY.BUSD_TOKEN_SYMBOL),
  USDCToken: parseString(ONCHAIN_CONFIG_KEY.USDC_TOKEN),
  USDCTokenSymbol: parseString(ONCHAIN_CONFIG_KEY.USDC_TOKEN_SYMBOL),
  oracleAggregator: parseString(ONCHAIN_CONFIG_KEY.ORACLE_AGGREGATOR),
  referrals: parseString(ONCHAIN_CONFIG_KEY.REFERRALS),
  fees: parseString(ONCHAIN_CONFIG_KEY.FEE_CONTRACT),
  uniwhaleToken: parseString(ONCHAIN_CONFIG_KEY.UNIWHALE_TOKEN),
  esUniwhaleToken: parseString(ONCHAIN_CONFIG_KEY.ES_UNIWHALE_TOKEN),
  stakingHelper: parseString(ONCHAIN_CONFIG_KEY.STAKING_HELPER),
  traderFarm: parseString(ONCHAIN_CONFIG_KEY.TRADER_FARM),
  isFork: parseString(ONCHAIN_CONFIG_KEY.IS_FORK, a => a === "true"),
  isMockToken: parseString(
    ONCHAIN_CONFIG_KEY.IS_MOCK_TOKENS,
    a => a === "true",
  ),
  pythEnv: parseString<PythEnv>(ONCHAIN_CONFIG_KEY.PYTH_ENV),
  approvedPriceSymbols: parseString(
    ONCHAIN_CONFIG_KEY.APPROVED_PRICE_SYMBOLS,
    a => a.split(",").map(a => a as PriceSymbol),
  ),
  revenuePool: parseString(ONCHAIN_CONFIG_KEY.REVENUE_POOL),
  airdropHelper: parseString(ONCHAIN_CONFIG_KEY.AIRDROP_HELPER),
  multiCall: parseString(ONCHAIN_CONFIG_KEY.MULTI_CALL),
  testnetPOAP: parseString(ONCHAIN_CONFIG_KEY.TESTNET_POAP),
}
export const isCompatibleCachedConfig = (
  config: any,
): config is OnChainConfigType =>
  keys(configParser).every(key => config[key] != null)

export type OnChainConfigType = {
  [K in keyof typeof configParser]: ReturnType<
    (typeof configParser)[K]["parse"]
  >
}

export const fetchOnChainConfig = (
  config: UniwhaleConfig,
): Observable<OnChainConfigType> => {
  const keysToFetch = keys(configParser)
  const keyToGetConfig = keysToFetch.map(a => configParser[a].key)
  return from(
    config.getConfigs(keyToGetConfig).then(values => {
      const result = {} as OnChainConfigType
      for (let i = 0; i < keysToFetch.length; i++) {
        const key = keysToFetch[i]!
        const value = values[i]
        const parser = configParser[key]
        // @ts-ignore
        result[key] = parser.parse(value)
      }
      return result
    }),
  )
}
