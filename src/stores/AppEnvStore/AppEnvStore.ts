import { Client } from "@urql/core"
import { ethers } from "ethers"
import {
  computed,
  makeObservable,
  observable,
  reaction,
  runInAction,
} from "mobx"
import { persist } from "mobx-persist"
import { from, Observable, shareReplay } from "rxjs"
import SturdyWebSocket from "sturdy-websocket"
import { ChainInfo } from "../../utils/ChainInfo"
import { ChainInfoPresets } from "../../utils/ChainInfoPresets/ChainInfoPresets"
import { noop } from "../../utils/fnHelpers"
import { isPromiseLike } from "../../utils/promiseHelpers"
import { UniwhaleConfig, UniwhaleConfig__factory } from "../contracts/generated"
import { createHasuraClient } from "../utils/hasuraClient"
import { LazyValue } from "../utils/LazyValue"
import { hydrate } from "../utils/persist"
import {
  AppEnv,
  fetchAppEnv,
  fetchOnChainConfig,
  isCompatibleCachedConfig,
  OnChainConfigType,
} from "./AppEnvStore.service"
import { PythModule } from "./PythModule"
import { PriceSymbol } from "./PythModule.service"

export class AppEnvStore {
  constructor() {
    makeObservable(this)
    void hydrate("io.uniwhale.appEnvStore", this).then(() => {
      reaction(
        () => this.#onChainConfig.value$,
        value => {
          if (value != null) {
            runInAction(() => {
              this.savedConfig = value
            })
          }
        },
        { fireImmediately: true, onError: noop },
      )
    })
  }

  @persist @observable origin = window.location.origin
  @persist @observable mockUser?: string

  @persist @observable acceptedDisclaimers = false
  @persist @observable previousMarket?: PriceSymbol

  private appEnv = new LazyValue(
    () => this.origin,
    origin => from(fetchAppEnv(origin)),
  )

  @computed get appEnv$(): AppEnv {
    return this.appEnv.value$
  }

  transactionExplorerUrl(txHash: string): string {
    const { explorerUrl } = this.appEnv$
    return `${explorerUrl}/tx/${txHash}`
  }

  addressExplorerUrl(accountAddress: string): string {
    const { explorerUrl } = this.appEnv$
    return `${explorerUrl}/address/${accountAddress}`
  }

  @computed({ keepAlive: true }) get hasura$(): Client {
    return createHasuraClient(this.appEnv$.hasuraUrl)
    // return createHasuraClient("https://hasura.uniwhale.tk/v1/graphql")
  }

  @computed({ keepAlive: true }) get priceHasura$(): Client {
    return createHasuraClient(this.appEnv$.priceHasuraUrl)
  }

  @computed({ keepAlive: true }) get airdropHasura$(): Client {
    return createHasuraClient(this.appEnv$.airdropHasuraUrl)
  }

  @computed({ keepAlive: true })
  get publicJSONRPCProvider$(): ethers.providers.JsonRpcProvider {
    return new ethers.providers.JsonRpcProvider(this.appEnv$.rpcUrl, {
      name: this.appEnv$.chainName,
      chainId: this.appEnv$.chainId,
    })
  }

  @computed({ keepAlive: true })
  get websocketProvider$(): ethers.providers.WebSocketProvider {
    const wsRPCUrl = this.appEnv$.wsRPCUrl
    return new ethers.providers.WebSocketProvider(
      new SturdyWebSocket(wsRPCUrl),
      {
        name: this.appEnv$.chainName,
        chainId: this.appEnv$.chainId,
      },
    )
  }

  #appOpenBlockHeight = new LazyValue(
    () => this.publicJSONRPCProvider$,
    lib => from(lib.getBlockNumber()),
  )
  @computed get appOpenBlockHeight$(): number {
    return this.#appOpenBlockHeight.value$
  }

  @computed({ keepAlive: true })
  get currentBlockObservable$(): Observable<number> {
    const provider = this.websocketProvider$
    return new Observable<number>(subscriber => {
      const listener = (block: number): void => {
        subscriber.next(block)
      }
      provider.on("block", listener)
      return () => {
        provider.removeListener("block", listener)
      }
    }).pipe(
      shareReplay({
        bufferSize: 1,
        refCount: true,
      }),
    )
  }

  #currentBlock = new LazyValue(
    () => this.currentBlockObservable$,
    block$ => block$,
  )

  @computed get currentBlock$(): number {
    return this.#currentBlock.value$
  }

  @computed({ keepAlive: true }) get uniwhaleConfig$(): UniwhaleConfig {
    return UniwhaleConfig__factory.connect(
      this.appEnv$.uniwhaleConfigAddress,
      this.publicJSONRPCProvider$,
    )
  }

  @persist("object") @observable.ref savedConfig?: OnChainConfigType

  #onChainConfig = new LazyValue(() => this.uniwhaleConfig$, fetchOnChainConfig)

  @computed get onChainConfigs$(): OnChainConfigType {
    try {
      return this.#onChainConfig.value$
    } catch (e) {
      if (
        isPromiseLike(e) &&
        this.savedConfig != null &&
        isCompatibleCachedConfig(this.savedConfig)
      ) {
        return this.savedConfig
      }
      throw e
    }
  }

  pyth = new PythModule(this)

  @computed get chainInfo$(): ChainInfo {
    return {
      ...ChainInfoPresets.BSC,
      displayName: this.appEnv$.chainSwitcherName,
    }
  }
}
