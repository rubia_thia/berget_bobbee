import { BigNumber as EthBN } from "ethers"
import { computed, makeObservable } from "mobx"
import { createTransformer } from "mobx-utils"
import { memoizeWith } from "ramda"
import { shareReplay } from "rxjs"
import { MockPyth, MockPyth__factory } from "../contracts/generated"
import { LazyValue } from "../utils/LazyValue"
import { waitFor } from "../utils/waitFor"
import { AppEnvStore } from "./AppEnvStore"
import {
  getPriceId,
  getTokenNames,
  PriceSymbol,
  PythEnv,
  subscribeToPriceId,
} from "./PythModule.service"

export class PythModule {
  constructor(readonly store: AppEnvStore) {
    makeObservable(this)
  }

  @computed get pythEnv$(): PythEnv {
    return this.store.onChainConfigs$.pythEnv
  }

  priceFor = memoizeWith(
    String,
    (priceId: string) =>
      new LazyValue(
        () => [priceId] as const,
        ([priceId]) => this.getCurrentPriceObservable(priceId),
      ),
  )

  getCurrentPriceObservable = memoizeWith(String, (priceId: string) =>
    subscribeToPriceId(this.store.priceHasura$, priceId).pipe(
      shareReplay({
        refCount: true,
        bufferSize: 1,
      }),
    ),
  )

  priceIdFor = createTransformer(
    (priceSymbol: PriceSymbol) => getPriceId(priceSymbol, this.pythEnv$),
    { keepAlive: true },
  )

  priceSymbolFromId = createTransformer(
    (priceId: string): PriceSymbol | undefined => {
      return this.store.onChainConfigs$.approvedPriceSymbols.find(
        a => this.priceIdFor(a) === priceId,
      )
    },
    {
      keepAlive: true,
    },
  )

  tokenSymbolsFor = createTransformer(
    (priceSymbol: PriceSymbol) => getTokenNames(priceSymbol, this.pythEnv$),
    { keepAlive: true },
  )

  async createPriceFeedUpdateDate(priceId: string): Promise<string[]> {
    return waitFor(() => [this.priceFor(priceId).value$.priceData])
  }

  async getUpdateFee(priceData: string[]): Promise<EthBN> {
    return this.mockPyth$.getUpdateFee(priceData.length)
  }

  marketPrecisionFor = createTransformer(
    (priceSymbol: PriceSymbol) =>
      this.store.appEnv$.tradeMarketPrecision[priceSymbol] ?? 2,
    { keepAlive: true },
  )

  @computed({ keepAlive: true }) get mockPyth$(): MockPyth {
    return MockPyth__factory.connect(
      this.store.onChainConfigs$.pyth,
      this.store.publicJSONRPCProvider$,
    )
  }
}
