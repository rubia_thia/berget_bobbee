import { ethers } from "ethers"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { math } from "../../utils/numberHelpers/bigNumberExpressionParser"

export const kThenaRouterAddress = "0xd4ae6eCA985340Dd434D38F470aCCce4DC78D109"
export const kPancakeRouterAddress =
  "0x10ED43C718714eb63d5aA57B78B54704E256024E"
export const kBusdAddress = "0xe9e7CEA3DedcA5984780Bafc599bD69ADd087D56"
export const kUsdtAddress = "0x55d398326f99059fF775485246999027B3197955"
export const kWrappedBnbAddress = "0xbb4cdb9cbd36b01bd1cbaebf2de08d9173bc095c"
export const kWrappedEthAddress = "0x2170Ed0880ac9A755fd29B2688956BD959F933F8"
export const kUnwAddress = "0x5b65cd9feb54f1df3d0c60576003344079f8dc06"
export const kThenaTokenAddress = "0xF4C8E32EaDEC4BFe97E0F595AdD0f4450a863a11"
export const kXCadAddress = "0x431e0cd023a32532bf3969cddfc002c00e98429d"

const contractABI = [
  {
    inputs: [
      {
        internalType: "uint256",
        name: "amountIn",
        type: "uint256",
      },
      {
        internalType: "address[]",
        name: "path",
        type: "address[]",
      },
    ],
    name: "getAmountsOut",
    outputs: [
      {
        internalType: "uint256[]",
        name: "amounts",
        type: "uint256[]",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
]

export async function getUNWPrice(): Promise<BigNumber> {
  const provider = new ethers.providers.JsonRpcProvider(
    "https://bsc-dataseed1.binance.org",
  )
  const pancake = new ethers.Contract(
    kPancakeRouterAddress,
    contractABI,
    provider,
  )
  const input = ethers.utils.parseEther("10")
  const [, , UNWAmountFor10USDT] = await pancake["getAmountsOut"](input, [
    kUsdtAddress,
    kWrappedBnbAddress,
    kUnwAddress,
  ])
  const [, , USDTAmountFo10UNW] = await pancake["getAmountsOut"](input, [
    kUnwAddress,
    kWrappedBnbAddress,
    kUsdtAddress,
  ])
  return math`(${input} / ${UNWAmountFor10USDT} + ${USDTAmountFo10UNW} / ${input}) / ${2}`
}
