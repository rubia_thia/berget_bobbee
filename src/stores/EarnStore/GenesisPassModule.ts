import { ContractTransaction } from "ethers"
import { computed, makeObservable, observable } from "mobx"
import { from } from "rxjs"
import {
  fromContractToNative,
  fromNativeToContract,
} from "../../utils/contractHelpers/bigNumberContracts"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { mathIs } from "../../utils/numberHelpers/bigNumberExpressionParser"
import { LazyValue } from "../utils/LazyValue"
import { waitFor, waitUntil, waitUntilExist$ } from "../utils/waitFor"
import { EarnStore } from "./EarnStore"

export class GenesisPassModule {
  constructor(readonly store: EarnStore) {
    makeObservable(this)
  }

  @observable showStakeConfirmation = false

  @computed get ugpStakedIndex$(): string {
    if (!this.#hasStakeUGP.value$) {
      throw waitUntil(() => this.#hasStakeUGP.value$)
    }
    return "#" + BigNumber.toString(this.#stakedTokenId.value$)
  }

  #ugpBalance = new LazyValue(
    () => ({
      ugp: this.store.contracts.genesisPass$,
      user: this.store.authStore.account$,
    }),
    ({ ugp, user }) => from(ugp.balanceOf(user).then(BigNumber.from)),
  )

  #hasStakeUGP = new LazyValue(
    () => ({
      user: this.store.authStore.account$,
      ugp: this.store.contracts.genesisPass$,
      refresh: this.store.currencyStore.refreshSignal$,
    }),
    ({ ugp, user }) => from(ugp.hasStake(user)),
  )

  #stakedTokenId = new LazyValue(
    () => ({
      user: this.store.authStore.account$,
      ugp: this.store.contracts.genesisPass$,
    }),
    ({ ugp, user }) => from(ugp.getStaked(user).then(BigNumber.from)),
  )

  @computed get canStake$(): boolean {
    return (
      !this.#hasStakeUGP.value$ && mathIs`${this.#ugpBalance.value$} > ${0}`
    )
  }

  #ugpIndexes = new LazyValue(
    () => ({
      ugp: this.store.contracts.genesisPass$,
      amount: BigNumber.toNumber(this.#ugpBalance.value$),
      address: this.store.authStore.account$,
    }),
    ({ ugp, amount, address }) =>
      from(
        Promise.all(
          Array(amount)
            .fill(null)
            .map((_, i) =>
              ugp
                .tokenOfOwnerByIndex(address, i)
                .then(BigNumber.from)
                .then(BigNumber.toNumber),
            ),
        ),
      ),
  )

  @observable selectedUGPToStake?: {
    contractAddress: string
    nftIndex: number
  }

  @computed get availableUGPToStake$(): {
    contractAddress: string
    nftIndex: number
  }[] {
    return this.#ugpIndexes.value$.map((nftIndex: number) => ({
      contractAddress: this.store.contracts.genesisPass$.address,
      nftIndex,
    }))
  }

  @computed get canUnStake$(): boolean {
    return this.#hasStakeUGP.value$
  }

  async stakeUGP(): Promise<ContractTransaction> {
    const ugp = await waitFor(() => this.store.contracts.genesisPass$)
    return await ugp["stake(uint256)"](
      fromNativeToContract(BigNumber.from(this.selectedUGPToStake!.nftIndex), {
        decimals: 0,
      }),
    )
  }

  async unstakeUGP(): Promise<ContractTransaction> {
    const index = await waitFor(() => this.#stakedTokenId.value$)
    const ugp = await waitFor(() => this.store.contracts.genesisPass$)
    return await ugp.unstake(fromNativeToContract(index, { decimals: 0 }))
  }

  #spreadReductionPercentage = new LazyValue(
    () => ({
      oracle: this.store.contracts.oracleAggregator$,
      ugp: this.store.contracts.genesisPass$,
    }),
    ({ oracle, ugp }) =>
      from(
        oracle
          .getUGPDiscount(ugp.address)
          .then(fromContractToNative)
          .catch(() => null),
      ),
  )

  #tradingFeeDiscount = new LazyValue(
    () => ({
      fees: this.store.contracts.fees$,
      ugp: this.store.contracts.genesisPass$,
    }),
    ({ fees, ugp }) =>
      from(
        fees.getUGPFeeDiscPct(ugp.address).then(e => fromContractToNative(e)),
      ),
  )

  @computed get spreadReductionPercentage$(): BigNumber {
    return waitUntilExist$(() => this.#spreadReductionPercentage.value$)
  }

  @computed get tradingFeeDiscountPercentage$(): BigNumber {
    return this.#tradingFeeDiscount.value$
  }

  async addToken(): Promise<void> {
    const lib = await waitFor(() => this.store.authStore.library$)
    const ugp = await waitFor(() => this.store.contracts.genesisPass$)
    await lib.send("wallet_watchAsset", {
      type: "ERC20",
      options: {
        address: ugp.address,
        symbol: "UGP",
        decimals: 0,
      },
    } as any)
  }
}
