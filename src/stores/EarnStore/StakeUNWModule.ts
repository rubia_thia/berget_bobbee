import { ContractTransaction } from "ethers"
import { computed, makeObservable } from "mobx"
import { from } from "rxjs"
import {
  fromContractToNative,
  fromNativeToContract,
} from "../../utils/contractHelpers/bigNumberContracts"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { UniwhaleToken } from "../contracts/generated"
import { ERC20Tokens } from "../CurrencyStore/CurrencyStore.service"
import { MAX_AMOUNT_TO_APPROVE } from "../TradeStore/OrderCreationModule.service"
import { LazyValue } from "../utils/LazyValue"
import { RefreshStore } from "../utils/RefreshStore"
import { waitFor, waitUntilExist$ } from "../utils/waitFor"
import { EarnStore } from "./EarnStore"
import { getUNWPrice } from "./getUNWPrice"

export class StakeUNWModule {
  constructor(readonly store: EarnStore) {
    makeObservable(this)
  }

  #price = new LazyValue(
    () => null,
    () => from(getUNWPrice()),
  )

  @computed get price$(): BigNumber {
    // return BigNumber.from(1)
    return this.#price.value$
  }

  @computed private get uwn$(): UniwhaleToken {
    return this.store.contracts.uniwhaleToken$
  }

  refresh = new RefreshStore()

  #esUNWRewards = new LazyValue(
    () => ({
      user: this.store.authStore.account$,
      unw: this.uwn$,
      esUWN: this.store.contracts.esUniwhaleToken$,
      refresh: this.refresh.signal$,
    }),
    ({ unw, user, esUWN }) =>
      from(unw.getRewards(user, esUWN.address).then(fromContractToNative)),
  )

  #staked = new LazyValue(
    () => ({
      unw: this.uwn$,
      user: this.store.authStore.account$,
      refresh: this.refresh.signal$,
    }),
    ({ unw, user }) => from(unw.getStaked(user).then(fromContractToNative)),
  )

  #totalStaked = new LazyValue(
    () => ({
      unw: this.store.contracts.uniwhaleToken$,
      refresh: this.refresh.signal$,
    }),
    ({ unw }) => from(unw.getTotalStaked().then(fromContractToNative)),
  )
  @computed get totalStaked$(): BigNumber {
    return this.#totalStaked.value$
  }

  #balance = new LazyValue(
    () => ({
      unw: this.uwn$,
      user: this.store.authStore.account$,
      refresh: this.refresh.signal$,
    }),
    ({ unw, user }) => from(unw.balanceOf(user).then(fromContractToNative)),
  )

  @computed get esUNWReward$(): BigNumber {
    return this.#esUNWRewards.value$
  }

  @computed get apr$(): BigNumber {
    return waitUntilExist$(() => this.store.stats$.unwEmissionAPR)
  }

  @computed get staked$(): BigNumber {
    return this.#staked.value$
  }

  @computed get balance$(): BigNumber {
    return this.#balance.value$
  }

  @computed get currentAllowance$(): BigNumber {
    return this.store.currencyStore.allowanceOf$(
      ERC20Tokens.UNW,
      this.store.contracts.uniwhaleToken$.address,
    )
  }

  async approve(): Promise<ContractTransaction> {
    return await this.store.currencyStore
      .getErc20$(ERC20Tokens.UNW)
      .approve(
        this.store.contracts.uniwhaleToken$.address,
        MAX_AMOUNT_TO_APPROVE,
      )
  }

  async stake(amount: BigNumber): Promise<ContractTransaction> {
    return this.uwn$["stake(uint256)"](fromNativeToContract(amount))
  }

  async unstake(amount: BigNumber): Promise<ContractTransaction> {
    return this.uwn$.unstake(fromNativeToContract(amount))
  }

  async addToken(): Promise<void> {
    const lib = await waitFor(() => this.store.authStore.library$)
    const unw = await waitFor(() => this.store.contracts.uniwhaleToken$)
    await lib.send("wallet_watchAsset", {
      type: "ERC20",
      options: {
        address: unw.address,
        symbol: "UNW",
        decimals: 18,
      },
    } as any)
  }
}
