import { ContractTransaction } from "ethers"
import { computed, makeObservable } from "mobx"
import { from } from "rxjs"
import {
  fromContractToNative,
  fromNativeToContract,
} from "../../utils/contractHelpers/bigNumberContracts"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { math } from "../../utils/numberHelpers/bigNumberExpressionParser"
import { props } from "../../utils/promiseHelpers"
import { ERC20Tokens } from "../CurrencyStore/CurrencyStore.service"
import { MAX_AMOUNT_TO_APPROVE } from "../TradeStore/OrderCreationModule.service"
import { LazyValue } from "../utils/LazyValue"
import { RefreshStore } from "../utils/RefreshStore"
import { waitFor, waitUntilExist$ } from "../utils/waitFor"
import { EarnStore } from "./EarnStore"

export class StakeEsUNWModule {
  constructor(readonly store: EarnStore) {
    makeObservable(this)
  }

  refresh = new RefreshStore()

  @computed get price$(): BigNumber {
    return this.store.unw.price$
  }

  @computed get staked$(): BigNumber {
    return this.#staked.value$
  }

  @computed get balance$(): BigNumber {
    return this.#balance.value$
  }

  #feeShareForEsUNW = new LazyValue(
    () => ({
      registry: this.store.contracts.registryCore$,
      revenue: this.store.contracts.revenuePool$,
      esUNW: this.store.contracts.esUniwhaleToken$,
    }),
    ({ esUNW, revenue, registry }) =>
      from(
        props({
          feeFactor: registry.feeFactor().then(fromContractToNative),
          esUNWFeeShare: revenue
            .getShare(esUNW.address)
            .then(fromContractToNative),
        }).then(
          ({ esUNWFeeShare, feeFactor }) =>
            math`${esUNWFeeShare} x ${feeFactor}`,
        ),
      ),
  )

  @computed get feeAPR$(): BigNumber {
    const apr = waitUntilExist$(() => this.store.stats$.esUnwAPR)
    return math`${apr}  * ${this.#feeShareForEsUNW.value$} / ${this.price$}`
  }

  @computed get emissionAPR$(): BigNumber {
    return waitUntilExist$(() => this.store.stats$.esUnwEmissionAPR)
  }

  @computed get apr$(): BigNumber {
    return math`${this.feeAPR$} + ${this.emissionAPR$}`
  }

  #balance = new LazyValue(
    () => ({
      esUNW: this.store.contracts.esUniwhaleToken$,
      user: this.store.authStore.account$,
      refresh: this.refresh.signal$,
    }),
    ({ esUNW, user }) => from(esUNW.balanceOf(user).then(fromContractToNative)),
  )

  #staked = new LazyValue(
    () => ({
      esUNW: this.store.contracts.esUniwhaleToken$,
      user: this.store.authStore.account$,
      refresh: this.refresh.signal$,
    }),
    ({ esUNW, user }) => from(esUNW.getStaked(user).then(fromContractToNative)),
  )

  #totalStaked = new LazyValue(
    () => ({
      esUNW: this.store.contracts.esUniwhaleToken$,
      refresh: this.refresh.signal$,
    }),
    ({ esUNW }) => from(esUNW.getTotalStaked().then(fromContractToNative)),
  )
  @computed get totalStaked$(): BigNumber {
    return this.#totalStaked.value$
  }

  #rewards = new LazyValue(
    () => ({
      user: this.store.authStore.account$,
      revenue: this.store.appEnvStore.onChainConfigs$.revenuePool,
      esUNW: this.store.contracts.esUniwhaleToken$,
      refresh: this.refresh.signal$,
    }),
    ({ user, revenue, esUNW }) =>
      from(
        props({
          revenue: esUNW.getRewards(user, revenue).then(fromContractToNative),
          esUnwReward: esUNW
            .getRewards(user, esUNW.address)
            .then(fromContractToNative),
        }),
      ),
  )

  @computed get revenueReward$(): BigNumber {
    return this.#rewards.value$.revenue
  }

  @computed get esUWNReward$(): BigNumber {
    return this.#rewards.value$.esUnwReward
  }

  @computed get currentAllowance$(): BigNumber {
    return this.store.currencyStore.allowanceOf$(
      ERC20Tokens.esUNW,
      this.store.contracts.esUniwhaleToken$.address,
    )
  }

  async approve(): Promise<ContractTransaction> {
    return await this.store.currencyStore
      .getErc20$(ERC20Tokens.esUNW)
      .approve(
        this.store.contracts.esUniwhaleToken$.address,
        MAX_AMOUNT_TO_APPROVE,
      )
  }

  async stake(amount: BigNumber): Promise<ContractTransaction> {
    return this.store.contracts.esUniwhaleToken$["stake(uint256)"](
      fromNativeToContract(amount),
    )
  }

  async unstake(amount: BigNumber): Promise<ContractTransaction> {
    return this.store.contracts.esUniwhaleToken$.unstake(
      fromNativeToContract(amount),
    )
  }

  async addToken(): Promise<void> {
    const lib = await waitFor(() => this.store.authStore.library$)
    const esUnw = await waitFor(() => this.store.contracts.esUniwhaleToken$)
    await lib.send("wallet_watchAsset", {
      type: "ERC20",
      options: {
        address: esUnw.address,
        symbol: "esUNW",
        decimals: 18,
      },
    } as any)
  }
}
