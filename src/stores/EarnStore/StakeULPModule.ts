import { computed, makeObservable } from "mobx"
import { from } from "rxjs"
import { fromContractToNative } from "../../utils/contractHelpers/bigNumberContracts"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { math } from "../../utils/numberHelpers/bigNumberExpressionParser"
import { props } from "../../utils/promiseHelpers"
import { ulpPrice } from "../LiquidityStore/LiquidityStore.math"
import { LazyValue } from "../utils/LazyValue"
import { waitFor, waitUntilExist$ } from "../utils/waitFor"
import { EarnStore } from "./EarnStore"

export class StakeULPModule {
  constructor(readonly store: EarnStore) {
    makeObservable(this)
  }

  #stakedULP = new LazyValue(
    () => ({
      lp: this.store.contracts.liquidityPool$,
      user: this.store.authStore.account$,
      refresh: this.store.currencyStore.refresh.signal$,
    }),
    ({ lp, user }) => from(lp.getStaked(user).then(fromContractToNative)),
  )

  @computed get staked$(): BigNumber {
    return this.#stakedULP.value$
  }

  #rewards = new LazyValue(
    () => ({
      lp: this.store.contracts.liquidityPool$,
      user: this.store.authStore.account$,
      revenue: this.store.appEnvStore.onChainConfigs$.revenuePool,
      esUNW: this.store.appEnvStore.onChainConfigs$.esUniwhaleToken,
      refresh: this.store.currencyStore.refreshSignal$,
    }),
    ({ lp, user, revenue, esUNW }) =>
      from(
        props({
          revenue: lp.getRewards(user, revenue).then(fromContractToNative),
          unwReward: lp.getRewards(user, esUNW).then(fromContractToNative),
        }),
      ),
  )

  @computed get esUwnReward$(): BigNumber {
    return this.#rewards.value$.unwReward
  }

  @computed get revenueReward$(): BigNumber {
    return this.#rewards.value$.revenue
  }

  @computed get price$(): BigNumber {
    const { baseBalance, totalSupply } =
      this.store.tradeInfo.liquidityInfo.value$
    return ulpPrice(totalSupply, baseBalance)
  }

  #feeShareForLP = new LazyValue(
    () => ({
      registry: this.store.contracts.registryCore$,
      revenue: this.store.contracts.revenuePool$,
      ulp: this.store.contracts.liquidityPool$,
    }),
    ({ ulp, revenue, registry }) =>
      from(
        props({
          feeFactor: registry.feeFactor().then(fromContractToNative),
          lpFeeShare: revenue.getShare(ulp.address).then(fromContractToNative),
        }).then(
          ({ lpFeeShare, feeFactor }) => math`${lpFeeShare} x ${feeFactor}`,
        ),
      ),
  )

  @computed get feeAPR$(): BigNumber {
    const apr = waitUntilExist$(() => this.store.stats$.ulpAPR)
    return math`${apr} * ${this.#feeShareForLP.value$} / ${this.price$}`
  }

  @computed get emissionAPR$(): BigNumber {
    return waitUntilExist$(() => this.store.stats$.ulpEmissionAPR)
  }

  @computed get apr$(): BigNumber {
    return math`${this.emissionAPR$} + ${this.feeAPR$}`
  }

  async addToken(): Promise<void> {
    const lib = await waitFor(() => this.store.authStore.library$)
    const ulp = await waitFor(() => this.store.contracts.liquidityPool$)
    await lib.send("wallet_watchAsset", {
      type: "ERC20",
      options: {
        address: ulp.address,
        symbol: "ULP",
        decimals: 18,
      },
    } as any)
  }
}
