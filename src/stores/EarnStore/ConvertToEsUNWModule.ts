import { ContractTransaction } from "ethers"
import { computed, makeObservable } from "mobx"
import { asyncAction, runAsyncAction } from "../../utils/asyncAction"
import { fromNativeToContract } from "../../utils/contractHelpers/bigNumberContracts"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { ContractStore } from "../contracts/ContractStore"
import { CurrencyStore } from "../CurrencyStore/CurrencyStore"
import { ERC20Tokens } from "../CurrencyStore/CurrencyStore.service"
import { MAX_AMOUNT_TO_APPROVE } from "../TradeStore/OrderCreationModule.service"
import { SuspenseObservable } from "../utils/SuspenseObservable"

export class ConvertToEsUNWModule {
  constructor(
    private currencyStore: CurrencyStore,
    private contracts: ContractStore,
  ) {
    makeObservable(this)
  }

  tokenAmount = new SuspenseObservable<BigNumber>()

  @computed get currentAllowance$(): BigNumber {
    return this.currencyStore.allowanceOf$(
      ERC20Tokens.UNW,
      this.contracts.esUniwhaleToken$.address,
    )
  }

  async approve(): Promise<ContractTransaction> {
    return await this.currencyStore
      .getErc20$(ERC20Tokens.UNW)
      .approve(this.contracts.esUniwhaleToken$.address, MAX_AMOUNT_TO_APPROVE)
  }

  @asyncAction
  async convert(
    amount: BigNumber,
    run = runAsyncAction,
  ): Promise<ContractTransaction> {
    return run(
      this.contracts.esUniwhaleToken$["convert(uint256)"](
        fromNativeToContract(amount),
      ),
    )
  }
}
