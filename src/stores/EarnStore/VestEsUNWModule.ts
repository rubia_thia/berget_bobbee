import { ContractTransaction } from "ethers"
import { computed, makeObservable } from "mobx"
import { from } from "rxjs"
import {
  fromContractToNative,
  fromNativeToContract,
} from "../../utils/contractHelpers/bigNumberContracts"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import {
  math,
  mathIs,
} from "../../utils/numberHelpers/bigNumberExpressionParser"
import { props } from "../../utils/promiseHelpers"
import { ERC20Tokens } from "../CurrencyStore/CurrencyStore.service"
import { MAX_AMOUNT_TO_APPROVE } from "../TradeStore/OrderCreationModule.service"
import { LazyValue } from "../utils/LazyValue"
import { RefreshStore } from "../utils/RefreshStore"
import { SuspenseObservable } from "../utils/SuspenseObservable"
import { EarnStore } from "./EarnStore"

export class VestEsUNWModule {
  constructor(readonly store: EarnStore) {
    makeObservable(this)
  }

  refresh = new RefreshStore()

  @computed get balance$(): BigNumber {
    return this.store.esUnw.balance$
  }

  @computed get lockedAmount$(): BigNumber {
    return this.#locked.value$
  }

  @computed get remainingLockedAmount$(): BigNumber {
    return math`max(${0}, ${this.lockedAmount$} - ${this.vested$} / ${
      this.vestingRate$
    })`
  }

  amountToLock = new SuspenseObservable<BigNumber>()
  amountToUnlock = new SuspenseObservable<BigNumber>()

  #locked = new LazyValue(
    () => ({
      esUNW: this.store.contracts.esUniwhaleToken$,
      user: this.store.authStore.account$,
      refresh: this.refresh.signal$,
    }),
    ({ esUNW, user }) => from(esUNW.getLocked(user).then(fromContractToNative)),
  )

  @computed get vestingRate$(): BigNumber {
    const rate = this.#vestingRate.value$.rate
    if (BigNumber.isZero(rate)) {
      return BigNumber.from(0.000001)
    }
    return rate
  }

  @computed get vestingDuration$(): Duration {
    // return this.#vestingRate.value$.speed
    return { months: 6 }
  }

  #vestingRate = new LazyValue(
    () => ({
      esUNW: this.store.contracts.esUniwhaleToken$,
    }),
    ({ esUNW }) =>
      from(
        props({
          rate: esUNW.vestingRate().then(fromContractToNative),
          speed: esUNW.vestingSpeed().then(fromContractToNative),
        }),
      ),
  )

  #vested = new LazyValue(
    () => ({
      esUNW: this.store.contracts.esUniwhaleToken$,
      user: this.store.authStore.account$,
      refresh: this.refresh.signal$,
    }),
    ({ esUNW, user }) => from(esUNW.getVested(user).then(fromContractToNative)),
  )

  @computed get vested$(): BigNumber {
    return this.#vested.value$
  }

  @computed get hasVested$(): boolean {
    return mathIs`${this.vested$} > ${0.05}`
  }

  @computed get currentAllowance$(): BigNumber {
    return this.store.currencyStore.allowanceOf$(
      ERC20Tokens.esUNW,
      this.store.contracts.esUniwhaleToken$.address,
    )
  }

  async approve(): Promise<ContractTransaction> {
    return await this.store.currencyStore
      .getErc20$(ERC20Tokens.esUNW)
      .approve(
        this.store.contracts.esUniwhaleToken$.address,
        MAX_AMOUNT_TO_APPROVE,
      )
  }

  async lock(amount: BigNumber): Promise<ContractTransaction> {
    return this.store.contracts.esUniwhaleToken$.lock(
      fromNativeToContract(amount),
    )
  }

  async unlock(amount: BigNumber): Promise<ContractTransaction> {
    return this.store.contracts.esUniwhaleToken$.unlock(
      fromNativeToContract(amount),
    )
  }

  async vest(): Promise<ContractTransaction> {
    return this.store.contracts.esUniwhaleToken$["vest()"]()
  }
}
