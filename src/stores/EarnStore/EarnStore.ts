import { ContractTransaction } from "ethers"
import { action, computed, makeObservable, observable } from "mobx"
import { from } from "rxjs"
import { CompoundStrategy } from "../../screens/EarnScreen/types"
import { asyncAction, runAsyncAction } from "../../utils/asyncAction"
import {
  fromContractToNative,
  fromNativeToContract,
} from "../../utils/contractHelpers/bigNumberContracts"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import {
  math,
  mathIs,
} from "../../utils/numberHelpers/bigNumberExpressionParser"
import { TokenInfo } from "../../utils/TokenInfo"
import { TokenInfoPresets } from "../../utils/TokenInfoPresets/TokenInfoPresets"
import { isNotNull } from "../../utils/typeHelpers"
import { AppEnvStore } from "../AppEnvStore/AppEnvStore"
import { AuthStore } from "../AuthStore/AuthStore"
import { ContractStore } from "../contracts/ContractStore"
import { CurrencyStore } from "../CurrencyStore/CurrencyStore"
import { ERC20Tokens } from "../CurrencyStore/CurrencyStore.service"
import { TradeStats } from "../LiquidityStore/LiquidityStore.service"
import { TradeInfoStore } from "../TradeStore/TradeInfoStore"
import { LazyValue } from "../utils/LazyValue"
import { ConvertToEsUNWModule } from "./ConvertToEsUNWModule"
import { GenesisPassModule } from "./GenesisPassModule"
import { StakeEsUNWModule } from "./StakeEsUNWModule"
import { StakeULPModule } from "./StakeULPModule"
import { StakeUNWModule } from "./StakeUNWModule"
import { VestEsUNWModule } from "./VestEsUNWModule"

export class EarnStore {
  constructor(
    readonly appEnvStore: AppEnvStore,
    readonly authStore: AuthStore,
    readonly currencyStore: CurrencyStore,
    readonly contracts: ContractStore,
    readonly tradeInfo: TradeInfoStore,
  ) {
    makeObservable(this)
  }

  ugp = new GenesisPassModule(this)
  ulp = new StakeULPModule(this)
  unw = new StakeUNWModule(this)
  esUnw = new StakeEsUNWModule(this)
  vestEsUnw = new VestEsUNWModule(this)

  @observable unwConvertToEsUNWModule: undefined | ConvertToEsUNWModule

  @action
  onStartConvertUNWToEsUNW(): void {
    this.unwConvertToEsUNWModule = new ConvertToEsUNWModule(
      this.currencyStore,
      this.contracts,
    )
  }
  @action
  onStopConvertUNWToEsUNW(): void {
    this.unwConvertToEsUNWModule = undefined
  }

  @computed get stats$(): TradeStats {
    return this.tradeInfo.tradeStats$
  }

  @computed get usdtRewardToken$(): TokenInfo {
    return this.currencyStore.tokenInfo$(ERC20Tokens.AnchorToken)
  }

  @computed get unwRewardToken$(): TokenInfo {
    return TokenInfoPresets.UNW
  }

  @computed get esUnwRewardToken$(): TokenInfo {
    return TokenInfoPresets.esUNW
  }

  @computed get totalRevenueReward$(): BigNumber {
    return math`${this.ulp.revenueReward$} + ${this.esUnw.revenueReward$}`
  }

  #traderRewards = new LazyValue(
    () => ({
      traderFarm: this.contracts.traderFarm$,
      user: this.authStore.account$,
      esUNW: this.contracts.esUniwhaleToken$,
    }),
    ({ user, traderFarm, esUNW }) =>
      from(
        traderFarm.getRewards(user, esUNW.address).then(fromContractToNative),
      ),
  )

  @computed get traderRewards$(): BigNumber {
    return this.#traderRewards.value$
  }

  @computed get totalStakeEsUWNReward$(): BigNumber {
    return math`${this.ulp.esUwnReward$} + ${this.esUnw.esUWNReward$} + ${this.unw.esUNWReward$}`
  }

  @computed get totalEsUNWReward$(): BigNumber {
    return math`${this.totalStakeEsUWNReward$} + ${this.traderRewards$}`
  }

  @computed get totalUNWReward$(): BigNumber {
    return math`${this.vestEsUnw.vested$}`
  }

  @computed get totalClaimableInUSDT$(): BigNumber {
    return math`${this.totalRevenueReward$} + ${this.totalEsUNWReward$} * ${this.unw.price$}`
  }

  @computed get haveAnyReward$(): boolean {
    return mathIs`${this.totalClaimableInUSDT$} > ${0.1}`
  }

  @observable showClaimConfirmation = false

  @asyncAction
  async claimRewards(run = runAsyncAction): Promise<ContractTransaction> {
    const {
      liquidityPool$: lp,
      stakingHelper$: stakingHelper,
      uniwhaleToken$: unw,
      esUniwhaleToken$: esUNW,
      traderFarm$: traderFarm,
    } = this.contracts
    const contractTransaction = await run(
      stakingHelper.claimMany([
        lp.address,
        unw.address,
        esUNW.address,
        traderFarm.address,
      ]),
    )
    this.showClaimConfirmation = false
    return contractTransaction
  }

  @observable showCompoundConfirmation = false

  @asyncAction
  async compoundRewards(
    strategy: CompoundStrategy,
    run = runAsyncAction,
  ): Promise<ContractTransaction> {
    const {
      liquidityPool$: lp,
      uniwhaleToken$: unw,
      esUniwhaleToken$: esUNW,
      traderFarm$: traderFarm,
      multiCall$: multiCall,
    } = this.contracts
    const hasVested = mathIs`${this.vestEsUnw.vested$} > ${0}`
    const claims = [
      mathIs`${this.traderRewards$} > ${0}`
        ? {
            address: traderFarm.address,
            data: traderFarm.interface.encodeFunctionData("claim()"),
          }
        : null,
      mathIs`${this.ulp.revenueReward$} > ${0} || ${
        this.ulp.esUwnReward$
      } > ${0}`
        ? {
            address: lp.address,
            data: lp.interface.encodeFunctionData("claim()"),
          }
        : null,
      mathIs`${this.unw.esUNWReward$} > ${0}`
        ? {
            address: unw.address,
            data: unw.interface.encodeFunctionData("claim()"),
          }
        : null,
      mathIs`${this.esUnw.revenueReward$} > ${0} || ${
        this.esUnw.esUWNReward$
      } > ${0}`
        ? {
            address: esUNW.address,
            data: esUNW.interface.encodeFunctionData("claim()"),
          }
        : null,
      hasVested && strategy.unw != null
        ? {
            address: esUNW.address,
            data: esUNW.interface.encodeFunctionData("vest()"),
          }
        : null,
      strategy.usdt === "stake" &&
      mathIs`${this.ulp.revenueReward$} > ${0} || ${
        this.esUnw.revenueReward$
      } > ${0}`
        ? {
            address: lp.address,
            data: lp.interface.encodeFunctionData("mintAndStake(uint256)", [
              fromNativeToContract(
                math`${this.ulp.revenueReward$} + ${this.ulp.revenueReward$}`,
              ),
            ]),
          }
        : null,
      hasVested && strategy.unw === "stake"
        ? {
            address: unw.address,
            data: unw.interface.encodeFunctionData("stake(address,uint256)", [
              this.authStore.account$,
              fromNativeToContract(this.vestEsUnw.vested$),
            ]),
          }
        : null,
      hasVested && strategy.unw === "stakeAsEsUnw"
        ? {
            address: esUNW.address,
            data: esUNW.interface.encodeFunctionData("convertAndStake", [
              this.authStore.account$,
              fromNativeToContract(this.vestEsUnw.vested$),
            ]),
          }
        : null,
      strategy.esUNW === "stake"
        ? {
            address: esUNW.address,
            data: esUNW.interface.encodeFunctionData("stake(address,uint256)", [
              this.authStore.account$,
              fromNativeToContract(BigNumber.from(1)),
            ]),
          }
        : null,
    ].filter(isNotNull)
    // await esUNW.approve(multiCall.address, MAX_AMOUNT_TO_APPROVE)
    // await esUNW.approve(esUNW.address, MAX_AMOUNT_TO_APPROVE)
    const contractTransaction = await run(
      // esUNW["stake(uint256)"](fromNativeToContract(BigNumber.from(1))),
      multiCall.multiCall(
        claims.map(a => a.address),
        claims.map(a => a.data),
      ),
    )
    this.showCompoundConfirmation = false
    return contractTransaction
  }
}
