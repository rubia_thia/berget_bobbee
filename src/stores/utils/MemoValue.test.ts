import { noop } from "lodash"
import { observable, reaction, runInAction } from "mobx"
import { defer, sleep } from "../../utils/promiseHelpers"
import { LazyValue } from "./LazyValue"
import { MemoValue } from "./MemoValue"

const disposeFns = new Set<() => void>()
const subscribeMemoValue = (memoValue: MemoValue<any>): void => {
  disposeFns.add(
    reaction(() => memoValue.previousValue, noop, { onError: noop }),
  )
}
const subscribeLazyValue = (lazyValue: LazyValue<any, any>): void => {
  disposeFns.add(reaction(() => lazyValue.value$, noop, { onError: noop }))
}
afterEach(() => {
  disposeFns.forEach(f => f())
  disposeFns.clear()
})

describe("MemoValue", () => {
  let ob = observable({ value: 0 })

  beforeEach(() => {
    ob = observable({ value: 0 })
  })

  it("works", async () => {
    const m = new MemoValue(() => ob.value)

    subscribeMemoValue(m)

    expect(m.previousValue).toBeUndefined()

    runInAction(() => {
      ob.value = 1
    })
    await sleep(0)
    expect(m.previousValue).toBe(0)

    runInAction(() => {
      ob.value = 2
    })
    expect(m.previousValue).toBe(1)
  })

  it("work with LazyValue", async () => {
    let deferred = defer<number>()
    const returnPromise = jest.fn(() => {
      deferred = defer()
      return deferred.promise
    }) as any
    const lazy = new LazyValue(() => ob.value, returnPromise)
    const m = new MemoValue(() => lazy.value$)

    subscribeMemoValue(m)
    subscribeLazyValue(lazy)

    expect(m.previousValue).toBeUndefined()

    deferred.resolve(2)
    await sleep(0)
    expect(lazy.value$).toBe(2)
    expect(m.previousValue).toBeUndefined()

    runInAction(() => {
      ob.value = 3
    })
    deferred.resolve(4)
    await sleep(0)
    expect(lazy.value$).toBe(4)
    expect(m.previousValue).toBe(2)

    runInAction(() => {
      ob.value = 5
    })
    deferred.resolve(6)
    await sleep(0)
    expect(lazy.value$).toBe(6)
    expect(m.previousValue).toBe(4)
  })
})
