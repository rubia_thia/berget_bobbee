import { action, computed, makeObservable, observable } from "mobx"
import {
  PaginationInfo,
  PaginationPatch,
} from "../../components/Pagination/Pagination"

export class PaginationStore<T> {
  @observable current = 1
  @observable pageSize = 10

  constructor(private getData$: () => T[], options?: { pageSize?: number }) {
    makeObservable(this)
    this.pageSize = options?.pageSize ?? 10
  }

  @computed get paginationInfo$(): PaginationInfo {
    return {
      current: this.current,
      pageSize: this.pageSize,
      total: this.getData$().length,
    }
  }

  @computed get records$(): T[] {
    return this.getData$().slice(
      (this.current - 1) * this.pageSize,
      this.current * this.pageSize,
    )
  }

  @action
  onPaginationChange = (patch: PaginationPatch): void => {
    this.current = patch.current
  }
}
