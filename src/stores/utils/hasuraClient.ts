import type { Client, OperationResult } from "@urql/core"
import {
  createClient,
  dedupExchange,
  fetchExchange,
  subscriptionExchange,
} from "@urql/core"
import { cacheExchange } from "@urql/exchange-graphcache"
import { ethers } from "ethers"
import { from, map, Observable } from "rxjs"
import { SubscriptionClient } from "subscriptions-transport-ws"
import { Source, toObservable } from "wonka"

export const createHasuraClient = (url: string): Client => {
  const subscriptionClient = new SubscriptionClient(
    url.replace(/^http/, "ws"),
    {
      reconnect: true,
    },
  )
  return createClient({
    url,
    fetchOptions: {},
    exchanges: [
      dedupExchange,
      cacheExchange({
        keys: {
          pyth_prices_1m: () => null,
          pyth_prices_15m: () => null,
          pyth_prices_1h: () => null,
          pyth_prices_1d: () => null,
          uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent: data =>
            data["txHash"] as string,
          uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent: data =>
            data["txHash"] as string,
          uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent: data =>
            data["txHash"] as string,
          uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent: data =>
            data["txHash"] as string,
          uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent: data =>
            data["txHash"] as string,
          uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent: data =>
            data["txHash"] as string,
          uniwhale_v1_bsc_LimitBook_evt_CloseLimitOrderEvent: data =>
            data["txHash"] as string,
          uniwhale_v1_bsc_LimitBook_evt_PartialCloseLimitOrderEvent: data =>
            data["txHash"] as string,
          latest_trade_stats: () => null,
          latest_trade_stats_7d: () => null,

          uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent_aggregate: () =>
            null,
          uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent_aggregate_fields:
            () => null,
          uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent_sum_fields: () =>
            null,

          uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent_aggregate: () =>
            null,
          uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent_aggregate_fields:
            () => null,
          uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent_sum_fields:
            () => null,

          cp_referral_counter_aggregate: () => null,
          cp_referral_counter_aggregate_fields: () => null,
          cp_referral_counter_sum_fields: () => null,

          cp_referral_summary_aggregate: () => null,
          cp_referral_summary_aggregate_fields: () => null,
          cp_referral_summary_sum_fields: () => null,
        },
        updates: {},
      }),
      fetchExchange,
      subscriptionExchange({
        forwardSubscription: operation => subscriptionClient.request(operation),
      }),
    ],
  })
}

export function fromUrqlSource<Data>(
  source: Source<OperationResult<Data>>,
): Observable<Data>
export function fromUrqlSource<Data, TOutput>(
  source: Source<OperationResult<Data>>,
  getData: (result: { data: Data }) => TOutput,
): Observable<TOutput>
export function fromUrqlSource<Data, TOutput>(
  source: Source<OperationResult<Data>>,
  getData?: (result: { data: Data }) => TOutput,
): Observable<TOutput | Data> {
  const _getData = getData ?? (result => result.data as Data)

  return from(
    new Observable<OperationResult<Data>>(subscriber => {
      const subscription = toObservable(source).subscribe(subscriber)
      return () => subscription.unsubscribe()
    }) as Observable<OperationResult<Data>>,
  ).pipe(
    map(data => {
      if (data.error) {
        throw data.error
      }
      if (data.data == null) {
        throw new Error("Request returned empty payload", {
          cause: "fromUrqlSource",
        })
      }
      return _getData({ data: data.data })
    }),
  )
}

export type Hexlifible = Parameters<typeof ethers.utils.hexlify>[0]
export function ethArrayBufferToHasuraAddress(input: Hexlifible): string {
  return ethAddressToHasuraAddress(ethers.utils.hexlify(input))
}
export function hasuraAddressToEthArrayBuffer(input: string): Uint8Array {
  return new Buffer(ethers.utils.stripZeros(hasuraAddressToEth(input)))
}

export function ethAddressToHasuraAddress(input: string): string {
  return input.replace(/^0x/, "\\x").toLowerCase()
}

export function hasuraAddressToEth(input: string): string {
  return input.replace(/\\x/, "0x").toLowerCase()
}
