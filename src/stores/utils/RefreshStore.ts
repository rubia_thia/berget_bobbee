import { computed, makeObservable } from "mobx"
import { SuspenseObservable } from "./SuspenseObservable"

export class RefreshStore {
  constructor() {
    makeObservable(this)
  }
  #manualRefreshBalanceAndAllowance = new SuspenseObservable(Math.random())
  refresh(): void {
    this.#manualRefreshBalanceAndAllowance.set(Math.random())
  }
  @computed get signal$(): number {
    return this.#manualRefreshBalanceAndAllowance.read$
  }
}
