import {
  action,
  computed,
  IReactionOptions,
  makeObservable,
  observable,
  onBecomeObserved,
  onBecomeUnobserved,
  reaction,
} from "mobx"
import { Observable } from "rxjs"
import { defer, isPromiseLike } from "../../utils/promiseHelpers"

type LazyValueState<T> =
  | { type: "idle"; defer: defer.Deferred<void> }
  | { type: "value"; value: T }
  | { type: "error"; error: any }

export interface SharedLazyValue<Value> {
  hasValue: boolean

  value$: Value

  triggerUpdate(): Promise<void>
}
export function createStaticLazyValue<V>(v: V): SharedLazyValue<V> {
  return {
    hasValue: true,
    value$: v,
    triggerUpdate: async () => {},
  }
}

export class LazyValue<Value, Params> implements SharedLazyValue<Value> {
  @observable.ref private state: LazyValueState<Value> = {
    type: "idle",
    defer: defer(),
  }

  private readonly traceLog: (msg: string, ...extraArgs: any[]) => void

  @computed
  get hasValue(): boolean {
    return this.state.type === "value"
  }

  get value$(): Value {
    if (this.state.type === "idle") {
      throw this.state.defer.promise
    }
    if (this.state.type === "error") {
      throw this.state.error
    }
    return this.state.value
  }

  constructor(
    private readonly paramsGetter$: () => Params,
    private readonly fetchValue: (params: Params) => Observable<Value>,
    options?: {
      resetOnDepsChange?: boolean
      reactionOptions?: IReactionOptions<Params, boolean>
      debug?: boolean | string
    },
  ) {
    makeObservable(this)

    const trace = (msg: string, ...extraArgs: any[]): void => {
      if (options?.debug) {
        console.log(`${options?.debug || "LazyValue"} ${msg}`, ...extraArgs)
      }
    }
    this.traceLog = trace

    trace("constructor")

    let dispose: () => void
    onBecomeObserved(this, "state", async () => {
      trace("become observed")
      dispose = reaction(
        () => this.paramsGetter$(),
        params => {
          trace("params changed", params)
          if (options?.resetOnDepsChange) {
            void this.onReinitialize()
          } else {
            void this.updateValue(params)
          }
        },
        {
          ...options?.reactionOptions,
          fireImmediately: false,
          onError: error => {
            if (isPromiseLike(error)) {
              this.onReinitialize()
            } else {
              trace("error", error)
            }
          },
        },
      )
      // We need awaitOnThrownPromise to catch the cases
      // where this.paramsGetter$() throws a promise
      // and we need to await on it to reinitialize
      void awaitOnThrownPromise(() => this.triggerUpdate()).catch(
        action(e => {
          this.state = { type: "error", error: e }
        }),
      )
    })
    onBecomeUnobserved(this, "state", () => {
      trace("become unobserved")
      this.unsubscribeLatestFetchValueCall?.("unobserve")
      dispose?.()
    })
  }

  private unsubscribeLatestFetchValueCall?: (
    scene: "cleanup" | "unobserve",
  ) => void
  private updateValue = (params: Params): Promise<void> => {
    this.unsubscribeLatestFetchValueCall?.("cleanup")

    const source = this.fetchValue(params)

    const handleResult = this.updateValueFromObservable(source)

    this.unsubscribeLatestFetchValueCall = handleResult.unsubscribe
    return handleResult.promise
  }

  private updateValueFromObservable(
    observable: Observable<Value>,
  ): FetchValueSourceHandleResult {
    const deferred = defer()
    this.traceLog("subscribed")
    const sub = observable.subscribe({
      next: value => {
        this.traceLog("next", value)
        this.onReceiveValue(value)
        deferred.resolve()
      },
      error: err => {
        this.traceLog("error", err)
        this.onEncounterError(err)
        deferred.reject(err)
      },
    })
    return {
      promise: deferred.promise,
      unsubscribe: (scene: "cleanup" | "unobserve") => {
        if (scene === "cleanup") {
          deferred.resolve()
          sub.unsubscribe()
        } else if (scene === "unobserve") {
          // we want to wait for the first promise to result before unsubscribing
          deferred.promise.finally(() => {
            sub.unsubscribe()
          })
        }
      },
    }
  }

  @action private onReinitialize(): void {
    if (this.state.type !== "idle") {
      this.state = { type: "idle", defer: defer() }
    }
  }

  @action private onReceiveValue(value: Value): void {
    if (this.state.type === "idle") {
      this.state.defer.resolve()
    }
    this.state = { type: "value", value }
  }

  @action private onEncounterError(error: any): void {
    if (this.state.type === "idle") {
      this.state.defer.resolve()

      /**
       * only goto error state if previous state is idle
       * we don't want UI to go from success to error on updates like block height change
       */
      this.state = { type: "error", error }
    }
  }

  async triggerUpdate(): Promise<void> {
    await this.updateValue(this.paramsGetter$())
  }
}

interface FetchValueSourceHandleResult {
  promise: Promise<void>
  unsubscribe: (scene: "cleanup" | "unobserve") => void
}

async function awaitOnThrownPromise(
  action: () => void | Promise<void>,
): Promise<void> {
  try {
    await action()
  } catch (error) {
    if (error instanceof Promise) {
      error.finally(() => awaitOnThrownPromise(action))
    } else {
      throw error
    }
  }
}
