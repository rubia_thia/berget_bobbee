export type Result<TOK, TError> = Result.OK<TOK> | Result.Error<TError>

export namespace Result {
  export interface OK<T> {
    type: "ok"
    payload: T
  }

  export interface Error<T> {
    type: "error"
    payload: T
  }

  export const isResult = <TOK, TErr>(
    input: any,
  ): input is Result<TOK, TErr> => {
    if (input == null) return false
    if (typeof input !== "object") return false
    if (input.type !== "ok" && input.type !== "error") return false
    if (!("payload" in input)) return false
    return true
  }

  export const ok = <TOK>(payload: TOK): Result.OK<TOK> => ({
    type: "ok",
    payload,
  })

  export const error = <TError>(payload: TError): Result.Error<TError> => ({
    type: "error",
    payload,
  })

  export const maybeValue = <TOK, TError>(
    res: Result<TOK, TError>,
  ): undefined | TOK => {
    if (res.type === "ok") return res.payload
    return
  }

  export const maybeError = <TOK, TError>(
    res: Result<TOK, TError>,
  ): undefined | TError => {
    if (res.type === "error") return res.payload
    return
  }

  interface CurriedMapFn {
    <TOKInput, TOKOutput, TError>(mapping: (payload: TOKInput) => TOKOutput): (
      res: Result<TOKInput, TError>,
    ) => Result<TOKOutput, TError>
    <TOKInput, TOKOutput, TError>(
      mapping: (payload: TOKInput) => TOKOutput,
      res: Result<TOKInput, TError>,
    ): Result<TOKOutput, TError>
  }
  export const map: CurriedMapFn = <TOKInput, TOKOutput, TError>(
    mapping: (payload: TOKInput) => TOKOutput,
    res?: Result<TOKInput, TError>,
  ): any => {
    if (res == null) {
      return (res: Result<TOKInput, TError>) => realMap(mapping, res)
    } else {
      return realMap(mapping, res)
    }

    function realMap(
      mapping: (payload: TOKInput) => TOKOutput,
      res: Result<TOKInput, TError>,
    ): Result<TOKOutput, TError> {
      if (res.type === "error") return res
      return Result.ok(mapping(res.payload))
    }
  }

  interface CurriedFlatMapFn {
    <TOKInput, TOKOutput, TError>(
      mapping: (payload: TOKInput) => Result<TOKOutput, TError>,
    ): (res: Result<TOKInput, TError>) => Result<TOKOutput, TError>
    <TOKInput, TOKOutput, TError>(
      mapping: (payload: TOKInput) => Result<TOKOutput, TError>,
      res: Result<TOKInput, TError>,
    ): Result<TOKOutput, TError>
  }
  export const flatMap: CurriedFlatMapFn = <TOKInput, TOKOutput, TError>(
    mapping: (payload: TOKInput) => Result<TOKOutput, TError>,
    res?: Result<TOKInput, TError>,
  ): any => {
    if (res == null) {
      return (res: Result<TOKInput, TError>) => realMap(mapping, res)
    } else {
      return realMap(mapping, res)
    }

    function realMap(
      mapping: (payload: TOKInput) => Result<TOKOutput, TError>,
      res: Result<TOKInput, TError>,
    ): Result<TOKOutput, TError> {
      if (res.type === "error") return res
      return mapping(res.payload)
    }
  }
}
