import { create } from "mobx-persist"
import { MMKV } from "react-native-mmkv"

export const mmkvStorage = new MMKV()

const mmkvStorageLayer = {
  clear: () => {
    mmkvStorage.clearAll()
    return Promise.resolve()
  },
  setItem: (key: string, value: string) => {
    mmkvStorage.set(key, value)
    return Promise.resolve(true)
  },
  getItem: (key: string) => {
    const value = mmkvStorage.getString(key)
    return Promise.resolve(value ?? null)
  },
  removeItem: (key: string) => {
    mmkvStorage.delete(key)
    return Promise.resolve()
  },
}

export const hydrate = create({
  storage: mmkvStorageLayer,
  jsonify: true,
})

export { persist } from "mobx-persist"
