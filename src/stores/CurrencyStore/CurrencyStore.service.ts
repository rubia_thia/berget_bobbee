export enum ERC20Tokens {
  AnchorToken = "AnchorToken", // tUSD
  TBUSD = "TBUSD",
  TUSDT = "TUSDT",
  BUSD = "BUSD",
  USDC = "USDC",
  LiquidityPool = "LiquidityPool",
  UNW = "UNW",
  esUNW = "esUNW",
}

export type StableToken =
  | ERC20Tokens.AnchorToken
  | ERC20Tokens.TBUSD
  | ERC20Tokens.TUSDT
  | ERC20Tokens.BUSD
  | ERC20Tokens.USDC
