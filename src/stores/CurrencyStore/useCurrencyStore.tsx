import { FCC } from "../../utils/reactHelpers/types"
import { useCreation } from "../../utils/reactHelpers/useCreation"
import { useAppEnvStore } from "../AppEnvStore/useAppEnvStore"
import { useAuthStore } from "../AuthStore/useAuthStore"
import { createStore } from "../utils/createStore"
import { CurrencyStore } from "./CurrencyStore"

const { Provider, useStore } = createStore<CurrencyStore>("CurrencyStore")

export const useCurrencyStore = useStore.bind(null)

export const CurrencyStoreProvider: FCC = props => {
  const appEnvStore = useAppEnvStore()
  const authStore = useAuthStore()
  const store = useCreation(
    () => new CurrencyStore(appEnvStore, authStore),
    [appEnvStore, authStore],
  )
  return <Provider store={store}>{props.children}</Provider>
}
