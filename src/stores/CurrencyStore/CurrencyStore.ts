import { computed, makeObservable } from "mobx"
import { computedFn, createTransformer } from "mobx-utils"
import { memoizeWith } from "ramda"
import { from } from "rxjs"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { math } from "../../utils/numberHelpers/bigNumberExpressionParser"
import { TokenInfo } from "../../utils/TokenInfo"
import { TokenInfoPresets } from "../../utils/TokenInfoPresets/TokenInfoPresets"
import { assertNever } from "../../utils/types"
import { AppEnvStore } from "../AppEnvStore/AppEnvStore"
import { PriceSymbol } from "../AppEnvStore/PythModule.service"
import { AuthStore } from "../AuthStore/AuthStore"
import { ERC20, ERC20__factory } from "../contracts/generated"
import { LazyValue } from "../utils/LazyValue"
import { RefreshStore } from "../utils/RefreshStore"
import { ERC20Tokens } from "./CurrencyStore.service"

export class CurrencyStore {
  constructor(
    readonly appEnvStore: AppEnvStore,
    readonly authStore: AuthStore,
  ) {
    makeObservable(this)
  }

  ethBalance = new LazyValue(
    () => [this.authStore.library$, this.authStore.account$] as const,
    ([lib, acc]) =>
      from(lib.getBalance(acc).then(BigNumber.leftMoveDecimals(18))),
  )

  getErc20$ = createTransformer(
    (token: ERC20Tokens): ERC20 =>
      ERC20__factory.connect(
        this.contractAddress$(token),
        this.authStore.signerOrLibrary$,
      ),
    {
      keepAlive: true,
    },
  )

  refreshBalanceAndAllowance(): void {
    this.refresh.refresh()
  }

  refresh = new RefreshStore()

  @computed get refreshSignal$(): number {
    return this.refresh.signal$
  }

  #balanceOf = memoizeWith(String, (token: ERC20Tokens) => {
    return new LazyValue(
      () =>
        [
          this.getErc20$(token),
          this.authStore.account$,
          this.refreshSignal$,
        ] as const,
      ([erc20, acc]) => from(erc20.balanceOf(acc).then(BigNumber.from)),
    )
  })

  #allowanceOf = memoizeWith(
    (a, b) => `${a}-${b}`,
    (token: ERC20Tokens, address: string) => {
      return new LazyValue(
        () =>
          [
            this.getErc20$(token),
            this.authStore.account$,
            address,
            this.refreshSignal$,
          ] as const,
        ([erc20, owner, acc]) =>
          from(erc20.allowance(owner, acc).then(BigNumber.from)),
      )
    },
  )

  allowanceOf$ = computedFn((token: ERC20Tokens, address: string) => {
    const allowance = this.#allowanceOf(token, address).value$
    return math`${allowance} << ${this.decimalOf(token).value$}`
  })

  decimalOf = memoizeWith(String, (token: ERC20Tokens) => {
    return new LazyValue(
      () => this.getErc20$(token),
      erc20 => from(erc20.decimals()),
    )
  })

  balance$ = createTransformer(
    (token: ERC20Tokens) =>
      math`${this.#balanceOf(token).value$} << ${this.decimalOf(token).value$}`,
  )

  contractAddress$ = createTransformer((token: ERC20Tokens) => {
    if (token === ERC20Tokens.AnchorToken) {
      return this.appEnvStore.onChainConfigs$.anchorToken
    }
    if (token === ERC20Tokens.BUSD) {
      return this.appEnvStore.onChainConfigs$.BUSDToken
    }
    if (token === ERC20Tokens.USDC) {
      return this.appEnvStore.onChainConfigs$.USDCToken
    }
    if (token === ERC20Tokens.TUSDT) {
      return this.appEnvStore.onChainConfigs$.tUSDTToken
    }
    if (token === ERC20Tokens.TBUSD) {
      return this.appEnvStore.onChainConfigs$.tBUSDToken
    }
    if (token === ERC20Tokens.LiquidityPool) {
      return this.appEnvStore.onChainConfigs$.liquidityPool
    }
    if (token === ERC20Tokens.UNW) {
      return this.appEnvStore.onChainConfigs$.uniwhaleToken
    }
    if (token === ERC20Tokens.esUNW) {
      return this.appEnvStore.onChainConfigs$.esUniwhaleToken
    }
    assertNever(token)
  })

  @computed get nativeTokenSymbol$(): string {
    return this.appEnvStore.appEnv$.nativeCurrency.symbol
  }

  baseTokenInfo$ = createTransformer((priceSymbol: PriceSymbol): TokenInfo => {
    const [symbol] = this.appEnvStore.pyth.tokenSymbolsFor(priceSymbol)
    return {
      id: symbol,
      precision: 5,
      displayName: symbol,
      icon: TokenInfoPresets.MockUSDC.icon,
    }
  })

  tokenInfo$ = createTransformer((token: ERC20Tokens): TokenInfo => {
    if (token === ERC20Tokens.AnchorToken) {
      return {
        id: ERC20Tokens.AnchorToken,
        precision: TokenInfoPresets.MockUSDC.precision,
        displayName: this.appEnvStore.onChainConfigs$.anchorTokenSymbol,
        icon: TokenInfoPresets.MockUSDT.icon,
      }
    }
    if (token === ERC20Tokens.BUSD) {
      return {
        id: ERC20Tokens.BUSD,
        precision: TokenInfoPresets.MockBUSD.precision,
        displayName: this.appEnvStore.onChainConfigs$.BUSDTokenSymbol,
        icon: TokenInfoPresets.MockUSDT.icon,
      }
    }
    if (token === ERC20Tokens.USDC) {
      return {
        id: ERC20Tokens.USDC,
        precision: TokenInfoPresets.MockUSDC.precision,
        displayName: this.appEnvStore.onChainConfigs$.USDCTokenSymbol,
        icon: TokenInfoPresets.MockUSDC.icon,
      }
    }
    if (token === ERC20Tokens.TBUSD) {
      return {
        id: ERC20Tokens.TBUSD,
        precision: TokenInfoPresets.MockBUSD.precision,
        displayName: this.appEnvStore.onChainConfigs$.tBUSDTokenSymbol,
        icon: TokenInfoPresets.MockBUSD.icon,
      }
    }
    if (token === ERC20Tokens.TUSDT) {
      return {
        id: ERC20Tokens.TUSDT,
        precision: TokenInfoPresets.MockUSDT.precision,
        displayName: this.appEnvStore.onChainConfigs$.tUSDTTokenSymbol,
        icon: TokenInfoPresets.MockUSDT.icon,
      }
    }
    if (token === ERC20Tokens.LiquidityPool) {
      return TokenInfoPresets.ULP
    }
    if (token === ERC20Tokens.UNW) {
      return TokenInfoPresets.UNW
    }
    if (token === ERC20Tokens.esUNW) {
      return TokenInfoPresets.esUNW
    }
    assertNever(token)
  })
}
