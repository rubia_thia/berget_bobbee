import { ContractTransaction } from "ethers"
import { computed, makeObservable } from "mobx"
import { from, of } from "rxjs"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import {
  math,
  mathIs,
} from "../../utils/numberHelpers/bigNumberExpressionParser"
import { TokenInfoPresets } from "../../utils/TokenInfoPresets/TokenInfoPresets"
import { assertNever } from "../../utils/types"
import { AppEnvStore } from "../AppEnvStore/AppEnvStore"
import { AuthStore } from "../AuthStore/AuthStore"
import { ContractStore } from "../contracts/ContractStore"
import { CurrencyStore } from "../CurrencyStore/CurrencyStore"
import { TradeInfoStore } from "../TradeStore/TradeInfoStore"
import { LazyValue } from "../utils/LazyValue"
import { RefreshStore } from "../utils/RefreshStore"
import { getAirdropDetail } from "./AirdropStore.onChain.service"
import { getParticipantCount, getQuestScores } from "./AirdropStore.service"
import { MainnetContributorModule } from "./MainnetContributorModule"
import { TestnetContributorModule } from "./TestnetContributorModule"
import { UGPModule } from "./UGPModule"

export type Claimable =
  | { type: "unw" | "esuwn" }
  | { type: "ugp"; index: number }
  | { type: "poap"; index: number }

export class AirdropStore {
  constructor(
    readonly appEnvStore: AppEnvStore,
    readonly authStore: AuthStore,
    readonly currencyStore: CurrencyStore,
    readonly contracts: ContractStore,
    readonly tradeInfoStore: TradeInfoStore,
  ) {
    makeObservable(this)

    this.mainnetContributorModule = new MainnetContributorModule(
      this.authStore,
      this.appEnvStore,
    )
  }

  readonly lpTokenInfo = TokenInfoPresets.ULP

  currentTotalLiquidity = new LazyValue(
    () => [this.tradeInfoStore.liquidityInfo.value$] as const,
    ([liquidityInfo]) => of(liquidityInfo.baseBalance),
  )

  @computed get targetTotalLiquidity$(): BigNumber {
    return /* TODO */ BigNumber.from(5_000_000)
  }

  participantCount = new LazyValue(
    () => ({
      client: this.appEnvStore.airdropHasura$,
    }),
    ({ client }) => getParticipantCount(client),
  )

  #questScores = new LazyValue(
    () => ({
      client: this.appEnvStore.airdropHasura$,
      walletAddress: this.authStore.account$,
    }),
    ({ client, walletAddress }) => getQuestScores(client, walletAddress),
  )

  @computed get currentAccountAirdropUNWCount$(): BigNumber {
    return BigNumber.sum([
      this.mainnetContributorModule.earnedUNWAsLiquidityProvider$,
      this.mainnetContributorModule.earnedUNWAsTrader$,
      this.ugpModule.totalEarnedUNW$,
      this.testnetContributorModule.totalEarnedUNW$,
    ])
  }

  refresh = new RefreshStore()
  #onChainAirdropInfo = new LazyValue(
    () => ({
      helper: this.contracts.airdropHelper$,
      address: this.authStore.account$,
      ugp: this.contracts.genesisPass$,
      testnetPOAP: this.contracts.testnetPOAP$,
      refresh: this.refresh.signal$,
    }),
    a => from(getAirdropDetail(a)),
  )

  ugpModule = new UGPModule(this.#questScores, this.#onChainAirdropInfo, this)
  mainnetContributorModule: MainnetContributorModule
  testnetContributorModule = new TestnetContributorModule(this.#questScores)

  @computed get remainingAirdrop$(): BigNumber {
    return math`${this.remainingUNWAirdrop$} + ${this.remainingUGPAirdrop$} + ${this.remainingEsUNWAirdrop$} + ${this.remainingPOAPAirdrop$}`
  }

  @computed get remainingUNWAirdrop$(): BigNumber {
    const { claimableCyclesByHolder, claimedCyclesByHolder, endCycle } =
      this.#onChainAirdropInfo.value$
    return math`(${endCycle} - ${claimedCyclesByHolder}) * ${claimableCyclesByHolder}`
  }

  @computed get remainingUGPAirdrop$(): BigNumber {
    const { endCycle, ugpInfos } = this.#onChainAirdropInfo.value$
    return BigNumber.sum(
      ugpInfos.map(
        ugp =>
          math`(${endCycle} - ${ugp.claimedToCycle}) * ${ugp.claimablePerCycle}`,
      ),
    )
  }

  @computed get remainingPOAPAirdrop$(): BigNumber {
    const { endCycle, testnetPOAPInfos } = this.#onChainAirdropInfo.value$
    return BigNumber.sum(
      testnetPOAPInfos.map(
        ugp =>
          math`(${endCycle} - ${ugp.claimedToCycle}) * ${ugp.claimablePerCycle}`,
      ),
    )
  }

  @computed get remainingEsUNWAirdrop$(): BigNumber {
    const {
      claimedCyclesEscrowedByHolder,
      claimableCyclesEscrowedByHolder,
      endCycle,
    } = this.#onChainAirdropInfo.value$
    return math`(${endCycle} - ${claimedCyclesEscrowedByHolder}) * ${claimableCyclesEscrowedByHolder}`
  }

  @computed get EsUNWAirdropToClaim$(): BigNumber {
    const {
      claimedCyclesEscrowedByHolder,
      claimableCyclesEscrowedByHolder,
      currenCycle,
    } = this.#onChainAirdropInfo.value$
    return math`(${currenCycle} - ${claimedCyclesEscrowedByHolder}) * ${claimableCyclesEscrowedByHolder}`
  }

  @computed get UNWAirdropToClaim$(): BigNumber {
    const { claimableCyclesByHolder, claimedCyclesByHolder, currenCycle } =
      this.#onChainAirdropInfo.value$
    return math`(${currenCycle} - ${claimedCyclesByHolder}) * ${claimableCyclesByHolder}`
  }

  @computed get UGPAirdropToClaim$(): BigNumber {
    const { currenCycle, ugpInfos } = this.#onChainAirdropInfo.value$
    return BigNumber.sum(
      ugpInfos.map(
        i =>
          math`(${currenCycle} - ${i.claimedToCycle}) * ${i.claimablePerCycle}`,
      ),
    )
  }

  @computed get POAPAirdropToClaim$(): BigNumber {
    const { currenCycle, testnetPOAPInfos } = this.#onChainAirdropInfo.value$
    return BigNumber.sum(
      testnetPOAPInfos.map(
        i =>
          math`(${currenCycle} - ${i.claimedToCycle}) * ${i.claimablePerCycle}`,
      ),
    )
  }

  @computed get claimables$(): Claimable[] {
    const claimable: Claimable[] = []
    if (mathIs`${this.UNWAirdropToClaim$} > ${0}`) {
      claimable.push({ type: "unw" })
    }
    if (mathIs`${this.EsUNWAirdropToClaim$} > ${0}`) {
      claimable.push({ type: "esuwn" })
    }
    if (mathIs`${this.UGPAirdropToClaim$} > ${0}`) {
      claimable.push(
        ...this.#onChainAirdropInfo.value$.ugpInfos.map(({ index }) => ({
          type: "ugp" as const,
          index,
        })),
      )
    }
    if (mathIs`${this.POAPAirdropToClaim$} > ${0}`) {
      claimable.push(
        ...this.#onChainAirdropInfo.value$.testnetPOAPInfos.map(
          ({ index }) => ({
            type: "poap" as const,
            index,
          }),
        ),
      )
    }
    return claimable
  }

  async claim(claimables: Claimable[]): Promise<ContractTransaction> {
    const multiCall = this.contracts.multiCall$
    const airdropHelper$ = this.contracts.airdropHelper$

    const callContractDataSeq = claimables.map(claimable => {
      switch (claimable.type) {
        case "esuwn":
          return airdropHelper$.interface.encodeFunctionData(
            "claimEscrowed(address)",
            [this.authStore.account$],
          )
        case "unw":
          return airdropHelper$.interface.encodeFunctionData("claim(address)", [
            this.authStore.account$,
          ])
        case "ugp":
          return airdropHelper$.interface.encodeFunctionData(
            "claim(uint32,address)",
            [claimable.index, this.authStore.account$],
          )
        case "poap":
          return airdropHelper$.interface.encodeFunctionData(
            "claimPOAP(uint32,address)",
            [claimable.index, this.authStore.account$],
          )
        default:
          assertNever(claimable)
      }
    })
    return multiCall.multiCall(
      callContractDataSeq.map(() => airdropHelper$.address),
      callContractDataSeq,
    )
  }
}
