import { computed, makeObservable } from "mobx"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { SharedLazyValue } from "../utils/LazyValue"
import { QuestScores } from "./AirdropStore.service"

export class TestnetContributorModule {
  constructor(private questScores: SharedLazyValue<QuestScores>) {
    makeObservable(this)
  }

  @computed get totalEarnedUNW$(): BigNumber {
    return BigNumber.sum([
      this.questScores.value$.testnet_contributors_scores,
      this.questScores.value$.testnet_poap_token_scores,
    ])
  }

  @computed get awardUNWPerIssue$(): BigNumber {
    return BigNumber.from(120)
  }

  @computed get bugReportAwardTotalWonAmount$(): BigNumber {
    return this.questScores.value$.testnet_contributors_scores
  }

  @computed get isCurrentAccountPoapHolder$(): boolean {
    return !BigNumber.isZero(this.questScores.value$.testnet_poap_token_scores)
  }

  @computed get awardUNWPerPoap$(): BigNumber {
    return BigNumber.from(125)
  }
}
