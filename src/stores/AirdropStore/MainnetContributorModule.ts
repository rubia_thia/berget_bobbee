import { computed, makeObservable } from "mobx"
import { StatusBadgeStatus } from "../../screens/AirdropScreen/components/panelComponents/QuestItem"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { DELAYED, readResource } from "../../utils/SuspenseResource"
import { AppEnvStore } from "../AppEnvStore/AppEnvStore"
import { AuthStore } from "../AuthStore/AuthStore"
import { LazyValue } from "../utils/LazyValue"
import { safelyGet } from "../utils/waitFor"
import { fetchMainnetContributor } from "./AirdropStore.service"

export class MainnetContributorModule {
  constructor(private authStore: AuthStore, private appEnvStore: AppEnvStore) {
    makeObservable(this)
  }

  private contributorInfo = new LazyValue(
    () => ({
      client: this.appEnvStore.airdropHasura$,
      walletAddress: this.authStore.account$,
    }),
    ({ client, walletAddress }) =>
      fetchMainnetContributor(client, walletAddress),
  )

  // liquidity provider

  @computed
  get liquidityProviderStage1Status$(): StatusBadgeStatus {
    const height = safelyGet(
      () => this.liquidityProviderStage1LoyaltyPhaseSnapshotBlockHeight$,
    )
    if (height == null) {
      return "live"
    } else {
      return "finished"
    }
  }

  @computed
  get liquidityProviderStage2Status$(): StatusBadgeStatus {
    const stage1Height = safelyGet(
      () => this.liquidityProviderStage1SnapshotBlockHeight$,
    )
    const stage2Height = safelyGet(
      () => this.liquidityProviderStage2SnapshotBlockHeight$,
    )
    if (stage1Height == null) {
      return "upcoming"
    } else if (stage2Height == null) {
      return "live"
    } else {
      return "finished"
    }
  }

  @computed
  get earnedUNWAsLiquidityProvider$(): BigNumber {
    return BigNumber.sum([
      this.liquidityProviderStage1UNWAwardCount$,
      safelyGet(() => this.liquidityProviderStage1LoyaltyPhaseUNWAwardCount$) ??
        0,
      safelyGet(() => this.liquidityProviderStage2UNWAwardCount$) ?? 0,
    ])
  }

  // liquidity provider stage 1
  @computed
  get liquidityProviderStage1Target$(): BigNumber {
    return BigNumber.from(1_000_000)
  }

  @computed
  get liquidityProviderStage1MyULPCount$(): BigNumber {
    return this.contributorInfo.value$.liquidity.stage1.liquidity
  }

  @computed
  get liquidityProviderStage1UNWAwardCount$(): BigNumber {
    if (
      safelyGet(() => this.liquidityProviderStage1SnapshotBlockHeight$) == null
    ) {
      return readResource(DELAYED())
    }
    return this.contributorInfo.value$.liquidity.stage1.liquidityScores
  }

  @computed
  get liquidityProviderStage1SnapshotBlockHeight$(): number {
    return (
      this.appEnvStore.appEnv$
        .airdrop_MainnetContributor_LiquidityProvider_Stage1SnapshotBlockHeight ??
      readResource(DELAYED())
    )
  }

  // liquidity provider stage 1 loyalty phase
  @computed
  get liquidityProviderStage1LoyaltyPhaseMyULPCount$(): BigNumber {
    return this.contributorInfo.value$.liquidity.stage1.loyaltyPhaseLiquidity
  }

  @computed
  get liquidityProviderStage1LoyaltyPhaseIsQualified$(): boolean {
    if (
      safelyGet(
        () => this.liquidityProviderStage1LoyaltyPhaseSnapshotBlockHeight$,
      ) == null
    ) {
      return readResource(DELAYED())
    }

    return BigNumber.isGtZero(
      this.contributorInfo.value$.liquidity.stage1.loyaltyPhaseLiquidityScores,
    )
  }

  @computed
  get liquidityProviderStage1LoyaltyPhaseUNWAwardCount$(): BigNumber {
    if (
      safelyGet(
        () => this.liquidityProviderStage1LoyaltyPhaseSnapshotBlockHeight$,
      ) == null
    ) {
      return readResource(DELAYED())
    }

    return this.contributorInfo.value$.liquidity.stage1
      .loyaltyPhaseLiquidityScores
  }

  @computed
  get liquidityProviderStage1LoyaltyPhaseSnapshotBlockHeight$(): number {
    return (
      this.appEnvStore.appEnv$
        .airdrop_MainnetContributor_LiquidityProvider_Stage1LoyaltyPhaseSnapshotBlockHeight ??
      readResource(DELAYED())
    )
  }

  // liquidity provider stage 2
  @computed
  get liquidityProviderStage2Target$(): BigNumber {
    return BigNumber.from(5_000_000)
  }

  @computed
  get liquidityProviderStage2MyULPCount$(): BigNumber {
    return this.contributorInfo.value$.liquidity.stage2.liquidity
  }

  @computed
  get liquidityProviderStage2UNWAwardCount$(): BigNumber {
    if (
      safelyGet(() => this.liquidityProviderStage2SnapshotBlockHeight$) == null
    ) {
      return readResource(DELAYED())
    }
    return this.contributorInfo.value$.liquidity.stage2.liquidityScores
  }

  @computed
  get liquidityProviderStage2SnapshotBlockHeight$(): number {
    return (
      this.appEnvStore.appEnv$
        .airdrop_MainnetContributor_LiquidityProvider_Stage2SnapshotBlockHeight ??
      readResource(DELAYED())
    )
  }

  // trader

  @computed
  get traderStage1Status$(): StatusBadgeStatus {
    const height = safelyGet(() => this.traderStage1SnapshotBlockHeight$)
    if (height == null) {
      return "live"
    } else {
      return "finished"
    }
  }

  @computed
  get traderStage2Status$(): StatusBadgeStatus {
    const stage1Height = safelyGet(() => this.traderStage1SnapshotBlockHeight$)
    const stage2Height = safelyGet(() => this.traderStage2SnapshotBlockHeight$)
    if (stage1Height == null) {
      return "upcoming"
    } else if (stage2Height == null) {
      return "live"
    } else {
      return "finished"
    }
  }

  @computed
  get earnedUNWAsTrader$(): BigNumber {
    return BigNumber.sum([
      this.traderStage1TMyUNWAward$,
      safelyGet(() => this.traderStage2TMyUNWAward$) ?? 0,
    ])
  }

  // trader stage 1

  @computed
  get traderStage1SnapshotBlockHeight$(): number {
    return (
      this.appEnvStore.appEnv$
        .airdrop_MainnetContributor_Trader_Stage1SnapshotBlockHeight ??
      readResource(DELAYED())
    )
  }

  @computed
  get traderStage1Target$(): BigNumber {
    return this.contributorInfo.value$.trader.stage1.targetTradingVolume
  }

  @computed
  get traderStage1MyTradeVolume$(): BigNumber {
    return this.contributorInfo.value$.trader.stage1.myTradingVolume
  }

  @computed
  get traderStage1TotalTradeVolume$(): BigNumber {
    return this.contributorInfo.value$.trader.stage1.totalTradingVolume
  }

  @computed
  get traderStage1TMyUNWAward$(): BigNumber {
    return this.contributorInfo.value$.trader.stage1.scores
  }

  @computed
  get traderStage1FinalUNWAward$(): BigNumber {
    return this.contributorInfo.value$.trader.stage1.finalScores
  }

  @computed
  get traderStage1TotalUNWAward$(): BigNumber {
    return this.contributorInfo.value$.trader.stage1.totalScores
  }

  // trader stage 2

  @computed
  get traderStage2SnapshotBlockHeight$(): number {
    return (
      this.appEnvStore.appEnv$
        .airdrop_MainnetContributor_Trader_Stage2SnapshotBlockHeight ??
      readResource(DELAYED())
    )
  }

  @computed
  get traderStage2Target$(): BigNumber {
    return this.contributorInfo.value$.trader.stage2.targetTradingVolume
  }

  @computed
  get traderStage2MyTradeVolume$(): BigNumber {
    return this.contributorInfo.value$.trader.stage2.myTradingVolume
  }

  @computed
  get traderStage2TotalTradeVolume$(): BigNumber {
    return this.contributorInfo.value$.trader.stage2.totalTradingVolume
  }

  @computed
  get traderStage2TMyUNWAward$(): BigNumber {
    if (this.traderStage2Status$ === "upcoming") {
      return readResource(DELAYED())
    }

    return this.contributorInfo.value$.trader.stage2.scores
  }

  @computed
  get traderStage2TotalUNWAward$(): BigNumber {
    return this.contributorInfo.value$.trader.stage2.totalScores
  }

  @computed
  get traderStage2FinalUNWAward$(): BigNumber {
    return this.contributorInfo.value$.trader.stage2.finalScores
  }
}
