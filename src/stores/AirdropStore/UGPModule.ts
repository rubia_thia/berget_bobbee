import { computed, makeObservable, observable } from "mobx"
import { from, of } from "rxjs"
import { UGPClaimingRecord } from "../../screens/AirdropScreen/components/types"
import { fromContractToNative } from "../../utils/contractHelpers/bigNumberContracts"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { math } from "../../utils/numberHelpers/bigNumberExpressionParser"
import { props } from "../../utils/promiseHelpers"
import { LazyValue, SharedLazyValue } from "../utils/LazyValue"
import { PaginationStore } from "../utils/PaginationStore"
import { AirdropStore } from "./AirdropStore"
import { AirdropDetailType } from "./AirdropStore.onChain.service"
import { QuestScores } from "./AirdropStore.service"

export class UGPModule {
  constructor(
    private questScores: SharedLazyValue<QuestScores>,
    private airdropDetail: SharedLazyValue<AirdropDetailType>,
    private store: AirdropStore,
  ) {
    makeObservable(this)
  }

  @computed
  get awardUNWPerUGP$(): BigNumber {
    return BigNumber.from(960)
  }

  @computed
  get totalEarnedUNW$(): BigNumber {
    return this.questScores.value$.genesis_pass_token_scores
  }

  @computed get ugpIndexes$(): number[] {
    return this.airdropDetail.value$.ugpInfos.map(({ index }) => index)
  }

  @computed get nextAirdropBlockNumber$(): number {
    const { currenCycle, startBlock, cycleLength } = this.airdropDetail.value$
    return startBlock + (currenCycle + 1) * cycleLength
  }

  @computed get cyclesLeft$(): number {
    const { currenCycle, endCycle } = this.airdropDetail.value$
    return endCycle - currenCycle
  }

  @observable searchingNft: null | number = null

  #searchResult = new LazyValue(
    () => ({
      query: this.searchingNft,
      ugp: this.store.contracts.genesisPass$,
      helper: this.store.contracts.airdropHelper$,
    }),
    ({ ugp, helper, query }) =>
      query == null
        ? of(null)
        : from(
            ugp
              .tokenByIndex(query)
              .then(a =>
                props({
                  claimed: helper.claimedCyclesByTokenId(a),
                  claimable: helper
                    .claimPerCycleByTokenId(a)
                    .then(fromContractToNative),
                }),
              )
              .catch(() => null),
          ),
    {
      reactionOptions: {
        delay: 1000,
      },
    },
  )

  @computed get myUGPs$(): UGPClaimingRecord[] {
    const { endCycle, currenCycle, ugpInfos } = this.airdropDetail.value$
    if (this.searchingNft != null) {
      const search = this.#searchResult.value$
      if (!search) {
        return []
      }
      return [
        {
          nftNumber: this.searchingNft,
          remainingEsUNW: math`(${endCycle} - ${search.claimed}) * ${search.claimable}`,
          claimableEsUNW: math`(${currenCycle} - ${search.claimed}) * ${search.claimable}`,
        },
      ]
    }
    return ugpInfos.map(i => ({
      nftNumber: i.index,
      claimableEsUNW: math`(${currenCycle} - ${i.claimedToCycle}) * ${i.claimablePerCycle}`,
      remainingEsUNW: math`(${endCycle} - ${i.claimedToCycle}) * ${i.claimablePerCycle}`,
    }))
  }

  records = new PaginationStore(() => this.myUGPs$)
}
