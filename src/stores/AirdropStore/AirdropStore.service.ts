import { Client, gql } from "@urql/core"
import { interval, Observable, switchMap } from "rxjs"
import {
  FetchMainnetContributorQuery,
  FetchMainnetContributorQueryVariables,
  GetParticipantCountQuery,
  GetParticipantCountQueryVariables,
  GetQuestScoresQuery,
  GetQuestScoresQueryVariables,
} from "../../generated/graphql/graphql.generated"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { math } from "../../utils/numberHelpers/bigNumberExpressionParser"
import { startWithObservable } from "../../utils/rxjsHelpers/rxjsHelpers"
import {
  ethAddressToHasuraAddress,
  fromUrqlSource,
} from "../utils/hasuraClient"

export const getParticipantCount = (client: Client): Observable<number> => {
  return fromUrqlSource(
    client.query(
      gql<GetParticipantCountQuery, GetParticipantCountQueryVariables>`
        query GetParticipantCount {
          ad_summary {
            total_holders
          }
        }
      `,
      {},
    ),
    resp => resp.data.ad_summary?.[0]?.total_holders ?? 0,
  )
}

export interface QuestScores {
  total_scores: BigNumber
  genesis_pass_token_scores: BigNumber
  testnet_contributors_scores: BigNumber
  testnet_poap_token_scores: BigNumber
}
export const getQuestScores = (
  client: Client,
  walletAddress: string,
): Observable<QuestScores> => {
  return fromUrqlSource(
    client.query(
      gql<GetQuestScoresQuery, GetQuestScoresQueryVariables>`
        query GetQuestScores($walletAddress: bytea!) {
          ad_ranked_holders(where: { address: { _eq: $walletAddress } }) {
            total_scores
            genesis_pass_token_scores
            testnet_contributoers_scores
            testnet_poap_token_scores
          }
        }
      `,
      {
        walletAddress: ethAddressToHasuraAddress(walletAddress),
      },
    ),
    resp => {
      const data = resp.data.ad_ranked_holders?.[0]

      return {
        total_scores: BigNumber.from(data?.total_scores ?? 0),
        genesis_pass_token_scores: BigNumber.from(
          data?.genesis_pass_token_scores ?? 0,
        ),
        testnet_contributors_scores: BigNumber.from(
          data?.testnet_contributoers_scores ?? 0,
        ),
        testnet_poap_token_scores: BigNumber.from(
          data?.testnet_poap_token_scores ?? 0,
        ),
      }
    },
  )
}

export const fetchMainnetContributor = (
  client: Client,
  walletAddress: string,
): Observable<{
  liquidity: {
    stage1: {
      liquidity: BigNumber
      liquidityScores: BigNumber
      loyaltyPhaseLiquidity: BigNumber
      loyaltyPhaseLiquidityScores: BigNumber
    }
    stage2: {
      liquidity: BigNumber
      liquidityScores: BigNumber
    }
  }
  trader: {
    stage1: {
      myTradingVolume: BigNumber
      totalTradingVolume: BigNumber
      targetTradingVolume: BigNumber
      scores: BigNumber
      totalScores: BigNumber
      finalScores: BigNumber
    }
    stage2: {
      myTradingVolume: BigNumber
      totalTradingVolume: BigNumber
      targetTradingVolume: BigNumber
      scores: BigNumber
      totalScores: BigNumber
      finalScores: BigNumber
    }
  }
}> => {
  const source = fromUrqlSource(
    client.query(
      gql<FetchMainnetContributorQuery, FetchMainnetContributorQueryVariables>`
        query FetchMainnetContributor($walletAddress: bytea!) {
          liquidity_bootstrap_stage_1(
            where: { address: { _eq: $walletAddress } }
          ) {
            balance
            scores
          }
          liquidity_bootstrap_stage_2(
            where: { address: { _eq: $walletAddress } }
          ) {
            balance
            scores
          }
          liquidity_bootstrap_stage_3(
            where: { address: { _eq: $walletAddress } }
          ) {
            balance
            scores
          }
          trade_bootstrap_stage_1(
            where: { trade_user: { _eq: $walletAddress } }
          ) {
            open_position
            close_position
          }
          trade_bootstrap_stage_2(
            where: { trade_user: { _eq: $walletAddress } }
          ) {
            open_position
            close_position
          }
          trading_total {
            total_trade_volume
          }
        }
      `,
      {
        walletAddress: ethAddressToHasuraAddress(walletAddress),
      },
    ),
    resp => {
      const traderStage1MyTradingVolume = BigNumber.sum([
        BigNumber.from(
          resp.data.trade_bootstrap_stage_1[0]?.open_position ?? 0,
        ),
        BigNumber.from(
          resp.data.trade_bootstrap_stage_1[0]?.close_position ?? 0,
        ),
      ])
      const traderStage1TargetTradingVolume = BigNumber.from(100_000_000)
      const traderStage1TotalScores = BigNumber.from(1_000_000)
      const traderStage1TotalTradingVolume = BigNumber.from(
        resp.data.trading_total[0]?.total_trade_volume ?? 0,
      )

      const traderStage2MyTradingVolume = BigNumber.sum([
        BigNumber.from(
          resp.data.trade_bootstrap_stage_2[0]?.open_position ?? 0,
        ),
        BigNumber.from(
          resp.data.trade_bootstrap_stage_2[0]?.close_position ?? 0,
        ),
      ])
      const traderStage2TargetTradingVolume = BigNumber.from(1_000_000_000) // TODO
      const traderStage2TotalScores = BigNumber.from(1_000_000) // TODO
      const traderStage2TotalTradingVolume = BigNumber.from(
        resp.data.trading_total[0]?.total_trade_volume ?? 0,
      )
      return {
        liquidity: {
          stage1: {
            liquidity: BigNumber.from(
              resp.data.liquidity_bootstrap_stage_1[0]?.balance ?? 0,
            ),
            liquidityScores: BigNumber.from(
              resp.data.liquidity_bootstrap_stage_1[0]?.scores ?? 0,
            ),
            loyaltyPhaseLiquidity: BigNumber.from(
              resp.data.liquidity_bootstrap_stage_2[0]?.balance ?? 0,
            ),
            loyaltyPhaseLiquidityScores: BigNumber.from(
              resp.data.liquidity_bootstrap_stage_2[0]?.scores ?? 0,
            ),
          },
          stage2: {
            liquidity: BigNumber.from(
              resp.data.liquidity_bootstrap_stage_3[0]?.balance ?? 0,
            ),
            liquidityScores: BigNumber.from(
              resp.data.liquidity_bootstrap_stage_3[0]?.scores ?? 0,
            ),
          },
        },
        trader: {
          stage1: {
            myTradingVolume: traderStage1MyTradingVolume,
            totalTradingVolume: traderStage1TotalTradingVolume,
            targetTradingVolume: traderStage1TargetTradingVolume,
            scores: math`${traderStage1MyTradingVolume} / ${traderStage1TargetTradingVolume} * ${traderStage1TotalScores}`,
            totalScores: math`${traderStage1TotalTradingVolume} / ${traderStage1TargetTradingVolume} * ${traderStage1TotalScores}`,
            finalScores: traderStage1TotalScores,
          },
          stage2: {
            myTradingVolume: traderStage2MyTradingVolume,
            totalTradingVolume: traderStage2TotalTradingVolume,
            targetTradingVolume: traderStage2TargetTradingVolume,
            scores: BigNumber.ZERO, // math`${traderStage2MyTradingVolume} / ${traderStage2TargetTradingVolume} * ${traderStage2TotalScores}`,
            totalScores: math`${traderStage2TotalTradingVolume} / ${traderStage2TargetTradingVolume} * ${traderStage2TotalScores}`,
            finalScores: traderStage2TotalScores,
          },
        },
      }
    },
  )

  return interval(1000 * 15).pipe(
    switchMap(() => source),
    startWithObservable(source),
  )
}
