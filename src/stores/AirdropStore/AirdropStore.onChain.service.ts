import { compose } from "ramda"
import { fromContractToNative } from "../../utils/contractHelpers/bigNumberContracts"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { props } from "../../utils/promiseHelpers"
import { AirdropHelper, UniwhalePass } from "../contracts/generated"

export type AirdropDetailType = Awaited<ReturnType<typeof getAirdropDetail>>

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export async function getAirdropDetail({
  helper,
  ugp,
  address,
  testnetPOAP,
}: {
  helper: AirdropHelper
  address: string
  ugp: UniwhalePass
  testnetPOAP: UniwhalePass
}) {
  const toNumber = compose(BigNumber.toNumber, BigNumber.from)
  const ugpIndexes = await ugp
    .balanceOf(address)
    .then(toNumber)
    .then(ownedAmount => {
      return Promise.all(
        Array(ownedAmount)
          .fill(null)
          .map((_, i) => ugp.tokenOfOwnerByIndex(address, i).then(toNumber)),
      )
    })
  const ugpInfos = Promise.all(
    ugpIndexes.map(index =>
      props({
        index: Promise.resolve(index),
        claimedToCycle: helper
          .claimedCyclesByTokenId(index)
          .then(toNumber) as Promise<number>,
        claimablePerCycle: helper
          .claimPerCycleByTokenId(index)
          .then(fromContractToNative) as Promise<BigNumber>,
      }),
    ),
  )
  const ownedTestnetPOAP = await testnetPOAP
    .balanceOf(address)
    .then(toNumber)
    .then(ownedAmount => {
      return Promise.all(
        Array(ownedAmount)
          .fill(null)
          .map((_, i) =>
            testnetPOAP.tokenOfOwnerByIndex(address, i).then(toNumber),
          ),
      )
    })
  const testnetPOAPInfos = Promise.all(
    ownedTestnetPOAP.map(index =>
      props({
        index: Promise.resolve(index),
        claimedToCycle: helper
          .claimedCyclesByPOAPTokenId(index)
          .then(toNumber) as Promise<number>,
        claimablePerCycle: helper
          .claimPerCycleByPOAPTokenId(index)
          .then(fromContractToNative) as Promise<BigNumber>,
      }),
    ),
  )

  const inputs = {
    startBlock: helper.startBlock().then(toNumber),
    cycleLength: helper.blocksPerCycle().then(toNumber),
    endCycle: helper.endCycle().then(toNumber),
    currenCycle: helper.getCurrentCycle().then(toNumber),

    claimedCyclesByHolder: helper.claimedCyclesByHolder(address).then(toNumber),
    claimedCyclesEscrowedByHolder: helper
      .claimedCyclesEscrowedByHolder(address)
      .then(toNumber),

    claimableCyclesByHolder: helper
      .claimPerCycleByHolder(address)
      .then(fromContractToNative),
    claimableCyclesEscrowedByHolder: helper
      .claimEscrowedPerCycleByHolder(address)
      .then(fromContractToNative),

    ugpInfos,
    testnetPOAPInfos,
  }
  return props(inputs)
}
