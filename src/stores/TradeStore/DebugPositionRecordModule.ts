import {
  DebugPositionRecord,
  DebugPositionStatus,
} from "../../screens/Debug/components/PositionsTabContent/types"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { math } from "../../utils/numberHelpers/bigNumberExpressionParser"
import { suspenseResource } from "../../utils/SuspenseResource"
import { TradeCellModule } from "./TradeCellModule"

export function fromTradeToDebugRecord(
  cell: TradeCellModule,
): DebugPositionRecord {
  const { liquidationPrice } = cell.trade

  return {
    status: !cell.isClosingTrade
      ? DebugPositionStatus.open
      : cell.isClosedByLiquidator
      ? DebugPositionStatus.closedByLiquidation
      : cell.isClosedByStopLoss
      ? DebugPositionStatus.closedByStopLoss
      : cell.isClosedByTakeProfit
      ? DebugPositionStatus.closedByTakeProfit
      : DebugPositionStatus.closedByUser,
    openedAt: new Date(cell.openEvent.blockTime),
    lastUpdatedAt: new Date(cell.trade.blockTime),
    user: cell.trade.user,
    baseToken: cell.baseTokenInfo$,
    quoteToken: cell.quoteTokenInfo$,
    id: cell.trade.orderHash,
    direction: cell.orderDirection,
    entryPrice: cell.entryPrice,
    leverage: cell.leverage,
    margin: cell.isClosingTrade ? cell.trade.margin : cell.remainingMargin,
    positionSize: cell.isClosingTrade
      ? math`${cell.trade.leverage} x ${cell.trade.margin}`
      : cell.remainingPosition,
    markPrice: suspenseResource(() => cell.currentMarkPrice$),
    adjustedMarkPriceOrClosing: suspenseResource(() =>
      cell.isClosingTrade
        ? cell.closedPrice
        : cell.calcAdjustedClosePrice$(
            cell.remainingMargin,
            cell.currentMarkPrice$,
          ),
    ),
    liquidityPrice: liquidationPrice,
    stopLossPrice: cell.isClosingTrade
      ? cell.trade.stopLoss
      : cell.stopLossPriceDisplay$,
    takeProfitPrice: cell.isClosingTrade
      ? cell.trade.profitTarget
      : cell.takeProfitPriceDisplay$,
    pnl: suspenseResource(() =>
      cell.isClosingTrade
        ? {
            delta: cell.grossPnl,
            deltaPercentage: cell.grossPnlPercentage,
          }
        : {
            delta: cell.unrealizedPnl$.amount,
            deltaPercentage: cell.unrealizedPnl$.percentage,
          },
    ),
    fees: cell.isClosingTrade
      ? {
          total: suspenseResource(() =>
            cell.trade.type === "close"
              ? math`${cell.openEvent.fee} + ${cell.trade.closeFundingFee} + ${cell.trade.closeRolloverFee}`
              : cell.openFee$,
          ),
          open: suspenseResource(() => cell.openEvent.fee),
          funding: suspenseResource(() =>
            cell.trade.type === "close"
              ? cell.trade.closeFundingFee
              : BigNumber.from(0),
          ),
          rollover: suspenseResource(() =>
            cell.trade.type === "close"
              ? cell.trade.closeRolloverFee
              : BigNumber.from(0),
          ),
        }
      : {
          total: suspenseResource(
            () =>
              math`${cell.openFee$} + ${
                cell.calcAccumulatedFeeForClosing$(cell.remainingMargin).total
              }`,
          ),
          open: suspenseResource(() => cell.openFee$),
          funding: suspenseResource(
            () =>
              cell.calcAccumulatedFeeForClosing$(cell.remainingMargin)
                .fundingFee,
          ),
          rollover: suspenseResource(
            () =>
              cell.calcAccumulatedFeeForClosing$(cell.remainingMargin)
                .rolloverFee,
          ),
        },
  }
}
