import { Client, gql } from "@urql/core"
import { combineLatestWith, map, Observable, startWith } from "rxjs"
import {
  CloseOrderEventHistoriesQuery,
  CloseOrderEventHistoriesQueryVariables,
  CloseOrderEventsSubscription,
  CloseOrderEventsSubscriptionVariables,
  OpenOrderEventHistoriesQuery,
  OpenOrderEventHistoriesQueryVariables,
  OpenOrderEventsSubscription,
  OpenOrderEventsSubscriptionVariables,
  Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Bool_Exp,
  Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Bool_Exp,
  Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Bool_Exp,
  UpdateOrderEventHistoriesQuery,
  UpdateOrderEventHistoriesQueryVariables,
  UpdateOrderEventsSubscription,
  UpdateOrderEventsSubscriptionVariables,
} from "../../generated/graphql/graphql.generated"
import { fromContractToNative } from "../../utils/contractHelpers/bigNumberContracts"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { fromUrqlSource, hasuraAddressToEth } from "../utils/hasuraClient"

export namespace MarketOrderEvent {
  export type Base = {
    priceId: string
    user: string
    orderHash: string
    isLong: boolean
    margin: BigNumber
    slippage: BigNumber
    stopLoss: null | BigNumber
    profitTarget: null | BigNumber
    leverage: BigNumber
    maxPercentagePnL: BigNumber
    openPrice: BigNumber
    liquidationPrice: BigNumber
    accruedFee: {
      rolloverFee: BigNumber
      fundingFee: BigNumber
      lastUpdate: number
    }
    fundingFeeBase: BigNumber
    executionBlock: number
    blockNumber: number
    blockTime: string
    txHash: string
  }
  export interface OpenEvent extends Base {
    type: "open"
    fee: BigNumber
  }

  export interface UpdateEvent extends Base {
    type: "update"
    updateIsAdding: boolean
    updateMarginDelta: BigNumber
  }

  export interface CloseEvent extends Base {
    type: "close"
    closeFundingFee: BigNumber
    closeRolloverFee: BigNumber
    closeLiquidatorFee: BigNumber
    closeSlippage: BigNumber
    closeFee: BigNumber
    closeOraclePrice: BigNumber
    closePrice: BigNumber
    closePercentage: BigNumber
    closeSettled: BigNumber
    isLiquidated: boolean
    isStop: boolean
  }
}

export type MarketOrderEvent =
  | MarketOrderEvent.OpenEvent
  | MarketOrderEvent.UpdateEvent
  | MarketOrderEvent.CloseEvent

type HasuraTrade = Omit<
  OpenOrderEventsSubscription["uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent"][number],
  "__typename" | "fee_fee"
>

const mapToNativeTrade = (event: HasuraTrade): MarketOrderEvent.Base => ({
  priceId: hasuraAddressToEth(event.trade_priceId),
  orderHash: hasuraAddressToEth(event.orderHash),
  user: hasuraAddressToEth(event.trade_user),
  isLong: event.trade_isBuy,
  leverage: fromContractToNative(event.trade_leverage),
  liquidationPrice: fromContractToNative(event.trade_liquidationPrice),
  margin: fromContractToNative(event.trade_margin),
  openPrice: fromContractToNative(event.trade_openPrice),
  slippage: fromContractToNative(event.trade_slippage),
  maxPercentagePnL: fromContractToNative(event.trade_maxPercentagePnL),
  accruedFee: {
    fundingFee: fromContractToNative(event.onOrderUpdate_accruedFee_fundingFee),
    rolloverFee: fromContractToNative(
      event.onOrderUpdate_accruedFee_rolloverFee,
    ),
    lastUpdate: event.onOrderUpdate_accruedFee_lastUpdate,
  },
  fundingFeeBase: fromContractToNative(event.onOrderUpdate_feeBase),
  profitTarget:
    event.trade_profitTarget === 0
      ? null
      : fromContractToNative(event.trade_profitTarget),
  stopLoss:
    event.trade_stopLoss === 0
      ? null
      : fromContractToNative(event.trade_stopLoss),
  executionBlock: event.trade_executionBlock,
  blockNumber: event.evt_block_number,
  blockTime: event.evt_block_time,
  txHash: hasuraAddressToEth(event.evt_tx_hash),
})

export function openOrderEvents(
  client: Client,
  currentBlock: number,
  where: Uniwhale_V1_Bsc_TradingCore_Evt_OpenMarketOrderEvent_Bool_Exp,
  limit?: number,
): Observable<MarketOrderEvent.OpenEvent[]> {
  const openEventFragment = gql`
    fragment OpenEvent on uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent {
      orderHash
      trade_priceId
      trade_isBuy
      trade_margin
      trade_slippage
      trade_stopLoss
      trade_profitTarget
      trade_leverage
      trade_maxPercentagePnL
      trade_openPrice
      trade_liquidationPrice
      trade_executionBlock
      trade_user
      fee_fee
      onOrderUpdate_feeBase
      onOrderUpdate_accruedFee_fundingFee
      onOrderUpdate_accruedFee_rolloverFee
      onOrderUpdate_accruedFee_lastUpdate
      evt_block_number
      evt_block_time
      evt_tx_hash
    }
  `
  const historical = fromUrqlSource(
    client.query<
      OpenOrderEventHistoriesQuery,
      OpenOrderEventHistoriesQueryVariables
    >(
      gql`
        query OpenOrderEventHistories($where: uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent_bool_exp, $limit: Int) {
          uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent(
            where: $where
            limit: $limit
            order_by: { evt_block_number: desc }
          ) {
            ...OpenEvent
          }
          ${openEventFragment}
        }
      `,
      {
        where,
        limit,
      },
    ),
    a =>
      a.data.uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent.map(
        (e): MarketOrderEvent.OpenEvent => ({
          type: "open",
          ...mapToNativeTrade(e),
          fee: fromContractToNative(e.fee_fee),
        }),
      ),
  )
  const newData = fromUrqlSource(
    client.subscription<
      OpenOrderEventsSubscription,
      OpenOrderEventsSubscriptionVariables
    >(
      gql`
        subscription OpenOrderEvents(
          $where: uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent_bool_exp!
        ) {
          uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent(
            where: $where
            order_by: { evt_block_number: desc }
          ) {
            ...OpenEvent
          }
        }
        ${openEventFragment}
      `,
      {
        where: {
          ...where,
          evt_block_number: { _gt: currentBlock },
        },
      },
    ),
    a =>
      a.data.uniwhale_v1_bsc_TradingCore_evt_OpenMarketOrderEvent.map(
        (e): MarketOrderEvent.OpenEvent => ({
          type: "open",
          ...mapToNativeTrade(e),
          fee: fromContractToNative(e.fee_fee),
        }),
      ),
  )
  return historical.pipe(
    combineLatestWith(
      newData.pipe(startWith([] as MarketOrderEvent.OpenEvent[])),
    ),
    map(([a, b]) => b.concat(a)),
  )
}

export function updateOrderEvents(
  client: Client,
  currentBlock: number,
  where: Uniwhale_V1_Bsc_TradingCore_Evt_UpdateOpenOrderEvent_Bool_Exp,
  limit?: number,
): Observable<MarketOrderEvent.UpdateEvent[]> {
  const updateEventFragment = gql`
    fragment UpdateEvent on uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent {
      orderHash
      trade_priceId
      trade_isBuy
      trade_margin
      trade_slippage
      trade_stopLoss
      trade_profitTarget
      trade_leverage
      trade_maxPercentagePnL
      trade_openPrice
      trade_liquidationPrice
      trade_executionBlock
      trade_user
      onUpdateTrade_marginDelta
      onUpdateTrade_isAdding
      onOrderUpdate_feeBase
      onOrderUpdate_accruedFee_fundingFee
      onOrderUpdate_accruedFee_rolloverFee
      onOrderUpdate_accruedFee_lastUpdate
      evt_block_number
      evt_block_time
      evt_tx_hash
    }
  `
  const historical = fromUrqlSource(
    client.query<
      UpdateOrderEventHistoriesQuery,
      UpdateOrderEventHistoriesQueryVariables
    >(
      gql`
    query UpdateOrderEventHistories($where: uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent_bool_exp, $limit: Int) {
      uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent(
        where: $where
        limit: $limit
        order_by: { evt_block_number: desc }
      ) {
        ...UpdateEvent
      }
      ${updateEventFragment}
    }
  `,
      {
        where,
        limit,
      },
    ),
    a =>
      a.data.uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent.map(
        (e): MarketOrderEvent.UpdateEvent => ({
          type: "update",
          ...mapToNativeTrade(e),
          updateIsAdding: e.onUpdateTrade_isAdding,
          updateMarginDelta: fromContractToNative(e.onUpdateTrade_marginDelta),
        }),
      ),
  )
  const newData = fromUrqlSource(
    client.subscription<
      UpdateOrderEventsSubscription,
      UpdateOrderEventsSubscriptionVariables
    >(
      gql`
        subscription UpdateOrderEvents(
          $where: uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent_bool_exp
        ) {
          uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent(
            where: $where
            order_by: { evt_block_number: desc }
          ) {
            ...UpdateEvent
          }
        }
        ${updateEventFragment}
      `,
      {
        where: {
          ...where,
          evt_block_number: { _gt: currentBlock },
        },
      },
    ),
    a =>
      a.data.uniwhale_v1_bsc_TradingCore_evt_UpdateOpenOrderEvent.map(
        (e): MarketOrderEvent.UpdateEvent => ({
          type: "update",
          ...mapToNativeTrade(e),
          updateIsAdding: e.onUpdateTrade_isAdding,
          updateMarginDelta: fromContractToNative(e.onUpdateTrade_marginDelta),
        }),
      ),
  )
  return historical.pipe(
    combineLatestWith(
      newData.pipe(startWith([] as MarketOrderEvent.UpdateEvent[])),
    ),
    map(([a, b]) => b.concat(a)),
  )
}

export function closeOrderEvents(
  client: Client,
  currentBlock: number,
  where: Uniwhale_V1_Bsc_TradingCore_Evt_CloseMarketOrderEvent_Bool_Exp,
  limit?: number,
): Observable<MarketOrderEvent.CloseEvent[]> {
  const closeEventFragment = gql`
    fragment CloseEvent on uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent {
      orderHash
      closePercent
      trade_priceId
      trade_isBuy
      trade_margin
      trade_slippage
      trade_stopLoss
      trade_profitTarget
      trade_leverage
      trade_maxPercentagePnL
      trade_openPrice
      trade_liquidationPrice
      trade_executionBlock
      trade_user
      afterCloseTrade_settled
      afterCloseTrade_fees_fee
      afterCloseTrade_liquidationFee
      afterCloseTrade_oraclePrice
      onCloseTrade_closeNet
      onCloseTrade_grossPnL
      onCloseTrade_isLiquidated
      onCloseTrade_isStop
      onCloseTrade_fundingFee
      onCloseTrade_rolloverFee
      onCloseTrade_slippage
      onOrderUpdate_feeBase
      onOrderUpdate_accruedFee_fundingFee
      onOrderUpdate_accruedFee_rolloverFee
      onOrderUpdate_accruedFee_lastUpdate
      evt_block_number
      evt_block_time
      evt_tx_hash
    }
  `
  const historical = fromUrqlSource(
    client.query<
      CloseOrderEventHistoriesQuery,
      CloseOrderEventHistoriesQueryVariables
    >(
      gql`
        query CloseOrderEventHistories($where: uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent_bool_exp, $limit: Int) {
          uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent(
            where: $where
            order_by: { evt_block_number: desc }
            limit: $limit
          ) {
            ...CloseEvent
          }
          ${closeEventFragment}
        }
      `,
      {
        where,
        limit,
      },
    ),
    a =>
      a.data.uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent.map(
        (e): MarketOrderEvent.CloseEvent => ({
          type: "close",
          ...mapToNativeTrade(e),
          closePercentage: fromContractToNative(e.closePercent),
          closeOraclePrice: fromContractToNative(e.afterCloseTrade_oraclePrice),
          closeSettled: fromContractToNative(e.afterCloseTrade_settled),
          closePrice: fromContractToNative(e.onCloseTrade_closeNet),
          closeSlippage: fromContractToNative(e.onCloseTrade_slippage),
          closeFundingFee: fromContractToNative(e.onCloseTrade_fundingFee),
          closeRolloverFee: fromContractToNative(e.onCloseTrade_rolloverFee),
          closeLiquidatorFee: fromContractToNative(
            e.afterCloseTrade_liquidationFee,
          ),
          closeFee: fromContractToNative(e.afterCloseTrade_fees_fee),
          isLiquidated: e.onCloseTrade_isLiquidated,
          isStop: e.onCloseTrade_isStop,
        }),
      ),
  )
  const newData = fromUrqlSource(
    client.subscription<
      CloseOrderEventsSubscription,
      CloseOrderEventsSubscriptionVariables
    >(
      gql`
        subscription CloseOrderEvents(
          $where: uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent_bool_exp
        ) {
          uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent(
            where: $where
            order_by: { evt_block_number: desc }
          ) {
            ...CloseEvent
          }
        }
        ${closeEventFragment}
      `,
      {
        where: {
          ...where,
          evt_block_number: { _gt: currentBlock },
        },
      },
    ),
    a =>
      a.data.uniwhale_v1_bsc_TradingCore_evt_CloseMarketOrderEvent.map(
        (e): MarketOrderEvent.CloseEvent => ({
          type: "close",
          ...mapToNativeTrade(e),
          closePercentage: fromContractToNative(e.closePercent),
          closeOraclePrice: fromContractToNative(e.afterCloseTrade_oraclePrice),
          closeSettled: fromContractToNative(e.afterCloseTrade_settled),
          closeLiquidatorFee: fromContractToNative(
            e.afterCloseTrade_liquidationFee,
          ),
          closeFee: fromContractToNative(e.afterCloseTrade_fees_fee),
          closePrice: fromContractToNative(e.onCloseTrade_closeNet),
          closeSlippage: fromContractToNative(e.onCloseTrade_slippage),
          closeFundingFee: fromContractToNative(e.onCloseTrade_fundingFee),
          closeRolloverFee: fromContractToNative(e.onCloseTrade_rolloverFee),
          isLiquidated: e.onCloseTrade_isLiquidated,
          isStop: e.onCloseTrade_isStop,
        }),
      ),
  )
  return historical.pipe(
    combineLatestWith(
      newData.pipe(startWith([] as MarketOrderEvent.CloseEvent[])),
    ),
    map(([a, b]) => b.concat(a)),
  )
}
