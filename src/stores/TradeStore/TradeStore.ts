import { action, computed, makeObservable, observable } from "mobx"
import { DisplayableError } from "../../utils/errorHelpers"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { math } from "../../utils/numberHelpers/bigNumberExpressionParser"
import { TokenInfo } from "../../utils/TokenInfo"
import { AppEnvStore } from "../AppEnvStore/AppEnvStore"
import { PriceSymbol, PythPrice } from "../AppEnvStore/PythModule.service"
import { AuthStore } from "../AuthStore/AuthStore"
import { ContractStore } from "../contracts/ContractStore"
import { CurrencyStore } from "../CurrencyStore/CurrencyStore"
import { ERC20Tokens } from "../CurrencyStore/CurrencyStore.service"
import { OrderCreationModule } from "./OrderCreationModule"
import { OrderHistoryModule } from "./OrderHistoryModule"
import { PriceChartModule } from "./PriceChartModule"
import { TradeInfoStore } from "./TradeInfoStore"
import { TradePromoteModule } from "./TradePromoteModule"
import { TradeCoreInfosType } from "./TradeStore.service"

export class TradeStore {
  constructor(
    priceSymbol: PriceSymbol,
    readonly appEnvStore: AppEnvStore,
    readonly authStore: AuthStore,
    readonly currencyStore: CurrencyStore,
    readonly contracts: ContractStore,
    readonly tradeInfo: TradeInfoStore,
  ) {
    this.chart = new PriceChartModule(
      this.appEnvStore,
      this.authStore,
      this.tradeInfo,
      () => this.currentPriceId$,
      () => this.currentSelectedMarketPriceIdInfo$,
    )
    this.priceSymbol = priceSymbol
    makeObservable(this)
  }

  tradePromote = new TradePromoteModule(this)

  @observable private priceSymbol: PriceSymbol

  @action selectPriceSymbol(priceSymbol: PriceSymbol): void {
    this.priceSymbol = priceSymbol
    this.appEnvStore.previousMarket = priceSymbol
  }

  chart: PriceChartModule

  orderCreation = new OrderCreationModule(this)
  orderHistory = new OrderHistoryModule(this)

  @computed get baseTokenInfo$(): TokenInfo {
    return this.currencyStore.baseTokenInfo$(this.priceSymbol$)
  }

  @computed get anchorTokenInfo$(): TokenInfo {
    return {
      ...this.currencyStore.tokenInfo$(ERC20Tokens.AnchorToken),
      precision: this.priceSymbolPrecision$,
    }
  }

  @computed get priceSymbolPrecision$(): number {
    return this.appEnvStore.pyth.marketPrecisionFor(this.priceSymbol$)
  }

  @computed get priceSymbol$(): PriceSymbol {
    const approvedPriceSymbols =
      this.appEnvStore.onChainConfigs$.approvedPriceSymbols
    if (!approvedPriceSymbols.includes(this.priceSymbol)) {
      throw new DisplayableError(
        `Price symbol ${this.priceSymbol} not approved`,
        {
          cause: {
            source: "TradeStore#priceSymbol$",
            approvedPriceSymbols,
          },
        },
      )
    }
    return this.priceSymbol
  }

  @computed get currentPriceId$(): string {
    return this.appEnvStore.pyth.priceIdFor(this.priceSymbol$)
  }

  @computed get priceFromPyth$(): PythPrice {
    return this.appEnvStore.pyth.priceFor(this.currentPriceId$).value$
  }

  @computed get currentSelectedMarketPriceIdInfo$(): TradeCoreInfosType {
    // use default 0x0 account if not connected
    const account = this.authStore.connected
      ? this.authStore.account$
      : "0x0000000000000000000000000000000000000000"
    return this.tradeInfo.infoForPriceId$(this.currentPriceId$, account)
  }

  @computed get markPrice$(): BigNumber {
    return this.priceFromPyth$.price
  }

  #blockCountPerSecond = math`${1} / ${3}`
  #hourlyRateFactor = math`${60} * ${60} * ${this.#blockCountPerSecond}`
  @computed get longFundingFeeHourlyRate$(): BigNumber {
    const { fundingFeeRate } = this.currentSelectedMarketPriceIdInfo$
    const imbalance = this.tradeInfo.longImbalanceForPriceId$(
      this.currentPriceId$,
    )
    return math`${fundingFeeRate} x ${imbalance} * ${this.#hourlyRateFactor}`
  }
  @computed get shortFundingFeeHourlyRate$(): BigNumber {
    const { fundingFeeRate } = this.currentSelectedMarketPriceIdInfo$
    const imbalance = this.tradeInfo.shortImbalanceForPriceId$(
      this.currentPriceId$,
    )
    return math`${fundingFeeRate} x ${imbalance}* ${this.#hourlyRateFactor}`
  }
  @computed get rolloverFeeHourlyRate$(): BigNumber {
    return math`${this.currentSelectedMarketPriceIdInfo$.rolloverFeeRate} * ${
      this.#hourlyRateFactor
    }`
  }
}
