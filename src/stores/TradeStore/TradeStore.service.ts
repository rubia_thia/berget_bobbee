import {
  combineLatest,
  map,
  merge,
  Observable,
  switchMap,
  throttleTime,
} from "rxjs"
import {
  fromContractToNative,
  fromNativeToContract,
} from "../../utils/contractHelpers/bigNumberContracts"
import { SignedBalance } from "../../utils/contractHelpers/SignedBalance"
import { DisplayableError } from "../../utils/errorHelpers"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { fromContractEvent } from "../../utils/rxjsHelpers/fromContractEvent"
import {
  ObservableValueOf,
  startWithObservable,
} from "../../utils/rxjsHelpers/rxjsHelpers"
import {
  Fees,
  OracleAggregator,
  RegistryCore,
  RegistryReader,
  TradingCore,
} from "../contracts/generated"

export function getAccumulatedFee(
  currentBlockObservable: Observable<number>,
  registryCore: RegistryCore,
  registryReader: RegistryReader,
  orderHash: string,
  closePercentage: BigNumber,
): Observable<{
  fundingFee: BigNumber
  rolloverFee: BigNumber
}> {
  const fetchAccumulatedFee = async (): Promise<{
    fundingFee: BigNumber
    rolloverFee: BigNumber
  }> => {
    return registryReader
      .getAccumulatedFee(
        registryCore.address,
        orderHash,
        fromNativeToContract(closePercentage),
      )
      .then(res => ({
        fundingFee: fromContractToNative(res[0]),
        rolloverFee: fromContractToNative(res[1]),
      }))
  }

  return currentBlockObservable.pipe(
    throttleTime(1000 * 3),
    switchMap(fetchAccumulatedFee),
    startWithObservable(fetchAccumulatedFee()),
  )
}

export type TradeCoreInfosType = ObservableValueOf<
  ReturnType<typeof tradeCoreInfos>
>

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export function tradeCoreInfos(
  walletAddress: string,
  priceId: string,
  tradingCore: TradingCore,
  registryCore: RegistryCore,
  registryCoreForEvent: RegistryCore,
  feesForEvent: Fees,
  registryReader: RegistryReader,
  oracleAggregator: OracleAggregator,
  currentBlockObservable: Observable<number>,
) {
  const marketParameters$ = currentBlockObservable.pipe(
    throttleTime(1000 * 10),
    switchMap(() =>
      fetchMarketParameters(
        registryCore,
        oracleAggregator,
        registryReader,
        priceId,
      ),
    ),
    startWithObservable(
      fetchMarketParameters(
        registryCore,
        oracleAggregator,
        registryReader,
        priceId,
      ),
    ),
  )

  const fetchOpenFee = (): Promise<BigNumber> =>
    feesForEvent
      .getOpenFee(walletAddress)
      .then(a => fromContractToNative(a.fee))
  const openFee$ = merge(
    fromContractEvent(feesForEvent, feesForEvent.filters["SetOpenFeeEvent"]()),
    fromContractEvent(
      feesForEvent,
      feesForEvent.filters["SetReferralEvent(address)"](),
    ),
  ).pipe(switchMap(fetchOpenFee), startWithObservable(fetchOpenFee()))

  const maxPercentagePnLFactor = registryCore
    .maxPercentagePnLFactor()
    .then(fromContractToNative)
  const maxPercentagePnLFactor$ = fromContractEvent(
    registryCoreForEvent,
    registryCoreForEvent.filters.SetMaxPercentagePnLFactorEvent(),
  ).pipe(
    map(([maxPercentagePnLFactor]) =>
      fromContractToNative(maxPercentagePnLFactor),
    ),
    startWithObservable(maxPercentagePnLFactor),
  )

  const maxPercentagePnLCap = registryCore
    .maxPercentagePnLCap()
    .then(fromContractToNative)
  const maxPercentagePnLCap$ = fromContractEvent(
    registryCoreForEvent,
    registryCoreForEvent.filters.SetMaxPercentagePnLCapEvent(),
  ).pipe(
    map(([maxPercentagePnLCap]) => fromContractToNative(maxPercentagePnLCap)),
    startWithObservable(maxPercentagePnLCap),
  )

  const maxPercentagePnLFloor = registryCore
    .maxPercentagePnLFloor()
    .then(fromContractToNative)
  const maxPercentagePnLFloor$ = fromContractEvent(
    registryCoreForEvent,
    registryCoreForEvent.filters.SetMaxPercentagePnLFloorEvent(),
  ).pipe(
    map(([maxPercentagePnLFloor]) =>
      fromContractToNative(maxPercentagePnLFloor),
    ),
    startWithObservable(maxPercentagePnLFloor),
  )

  return combineLatest({
    marketParameters: marketParameters$,
    openFee: openFee$,
    maxPercentagePnLFactor: maxPercentagePnLFactor$,
    maxPercentagePnLCap: maxPercentagePnLCap$,
    maxPercentagePnLFloor: maxPercentagePnLFloor$,
  }).pipe(
    map(({ marketParameters, ...rest }) => ({
      ...rest,
      ...marketParameters,
    })),
  )
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
function fetchMarketParameters(
  registryCore: RegistryCore,
  oracleAggregator: OracleAggregator,
  registryReader: RegistryReader,
  priceId: string,
) {
  return registryReader
    .getMarketParameters(registryCore.address, oracleAggregator.address, [
      priceId,
    ])
    .then(async ([a]) => {
      if (a == null) {
        throw new DisplayableError(`Price id ${priceId} not found`, {
          cause: "fetchMarketParameters",
        })
      }
      return {
        liquidationThreshold: fromContractToNative(a.liquidationThreshold),
        maxLeverage: fromContractToNative(a.maxLeverage),
        minLeverage: fromContractToNative(a.minLeverage),
        fundingFeeRate: fromContractToNative(a.fundingFeeRate),
        rolloverFeeRate: fromContractToNative(a.rolloverFeeRate),
        impactRefDepthLong: fromContractToNative(a.impactRefDepthLong),
        impactRefDepthShort: fromContractToNative(a.impactRefDepthShort),
        longFundingFee: SignedBalance.fromContractData(a.longFundingFee)
          .balance,
        shortFundingFee: SignedBalance.fromContractData(a.shortFundingFee)
          .balance,
        totalLong: fromContractToNative(a.totalLong),
        totalShort: fromContractToNative(a.totalShort),
        confMultiplier: fromContractToNative(a.confMultiplier),
        stableThreshold: fromContractToNative(a.staleThreshold),
        maxLong: fromContractToNative(a.maxTotalLong),
        maxShort: fromContractToNative(a.maxTotalShort),
      }
    })
}
