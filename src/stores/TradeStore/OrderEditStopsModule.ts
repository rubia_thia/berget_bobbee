import { ContractTransaction } from "ethers"
import { computed, makeObservable, observable } from "mobx"
import {
  ActionTrigger,
  ActionTriggerPatch,
} from "../../screens/TradeScreen/components/OrderCreation/types"
import {
  UpdateSLTPTriggerModalFormError,
  UpdateSLTPTriggerModalFormErrorType,
} from "../../screens/TradeScreen/components/UpdateSLTPTriggerModalContent.types"
import { MarginManipulation } from "../../screens/TradeScreen/types"
import { asyncAction, runAsyncAction } from "../../utils/asyncAction"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { mathIs } from "../../utils/numberHelpers/bigNumberExpressionParser"
import { Result } from "../utils/Result"
import { waitUntilExist$ } from "../utils/waitFor"
import { convertPatchTriggerToDisplay } from "./OrderCreationModule.service"
import { updateStops } from "./OrderEditStopsModule.service"
import { TradeCellModule } from "./TradeCellModule"
import { TradeStore } from "./TradeStore"

type UpdateStopFormData = { stopLoss: BigNumber; profitTarget: BigNumber }

export class OrderEditStopsModule {
  constructor(readonly trade: TradeCellModule, readonly store: TradeStore) {
    makeObservable(this)
  }

  @observable direction: MarginManipulation = MarginManipulation.Increase

  @observable stopLossTrigger?: ActionTriggerPatch.Price

  @computed get stopLossTriggerDisplay$(): ActionTrigger | null {
    const patch: undefined | ActionTriggerPatch =
      this.stopLossTrigger ??
      (this.trade.trade.stopLoss == null
        ? undefined
        : {
            sourceType: "price",
            price: this.trade.trade.stopLoss,
          })
    if (patch == null) return null
    return convertPatchTriggerToDisplay(
      this.trade.orderDirection,
      "stopLoss",
      this.trade.entryPrice,
      this.trade.leverage,
      patch,
    )
  }

  @observable takeProfitTrigger?: ActionTriggerPatch.Price

  @computed get takeProfitTriggerDisplay$(): ActionTrigger | null {
    const patch: undefined | ActionTriggerPatch =
      this.takeProfitTrigger ??
      (this.trade.trade.profitTarget == null
        ? undefined
        : {
            sourceType: "price",
            price: this.trade.trade.profitTarget,
          })
    if (patch == null) return null
    return convertPatchTriggerToDisplay(
      this.trade.orderDirection,
      "takeProfit",
      this.trade.entryPrice,
      this.trade.leverage,
      patch,
    )
  }

  @computed get stopLossTriggerEstimatedPnl$(): BigNumber {
    const trigger = waitUntilExist$(() => this.stopLossTriggerDisplay$)
    return this.trade.pnlForClosePrice(
      this.trade.remainingMargin,
      this.trade.calcAdjustedClosePrice$(
        this.trade.remainingMargin,
        trigger.price,
      ),
    )
  }

  @computed get takeProfitTriggerEstimatedPnl$(): BigNumber {
    const trigger = waitUntilExist$(() => this.takeProfitTriggerDisplay$)
    return this.trade.pnlForClosePrice(
      this.trade.remainingMargin,
      this.trade.calcAdjustedClosePrice$(
        this.trade.remainingMargin,
        trigger.price,
      ),
    )
  }

  @computed get formData$(): Result<
    UpdateStopFormData,
    UpdateSLTPTriggerModalFormError[]
  > {
    const errors: UpdateSLTPTriggerModalFormError[] = []

    const stopLoss = this.stopLossTriggerDisplay$
    if (this.stopLossTrigger != null && stopLoss != null) {
      if (
        mathIs`${stopLoss.percentage} < ${0}` ||
        mathIs`${stopLoss.percentage} > ${1}`
      ) {
        errors.push({
          type: UpdateSLTPTriggerModalFormErrorType.StopLossPercentageOutOfRange as const,
          maxStopLossPercentage: BigNumber.from(1),
          minStopLossPercentage: BigNumber.ZERO,
        })
      } else if (
        this.trade.isLong &&
        !mathIs`${stopLoss.price} >= ${this.trade.trade.liquidationPrice}`
      ) {
        errors.push({
          type: UpdateSLTPTriggerModalFormErrorType.StopLossShouldGteLiquidationPrice as const,
        })
      } else if (
        !this.trade.isLong &&
        !mathIs`${stopLoss.price} <= ${this.trade.trade.liquidationPrice}`
      ) {
        errors.push({
          type: UpdateSLTPTriggerModalFormErrorType.StopLossShouldLteLiquidationPrice as const,
        })
      }
    }

    const profitPercentage = this.takeProfitTriggerDisplay$?.percentage
    if (
      this.takeProfitTrigger != null &&
      profitPercentage != null &&
      (mathIs`${profitPercentage} <= ${0}` ||
        mathIs`${profitPercentage} > ${this.trade.maxTakeProfitPercentage$}`)
    ) {
      errors.push({
        type: UpdateSLTPTriggerModalFormErrorType.TakeProfitPercentageOutOfRange as const,
        maxTakeProfitPercentage: this.trade.maxTakeProfitPercentage$,
        minTakeProfitPercentage: BigNumber.ZERO,
      })
    }

    if (errors.length > 0) {
      return Result.error(errors)
    }

    return Result.ok({
      stopLoss: this.stopLossTriggerDisplay$?.price ?? BigNumber.from(0),
      profitTarget: this.takeProfitTriggerDisplay$?.price ?? BigNumber.from(0),
    })
  }

  @asyncAction async updateStops(
    formData: UpdateStopFormData,
    run = runAsyncAction,
  ): Promise<ContractTransaction> {
    const transaction = await run(
      updateStops({
        priceId: this.store.currentPriceId$,
        profitTarget: formData.profitTarget,
        stopLoss: formData.stopLoss,
        pyth: this.store.appEnvStore.pyth,
        tradeCore: this.store.contracts.tradingCore$,
        orderHash: this.trade.trade.orderHash,
      }),
    )
    this.store.orderHistory.editStops = undefined
    return transaction
  }
}
