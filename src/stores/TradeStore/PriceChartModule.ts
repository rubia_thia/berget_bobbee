import { computed, makeObservable, observable } from "mobx"
import {
  concatWith,
  filter,
  first,
  map,
  Observable,
  of,
  scan,
  skip,
  startWith,
  zipWith,
} from "rxjs"
import { PriceChange } from "../../components/LightweightCharts/LightweightCharts.types"
import { PriceChangeDirection } from "../../screens/TradeScreen/types"
import { hasAny, last } from "../../utils/arrayHelpers"
import { bigNumberComparer } from "../../utils/isShallowEqual/bigNumberComparer"
import { isShallowEqual } from "../../utils/isShallowEqual/isShallowEqual"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { math } from "../../utils/numberHelpers/bigNumberExpressionParser"
import { checkNever, isNotNull } from "../../utils/typeHelpers"
import { OneOrMore } from "../../utils/types"
import { AppEnvStore } from "../AppEnvStore/AppEnvStore"
import { PythPrice } from "../AppEnvStore/PythModule.service"
import { AuthStore } from "../AuthStore/AuthStore"
import { LazyValue } from "../utils/LazyValue"
import {
  get24HourPriceChange,
  getHistoricalPrices,
  getPriceBefore24Hours,
  HistoricalPriceTimeWindow,
} from "./PriceChartModule.hasura"
import { TradeInfoStore } from "./TradeInfoStore"
import { TradeCoreInfosType } from "./TradeStore.service"

export class PriceChartModule {
  constructor(
    readonly appEnvStore: AppEnvStore,
    readonly authStore: AuthStore,
    readonly tradeInfo: TradeInfoStore,
    private getPriceId$: () => string,
    private getPriceIdInfo$: () => TradeCoreInfosType,
  ) {
    makeObservable(this)
  }

  @computed
  get newestPythPriceObservable$(): Observable<PythPrice> {
    return this.appEnvStore.pyth.getCurrentPriceObservable(this.getPriceId$())
  }

  private newestPythPrice = new LazyValue(
    () => this.newestPythPriceObservable$,
    priceObservable => priceObservable,
  )

  @computed
  get currentPrice$(): BigNumber {
    return this.newestPythPrice.value$.price
  }

  currentPriceChangeDirection = new LazyValue(
    () => this.newestPythPriceObservable$,
    priceObservable =>
      priceObservable.pipe(
        zipWith(priceObservable.pipe(skip(1))),
        map(([prev, current]) =>
          BigNumber.isGt(current.price, prev.price)
            ? BigNumber.isEq(current.price, prev.price)
              ? PriceChangeDirection.Keep
              : PriceChangeDirection.Up
            : PriceChangeDirection.Down,
        ),
        startWith(PriceChangeDirection.Keep),
      ),
  )

  #priceChangeIn24 = new LazyValue(
    () => ({
      hasura: this.appEnvStore.priceHasura$,
      priceId: this.getPriceId$(),
    }),
    ({ priceId, hasura }) => get24HourPriceChange(hasura, priceId),
  )

  #priceBefore24Hour = new LazyValue(
    () => ({
      hasura: this.appEnvStore.priceHasura$,
      priceId: this.getPriceId$(),
    }),
    ({ hasura, priceId }) => getPriceBefore24Hours(hasura, priceId),
  )

  @computed
  get priceChangeIn24Hours$(): {
    priceDelta: BigNumber
    priceDeltaPercentage: BigNumber
  } {
    const priceBefore24Hours = this.#priceBefore24Hour.value$?.price

    if (priceBefore24Hours == null) {
      return {
        priceDelta: BigNumber.ZERO,
        priceDeltaPercentage: BigNumber.ZERO,
      }
    }

    const delta = BigNumber.minus(this.currentPrice$, priceBefore24Hours)
    const percentage = BigNumber.div(delta, this.currentPrice$)
    return {
      priceDelta: delta,
      priceDeltaPercentage: percentage,
    }
  }

  #newDataHighAndLows = new LazyValue(
    () => this.newestPythPriceObservable$,
    priceObservable =>
      priceObservable.pipe(
        scan(
          (acc, curr) => {
            return {
              max: math`max(${curr.price}, ${acc.max})`,
              min: math`min(${curr.price}, ${acc.min})`,
            }
          },
          {
            max: BigNumber.from(0),
            min: BigNumber.from(Number.MAX_VALUE),
          },
        ),
      ),
  )

  @computed get priceHighestIn24Hours$(): BigNumber {
    const newMax = this.#newDataHighAndLows.value$.max
    const oldMax = this.#priceChangeIn24.value$.highestPrice
    return math`max(${newMax}, ${oldMax})`
  }

  @computed get priceLowestIn24Hours$(): BigNumber {
    const newMin = this.#newDataHighAndLows.value$.min
    const oldMin = this.#priceChangeIn24.value$.lowestPrice
    return math`min(${newMin}, ${oldMin})`
  }

  @observable historicalTimeWindow: HistoricalPriceTimeWindow = "1m"

  @computed
  private get historicalTimeWindowAsNumber(): number {
    switch (this.historicalTimeWindow) {
      case "1m":
        return 1000 * 60
      case "15m":
        return 1000 * 60 * 15
      case "1h":
        return 1000 * 60 * 60
      case "1d":
        return 1000 * 60 * 60 * 24
      default:
        checkNever(this.historicalTimeWindow)
        return 1000 * 60
    }
  }

  #priceChangeObservable = new LazyValue(
    () =>
      [
        this.appEnvStore.priceHasura$,
        this.getPriceId$(),
        this.newestPythPriceObservable$,
        this.getPriceIdInfo$().confMultiplier,
        this.historicalTimeWindow,
        this.historicalTimeWindowAsNumber,
      ] as const,
    ([
      priceHasura,
      priceId,
      newestPythPriceObservable,
      confMultiplier,
      historicalTimeWindow,
      historicalTimeWindowAsNumber,
    ]) =>
      of(
        getHistoricalPrices(
          priceHasura,
          priceId,
          confMultiplier,
          historicalTimeWindow,
        ).pipe(
          filter((a): a is OneOrMore<PriceChange> => hasAny(a)),
          first(),
          concatWith(
            newestPythPriceObservable.pipe(
              scan(
                (acc, curr) =>
                  priceReducer(
                    acc,
                    curr,
                    historicalTimeWindowAsNumber,
                    confMultiplier,
                  ),
                null as null | PriceChangeWithReducerExtraFields,
              ),
              filter(isNotNull),
              map(a => [a] as OneOrMore<typeof a>),
            ),
          ),
        ),
      ),
    {
      reactionOptions: {
        equals: (a, b) => isShallowEqual(a, b, bigNumberComparer),
      },
    },
  )

  get priceChange$(): Observable<OneOrMore<PriceChange>> {
    return this.#priceChangeObservable.value$
  }

  currPriceChange = new LazyValue(
    () => [this.priceChange$] as const,
    ([priceChange]) =>
      priceChange.pipe(
        map(p => last(p)),
        filter(isNotNull),
      ),
  )

  lastFragmentPriceChange = new LazyValue(
    () => [this.priceChange$] as const,
    ([priceChange]) =>
      priceChange.pipe(
        scan(
          ([farPrev, prev], [curr]) => {
            if (prev == null) return [undefined, curr] as const
            return prev.time.getTime() === curr.time.getTime()
              ? ([farPrev, curr] as const)
              : ([prev, curr] as const)
          },
          [undefined, undefined] as readonly [
            prev: undefined | PriceChange,
            curr: undefined | PriceChange,
          ],
        ),
        map(([prev]) => prev),
        filter(isNotNull),
      ),
  )
}

interface PriceChangeWithReducerExtraFields extends PriceChange {
  endAsk: BigNumber
  endBid: BigNumber
}
const priceReducer = (
  prevResult: null | PriceChangeWithReducerExtraFields,
  price: PythPrice,
  timeWindow: number,
  confMultiplier: BigNumber,
): PriceChangeWithReducerExtraFields => {
  const roundUpTimestamp = (time: Date): Date =>
    new Date(Math.ceil(time.getTime() / timeWindow) * timeWindow)

  const time = roundUpTimestamp(price.publishTime)
  const isInTheSameSec =
    prevResult == null ? false : prevResult.time.getTime() === time.getTime()

  const ask = BigNumber.add(price.price)(
    BigNumber.mul(price.conf)(confMultiplier),
  )
  const bid = BigNumber.minus(price.price)(
    BigNumber.mul(price.conf)(confMultiplier),
  )

  if (prevResult == null) {
    return {
      time,
      start: price.price,
      end: price.price,
      high: price.price,
      low: price.price,
      ask,
      bid,
      highestAsk: ask,
      lowestBid: bid,
      endAsk: ask,
      endBid: bid,
    }
  } else if (!isInTheSameSec) {
    return {
      time,
      start: prevResult.end,
      end: price.price,
      high: price.price,
      low: price.price,
      ask,
      bid,
      highestAsk: BigNumber.max([prevResult.endAsk, ask]),
      lowestBid: BigNumber.min([prevResult.endBid, bid]),
      endAsk: ask,
      endBid: bid,
    }
  } else {
    return {
      time,
      start: prevResult.start,
      end: price.price,
      high: BigNumber.max([prevResult.high, price.price]),
      low: BigNumber.min([prevResult.low, price.price]),
      ask,
      bid,
      highestAsk: BigNumber.max([prevResult.highestAsk, ask]),
      lowestBid: BigNumber.min([prevResult.lowestBid, bid]),
      endAsk: ask,
      endBid: bid,
    }
  }
}
