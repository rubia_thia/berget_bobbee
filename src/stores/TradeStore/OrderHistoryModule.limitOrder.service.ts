import { Client, gql } from "@urql/core"
import { combineLatestWith, map, Observable, startWith } from "rxjs"
import {
  CloseLimitOrderEventHistoriesQuery,
  CloseLimitOrderEventHistoriesQueryVariables,
  CloseLimitOrderEventsSubscription,
  CloseLimitOrderEventsSubscriptionVariables,
  ExecuteLimitOrderEventHistoriesQuery,
  ExecuteLimitOrderEventHistoriesQueryVariables,
  ExecuteLimitOrderEventsSubscription,
  ExecuteLimitOrderEventsSubscriptionVariables,
  OpenLimitOrderEventHistoriesQuery,
  OpenLimitOrderEventHistoriesQueryVariables,
  OpenLimitOrderEventsSubscription,
  OpenLimitOrderEventsSubscriptionVariables,
  PartialCloseLimitOrderEventHistoriesQuery,
  PartialCloseLimitOrderEventHistoriesQueryVariables,
  PartialCloseLimitOrderEventsSubscription,
  PartialCloseLimitOrderEventsSubscriptionVariables,
  UpdateLimitOrderEventHistoriesQuery,
  UpdateLimitOrderEventHistoriesQueryVariables,
  UpdateLimitOrderEventsSubscription,
  UpdateLimitOrderEventsSubscriptionVariables,
} from "../../generated/graphql/graphql.generated"
import { fromContractToNative } from "../../utils/contractHelpers/bigNumberContracts"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import {
  ethAddressToHasuraAddress,
  fromUrqlSource,
  hasuraAddressToEth,
} from "../utils/hasuraClient"

export namespace LimitBookEvent {
  export interface Trade {
    isLong: boolean
    margin: BigNumber
    slippage: BigNumber
    stopLoss: null | BigNumber
    profitTarget: null | BigNumber
    leverage: BigNumber
    maxPercentagePnL: BigNumber
    openPrice: BigNumber
    liquidationPrice: BigNumber
  }

  export type Base = {
    priceId: string
    orderHash: string
    blockNumber: number
    blockTime: string
    txHash: string
  }

  export interface OpenLimitOrderEvent extends Base, Trade {
    type: "OpenLimitOrderEvent"
  }

  export interface UpdateOpenLimitOrderEvent extends Base, Trade {
    type: "UpdateOpenLimitOrderEvent"
  }

  export interface ExecuteLimitOrderEvent extends Base {
    type: "ExecuteLimitOrderEvent"
  }

  export interface CloseLimitOrderEvent extends Base {
    type: "CloseLimitOrderEvent"
  }

  export interface PartialCloseLimitOrderEvent extends Base, Trade {
    type: "PartialCloseLimitOrderEvent"
    closePercentage: BigNumber
  }
}

export type LimitBookEvent =
  | LimitBookEvent.OpenLimitOrderEvent
  | LimitBookEvent.UpdateOpenLimitOrderEvent
  | LimitBookEvent.ExecuteLimitOrderEvent
  | LimitBookEvent.CloseLimitOrderEvent
  | LimitBookEvent.PartialCloseLimitOrderEvent

export type OpeningLimitBookEvent =
  | LimitBookEvent.OpenLimitOrderEvent
  | LimitBookEvent.UpdateOpenLimitOrderEvent
  | LimitBookEvent.PartialCloseLimitOrderEvent

type HasuraTrade = Omit<
  OpenLimitOrderEventsSubscription["uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent"][number],
  "__typename"
>

type BaseTrade = Omit<
  CloseLimitOrderEventHistoriesQuery["uniwhale_v1_bsc_LimitBook_evt_CloseLimitOrderEvent"][number],
  "__typename"
>

const mapToNativeBase = (event: BaseTrade): LimitBookEvent.Base => ({
  priceId: hasuraAddressToEth(event.trade_priceId),
  orderHash: hasuraAddressToEth(event.orderHash),
  blockNumber: event.evt_block_number,
  blockTime: event.evt_block_time,
  txHash: hasuraAddressToEth(event.evt_tx_hash),
})

const mapToNativeTrade = (event: HasuraTrade): LimitBookEvent.Trade => ({
  isLong: event.trade_isBuy,
  leverage: fromContractToNative(event.trade_leverage),
  liquidationPrice: fromContractToNative(event.trade_liquidationPrice),
  margin: fromContractToNative(event.trade_margin),
  openPrice: fromContractToNative(event.trade_openPrice),
  slippage: fromContractToNative(event.trade_slippage),
  maxPercentagePnL: fromContractToNative(event.trade_maxPercentagePnL),
  profitTarget:
    event.trade_profitTarget === 0
      ? null
      : fromContractToNative(event.trade_profitTarget),
  stopLoss:
    event.trade_stopLoss === 0
      ? null
      : fromContractToNative(event.trade_stopLoss),
})

export function openLimitOrderEvents(
  client: Client,
  currentBlock: number,
  myAddress: string,
): Observable<LimitBookEvent.OpenLimitOrderEvent[]> {
  const openEventFragment = gql`
    fragment OpenLimitOrderEvent on uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent {
      trade_priceId
      orderHash
      trade_isBuy
      trade_margin
      trade_slippage
      trade_stopLoss
      trade_profitTarget
      trade_leverage
      trade_maxPercentagePnL
      trade_openPrice
      trade_liquidationPrice
      evt_block_number
      evt_block_time
      evt_tx_hash
    }
  `
  const historical = fromUrqlSource(
    client.query<
      OpenLimitOrderEventHistoriesQuery,
      OpenLimitOrderEventHistoriesQueryVariables
    >(
      gql`
        query OpenLimitOrderEventHistories($myAddress: bytea!) {
          uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent(
            where: { trade_user: { _eq: $myAddress } }
            order_by: { evt_block_number: desc }
          ) {
            ...OpenLimitOrderEvent
          }
          ${openEventFragment}
        }
      `,
      {
        myAddress: ethAddressToHasuraAddress(myAddress),
      },
    ),
    a =>
      a.data.uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent.map(
        (e): LimitBookEvent.OpenLimitOrderEvent => ({
          type: "OpenLimitOrderEvent",
          ...mapToNativeTrade(e),
          ...mapToNativeBase(e),
        }),
      ),
  )
  const newData = fromUrqlSource(
    client.subscription<
      OpenLimitOrderEventsSubscription,
      OpenLimitOrderEventsSubscriptionVariables
    >(
      gql`
        subscription OpenLimitOrderEvents(
          $myAddress: bytea!
          $currentBlock: bigint!
        ) {
          uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent(
            where: {
              trade_user: { _eq: $myAddress }
              evt_block_number: { _gt: $currentBlock }
            }
            order_by: { evt_block_number: desc }
          ) {
            ...OpenLimitOrderEvent
          }
        }
        ${openEventFragment}
      `,
      {
        myAddress: ethAddressToHasuraAddress(myAddress),
        currentBlock,
      },
    ),
    a =>
      a.data.uniwhale_v1_bsc_LimitBook_evt_OpenLimitOrderEvent.map(
        (e): LimitBookEvent.OpenLimitOrderEvent => ({
          type: "OpenLimitOrderEvent",
          ...mapToNativeTrade(e),
          ...mapToNativeBase(e),
        }),
      ),
  )
  return historical.pipe(
    combineLatestWith(
      newData.pipe(startWith([] as LimitBookEvent.OpenLimitOrderEvent[])),
    ),
    map(([a, b]) => b.concat(a)),
  )
}

export function updateLimitOrderEvents(
  client: Client,
  currentBlock: number,
  myAddress: string,
): Observable<LimitBookEvent.UpdateOpenLimitOrderEvent[]> {
  const updateLimitEventFragment = gql`
    fragment UpdateLimitOrderEvent on uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent {
      orderHash
      trade_priceId
      trade_isBuy
      trade_margin
      trade_slippage
      trade_stopLoss
      trade_profitTarget
      trade_leverage
      trade_maxPercentagePnL
      trade_openPrice
      trade_liquidationPrice
      evt_block_number
      evt_block_time
      evt_tx_hash
    }
  `
  const historical = fromUrqlSource(
    client.query<
      UpdateLimitOrderEventHistoriesQuery,
      UpdateLimitOrderEventHistoriesQueryVariables
    >(
      gql`
    query UpdateLimitOrderEventHistories($myAddress: bytea!) {
      uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent(
        where: { trade_user: { _eq: $myAddress } }
        order_by: { evt_block_number: desc }
      ) {
        ...UpdateLimitOrderEvent
      }
      ${updateLimitEventFragment}
    }
  `,
      {
        myAddress: ethAddressToHasuraAddress(myAddress),
      },
    ),
    a =>
      a.data.uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent.map(
        (e): LimitBookEvent.UpdateOpenLimitOrderEvent => ({
          type: "UpdateOpenLimitOrderEvent",
          ...mapToNativeTrade(e),
          ...mapToNativeBase(e),
        }),
      ),
  )
  const newData = fromUrqlSource(
    client.subscription<
      UpdateLimitOrderEventsSubscription,
      UpdateLimitOrderEventsSubscriptionVariables
    >(
      gql`
        subscription UpdateLimitOrderEvents(
          $myAddress: bytea!
          $currentBlock: bigint!
        ) {
          uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent(
            where: {
              trade_user: { _eq: $myAddress }
              evt_block_number: { _gt: $currentBlock }
            }
            order_by: { evt_block_number: desc }
          ) {
            ...UpdateLimitOrderEvent
          }
        }
        ${updateLimitEventFragment}
      `,
      {
        myAddress: ethAddressToHasuraAddress(myAddress),
        currentBlock,
      },
    ),
    a =>
      a.data.uniwhale_v1_bsc_LimitBook_evt_UpdateOpenLimitOrderEvent.map(
        (e): LimitBookEvent.UpdateOpenLimitOrderEvent => ({
          type: "UpdateOpenLimitOrderEvent",
          ...mapToNativeTrade(e),
          ...mapToNativeBase(e),
        }),
      ),
  )
  return historical.pipe(
    combineLatestWith(
      newData.pipe(startWith([] as LimitBookEvent.UpdateOpenLimitOrderEvent[])),
    ),
    map(([a, b]) => b.concat(a)),
  )
}

export function closeLimitOrderEvents(
  client: Client,
  currentBlock: number,
  myAddress: string,
): Observable<LimitBookEvent.CloseLimitOrderEvent[]> {
  const closeLimitOrderEventFragment = gql`
    fragment CloseLimitOrderEvent on uniwhale_v1_bsc_LimitBook_evt_CloseLimitOrderEvent {
      orderHash
      trade_priceId
      evt_block_number
      evt_block_time
      evt_tx_hash
    }
  `
  const historical = fromUrqlSource(
    client.query<
      CloseLimitOrderEventHistoriesQuery,
      CloseLimitOrderEventHistoriesQueryVariables
    >(
      gql`
        query CloseLimitOrderEventHistories($myAddress: bytea!) {
          uniwhale_v1_bsc_LimitBook_evt_CloseLimitOrderEvent(
            where: { trade_user: { _eq: $myAddress } }
            order_by: { evt_block_number: desc }
          ) {
            ...CloseLimitOrderEvent
          }
          ${closeLimitOrderEventFragment}
        }
      `,
      {
        myAddress: ethAddressToHasuraAddress(myAddress),
      },
    ),
    a =>
      a.data.uniwhale_v1_bsc_LimitBook_evt_CloseLimitOrderEvent.map(
        (e): LimitBookEvent.CloseLimitOrderEvent => ({
          type: "CloseLimitOrderEvent",
          ...mapToNativeBase(e),
        }),
      ),
  )
  const newData = fromUrqlSource(
    client.subscription<
      CloseLimitOrderEventsSubscription,
      CloseLimitOrderEventsSubscriptionVariables
    >(
      gql`
        subscription CloseLimitOrderEvents(
          $myAddress: bytea!
          $currentBlock: bigint!
        ) {
          uniwhale_v1_bsc_LimitBook_evt_CloseLimitOrderEvent(
            where: {
              trade_user: { _eq: $myAddress }
              evt_block_number: { _gt: $currentBlock }
            }
            order_by: { evt_block_number: desc }
          ) {
            ...CloseLimitOrderEvent
          }
        }
        ${closeLimitOrderEventFragment}
      `,
      {
        myAddress: ethAddressToHasuraAddress(myAddress),
        currentBlock,
      },
    ),
    a =>
      a.data.uniwhale_v1_bsc_LimitBook_evt_CloseLimitOrderEvent.map(
        (e): LimitBookEvent.CloseLimitOrderEvent => ({
          type: "CloseLimitOrderEvent",
          ...mapToNativeBase(e),
        }),
      ),
  )
  return historical.pipe(
    combineLatestWith(
      newData.pipe(startWith([] as LimitBookEvent.CloseLimitOrderEvent[])),
    ),
    map(([a, b]) => b.concat(a)),
  )
}

export function executeLimitOrderEvents(
  client: Client,
  currentBlock: number,
  myAddress: string,
): Observable<LimitBookEvent.ExecuteLimitOrderEvent[]> {
  const executeLimitOrderEventFragment = gql`
    fragment ExecuteLimitOrderEvent on uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent {
      orderHash
      trade_priceId
      evt_block_number
      evt_block_time
      evt_tx_hash
    }
  `
  const historical = fromUrqlSource(
    client.query<
      ExecuteLimitOrderEventHistoriesQuery,
      ExecuteLimitOrderEventHistoriesQueryVariables
    >(
      gql`
        query ExecuteLimitOrderEventHistories($myAddress: bytea!) {
          uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent(
            where: { trade_user: { _eq: $myAddress } }
            order_by: { evt_block_number: desc }
          ) {
            ...ExecuteLimitOrderEvent
          }
          ${executeLimitOrderEventFragment}
        }
      `,
      {
        myAddress: ethAddressToHasuraAddress(myAddress),
      },
    ),
    a =>
      a.data.uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent.map(
        (e): LimitBookEvent.ExecuteLimitOrderEvent => ({
          type: "ExecuteLimitOrderEvent",
          ...mapToNativeBase(e),
        }),
      ),
  )
  const newData = fromUrqlSource(
    client.subscription<
      ExecuteLimitOrderEventsSubscription,
      ExecuteLimitOrderEventsSubscriptionVariables
    >(
      gql`
        subscription ExecuteLimitOrderEvents(
          $myAddress: bytea!
          $currentBlock: bigint!
        ) {
          uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent(
            where: {
              trade_user: { _eq: $myAddress }
              evt_block_number: { _gt: $currentBlock }
            }
            order_by: { evt_block_number: desc }
          ) {
            ...ExecuteLimitOrderEvent
          }
        }
        ${executeLimitOrderEventFragment}
      `,
      {
        myAddress: ethAddressToHasuraAddress(myAddress),
        currentBlock,
      },
    ),
    a =>
      a.data.uniwhale_v1_bsc_LimitBook_evt_ExecuteLimitOrderEvent.map(
        (e): LimitBookEvent.ExecuteLimitOrderEvent => ({
          type: "ExecuteLimitOrderEvent",
          ...mapToNativeBase(e),
        }),
      ),
  )
  return historical.pipe(
    combineLatestWith(
      newData.pipe(startWith([] as LimitBookEvent.ExecuteLimitOrderEvent[])),
    ),
    map(([a, b]) => b.concat(a)),
  )
}

export function partialCloseLimitOrderEvents(
  client: Client,
  currentBlock: number,
  myAddress: string,
): Observable<LimitBookEvent.PartialCloseLimitOrderEvent[]> {
  const partialCloseLimitEventFragment = gql`
    fragment PartialCloseLimitOrderEvent on uniwhale_v1_bsc_LimitBook_evt_PartialCloseLimitOrderEvent {
      orderHash
      trade_priceId
      trade_isBuy
      trade_margin
      trade_slippage
      trade_stopLoss
      trade_profitTarget
      trade_leverage
      trade_maxPercentagePnL
      trade_openPrice
      trade_liquidationPrice
      evt_block_number
      evt_block_time
      evt_tx_hash
      closePercent
    }
  `
  const historical = fromUrqlSource(
    client.query<
      PartialCloseLimitOrderEventHistoriesQuery,
      PartialCloseLimitOrderEventHistoriesQueryVariables
    >(
      gql`
        query PartialCloseLimitOrderEventHistories($myAddress: bytea!) {
          uniwhale_v1_bsc_LimitBook_evt_PartialCloseLimitOrderEvent(
            where: { trade_user: { _eq: $myAddress } }
            order_by: { evt_block_number: desc }
          ) {
            ...PartialCloseLimitOrderEvent
          }
          ${partialCloseLimitEventFragment}
        }
      `,
      {
        myAddress: ethAddressToHasuraAddress(myAddress),
      },
    ),
    a =>
      a.data.uniwhale_v1_bsc_LimitBook_evt_PartialCloseLimitOrderEvent.map(
        (e): LimitBookEvent.PartialCloseLimitOrderEvent => ({
          type: "PartialCloseLimitOrderEvent",
          ...mapToNativeTrade(e),
          ...mapToNativeBase(e),
          closePercentage: fromContractToNative(e.closePercent),
        }),
      ),
  )
  const newData = fromUrqlSource(
    client.subscription<
      PartialCloseLimitOrderEventsSubscription,
      PartialCloseLimitOrderEventsSubscriptionVariables
    >(
      gql`
        subscription PartialCloseLimitOrderEvents(
          $myAddress: bytea!
          $currentBlock: bigint!
        ) {
          uniwhale_v1_bsc_LimitBook_evt_PartialCloseLimitOrderEvent(
            where: {
              trade_user: { _eq: $myAddress }
              evt_block_number: { _gt: $currentBlock }
            }
            order_by: { evt_block_number: desc }
          ) {
            ...PartialCloseLimitOrderEvent
          }
        }
        ${partialCloseLimitEventFragment}
      `,
      {
        myAddress: ethAddressToHasuraAddress(myAddress),
        currentBlock,
      },
    ),
    a =>
      a.data.uniwhale_v1_bsc_LimitBook_evt_PartialCloseLimitOrderEvent.map(
        (e): LimitBookEvent.PartialCloseLimitOrderEvent => ({
          type: "PartialCloseLimitOrderEvent",
          ...mapToNativeTrade(e),
          ...mapToNativeBase(e),
          closePercentage: fromContractToNative(e.closePercent),
        }),
      ),
  )
  return historical.pipe(
    combineLatestWith(
      newData.pipe(
        startWith([] as LimitBookEvent.PartialCloseLimitOrderEvent[]),
      ),
    ),
    map(([a, b]) => b.concat(a)),
  )
}
