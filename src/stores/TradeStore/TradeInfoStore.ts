import { computed, makeObservable } from "mobx"
import { computedFn, createTransformer } from "mobx-utils"
import { memoizeWith } from "ramda"
import { from } from "rxjs"
import {
  fromContractToNative,
  fromNativeToContract,
} from "../../utils/contractHelpers/bigNumberContracts"
import { bigNumberComparer } from "../../utils/isShallowEqual/bigNumberComparer"
import { isShallowEqual } from "../../utils/isShallowEqual/isShallowEqual"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { math } from "../../utils/numberHelpers/bigNumberExpressionParser"
import { props } from "../../utils/promiseHelpers"
import { TokenInfo } from "../../utils/TokenInfo"
import { AppEnvStore } from "../AppEnvStore/AppEnvStore"
import { AuthStore } from "../AuthStore/AuthStore"
import { ContractStore } from "../contracts/ContractStore"
import { CurrencyStore } from "../CurrencyStore/CurrencyStore"
import {
  ERC20Tokens,
  StableToken,
} from "../CurrencyStore/CurrencyStore.service"
import {
  getTradeStats,
  TradeStats,
} from "../LiquidityStore/LiquidityStore.service"
import { LazyValue } from "../utils/LazyValue"
import { getAccumulatedFee, tradeCoreInfos } from "./TradeStore.service"

export class TradeInfoStore {
  constructor(
    readonly appEnvStore: AppEnvStore,
    readonly authStore: AuthStore,
    readonly currencyStore: CurrencyStore,
    readonly contracts: ContractStore,
    readonly contractsForEvent: ContractStore,
  ) {
    makeObservable(this)
  }

  quoteToken = ERC20Tokens.AnchorToken
  @computed get quoteTokenInfo$(): TokenInfo {
    return this.currencyStore.tokenInfo$(this.quoteToken)
  }

  #info = memoizeWith(
    (priceId: string, walletAddress: string) => `${priceId}-${walletAddress}`,
    (priceId: string, walletAddress: string) =>
      new LazyValue(
        () =>
          [
            priceId,
            walletAddress,
            this.contracts.tradingCore$,
            this.contracts.registryCore$,
            this.contractsForEvent.registryCore$,
            this.contractsForEvent.fees$,
            this.contracts.registryReader$,
            this.contracts.oracleAggregator$,
            this.appEnvStore.currentBlockObservable$,
          ] as const,
        ([
          priceId,
          walletAddress,
          tradingCore,
          registryCore,
          registryCoreForEvent,
          feesForEvent,
          registryReader,
          oracleAggregator,
          currentBlockObservable,
        ]) =>
          tradeCoreInfos(
            walletAddress,
            priceId,
            tradingCore,
            registryCore,
            registryCoreForEvent,
            feesForEvent,
            registryReader,
            oracleAggregator,
            currentBlockObservable,
          ),
      ),
  )

  getAccumulatedFeeFromContract = memoizeWith(
    (hash, pct) => `${hash}-${pct}`,
    (orderHash: string, closePercentage: BigNumber) =>
      new LazyValue(
        () =>
          [
            this.appEnvStore.currentBlockObservable$,
            this.contracts.registryCore$,
            this.contracts.registryReader$,
          ] as const,
        ([currentBlockObservable, registryCore, registryReader]) =>
          getAccumulatedFee(
            currentBlockObservable,
            registryCore,
            registryReader,
            orderHash,
            closePercentage,
          ),
      ),
  )

  infoForPriceId$ = computedFn(
    (priceId: string, account: string) => this.#info(priceId, account).value$,
    { equals: (a, b) => isShallowEqual(a, b, bigNumberComparer) },
  )

  longImbalanceForPriceId$ = createTransformer((priceId: string) => {
    /**
     * https://github.com/uniwhale-io/uniwhale-v1/blob/5dc2d014725b775f5f7638800b80123d001b98ea/packages/contracts/core-v1/contracts/interfaces/AbstractRegistry.sol#L338-L342
     */

    const info = this.infoForPriceId$(priceId, this.authStore.account$)
    const totalLongPerPriceId = BigNumber.max([info.totalLong, 1])
    const totalShortPerPriceId = BigNumber.max([info.totalShort, 1])
    if (BigNumber.isZero(totalLongPerPriceId)) return BigNumber.ZERO
    return math`(${totalLongPerPriceId} - ${totalShortPerPriceId}) / ${totalLongPerPriceId}`
  })

  shortImbalanceForPriceId$ = createTransformer((priceId: string) => {
    /**
     * https://github.com/uniwhale-io/uniwhale-v1/blob/5dc2d014725b775f5f7638800b80123d001b98ea/packages/contracts/core-v1/contracts/interfaces/AbstractRegistry.sol#L352-L356
     */

    const info = this.infoForPriceId$(priceId, this.authStore.account$)
    const totalLongPerPriceId = BigNumber.max([info.totalLong, 1])
    const totalShortPerPriceId = BigNumber.max([info.totalShort, 1])
    if (BigNumber.isZero(totalShortPerPriceId)) return BigNumber.ZERO
    return math`(${totalShortPerPriceId} - ${totalLongPerPriceId}) / ${totalShortPerPriceId}`
  })

  liquidityInfo = new LazyValue(
    () => ({
      lp: this.contracts.liquidityPool$,
      esunw: this.contracts.esUniwhaleToken$,
      revenuePool: this.contracts.revenuePool$,
      registry: this.contracts.registryCore$,
      refresh: this.currencyStore.refreshSignal$,
    }),
    ({ lp: liquidityPool, revenuePool, esunw, registry }) =>
      from(
        props({
          baseBalance: liquidityPool
            .getBaseBalance()
            .then(fromContractToNative),
          totalSupply: liquidityPool.totalSupply().then(fromContractToNative),
          mintFee: liquidityPool.mintFee().then(fromContractToNative),
          burnFee: liquidityPool.burnFee().then(fromContractToNative),
          minCollateral: registry.minCollateral().then(fromContractToNative),
          feeFactor: registry.feeFactor().then(fromContractToNative),
          feeFactorUlpShare: revenuePool
            .getShare(liquidityPool.address)
            .then(fromContractToNative),
          feeFactorEsUnwShare: revenuePool
            .getShare(esunw.address)
            .then(fromContractToNative),
        }).then(
          ({
            feeFactor,
            feeFactorUlpShare,
            feeFactorEsUnwShare,
            ...restInfo
          }) => {
            return {
              ...restInfo,
              feeRebateFactor: feeFactor,
              ulpFeeRebateFactor: BigNumber.mul(feeFactor, feeFactorUlpShare),
              esUnwFeeRebateFactor: BigNumber.mul(
                feeFactor,
                feeFactorEsUnwShare,
              ),
            }
          },
        ),
      ),
  )

  @computed get allSupportedStableToken$(): StableToken[] {
    return (
      [
        ERC20Tokens.AnchorToken,
        ERC20Tokens.TBUSD,
        ERC20Tokens.TUSDT,
        ERC20Tokens.BUSD,
        ERC20Tokens.USDC,
      ] as StableToken[]
    ).filter(this.currencyStore.contractAddress$)
  }

  swapPoolFee = BigNumber.from(0.0025)

  #exchangeRate = memoizeWith(
    (a, b) => `${a} ${b}`,
    (ft: ERC20Tokens, tt: ERC20Tokens) =>
      new LazyValue(
        () => ({
          fromToken: this.currencyStore.contractAddress$(ft),
          to: this.currencyStore.contractAddress$(tt),
          swap: this.contracts.swapRouter$,
          poolFee: this.swapPoolFee,
          refresh: this.currencyStore.refreshSignal$,
        }),
        ({ fromToken, to, swap, poolFee }) =>
          from(
            swap
              .getAmountGivenIn({
                poolFee: fromNativeToContract(poolFee, { decimals: 6 }),
                amountIn: fromNativeToContract(BigNumber.from(1)),
                amountOutMinimum: fromNativeToContract(BigNumber.from(0)),
                tokenIn: fromToken,
                tokenOut: to,
              })
              .then(fromContractToNative),
          ),
      ),
  )

  exchangeRate$ = computedFn((ft: ERC20Tokens, tt: ERC20Tokens) =>
    ft === tt ? BigNumber.from(1) : this.#exchangeRate(ft, tt).value$,
  )

  #tradeStats = new LazyValue(
    () => this.appEnvStore.hasura$,
    client => getTradeStats(client),
  )

  @computed get tradeStats$(): TradeStats {
    return this.#tradeStats.value$
  }
}
