import { ContractTransaction } from "ethers"
import { action, computed, makeObservable, observable } from "mobx"
import {
  ClosePositionModalFormPrice,
  ClosePositionModalFormPricingPatch,
} from "../../screens/TradeScreen/components/ClosePositionModalContent/ClosePositionModalContent"
import {
  ClosePositionModalFormError,
  ClosePositionModalFormErrorType,
} from "../../screens/TradeScreen/components/ClosePositionModalContent/ClosePositionModalContent.types"
import { asyncAction, runAsyncAction } from "../../utils/asyncAction"
import { DisplayableError } from "../../utils/errorHelpers"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { math } from "../../utils/numberHelpers/bigNumberExpressionParser"
import { readResource, suspenseResource } from "../../utils/SuspenseResource"
import { Result } from "../utils/Result"
import { closeMarketOrder } from "./OrderCreationModule.service"
import { TradeCellModule } from "./TradeCellModule"
import { TradeStore } from "./TradeStore"

export type CancelFormData = {
  orderHash: string
  limitPrice: BigNumber
  percentage: BigNumber
}

export class OrderCloseModule {
  constructor(readonly trade: TradeCellModule, readonly store: TradeStore) {
    makeObservable(this)
  }

  @observable.ref positionSize?: {
    percentage: BigNumber
    amount: BigNumber
  }

  @observable.ref closingPrice: ClosePositionModalFormPrice = {
    type: "marketPrice",
    price: suspenseResource(() => this.trade.currentMarkPrice$),
  }

  @action setClosingPrice(input: ClosePositionModalFormPricingPatch): void {
    this.closingPrice =
      input.type === "marketPrice"
        ? {
            type: "marketPrice",
            price: suspenseResource(() => this.trade.currentMarkPrice$),
          }
        : input
  }

  @computed get cancelForm$(): Result<
    CancelFormData,
    ClosePositionModalFormError
  > {
    if (this.store.orderHistory.wasClosed$(this.trade.trade.orderHash)) {
      return Result.error({
        type: ClosePositionModalFormErrorType.OrderNotFound as const,
      })
    }
    const position = this.positionSize?.amount
    if (
      this.closingPrice.type === "limitPrice" &&
      readResource(this.closingPrice.price) == null
    ) {
      return Result.error({
        type: ClosePositionModalFormErrorType.EnterPrice as const,
      })
    }
    if (position == null || BigNumber.isLte(position, 0)) {
      return Result.error({
        type: ClosePositionModalFormErrorType.EnterPosition as const,
      })
    }
    if (BigNumber.isGt(position, this.trade.remainingPosition)) {
      return Result.error({
        type: ClosePositionModalFormErrorType.InsufficientPositionSize as const,
      })
    }
    return Result.ok({
      orderHash: this.trade.trade.orderHash,
      limitPrice:
        this.closingPrice.type === "marketPrice"
          ? this.trade.isLong
            ? BigNumber.from(0)
            : math`${this.trade.currentMarkPrice$} x ${2}`
          : readResource(this.closingPrice.price)!,
      percentage: math`${position} / ${this.trade.remainingPosition}`,
    })
  }

  @asyncAction async cancelOrder(
    formData: CancelFormData,
    run = runAsyncAction,
  ): Promise<ContractTransaction> {
    if (this.store.orderHistory.wasClosed$(formData.orderHash)) {
      throw new DisplayableError("Order was already closed")
    }
    try {
      const transaction = await run(
        closeMarketOrder({
          pyth: this.store.appEnvStore.pyth,
          tradeCore: this.store.contracts.tradingCore$,
          priceId: this.trade.trade.priceId,
          orderHash: formData.orderHash,
          limitPrice: formData.limitPrice,
          percentage: formData.percentage,
        }),
      )
      this.store.orderHistory.closingOrder = undefined
      return transaction
    } finally {
      this.store.orderHistory.closingOrderHashes =
        this.store.orderHistory.closingOrderHashes.filter(
          a => a !== formData.orderHash,
        )
    }
  }
}
