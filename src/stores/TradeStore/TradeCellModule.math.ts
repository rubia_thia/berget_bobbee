import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { math } from "../../utils/numberHelpers/bigNumberExpressionParser"

export const calcClosePosition = (
  trade: {
    leverage: BigNumber
  },
  closingMargin: BigNumber,
): BigNumber => {
  return math`${trade.leverage} x ${closingMargin}`
}

export interface AccruedFee {
  fundingFee: BigNumber
  rolloverFee: BigNumber
  lastUpdate: number
}

export interface GetAccumulatedFundingFeeContextInfo {
  totalLong: BigNumber
  totalShort: BigNumber
  longFundingFee: BigNumber
  shortFundingFee: BigNumber
  rolloverFeeRate: BigNumber
  currentBlock: number
}
interface GetAccumulatedFundingFeeTrade {
  isLong: boolean
  leverage: BigNumber
  margin: BigNumber
  accruedFee: AccruedFee
  fundingFeeBase: BigNumber
}
export const getAccumulatedFee = (
  contextInfo: GetAccumulatedFundingFeeContextInfo,
  trade: GetAccumulatedFundingFeeTrade,
  closingMargin: BigNumber,
): {
  fundingFee: BigNumber
  rolloverFee: BigNumber
} => {
  /**
   * https://github.com/uniwhale-io/uniwhale-v1/blob/654946f168a3461791e8bf5306a2dec802860fc5/packages/contracts/core-v1/contracts/RegistryCore.sol#L394-L432
   */
  /**
   * uint256 _totalPosition = isBuy
   *   ? totalLongPerPriceId[priceId]
   *   : totalShortPerPriceId[priceId];
   * fundingFee = isBuy
   *   ? _getLatestFundingFeeBalance(
   *     _longFundingFeePerPriceId[priceId],
   *     _totalPosition,
   *     totalShortPerPriceId[priceId],
   *     fundingFeePerPriceId[priceId]
   *   )
   *   : _getLatestFundingFeeBalance(
   *     _shortFundingFeePerPriceId[priceId],
   *     _totalPosition,
   *     totalLongPerPriceId[priceId],
   *     fundingFeePerPriceId[priceId]
   *   );
   *
   * int256 accruedFundingFee = fundingFee.balance;
   * if (_totalPosition > 0)
   *   accruedFundingFee = accruedFundingFee
   *     .mulDown(leverage)
   *     .mulDown(margin)
   *     .divDown(_totalPosition);
   * accruedFundingFee = accruedFundingFee.sub(
   *   _fundingFeeBaseByOrderHash[orderHash]
   * );
   *
   * accruedFee = _accruedFeeByOrderHash[orderHash];
   * accruedFee.fundingFee = int256(accruedFee.fundingFee)
   *   .add(accruedFundingFee)
   *   .toInt128();
   *
   * accruedFee.rolloverFee = uint256(accruedFee.rolloverFee)
   *   .add(
   *     uint256(rolloverFeePerPriceId[priceId]).mulDown(margin) *
   *       (block.number.sub(uint256(accruedFee.lastUpdate)))
   *   )
   *   .toUint128();
   * accruedFee.lastUpdate = block.number.toUint32();
   */

  const totalPosition = trade.isLong
    ? contextInfo.totalLong
    : contextInfo.totalShort

  let accruedFundingFee = trade.isLong
    ? contextInfo.longFundingFee
    : contextInfo.shortFundingFee
  if (BigNumber.isGtZero(totalPosition)) {
    accruedFundingFee = math`(${accruedFundingFee} x
      ${trade.leverage} x
      ${trade.margin}
    ) / ${totalPosition}`
  }
  accruedFundingFee = math`${accruedFundingFee} - ${trade.fundingFeeBase}`

  const fundingFee = math`${trade.accruedFee.fundingFee} + ${accruedFundingFee}`
  const rolloverFee = math`${trade.accruedFee.rolloverFee} +
    (${contextInfo.rolloverFeeRate} x ${trade.margin} x
      (${contextInfo.currentBlock} - ${trade.accruedFee.lastUpdate})
    )`

  /**
   * https://github.com/uniwhale-io/uniwhale-v1/blob/654946f168a3461791e8bf5306a2dec802860fc5/packages/contracts/core-v1/contracts/RegistryCore.sol#L82-L85
   */
  const closePercentage = math`${closingMargin} / ${trade.margin}`
  return {
    fundingFee: math`${fundingFee} * ${closePercentage}`,
    rolloverFee: math`${rolloverFee} * ${closePercentage}`,
  }
}

export const getAdjustedClosePrice = (
  contextInfo: GetAccumulatedFundingFeeContextInfo &
    GetTradeCloseSlippageContextInfo,
  trade: GetAccumulatedFundingFeeTrade & {
    isLong: boolean
    openPrice: BigNumber
    openSlippage: BigNumber
    leverage: BigNumber
  },
  markPrice: BigNumber,
  closingMargin: BigNumber,
): BigNumber => {
  /**
   * https://github.com/uniwhale-io/uniwhale-v1/blob/07c8f0a71a985e5333fa28d294999020ecaff15c/packages/contracts/core-v1/contracts/TradingCore.sol#L576-L633
   */
  /**
   * uint256 closePosition = trade.leverage.mulDown(closeMargin);
   *
   * (onCloseTrade.fundingFee, onCloseTrade.rolloverFee) = registry
   *   .getAccumulatedFee(orderHash, closeMargin);
   *
   * int256 accumulatedFee = onCloseTrade.fundingFee.add(
   *   onCloseTrade.rolloverFee
   * );
   *
   * uint256 openNet = trade.isBuy
   *   ? trade.openPrice.add(trade.slippage)
   *   : trade.openPrice.sub(trade.slippage);
   *
   * onCloseTrade.closeNet = trade.isBuy
   *   ? int256(
   *     closePrice.sub(accumulatedFee.mulDown(openNet).divDown(closePosition))
   *   ).toUint256()
   *   : int256(
   *     closePrice.add(accumulatedFee.mulDown(openNet).divDown(closePosition))
   *   ).toUint256();
   *
   * // we dont need to handle bot related logic
   *
   * onCloseTrade.slippage = registry.getSlippage(
   *   trade.priceId,
   *   !trade.isBuy,
   *   onCloseTrade.closeNet,
   *   closePosition
   * );
   * onCloseTrade.closeNet = trade.isBuy
   *   ? onCloseTrade.closeNet.sub(onCloseTrade.slippage)
   *   : onCloseTrade.closeNet.add(onCloseTrade.slippage);
   */

  const closePosition = calcClosePosition(trade, closingMargin)
  const { fundingFee, rolloverFee } = getAccumulatedFee(
    contextInfo,
    trade,
    closingMargin,
  )
  const accumulatedFee = math`${fundingFee} + ${rolloverFee}`

  const openNet = trade.isLong
    ? math`${trade.openPrice} + ${trade.openSlippage}`
    : math`${trade.openPrice} - ${trade.openSlippage}`
  const adjustment = math`${accumulatedFee} x ${openNet} / ${closePosition}`
  const closePriceAffectedByFee = trade.isLong
    ? math`${markPrice} - ${adjustment}`
    : math`${markPrice} + ${adjustment}`

  const closeSlippage = getTradeCloseSlippage(
    contextInfo,
    trade,
    closePriceAffectedByFee,
    closingMargin,
  )

  return trade.isLong
    ? math`${closePriceAffectedByFee} - ${closeSlippage}`
    : math`${closePriceAffectedByFee} + ${closeSlippage}`
}

export interface GetTradeCloseSlippageContextInfo {
  totalLong: BigNumber
  totalShort: BigNumber
  impactRefDepthLong: BigNumber
  impactRefDepthShort: BigNumber
}
export const getTradeCloseSlippage = (
  contextInfo: GetTradeCloseSlippageContextInfo,
  trade: {
    isLong: boolean
    leverage: BigNumber
  },
  closePrice: BigNumber,
  closingMargin: BigNumber,
): BigNumber => {
  /**
   * https://github.com/uniwhale-io/uniwhale-v1/blob/88b0b09d869286d5903032da39eeaaa0e2d74c95/packages/contracts/core-v1/contracts/interfaces/AbstractRegistry.sol#L308-L323
   */
  const closingPosition = calcClosePosition(trade, closingMargin)
  /**
   * uint256 impact = (
   *   (isBuy ? totalLong[priceId] : totalShort[priceId])
   *     .add(position)
   * ).divDown(
   *     isBuy
   *       ? impactRefDepthLongPerPriceId[priceId]
   *       : impactRefDepthShortPerPriceId[priceId]
   *   );
   * return price.mulDown(impact).divDown(uint256(100e18));
   */
  const impact = math`(${
    trade.isLong ? contextInfo.totalLong : contextInfo.totalShort
  } + ${closingPosition}) / ${
    trade.isLong
      ? contextInfo.impactRefDepthLong
      : contextInfo.impactRefDepthShort
  }`
  return math`${closePrice} * ${impact} / ${100}`
}
