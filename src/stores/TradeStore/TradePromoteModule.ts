import { computed, makeObservable } from "mobx"
import { from } from "rxjs"
import { fromContractToNative } from "../../utils/contractHelpers/bigNumberContracts"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import {
  math,
  mathIs,
} from "../../utils/numberHelpers/bigNumberExpressionParser"
import { LazyValue } from "../utils/LazyValue"
import { TradeStore } from "./TradeStore"

export class TradePromoteModule {
  constructor(readonly store: TradeStore) {
    makeObservable(this)
  }

  #blocksPerRun = new LazyValue(
    () => ({
      traderFarm: this.store.contracts.traderFarm$,
    }),
    ({ traderFarm }) => from(traderFarm.blocksPerRun().then(n => n.toNumber())),
  )

  @computed
  get blocksPerRun$(): number {
    return this.#blocksPerRun.value$
  }

  #emissionPerBlock = new LazyValue(
    () => ({
      traderFarm: this.store.contracts.traderFarm$,
      esUNW: this.store.contracts.esUniwhaleToken$,
    }),
    ({ esUNW, traderFarm }) =>
      from(esUNW.emissions(traderFarm.address).then(fromContractToNative)),
  )

  @computed get emissionPerBlock$(): BigNumber {
    return this.#emissionPerBlock.value$
  }

  @computed get emissionPerDay$(): BigNumber {
    return math`${this.emissionPerBlock$} * ${this.blocksPerRun$}`
  }

  @computed get remainingBlock$(): number {
    return (
      (this.currentRun$ + 1) * this.blocksPerRun$ -
      this.store.appEnvStore.currentBlock$
    )
  }

  @computed get currentRun$(): number {
    return Math.floor(
      this.store.appEnvStore.appOpenBlockHeight$ / this.blocksPerRun$,
    )
  }

  #myTradeVolume = new LazyValue(
    () => ({
      traderFarm: this.store.contracts.traderFarm$,
      user: this.store.authStore.account$,
      currentRun: this.currentRun$,
    }),
    ({ user, traderFarm, currentRun }) =>
      from(
        traderFarm
          .volumeByUserPerRun(currentRun, user)
          .then(fromContractToNative),
      ),
  )

  @computed get myTradeVolume$(): BigNumber {
    return this.#myTradeVolume.value$
  }

  #totalTradingVolume = new LazyValue(
    () => ({
      traderFarm: this.store.contracts.traderFarm$,
      currentRun: this.currentRun$,
    }),
    ({ traderFarm, currentRun }) =>
      from(traderFarm.volumePerRun(currentRun).then(fromContractToNative)),
  )

  @computed get totalTradingVolume$(): BigNumber {
    return this.#totalTradingVolume.value$
  }

  @computed get myEmissionPercentage$(): BigNumber {
    if (mathIs`${this.totalTradingVolume$} === ${0}`) {
      return BigNumber.ZERO
    }
    return math`${this.myTradeVolume$} / ${this.totalTradingVolume$}`
  }

  @computed get myEstimatedEmission$(): BigNumber {
    return math`${this.emissionPerDay$} * ${this.myEmissionPercentage$}`
  }
}
