import { computed, makeObservable } from "mobx"
import { DebugPositionRecord } from "../../screens/Debug/components/PositionsTabContent/types"
import {
  OrderDirection,
  OrderType,
  PositionRecord,
  TradeRecord,
} from "../../screens/TradeScreen/types"
import { DisplayableError } from "../../utils/errorHelpers"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import {
  math,
  mathIs,
} from "../../utils/numberHelpers/bigNumberExpressionParser"
import { suspenseResource } from "../../utils/SuspenseResource"
import { TokenInfo } from "../../utils/TokenInfo"
import { checkNever } from "../../utils/typeHelpers"
import { safelyGet } from "../utils/waitFor"
import { fromTradeToDebugRecord } from "./DebugPositionRecordModule"
import {
  getExitPriceFromProfitPercentage,
  isShouldOverflowOpenPrice,
} from "./OrderCreationModule.service"
import { MarketOrderEvent } from "./OrderHistoryModule.marketOrder.service"
import {
  calcClosePosition,
  getAccumulatedFee,
  getAdjustedClosePrice,
} from "./TradeCellModule.math"
import { TradeInfoStore } from "./TradeInfoStore"
import { TradeCoreInfosType } from "./TradeStore.service"

export class TradeCellModule {
  constructor(
    readonly trade: MarketOrderEvent,
    readonly store: TradeInfoStore,
    readonly openEvent: MarketOrderEvent.OpenEvent,
  ) {
    makeObservable(this)
  }

  @computed get isClosingTrade(): boolean {
    return (
      this.trade.type === "close" &&
      BigNumber.isEq(this.trade.closePercentage)(1)
    )
  }

  @computed get entryPrice(): BigNumber {
    const op = this.trade.isLong ? "+" : "-"
    const { openPrice, slippage } = this.trade
    return math`${openPrice} ${op} ${slippage}`
  }

  @computed get closedPrice(): BigNumber {
    if (this.trade.type !== "close") {
      throw new Error("closedPrice is only available for closed trades")
    }

    return this.trade.closePrice
  }

  @computed get remainingMargin(): BigNumber {
    const margin = this.trade.margin
    if (this.trade.type === "close") {
      return math`${margin} x (${1} - ${this.trade.closePercentage})`
    }

    /**
     * this.trade.margin is already the updated value for the `update` event,
     * so we don't need to update it with marginDelta
     */

    return margin
  }

  @computed get originMargin(): BigNumber {
    const margin = this.trade.margin

    if (this.trade.type === "update") {
      return math`${margin} ${this.trade.updateIsAdding ? "-" : "+"} ${
        this.trade.updateMarginDelta
      }`
    }

    return margin
  }

  @computed get closedMargin(): BigNumber {
    if (this.trade.type !== "close") {
      throw new Error("closedMargin is only available for closed trades")
    }
    const margin = this.trade.margin
    return math`${margin} x ${this.trade.closePercentage}`
  }

  @computed get leverage(): BigNumber {
    return this.trade.leverage
  }

  @computed get remainingPosition(): BigNumber {
    return math`${this.leverage} x ${this.remainingMargin}`
  }

  @computed get currentMarkPrice$(): BigNumber {
    return this.store.appEnvStore.pyth.priceFor(this.trade.priceId).value$.price
  }

  @computed get info$(): TradeCoreInfosType {
    return this.store.infoForPriceId$(this.trade.priceId, this.trade.user)
  }

  @computed get isLong(): boolean {
    return this.trade.isLong
  }

  @computed get accumulatedFeesFromContract(): {
    fundingFee: BigNumber
    rolloverFee: BigNumber
  } {
    const currentMargin = this.remainingMargin
    const originalMargin = this.openEvent.margin
    return this.store.getAccumulatedFeeFromContract(
      this.trade.orderHash,
      math`${currentMargin} / ${originalMargin}`,
    ).value$
  }

  @computed get currentBlock$(): number {
    return (
      safelyGet(() => this.store.appEnvStore.currentBlock$) ??
      this.store.appEnvStore.appOpenBlockHeight$
    )
  }

  calcAdjustedClosePrice$(
    closingMargin: BigNumber,
    markPrice: BigNumber,
  ): BigNumber {
    return getAdjustedClosePrice(
      {
        totalLong: this.info$.totalLong,
        totalShort: this.info$.totalShort,
        impactRefDepthLong: this.info$.impactRefDepthLong,
        impactRefDepthShort: this.info$.impactRefDepthShort,
        longFundingFee: this.info$.longFundingFee,
        shortFundingFee: this.info$.shortFundingFee,
        rolloverFeeRate: this.info$.rolloverFeeRate,
        currentBlock: this.currentBlock$,
      },
      {
        isLong: this.isLong,
        leverage: this.leverage,
        margin: this.remainingMargin,
        openPrice: this.trade.openPrice,
        openSlippage: this.trade.slippage,
        accruedFee: this.trade.accruedFee,
        fundingFeeBase: this.trade.fundingFeeBase,
      },
      markPrice,
      closingMargin,
    )
  }

  calcAccumulatedFeeForClosing$(closingMargin: BigNumber): {
    total: BigNumber
    fundingFee: BigNumber
    rolloverFee: BigNumber
  } {
    const { fundingFee, rolloverFee } = getAccumulatedFee(
      {
        totalLong: this.info$.totalLong,
        totalShort: this.info$.totalShort,
        longFundingFee: this.info$.longFundingFee,
        shortFundingFee: this.info$.shortFundingFee,
        rolloverFeeRate: this.info$.rolloverFeeRate,
        currentBlock: this.currentBlock$,
      },
      {
        isLong: this.isLong,
        leverage: this.leverage,
        margin: this.remainingMargin,
        accruedFee: this.trade.accruedFee,
        fundingFeeBase: this.trade.fundingFeeBase,
      },
      closingMargin,
    )
    return {
      total: math`${fundingFee} + ${rolloverFee}`,
      fundingFee,
      rolloverFee,
    }
  }

  pnlForClosePrice(closeMargin: BigNumber, closePrice: BigNumber): BigNumber {
    /**
     * https://github.com/uniwhale-io/uniwhale-v1/blob/07c8f0a71a985e5333fa28d294999020ecaff15c/packages/contracts/core-v1/contracts/TradingCore.sol#L635-L650
     */

    const { isLong, entryPrice } = this
    const closePosition = calcClosePosition(this.trade, closeMargin)

    const closeNet = closePrice
    const openNet = entryPrice

    const absPositionPnL = math`abs(${closeNet} - ${openNet}) * ${closePosition} / ${openNet}`

    const isWinningMoney =
      (isLong && closeNet > openNet) || (!isLong && openNet > closeNet)

    let grossPnl: BigNumber
    if (isWinningMoney) {
      grossPnl = math`${closeMargin} + ${absPositionPnL}`
    } else {
      grossPnl = mathIs`${closeMargin} < ${absPositionPnL}`
        ? BigNumber.ZERO
        : math`${closeMargin} - ${absPositionPnL}`
    }
    const maxPnl = math`${closeMargin} * ${this.maxTakeProfitPercentage$}`
    if (BigNumber.isGt(grossPnl, maxPnl)) {
      grossPnl = maxPnl
    }

    return math`${grossPnl} - ${closeMargin}`
  }

  @computed get maxTakeProfitPercentage$(): BigNumber {
    return this.trade.maxPercentagePnL
  }

  /**
   * it will >openPrice for a long position, will <openPrice for a short position
   */
  @computed get takeProfitPriceLimitation$(): BigNumber {
    return getExitPriceFromProfitPercentage(
      isShouldOverflowOpenPrice(this.orderDirection, "takeProfit"),
      this.leverage,
      this.entryPrice,
      this.maxTakeProfitPercentage$,
    )
  }

  @computed get unrealizedPnl$(): { amount: BigNumber; percentage: BigNumber } {
    const amount = this.pnlForClosePrice(
      this.remainingMargin,
      this.calcAdjustedClosePrice$(
        this.remainingMargin,
        this.currentMarkPrice$,
      ),
    )
    const percentage = math`${amount} / ${this.remainingMargin}`
    return { amount, percentage }
  }

  @computed get openFee$(): BigNumber {
    const currentMargin = this.remainingMargin
    const originalMargin = this.openEvent.margin
    const originalFee = this.openEvent.fee
    return math`${originalFee} x ${currentMargin} / ${originalMargin}`
  }

  @computed get realizedPnl(): BigNumber {
    if (this.trade.type !== "close") {
      throw new Error("realizedPnl$ should only be called for a closed trade")
    }
    return math`${this.trade.closeSettled} - ${this.closedMargin}`
  }

  @computed get grossPnl(): BigNumber {
    if (this.trade.type !== "close") {
      throw new Error("realizedPnl$ should only be called for a closed trade")
    }
    return math`${this.trade.closeSettled} - ${this.closedMargin} + ${this.trade.closeFee} + ${this.trade.closeLiquidatorFee}`
  }

  @computed get grossPnlPercentage(): BigNumber {
    return math`${this.grossPnl} / ${this.closedMargin}`
  }

  @computed get realizedPnlPercentage(): BigNumber {
    return math`${this.realizedPnl} / ${this.closedMargin}`
  }

  @computed get isClosedByLiquidator(): boolean {
    if (this.trade.type !== "close") return false
    return this.trade.isLiquidated
  }

  @computed get isClosedByTakeProfit(): boolean {
    if (this.trade.type !== "close") return false
    if (!this.trade.isStop) return false
    return !BigNumber.isNegative(this.realizedPnl)
  }

  @computed get isClosedByStopLoss(): boolean {
    if (this.trade.type !== "close") return false
    if (!this.trade.isStop) return false
    return BigNumber.isNegative(this.realizedPnl)
  }

  @computed get baseTokenInfo$(): TokenInfo {
    const { currencyStore, appEnvStore } = this.store
    return currencyStore.baseTokenInfo$(
      appEnvStore.pyth.priceSymbolFromId(this.trade.priceId)!,
    )
  }

  @computed get quoteTokenInfo$(): TokenInfo {
    const pyth = this.store.appEnvStore.pyth
    return {
      ...this.store.quoteTokenInfo$,
      precision: pyth.marketPrecisionFor(
        pyth.priceSymbolFromId(this.trade.priceId)!,
      ),
    }
  }

  @computed get positionRecord$(): PositionRecord {
    const { liquidationPrice } = this.trade

    return {
      openedAt: new Date(this.openEvent.blockTime),
      baseToken: this.baseTokenInfo$,
      quoteToken: this.quoteTokenInfo$,
      id: this.trade.orderHash,
      direction: this.orderDirection,
      entryPrice: this.entryPrice,
      leverage: this.leverage,
      margin: this.remainingMargin,
      positionSize: this.remainingPosition,
      markPrice: suspenseResource(() => this.currentMarkPrice$),
      liquidityPrice: liquidationPrice,
      stopLossPrice: this.stopLossPriceDisplay$,
      takeProfitPrice: this.takeProfitPriceDisplay$,
      pnl: suspenseResource(() => ({
        delta: this.unrealizedPnl$.amount,
        deltaPercentage: this.unrealizedPnl$.percentage,
      })),
      fees: {
        total: suspenseResource(
          () =>
            math`${this.openFee$} + ${
              this.calcAccumulatedFeeForClosing$(this.remainingMargin).total
            }`,
        ),
        open: suspenseResource(() => this.openFee$),
        funding: suspenseResource(
          () =>
            this.calcAccumulatedFeeForClosing$(this.remainingMargin).fundingFee,
        ),
        // funding: suspenseResource(() => feesFromContract.value$.fundingFee),
        rollover: suspenseResource(
          () =>
            this.calcAccumulatedFeeForClosing$(this.remainingMargin)
              .rolloverFee,
        ),
        // rollover: suspenseResource(() => feesFromContract.value$.rolloverFee),
      },
    }
  }

  @computed get debugPositionRecord$(): DebugPositionRecord {
    return fromTradeToDebugRecord(this)
  }

  @computed get takeProfitPriceDisplay$(): BigNumber | null {
    if (this.trade.profitTarget == null) return null
    if (isShouldOverflowOpenPrice(this.orderDirection, "takeProfit")) {
      return BigNumber.min([
        this.trade.profitTarget,
        this.takeProfitPriceLimitation$,
      ])
    } else {
      return BigNumber.max([
        this.trade.profitTarget,
        this.takeProfitPriceLimitation$,
      ])
    }
  }

  @computed get stopLossPriceDisplay$(): BigNumber | null {
    if (this.trade.stopLoss == null) return null
    if (isShouldOverflowOpenPrice(this.orderDirection, "stopLoss")) {
      return BigNumber.min([this.trade.stopLoss, this.trade.liquidationPrice])
    } else {
      return BigNumber.max([this.trade.stopLoss, this.trade.liquidationPrice])
    }
  }

  @computed get orderDirection(): OrderDirection {
    return this.trade.isLong ? OrderDirection.Long : OrderDirection.Short
  }

  @computed get record$(): TradeRecord {
    const common: TradeRecord.Common = {
      transactionHash: this.trade.txHash,
      orderType: OrderType.Market, // TODO
      orderDirection: this.trade.isLong
        ? OrderDirection.Long
        : OrderDirection.Short,
      createdAt: new Date(this.trade.blockTime),
      baseToken: this.baseTokenInfo$,
      quoteToken: this.quoteTokenInfo$,
      leverage: this.leverage,
      margin: this.originMargin,
      position: this.remainingPosition,
    }
    if (this.trade.type === "open") {
      return {
        ...common,
        type: TradeRecord.Type.CreateOrder,
        price: this.entryPrice,
        fee: this.trade.fee,
      }
    }
    if (this.trade.type === "close") {
      const closeFee = math`${this.trade.closeFee} + ${this.trade.closeSlippage} + ${this.trade.closeLiquidatorFee}`
      const fees = {
        total: math`${closeFee} + ${this.trade.closeFundingFee} + ${this.trade.closeRolloverFee}`,
        close: closeFee,
        funding: this.trade.closeFundingFee,
        rollover: this.trade.closeRolloverFee,
      }

      if (this.isClosedByLiquidator) {
        return {
          ...common,
          type: TradeRecord.Type.Liquidation,
          price: this.closedPrice,
          grossPnl: this.grossPnl,
          amount: this.closedMargin,
          fees,
        }
      }

      return {
        ...common,
        type: this.isClosedByTakeProfit
          ? TradeRecord.Type.TakeProfit
          : this.isClosedByStopLoss
          ? TradeRecord.Type.StopLoss
          : TradeRecord.Type.ClosePosition,
        price: this.closedPrice,
        grossPnl: this.grossPnl,
        fees,
      }
    }
    if (this.trade.type === "update") {
      if (BigNumber.isZero(this.trade.updateMarginDelta)) {
        return {
          ...common,
          type: TradeRecord.Type.UpdateActionTrigger,
          takeProfitPrice: this.takeProfitPriceDisplay$,
          stopLossPrice: this.trade.stopLoss,
        }
      }
      return {
        ...common,
        type: this.trade.updateIsAdding
          ? TradeRecord.Type.IncreaseMargin
          : TradeRecord.Type.DecreaseMargin,
        marginDelta: this.trade.updateMarginDelta,
      }
    }
    checkNever(this.trade)
    throw new DisplayableError(
      `Unexpected trade type: ${(this.trade as any).type}`,
      {
        cause: {
          source: "TradeCellModule#record$",
          trade: this.trade,
        },
      },
    )
  }
}
