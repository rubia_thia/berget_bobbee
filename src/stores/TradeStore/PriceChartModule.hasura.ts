import { Client, gql } from "@urql/core"
import { findLast } from "ramda"
import {
  combineLatest,
  firstValueFrom,
  interval,
  map,
  Observable,
  startWith,
  switchMap,
} from "rxjs"
import { PriceChange } from "../../components/LightweightCharts/LightweightCharts.types"
import {
  GetPriceBefore23HoursQuery,
  GetPriceBefore23HoursQueryVariables,
  HistoricalPrices15MQuery,
  HistoricalPrices15MQueryVariables,
  HistoricalPrices1dQuery,
  HistoricalPrices1dQueryVariables,
  HistoricalPrices1hQuery,
  HistoricalPrices1hQueryVariables,
  HistoricalPrices1MQuery,
  HistoricalPrices1MQueryVariables,
  HistoricalPrices24hChangeQuery,
  HistoricalPrices24hChangeQueryVariables,
} from "../../generated/graphql/graphql.generated"
import { hasAny } from "../../utils/arrayHelpers"
import { durationToMs } from "../../utils/datetimeHelpers"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { isNotNull } from "../../utils/typeHelpers"
import { assertNever } from "../../utils/types"
import {
  ethAddressToHasuraAddress,
  fromUrqlSource,
} from "../utils/hasuraClient"

export type HistoricalPriceTimeWindow = "1m" | "15m" | "1h" | "1d"
export const allTimeWindows: HistoricalPriceTimeWindow[] = [
  "1m",
  "15m",
  "1h",
  "1d",
]

export const getHistoricalPrices = (
  client: Client,
  priceId: string,
  confMultiplier: BigNumber,
  timeRange: HistoricalPriceTimeWindow,
): Observable<PriceChange[]> => {
  return _getHistoricalPrices(client, priceId, confMultiplier, timeRange).pipe(
    map(a =>
      a
        .map((item, index) => {
          if (index === a.length - 1) return null

          const res = {
            ...item,
            start: a[index + 1]!.end,
            high: BigNumber.max([item.high, a[index + 1]!.end]),
          }

          if (res.high > res.highestAsk) {
            res.highestAsk = a[index + 1]!.highestAsk
          }

          return res
        })
        .filter(isNotNull)
        .reverse(),
    ),
  )
}

function mapTimeWindowBasedHistoricalResult(
  confMultiplier: BigNumber,
  input: Omit<
    NonNullable<HistoricalPrices1MQuery["pyth_prices_1m"]>[number],
    "__typename"
  >[],
): Omit<PriceChange, "start">[] {
  return input.map(a => ({
    time: new Date(a.aggregated_time),
    end: BigNumber.from(a.close_price),
    low: BigNumber.from(a.low_price),
    high: BigNumber.from(a.high_price),
    lowestBid: BigNumber.minus(a.low_bid_price)(
      BigNumber.mul(confMultiplier)(a.low_bid_conf),
    ),
    highestAsk: BigNumber.add(a.high_ask_price)(
      BigNumber.mul(confMultiplier)(a.high_ask_conf),
    ),
  }))
}

const _getHistoricalPrices = (
  client: Client,
  priceId: string,
  confMultiplier: BigNumber,
  timeRange: HistoricalPriceTimeWindow,
): Observable<Omit<PriceChange, "start">[]> => {
  if (timeRange === "1m") {
    return fromUrqlSource(
      client.query<HistoricalPrices1MQuery, HistoricalPrices1MQueryVariables>(
        gql`
          query HistoricalPrices1M($priceId: bytea!) {
            pyth_prices_1m(
              where: { price_id: { _eq: $priceId } }
              order_by: { aggregated_time: desc }
              limit: 1000
            ) {
              aggregated_time
              close_price
              high_price
              high_ask_price
              high_ask_conf
              low_price
              low_bid_price
              low_bid_conf
            }
          }
        `,
        {
          priceId: ethAddressToHasuraAddress(priceId),
        },
      ),
      ({ data }) =>
        mapTimeWindowBasedHistoricalResult(confMultiplier, data.pyth_prices_1m),
    )
  }
  if (timeRange === "15m") {
    return fromUrqlSource(
      client.query<HistoricalPrices15MQuery, HistoricalPrices15MQueryVariables>(
        gql`
          query HistoricalPrices15M($priceId: bytea!) {
            pyth_prices_15m(
              where: { price_id: { _eq: $priceId } }
              order_by: { aggregated_time: desc }
              limit: 1000
            ) {
              aggregated_time
              close_price
              high_price
              high_ask_price
              high_ask_conf
              low_price
              low_bid_price
              low_bid_conf
            }
          }
        `,
        {
          priceId: ethAddressToHasuraAddress(priceId),
        },
      ),
      ({ data }) =>
        mapTimeWindowBasedHistoricalResult(
          confMultiplier,
          data.pyth_prices_15m,
        ),
    )
  }
  if (timeRange === "1h") {
    return fromUrqlSource(
      client.query<HistoricalPrices1hQuery, HistoricalPrices1hQueryVariables>(
        gql`
          query HistoricalPrices1h($priceId: bytea!) {
            pyth_prices_1h(
              where: { price_id: { _eq: $priceId } }
              order_by: { aggregated_time: desc }
              limit: 1000
            ) {
              aggregated_time
              close_price
              high_price
              high_ask_price
              high_ask_conf
              low_price
              low_bid_price
              low_bid_conf
            }
          }
        `,
        {
          priceId: ethAddressToHasuraAddress(priceId),
        },
      ),
      ({ data }) =>
        mapTimeWindowBasedHistoricalResult(confMultiplier, data.pyth_prices_1h),
    )
  }
  if (timeRange === "1d") {
    return fromUrqlSource(
      client.query<HistoricalPrices1dQuery, HistoricalPrices1dQueryVariables>(
        gql`
          query HistoricalPrices1d($priceId: bytea!) {
            pyth_prices_1d(
              where: { price_id: { _eq: $priceId } }
              order_by: { aggregated_time: desc }
              limit: 1000
            ) {
              aggregated_time
              close_price
              high_price
              high_ask_price
              high_ask_conf
              low_price
              low_bid_price
              low_bid_conf
            }
          }
        `,
        {
          priceId: ethAddressToHasuraAddress(priceId),
        },
      ),
      ({ data }) =>
        mapTimeWindowBasedHistoricalResult(confMultiplier, data.pyth_prices_1d),
    )
  }
  assertNever(timeRange)
}

export const get24HourPriceChange = (
  client: Client,
  priceId: string,
): Observable<{
  openPrice: BigNumber
  lowestPrice: BigNumber
  highestPrice: BigNumber
}> => {
  return fromUrqlSource(
    client.query<
      HistoricalPrices24hChangeQuery,
      HistoricalPrices24hChangeQueryVariables
    >(
      gql`
        query HistoricalPrices24hChange($priceId: bytea!) {
          pyth_prices_1h(
            where: { price_id: { _eq: $priceId } }
            order_by: { aggregated_time: desc }
            limit: 24
          ) {
            aggregated_time
            open_price
            close_price
            high_price
            high_ask_price
            high_ask_conf
            low_price
            low_bid_conf
            low_bid_conf
          }
        }
      `,
      {
        priceId: ethAddressToHasuraAddress(priceId),
      },
    ),
    ({ data }) => {
      const hourlyData = data.pyth_prices_1h.reverse()
      if (!hasAny(hourlyData)) {
        throw new Error("No hourly data found")
      }
      const openPrice = BigNumber.from(hourlyData[0].open_price)
      const lowestPrice = hourlyData.reduce(
        (p, curr) => (curr.low_price < p ? curr.low_price : p),
        Number.MAX_VALUE,
      )
      const highestPrice = hourlyData.reduce(
        (p, curr) => (curr.high_price > p ? curr.high_price : p),
        0,
      )
      return {
        openPrice: BigNumber.from(openPrice),
        lowestPrice: BigNumber.from(lowestPrice),
        highestPrice: BigNumber.from(highestPrice),
      }
    },
  )
}

export const getPriceBefore24Hours = (
  client: Client,
  priceId: string,
): Observable<undefined | { price: BigNumber }> => {
  const msIn24Hours = durationToMs({ hours: 24 })

  return interval(durationToMs({ minutes: 30 })).pipe(
    startWith(null),
    switchMap(() =>
      combineLatest(
        getPricesBefore23Hours(client, priceId),
        interval(durationToMs({ seconds: 30 })).pipe(startWith(-1)),
      ),
    ),
    map(([prices]) => {
      const timeBefore24Hours = Date.now() - msIn24Hours
      return findLast(p => p.time <= timeBefore24Hours, prices)
    }),
  )
}
async function getPricesBefore23Hours(
  client: Client,
  priceId: string,
): Promise<{ time: number; price: BigNumber }[]> {
  return firstValueFrom(
    fromUrqlSource(
      client.query<
        GetPriceBefore23HoursQuery,
        GetPriceBefore23HoursQueryVariables
      >(
        gql`
          query GetPriceBefore23Hours($priceId: bytea!, $time: timestamptz!) {
            pyth_prices_1m(
              where: {
                price_id: { _eq: $priceId }
                aggregated_time: { _lt: $time }
              }
              order_by: { aggregated_time: desc }
              limit: 61
            ) {
              aggregated_time
              open_price
            }
          }
        `,
        {
          priceId: ethAddressToHasuraAddress(priceId),
          time: new Date(Date.now() - 23 * 60 * 60 * 1000).toISOString(),
        },
      ),
      ({ data }) =>
        data.pyth_prices_1m.map(p => ({
          time: new Date(p.aggregated_time).getTime(),
          price: BigNumber.from(p.open_price),
        })),
    ),
  )
}
