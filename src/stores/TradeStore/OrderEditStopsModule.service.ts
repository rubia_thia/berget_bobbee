import { ContractTransaction } from "ethers"
import { fromNativeToContract } from "../../utils/contractHelpers/bigNumberContracts"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { PythModule } from "../AppEnvStore/PythModule"
import { TradingCore } from "../contracts/generated"

export async function updateStops({
  pyth,
  tradeCore,
  stopLoss,
  orderHash,
  priceId,
  profitTarget,
}: {
  priceId: string
  pyth: PythModule
  tradeCore: TradingCore
  orderHash: string
  stopLoss: BigNumber
  profitTarget: BigNumber
}): Promise<ContractTransaction> {
  const priceData = await pyth.createPriceFeedUpdateDate(priceId)
  const fee = await pyth.getUpdateFee(priceData)
  return await tradeCore.updateStop(
    orderHash,
    priceData,
    fromNativeToContract(profitTarget),
    fromNativeToContract(stopLoss),
    {
      value: fee,
      gasLimit: 3e6,
    },
  )
}
