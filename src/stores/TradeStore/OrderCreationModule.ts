import { ContractTransaction } from "ethers"
import { action, computed, makeObservable, observable } from "mobx"
import { equals } from "ramda"
import { from } from "rxjs"
import {
  ActionTrigger,
  ActionTriggerPatch,
  ActionTriggerPercentageOption,
  OrderCreationFormError,
  OrderCreationFormErrorType,
} from "../../screens/TradeScreen/components/OrderCreation/types"
import { OrderDirection, OrderType } from "../../screens/TradeScreen/types"
import { fromContractToNative } from "../../utils/contractHelpers/bigNumberContracts"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import {
  math,
  mathIs,
} from "../../utils/numberHelpers/bigNumberExpressionParser"
import { TokenInfo } from "../../utils/TokenInfo"
import { assertNever } from "../../utils/types"
import {
  ERC20Tokens,
  StableToken,
} from "../CurrencyStore/CurrencyStore.service"
import { LazyValue } from "../utils/LazyValue"
import { Result } from "../utils/Result"
import { SuspenseObservable } from "../utils/SuspenseObservable"
import { waitUntil } from "../utils/waitFor"
import {
  approveToken,
  convertPatchTriggerToDisplay,
  OpenMarketOrderProps,
  openOrder,
} from "./OrderCreationModule.service"
import { TradeStore } from "./TradeStore"
import { TradeCoreInfosType } from "./TradeStore.service"

export interface OrderCreationModuleFormData extends OpenMarketOrderProps {
  executionPrice: BigNumber
}

export class OrderCreationModule {
  constructor(readonly store: TradeStore) {
    makeObservable(this)
  }
  @observable type: OrderType = OrderType.Market
  @observable direction: OrderDirection = OrderDirection.Long

  @observable quoteToken: StableToken = ERC20Tokens.AnchorToken
  @computed get quoteTokenInfo$(): TokenInfo {
    return {
      ...this.store.currencyStore.tokenInfo$(this.quoteToken),
      precision: this.store.priceSymbolPrecision$,
    }
  }

  @action setQuoteToken(token: StableToken): void {
    this.quoteToken = token
  }

  @computed get allAvailableQuoteTokenInfo$(): TokenInfo[] {
    return this.store.tradeInfo.allSupportedStableToken$.map(
      this.store.currencyStore.tokenInfo$,
    )
  }

  @observable showSwapSlippageEditingModal = false
  recommendedSwapSlippage = BigNumber.from(0.0005)
  swapSlippage = new SuspenseObservable(this.recommendedSwapSlippage)

  @computed get exchangeRate$(): BigNumber {
    return this.store.tradeInfo.exchangeRate$(
      this.quoteToken,
      ERC20Tokens.AnchorToken,
    )
  }

  @computed get anchorTokenMargin$(): BigNumber {
    const margin = this.inputMargin.read$
    if (BigNumber.isZero(margin)) {
      throw waitUntil(() => !BigNumber.isZero(margin))
    }
    return math`${margin} * ${this.exchangeRate$}`
  }

  @computed get isAnchorToken$(): boolean {
    return this.quoteToken === ERC20Tokens.AnchorToken
  }

  #minPositionPerTrade = new LazyValue(
    () => this.store.contracts.registryCore$,
    rg => from(rg.minPositionPerTrade().then(fromContractToNative)),
  )

  @computed({ equals: equals }) get formError$():
    | OrderCreationFormError
    | undefined {
    return Result.maybeError(this.formData$)
  }

  @computed get formData$(): Result<
    OrderCreationModuleFormData,
    OrderCreationFormError
  > {
    if (!this.store.authStore.connected) {
      return Result.error<OrderCreationFormError.Common>({
        type: OrderCreationFormErrorType.ConnectWalletRequired,
      })
    }
    if (!this.store.authStore.connectedToTheRightChain$) {
      return Result.error({
        type: OrderCreationFormErrorType.ConnectedToTheWrongNetwork,
      })
    }
    if (this.type !== OrderType.Limit) {
      if (
        this.slippage.get() == null ||
        BigNumber.isZero(this.slippage.read$)
      ) {
        return Result.error<OrderCreationFormError.Common>({
          type: OrderCreationFormErrorType.SlippageIsEmpty,
        })
      }
      if (
        BigNumber.isLt(this.slippage.read$, this.slippageRange[0]) ||
        BigNumber.isGt(this.slippage.read$, this.slippageRange[1])
      ) {
        return Result.error<OrderCreationFormError.SlippageOutOfRange>({
          type: OrderCreationFormErrorType.SlippageOutOfRange,
          min: this.slippageRange[0],
          max: this.slippageRange[1],
        })
      }
    }
    if (this.leverage.get() == null || BigNumber.isZero(this.leverage.read$)) {
      return Result.error<OrderCreationFormError.Common>({
        type: OrderCreationFormErrorType.LeverageIsEmpty,
      })
    }
    if (
      BigNumber.isLt(this.leverage.read$, this.leverageRange$[0]) ||
      BigNumber.isGt(this.leverage.read$, this.leverageRange$[1])
    ) {
      return Result.error<OrderCreationFormError.LeverageOutOfRange>({
        type: OrderCreationFormErrorType.LeverageOutOfRange,
        min: this.leverageRange$[0],
        max: this.leverageRange$[1],
      })
    }
    if (
      this.inputMargin.get() == null ||
      BigNumber.isZero(this.inputMargin.read$)
    ) {
      return Result.error<OrderCreationFormError.Common>({
        type: OrderCreationFormErrorType.MarginAmountIsEmpty,
      })
    }
    if (BigNumber.isGt(this.inputMargin.read$)(this.quoteTokenBalance$)) {
      return Result.error<OrderCreationFormError.Common>({
        type: OrderCreationFormErrorType.InsufficientQuoteTokenBalance,
      })
    }
    if (this.stopLossTriggerDisplay$ != null) {
      const percentage = this.stopLossTriggerDisplay$.percentage
      if (mathIs`${percentage} > ${1} || ${percentage} < ${0}`) {
        return Result.error<OrderCreationFormError>({
          type: OrderCreationFormErrorType.StopLossPriceOutOfRange,
        })
      }
    }
    if (this.takeProfitTriggerDisplay$ != null) {
      const percentage = this.takeProfitTriggerDisplay$.percentage
      if (
        mathIs`${percentage} > ${
          this.maxPercentagePnL$
        } || ${percentage} < ${0}`
      ) {
        return Result.error<OrderCreationFormError>({
          type: OrderCreationFormErrorType.TakeProfitPriceOutOfRange,
        })
      }
    }
    const minPosition = this.#minPositionPerTrade.value$
    if (mathIs`${this.positionSize$} < ${minPosition}`) {
      return Result.error<OrderCreationFormError.PositionOutOfRange>({
        type: OrderCreationFormErrorType.PositionIsTooMall,
        minPosition: minPosition,
      })
    }
    return Result.ok({
      user: this.store.authStore.account$,
      priceId: this.store.currentPriceId$,
      pyth: this.store.appEnvStore.pyth,
      tradeCore: this.store.contracts.tradingCore$,
      limitBook: this.store.contracts.limitBook$,
      quoteTokenAddress:
        this.quoteToken === ERC20Tokens.AnchorToken
          ? undefined
          : this.store.currencyStore.contractAddress$(this.quoteToken),
      anchorTokenAddress: this.store.currencyStore.contractAddress$(
        ERC20Tokens.AnchorToken,
      ),
      tradeCoreWithRouter: this.store.contracts.tradingCoreWithRouter$,
      direction: this.direction,
      type: this.type,
      anchorMargin: this.anchorTokenMargin$,
      inputQuoteMargin: this.inputMargin.read$,
      leverage: this.leverage$,
      profitTarget: this.takeProfitTriggerDisplay$?.price ?? BigNumber.from(0),
      stopLoss: this.stopLossTriggerDisplay$?.price ?? BigNumber.from(0),
      executionPrice: this.executePrice$,
      limitPrice: this.limitPrice$,
      swapPoolFee: this.store.tradeInfo.swapPoolFee,
      swapSlippage: this.swapSlippage.read$,
      registry: this.store.contracts.registryCore$,
      registryReader: this.store.contracts.registryReader$,
      tradeCoreLib: this.store.contracts.tradingCoreLib$,
    })
  }

  @computed get leverageRange$(): [min: BigNumber, max: BigNumber] {
    return [
      this.store.currentSelectedMarketPriceIdInfo$.minLeverage,
      this.store.currentSelectedMarketPriceIdInfo$.maxLeverage,
    ]
  }

  leverage = new SuspenseObservable(BigNumber.from(2))

  @computed get quoteTokenBalance$(): BigNumber {
    return this.store.currencyStore.balance$(this.quoteToken)
  }

  @computed get interactionAddress$(): string {
    switch (this.type) {
      case OrderType.Market:
        if (this.quoteToken !== ERC20Tokens.AnchorToken) {
          return this.store.contracts.tradingCoreWithRouter$.address
        }
        return this.store.contracts.tradingCore$.address
      case OrderType.Limit:
        return this.store.contracts.limitBook$.address
      default:
        assertNever(this.type)
    }
  }

  @computed get quoteTokenAllowance$(): BigNumber {
    return this.store.currencyStore.allowanceOf$(
      this.quoteToken,
      this.interactionAddress$,
    )
  }

  @computed get needApproveFirst$(): boolean {
    const allowance = this.quoteTokenAllowance$
    return mathIs`${allowance} == ${0} || ${allowance} < ${
      this.inputMargin.read$
    }`
  }

  readonly slippageRange: [min: BigNumber, max: BigNumber] = [
    BigNumber.from(0.0001),
    BigNumber.from(0.49),
  ]

  slippage = new SuspenseObservable(BigNumber.from(0.01))

  inputMargin = new SuspenseObservable<BigNumber>(BigNumber.from(100))

  @computed
  get stopLossTriggerPercentageOptions$(): ActionTriggerPercentageOption[] {
    return [
      { value: null },
      { value: BigNumber.from(0.1) },
      { value: BigNumber.from(0.25) },
      { value: BigNumber.from(0.5) },
      { value: BigNumber.from(0.75) },
    ]
  }

  @observable stopLossTrigger?: ActionTriggerPatch

  @computed get stopLossTriggerDisplay$(): ActionTrigger | null {
    if (this.stopLossTrigger == null) return null
    return convertPatchTriggerToDisplay(
      this.direction,
      "stopLoss",
      this.executePrice$,
      this.leverage$,
      this.stopLossTrigger,
    )
  }

  @computed
  get takeProfitTriggerPercentageOptions$(): ActionTriggerPercentageOption[] {
    return [
      { value: BigNumber.from(0.25) },
      { value: BigNumber.from(0.5) },
      { value: BigNumber.from(0.75) },
      { value: BigNumber.from(1) },
      { value: BigNumber.from(9) },
    ]
  }

  readonly defaultTakeProfitTrigger: ActionTriggerPatch = {
    sourceType: "percentage",
    percentage: BigNumber.from(9),
  }

  @observable takeProfitTrigger?: ActionTriggerPatch =
    this.defaultTakeProfitTrigger

  @computed get takeProfitTriggerDisplay$(): ActionTrigger | null {
    if (this.takeProfitTrigger == null) return null
    return convertPatchTriggerToDisplay(
      this.direction,
      "takeProfit",
      this.executePrice$,
      this.leverage$,
      this.takeProfitTrigger,
    )
  }

  @computed get positionSize$(): BigNumber {
    return math`${this.anchorTokenMargin$} * ${this.leverage$}`
  }

  @computed get info$(): TradeCoreInfosType {
    return this.store.currentSelectedMarketPriceIdInfo$
  }

  @observable inputLimitPriceEdited = false
  inputLimitPrice = new SuspenseObservable<BigNumber>()

  @computed get markPrice$(): BigNumber {
    return this.store.markPrice$
  }

  @computed get entryPrice$(): BigNumber {
    const priceBuffer = math`${this.store.priceFromPyth$.conf} * ${this.info$.confMultiplier}`

    if (this.direction === OrderDirection.Long) {
      return math`${this.markPrice$} + ${priceBuffer}`
    } else {
      return math`${this.markPrice$} - ${priceBuffer}`
    }
  }

  @computed get exitPrice$(): BigNumber {
    const priceBuffer = math`${this.store.priceFromPyth$.conf} * ${this.info$.confMultiplier}`

    if (this.direction === OrderDirection.Long) {
      return math`${this.markPrice$} - ${priceBuffer}`
    } else {
      return math`${this.markPrice$} + ${priceBuffer}`
    }
  }

  @computed get marketPrice$(): BigNumber {
    return this.entryPrice$
  }

  @computed get executePrice$(): BigNumber {
    if (this.type === OrderType.Market) {
      return this.marketPrice$
    }
    if (this.type === OrderType.Limit) {
      if (this.inputLimitPriceEdited) {
        return this.inputLimitPrice.read$
      }
      return this.marketPrice$
    }
    assertNever(this.type)
  }

  @computed get limitPrice$(): BigNumber {
    if (this.type === OrderType.Limit) {
      return this.executePrice$
    } else {
      const op = this.direction === OrderDirection.Long ? "+" : "-"
      return math`${this.executePrice$} * (${1} ${op} ${this.slippage.read$})`
    }
  }

  @computed get liquidationPrice$(): BigNumber {
    /**
     * https://github.com/uniwhale-io/uniwhale-v1/blob/3b0267afe1257368cdb4be1d66e70fc8ec005d4c/packages/contracts/core-v1/contracts/TradingCore.sol#L405-L417
     */

    const threshold = this.info$.liquidationThreshold

    const liquidationPriceRatio = math`(${this.leverage$} ${
      this.direction === OrderDirection.Long ? "-" : "+"
    } ${threshold}) / ${this.leverage$}`

    return math`${this.executePrice$} * ${liquidationPriceRatio}`
  }

  @computed get priceSpreadRate$(): BigNumber {
    const isLong = this.direction === OrderDirection.Long
    const total = isLong ? this.info$.totalLong : this.info$.totalShort
    const refDepth = isLong
      ? this.info$.impactRefDepthLong
      : this.info$.impactRefDepthShort
    const leverage = this.leverage$
    const margin = this.anchorTokenMargin$
    return math`(${total} + ${leverage} * ${margin}) / ${refDepth} / ${100}`
  }

  @computed get leverage$(): BigNumber {
    const leverage = this.leverage.read$
    if (BigNumber.isZero(leverage)) {
      throw waitUntil(() => !BigNumber.isZero(leverage))
    }
    return leverage
  }

  @computed get fees$(): BigNumber {
    return math`${this.leverage$} x ${this.anchorTokenMargin$} * ${this.info$.openFee}`
  }

  @computed get maxPercentagePnL$(): BigNumber {
    return BigNumber.from(9)
  }

  async approveToken(): Promise<ContractTransaction> {
    return await approveToken(
      this.interactionAddress$,
      this.store.currencyStore.getErc20$(this.quoteToken),
    )
  }

  async submit(data: OpenMarketOrderProps): Promise<ContractTransaction> {
    return openOrder(data)
  }
}
