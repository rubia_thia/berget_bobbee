import { FCC } from "../../utils/reactHelpers/types"
import { useCreation } from "../../utils/reactHelpers/useCreation"
import { useAppEnvStore } from "../AppEnvStore/useAppEnvStore"
import { useAuthStore } from "../AuthStore/useAuthStore"
import {
  useContractStore,
  useContractStoreForEventListen,
} from "../contracts/useContractStore"
import { useCurrencyStore } from "../CurrencyStore/useCurrencyStore"
import { createStore } from "../utils/createStore"
import { TradeInfoStore } from "./TradeInfoStore"

const { Provider, useStore, ContextBridgeSymbol } =
  createStore<TradeInfoStore>("TradeInfoStore")

export const TradeInfoStoreContextBridgeSymbol = ContextBridgeSymbol

export const useTradeInfoStore = useStore.bind(null)

export const TradeInfoStoreProvider: FCC = props => {
  const appEnvStore = useAppEnvStore()
  const authStore = useAuthStore()
  const currencyStore = useCurrencyStore()
  const contracts = useContractStore()
  const contractsForEvent = useContractStoreForEventListen()

  const store = useCreation(
    () =>
      new TradeInfoStore(
        appEnvStore,
        authStore,
        currencyStore,
        contracts,
        contractsForEvent,
      ),
    [appEnvStore, authStore, contracts, contractsForEvent, currencyStore],
  )

  return <Provider store={store}>{props.children}</Provider>
}
