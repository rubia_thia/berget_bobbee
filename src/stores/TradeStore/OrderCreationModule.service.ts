import { ContractTransaction } from "ethers"
import {
  ActionTrigger,
  ActionTriggerPatch,
} from "../../screens/TradeScreen/components/OrderCreation/types"
import { OrderDirection, OrderType } from "../../screens/TradeScreen/types"
import { fromNativeToContract } from "../../utils/contractHelpers/bigNumberContracts"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { math } from "../../utils/numberHelpers/bigNumberExpressionParser"
import { checkNever } from "../../utils/typeHelpers"
import { assertNever } from "../../utils/types"
import { PythModule } from "../AppEnvStore/PythModule"
import {
  ERC20,
  LimitBook,
  RegistryCore,
  RegistryReader,
  TradingCore,
  TradingCoreLib,
  TradingCoreWithRouter,
} from "../contracts/generated"
import {
  IBook,
  ISwapRouter,
} from "../contracts/generated/TradingCoreWithRouter"

export function getProfitPercentageFromExitPrice(
  overflowOpenPrice: boolean,
  leverage: BigNumber,
  entryPrice: BigNumber,
  exitPrice: BigNumber,
): BigNumber {
  const priceDelta = overflowOpenPrice
    ? math`${exitPrice} - ${entryPrice}`
    : math`${entryPrice} - ${exitPrice}`
  return math`${priceDelta} * ${leverage} / ${entryPrice}`
}

export function getExitPriceFromProfitPercentage(
  overflowOpenPrice: boolean,
  leverage: BigNumber,
  entryPrice: BigNumber,
  profitPercentage: BigNumber,
): BigNumber {
  const priceDelta = math`${entryPrice} * ${profitPercentage} / ${leverage}`
  const exitPrice = overflowOpenPrice
    ? math`${entryPrice} + ${priceDelta}`
    : math`${entryPrice} - ${priceDelta}`
  return BigNumber.max([BigNumber.ZERO, exitPrice])
}

export const isShouldOverflowOpenPrice = (
  direction: OrderDirection,
  type: "stopLoss" | "takeProfit",
): boolean => {
  return (
    (direction === OrderDirection.Long && type === "takeProfit") ||
    (direction === OrderDirection.Short && type === "stopLoss")
  )
}

export function convertPatchTriggerToDisplay(
  direction: OrderDirection,
  type: "stopLoss" | "takeProfit",
  entryPrice: BigNumber,
  leverage: BigNumber,
  patch: ActionTriggerPatch,
): ActionTrigger {
  const overflow = isShouldOverflowOpenPrice(direction, type)

  if (patch.sourceType === "percentage") {
    const { percentage } = patch
    return {
      sourceType: "percentage",
      percentage,
      price: getExitPriceFromProfitPercentage(
        overflow,
        leverage,
        entryPrice,
        patch.percentage,
      ),
    }
  } else if (patch.sourceType === "price") {
    return {
      sourceType: "price",
      percentage: getProfitPercentageFromExitPrice(
        overflow,
        leverage,
        entryPrice,
        patch.price,
      ),
      price: patch.price,
    }
  } else {
    checkNever(patch)
    return null as never
  }
}

export interface OpenMarketOrderProps {
  user: string
  priceId: string
  pyth: PythModule
  registry: RegistryCore
  registryReader: RegistryReader
  tradeCore: TradingCore
  tradeCoreLib: TradingCoreLib
  tradeCoreWithRouter: TradingCoreWithRouter
  quoteTokenAddress?: string
  anchorTokenAddress: string
  swapPoolFee: BigNumber
  swapSlippage: BigNumber
  limitBook: LimitBook
  direction: OrderDirection
  type: OrderType
  inputQuoteMargin: BigNumber
  anchorMargin: BigNumber
  leverage: BigNumber
  stopLoss: BigNumber
  profitTarget: BigNumber
  limitPrice: BigNumber
}

export const MAX_AMOUNT_TO_APPROVE = BigNumber.toEtherBigNumber({
  decimals: 0,
})(math`${2} ** ${256} - ${1}`)

export async function approveToken(
  contractAddress: string,
  quoteToken: ERC20,
): Promise<ContractTransaction> {
  return await quoteToken.approve(contractAddress, MAX_AMOUNT_TO_APPROVE)
}

export async function openOrder(
  props: OpenMarketOrderProps,
): Promise<ContractTransaction> {
  const openData: IBook.OpenTradeInputStruct = {
    isBuy: props.direction === OrderDirection.Long,
    priceId: props.priceId,
    margin: fromNativeToContract(props.anchorMargin),
    leverage: fromNativeToContract(props.leverage),
    stopLoss: fromNativeToContract(props.stopLoss),
    profitTarget: fromNativeToContract(props.profitTarget),
    limitPrice: fromNativeToContract(props.limitPrice),
    user: props.user,
  }

  const swapInput: ISwapRouter.SwapGivenOutInputStruct | undefined =
    props.quoteTokenAddress != null
      ? {
          poolFee: fromNativeToContract(props.swapPoolFee, { decimals: 6 }),
          tokenIn: props.quoteTokenAddress,
          tokenOut: props.anchorTokenAddress,
          amountOut: fromNativeToContract(
            math`${props.anchorMargin} * (${1} - ${props.swapSlippage})`,
          ),
          amountInMaximum: fromNativeToContract(props.inputQuoteMargin),
        }
      : undefined

  if (props.type === OrderType.Market) {
    const priceData = await props.pyth.createPriceFeedUpdateDate(props.priceId)
    const currentPrice = fromNativeToContract(
      await props.pyth.priceFor(props.priceId).value$.price,
    )
    const fee = await props.pyth.getUpdateFee(priceData)
    if (swapInput) {
      await props.tradeCoreWithRouter.canOpenMarketOrder(
        openData,
        swapInput,
        currentPrice,
      )
      return props.tradeCoreWithRouter.openMarketOrder(
        openData,
        priceData,
        swapInput,
        {
          value: fee,
          gasLimit: 3e6,
        },
      )
    }
    await props.registryReader.canOpenMarketOrder(
      props.tradeCore.address,
      props.tradeCoreLib.address,
      props.registry.address,
      openData,
      currentPrice,
    )
    return props.tradeCore[
      "openMarketOrder((bytes32,address,bool,uint128,uint128,uint128,uint128,uint128),bytes[])"
    ](openData, priceData, {
      value: fee,
      gasLimit: 3e6,
    })
  }
  if (props.type === OrderType.Limit) {
    if (swapInput) {
      await props.limitBook.canOpenLimitOrder(openData, swapInput)
      return props.limitBook[
        "openLimitOrder((bytes32,address,bool,uint128,uint128,uint128,uint128,uint128),(address,address,uint256,uint256,uint24))"
      ](openData, swapInput, {
        gasLimit: 3e6,
      })
    }
    await props.registryReader.canOpenLimitOrder(
      props.tradeCore.address,
      props.tradeCoreLib.address,
      props.registry.address,
      openData,
    )
    return props.limitBook[
      "openLimitOrder((bytes32,address,bool,uint128,uint128,uint128,uint128,uint128))"
    ](openData, {
      gasLimit: 3e6,
    })
  }
  assertNever(props.type)
}

export interface CloseMarketOrderProps {
  priceId: string
  pyth: PythModule
  orderHash: string
  tradeCore: TradingCore
  limitPrice: BigNumber
  percentage: BigNumber
}

export async function closeMarketOrder(
  props: CloseMarketOrderProps,
): Promise<ContractTransaction> {
  const priceData = await props.pyth.createPriceFeedUpdateDate(props.priceId)
  const fee = await props.pyth.getUpdateFee(priceData)
  return await props.tradeCore.closeMarketOrder(
    {
      orderHash: props.orderHash,
      limitPrice: fromNativeToContract(props.limitPrice),
      closePercent: fromNativeToContract(props.percentage),
    },
    priceData,
    {
      value: fee,
      gasLimit: 3e6,
    },
  )
}

export interface CloseLimitOrderProps {
  priceId: string
  orderHash: string
  limitBook: LimitBook
  percentage: BigNumber
}

export async function closeLimitOrder(
  props: CloseLimitOrderProps,
): Promise<ContractTransaction> {
  return await props.limitBook.closeLimitOrder(
    props.orderHash,
    fromNativeToContract(props.percentage),
    {
      gasLimit: 3e6,
    },
  )
}
