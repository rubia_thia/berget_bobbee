import { DisplayableError } from "../../utils/errorHelpers"
import { FCC } from "../../utils/reactHelpers/types"
import { useCreation } from "../../utils/reactHelpers/useCreation"
import { isPriceSymbol, PriceSymbol } from "../AppEnvStore/PythModule.service"
import { useAppEnvStore } from "../AppEnvStore/useAppEnvStore"
import { useAuthStore } from "../AuthStore/useAuthStore"
import { useContractStore } from "../contracts/useContractStore"
import { useCurrencyStore } from "../CurrencyStore/useCurrencyStore"
import { createStore } from "../utils/createStore"
import { TradeStore } from "./TradeStore"
import { useTradeInfoStore } from "./useTradeInfoStore"

const { Provider, useStore, ContextBridgeSymbol } =
  createStore<TradeStore>("TradeStore")

export const TradeStoreContextBridgeSymbol = ContextBridgeSymbol

export const useTradeStore = useStore.bind(null)

export const useCheckedPriceSymbol = (
  priceSymbol: undefined | string,
): undefined | PriceSymbol => {
  if (priceSymbol != null && !isPriceSymbol(priceSymbol)) {
    throw new DisplayableError(
      `Token pair "${priceSymbol}" not supported yet`,
      {
        cause: "useCheckedPriceSymbol",
      },
    )
  }

  return priceSymbol
}

export const TradeStoreProvider: FCC<{ priceSymbol?: PriceSymbol }> = props => {
  const appEnvStore = useAppEnvStore()
  const authStore = useAuthStore()
  const currencyStore = useCurrencyStore()
  const contracts = useContractStore()
  const tradeInfo = useTradeInfoStore()

  const symbol =
    props.priceSymbol ?? appEnvStore.onChainConfigs$.approvedPriceSymbols[0]!

  const store = useCreation(
    () =>
      new TradeStore(
        symbol,
        appEnvStore,
        authStore,
        currencyStore,
        contracts,
        tradeInfo,
      ),
    [appEnvStore, authStore, contracts, currencyStore, symbol, tradeInfo],
  )

  return <Provider store={store}>{props.children}</Provider>
}
