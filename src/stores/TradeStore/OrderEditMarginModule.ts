import { ContractTransaction } from "ethers"
import { computed, makeObservable, observable } from "mobx"
import {
  EditMarginModalFormError,
  EditMarginModalFormErrorType,
} from "../../screens/TradeScreen/components/EditMarginModalContent.types"
import { MarginManipulation } from "../../screens/TradeScreen/types"
import { asyncAction, runAsyncAction } from "../../utils/asyncAction"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import {
  math,
  mathIs,
} from "../../utils/numberHelpers/bigNumberExpressionParser"
import { Result } from "../utils/Result"
import { SuspenseObservable } from "../utils/SuspenseObservable"
import { changeMargin } from "./OrderEditMarginModule.service"
import { TradeCellModule } from "./TradeCellModule"
import { TradeStore } from "./TradeStore"
import { TradeCoreInfosType } from "./TradeStore.service"

export class OrderEditMarginModule {
  constructor(readonly trade: TradeCellModule, readonly store: TradeStore) {
    makeObservable(this)
  }

  @observable direction: MarginManipulation = MarginManipulation.Increase

  amount = new SuspenseObservable<BigNumber>()

  @computed get leverage$(): BigNumber {
    return math`${this.trade.remainingPosition} / ${this.margin$}`
  }

  @computed get margin$(): BigNumber {
    return math`${this.trade.remainingMargin} ${
      this.direction === MarginManipulation.Increase ? "+" : "-"
    } ${this.amount.read$}`
  }

  @computed get liquidationPrice$(): BigNumber {
    return this.trade.trade.liquidationPrice
  }

  @computed get newLiquidationPrice$(): BigNumber {
    const liquidationThreshold = this.info$.liquidationThreshold
    return math`${this.trade.entryPrice} * (${1} - ${liquidationThreshold} / ${
      this.leverage$
    })`
  }

  @computed get executionFee$(): BigNumber {
    return BigNumber.from(0)
  }

  @computed get balance$(): BigNumber {
    return this.store.orderCreation.quoteTokenBalance$
  }

  @computed get info$(): TradeCoreInfosType {
    return this.trade.info$
  }

  @computed get formData$(): Result<
    { direction: MarginManipulation; amount: BigNumber },
    EditMarginModalFormError
  > {
    if (this.amount.get() == null || BigNumber.isZero(this.amount.read$)) {
      return Result.error({ type: EditMarginModalFormErrorType.EnterAmount })
    }
    if (
      this.direction === MarginManipulation.Increase &&
      mathIs`${this.amount.read$} > ${this.balance$}`
    ) {
      return Result.error({
        type: EditMarginModalFormErrorType.InsufficientQuoteTokenBalance,
      })
    }
    const newLeverage = this.leverage$
    if (
      this.direction === MarginManipulation.Increase &&
      mathIs`${newLeverage} < ${this.info$.minLeverage}`
    ) {
      return Result.error({
        type: EditMarginModalFormErrorType.LeverageTooLow,
      })
    }
    if (
      this.direction === MarginManipulation.Decrease &&
      mathIs`${newLeverage} < ${0}`
    ) {
      return Result.error({
        type: EditMarginModalFormErrorType.ExceedMargin,
      })
    }
    if (
      this.direction === MarginManipulation.Decrease &&
      mathIs`${newLeverage} > ${this.info$.maxLeverage}`
    ) {
      return Result.error({
        type: EditMarginModalFormErrorType.LeverageTooHigh,
      })
    }
    return Result.ok({
      amount: this.amount.read$,
      direction: this.direction,
    })
  }

  @computed get maxMargin$(): BigNumber {
    const threshold = this.info$.liquidationThreshold
    const minLeverage = this.info$.minLeverage
    return math`${this.trade.remainingPosition} / max(${threshold}, ${minLeverage})`
  }

  @computed get minMargin$(): BigNumber {
    const maxLeverage = this.info$.maxLeverage
    return math`${this.trade.remainingPosition} / ${maxLeverage}`
  }

  @computed get maxAmountToDeposit$(): BigNumber {
    return math`max(min(${this.balance$}, ${this.maxMargin$} - ${
      this.trade.remainingMargin
    }) - ${0.0000001}, ${0})` // We need this to avoid rounding errors
  }

  @computed get maxAmountToWithdraw$(): BigNumber {
    return math`${this.trade.remainingMargin} - ${this.minMargin$}`
  }

  @asyncAction async changeMargin(
    formData: { direction: MarginManipulation; amount: BigNumber },
    run = runAsyncAction,
  ): Promise<ContractTransaction> {
    const transaction = await run(
      changeMargin(
        formData.direction,
        this.store.appEnvStore.pyth,
        this.store.contracts.tradingCore$,
        this.trade.trade.orderHash,
        this.trade.trade.priceId,
        formData.amount,
      ),
    )
    this.store.orderHistory.editMargin = undefined
    return transaction
  }
}
