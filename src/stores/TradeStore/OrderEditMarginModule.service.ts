import { ContractTransaction } from "ethers"
import { MarginManipulation } from "../../screens/TradeScreen/types"
import { fromNativeToContract } from "../../utils/contractHelpers/bigNumberContracts"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { PythModule } from "../AppEnvStore/PythModule"
import { TradingCore } from "../contracts/generated"

export async function changeMargin(
  direction: MarginManipulation,
  pyth: PythModule,
  tc: TradingCore,
  orderHash: string,
  priceId: string,
  margin: BigNumber,
): Promise<ContractTransaction> {
  const priceData = await pyth.createPriceFeedUpdateDate(priceId)
  const fee = await pyth.getUpdateFee(priceData)
  if (direction === MarginManipulation.Increase) {
    return tc.addMargin(orderHash, priceData, fromNativeToContract(margin), {
      value: fee,
      gasLimit: 3e6,
    })
  } else {
    return tc.removeMargin(orderHash, priceData, fromNativeToContract(margin), {
      value: fee,
      gasLimit: 3e6,
    })
  }
}
