import { action, computed, makeObservable, observable } from "mobx"
import { createTransformer } from "mobx-utils"
import { memoizeWith, uniq, uniqBy } from "ramda"
import type {
  OpenOrderRecord,
  PositionRecord,
  TradeRecord,
} from "../../screens/TradeScreen/types"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import {
  math,
  mathIs,
} from "../../utils/numberHelpers/bigNumberExpressionParser"
import { suspenseResource } from "../../utils/SuspenseResource"
import { ethAddressToHasuraAddress } from "../utils/hasuraClient"
import { LazyValue } from "../utils/LazyValue"
import { PaginationStore } from "../utils/PaginationStore"
import { LimitBookCellModule } from "./LimitBookCellModule"
import { OrderCloseModule } from "./OrderCloseModule"
import { OrderEditMarginModule } from "./OrderEditMarginModule"
import { OrderEditStopsModule } from "./OrderEditStopsModule"
import {
  closeLimitOrderEvents,
  executeLimitOrderEvents,
  OpeningLimitBookEvent,
  openLimitOrderEvents,
  partialCloseLimitOrderEvents,
  updateLimitOrderEvents,
} from "./OrderHistoryModule.limitOrder.service"
import {
  closeOrderEvents,
  MarketOrderEvent,
  openOrderEvents,
  updateOrderEvents,
} from "./OrderHistoryModule.marketOrder.service"
import { TradeCellModule } from "./TradeCellModule"
import { TradeStore } from "./TradeStore"

export class OrderHistoryModule {
  constructor(readonly store: TradeStore) {
    makeObservable(this)
  }

  @observable.ref editMargin?: OrderEditMarginModule
  @action editTradeMargin(trade: TradeCellModule): void {
    this.editMargin = new OrderEditMarginModule(trade, this.store)
  }

  @observable.ref editStops?: OrderEditStopsModule
  @action editTradeStops(trade: TradeCellModule): void {
    this.editStops = new OrderEditStopsModule(trade, this.store)
  }

  @observable.ref closingOrder?: OrderCloseModule
  @observable closingOrderHashes: string[] = []
  @action closeOrder(trade: TradeCellModule): void {
    this.closingOrder = new OrderCloseModule(trade, this.store)
  }

  @observable closingLimitOrderHashes = new Set<string>()

  #openOrderEvents = new LazyValue(
    () =>
      [
        this.store.appEnvStore.hasura$,
        this.store.appEnvStore.appOpenBlockHeight$,
        this.store.authStore.account$,
      ] as const,
    ([tc, block, acc]) =>
      openOrderEvents(tc, block, {
        trade_user: { _eq: ethAddressToHasuraAddress(acc) },
      }),
  )

  #updateOrderEvents = new LazyValue(
    () =>
      [
        this.store.appEnvStore.hasura$,
        this.store.appEnvStore.appOpenBlockHeight$,
        this.store.authStore.account$,
      ] as const,
    ([tc, block, acc]) =>
      updateOrderEvents(tc, block, {
        trade_user: { _eq: ethAddressToHasuraAddress(acc) },
      }),
  )

  #closeOrderEvents = new LazyValue(
    () =>
      [
        this.store.appEnvStore.hasura$,
        this.store.appEnvStore.appOpenBlockHeight$,
        this.store.authStore.account$,
      ] as const,
    ([tc, block, acc]) =>
      closeOrderEvents(tc, block, {
        trade_user: { _eq: ethAddressToHasuraAddress(acc) },
      }),
  )

  cellModule = memoizeWith(
    t => t.txHash,
    (t: MarketOrderEvent) => {
      const openEvent =
        t.type === "open"
          ? t
          : this.#openOrderEvents.value$.find(a => a.orderHash === t.orderHash)!
      return new TradeCellModule(t, this.store.tradeInfo, openEvent)
    },
  )

  @computed get openPositions$(): TradeCellModule[] {
    if (!this.store.authStore.connected) {
      return []
    }
    return this.#openOrderEvents.value$
      .map(
        x => this.allTradeEvents$.find(a => a.trade.orderHash === x.orderHash)!,
      )
      .filter(a => !this.wasClosed$(a.trade.orderHash))
  }

  @computed get openPositionsLength$(): number {
    return this.openPositions$.length
  }

  @computed
  get openPositionsPaginationStore(): PaginationStore<PositionRecord> {
    return new PaginationStore(() =>
      this.openPositions$.map(p => ({
        ...p.positionRecord$,
        canceling: suspenseResource(() =>
          this.closingOrderHashes.includes(p.trade.orderHash),
        ),
        onCancel: () => this.closeOrder(p),
        onEditMargin: () => this.editTradeMargin(p),
        onEditActionTriggers: () => this.editTradeStops(p),
      })),
    )
  }

  @computed get totalOpenPositionMargin$(): BigNumber {
    return this.openPositions$
      .map(a => a.remainingMargin)
      .reduce((a, b) => math`${a} + ${b}`, BigNumber.from(0))
  }

  @computed get unrealizedPnl$(): BigNumber {
    return this.openPositions$
      .map(a => a.unrealizedPnl$.amount)
      .reduce((a, b) => math`${a} + ${b}`, BigNumber.from(0))
  }

  @computed get unrealizedPnlPercentage$(): BigNumber {
    if (mathIs`${this.totalOpenPositionMargin$} == ${0}`) {
      return BigNumber.from(0)
    }
    return math`${this.unrealizedPnl$} / ${this.totalOpenPositionMargin$}`
  }

  @computed get currentOrderHashes$(): string[] {
    if (!this.store.authStore.connected) {
      return []
    }
    const updatedIds = this.#updateOrderEvents.value$.map(o => ({
      id: o.orderHash,
      block: o.blockNumber,
    }))
    const closedIds = this.closedOrderHashes$
    const openIds = this.#openOrderEvents.value$.map(o => ({
      id: o.orderHash,
      block: o.blockNumber,
    }))
    return uniq(
      [...updatedIds, ...openIds]
        .sort((a, b) => (b.block > a.block ? 1 : -1))
        .map(a => a.id),
    ).filter(a => !closedIds.includes(a))
  }

  wasClosed$ = createTransformer((orderHash: string) =>
    this.closedOrderHashes$.includes(orderHash),
  )

  @computed get closedOrderHashes$(): string[] {
    if (!this.store.authStore.connected) {
      return []
    }
    return this.allTradeEvents$
      .filter(a => a.isClosingTrade)
      .map(o => o.trade.orderHash)
  }

  @computed get allTradeEvents$(): TradeCellModule[] {
    if (!this.store.authStore.connected) {
      return []
    }
    const closed = this.#closeOrderEvents.value$
    const updates = this.#updateOrderEvents.value$
    const openTrade = this.#openOrderEvents.value$
    return [...closed, ...updates, ...openTrade]
      .sort((a, b) => Number(b.blockNumber - a.blockNumber))
      .map(this.cellModule)
  }

  @computed get allTradeEventsCount$(): number {
    return this.allTradeEvents$.length
  }

  @computed
  get allTradeEventsPaginationStore(): PaginationStore<TradeRecord> {
    return new PaginationStore(() => this.allTradeEvents$.map(e => e.record$))
  }

  #openLimitOrderEvents = new LazyValue(
    () =>
      [
        this.store.appEnvStore.hasura$,
        this.store.appEnvStore.appOpenBlockHeight$,
        this.store.authStore.account$,
      ] as const,
    ([tc, block, acc]) => openLimitOrderEvents(tc, block, acc),
  )

  #updateLimitOrderEvents = new LazyValue(
    () =>
      [
        this.store.appEnvStore.hasura$,
        this.store.appEnvStore.appOpenBlockHeight$,
        this.store.authStore.account$,
      ] as const,
    ([tc, block, acc]) => updateLimitOrderEvents(tc, block, acc),
  )

  #closeLimitOrderEvents = new LazyValue(
    () =>
      [
        this.store.appEnvStore.hasura$,
        this.store.appEnvStore.appOpenBlockHeight$,
        this.store.authStore.account$,
      ] as const,
    ([tc, block, acc]) => closeLimitOrderEvents(tc, block, acc),
  )

  #executeLimitOrderEvents = new LazyValue(
    () =>
      [
        this.store.appEnvStore.hasura$,
        this.store.appEnvStore.appOpenBlockHeight$,
        this.store.authStore.account$,
      ] as const,
    ([tc, block, acc]) => executeLimitOrderEvents(tc, block, acc),
  )

  #partialCloseLimitOrderEvents = new LazyValue(
    () =>
      [
        this.store.appEnvStore.hasura$,
        this.store.appEnvStore.appOpenBlockHeight$,
        this.store.authStore.account$,
      ] as const,
    ([tc, block, acc]) => partialCloseLimitOrderEvents(tc, block, acc),
  )

  limitBookCellModule = memoizeWith(
    t => t.txHash,
    (t: OpeningLimitBookEvent) => new LimitBookCellModule(t, this, this.store),
  )

  @computed get limitBookEvents$(): LimitBookCellModule[] {
    if (!this.store.authStore.connected) {
      return []
    }
    const openTrade = this.#openLimitOrderEvents.value$
    const closed = this.#closeLimitOrderEvents.value$
    const updates = this.#updateLimitOrderEvents.value$
    const partialClose = this.#partialCloseLimitOrderEvents.value$
    const executed = this.#executeLimitOrderEvents.value$
    const needToBeRemoved = [...closed, ...executed].map(a => a.orderHash)
    const allOrderWithUpdates = [...openTrade, ...updates, ...partialClose]
      .filter(a => !needToBeRemoved.includes(a.orderHash))
      .sort((a, b) => Number(b.blockNumber - a.blockNumber))
    return uniqBy(a => a.orderHash, allOrderWithUpdates).map(
      this.limitBookCellModule,
    )
  }

  @computed get limitBookEventsCount$(): number {
    return this.limitBookEvents$.length
  }

  @computed
  get limitBookEventsPaginationStore(): PaginationStore<OpenOrderRecord> {
    return new PaginationStore(() => this.limitBookEvents$.map(e => e.record$))
  }
}
