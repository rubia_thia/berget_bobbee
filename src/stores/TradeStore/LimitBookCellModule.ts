import { TransactionResponse } from "@ethersproject/providers"
import { computed, makeObservable } from "mobx"
import {
  OpenOrderRecord,
  OrderDirection,
  OrderType,
} from "../../screens/TradeScreen/types"
import { asyncAction, runAsyncAction } from "../../utils/asyncAction"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { math } from "../../utils/numberHelpers/bigNumberExpressionParser"
import { suspenseResource } from "../../utils/SuspenseResource"
import { TokenInfo } from "../../utils/TokenInfo"
import { closeLimitOrder } from "./OrderCreationModule.service"
import { OrderHistoryModule } from "./OrderHistoryModule"
import { OpeningLimitBookEvent } from "./OrderHistoryModule.limitOrder.service"
import { TradeStore } from "./TradeStore"

export class LimitBookCellModule {
  constructor(
    readonly trade: OpeningLimitBookEvent,
    readonly history: OrderHistoryModule,
    readonly store: TradeStore,
  ) {
    makeObservable(this)
  }

  @computed get remainingMargin(): BigNumber {
    const margin = this.trade.margin
    if (this.trade.type === "PartialCloseLimitOrderEvent") {
      return math`${margin} x (${1} - ${this.trade.closePercentage})`
    }
    return margin
  }

  @computed get leverage(): BigNumber {
    return this.trade.leverage
  }
  //
  @computed get position(): BigNumber {
    return math`${this.leverage} x ${this.remainingMargin}`
  }

  // @computed get currentMarketPrice$(): BigNumber {
  //   return this.store.appEnvStore.pyth.priceFor(this.trade.priceId).value$.price
  // }

  @computed get baseTokenInfo$(): TokenInfo {
    const { currencyStore, appEnvStore } = this.store
    return currencyStore.baseTokenInfo$(
      appEnvStore.pyth.priceSymbolFromId(this.trade.priceId)!,
    )
  }

  @computed get quoteTokenInfo$(): TokenInfo {
    return this.store.anchorTokenInfo$
  }

  @computed get record$(): OpenOrderRecord {
    return {
      orderHash: this.trade.orderHash,
      baseToken: this.baseTokenInfo$,
      quoteToken: this.quoteTokenInfo$,
      leverage: this.leverage,
      margin: this.remainingMargin,
      orderDirection: this.orderDirection,
      orderType: OrderType.Limit,
      position: this.position,
      isCanceling: suspenseResource(() =>
        this.history.closingLimitOrderHashes.has(this.trade.orderHash),
      ),
      createdAt: new Date(this.trade.blockTime),
      askPrice: this.trade.openPrice,
    }
  }

  @asyncAction
  async cancelOrder(run = runAsyncAction): Promise<TransactionResponse> {
    return await run(
      closeLimitOrder({
        limitBook: this.store.contracts.limitBook$,
        priceId: this.trade.priceId,
        orderHash: this.trade.orderHash,
        percentage: BigNumber.from(1),
      }),
    )
  }

  @computed get orderDirection(): OrderDirection {
    return this.trade.isLong ? OrderDirection.Long : OrderDirection.Short
  }
}
