import { FCC } from "../../utils/reactHelpers/types"
import { useCreation } from "../../utils/reactHelpers/useCreation"
import { useAppEnvStore } from "../AppEnvStore/useAppEnvStore"
import { useAuthStore } from "../AuthStore/useAuthStore"
import { useCurrencyStore } from "../CurrencyStore/useCurrencyStore"
import { createStore } from "../utils/createStore"
import { ContractStore } from "./ContractStore"

export interface ContextValue {
  forContractCall: ContractStore
  forEventListen: ContractStore
}

const { Provider, useStore } = createStore<ContextValue>("ContractStore")

export const useContractStore = (): ContractStore => {
  return useStore().forContractCall
}

export const useContractStoreForEventListen = (): ContractStore => {
  return useStore().forEventListen
}

export const ContractStoreProvider: FCC = props => {
  const appEnvStore = useAppEnvStore()
  const authStore = useAuthStore()
  const currencyStore = useCurrencyStore()
  const value: ContextValue = useCreation(
    () => ({
      forContractCall: new ContractStore(
        appEnvStore,
        currencyStore,
        () => authStore.signerOrLibrary$,
      ),
      forEventListen: new ContractStore(
        appEnvStore,
        currencyStore,
        () => appEnvStore.websocketProvider$,
      ),
    }),
    [appEnvStore, authStore, currencyStore],
  )
  return <Provider store={value}>{props.children}</Provider>
}
