/* Autogenerated file. Do not edit manually. */
/* tslint:disable */
/* eslint-disable */
import type { Provider, TransactionRequest } from "@ethersproject/providers"
import { Contract, ContractFactory, Overrides, Signer, utils } from "ethers"
import type { PromiseOrValue } from "../common"
import type { TraderFarm, TraderFarmInterface } from "../TraderFarm"

const _abi = [
  {
    inputs: [],
    stateMutability: "nonpayable",
    type: "constructor",
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: "address",
        name: "rewardToken",
        type: "address",
      },
    ],
    name: "AddRewardTokenEvent",
    type: "event",
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: "address",
        name: "user",
        type: "address",
      },
      {
        indexed: true,
        internalType: "address",
        name: "rewardToken",
        type: "address",
      },
      {
        indexed: false,
        internalType: "uint256",
        name: "claimed",
        type: "uint256",
      },
    ],
    name: "ClaimEvent",
    type: "event",
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: false,
        internalType: "uint8",
        name: "version",
        type: "uint8",
      },
    ],
    name: "Initialized",
    type: "event",
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: "address",
        name: "previousOwner",
        type: "address",
      },
      {
        indexed: true,
        internalType: "address",
        name: "newOwner",
        type: "address",
      },
    ],
    name: "OwnershipTransferred",
    type: "event",
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: false,
        internalType: "bool",
        name: "paused",
        type: "bool",
      },
    ],
    name: "PauseEvent",
    type: "event",
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: "address",
        name: "rewardToken",
        type: "address",
      },
    ],
    name: "RemoveRewardTokenEvent",
    type: "event",
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: "bytes32",
        name: "role",
        type: "bytes32",
      },
      {
        indexed: true,
        internalType: "bytes32",
        name: "previousAdminRole",
        type: "bytes32",
      },
      {
        indexed: true,
        internalType: "bytes32",
        name: "newAdminRole",
        type: "bytes32",
      },
    ],
    name: "RoleAdminChanged",
    type: "event",
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: "bytes32",
        name: "role",
        type: "bytes32",
      },
      {
        indexed: true,
        internalType: "address",
        name: "account",
        type: "address",
      },
      {
        indexed: true,
        internalType: "address",
        name: "sender",
        type: "address",
      },
    ],
    name: "RoleGranted",
    type: "event",
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: "bytes32",
        name: "role",
        type: "bytes32",
      },
      {
        indexed: true,
        internalType: "address",
        name: "account",
        type: "address",
      },
      {
        indexed: true,
        internalType: "address",
        name: "sender",
        type: "address",
      },
    ],
    name: "RoleRevoked",
    type: "event",
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: "address",
        name: "sender",
        type: "address",
      },
      {
        indexed: true,
        internalType: "address",
        name: "user",
        type: "address",
      },
      {
        indexed: false,
        internalType: "uint256",
        name: "amount",
        type: "uint256",
      },
    ],
    name: "StakeEvent",
    type: "event",
  },
  {
    inputs: [],
    name: "APPROVED_ROLE",
    outputs: [
      {
        internalType: "bytes32",
        name: "",
        type: "bytes32",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [],
    name: "DEFAULT_ADMIN_ROLE",
    outputs: [
      {
        internalType: "bytes32",
        name: "",
        type: "bytes32",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "contract IMintable",
        name: "rewardToken",
        type: "address",
      },
    ],
    name: "addRewardToken",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [],
    name: "blocksPerRun",
    outputs: [
      {
        internalType: "uint256",
        name: "",
        type: "uint256",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "address",
        name: "_user",
        type: "address",
      },
    ],
    name: "claim",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "address",
        name: "_user",
        type: "address",
      },
      {
        internalType: "address",
        name: "_rewardToken",
        type: "address",
      },
    ],
    name: "claim",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [],
    name: "claim",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "uint64",
        name: "",
        type: "uint64",
      },
      {
        internalType: "address",
        name: "",
        type: "address",
      },
    ],
    name: "claimedByUserPerRun",
    outputs: [
      {
        internalType: "bool",
        name: "",
        type: "bool",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "uint64",
        name: "",
        type: "uint64",
      },
      {
        internalType: "address",
        name: "",
        type: "address",
      },
      {
        internalType: "address",
        name: "",
        type: "address",
      },
    ],
    name: "claimedRewardTokenByUserPerRun",
    outputs: [
      {
        internalType: "bool",
        name: "",
        type: "bool",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "address",
        name: "rewardToken",
        type: "address",
      },
    ],
    name: "contains",
    outputs: [
      {
        internalType: "bool",
        name: "",
        type: "bool",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "address",
        name: "_user",
        type: "address",
      },
      {
        internalType: "address",
        name: "_rewardToken",
        type: "address",
      },
    ],
    name: "getRewards",
    outputs: [
      {
        internalType: "uint256",
        name: "claimed",
        type: "uint256",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "bytes32",
        name: "role",
        type: "bytes32",
      },
    ],
    name: "getRoleAdmin",
    outputs: [
      {
        internalType: "bytes32",
        name: "",
        type: "bytes32",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "address",
        name: "_user",
        type: "address",
      },
    ],
    name: "getStaked",
    outputs: [
      {
        internalType: "uint256",
        name: "",
        type: "uint256",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [],
    name: "getTotalStaked",
    outputs: [
      {
        internalType: "uint256",
        name: "",
        type: "uint256",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "bytes32",
        name: "role",
        type: "bytes32",
      },
      {
        internalType: "address",
        name: "account",
        type: "address",
      },
    ],
    name: "grantRole",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "bytes32",
        name: "role",
        type: "bytes32",
      },
      {
        internalType: "address",
        name: "account",
        type: "address",
      },
    ],
    name: "hasRole",
    outputs: [
      {
        internalType: "bool",
        name: "",
        type: "bool",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "address",
        name: "_user",
        type: "address",
      },
    ],
    name: "hasStake",
    outputs: [
      {
        internalType: "bool",
        name: "",
        type: "bool",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [],
    name: "initialRun",
    outputs: [
      {
        internalType: "uint64",
        name: "",
        type: "uint64",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "address",
        name: "owner",
        type: "address",
      },
      {
        internalType: "uint256",
        name: "_blocksPerRun",
        type: "uint256",
      },
    ],
    name: "initialize",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [],
    name: "owner",
    outputs: [
      {
        internalType: "address",
        name: "",
        type: "address",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [],
    name: "pauseStaking",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "contract IMintable",
        name: "rewardToken",
        type: "address",
      },
    ],
    name: "removeRewardToken",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [],
    name: "renounceOwnership",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "bytes32",
        name: "role",
        type: "bytes32",
      },
      {
        internalType: "address",
        name: "account",
        type: "address",
      },
    ],
    name: "renounceRole",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "bytes32",
        name: "role",
        type: "bytes32",
      },
      {
        internalType: "address",
        name: "account",
        type: "address",
      },
    ],
    name: "revokeRole",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "uint256",
        name: "amount",
        type: "uint256",
      },
    ],
    name: "stake",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "address",
        name: "staker",
        type: "address",
      },
      {
        internalType: "uint256",
        name: "amount",
        type: "uint256",
      },
    ],
    name: "stake",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [],
    name: "stakingPaused",
    outputs: [
      {
        internalType: "bool",
        name: "",
        type: "bool",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "bytes4",
        name: "interfaceId",
        type: "bytes4",
      },
    ],
    name: "supportsInterface",
    outputs: [
      {
        internalType: "bool",
        name: "",
        type: "bool",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "address",
        name: "newOwner",
        type: "address",
      },
    ],
    name: "transferOwnership",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [],
    name: "unpauseStaking",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "uint256",
        name: "amount",
        type: "uint256",
      },
    ],
    name: "unstake",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "address",
        name: "staker",
        type: "address",
      },
      {
        internalType: "uint256",
        name: "amount",
        type: "uint256",
      },
    ],
    name: "unstake",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "uint64",
        name: "",
        type: "uint64",
      },
      {
        internalType: "address",
        name: "",
        type: "address",
      },
    ],
    name: "volumeByUserPerRun",
    outputs: [
      {
        internalType: "uint256",
        name: "",
        type: "uint256",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "uint64",
        name: "",
        type: "uint64",
      },
    ],
    name: "volumePerRun",
    outputs: [
      {
        internalType: "uint256",
        name: "",
        type: "uint256",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
]

const _bytecode =
  "0x608060405234801561001057600080fd5b5061001961001e565b6100de565b600054610100900460ff161561008a5760405162461bcd60e51b815260206004820152602760248201527f496e697469616c697a61626c653a20636f6e747261637420697320696e697469604482015266616c697a696e6760c81b606482015260840160405180910390fd5b60005460ff90811610156100dc576000805460ff191660ff9081179091556040519081527f7f26b83ff96e1f2b6a682f133852f6798a09c465da95921460cefb38474024989060200160405180910390a15b565b61207b80620000ee6000396000f3fe608060405234801561001057600080fd5b50600436106102415760003560e01c8063779bcb9b11610145578063bbb781cc116100bd578063d547741f1161008c578063e73e14bf11610071578063e73e14bf14610559578063f2fde38b14610567578063f999c5061461057a57600080fd5b8063d547741f14610514578063dadfac741461052757600080fd5b8063bbb781cc146104ac578063be6a762d146104b9578063c2a672e0146104ee578063cd6dc6871461050157600080fd5b806391d1485411610114578063a217fddf116100f9578063a217fddf1461047e578063a694fc3a14610486578063adc9772e1461049957600080fd5b806391d148541461043d57806393f4bcde1461047657600080fd5b8063779bcb9b146103d75780637aab5db4146103ea5780638da5cb5b146104195780638eab35c41461043457600080fd5b806336568abe116101d85780633d509c97116101a75780634e71d92d1161018c5780634e71d92d146103b45780635dbe47e8146103bc578063715018a6146103cf57600080fd5b80633d509c971461037a5780634bda377a1461038d57600080fd5b806336568abe1461030857806336ff40c71461031b578063385aedc91461033c578063399080ec1461036757600080fd5b806321c0b3421161021457806321c0b342146102ac578063248a9ca3146102bf5780632e17de78146102e25780632f2ff15d146102f557600080fd5b806301ffc9a7146102465780630917e7761461026e5780631c03e6cc146102845780631e83409a14610299575b600080fd5b610259610254366004611c96565b610582565b60405190151581526020015b60405180910390f35b61027661061b565b604051908152602001610265565b610297610292366004611ced565b61062b565b005b6102976102a7366004611ced565b610676565b6102976102ba366004611d0a565b6106a6565b6102766102cd366004611d43565b60009081526097602052604090206001015490565b6102976102f0366004611d43565b6106d8565b610297610303366004611d5c565b6106e3565b610297610316366004611d5c565b61070d565b610276610329366004611d99565b6101016020526000908152604090205481565b61027661034a366004611db4565b60ff60209081526000928352604080842090915290825290205481565b610276610375366004611ced565b61079a565b610297610388366004611ced565b6107ac565b6102767f4a0c3698e72495f6d49f6ef074f2b34cac5b153c817a7cc37789cccbb873cf5d81565b6102976107f7565b6102596103ca366004611ced565b610826565b610297610833565b6102766103e5366004611d0a565b610845565b6102596103f8366004611db4565b61010060209081526000928352604080842090915290825290205460ff1681565b6033546040516001600160a01b039091168152602001610265565b61027660fc5481565b61025961044b366004611d5c565b60009182526097602090815260408084206001600160a01b0393909316845291905290205460ff1690565b610297610a39565b610276600081565b610297610494366004611d43565b610a82565b6102976104a7366004611dd0565b610a8c565b60fb546102599060ff1681565b6102596104c7366004611dfc565b61010260209081526000938452604080852082529284528284209052825290205460ff1681565b6102976104fc366004611dd0565b610bd3565b61029761050f366004611dd0565b610bde565b610297610522366004611d5c565b610d7e565b60fb5461054090610100900467ffffffffffffffff1681565b60405167ffffffffffffffff9091168152602001610265565b610259610375366004611ced565b610297610575366004611ced565b610da3565b610297610e30565b60007fffffffff0000000000000000000000000000000000000000000000000000000082167f7965db0b00000000000000000000000000000000000000000000000000000000148061061557507f01ffc9a7000000000000000000000000000000000000000000000000000000007fffffffff000000000000000000000000000000000000000000000000000000008316145b92915050565b60006106286103e6610e78565b90565b610633610ea2565b61063e60fd82610efc565b506040516001600160a01b038216907f2468839e21a847afe14ae0ce4459ef894e5faf8a2e00acd7190f91bf8df96e2590600090a250565b60fb546106889060ff161560c9610f18565b610690610f26565b61069981610f7f565b6106a3600160c955565b50565b60fb546106b89060ff161560c9610f18565b6106c0610f26565b6106ca8282610fc3565b6106d4600160c955565b5050565b6106a36103e6610e78565b6000828152609760205260409020600101546106fe816112ca565b61070883836112d4565b505050565b6001600160a01b03811633146107905760405162461bcd60e51b815260206004820152602f60248201527f416363657373436f6e74726f6c3a2063616e206f6e6c792072656e6f756e636560448201527f20726f6c657320666f722073656c66000000000000000000000000000000000060648201526084015b60405180910390fd5b6106d48282611376565b60006107a76103e6610e78565b919050565b6107b4610ea2565b6107bf60fd826113f9565b506040516001600160a01b038216907fcb6ba994194a6d9172eabc8c4f65c9bbec7bc30ebf5c28697a29cda13160a38d90600090a250565b60fb546108099060ff161560c9610f18565b610811610f26565b61081a33610f7f565b610824600160c955565b565b600061061560fd8361140e565b61083b610ea2565b6108246000611430565b60008060fc54436108569190611e5b565b60009250905061086760fd8461140e565b15610a325760fb54610100900467ffffffffffffffff165b818167ffffffffffffffff1610156109955767ffffffffffffffff81166000908152610100602090815260408083206001600160a01b038916845290915290205460ff1615801561090b575067ffffffffffffffff81166000908152610102602090815260408083206001600160a01b03808a168552908352818420908816845290915290205460ff16155b8015610930575067ffffffffffffffff81166000908152610101602052604090205415155b156109855767ffffffffffffffff81166000908152610101602090815260408083205460ff83528184206001600160a01b038a168552909252909120546109829161097b919061149a565b84906114f0565b92505b61098e81611e7d565b905061087f565b5060fc546040517f18b9f9e0000000000000000000000000000000000000000000000000000000008152306004820152610a25906001600160a01b038616906318b9f9e090602401602060405180830381865afa1580156109fa573d6000803e3d6000fd5b505050506040513d601f19601f82011682018060405250810190610a1e9190611ea4565b849061150d565b610a2f9190611ebd565b91505b5092915050565b610a41610ea2565b60fb805460ff19169055604051600081527f10e1c3fcaff06b68391033547e8f9bb8067d7c4a2e32659b0629153814d242d3906020015b60405180910390a1565b6106a360c8610e78565b60fb54610a9e9060ff161560c9610f18565b610aa6610f26565b7f4a0c3698e72495f6d49f6ef074f2b34cac5b153c817a7cc37789cccbb873cf5d610ad0816112ca565b6000610ae860fc5443610ae39190611e5b565b61154d565b67ffffffffffffffff8116600090815260ff602090815260408083206001600160a01b0389168452909152902054909150610b2390846114f0565b67ffffffffffffffff8216600081815260ff602090815260408083206001600160a01b038a1684528252808320949094559181526101019091522054610b6990846114f0565b67ffffffffffffffff82166000908152610101602090815260409182902092909255518481526001600160a01b0386169133917f160ffcaa807f78c8b4983836e2396338d073e75695ac448aa0b5e4a75b210b1d910160405180910390a350506106d4600160c955565b6106d46103e6610e78565b600054610100900460ff1615808015610bfe5750600054600160ff909116105b80610c185750303b158015610c18575060005460ff166001145b610c8a5760405162461bcd60e51b815260206004820152602e60248201527f496e697469616c697a61626c653a20636f6e747261637420697320616c72656160448201527f647920696e697469616c697a65640000000000000000000000000000000000006064820152608401610787565b6000805460ff191660011790558015610cad576000805461ff0019166101001790555b610cb56115d1565b610cbd61163c565b610cc56116af565b610cce83611430565b610cd96000846112d4565b60fc829055610ceb610ae38343611e5b565b60fb805460ff1967ffffffffffffffff9390931661010002929092167fffffffffffffffffffffffffffffffffffffffffffffff0000000000000000009092169190911760011790558015610708576000805461ff0019169055604051600181527f7f26b83ff96e1f2b6a682f133852f6798a09c465da95921460cefb38474024989060200160405180910390a1505050565b600082815260976020526040902060010154610d99816112ca565b6107088383611376565b610dab610ea2565b6001600160a01b038116610e275760405162461bcd60e51b815260206004820152602660248201527f4f776e61626c653a206e6577206f776e657220697320746865207a65726f206160448201527f64647265737300000000000000000000000000000000000000000000000000006064820152608401610787565b6106a381611430565b610e38610ea2565b60fb805460ff191660019081179091556040519081527f10e1c3fcaff06b68391033547e8f9bb8067d7c4a2e32659b0629153814d242d390602001610a78565b6106a3817f554e570000000000000000000000000000000000000000000000000000000000611722565b6033546001600160a01b031633146108245760405162461bcd60e51b815260206004820181905260248201527f4f776e61626c653a2063616c6c6572206973206e6f7420746865206f776e65726044820152606401610787565b6000610f11836001600160a01b038416611785565b9392505050565b816106d4576106d481610e78565b600260c95403610f785760405162461bcd60e51b815260206004820152601f60248201527f5265656e7472616e637947756172643a207265656e7472616e742063616c6c006044820152606401610787565b600260c955565b6000610f8b60fd6117d4565b905060005b8181101561070857610fac83610fa760fd846117de565b610fc3565b610fb581611ed4565b9050610f90565b600160c955565b610fd9610fd160fd8361140e565b6102bf610f18565b600060fc5443610fe99190611e5b565b60fb54909150600090610100900467ffffffffffffffff165b828167ffffffffffffffff1610156111595767ffffffffffffffff81166000908152610100602090815260408083206001600160a01b038916845290915290205460ff1615801561108e575067ffffffffffffffff81166000908152610102602090815260408083206001600160a01b03808a168552908352818420908816845290915290205460ff16155b156111495767ffffffffffffffff811660009081526101016020526040902054156111035767ffffffffffffffff81166000908152610101602090815260408083205460ff83528184206001600160a01b038a16855290925290912054611100916110f9919061149a565b83906114f0565b91505b67ffffffffffffffff81166000908152610102602090815260408083206001600160a01b03808a16855290835281842090881684529091529020805460ff191660011790555b61115281611e7d565b9050611002565b5060fc546040517f18b9f9e00000000000000000000000000000000000000000000000000000000081523060048201526111e9906001600160a01b038616906318b9f9e090602401602060405180830381865afa1580156111be573d6000803e3d6000fd5b505050506040513d601f19601f820116820180604052508101906111e29190611ea4565b839061150d565b6111f39190611ebd565b90508015611277576040517f40c10f190000000000000000000000000000000000000000000000000000000081526001600160a01b038581166004830152602482018390528416906340c10f1990604401600060405180830381600087803b15801561125e57600080fd5b505af1158015611272573d6000803e3d6000fd5b505050505b826001600160a01b0316846001600160a01b03167fc6523614a4ee35b875baef260f78a91708fdf636c7fec1e9a186a7ae590c4e46836040516112bc91815260200190565b60405180910390a350505050565b6106a381336117ea565b60008281526097602090815260408083206001600160a01b038516845290915290205460ff166106d45760008281526097602090815260408083206001600160a01b03851684529091529020805460ff191660011790556113323390565b6001600160a01b0316816001600160a01b0316837f2f8788117e7eff1d82e926ec794901d17c78024a50270940304540a733656f0d60405160405180910390a45050565b60008281526097602090815260408083206001600160a01b038516845290915290205460ff16156106d45760008281526097602090815260408083206001600160a01b0385168085529252808320805460ff1916905551339285917ff6391f5c32d9c69d2a47ea670b442974b53935d1edc7fd64eb21e047a839171b9190a45050565b6000610f11836001600160a01b03841661185f565b6001600160a01b03811660009081526001830160205260408120541515610f11565b603380546001600160a01b038381167fffffffffffffffffffffffff0000000000000000000000000000000000000000831681179093556040519116919082907f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e090600090a35050565b60006114a98215156004610f18565b60006114bd670de0b6b3a764000085611ebd565b90506114e68415806114df5750670de0b6b3a76400006114dd8684611e5b565b145b6005610f18565b610a2f8382611e5b565b6000806114fd8385611eee565b9050610f11848210156000610f18565b60008061151a8385611ebd565b905061153b8415806115345750836115328684611e5b565b145b6003610f18565b610a2f670de0b6b3a764000082611e5b565b600067ffffffffffffffff8211156115cd5760405162461bcd60e51b815260206004820152602660248201527f53616665436173743a2076616c756520646f65736e27742066697420696e203660448201527f34206269747300000000000000000000000000000000000000000000000000006064820152608401610787565b5090565b600054610100900460ff166108245760405162461bcd60e51b815260206004820152602b60248201527f496e697469616c697a61626c653a20636f6e7472616374206973206e6f74206960448201526a6e697469616c697a696e6760a81b6064820152608401610787565b600054610100900460ff166116a75760405162461bcd60e51b815260206004820152602b60248201527f496e697469616c697a61626c653a20636f6e7472616374206973206e6f74206960448201526a6e697469616c697a696e6760a81b6064820152608401610787565b610824611952565b600054610100900460ff1661171a5760405162461bcd60e51b815260206004820152602b60248201527f496e697469616c697a61626c653a20636f6e7472616374206973206e6f74206960448201526a6e697469616c697a696e6760a81b6064820152608401610787565b6108246119bd565b62461bcd60e51b600090815260206004526007602452600a808404818106603090810160081b958390069590950190829004918206850160101b01602363ffffff0060e086901c160160181b0190930160c81b604481905260e883901c91606490fd5b60008181526001830160205260408120546117cc57508154600181810184556000848152602080822090930184905584548482528286019093526040902091909155610615565b506000610615565b6000610615825490565b6000610f118383611a31565b60008281526097602090815260408083206001600160a01b038516845290915290205460ff166106d45761181d81611a5b565b611828836020611a6d565b604051602001611839929190611f25565b60408051601f198184030181529082905262461bcd60e51b825261078791600401611fa6565b60008181526001830160205260408120548015611948576000611883600183611fd9565b855490915060009061189790600190611fd9565b90508181146118fc5760008660000182815481106118b7576118b7611fec565b90600052602060002001549050808760000184815481106118da576118da611fec565b6000918252602080832090910192909255918252600188019052604090208390555b855486908061190d5761190d612002565b600190038181906000526020600020016000905590558560010160008681526020019081526020016000206000905560019350505050610615565b6000915050610615565b600054610100900460ff16610fbc5760405162461bcd60e51b815260206004820152602b60248201527f496e697469616c697a61626c653a20636f6e7472616374206973206e6f74206960448201526a6e697469616c697a696e6760a81b6064820152608401610787565b600054610100900460ff16611a285760405162461bcd60e51b815260206004820152602b60248201527f496e697469616c697a61626c653a20636f6e7472616374206973206e6f74206960448201526a6e697469616c697a696e6760a81b6064820152608401610787565b61082433611430565b6000826000018281548110611a4857611a48611fec565b9060005260206000200154905092915050565b60606106156001600160a01b03831660145b60606000611a7c836002611ebd565b611a87906002611eee565b67ffffffffffffffff811115611a9f57611a9f612018565b6040519080825280601f01601f191660200182016040528015611ac9576020820181803683370190505b5090507f300000000000000000000000000000000000000000000000000000000000000081600081518110611b0057611b00611fec565b60200101907effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff1916908160001a9053507f780000000000000000000000000000000000000000000000000000000000000081600181518110611b6357611b63611fec565b60200101907effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff1916908160001a9053506000611b9f846002611ebd565b611baa906001611eee565b90505b6001811115611c47577f303132333435363738396162636465660000000000000000000000000000000085600f1660108110611beb57611beb611fec565b1a60f81b828281518110611c0157611c01611fec565b60200101907effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff1916908160001a90535060049490941c93611c408161202e565b9050611bad565b508315610f115760405162461bcd60e51b815260206004820181905260248201527f537472696e67733a20686578206c656e67746820696e73756666696369656e746044820152606401610787565b600060208284031215611ca857600080fd5b81357fffffffff0000000000000000000000000000000000000000000000000000000081168114610f1157600080fd5b6001600160a01b03811681146106a357600080fd5b600060208284031215611cff57600080fd5b8135610f1181611cd8565b60008060408385031215611d1d57600080fd5b8235611d2881611cd8565b91506020830135611d3881611cd8565b809150509250929050565b600060208284031215611d5557600080fd5b5035919050565b60008060408385031215611d6f57600080fd5b823591506020830135611d3881611cd8565b803567ffffffffffffffff811681146107a757600080fd5b600060208284031215611dab57600080fd5b610f1182611d81565b60008060408385031215611dc757600080fd5b611d2883611d81565b60008060408385031215611de357600080fd5b8235611dee81611cd8565b946020939093013593505050565b600080600060608486031215611e1157600080fd5b611e1a84611d81565b92506020840135611e2a81611cd8565b91506040840135611e3a81611cd8565b809150509250925092565b634e487b7160e01b600052601160045260246000fd5b600082611e7857634e487b7160e01b600052601260045260246000fd5b500490565b600067ffffffffffffffff808316818103611e9a57611e9a611e45565b6001019392505050565b600060208284031215611eb657600080fd5b5051919050565b808202811582820484141761061557610615611e45565b60006000198203611ee757611ee7611e45565b5060010190565b8082018082111561061557610615611e45565b60005b83811015611f1c578181015183820152602001611f04565b50506000910152565b7f416363657373436f6e74726f6c3a206163636f756e7420000000000000000000815260008351611f5d816017850160208801611f01565b7f206973206d697373696e6720726f6c65200000000000000000000000000000006017918401918201528351611f9a816028840160208801611f01565b01602801949350505050565b6020815260008251806020840152611fc5816040850160208701611f01565b601f01601f19169190910160400192915050565b8181038181111561061557610615611e45565b634e487b7160e01b600052603260045260246000fd5b634e487b7160e01b600052603160045260246000fd5b634e487b7160e01b600052604160045260246000fd5b60008161203d5761203d611e45565b50600019019056fea2646970667358221220e9d20f8d210d0151b7cf550690d5b22db923a0dc904b086a43cfe8c3a39d8bab64736f6c63430008110033"

type TraderFarmConstructorParams =
  | [signer?: Signer]
  | ConstructorParameters<typeof ContractFactory>

const isSuperArgs = (
  xs: TraderFarmConstructorParams,
): xs is ConstructorParameters<typeof ContractFactory> => xs.length > 1

export class TraderFarm__factory extends ContractFactory {
  constructor(...args: TraderFarmConstructorParams) {
    if (isSuperArgs(args)) {
      super(...args)
    } else {
      super(_abi, _bytecode, args[0])
    }
  }

  override deploy(
    overrides?: Overrides & { from?: PromiseOrValue<string> },
  ): Promise<TraderFarm> {
    return super.deploy(overrides || {}) as Promise<TraderFarm>
  }
  override getDeployTransaction(
    overrides?: Overrides & { from?: PromiseOrValue<string> },
  ): TransactionRequest {
    return super.getDeployTransaction(overrides || {})
  }
  override attach(address: string): TraderFarm {
    return super.attach(address) as TraderFarm
  }
  override connect(signer: Signer): TraderFarm__factory {
    return super.connect(signer) as TraderFarm__factory
  }

  static readonly bytecode = _bytecode
  static readonly abi = _abi
  static createInterface(): TraderFarmInterface {
    return new utils.Interface(_abi) as TraderFarmInterface
  }
  static connect(
    address: string,
    signerOrProvider: Signer | Provider,
  ): TraderFarm {
    return new Contract(address, _abi, signerOrProvider) as TraderFarm
  }
}
