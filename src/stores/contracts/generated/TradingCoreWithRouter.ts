/* Autogenerated file. Do not edit manually. */
/* tslint:disable */
/* eslint-disable */
import type {
  EventFragment,
  FunctionFragment,
  Result,
} from "@ethersproject/abi"
import type { Listener, Provider } from "@ethersproject/providers"
import type {
  BaseContract,
  BigNumber,
  BigNumberish,
  BytesLike,
  CallOverrides,
  ContractTransaction,
  Overrides,
  PayableOverrides,
  PopulatedTransaction,
  Signer,
  utils,
} from "ethers"
import type {
  OnEvent,
  PromiseOrValue,
  TypedEvent,
  TypedEventFilter,
  TypedListener,
} from "./common"

export declare namespace ISwapRouter {
  export type SwapGivenOutInputStruct = {
    tokenIn: PromiseOrValue<string>
    tokenOut: PromiseOrValue<string>
    amountOut: PromiseOrValue<BigNumberish>
    amountInMaximum: PromiseOrValue<BigNumberish>
    poolFee: PromiseOrValue<BigNumberish>
  }

  export type SwapGivenOutInputStructOutput = [
    string,
    string,
    BigNumber,
    BigNumber,
    number,
  ] & {
    tokenIn: string
    tokenOut: string
    amountOut: BigNumber
    amountInMaximum: BigNumber
    poolFee: number
  }

  export type SwapGivenInInputStruct = {
    tokenIn: PromiseOrValue<string>
    tokenOut: PromiseOrValue<string>
    amountIn: PromiseOrValue<BigNumberish>
    amountOutMinimum: PromiseOrValue<BigNumberish>
    poolFee: PromiseOrValue<BigNumberish>
  }

  export type SwapGivenInInputStructOutput = [
    string,
    string,
    BigNumber,
    BigNumber,
    number,
  ] & {
    tokenIn: string
    tokenOut: string
    amountIn: BigNumber
    amountOutMinimum: BigNumber
    poolFee: number
  }
}

export declare namespace IBook {
  export type OpenTradeInputStruct = {
    priceId: PromiseOrValue<BytesLike>
    user: PromiseOrValue<string>
    isBuy: PromiseOrValue<boolean>
    margin: PromiseOrValue<BigNumberish>
    leverage: PromiseOrValue<BigNumberish>
    profitTarget: PromiseOrValue<BigNumberish>
    stopLoss: PromiseOrValue<BigNumberish>
    limitPrice: PromiseOrValue<BigNumberish>
  }

  export type OpenTradeInputStructOutput = [
    string,
    string,
    boolean,
    BigNumber,
    BigNumber,
    BigNumber,
    BigNumber,
    BigNumber,
  ] & {
    priceId: string
    user: string
    isBuy: boolean
    margin: BigNumber
    leverage: BigNumber
    profitTarget: BigNumber
    stopLoss: BigNumber
    limitPrice: BigNumber
  }

  export type CloseTradeInputStruct = {
    orderHash: PromiseOrValue<BytesLike>
    limitPrice: PromiseOrValue<BigNumberish>
    closePercent: PromiseOrValue<BigNumberish>
  }

  export type CloseTradeInputStructOutput = [string, BigNumber, BigNumber] & {
    orderHash: string
    limitPrice: BigNumber
    closePercent: BigNumber
  }
}

export declare namespace IRegistry {
  export type TradeStruct = {
    user: PromiseOrValue<string>
    isBuy: PromiseOrValue<boolean>
    executionBlock: PromiseOrValue<BigNumberish>
    executionTime: PromiseOrValue<BigNumberish>
    priceId: PromiseOrValue<BytesLike>
    margin: PromiseOrValue<BigNumberish>
    leverage: PromiseOrValue<BigNumberish>
    openPrice: PromiseOrValue<BigNumberish>
    slippage: PromiseOrValue<BigNumberish>
    liquidationPrice: PromiseOrValue<BigNumberish>
    profitTarget: PromiseOrValue<BigNumberish>
    stopLoss: PromiseOrValue<BigNumberish>
    maxPercentagePnL: PromiseOrValue<BigNumberish>
    salt: PromiseOrValue<BigNumberish>
  }

  export type TradeStructOutput = [
    string,
    boolean,
    number,
    number,
    string,
    BigNumber,
    BigNumber,
    BigNumber,
    BigNumber,
    BigNumber,
    BigNumber,
    BigNumber,
    BigNumber,
    BigNumber,
  ] & {
    user: string
    isBuy: boolean
    executionBlock: number
    executionTime: number
    priceId: string
    margin: BigNumber
    leverage: BigNumber
    openPrice: BigNumber
    slippage: BigNumber
    liquidationPrice: BigNumber
    profitTarget: BigNumber
    stopLoss: BigNumber
    maxPercentagePnL: BigNumber
    salt: BigNumber
  }
}

export declare namespace IFee {
  export type FeeStruct = {
    fee: PromiseOrValue<BigNumberish>
    referredFee: PromiseOrValue<BigNumberish>
    referralFee: PromiseOrValue<BigNumberish>
    referralCode: PromiseOrValue<BytesLike>
    referrer: PromiseOrValue<string>
  }

  export type FeeStructOutput = [
    BigNumber,
    BigNumber,
    BigNumber,
    string,
    string,
  ] & {
    fee: BigNumber
    referredFee: BigNumber
    referralFee: BigNumber
    referralCode: string
    referrer: string
  }
}

export interface TradingCoreWithRouterInterface extends utils.Interface {
  functions: {
    "addAllowlist(address[])": FunctionFragment
    "addMargin(bytes32,bytes[],(address,address,uint256,uint256,uint24))": FunctionFragment
    "allowlist()": FunctionFragment
    "allowlisted(address)": FunctionFragment
    "approveToken(address,bool)": FunctionFragment
    "approvedToken(address)": FunctionFragment
    "canOpenMarketOrder((bytes32,address,bool,uint128,uint128,uint128,uint128,uint128),(address,address,uint256,uint256,uint24),uint128)": FunctionFragment
    "closeMarketOrder((bytes32,uint128,uint64),bytes[],(address,address,uint256,uint256,uint24))": FunctionFragment
    "initialize(address,address,address,address)": FunctionFragment
    "offAllowlist()": FunctionFragment
    "onAllowlist()": FunctionFragment
    "openMarketOrder((bytes32,address,bool,uint128,uint128,uint128,uint128,uint128),bytes[],(address,address,uint256,uint256,uint24))": FunctionFragment
    "owner()": FunctionFragment
    "pause()": FunctionFragment
    "paused()": FunctionFragment
    "removeAllowlist(address[])": FunctionFragment
    "removeMargin(bytes32,bytes[],(address,address,uint256,uint256,uint24))": FunctionFragment
    "renounceOwnership()": FunctionFragment
    "setSwapRouter(address)": FunctionFragment
    "setTradingCoreLib(address)": FunctionFragment
    "swapRouter()": FunctionFragment
    "tradingCoreLib()": FunctionFragment
    "transferOwnership(address)": FunctionFragment
    "unpause()": FunctionFragment
  }

  getFunction(
    nameOrSignatureOrTopic:
      | "addAllowlist"
      | "addMargin"
      | "allowlist"
      | "allowlisted"
      | "approveToken"
      | "approvedToken"
      | "canOpenMarketOrder"
      | "closeMarketOrder"
      | "initialize"
      | "offAllowlist"
      | "onAllowlist"
      | "openMarketOrder"
      | "owner"
      | "pause"
      | "paused"
      | "removeAllowlist"
      | "removeMargin"
      | "renounceOwnership"
      | "setSwapRouter"
      | "setTradingCoreLib"
      | "swapRouter"
      | "tradingCoreLib"
      | "transferOwnership"
      | "unpause",
  ): FunctionFragment

  encodeFunctionData(
    functionFragment: "addAllowlist",
    values: [PromiseOrValue<string>[]],
  ): string
  encodeFunctionData(
    functionFragment: "addMargin",
    values: [
      PromiseOrValue<BytesLike>,
      PromiseOrValue<BytesLike>[],
      ISwapRouter.SwapGivenOutInputStruct,
    ],
  ): string
  encodeFunctionData(functionFragment: "allowlist", values?: undefined): string
  encodeFunctionData(
    functionFragment: "allowlisted",
    values: [PromiseOrValue<string>],
  ): string
  encodeFunctionData(
    functionFragment: "approveToken",
    values: [PromiseOrValue<string>, PromiseOrValue<boolean>],
  ): string
  encodeFunctionData(
    functionFragment: "approvedToken",
    values: [PromiseOrValue<string>],
  ): string
  encodeFunctionData(
    functionFragment: "canOpenMarketOrder",
    values: [
      IBook.OpenTradeInputStruct,
      ISwapRouter.SwapGivenOutInputStruct,
      PromiseOrValue<BigNumberish>,
    ],
  ): string
  encodeFunctionData(
    functionFragment: "closeMarketOrder",
    values: [
      IBook.CloseTradeInputStruct,
      PromiseOrValue<BytesLike>[],
      ISwapRouter.SwapGivenInInputStruct,
    ],
  ): string
  encodeFunctionData(
    functionFragment: "initialize",
    values: [
      PromiseOrValue<string>,
      PromiseOrValue<string>,
      PromiseOrValue<string>,
      PromiseOrValue<string>,
    ],
  ): string
  encodeFunctionData(
    functionFragment: "offAllowlist",
    values?: undefined,
  ): string
  encodeFunctionData(
    functionFragment: "onAllowlist",
    values?: undefined,
  ): string
  encodeFunctionData(
    functionFragment: "openMarketOrder",
    values: [
      IBook.OpenTradeInputStruct,
      PromiseOrValue<BytesLike>[],
      ISwapRouter.SwapGivenOutInputStruct,
    ],
  ): string
  encodeFunctionData(functionFragment: "owner", values?: undefined): string
  encodeFunctionData(functionFragment: "pause", values?: undefined): string
  encodeFunctionData(functionFragment: "paused", values?: undefined): string
  encodeFunctionData(
    functionFragment: "removeAllowlist",
    values: [PromiseOrValue<string>[]],
  ): string
  encodeFunctionData(
    functionFragment: "removeMargin",
    values: [
      PromiseOrValue<BytesLike>,
      PromiseOrValue<BytesLike>[],
      ISwapRouter.SwapGivenInInputStruct,
    ],
  ): string
  encodeFunctionData(
    functionFragment: "renounceOwnership",
    values?: undefined,
  ): string
  encodeFunctionData(
    functionFragment: "setSwapRouter",
    values: [PromiseOrValue<string>],
  ): string
  encodeFunctionData(
    functionFragment: "setTradingCoreLib",
    values: [PromiseOrValue<string>],
  ): string
  encodeFunctionData(functionFragment: "swapRouter", values?: undefined): string
  encodeFunctionData(
    functionFragment: "tradingCoreLib",
    values?: undefined,
  ): string
  encodeFunctionData(
    functionFragment: "transferOwnership",
    values: [PromiseOrValue<string>],
  ): string
  encodeFunctionData(functionFragment: "unpause", values?: undefined): string

  decodeFunctionResult(
    functionFragment: "addAllowlist",
    data: BytesLike,
  ): Result
  decodeFunctionResult(functionFragment: "addMargin", data: BytesLike): Result
  decodeFunctionResult(functionFragment: "allowlist", data: BytesLike): Result
  decodeFunctionResult(functionFragment: "allowlisted", data: BytesLike): Result
  decodeFunctionResult(
    functionFragment: "approveToken",
    data: BytesLike,
  ): Result
  decodeFunctionResult(
    functionFragment: "approvedToken",
    data: BytesLike,
  ): Result
  decodeFunctionResult(
    functionFragment: "canOpenMarketOrder",
    data: BytesLike,
  ): Result
  decodeFunctionResult(
    functionFragment: "closeMarketOrder",
    data: BytesLike,
  ): Result
  decodeFunctionResult(functionFragment: "initialize", data: BytesLike): Result
  decodeFunctionResult(
    functionFragment: "offAllowlist",
    data: BytesLike,
  ): Result
  decodeFunctionResult(functionFragment: "onAllowlist", data: BytesLike): Result
  decodeFunctionResult(
    functionFragment: "openMarketOrder",
    data: BytesLike,
  ): Result
  decodeFunctionResult(functionFragment: "owner", data: BytesLike): Result
  decodeFunctionResult(functionFragment: "pause", data: BytesLike): Result
  decodeFunctionResult(functionFragment: "paused", data: BytesLike): Result
  decodeFunctionResult(
    functionFragment: "removeAllowlist",
    data: BytesLike,
  ): Result
  decodeFunctionResult(
    functionFragment: "removeMargin",
    data: BytesLike,
  ): Result
  decodeFunctionResult(
    functionFragment: "renounceOwnership",
    data: BytesLike,
  ): Result
  decodeFunctionResult(
    functionFragment: "setSwapRouter",
    data: BytesLike,
  ): Result
  decodeFunctionResult(
    functionFragment: "setTradingCoreLib",
    data: BytesLike,
  ): Result
  decodeFunctionResult(functionFragment: "swapRouter", data: BytesLike): Result
  decodeFunctionResult(
    functionFragment: "tradingCoreLib",
    data: BytesLike,
  ): Result
  decodeFunctionResult(
    functionFragment: "transferOwnership",
    data: BytesLike,
  ): Result
  decodeFunctionResult(functionFragment: "unpause", data: BytesLike): Result

  events: {
    "AddAllowlistEvent(address[])": EventFragment
    "AllowlistEvent(bool)": EventFragment
    "Initialized(uint8)": EventFragment
    "OwnershipTransferred(address,address)": EventFragment
    "Paused(address)": EventFragment
    "RemoveAllowlistEvent(address[])": EventFragment
    "SetApprovedTokenEvent(address,bool)": EventFragment
    "SetSwapRouterEvent(address)": EventFragment
    "SetTradingCoreLibEvent(address)": EventFragment
    "Unpaused(address)": EventFragment
  }

  getEvent(nameOrSignatureOrTopic: "AddAllowlistEvent"): EventFragment
  getEvent(nameOrSignatureOrTopic: "AllowlistEvent"): EventFragment
  getEvent(nameOrSignatureOrTopic: "Initialized"): EventFragment
  getEvent(nameOrSignatureOrTopic: "OwnershipTransferred"): EventFragment
  getEvent(nameOrSignatureOrTopic: "Paused"): EventFragment
  getEvent(nameOrSignatureOrTopic: "RemoveAllowlistEvent"): EventFragment
  getEvent(nameOrSignatureOrTopic: "SetApprovedTokenEvent"): EventFragment
  getEvent(nameOrSignatureOrTopic: "SetSwapRouterEvent"): EventFragment
  getEvent(nameOrSignatureOrTopic: "SetTradingCoreLibEvent"): EventFragment
  getEvent(nameOrSignatureOrTopic: "Unpaused"): EventFragment
}

export interface AddAllowlistEventEventObject {
  _allowed: string[]
}
export type AddAllowlistEventEvent = TypedEvent<
  [string[]],
  AddAllowlistEventEventObject
>

export type AddAllowlistEventEventFilter =
  TypedEventFilter<AddAllowlistEventEvent>

export interface AllowlistEventEventObject {
  allowlist: boolean
}
export type AllowlistEventEvent = TypedEvent<
  [boolean],
  AllowlistEventEventObject
>

export type AllowlistEventEventFilter = TypedEventFilter<AllowlistEventEvent>

export interface InitializedEventObject {
  version: number
}
export type InitializedEvent = TypedEvent<[number], InitializedEventObject>

export type InitializedEventFilter = TypedEventFilter<InitializedEvent>

export interface OwnershipTransferredEventObject {
  previousOwner: string
  newOwner: string
}
export type OwnershipTransferredEvent = TypedEvent<
  [string, string],
  OwnershipTransferredEventObject
>

export type OwnershipTransferredEventFilter =
  TypedEventFilter<OwnershipTransferredEvent>

export interface PausedEventObject {
  account: string
}
export type PausedEvent = TypedEvent<[string], PausedEventObject>

export type PausedEventFilter = TypedEventFilter<PausedEvent>

export interface RemoveAllowlistEventEventObject {
  _removed: string[]
}
export type RemoveAllowlistEventEvent = TypedEvent<
  [string[]],
  RemoveAllowlistEventEventObject
>

export type RemoveAllowlistEventEventFilter =
  TypedEventFilter<RemoveAllowlistEventEvent>

export interface SetApprovedTokenEventEventObject {
  token: string
  approved: boolean
}
export type SetApprovedTokenEventEvent = TypedEvent<
  [string, boolean],
  SetApprovedTokenEventEventObject
>

export type SetApprovedTokenEventEventFilter =
  TypedEventFilter<SetApprovedTokenEventEvent>

export interface SetSwapRouterEventEventObject {
  swapRouter: string
}
export type SetSwapRouterEventEvent = TypedEvent<
  [string],
  SetSwapRouterEventEventObject
>

export type SetSwapRouterEventEventFilter =
  TypedEventFilter<SetSwapRouterEventEvent>

export interface SetTradingCoreLibEventEventObject {
  tradingCoreLib: string
}
export type SetTradingCoreLibEventEvent = TypedEvent<
  [string],
  SetTradingCoreLibEventEventObject
>

export type SetTradingCoreLibEventEventFilter =
  TypedEventFilter<SetTradingCoreLibEventEvent>

export interface UnpausedEventObject {
  account: string
}
export type UnpausedEvent = TypedEvent<[string], UnpausedEventObject>

export type UnpausedEventFilter = TypedEventFilter<UnpausedEvent>

export interface TradingCoreWithRouter extends BaseContract {
  connect(signerOrProvider: Signer | Provider | string): this
  attach(addressOrName: string): this
  deployed(): Promise<this>

  interface: TradingCoreWithRouterInterface

  queryFilter<TEvent extends TypedEvent>(
    event: TypedEventFilter<TEvent>,
    fromBlockOrBlockhash?: string | number | undefined,
    toBlock?: string | number | undefined,
  ): Promise<Array<TEvent>>

  listeners<TEvent extends TypedEvent>(
    eventFilter?: TypedEventFilter<TEvent>,
  ): Array<TypedListener<TEvent>>
  listeners(eventName?: string): Array<Listener>
  removeAllListeners<TEvent extends TypedEvent>(
    eventFilter: TypedEventFilter<TEvent>,
  ): this
  removeAllListeners(eventName?: string): this
  off: OnEvent<this>
  on: OnEvent<this>
  once: OnEvent<this>
  removeListener: OnEvent<this>

  functions: {
    addAllowlist(
      _allowed: PromiseOrValue<string>[],
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<ContractTransaction>

    addMargin(
      orderHash: PromiseOrValue<BytesLike>,
      priceData: PromiseOrValue<BytesLike>[],
      input: ISwapRouter.SwapGivenOutInputStruct,
      overrides?: PayableOverrides & { from?: PromiseOrValue<string> },
    ): Promise<ContractTransaction>

    allowlist(overrides?: CallOverrides): Promise<[boolean]>

    allowlisted(
      arg0: PromiseOrValue<string>,
      overrides?: CallOverrides,
    ): Promise<[boolean]>

    approveToken(
      token: PromiseOrValue<string>,
      approved: PromiseOrValue<boolean>,
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<ContractTransaction>

    approvedToken(
      arg0: PromiseOrValue<string>,
      overrides?: CallOverrides,
    ): Promise<[boolean]>

    canOpenMarketOrder(
      openData: IBook.OpenTradeInputStruct,
      input: ISwapRouter.SwapGivenOutInputStruct,
      openPrice: PromiseOrValue<BigNumberish>,
      overrides?: CallOverrides,
    ): Promise<
      [IRegistry.TradeStructOutput, IFee.FeeStructOutput] & {
        trade: IRegistry.TradeStructOutput
        _fee: IFee.FeeStructOutput
      }
    >

    closeMarketOrder(
      closeData: IBook.CloseTradeInputStruct,
      priceData: PromiseOrValue<BytesLike>[],
      input: ISwapRouter.SwapGivenInInputStruct,
      overrides?: PayableOverrides & { from?: PromiseOrValue<string> },
    ): Promise<ContractTransaction>

    initialize(
      _owner: PromiseOrValue<string>,
      _tradingCore: PromiseOrValue<string>,
      _registryCore: PromiseOrValue<string>,
      _swapRouter: PromiseOrValue<string>,
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<ContractTransaction>

    offAllowlist(
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<ContractTransaction>

    onAllowlist(
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<ContractTransaction>

    openMarketOrder(
      openData: IBook.OpenTradeInputStruct,
      priceData: PromiseOrValue<BytesLike>[],
      input: ISwapRouter.SwapGivenOutInputStruct,
      overrides?: PayableOverrides & { from?: PromiseOrValue<string> },
    ): Promise<ContractTransaction>

    owner(overrides?: CallOverrides): Promise<[string]>

    pause(
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<ContractTransaction>

    paused(overrides?: CallOverrides): Promise<[boolean]>

    removeAllowlist(
      _removed: PromiseOrValue<string>[],
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<ContractTransaction>

    removeMargin(
      orderHash: PromiseOrValue<BytesLike>,
      priceData: PromiseOrValue<BytesLike>[],
      input: ISwapRouter.SwapGivenInInputStruct,
      overrides?: PayableOverrides & { from?: PromiseOrValue<string> },
    ): Promise<ContractTransaction>

    renounceOwnership(
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<ContractTransaction>

    setSwapRouter(
      _swapRouter: PromiseOrValue<string>,
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<ContractTransaction>

    setTradingCoreLib(
      _tradingCoreLib: PromiseOrValue<string>,
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<ContractTransaction>

    swapRouter(overrides?: CallOverrides): Promise<[string]>

    tradingCoreLib(overrides?: CallOverrides): Promise<[string]>

    transferOwnership(
      newOwner: PromiseOrValue<string>,
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<ContractTransaction>

    unpause(
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<ContractTransaction>
  }

  addAllowlist(
    _allowed: PromiseOrValue<string>[],
    overrides?: Overrides & { from?: PromiseOrValue<string> },
  ): Promise<ContractTransaction>

  addMargin(
    orderHash: PromiseOrValue<BytesLike>,
    priceData: PromiseOrValue<BytesLike>[],
    input: ISwapRouter.SwapGivenOutInputStruct,
    overrides?: PayableOverrides & { from?: PromiseOrValue<string> },
  ): Promise<ContractTransaction>

  allowlist(overrides?: CallOverrides): Promise<boolean>

  allowlisted(
    arg0: PromiseOrValue<string>,
    overrides?: CallOverrides,
  ): Promise<boolean>

  approveToken(
    token: PromiseOrValue<string>,
    approved: PromiseOrValue<boolean>,
    overrides?: Overrides & { from?: PromiseOrValue<string> },
  ): Promise<ContractTransaction>

  approvedToken(
    arg0: PromiseOrValue<string>,
    overrides?: CallOverrides,
  ): Promise<boolean>

  canOpenMarketOrder(
    openData: IBook.OpenTradeInputStruct,
    input: ISwapRouter.SwapGivenOutInputStruct,
    openPrice: PromiseOrValue<BigNumberish>,
    overrides?: CallOverrides,
  ): Promise<
    [IRegistry.TradeStructOutput, IFee.FeeStructOutput] & {
      trade: IRegistry.TradeStructOutput
      _fee: IFee.FeeStructOutput
    }
  >

  closeMarketOrder(
    closeData: IBook.CloseTradeInputStruct,
    priceData: PromiseOrValue<BytesLike>[],
    input: ISwapRouter.SwapGivenInInputStruct,
    overrides?: PayableOverrides & { from?: PromiseOrValue<string> },
  ): Promise<ContractTransaction>

  initialize(
    _owner: PromiseOrValue<string>,
    _tradingCore: PromiseOrValue<string>,
    _registryCore: PromiseOrValue<string>,
    _swapRouter: PromiseOrValue<string>,
    overrides?: Overrides & { from?: PromiseOrValue<string> },
  ): Promise<ContractTransaction>

  offAllowlist(
    overrides?: Overrides & { from?: PromiseOrValue<string> },
  ): Promise<ContractTransaction>

  onAllowlist(
    overrides?: Overrides & { from?: PromiseOrValue<string> },
  ): Promise<ContractTransaction>

  openMarketOrder(
    openData: IBook.OpenTradeInputStruct,
    priceData: PromiseOrValue<BytesLike>[],
    input: ISwapRouter.SwapGivenOutInputStruct,
    overrides?: PayableOverrides & { from?: PromiseOrValue<string> },
  ): Promise<ContractTransaction>

  owner(overrides?: CallOverrides): Promise<string>

  pause(
    overrides?: Overrides & { from?: PromiseOrValue<string> },
  ): Promise<ContractTransaction>

  paused(overrides?: CallOverrides): Promise<boolean>

  removeAllowlist(
    _removed: PromiseOrValue<string>[],
    overrides?: Overrides & { from?: PromiseOrValue<string> },
  ): Promise<ContractTransaction>

  removeMargin(
    orderHash: PromiseOrValue<BytesLike>,
    priceData: PromiseOrValue<BytesLike>[],
    input: ISwapRouter.SwapGivenInInputStruct,
    overrides?: PayableOverrides & { from?: PromiseOrValue<string> },
  ): Promise<ContractTransaction>

  renounceOwnership(
    overrides?: Overrides & { from?: PromiseOrValue<string> },
  ): Promise<ContractTransaction>

  setSwapRouter(
    _swapRouter: PromiseOrValue<string>,
    overrides?: Overrides & { from?: PromiseOrValue<string> },
  ): Promise<ContractTransaction>

  setTradingCoreLib(
    _tradingCoreLib: PromiseOrValue<string>,
    overrides?: Overrides & { from?: PromiseOrValue<string> },
  ): Promise<ContractTransaction>

  swapRouter(overrides?: CallOverrides): Promise<string>

  tradingCoreLib(overrides?: CallOverrides): Promise<string>

  transferOwnership(
    newOwner: PromiseOrValue<string>,
    overrides?: Overrides & { from?: PromiseOrValue<string> },
  ): Promise<ContractTransaction>

  unpause(
    overrides?: Overrides & { from?: PromiseOrValue<string> },
  ): Promise<ContractTransaction>

  callStatic: {
    addAllowlist(
      _allowed: PromiseOrValue<string>[],
      overrides?: CallOverrides,
    ): Promise<void>

    addMargin(
      orderHash: PromiseOrValue<BytesLike>,
      priceData: PromiseOrValue<BytesLike>[],
      input: ISwapRouter.SwapGivenOutInputStruct,
      overrides?: CallOverrides,
    ): Promise<void>

    allowlist(overrides?: CallOverrides): Promise<boolean>

    allowlisted(
      arg0: PromiseOrValue<string>,
      overrides?: CallOverrides,
    ): Promise<boolean>

    approveToken(
      token: PromiseOrValue<string>,
      approved: PromiseOrValue<boolean>,
      overrides?: CallOverrides,
    ): Promise<void>

    approvedToken(
      arg0: PromiseOrValue<string>,
      overrides?: CallOverrides,
    ): Promise<boolean>

    canOpenMarketOrder(
      openData: IBook.OpenTradeInputStruct,
      input: ISwapRouter.SwapGivenOutInputStruct,
      openPrice: PromiseOrValue<BigNumberish>,
      overrides?: CallOverrides,
    ): Promise<
      [IRegistry.TradeStructOutput, IFee.FeeStructOutput] & {
        trade: IRegistry.TradeStructOutput
        _fee: IFee.FeeStructOutput
      }
    >

    closeMarketOrder(
      closeData: IBook.CloseTradeInputStruct,
      priceData: PromiseOrValue<BytesLike>[],
      input: ISwapRouter.SwapGivenInInputStruct,
      overrides?: CallOverrides,
    ): Promise<void>

    initialize(
      _owner: PromiseOrValue<string>,
      _tradingCore: PromiseOrValue<string>,
      _registryCore: PromiseOrValue<string>,
      _swapRouter: PromiseOrValue<string>,
      overrides?: CallOverrides,
    ): Promise<void>

    offAllowlist(overrides?: CallOverrides): Promise<void>

    onAllowlist(overrides?: CallOverrides): Promise<void>

    openMarketOrder(
      openData: IBook.OpenTradeInputStruct,
      priceData: PromiseOrValue<BytesLike>[],
      input: ISwapRouter.SwapGivenOutInputStruct,
      overrides?: CallOverrides,
    ): Promise<void>

    owner(overrides?: CallOverrides): Promise<string>

    pause(overrides?: CallOverrides): Promise<void>

    paused(overrides?: CallOverrides): Promise<boolean>

    removeAllowlist(
      _removed: PromiseOrValue<string>[],
      overrides?: CallOverrides,
    ): Promise<void>

    removeMargin(
      orderHash: PromiseOrValue<BytesLike>,
      priceData: PromiseOrValue<BytesLike>[],
      input: ISwapRouter.SwapGivenInInputStruct,
      overrides?: CallOverrides,
    ): Promise<void>

    renounceOwnership(overrides?: CallOverrides): Promise<void>

    setSwapRouter(
      _swapRouter: PromiseOrValue<string>,
      overrides?: CallOverrides,
    ): Promise<void>

    setTradingCoreLib(
      _tradingCoreLib: PromiseOrValue<string>,
      overrides?: CallOverrides,
    ): Promise<void>

    swapRouter(overrides?: CallOverrides): Promise<string>

    tradingCoreLib(overrides?: CallOverrides): Promise<string>

    transferOwnership(
      newOwner: PromiseOrValue<string>,
      overrides?: CallOverrides,
    ): Promise<void>

    unpause(overrides?: CallOverrides): Promise<void>
  }

  filters: {
    "AddAllowlistEvent(address[])"(
      _allowed?: null,
    ): AddAllowlistEventEventFilter
    AddAllowlistEvent(_allowed?: null): AddAllowlistEventEventFilter

    "AllowlistEvent(bool)"(allowlist?: null): AllowlistEventEventFilter
    AllowlistEvent(allowlist?: null): AllowlistEventEventFilter

    "Initialized(uint8)"(version?: null): InitializedEventFilter
    Initialized(version?: null): InitializedEventFilter

    "OwnershipTransferred(address,address)"(
      previousOwner?: PromiseOrValue<string> | null,
      newOwner?: PromiseOrValue<string> | null,
    ): OwnershipTransferredEventFilter
    OwnershipTransferred(
      previousOwner?: PromiseOrValue<string> | null,
      newOwner?: PromiseOrValue<string> | null,
    ): OwnershipTransferredEventFilter

    "Paused(address)"(account?: null): PausedEventFilter
    Paused(account?: null): PausedEventFilter

    "RemoveAllowlistEvent(address[])"(
      _removed?: null,
    ): RemoveAllowlistEventEventFilter
    RemoveAllowlistEvent(_removed?: null): RemoveAllowlistEventEventFilter

    "SetApprovedTokenEvent(address,bool)"(
      token?: null,
      approved?: null,
    ): SetApprovedTokenEventEventFilter
    SetApprovedTokenEvent(
      token?: null,
      approved?: null,
    ): SetApprovedTokenEventEventFilter

    "SetSwapRouterEvent(address)"(
      swapRouter?: null,
    ): SetSwapRouterEventEventFilter
    SetSwapRouterEvent(swapRouter?: null): SetSwapRouterEventEventFilter

    "SetTradingCoreLibEvent(address)"(
      tradingCoreLib?: null,
    ): SetTradingCoreLibEventEventFilter
    SetTradingCoreLibEvent(
      tradingCoreLib?: null,
    ): SetTradingCoreLibEventEventFilter

    "Unpaused(address)"(account?: null): UnpausedEventFilter
    Unpaused(account?: null): UnpausedEventFilter
  }

  estimateGas: {
    addAllowlist(
      _allowed: PromiseOrValue<string>[],
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<BigNumber>

    addMargin(
      orderHash: PromiseOrValue<BytesLike>,
      priceData: PromiseOrValue<BytesLike>[],
      input: ISwapRouter.SwapGivenOutInputStruct,
      overrides?: PayableOverrides & { from?: PromiseOrValue<string> },
    ): Promise<BigNumber>

    allowlist(overrides?: CallOverrides): Promise<BigNumber>

    allowlisted(
      arg0: PromiseOrValue<string>,
      overrides?: CallOverrides,
    ): Promise<BigNumber>

    approveToken(
      token: PromiseOrValue<string>,
      approved: PromiseOrValue<boolean>,
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<BigNumber>

    approvedToken(
      arg0: PromiseOrValue<string>,
      overrides?: CallOverrides,
    ): Promise<BigNumber>

    canOpenMarketOrder(
      openData: IBook.OpenTradeInputStruct,
      input: ISwapRouter.SwapGivenOutInputStruct,
      openPrice: PromiseOrValue<BigNumberish>,
      overrides?: CallOverrides,
    ): Promise<BigNumber>

    closeMarketOrder(
      closeData: IBook.CloseTradeInputStruct,
      priceData: PromiseOrValue<BytesLike>[],
      input: ISwapRouter.SwapGivenInInputStruct,
      overrides?: PayableOverrides & { from?: PromiseOrValue<string> },
    ): Promise<BigNumber>

    initialize(
      _owner: PromiseOrValue<string>,
      _tradingCore: PromiseOrValue<string>,
      _registryCore: PromiseOrValue<string>,
      _swapRouter: PromiseOrValue<string>,
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<BigNumber>

    offAllowlist(
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<BigNumber>

    onAllowlist(
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<BigNumber>

    openMarketOrder(
      openData: IBook.OpenTradeInputStruct,
      priceData: PromiseOrValue<BytesLike>[],
      input: ISwapRouter.SwapGivenOutInputStruct,
      overrides?: PayableOverrides & { from?: PromiseOrValue<string> },
    ): Promise<BigNumber>

    owner(overrides?: CallOverrides): Promise<BigNumber>

    pause(
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<BigNumber>

    paused(overrides?: CallOverrides): Promise<BigNumber>

    removeAllowlist(
      _removed: PromiseOrValue<string>[],
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<BigNumber>

    removeMargin(
      orderHash: PromiseOrValue<BytesLike>,
      priceData: PromiseOrValue<BytesLike>[],
      input: ISwapRouter.SwapGivenInInputStruct,
      overrides?: PayableOverrides & { from?: PromiseOrValue<string> },
    ): Promise<BigNumber>

    renounceOwnership(
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<BigNumber>

    setSwapRouter(
      _swapRouter: PromiseOrValue<string>,
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<BigNumber>

    setTradingCoreLib(
      _tradingCoreLib: PromiseOrValue<string>,
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<BigNumber>

    swapRouter(overrides?: CallOverrides): Promise<BigNumber>

    tradingCoreLib(overrides?: CallOverrides): Promise<BigNumber>

    transferOwnership(
      newOwner: PromiseOrValue<string>,
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<BigNumber>

    unpause(
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<BigNumber>
  }

  populateTransaction: {
    addAllowlist(
      _allowed: PromiseOrValue<string>[],
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<PopulatedTransaction>

    addMargin(
      orderHash: PromiseOrValue<BytesLike>,
      priceData: PromiseOrValue<BytesLike>[],
      input: ISwapRouter.SwapGivenOutInputStruct,
      overrides?: PayableOverrides & { from?: PromiseOrValue<string> },
    ): Promise<PopulatedTransaction>

    allowlist(overrides?: CallOverrides): Promise<PopulatedTransaction>

    allowlisted(
      arg0: PromiseOrValue<string>,
      overrides?: CallOverrides,
    ): Promise<PopulatedTransaction>

    approveToken(
      token: PromiseOrValue<string>,
      approved: PromiseOrValue<boolean>,
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<PopulatedTransaction>

    approvedToken(
      arg0: PromiseOrValue<string>,
      overrides?: CallOverrides,
    ): Promise<PopulatedTransaction>

    canOpenMarketOrder(
      openData: IBook.OpenTradeInputStruct,
      input: ISwapRouter.SwapGivenOutInputStruct,
      openPrice: PromiseOrValue<BigNumberish>,
      overrides?: CallOverrides,
    ): Promise<PopulatedTransaction>

    closeMarketOrder(
      closeData: IBook.CloseTradeInputStruct,
      priceData: PromiseOrValue<BytesLike>[],
      input: ISwapRouter.SwapGivenInInputStruct,
      overrides?: PayableOverrides & { from?: PromiseOrValue<string> },
    ): Promise<PopulatedTransaction>

    initialize(
      _owner: PromiseOrValue<string>,
      _tradingCore: PromiseOrValue<string>,
      _registryCore: PromiseOrValue<string>,
      _swapRouter: PromiseOrValue<string>,
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<PopulatedTransaction>

    offAllowlist(
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<PopulatedTransaction>

    onAllowlist(
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<PopulatedTransaction>

    openMarketOrder(
      openData: IBook.OpenTradeInputStruct,
      priceData: PromiseOrValue<BytesLike>[],
      input: ISwapRouter.SwapGivenOutInputStruct,
      overrides?: PayableOverrides & { from?: PromiseOrValue<string> },
    ): Promise<PopulatedTransaction>

    owner(overrides?: CallOverrides): Promise<PopulatedTransaction>

    pause(
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<PopulatedTransaction>

    paused(overrides?: CallOverrides): Promise<PopulatedTransaction>

    removeAllowlist(
      _removed: PromiseOrValue<string>[],
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<PopulatedTransaction>

    removeMargin(
      orderHash: PromiseOrValue<BytesLike>,
      priceData: PromiseOrValue<BytesLike>[],
      input: ISwapRouter.SwapGivenInInputStruct,
      overrides?: PayableOverrides & { from?: PromiseOrValue<string> },
    ): Promise<PopulatedTransaction>

    renounceOwnership(
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<PopulatedTransaction>

    setSwapRouter(
      _swapRouter: PromiseOrValue<string>,
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<PopulatedTransaction>

    setTradingCoreLib(
      _tradingCoreLib: PromiseOrValue<string>,
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<PopulatedTransaction>

    swapRouter(overrides?: CallOverrides): Promise<PopulatedTransaction>

    tradingCoreLib(overrides?: CallOverrides): Promise<PopulatedTransaction>

    transferOwnership(
      newOwner: PromiseOrValue<string>,
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<PopulatedTransaction>

    unpause(
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<PopulatedTransaction>
  }
}
