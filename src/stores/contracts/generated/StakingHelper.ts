/* Autogenerated file. Do not edit manually. */
/* tslint:disable */
/* eslint-disable */
import type { FunctionFragment, Result } from "@ethersproject/abi"
import type { Listener, Provider } from "@ethersproject/providers"
import type {
  BaseContract,
  BigNumber,
  BytesLike,
  CallOverrides,
  ContractTransaction,
  Overrides,
  PopulatedTransaction,
  Signer,
  utils,
} from "ethers"
import type {
  OnEvent,
  PromiseOrValue,
  TypedEvent,
  TypedEventFilter,
  TypedListener,
} from "./common"

export interface StakingHelperInterface extends utils.Interface {
  functions: {
    "claimAndStakeMany(address[],address[])": FunctionFragment
    "claimMany(address[])": FunctionFragment
  }

  getFunction(
    nameOrSignatureOrTopic: "claimAndStakeMany" | "claimMany",
  ): FunctionFragment

  encodeFunctionData(
    functionFragment: "claimAndStakeMany",
    values: [PromiseOrValue<string>[], PromiseOrValue<string>[]],
  ): string
  encodeFunctionData(
    functionFragment: "claimMany",
    values: [PromiseOrValue<string>[]],
  ): string

  decodeFunctionResult(
    functionFragment: "claimAndStakeMany",
    data: BytesLike,
  ): Result
  decodeFunctionResult(functionFragment: "claimMany", data: BytesLike): Result

  events: {}
}

export interface StakingHelper extends BaseContract {
  connect(signerOrProvider: Signer | Provider | string): this
  attach(addressOrName: string): this
  deployed(): Promise<this>

  interface: StakingHelperInterface

  queryFilter<TEvent extends TypedEvent>(
    event: TypedEventFilter<TEvent>,
    fromBlockOrBlockhash?: string | number | undefined,
    toBlock?: string | number | undefined,
  ): Promise<Array<TEvent>>

  listeners<TEvent extends TypedEvent>(
    eventFilter?: TypedEventFilter<TEvent>,
  ): Array<TypedListener<TEvent>>
  listeners(eventName?: string): Array<Listener>
  removeAllListeners<TEvent extends TypedEvent>(
    eventFilter: TypedEventFilter<TEvent>,
  ): this
  removeAllListeners(eventName?: string): this
  off: OnEvent<this>
  on: OnEvent<this>
  once: OnEvent<this>
  removeListener: OnEvent<this>

  functions: {
    claimAndStakeMany(
      claimables: PromiseOrValue<string>[],
      stakeables: PromiseOrValue<string>[],
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<ContractTransaction>

    claimMany(
      claimables: PromiseOrValue<string>[],
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<ContractTransaction>
  }

  claimAndStakeMany(
    claimables: PromiseOrValue<string>[],
    stakeables: PromiseOrValue<string>[],
    overrides?: Overrides & { from?: PromiseOrValue<string> },
  ): Promise<ContractTransaction>

  claimMany(
    claimables: PromiseOrValue<string>[],
    overrides?: Overrides & { from?: PromiseOrValue<string> },
  ): Promise<ContractTransaction>

  callStatic: {
    claimAndStakeMany(
      claimables: PromiseOrValue<string>[],
      stakeables: PromiseOrValue<string>[],
      overrides?: CallOverrides,
    ): Promise<void>

    claimMany(
      claimables: PromiseOrValue<string>[],
      overrides?: CallOverrides,
    ): Promise<void>
  }

  filters: {}

  estimateGas: {
    claimAndStakeMany(
      claimables: PromiseOrValue<string>[],
      stakeables: PromiseOrValue<string>[],
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<BigNumber>

    claimMany(
      claimables: PromiseOrValue<string>[],
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<BigNumber>
  }

  populateTransaction: {
    claimAndStakeMany(
      claimables: PromiseOrValue<string>[],
      stakeables: PromiseOrValue<string>[],
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<PopulatedTransaction>

    claimMany(
      claimables: PromiseOrValue<string>[],
      overrides?: Overrides & { from?: PromiseOrValue<string> },
    ): Promise<PopulatedTransaction>
  }
}
