import { Provider } from "@ethersproject/providers"
import { Signer } from "ethers"
import { computed, makeObservable } from "mobx"
import { createTransformer } from "mobx-utils"
import { AppEnvStore } from "../AppEnvStore/AppEnvStore"
import { CurrencyStore } from "../CurrencyStore/CurrencyStore"
import { ERC20Tokens } from "../CurrencyStore/CurrencyStore.service"
import {
  AirdropHelper,
  AirdropHelper__factory,
  EsUniwhaleToken,
  EsUniwhaleToken__factory,
  Fees,
  Fees__factory,
  LimitBook,
  LimitBook__factory,
  LiquidityPool,
  LiquidityPool__factory,
  MultiCall,
  MultiCall__factory,
  OracleAggregator,
  OracleAggregator__factory,
  Referrals,
  Referrals__factory,
  RegistryCore,
  RegistryCore__factory,
  RegistryReader,
  RegistryReader__factory,
  RevenuePool,
  RevenuePool__factory,
  StakingHelper,
  StakingHelper__factory,
  SwapRouter,
  SwapRouter__factory,
  TestToken,
  TestToken__factory,
  TraderFarm,
  TraderFarm__factory,
  TradingCore,
  TradingCoreLib,
  TradingCoreLib__factory,
  TradingCoreWithRouter,
  TradingCoreWithRouter__factory,
  TradingCore__factory,
  UniwhalePass,
  UniwhalePass__factory,
  UniwhaleToken,
  UniwhaleToken__factory,
} from "./generated"

export class ContractStore {
  constructor(
    private appEnvStore: AppEnvStore,
    private currencyStore: CurrencyStore,
    private getSignerOrProvider$: () => Signer | Provider,
  ) {
    makeObservable(this)
  }

  @computed get registryCore$(): RegistryCore {
    return RegistryCore__factory.connect(
      this.appEnvStore.onChainConfigs$.registryCore,
      this.getSignerOrProvider$(),
    )
  }

  @computed get liquidityPool$(): LiquidityPool {
    return LiquidityPool__factory.connect(
      this.appEnvStore.onChainConfigs$.liquidityPool,
      this.getSignerOrProvider$(),
    )
  }

  @computed get swapRouter$(): SwapRouter {
    return SwapRouter__factory.connect(
      this.appEnvStore.onChainConfigs$.swapRouter,
      this.getSignerOrProvider$(),
    )
  }

  @computed get genesisPass$(): UniwhalePass {
    return UniwhalePass__factory.connect(
      this.appEnvStore.onChainConfigs$.genesisPass,
      this.getSignerOrProvider$(),
    )
  }

  @computed get tradingCore$(): TradingCore {
    return TradingCore__factory.connect(
      this.appEnvStore.onChainConfigs$.tradingCore,
      this.getSignerOrProvider$(),
    )
  }

  @computed get tradingCoreWithRouter$(): TradingCoreWithRouter {
    return TradingCoreWithRouter__factory.connect(
      this.appEnvStore.onChainConfigs$.tradingCoreWithRouter,
      this.getSignerOrProvider$(),
    )
  }

  @computed get tradingCoreLib$(): TradingCoreLib {
    return TradingCoreLib__factory.connect(
      this.appEnvStore.onChainConfigs$.tradingCoreLib,
      this.getSignerOrProvider$(),
    )
  }

  @computed get registryReader$(): RegistryReader {
    return RegistryReader__factory.connect(
      this.appEnvStore.onChainConfigs$.registryReader,
      this.getSignerOrProvider$(),
    )
  }

  @computed get limitBook$(): LimitBook {
    return LimitBook__factory.connect(
      this.appEnvStore.onChainConfigs$.limitBook,
      this.getSignerOrProvider$(),
    )
  }

  @computed get oracleAggregator$(): OracleAggregator {
    return OracleAggregator__factory.connect(
      this.appEnvStore.onChainConfigs$.oracleAggregator,
      this.getSignerOrProvider$(),
    )
  }

  @computed get referrals$(): Referrals {
    return Referrals__factory.connect(
      this.appEnvStore.onChainConfigs$.referrals,
      this.getSignerOrProvider$(),
    )
  }

  @computed get fees$(): Fees {
    return Fees__factory.connect(
      this.appEnvStore.onChainConfigs$.fees,
      this.getSignerOrProvider$(),
    )
  }

  @computed get uniwhaleToken$(): UniwhaleToken {
    return UniwhaleToken__factory.connect(
      this.appEnvStore.onChainConfigs$.uniwhaleToken,
      this.getSignerOrProvider$(),
    )
  }

  @computed get esUniwhaleToken$(): EsUniwhaleToken {
    return EsUniwhaleToken__factory.connect(
      this.appEnvStore.onChainConfigs$.esUniwhaleToken,
      this.getSignerOrProvider$(),
    )
  }

  @computed get stakingHelper$(): StakingHelper {
    return StakingHelper__factory.connect(
      this.appEnvStore.onChainConfigs$.stakingHelper,
      this.getSignerOrProvider$(),
    )
  }

  @computed get traderFarm$(): TraderFarm {
    return TraderFarm__factory.connect(
      this.appEnvStore.onChainConfigs$.traderFarm,
      this.getSignerOrProvider$(),
    )
  }

  @computed get revenuePool$(): RevenuePool {
    return RevenuePool__factory.connect(
      this.appEnvStore.onChainConfigs$.revenuePool,
      this.getSignerOrProvider$(),
    )
  }

  @computed get airdropHelper$(): AirdropHelper {
    return AirdropHelper__factory.connect(
      this.appEnvStore.onChainConfigs$.airdropHelper,
      this.getSignerOrProvider$(),
    )
  }

  @computed get multiCall$(): MultiCall {
    return MultiCall__factory.connect(
      this.appEnvStore.onChainConfigs$.multiCall,
      this.getSignerOrProvider$(),
    )
  }

  @computed get testnetPOAP$(): UniwhalePass {
    return UniwhalePass__factory.connect(
      this.appEnvStore.onChainConfigs$.testnetPOAP,
      this.getSignerOrProvider$(),
    )
  }
  // @computed get testnetPOAP$(): IERC721 {
  //   return IERC721__factory.connect(
  //     this.appEnvStore.onChainConfigs$.testnetPOAP,
  //     this.getSignerOrProvider$(),
  //   )
  // }

  mockToken$ = createTransformer(
    (token: ERC20Tokens): TestToken =>
      TestToken__factory.connect(
        this.currencyStore.contractAddress$(token),
        this.getSignerOrProvider$(),
      ),
    {
      keepAlive: true,
    },
  )
}
