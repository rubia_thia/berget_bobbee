import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import {
  math,
  mathIs,
} from "../../utils/numberHelpers/bigNumberExpressionParser"

export function ulpPrice(
  lpTokenSupply: BigNumber,
  currentLiquidity: BigNumber,
): BigNumber {
  if (mathIs`${currentLiquidity} == ${0}`) {
    return BigNumber.from(1)
  }
  return math`${currentLiquidity} / ${lpTokenSupply}`
}
