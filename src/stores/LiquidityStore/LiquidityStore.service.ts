import { Client, gql } from "@urql/core"
import { ContractTransaction } from "ethers"
import { Observable } from "rxjs"
import {
  LatestTradeStatsQuery,
  LatestTradeStatsQueryVariables,
} from "../../generated/graphql/graphql.generated"
import { LiquidityMutationDirection } from "../../screens/LiquidityScreen/types"
import {
  fromContractToNative,
  fromNativeToContract,
} from "../../utils/contractHelpers/bigNumberContracts"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import { math } from "../../utils/numberHelpers/bigNumberExpressionParser"
import { ERC20, LiquidityPool } from "../contracts/generated"
import { fromUrqlSource } from "../utils/hasuraClient"

export const getCurrentLiquidity = (
  anchorTokenContract: ERC20,
  liquidityPoolContract: LiquidityPool,
): Promise<BigNumber> => {
  return anchorTokenContract
    .balanceOf(liquidityPoolContract.address)
    .then(fromContractToNative)
}

export type ChangeLiquidityForm = {
  tokenAddress?: string
  direction: LiquidityMutationDirection
  amount: BigNumber
  currentULPBalance: BigNumber
  liquidityPool: LiquidityPool
  poolFee: BigNumber
  slippage: BigNumber
}

export async function changeLiquidity(
  form: ChangeLiquidityForm,
): Promise<ContractTransaction> {
  if (form.direction === LiquidityMutationDirection.Deposit) {
    if (form.tokenAddress == null) {
      return await form.liquidityPool["mintAndStake(uint256)"](
        fromNativeToContract(form.amount),
        {
          gasLimit: 3e6,
        },
      )
    }
    return await form.liquidityPool[
      "mintAndStake(uint256,uint256,address,uint24)"
    ](
      fromNativeToContract(form.amount),
      fromNativeToContract(math`${form.amount} * (${1} - ${form.slippage})`),
      form.tokenAddress,
      fromNativeToContract(form.poolFee, { decimals: 6 }),
      {
        gasLimit: 3e6,
      },
    )
  } else {
    if (form.tokenAddress == null) {
      return await form.liquidityPool["unstakeAndBurn(uint256)"](
        fromNativeToContract(form.amount),
        {
          gasLimit: 3e6,
        },
      )
    }
    return await form.liquidityPool[
      "unstakeAndBurn(uint256,uint256,address,uint24)"
    ](
      fromNativeToContract(form.amount),
      fromNativeToContract(math`${form.amount} * (${1} - ${form.slippage})`),
      form.tokenAddress,
      fromNativeToContract(form.poolFee, { decimals: 6 }),
      {
        gasLimit: 3e6,
      },
    )
  }
}

export type TradeStats = {
  ulpAPR?: BigNumber
  ulpEmissionAPR?: BigNumber
  unwAPR?: BigNumber
  esUnwAPR?: BigNumber
  unwEmissionAPR?: BigNumber
  esUnwEmissionAPR?: BigNumber
  feeIn7Days?: BigNumber
  accumulatedFee?: BigNumber
  updatedAt?: Date
}

export function getTradeStats(client: Client): Observable<TradeStats> {
  return fromUrqlSource(
    client.query<LatestTradeStatsQuery, LatestTradeStatsQueryVariables>(
      gql`
        query LatestTradeStats {
          latest_trade_stats_7d {
            timestamp
            apr_7d
            fee_7d
            accumulated_fee
            unw_apr_7d
            unw_emission_apr_7d
            es_unw_apr_7d
            es_unw_emission_apr_7d
            ulp_emission_apr_7d
          }
        }
      `,
      {},
    ),
    result => {
      const stat = result.data.latest_trade_stats_7d[0]
      return {
        ulpAPR:
          stat?.apr_7d != null
            ? math`${BigNumber.from(stat.apr_7d)}`
            : undefined,
        ulpEmissionAPR:
          stat?.ulp_emission_apr_7d != null
            ? math`${BigNumber.from(stat.ulp_emission_apr_7d)} x ${0.7}`
            : undefined,
        feeIn7Days:
          stat?.fee_7d != null ? BigNumber.from(stat.fee_7d) : undefined,
        accumulatedFee: stat?.accumulated_fee
          ? BigNumber.from(stat.accumulated_fee)
          : undefined,
        updatedAt: stat?.timestamp
          ? new Date(stat.timestamp * 1000)
          : undefined,
        unwAPR:
          stat?.unw_apr_7d != null
            ? BigNumber.from(stat.unw_apr_7d)
            : undefined,
        esUnwAPR:
          stat?.es_unw_apr_7d != null
            ? BigNumber.from(stat.es_unw_apr_7d)
            : undefined,
        unwEmissionAPR:
          stat?.unw_emission_apr_7d != null
            ? BigNumber.from(stat.unw_emission_apr_7d)
            : undefined,
        esUnwEmissionAPR:
          stat?.es_unw_emission_apr_7d != null
            ? BigNumber.from(stat.es_unw_emission_apr_7d)
            : undefined,
      }
    },
  )
}
