import { ContractTransaction } from "ethers"
import { action, computed, makeObservable, observable } from "mobx"
import { from } from "rxjs"
import {
  ActionPanelFormError,
  ActionPanelFormErrorType,
} from "../../screens/LiquidityScreen/components/ActionPanel/ActionPanel.types"
import { LiquidityMutationDirection } from "../../screens/LiquidityScreen/types"
import { asyncAction, runAsyncAction } from "../../utils/asyncAction"
import { fromContractToNative } from "../../utils/contractHelpers/bigNumberContracts"
import { BigNumber } from "../../utils/numberHelpers/BigNumber"
import {
  math,
  mathIs,
} from "../../utils/numberHelpers/bigNumberExpressionParser"
import { props } from "../../utils/promiseHelpers"
import { TokenInfo } from "../../utils/TokenInfo"
import { AppEnvStore } from "../AppEnvStore/AppEnvStore"
import { AuthStore } from "../AuthStore/AuthStore"
import { ContractStore } from "../contracts/ContractStore"
import { CurrencyStore } from "../CurrencyStore/CurrencyStore"
import {
  ERC20Tokens,
  StableToken,
} from "../CurrencyStore/CurrencyStore.service"
import { MAX_AMOUNT_TO_APPROVE } from "../TradeStore/OrderCreationModule.service"
import { TradeInfoStore } from "../TradeStore/TradeInfoStore"
import { LazyValue } from "../utils/LazyValue"
import { Result } from "../utils/Result"
import { SuspenseObservable } from "../utils/SuspenseObservable"
import { waitUntilExist$ } from "../utils/waitFor"
import { ulpPrice } from "./LiquidityStore.math"
import {
  changeLiquidity,
  ChangeLiquidityForm,
  TradeStats,
} from "./LiquidityStore.service"

export class LiquidityStore {
  constructor(
    readonly appEnvStore: AppEnvStore,
    readonly authStore: AuthStore,
    readonly currencyStore: CurrencyStore,
    readonly contracts: ContractStore,
    readonly tradeInfo: TradeInfoStore,
  ) {
    makeObservable(this)
  }

  @observable selectedToken: StableToken = ERC20Tokens.AnchorToken

  @computed get selectedTokenInfo$(): TokenInfo {
    return this.currencyStore.tokenInfo$(this.selectedToken)
  }

  @computed get anchorTokenInfo$(): TokenInfo {
    return this.currencyStore.tokenInfo$(ERC20Tokens.AnchorToken)
  }

  @computed get selectedTokenBalance$(): BigNumber {
    return this.currencyStore.balance$(this.selectedToken)
  }

  @computed get allTokenInfo$(): TokenInfo[] {
    return this.tradeInfo.allSupportedStableToken$.map(
      this.currencyStore.tokenInfo$,
    )
  }

  @observable showSwapSlippageEditingModal = false
  recommendedSwapSlippage = BigNumber.from(0.005)
  swapSlippage = new SuspenseObservable(this.recommendedSwapSlippage)

  @action selected(token: TokenInfo): void {
    this.selectedToken = token.id as StableToken
  }

  @computed get liquidityTokenInfo$(): TokenInfo {
    return this.currencyStore.tokenInfo$(ERC20Tokens.LiquidityPool)
  }

  #liquidityBalance = new LazyValue(
    () => ({
      acc: this.authStore.account$,
      lp: this.contracts.liquidityPool$,
      refresh: this.currencyStore.refreshSignal$,
    }),
    ({ lp, acc }) => from(lp.getStaked(acc).then(fromContractToNative)),
  )

  @computed get liquidityBalance$(): BigNumber {
    return this.#liquidityBalance.value$
  }

  amountToDeposit = new SuspenseObservable<BigNumber>()
  amountToWithdraw = new SuspenseObservable<BigNumber>()

  @observable direction = LiquidityMutationDirection.Deposit

  @computed get formData$(): Result<ChangeLiquidityForm, ActionPanelFormError> {
    if (!this.authStore.connected) {
      return Result.error({
        type: ActionPanelFormErrorType.ConnectWallet,
      })
    }
    if (this.direction === LiquidityMutationDirection.Deposit) {
      if (
        this.amountToDeposit.get() == null ||
        mathIs`${this.amountToDeposit.read$} == ${0}`
      ) {
        return Result.error({
          type: ActionPanelFormErrorType.EnterAmount,
        })
      }
    } else {
      if (
        this.amountToWithdraw.get() == null ||
        mathIs`${this.amountToWithdraw.read$} == ${0}`
      ) {
        return Result.error({
          type: ActionPanelFormErrorType.EnterAmount,
        })
      }
    }
    if (this.direction === LiquidityMutationDirection.Deposit) {
      if (
        mathIs`${this.amountToDeposit.read$} > ${this.selectedTokenBalance$}`
      ) {
        return Result.error({
          type: ActionPanelFormErrorType.InsufficientBalance,
        })
      }
    } else {
      if (mathIs`${this.amountToWithdraw.read$} > ${this.liquidityBalance$}`) {
        return Result.error({
          type: ActionPanelFormErrorType.InsufficientBalance,
        })
      }
    }
    return Result.ok({
      tokenAddress: this.isUsingAnchorToken$
        ? undefined
        : this.currencyStore.contractAddress$(this.selectedToken),
      direction: this.direction,
      amount:
        this.direction === LiquidityMutationDirection.Deposit
          ? this.amountToDeposit.read$
          : this.amountToWithdraw.read$,
      liquidityPool: this.contracts.liquidityPool$,
      currentULPBalance: this.liquidityBalance$,
      poolFee: this.tradeInfo.swapPoolFee,
      slippage: this.swapSlippage.read$,
    })
  }

  @computed get isUsingAnchorToken$(): boolean {
    return this.selectedToken === ERC20Tokens.AnchorToken
  }

  @computed get quoteTokenAllowance$(): BigNumber {
    return this.currencyStore.allowanceOf$(
      this.selectedToken,
      this.contracts.liquidityPool$.address,
    )
  }

  @computed get needApprove$(): boolean {
    if (this.direction !== LiquidityMutationDirection.Deposit) {
      return false
    }
    return mathIs`${this.quoteTokenAllowance$} < ${this.amountToDeposit.read$}`
  }

  async approve(): Promise<ContractTransaction> {
    return await this.currencyStore
      .getErc20$(this.selectedToken)
      .approve(this.contracts.liquidityPool$.address, MAX_AMOUNT_TO_APPROVE)
  }

  @asyncAction async submit(
    form: ChangeLiquidityForm,
    run = runAsyncAction,
  ): Promise<ContractTransaction> {
    return await run(changeLiquidity(form))
  }

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  @computed get currentInfo$() {
    return this.tradeInfo.liquidityInfo.value$
  }

  @computed get anchorLiquidity$(): BigNumber {
    return this.currentInfo$.baseBalance
  }

  @computed get ulpPrice$(): BigNumber {
    return ulpPrice(this.currentInfo$.totalSupply, this.anchorLiquidity$)
  }

  @computed get exchangeRate$(): BigNumber {
    return this.tradeInfo.exchangeRate$(
      this.selectedToken,
      ERC20Tokens.AnchorToken,
    )
  }

  @computed get ulpMintAmount$(): BigNumber {
    return math`(${this.amountToDeposit.read$} * ${this.exchangeRate$} - ${this.fee$}) / ${this.ulpPrice$}`
  }

  @computed get anchorWithdrawAmount$(): BigNumber {
    const exchangeRate = this.tradeInfo.exchangeRate$(
      ERC20Tokens.AnchorToken,
      this.selectedToken,
    )
    return math`(${this.amountToWithdraw.read$} * ${this.ulpPrice$} - ${this.fee$}) * ${exchangeRate}`
  }

  @computed get feeRate$(): BigNumber {
    if (this.direction === LiquidityMutationDirection.Deposit) {
      return this.currentInfo$.mintFee
    }
    return this.currentInfo$.burnFee
  }

  @computed get fee$(): BigNumber {
    if (this.direction === LiquidityMutationDirection.Deposit) {
      return math`${this.amountToDeposit.read$} * ${this.feeRate$}`
    }
    return math`${this.amountToWithdraw.read$} * ${this.ulpPrice$} * ${this.feeRate$}`
  }

  @computed get stats$(): TradeStats {
    return this.tradeInfo.tradeStats$
  }

  @computed get currentWithdrawLimit$(): BigNumber {
    const { minCollateral } = this.currentInfo$
    return math`${this.anchorLiquidity$} - ${minCollateral}`
  }

  #feeShareForLP = new LazyValue(
    () => ({
      registry: this.contracts.registryCore$,
      revenue: this.contracts.revenuePool$,
      ulp: this.contracts.liquidityPool$,
    }),
    ({ ulp, revenue, registry }) =>
      from(
        props({
          feeFactor: registry.feeFactor().then(fromContractToNative),
          lpFeeShare: revenue.getShare(ulp.address).then(fromContractToNative),
        }).then(
          ({ lpFeeShare, feeFactor }) => math`${lpFeeShare} x ${feeFactor}`,
        ),
      ),
  )

  @computed get feeAPR$(): BigNumber {
    const apr = waitUntilExist$(() => this.stats$.ulpAPR)
    return math`${apr} * ${this.#feeShareForLP.value$} / ${this.ulpPrice$}`
  }

  @computed get emissionAPR$(): BigNumber {
    return waitUntilExist$(() => this.stats$.ulpEmissionAPR)
  }

  @computed get apr$(): BigNumber {
    return math`${this.emissionAPR$} + ${this.feeAPR$}`
  }

  @computed get accumulatedFeeIn7Days$(): BigNumber {
    return waitUntilExist$(() => this.stats$.feeIn7Days)
  }

  @computed get distributedFeeInTotal$(): BigNumber {
    const totalFee = waitUntilExist$(() => this.stats$.accumulatedFee)
    const feeFactor = this.feeDistributedRates$.totalRebateFeeFactor
    return math`${totalFee} * ${feeFactor}`
  }

  @computed get feeDistributedRates$(): {
    ulpHolder: BigNumber
    esUNWHolder: BigNumber
    totalRebateFeeFactor: BigNumber
    reserved: BigNumber
  } {
    const totalRebateFeeFactor = BigNumber.sum([
      this.tradeInfo.liquidityInfo.value$.ulpFeeRebateFactor,
      this.tradeInfo.liquidityInfo.value$.esUnwFeeRebateFactor,
    ])

    return {
      ulpHolder: this.tradeInfo.liquidityInfo.value$.ulpFeeRebateFactor,
      esUNWHolder: this.tradeInfo.liquidityInfo.value$.esUnwFeeRebateFactor,
      totalRebateFeeFactor,
      reserved: math`${1} - ${totalRebateFeeFactor}`,
    }
  }

  @observable showConfirmation = false

  @computed get totalSuppliedLpTokenAmount$(): BigNumber {
    return this.currentInfo$.totalSupply
  }
}
