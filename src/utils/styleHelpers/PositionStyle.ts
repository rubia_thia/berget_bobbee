import { StyleProp, StyleSheet, ViewStyle } from "react-native"
import { useIsRTL } from "../reactNativeHelpers/useIsRTL"
import { omitEmptyValueFields } from "./styleHelpers"

export interface NormalizedPositionStyle {
  position?: ViewStyle["position"]
  zIndex?: number
  top?: number
  right?: number
  bottom?: number
  left?: number
}

export interface PositionStyle {
  position?: ViewStyle["position"]
  zIndex?: number
  top?: number
  right?: number
  bottom?: number
  left?: number
  start?: number
  end?: number
}

export const useNormalizePositionStyle = (
  pos: undefined | StyleProp<PositionStyle>,
): NormalizedPositionStyle => {
  const isRTL = useIsRTL()

  if (pos == null) return {}

  const _pos = StyleSheet.flatten(pos)

  return omitEmptyValueFields({
    position: _pos.position,
    zIndex: _pos.zIndex,
    top: _pos.top,
    bottom: _pos.bottom,
    left: _pos.left ?? (isRTL ? _pos.start : _pos.end),
    right: _pos.right ?? (isRTL ? _pos.end : _pos.start),
  })
}
