import { FlexStyle as _FlexStyle } from "react-native"

export type FlexStyle = Pick<
  _FlexStyle,
  | "flex"
  | "flexShrink"
  | "flexGrow"
  | "flexBasis"
  | "flexDirection"
  | "flexWrap"
  | "alignItems"
  | "alignContent"
  | "alignSelf"
  | "justifyContent"
>
