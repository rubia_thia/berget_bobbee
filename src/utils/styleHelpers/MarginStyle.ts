export interface NormalizedMarginStyle {
  marginTop?: number
  marginBottom?: number
  marginLeft?: number
  marginRight?: number
}

export interface MarginStyle {
  margin?: number
  marginBottom?: number
  marginEnd?: number
  marginHorizontal?: number
  marginLeft?: number
  marginRight?: number
  marginStart?: number
  marginTop?: number
  marginVertical?: number
}
