import { StyleProp, StyleSheet } from "react-native"
import { useIsRTL } from "../reactNativeHelpers/useIsRTL"
import { omitEmptyValueFields } from "./styleHelpers"

export interface BorderRadiusStyle {
  borderRadius?: number
  borderTopLeftRadius?: number
  borderTopRightRadius?: number
  borderTopStartRadius?: number
  borderTopEndRadius?: number
  borderBottomLeftRadius?: number
  borderBottomRightRadius?: number
  borderBottomStartRadius?: number
  borderBottomEndRadius?: number
}

export const useNormalizeBorderRadiusStyle = (
  borderRadius: undefined | number | StyleProp<BorderRadiusStyle>,
): {
  borderTopLeftRadius?: number
  borderTopRightRadius?: number
  borderBottomLeftRadius?: number
  borderBottomRightRadius?: number
} => {
  const isRTL = useIsRTL()

  if (borderRadius == null) return {}

  if (typeof borderRadius === "number") {
    return {
      borderTopLeftRadius: borderRadius,
      borderTopRightRadius: borderRadius,
      borderBottomLeftRadius: borderRadius,
      borderBottomRightRadius: borderRadius,
    }
  }

  const _borderRadius = StyleSheet.flatten(borderRadius)

  return omitEmptyValueFields({
    borderTopLeftRadius:
      _borderRadius.borderTopLeftRadius ??
      (isRTL
        ? _borderRadius.borderTopEndRadius
        : _borderRadius.borderTopStartRadius) ??
      _borderRadius.borderRadius,
    borderTopRightRadius:
      _borderRadius.borderTopRightRadius ??
      (isRTL
        ? _borderRadius.borderTopStartRadius
        : _borderRadius.borderTopEndRadius) ??
      _borderRadius.borderRadius,
    borderBottomLeftRadius:
      _borderRadius.borderBottomLeftRadius ??
      (isRTL
        ? _borderRadius.borderBottomEndRadius
        : _borderRadius.borderBottomStartRadius) ??
      _borderRadius.borderRadius,
    borderBottomRightRadius:
      _borderRadius.borderBottomRightRadius ??
      (isRTL
        ? _borderRadius.borderBottomStartRadius
        : _borderRadius.borderBottomEndRadius) ??
      _borderRadius.borderRadius,
  })
}
