import { StyleProp, StyleSheet } from "react-native"
import { useIsRTL } from "../reactNativeHelpers/useIsRTL"
import { omitEmptyValueFields } from "./styleHelpers"

export interface PaddingStyle {
  padding?: number
  paddingBottom?: number
  paddingEnd?: number
  paddingHorizontal?: number
  paddingLeft?: number
  paddingRight?: number
  paddingStart?: number
  paddingTop?: number
  paddingVertical?: number
}

export const useNormalizePaddingStyle = (
  padding: undefined | number | StyleProp<PaddingStyle>,
): {
  paddingTop?: number
  paddingBottom?: number
  paddingLeft?: number
  paddingRight?: number
} => {
  const isRTL = useIsRTL()

  if (padding == null) return {}

  if (typeof padding === "number") {
    return {
      paddingTop: padding,
      paddingBottom: padding,
      paddingLeft: padding,
      paddingRight: padding,
    }
  }

  const _padding = StyleSheet.flatten(padding)

  return omitEmptyValueFields({
    paddingTop:
      _padding.paddingTop ?? _padding.paddingVertical ?? _padding.padding,
    paddingBottom:
      _padding.paddingBottom ?? _padding.paddingVertical ?? _padding.padding,
    paddingLeft:
      _padding.paddingLeft ??
      (isRTL ? _padding.paddingEnd : _padding.paddingStart) ??
      _padding.paddingHorizontal ??
      _padding.padding,
    paddingRight:
      _padding.paddingRight ??
      (isRTL ? _padding.paddingStart : _padding.paddingEnd) ??
      _padding.paddingHorizontal ??
      _padding.padding,
  })
}
