import { filter } from "ramda"
import {
  Platform,
  StyleProp,
  StyleSheet,
  TextStyle,
  ViewStyle,
} from "react-native"
import { isNotNull } from "../typeHelpers"

export const styleSheetCompose = <T>(
  ...styles: (StyleProp<T> | Array<StyleProp<T>>)[]
): StyleProp<T> => {
  return styles.reduce((acc, style) => StyleSheet.compose(acc, style))
}

export const styleSheetFlatCompose = <T>(
  ...styles: (StyleProp<T> | Array<StyleProp<T>>)[]
): T extends (infer U)[] ? U : T => {
  return StyleSheet.flatten(styleSheetCompose(...styles))
}

export const omitEmptyValueFields = filter(isNotNull)

export const marginCenter: ViewStyle = {
  marginTop: "auto",
  marginBottom: "auto",
  marginLeft: "auto",
  marginRight: "auto",
}

export const systemFontFamily: TextStyle["fontFamily"] =
  // https://github.com/necolas/react-native-web/blob/07feddf300aa86da0a633d60c05fe0a12594b75d/packages/react-native-web/src/exports/StyleSheet/compiler/createReactDOMStyle.js#L149
  Platform.OS === "web" ? "System" : undefined
