// https://necolas.github.io/react-native-web/docs/use-locale-context/
// @ts-ignore
import { I18nManager, Platform, useLocaleContext } from "react-native"

export const useIsRTL = (): boolean => {
  const localeContext = useLocaleContext()

  if (Platform.OS === "web") {
    return localeContext.direction === "rtl"
  } else {
    return I18nManager.isRTL
  }
}
