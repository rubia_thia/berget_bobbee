import type { Workbox } from "workbox-window"
import { type MessageController } from "../components/MessageProvider/MessageProvider"

export type OnWorkboxReadyFn = (
  onReady: (workbox: undefined | Workbox) => void,
) => void

export type GetMessageControllerFn = (
  callbackOnce: (ctrl: MessageController) => void,
) => void

export type EnableWorkboxInBrowserFn = (
  getMessageController: GetMessageControllerFn,
) => void
