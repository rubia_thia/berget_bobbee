import { Duration } from "date-fns"
import { sum } from "ramda"
import { IntlShape } from "react-intl"

export const durationToMs = (duration: Duration): number => {
  const {
    years = 0,
    months = 0,
    weeks = 0,
    days = 0,
    hours = 0,
    minutes = 0,
    seconds = 0,
  } = duration

  return sum([
    years * 365 * 24 * 60 * 60 * 1000,
    months * 30 * 24 * 60 * 60 * 1000,
    weeks * 7 * 24 * 60 * 60 * 1000,
    days * 24 * 60 * 60 * 1000,
    hours * 60 * 60 * 1000,
    minutes * 60 * 1000,
    seconds * 1000,
  ])
}

export const formatDuration = (
  intl: IntlShape,
  duration: Duration,
): undefined | string => {
  const sortedDurationKeys: (keyof Duration)[] = [
    "years",
    "months",
    "weeks",
    "days",
    "hours",
    "minutes",
    "seconds",
  ]

  const filledFieldName = sortedDurationKeys.find(key => duration[key] != null)

  if (filledFieldName == null) return

  return intl.formatRelativeTime(
    duration[filledFieldName]!,
    filledFieldName,
    {},
  )
}
