import { ChainInfo } from "../ChainInfo"
import BSCIcon from "./BSC.svg"

export namespace ChainInfoPresets {
  export const BSC: ChainInfo = {
    id: "bsc",
    displayName: "BSC",
    Icon: BSCIcon,
  }
}
