import { BigNumber } from "../numberHelpers/BigNumber"
import { Comparer } from "./isShallowEqual"

export const bigNumberComparer: Comparer = (a, b) => {
  if (BigNumber.isBigNumber(a) || BigNumber.isBigNumber(b)) {
    return BigNumber.isEq(a, b)
  } else {
    return undefined
  }
}
