import { BaseContract } from "ethers"
import { Observable } from "rxjs"
import {
  TypedEvent,
  TypedEventFilter,
} from "../../stores/contracts/generated/common"

type EventListenerArgsArray<T> = T extends TypedEvent<infer U>
  ? [...U, T]
  : never

export function fromContractEvent<TEvent extends TypedEvent>(
  contract: BaseContract,
  eventFilter: TypedEventFilter<TEvent>,
): Observable<EventListenerArgsArray<TEvent>> {
  return new Observable(ob => {
    const listener = (...args: EventListenerArgsArray<TEvent>): void => {
      ob.next(args)
    }

    contract.on(eventFilter, listener as any)
    return () => contract.off(eventFilter, listener as any)
  })
}
