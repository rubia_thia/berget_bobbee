import { from, Observable, ObservableInput, OperatorFunction } from "rxjs"
import { noop } from "../fnHelpers"

export type ObservableValueOf<T> = T extends ObservableInput<infer U>
  ? U
  : never

export function startWithObservable<T>(
  value: ObservableInput<T>,
): OperatorFunction<T, T> {
  return source =>
    new Observable(observer => {
      const sourceSubscription = source.subscribe({
        complete: () => observer.complete(),
        error: err => observer.error(err),
        next: val => {
          valueSubscription.unsubscribe()
          observer.next(val)
        },
      })

      const valueSubscription = from(value).subscribe({
        complete: noop,
        error: err => observer.error(err),
        next: val => observer.next(val),
      })

      return () => {
        sourceSubscription.unsubscribe()
        valueSubscription.unsubscribe()
      }
    })
}
