import { ComponentType } from "react"
import { ColorValue } from "react-native"

export interface ChainInfo {
  id: string

  displayName: string

  Icon: ComponentType<{
    width?: string | number
    height?: string | number
    fill?: ColorValue
  }>
}
