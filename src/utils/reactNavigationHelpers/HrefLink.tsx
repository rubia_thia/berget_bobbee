import { MouseEvent as ReactMouseEvent, ReactNode, useMemo } from "react"
import {
  GestureResponderEvent,
  Linking,
  Platform,
  StyleProp,
  TouchableOpacity,
  ViewStyle,
} from "react-native"
import { FCS } from "../reactHelpers/types"

type HrefLinkPressEvent =
  | ReactMouseEvent<HTMLAnchorElement, MouseEvent>
  | GestureResponderEvent

export interface HrefLinkComponentProps {
  accessibilityRole?: "link"
  href?: string
  hrefAttrs?: Record<string, any>
  onPress?: (e?: HrefLinkPressEvent) => void
  style?: StyleProp<ViewStyle>
}

export interface HrefLinkProps {
  href?: string
  onPress?: (e?: HrefLinkPressEvent) => void
  children?: ReactNode | ((linkProps: HrefLinkComponentProps) => ReactNode)
}

export const HrefLink: FCS<HrefLinkProps> = props => {
  const propsOnPress = props.onPress
  const propsHref = props.href

  const linkProps: HrefLinkComponentProps = useMemo(
    () =>
      propsHref == null
        ? { style: props.style, onPress: propsOnPress }
        : {
            style: props.style,
            accessibilityRole: "link" as const,
            ...Platform.select({
              web: {
                href: propsHref,
                hrefAttrs: {
                  rel: "noopener noreferrer",
                  target: "_blank",
                },
                onPress: propsOnPress,
              },
              default: {
                onPress: (e?: HrefLinkPressEvent): void => {
                  propsOnPress?.(e as any)
                  if (!e?.defaultPrevented) {
                    void Linking.openURL(propsHref)
                  }
                },
              },
            }),
          },
    [props.style, propsHref, propsOnPress],
  )

  if (typeof props.children === "function") {
    return <>{props.children(linkProps)}</>
  } else {
    return <TouchableOpacity {...linkProps}>{props.children}</TouchableOpacity>
  }
}
