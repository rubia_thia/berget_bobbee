import { NavigationAction } from "@react-navigation/core"
import {
  CompositeNavigationProp,
  NavigationProp,
  useLinkProps as _useLinkProps,
} from "@react-navigation/native"
import { To } from "@react-navigation/native/src/useLinkTo"
import { MouseEvent as ReactMouseEvent, useMemo, useState } from "react"
import { GestureResponderEvent } from "react-native"
import { useListenLinkActive } from "./linkTestHelpers"
import { InferNavigatorParamsList } from "./reactNavigationHelpers"

export interface NavLinkComponentActiveState {
  isActive: boolean
  isChildrenActive: boolean
}

export type NavLinkPressEvent =
  | ReactMouseEvent<HTMLAnchorElement, MouseEvent>
  | GestureResponderEvent

export type NavLinkComponentProps = {
  accessibilityRole?: "link"
  href?: string
  hrefAttrs?: Record<string, any>
  onPress: (e?: NavLinkPressEvent) => void
}

export const useNavLinkProps = <
  N extends NavigationProp<any> | CompositeNavigationProp<any, any>,
  RouteName extends keyof InferNavigatorParamsList<N> = keyof InferNavigatorParamsList<N>,
>(
  navigation: N,
  options: {
    to: To<InferNavigatorParamsList<N>, RouteName>
    action?: NavigationAction
  },
): [
  linkProps: NavLinkComponentProps,
  activeState: NavLinkComponentActiveState,
] => {
  const res = _useLinkProps<InferNavigatorParamsList<N>>(options as any)

  const [activeState, setActiveState] = useState({
    isActive: false,
    isChildrenActive: false,
  })

  useListenLinkActive(navigation, options.to, setActiveState)

  return useMemo(() => [res, activeState], [activeState, res])
}
