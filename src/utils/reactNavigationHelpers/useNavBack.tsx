import { NavigationProp, useNavigation } from "@react-navigation/native"
import { usePersistFn } from "../reactHelpers/usePersistFn"
import { isStackNavigationProp } from "./isStackNavigationProp"
import { useQuitFlow } from "./useQuitFlow"

export type NavBackFn = () => void

export const useNavBack = (navigation?: NavigationProp<any>): NavBackFn => {
  const thisLevelNavigation = useNavigation()
  const quitFlow = useQuitFlow()

  const _navigation = navigation ?? thisLevelNavigation

  return usePersistFn(() => {
    if (_navigation.getState().index === 0) {
      quitFlow()
      return
    }

    if (isStackNavigationProp(_navigation)) {
      _navigation.pop()
    } else {
      _navigation.goBack()
    }
  })
}
