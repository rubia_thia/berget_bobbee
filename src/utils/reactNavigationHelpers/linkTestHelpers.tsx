import { NavigationState } from "@react-navigation/core"
import { EventMapCore } from "@react-navigation/core/src/types"
import {
  CompositeNavigationProp,
  NavigationProp,
} from "@react-navigation/native"
import { To } from "@react-navigation/native/lib/typescript/src/useLinkTo"
import { PartialState } from "@react-navigation/routers/lib/typescript/src/types"
import { equals } from "ramda"
import { createContext, useCallback, useContext, useEffect } from "react"
import { noop } from "../fnHelpers"
import { isShallowEqual } from "../isShallowEqual/isShallowEqual"
import { FCC } from "../reactHelpers/types"
import { useLatestValueRef } from "../reactHelpers/useLatestValueRef"
import { usePersistFn } from "../reactHelpers/usePersistFn"
import { AnyObject, Keyof } from "../types"
import {
  AnySimpleRoute,
  AnySimpleRouteWithParents,
  getActiveRoute,
  getMatchedRoute,
  InferNavigatorParamsList,
  isRoutesEqual,
} from "./reactNavigationHelpers"

export type MockLinkActiveRouteFn = (to: To<AnyObject>) => {
  isActive: boolean
  isChildrenActive: boolean
}

const MockLinkActiveRouteContext = createContext<null | MockLinkActiveRouteFn>(
  null,
)

export const MockLinkActiveRouteProvider: FCC<{
  fn: MockLinkActiveRouteFn
}> = props => {
  return (
    <MockLinkActiveRouteContext.Provider value={props.fn}>
      {props.children}
    </MockLinkActiveRouteContext.Provider>
  )
}

const _mockNavigationStateSymbol = Symbol("mock navigation state")
const _mockNavigationState: NavigationState = {
  key: "mock",
  index: 0,
  routeNames: [],
  routes: [],
  type: "stack",
  stale: false,
}
export const isMockNavigationState = (state: NavigationState): boolean => {
  // @ts-ignore
  return state[_mockNavigationStateSymbol] === true
}
const _mockNavigationTemplate: NavigationProp<
  AnyObject,
  Keyof<AnyObject>,
  undefined,
  NavigationState<AnyObject>,
  AnyObject,
  EventMapCore<NavigationState<AnyObject>>
> = {
  addListener: () => noop,
  removeListener: noop,
  navigate: noop,
  dispatch: noop,
  goBack: noop,
  setOptions: noop,
  setParams: noop,
  reset: noop,
  getId: () => undefined,
  canGoBack: () => false,
  isFocused: () => false,
  getParent: () => undefined as any,
  getState: () => {
    const res = { ..._mockNavigationState }
    // @ts-ignore
    res[_mockNavigationStateSymbol] = true
    return res as any
  },
}

const _mockNavigationSymbol = Symbol("mock navigation")
export function isMockNavigation(
  nav: NavigationProp<any> | CompositeNavigationProp<any, any>,
): boolean {
  // @ts-ignore
  return nav[_mockNavigationSymbol] === true
}
export function mockNavigation<ParamsList extends AnyObject>(): NavigationProp<
  ParamsList,
  Keyof<ParamsList>,
  undefined,
  NavigationState<ParamsList>,
  ParamsList,
  EventMapCore<NavigationState<ParamsList>>
> {
  const res = { ..._mockNavigationTemplate }
  // @ts-ignore
  res[_mockNavigationSymbol] = true
  return res as any
}

interface ActiveState {
  isActive: boolean
  isChildrenActive: boolean
}

export function useListenLinkActive<
  N extends NavigationProp<any> | CompositeNavigationProp<any, any>,
  RouteName extends keyof InferNavigatorParamsList<N> = keyof InferNavigatorParamsList<N>,
>(
  navigation: N,
  to: To<InferNavigatorParamsList<N>, RouteName>,
  setActiveState: (updater: (oldState: ActiveState) => ActiveState) => void,
): void {
  const mockLinkActiveRouteCtx = useContext(MockLinkActiveRouteContext)

  const memoizedTo = useLatestValueRef(to, equals).current

  const _setActiveState = usePersistFn((newState: ActiveState): void => {
    setActiveState(oldState =>
      isShallowEqual(oldState, newState) ? oldState : newState,
    )
  })

  const onNavStateChange = useCallback(
    (state: undefined | NavigationState) => {
      if (state == null) return

      if (isMockNavigationState(state)) {
        if (mockLinkActiveRouteCtx == null) return
        _setActiveState(mockLinkActiveRouteCtx(memoizedTo as any))
        return
      }

      const matchedRoute = getMatchedRoute(state, memoizedTo)
      if (matchedRoute == null) {
        _setActiveState({
          isActive: false,
          isChildrenActive: false,
        })
        return
      }

      const activeRoute = getActiveRoute(state)

      if (
        activeRoute.parents.length === 0 &&
        matchedRoute.parents.length === 0
      ) {
        _setActiveState({
          isActive: isRoutesEqual(activeRoute, matchedRoute),
          isChildrenActive: false,
        })
        return
      }

      if (
        activeRoute.key != null &&
        matchedRoute.key != null &&
        activeRoute.key === matchedRoute.key
      ) {
        _setActiveState({
          isActive: true,
          isChildrenActive: false,
        })
        return
      }

      const isActive = isRoutesEqual(matchedRoute, activeRoute)
      _setActiveState({
        isActive,
        isChildrenActive:
          !isActive && checkIsChildrenActive(matchedRoute, activeRoute),
      })
    },
    [_setActiveState, memoizedTo, mockLinkActiveRouteCtx],
  )

  useEffect(() => {
    onNavStateChange(navigation.getState())
    return navigation.addListener("state", e => {
      onNavStateChange(e.data.state)
    })
  }, [navigation, onNavStateChange])
}

const checkIsChildrenActive = (
  parentRoute: AnySimpleRouteWithParents & {
    state?: NavigationState | PartialState<NavigationState>
  },
  activeRoute: AnySimpleRouteWithParents,
): boolean => {
  const currentSubStateRouteParents: AnySimpleRouteWithParents["parents"] = [
    ...parentRoute.parents,
    parentRoute,
  ]

  if (parentRoute.state == null || parentRoute.state.routes.length === 0) {
    return false
  }

  const activeSubRoute = (parentRoute.state.routes as AnySimpleRoute[]).find(
    r =>
      isRoutesEqual(
        { ...r, parents: currentSubStateRouteParents },
        activeRoute,
      ),
  )
  if (activeSubRoute != null) {
    return true
  }

  return parentRoute.state.routes.some(r =>
    checkIsChildrenActive(
      { ...r, parents: currentSubStateRouteParents },
      activeRoute,
    ),
  )
}
