import { createContext, useContext } from "react"
import { FCC } from "../reactHelpers/types"
import { usePersistFn } from "../reactHelpers/usePersistFn"

export type QuitFlowFn = () => void

const QuitFlowContext = createContext<null | QuitFlowFn>(null)

export const QuitFlowProvider: FCC<{ quitFlow: QuitFlowFn }> = props => {
  const quitFlow = usePersistFn(props.quitFlow)

  return (
    <QuitFlowContext.Provider value={quitFlow}>
      {props.children}
    </QuitFlowContext.Provider>
  )
}

export const useQuitFlow = (): QuitFlowFn => {
  const fn = useContext(QuitFlowContext)

  if (fn == null) {
    throw new Error("useQuitFlow must be used within a QuitFlowProvider")
  }

  return fn
}
