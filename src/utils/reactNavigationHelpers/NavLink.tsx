import { NavigationAction } from "@react-navigation/core"
import {
  CompositeNavigationProp,
  NavigationProp,
} from "@react-navigation/native"
import { To } from "@react-navigation/native/lib/typescript/src/useLinkTo"
import { MouseEvent as ReactMouseEvent, ReactNode } from "react"
import { GestureResponderEvent, View } from "react-native"
import { InferNavigatorParamsList } from "./reactNavigationHelpers"
import {
  NavLinkComponentActiveState,
  NavLinkComponentProps,
  NavLinkPressEvent,
  useNavLinkProps,
} from "./useNavLinkProps"

export interface NavLinkProps<
  N extends NavigationProp<any> | CompositeNavigationProp<any, any>,
  RouteName extends keyof InferNavigatorParamsList<N>,
> {
  navigation: N
  to: To<InferNavigatorParamsList<N>, RouteName>
  action?: NavigationAction
  onPress?: (e: NavLinkPressEvent) => void
  children?:
    | ReactNode
    | ((
        linkProps: NavLinkComponentProps,
        activeState: NavLinkComponentActiveState,
      ) => ReactNode)
}

export const NavLink = <
  N extends NavigationProp<any> | CompositeNavigationProp<any, any>,
  RouteName extends keyof InferNavigatorParamsList<N>,
>(
  props: NavLinkProps<N, RouteName>,
): JSX.Element => {
  const [linkProps, activeState] = useNavLinkProps(props.navigation, {
    to: props.to,
    action: props.action,
  })

  const onPress = (
    e: ReactMouseEvent<HTMLAnchorElement, MouseEvent> | GestureResponderEvent,
  ): void => {
    props.onPress?.(e as any)
    linkProps.onPress?.(e)
  }

  if (typeof props.children === "function") {
    return <>{props.children(linkProps, activeState)}</>
  } else {
    return (
      <View {...linkProps} {...{ onPress }}>
        {props.children}
      </View>
    )
  }
}
