import { NavigationAction, NavigationState } from "@react-navigation/core"
import {
  getStateFromPath,
  NavigationProp,
  PartialRoute,
  Route,
  useLinkTo as _useLinkTo,
} from "@react-navigation/native"
import { To } from "@react-navigation/native/lib/typescript/src/useLinkTo"
import { PartialState } from "@react-navigation/routers/lib/typescript/src/types"
import { equals } from "ramda"
import { isShallowEqual } from "../isShallowEqual/isShallowEqual"
import { AnyObject, CompactType, UnionToIntersection } from "../types"

export type UniversalLinkInfo =
  | ({
      type: "nav"
    } & NavLinkInfo<any>)
  | {
      type: "href"
      href: string
    }

export interface NavLinkInfo<ParamsList extends AnyObject> {
  navigation: NavigationProp<any>
  to: To<ParamsList>
  action?: NavigationAction
}

export const navLinkInfo = <NavProp extends NavigationProp<any>>(info: {
  navigation: NavProp
  to: To<InferNavigatorParamsList<NavProp>>
  action?: NavigationAction
}): NavLinkInfo<InferNavigatorParamsList<NavProp>> => {
  return info
}

export type InferNavigatorParamsList<T> = CompactType<
  UnionToIntersection<
    T extends {
      navigate: (options: infer R) => any
    }
      ? R extends infer R1 extends { name: string }
        ? {
            [K in R1["name"]]: R1 extends { name: K; params: infer R2 }
              ? R2
              : never
          }
        : never
      : never
  >
>

export type ExtractPathParams<Path> = CompactType<
  Path extends `${infer Segment}/${infer Rest}`
    ? ExtractPathParams<Segment> & ExtractPathParams<Rest>
    : Path extends `:${infer Param}?`
    ? Partial<Record<Param, string>>
    : Path extends `:${infer Param}`
    ? Record<Param, string>
    : Path extends `*`
    ? Record<"*", string>
    : AnyObject
>

export const useLinkTo = <ParamsList extends AnyObject>(
  navigation: NavigationProp<ParamsList, any, any, any, any, any>,
): ReturnType<typeof _useLinkTo<ParamsList>> => {
  return _useLinkTo<ParamsList>()
}

export interface AnySimpleRoute {
  key?: string
  name: string
  params?: AnyObject
}
export interface AnySimpleRouteWithParents extends AnySimpleRoute {
  parents: AnySimpleRouteWithParents[]
}

export const isRoutesEqual = (
  r1: AnySimpleRouteWithParents,
  r2: AnySimpleRouteWithParents,
): boolean => {
  if (r1.parents.length === 0 && r2.parents.length === 0) {
    return isSimpleEqual(r1, r2)
  }

  return (
    isSimpleEqual(r1, r2) &&
    r1.parents.every((r, idx) => isSimpleEqual(r, r2.parents[idx]!))
  )

  function isSimpleEqual(r1: AnySimpleRoute, r2: AnySimpleRoute): boolean {
    if (r1.key != null && r2.key != null) return r1.key === r2.key
    return r1.name === r2.name && equals(r1.params, r2.params)
  }
}

export function getActiveRoute(
  state: NavigationState | PartialState<NavigationState>,
): AnySimpleRouteWithParents {
  return _getActiveRoute(state, [])
}
function _getActiveRoute(
  state: NavigationState | PartialState<NavigationState>,
  parents: AnySimpleRouteWithParents[],
): AnySimpleRouteWithParents {
  const route =
    typeof state.index === "number"
      ? state.routes[state.index]!
      : state.routes[state.routes.length - 1]!

  if (route.state) {
    return _getActiveRoute(route.state, [...parents, { ...route, parents }])
  }

  return { ...route, parents }
}

export function getMatchedRoute(
  state: NavigationState | PartialState<NavigationState>,
  to: To<any>,
):
  | undefined
  | (AnySimpleRouteWithParents & {
      state?: NavigationState | PartialState<NavigationState>
    }) {
  return _getMatchedRoute(state, to, [])
}
function _getMatchedRoute(
  state: NavigationState | PartialState<NavigationState>,
  to: To<any>,
  parents: AnySimpleRouteWithParents[],
):
  | undefined
  | (AnySimpleRouteWithParents & {
      state?: NavigationState | PartialState<NavigationState>
    }) {
  if (typeof to === "string") {
    const _state = getStateFromPath(to)
    if (_state == null) return
    const matchingRoute = getActiveRoute(_state)
    return _getMatchedRoute(
      state,
      {
        screen: matchingRoute.name,
        params: matchingRoute.params,
      },
      parents,
    )
  }

  const routes = state.routes as PartialRoute<Route<string>>[]

  const route = routes.find(
    r => r.name === to.screen && isShallowEqual(r.params, to.params),
  )
  if (route != null) {
    return { ...route, parents }
  }

  for (const route of routes) {
    if (route.state == null) continue
    const matchedRoute = _getMatchedRoute(route.state, to, [
      ...parents,
      { ...route, parents },
    ])
    if (matchedRoute) return { ...matchedRoute, parents }
  }

  return
}
