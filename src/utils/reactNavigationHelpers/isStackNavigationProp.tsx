import { NavigationProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

export function isStackNavigationProp(
  nav: NavigationProp<any>,
): nav is StackNavigationProp<any, any> {
  return nav.getState().type === "stack"
}
