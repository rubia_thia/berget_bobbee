/* eslint-disable @typescript-eslint/no-var-requires */

import { TokenInfo } from "../TokenInfo"

export namespace TokenInfoPresets {
  export const ULP: TokenInfo = {
    id: "ulp",
    displayName: "ULP",
    precision: 4,
    icon: require("./_/ULP.png"),
  }

  export const UNW: TokenInfo = {
    id: "unw",
    displayName: "UNW",
    precision: 4,
    icon: require("./_/UNW.png"),
  }

  export const esUNW: TokenInfo = {
    id: "ulp",
    displayName: "esUNW",
    precision: 4,
    icon: require("./_/esUNW.png"),
  }

  export const NFT_UGP: TokenInfo = {
    id: "nft_ugp",
    displayName: "Genesis Pass",
    precision: 0,
    icon: require("./_/NFT_UGP.png"),
  }

  export const MockBTC: TokenInfo = {
    id: "btc",
    displayName: "BTC",
    precision: 5,
    icon: require("./_/BTC.png"),
  }

  export const MockBUSD: TokenInfo = {
    id: "busd",
    displayName: "BUSD",
    precision: 2,
    icon: require("./_/BUSD.png"),
  }

  export const MockUSDC: TokenInfo = {
    id: "usdc",
    displayName: "USDC",
    precision: 2,
    icon: require("./_/USDC.png"),
  }

  export const MockUSDT: TokenInfo = {
    id: "usdt",
    displayName: "USDT",
    precision: 2,
    icon: require("./_/USDT.png"),
  }
}
