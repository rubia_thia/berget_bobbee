import {
  EnableWorkboxInBrowserFn,
  OnWorkboxReadyFn,
} from "./enableWorkboxInBrowser.types"

export const onWorkboxReady: OnWorkboxReadyFn = (): void => {
  // no op in native
}

export const enableWorkboxInBrowser: EnableWorkboxInBrowserFn = (): void => {
  // no op in native
}
