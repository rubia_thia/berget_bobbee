export function isNotNull<T>(input: T | undefined | null): input is T {
  return input != null
}

export function isNull<T>(
  input: T | undefined | null,
): input is undefined | null {
  return input == null
}

/**
 * Designed to type checking union type/enum values exhausted
 */
export function checkNever(input: never): undefined {
  return
}

/**
 * Designed to type checking some object field destructuring exhausted
 *
 * @example
 * ```typescript
 * const renderFunction: RenderFunction = ({ propA, propB, ...rest}) => {
 *   checkObjEmpty(rest)
 *
 *   //...
 * }
 * ```
 */
export function checkObjEmpty(input: Record<string, never>): undefined {
  return
}
// checkEmptyRest({})
// checkEmptyRest({ a: 1 })
// checkEmptyRest(undefined)
// checkEmptyRest(null)
// checkEmptyRest([])
