import { Platform } from "react-native"

export const isTouchDevice = (): boolean => {
  if (Platform.OS === "web") {
    return "ontouchstart" in window || navigator.maxTouchPoints > 0
  } else {
    return true
  }
}
