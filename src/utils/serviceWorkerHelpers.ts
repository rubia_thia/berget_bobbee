import { defer } from "./promiseHelpers"

export type ServiceWorkerState = "waiting" | "activated" | "broken"
export const untilArriveWaitingState = (
  sw: ServiceWorker,
): Promise<ServiceWorkerState> => {
  const deferred = defer<ServiceWorkerState>()

  const onStateChange = (): void => {
    const off = (): void => sw.removeEventListener("statechange", onStateChange)

    if (sw.state === "redundant") {
      off()
      deferred.resolve("broken")
    } else if (sw.state === "activated") {
      off()
      deferred.resolve("activated")
    } else if (isWaiting(sw)) {
      off()
      deferred.resolve("waiting")
    }
  }

  if (isNotEligibleToWaiting(sw)) {
    sw.addEventListener("statechange", onStateChange)
  }
  onStateChange()

  return deferred.promise
}

const isWaiting = (sw: ServiceWorker): boolean => {
  return sw.state === "activating"
}

const isNotEligibleToWaiting = (sw: ServiceWorker): boolean => {
  return (
    sw.state === "parsed" ||
    sw.state === "installing" ||
    sw.state === "installed"
  )
}
