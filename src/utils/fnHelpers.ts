import _debounce from "debounce-fn"

// eslint-disable-next-line @typescript-eslint/no-empty-function
export const noop = (...args: any[]): void => {}

export type DebouncedFn<Args extends any[]> = {
  (...args: Args): void
  cancel(): void
}
export function debounce<Args extends any[]>(
  func: (...args: Args) => any,
  wait: number,
  options: {
    maxWait?: number
    leading?: boolean
    trailing?: boolean
  } = {},
): DebouncedFn<Args> {
  return _debounce(func, {
    wait,
    maxWait: options.maxWait ?? Infinity,
    before: options.leading ?? false,
    after: options.trailing ?? true,
  })
}

export type ThrottledFn<Args extends any[]> = {
  (...args: Args): void
  cancel(): void
}
export function throttle<Args extends any[]>(
  func: (...args: Args) => any,
  wait: number,
  options: {
    leading?: boolean
    trailing?: boolean
  } = {},
): ThrottledFn<Args> {
  const leading = "leading" in options ? !!options.leading : true
  const trailing = "trailing" in options ? !!options.trailing : true

  return debounce(func, wait, {
    leading: leading,
    maxWait: wait,
    trailing: trailing,
  })
}
