import { ImageSourcePropType } from "react-native"

export interface TokenInfo {
  id: string
  displayName: string
  precision?: number
  icon: ImageSourcePropType
}

export namespace TokenInfo {
  export const fallbackPrecision = 0

  export const getPrecision = (tokenInfo?: TokenInfo): number => {
    return tokenInfo?.precision ?? fallbackPrecision
  }

  export const isIdentical = (
    token1: TokenInfo,
    token2: TokenInfo,
  ): boolean => {
    return token1.id === token2.id
  }
}
