export function isInteger(n: number): boolean {
  return parseInt(String(n), 10) === n
}
