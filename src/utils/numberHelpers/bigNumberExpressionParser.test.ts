import { BigNumber } from "./BigNumber"
import { math, mathIs } from "./bigNumberExpressionParser"

describe("bigNumberExpressionParser", function () {
  it("should parse a simple expression", function () {
    expect(math`${1} + ${2}`).toEqual(BigNumber.from(3))
    expect(math`${1} * ${2}`).toEqual(BigNumber.from(2))
    expect(math`${2} / ${1}`).toEqual(BigNumber.from(2))

    expect(math`(${2} + ${3}) / (${2} * (${1} + ${3}))`).toEqual(
      BigNumber.from(5 / 8),
    )

    expect(math` ${10} ** ${4}`).toEqual(BigNumber.from(10000))
    expect(math`-${10} + ${4}`).toEqual(BigNumber.from(-6))

    expect(math`${10_000_000} << ${3}`).toEqual(BigNumber.from(10_000))
    expect(() => math`${1} / ${0}`).toThrow(/Division by zero/)

    expect(math`${2e18} *~ ${3e18}`).toEqual(BigNumber.from(6e18))
    expect(math`${6e18} /~ ${3e18}`).toEqual(BigNumber.from(2e18))

    expect(math`${2} x ${3}`).toEqual(BigNumber.from(6))
    expect(math`sqrt(${4})`).toEqual(BigNumber.from(2))

    expect(math`abs(${-4} + ${2})`).toEqual(BigNumber.from(2))

    expect(math`max(${4}, ${5})`).toEqual(BigNumber.from(5))
    expect(math`min((${4} - ${3}), ${5})`).toEqual(BigNumber.from(1))
    expect(math`max(${0}, min(${4}, ${2}))`).toEqual(BigNumber.from(2))
  })

  it("should mathIs", function () {
    expect(mathIs`${1} > ${3}`).toBeFalsy()
    expect(mathIs`${3} > ${2}`).toBeTruthy()
    expect(mathIs`${2} ^ ${2} == ${4}`).toBeTruthy()
    expect(mathIs`${2} ^ ${2} === ${4}`).toBeTruthy()
    expect(mathIs`${1} == ${2} || ${1} == ${3} || ${2} == ${2}`).toBeTruthy()
    expect(mathIs`${1} == ${2} && ${1} == ${3} && ${2} == ${2}`).toBeFalsy()
    expect(mathIs`${1} < ${2} && ${1} < ${3} && ${2} >= ${2}`).toBeTruthy()

    expect(mathIs`${1} ${"<="} ${2}`).toBeTruthy()
  })
})
