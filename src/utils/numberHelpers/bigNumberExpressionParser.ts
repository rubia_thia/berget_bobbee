import { BigNumber, BigNumberSource } from "./BigNumber"
import { Parser, ParserError } from "./ParsorC"

export type ExpressionSource =
  | {
      type: "expression"
      expression: string
    }
  | {
      type: "value"
      value: BigNumberSource
    }

const expression = <Op extends string>(
  input: Op,
): Parser<ExpressionSource, Op> =>
  new Parser<ExpressionSource, Op>(i => {
    const expressions = i.slice(0, input.length).map(a => {
      if (a.type !== "expression") {
        throw new ParserError(`not expression ${input}`)
      }
      return a.expression
    })
    if (expressions.join("") === input) {
      return [input, i.slice(input.length)]
    }
    throw new ParserError(`not matching ${input}`, i)
  })

function fails<T>(): Parser<ExpressionSource, T> {
  return new Parser(() => {
    throw new ParserError("Fails")
  })
}

function oneOf<T>(
  p: Parser<ExpressionSource, T>[],
): Parser<ExpressionSource, T> {
  return p.reduce((a, b) => a.or(b), fails())
}

// \d+
function some<T>(
  p: Parser<ExpressionSource, T>,
): Parser<ExpressionSource, T[]> {
  return p.flatMap(x => someOrNone(p).map(xs => [x, ...xs]))
}
// \d*
function someOrNone<T>(
  p: Parser<ExpressionSource, T>,
): Parser<ExpressionSource, T[]> {
  return some(p).or(Parser.unit([]))
}

const number = new Parser<ExpressionSource, BigNumberSource>(i => {
  const [head, ...tail] = i
  if (head == null || head.type !== "value") {
    throw new ParserError(`Not a BigNumberSource`)
  }
  return [head.value, tail]
})

const deferredExpression = defer(() => expr)

const paras = expression("(")
  .apply(deferredExpression, (_, b) => b)
  .apply(expression(")"), (a, _) => a)

const factor = number.or(paras)

const neg = expression("-").apply(
  factor,
  (_, b) => BigNumber.neg(b) as BigNumberSource,
)

const sqrt = expression("sqrt").apply(
  paras,
  (_, b) => BigNumber.sqrt(b) as BigNumberSource,
)

const abs = expression("abs").apply(
  paras,
  (_, b) => BigNumber.abs(b) as BigNumberSource,
)

const minAndMax = oneOf([
  expression("max").map(
    () => (a: BigNumberSource) => (b: BigNumberSource) => BigNumber.max([a, b]),
  ),
  expression("min").map(
    () => (a: BigNumberSource) => (b: BigNumberSource) => BigNumber.min([a, b]),
  ),
])
  .apply(expression("("), (a, _) => a)
  .apply(deferredExpression, (a, b) => a(b))
  .apply(expression(","), (a, _) => a)
  .apply(deferredExpression, (a, b) => a(b))
  .apply(expression(")"), (a, _) => a)

const numberWithDec = neg.or(sqrt).or(abs).or(minAndMax).or(factor)

const term = numberWithDec.apply(
  someOrNone(
    oneOf(
      [
        expression("<<").map(
          () => (x: BigNumberSource) => (y: BigNumberSource) =>
            BigNumber.leftMoveDecimals(
              BigNumber.toNumber(y),
              x,
            ) as BigNumberSource,
        ),
        expression(">>").map(
          () => (x: BigNumberSource) => (y: BigNumberSource) =>
            BigNumber.rightMoveDecimals(
              BigNumber.toNumber(y),
              x,
            ) as BigNumberSource,
        ),
        oneOf([expression("**"), expression("^")]).map(
          () => (x: BigNumberSource) => (y: BigNumberSource) =>
            BigNumber.pow(x, BigNumber.toNumber(y)) as BigNumberSource,
        ),
        expression("*~").map(
          () => (x: BigNumberSource) => (y: BigNumberSource) =>
            BigNumber.div(
              BigNumber.mul(x, y),
              BigNumber.ONE,
            ) as BigNumberSource,
        ),
        expression("/~").map(
          () => (x: BigNumberSource) => (y: BigNumberSource) =>
            BigNumber.mul(
              BigNumber.div(x, y),
              BigNumber.ONE,
            ) as BigNumberSource,
        ),
        expression("*" as string)
          .or(expression("x"))
          .map(() => BigNumber.mul),
        expression("/").map(() => BigNumber.div),
      ].map(o => o.apply(numberWithDec, (op, right) => [op, right] as const)),
    ),
  ),
  (left, list) =>
    list.reduce(
      (curr, [op, right]) => op(curr)(right),
      left,
    ) as BigNumberSource,
)

const expr: Parser<ExpressionSource, BigNumberSource> = term.apply(
  someOrNone(
    oneOf(
      [
        expression("+").map(() => BigNumber.add),
        expression("-").map(() => BigNumber.minus),
      ].map(o => o.apply(term, (op, right) => [op, right] as const)),
    ),
  ),
  (left, list) =>
    list.reduce(
      (curr, [op, right]) => op(curr)(right),
      left,
    ) as BigNumberSource,
)

function defer<T>(
  p: () => Parser<ExpressionSource, T>,
): Parser<ExpressionSource, T> {
  return new Parser(input => {
    return p().parse(input)
  })
}

export const EOF = new Parser<ExpressionSource, null>(input => {
  if (input.length === 0) {
    return [null, []]
  }
  throw new ParserError(
    `Did not reach parse to end, remaining "${input
      .map(x =>
        x.type === "expression"
          ? x.expression
          : BigNumber.from(x.value).toString(),
      )
      .join(" ")}"`,
  )
})

function extractExpressions(
  operators: TemplateStringsArray,
  args: BigNumberSource[],
): ExpressionSource[] {
  const result: ExpressionSource[] = []
  for (let i = 0; i < operators.length; i++) {
    const chars = operators[i]!.split("").filter(x => x.trim())
    for (const char of chars) {
      if (!isNaN(char as any)) {
        throw new ParserError(
          `You need to wrap all the numbers in \${}, found a ${char}`,
        )
      }
      result.push({
        type: "expression",
        expression: char,
      })
    }
    const arg = args[i]
    if (arg != null) {
      if (
        typeof arg === "string" &&
        ["+", "-", "*", "/", ">", "<", "===", "==", "<=", ">="].includes(
          arg.trim(),
        )
      ) {
        result.push(
          ...arg.split("").map(a => ({
            type: "expression" as const,
            expression: a,
          })),
        )
      } else {
        result.push({
          type: "value",
          value: arg!,
        })
      }
    }
  }
  return result
}

/**
 * this is the helper method to do BigNumber calculations
 * It supports normal operators like +, -, *, /, **
 * And also
 * - *leftMoveDecimals* via <<
 * - *mulDown* via *~
 * - *divDown* via /~
 */
export function math(
  operators: TemplateStringsArray,
  ...args: Array<BigNumberSource>
): BigNumber {
  const result = extractExpressions(operators, args)
  const [parsed] = expr.apply(EOF, a => a).parse(result)
  return BigNumber.from(parsed)
}

export function mathDebug(
  operators: TemplateStringsArray,
  ...args: Array<BigNumberSource>
): BigNumber {
  console.time("mathDebug")
  const result = extractExpressions(operators, args)
  console.timeLog("mathDebug", "expressed extracted")
  console.log(
    "mathDebug",
    result
      .map(a => (a.type === "expression" ? a.expression : a.value))
      .join(""),
  )
  const [parsed] = expr.apply(EOF, a => a).parse(result)
  console.timeEnd("mathDebug")
  console.log("mathDebug", parsed.toString())
  return BigNumber.from(parsed)
}

const comparator = expr
  .apply(
    oneOf([
      expression(">=").map(() => BigNumber.isGte),
      expression(">").map(() => BigNumber.isGt),
      expression("<=").map(() => BigNumber.isLte),
      expression("<").map(() => BigNumber.isLt),
      expression<"===" | "==">("===")
        .or(expression("=="))
        .map(() => BigNumber.isEq),
      expression<"!==" | "!=">("!==")
        .or(expression("!="))
        .map(
          () => (a: BigNumberSource) => (b: BigNumberSource) =>
            !BigNumber.isEq(a)(b),
        ),
    ]),
    (a, b) => [a, b] as const,
  )
  .apply(expr, ([a, op], b) => op(a)(b))

const logicOperator = comparator.apply(
  someOrNone(
    oneOf(
      [
        expression("&&").map(() => (a: boolean) => (b: boolean) => a && b),
        expression("||").map(() => (a: boolean) => (b: boolean) => a || b),
      ].map(o => o.apply(comparator, (op, right) => [op, right] as const)),
    ),
  ),
  (left, list) => list.reduce((curr, [op, right]) => op(curr)(right), left),
)

export function mathIs(
  operators: TemplateStringsArray,
  ...args: Array<BigNumberSource>
): boolean {
  const result = extractExpressions(operators, args)
  const [parsed] = logicOperator.apply(EOF, a => a).parse(result)
  return parsed
}
