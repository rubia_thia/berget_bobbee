import { Big, BigSource } from "big.js"
import { BigNumber as EthBN } from "ethers"
import { compose, curryN, reduce, sort, __ as R__ } from "ramda"
import { OneOrMore } from "../types"
import { isInteger } from "./numberHelpers"

export type BigNumberSource = number | string | Big | EthBN | BigNumber

const toBig = (num: BigNumberSource): Big => {
  if (num instanceof Big) {
    return num
  }

  return new Big(num as BigSource)
}

const fromBig = (num: Big): BigNumber => {
  return num as unknown as BigNumber
}

export type BigNumber = Omit<Big, keyof Big> & {
  readonly ___unique: unique symbol
  ___B: "BigNumber"
}
export namespace BigNumber {
  export const { roundUp, roundDown, roundHalfUp, roundHalfEven } = Big
  export type RoundingMode =
    | typeof roundUp
    | typeof roundDown
    | typeof roundHalfUp
    | typeof roundHalfEven

  export const isBigNumber = (num: any): num is BigNumber => {
    return num instanceof Big
  }

  export const safeFrom = (value: BigNumberSource): undefined | BigNumber => {
    try {
      return from(value)
    } catch (e) {
      return undefined
    }
  }

  export const from = (value: BigNumberSource): BigNumber => {
    if (EthBN.isBigNumber(value)) {
      return fromBig(toBig(value.toString()))
    }

    return fromBig(toBig(value as any))
  }

  export const toString = (value: BigNumberSource): string => {
    return toBig(value).toString()
  }

  export const toNumber = (value: BigNumberSource): number => {
    return toBig(value).toNumber()
  }

  export const toFixed = curry2(
    (
      options: {
        precision?: number
        roundingMode?: RoundingMode
      },
      value: BigNumberSource,
    ): string => {
      return toBig(value).toFixed(options.precision, options.roundingMode)
    },
  )

  export const toExponential = curry2(
    (
      options: {
        precision?: number
        roundingMode?: RoundingMode
      },
      value: BigNumberSource,
    ): string => {
      return toBig(value).toExponential(options.precision, options.roundingMode)
    },
  )

  export const toEth = (value: BigNumberSource): EthBN => {
    return toEtherBigNumber({
      decimals: 0,
    })(value)
  }

  export const toEtherBigNumber = curry2(
    (
      options: {
        decimals: number
        roundingMode?: RoundingMode
      },
      value: BigNumberSource,
    ): EthBN => {
      if (options.decimals < 0 || !isInteger(options.decimals)) {
        throw new Error(
          "[BigNumber.toEtherBigNumber] `options.decimals` must be positive integer",
        )
      }

      return compose(
        EthBN.from,
        toFixed({ precision: 0, roundingMode: options.roundingMode }),
        rightMoveDecimals(options.decimals),
        toBig,
      )(value)
    },
  )

  export const isNegative = (value: BigNumberSource): boolean => {
    return toBig(value).lt(0)
  }

  export const isGtZero = (value: BigNumberSource): boolean => {
    return toBig(value).gt(0)
  }

  export const isZero = (value: BigNumberSource): boolean => {
    return toBig(value).eq(0)
  }

  export const isEq = curry2(
    (value: BigNumberSource, a: BigNumberSource): boolean => {
      return toBig(value).eq(toBig(a))
    },
  )

  export const isGt = curry2(
    (value: BigNumberSource, a: BigNumberSource): boolean => {
      return toBig(value).gt(toBig(a))
    },
  )

  export const isGte = curry2(
    (value: BigNumberSource, a: BigNumberSource): boolean => {
      return toBig(value).gte(toBig(a))
    },
  )

  export const isLt = curry2(
    (value: BigNumberSource, a: BigNumberSource): boolean => {
      return toBig(value).lt(toBig(a))
    },
  )

  export const isLte = curry2(
    (value: BigNumberSource, a: BigNumberSource): boolean => {
      return toBig(value).lte(toBig(a))
    },
  )

  export const setPrecision = curry2(
    (
      options: {
        precision?: number
        roundingMode?: RoundingMode
      },
      value: BigNumberSource,
    ): BigNumber => {
      return fromBig(
        toBig(
          toBig(value).toPrecision(options.precision, options.roundingMode),
        ),
      )
    },
  )

  export const getPrecision = (value: BigNumberSource): number => {
    return toBig(value).c.length - (toBig(value).e + 1)
  }

  export const getIntegerLength = (value: BigNumberSource): number => {
    return toBig(value).e + 1
  }

  export const leftMoveDecimals = curry2(
    (distance: number, value: BigNumberSource): BigNumber =>
      moveDecimals({ distance }, value),
  )

  export const rightMoveDecimals = curry2(
    (distance: number, value: BigNumberSource): BigNumber =>
      moveDecimals({ distance: -distance }, value),
  )

  export const moveDecimals = curry2(
    (options: { distance: number }, value: BigNumberSource): BigNumber => {
      if (options.distance > 0) {
        return fromBig(toBig(value).div(10 ** options.distance))
      }

      if (options.distance < 0) {
        return fromBig(toBig(value).mul(10 ** -options.distance))
      }

      // distance === 0
      return from(value)
    },
  )

  export const getDecimalPart = curry2(
    (
      options: { precision: number },
      value: BigNumberSource,
    ): undefined | string => {
      /**
       * `toString` will return `"1e-8"` in some case, so we choose `toFixed` here
       */
      const formatted = toFixed(
        {
          precision: Math.min(getPrecision(value), options.precision),
          roundingMode: roundDown,
        },
        value,
      )

      const [, decimals] = formatted.split(".")
      if (decimals == null) return undefined
      return decimals
    },
  )

  export const abs = (value: BigNumberSource): BigNumber => {
    return fromBig(toBig(value).abs())
  }

  export const neg = (value: BigNumberSource): BigNumber => {
    return fromBig(toBig(value).neg())
  }

  export const sqrt = (value: BigNumberSource): BigNumber => {
    return fromBig(toBig(value).sqrt())
  }

  export const add = curry2(
    (value: BigNumberSource, a: BigNumberSource): BigNumber => {
      return fromBig(toBig(value).add(toBig(a)))
    },
  )

  export const minus = curry2(
    (value: BigNumberSource, a: BigNumberSource): BigNumber => {
      return fromBig(toBig(value).minus(toBig(a)))
    },
  )

  export const mul = curry2(
    (value: BigNumberSource, a: BigNumberSource): BigNumber => {
      return fromBig(toBig(value).mul(toBig(a)))
    },
  )

  export const div = curry2(
    (value: BigNumberSource, a: BigNumberSource): BigNumber => {
      return fromBig(toBig(value).div(toBig(a)))
    },
  )

  export const pow = curry2((value: BigNumberSource, a: number): BigNumber => {
    return fromBig(toBig(value).pow(a))
  })

  export const round = curry2(
    (
      options: {
        precision?: number
        roundingMode?: RoundingMode
      },
      value: BigNumberSource,
    ): BigNumber => {
      return fromBig(
        toBig(value).round(options.precision, options.roundingMode),
      )
    },
  )

  export const toPrecision = curry2(
    (
      options: {
        precision?: number
        roundingMode?: RoundingMode
      },
      value: BigNumberSource,
    ): string => {
      return toBig(value).toPrecision(options.precision, options.roundingMode)
    },
  )

  export const ascend = curry2(
    (a: BigNumberSource, b: BigNumberSource): -1 | 0 | 1 =>
      isLt(a, b) ? -1 : isGt(a, b) ? 1 : 0,
  )

  export const descend = curry2(
    (a: BigNumberSource, b: BigNumberSource): -1 | 0 | 1 =>
      isLt(a, b) ? 1 : isGt(a, b) ? -1 : 0,
  )

  export const max = (numbers: OneOrMore<BigNumberSource>): BigNumber => {
    return from(sort(descend, numbers)[0]!)
  }

  export const min = (numbers: OneOrMore<BigNumberSource>): BigNumber => {
    return from(sort(ascend, numbers)[0]!)
  }

  export const clamp = (
    range: [min: BigNumber, max: BigNumber],
    n: BigNumber,
  ): BigNumber => {
    const [min, max] = range
    if (isGte(n, max)) return max
    if (isLte(n, min)) return min
    return n
  }

  export const sum = (numbers: BigNumberSource[]): BigNumber => {
    return reduce((acc, n) => add(acc, n), ZERO, numbers)
  }

  export const ZERO = BigNumber.from(0)

  export const ONE = BigNumber.from(1e18)
}

interface Curry2<Args extends [any, any], Ret> {
  (a: Args[0]): (b: Args[1]) => Ret
  (...args: Args): Ret

  (a: typeof R__): Curry2<Args, Ret>
  (a: typeof R__, b: Args[1]): (a: Args[0]) => Ret
}
function curry2<Args extends [any, any], Ret>(
  fn: (...args: Args) => Ret,
): Curry2<Args, Ret> {
  // @ts-ignore
  return curryN(2, fn as any)
}
