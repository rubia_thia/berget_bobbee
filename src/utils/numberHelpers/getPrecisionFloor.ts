import { compose, __ } from "ramda"
import { BigNumber } from "./BigNumber"

export function getPrecisionFloor(precision: number): BigNumber {
  return compose(
    BigNumber.pow(__, precision),
    BigNumber.div(__, 10),
    BigNumber.from,
  )(1)
}
