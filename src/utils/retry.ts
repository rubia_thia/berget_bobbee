export function retry<T>(
  fn: () => Promise<T>,
  maxRetries = 3,
  delay = 1000,
): Promise<T> {
  return new Promise((resolve, reject) => {
    let retries = 0
    const retryFn = (): void => {
      fn()
        .then(resolve)
        .catch(err => {
          if (retries >= maxRetries) {
            reject(err)
          } else {
            retries++
            setTimeout(retryFn, delay)
          }
        })
    }
    retryFn()
  })
}
