import { Children, ComponentType, ReactNode } from "react"
import { wrapText } from "./reactHelpers/childrenHelpers"
import { FCC } from "./reactHelpers/types"

export const cleanupWhitespaceInChunks = (chunks: ReactNode): ReactNode => {
  const nodes = Children.toArray(chunks)
  return nodes.filter(node => {
    if (typeof node === "string") {
      return node.trim().length > 0
    }
    return true
  })
}

export const FormattedMessageOutsideText: FCC<{
  WrapText?: ComponentType
}> = props => {
  let children = cleanupWhitespaceInChunks(props.children)

  if (props.WrapText != null) {
    children = wrapText(children, {
      Text: props.WrapText,
    })
  }

  return <>{children}</>
}
