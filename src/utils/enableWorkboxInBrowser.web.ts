import { createElement } from "react"
import { Workbox } from "workbox-window"
import { SwitchToLatestAppVersionMessageItem } from "../components/MessageProvider/SwitchToLatestAppVersionMessageItem"
import {
  EnableWorkboxInBrowserFn,
  OnWorkboxReadyFn,
} from "./enableWorkboxInBrowser.types"

let wb: undefined | Workbox

export const onWorkboxReady: OnWorkboxReadyFn = async cb => {
  if (wb == null) {
    if (!("serviceWorker" in navigator)) return

    try {
      const _wb = new Workbox("/sw.js")
      await _wb.register()
      const a = await _wb.controlling
      a
      wb = _wb
    } catch (err) {
      console.warn("[init workbox ready]", err)
    }
  }

  cb(wb)
}

export const enableWorkboxInBrowser: EnableWorkboxInBrowserFn =
  getMessageController => {
    onWorkboxReady(wb => {
      if (wb == null) return
      if (__DEV__) return

      setInterval(() => {
        void wb.update()
      }, 1000 * 60 * 60 * 0.5)

      wb.addEventListener("controlling", () => {
        window.location.reload()
      })

      wb.addEventListener("waiting", () => {
        getMessageController(ctrl => {
          ctrl.show(ctx => ({
            key: "App updated notification",
            message: createElement(SwitchToLatestAppVersionMessageItem, {
              onSwitch: () => wb.messageSkipWaiting(),
              onDismiss: ctx.close,
            }),
            autoDismiss: null,
          }))
        })
      })
    })
  }
