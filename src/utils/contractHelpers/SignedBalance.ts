import { IRegistryCore } from "../../stores/contracts/generated/RegistryReader"
import { BigNumber } from "../numberHelpers/BigNumber"
import { fromContractToNative } from "./bigNumberContracts"

export interface SignedBalance {
  balance: BigNumber
  lastUpdate: number
}

export namespace SignedBalance {
  export function fromContractData(
    data: IRegistryCore.SignedBalanceStructOutput,
  ): SignedBalance {
    return {
      balance: fromContractToNative(data.balance),
      lastUpdate: data.lastUpdate,
    }
  }
}
