import { BigNumber as EthBN } from "ethers"
import { BigNumber } from "../numberHelpers/BigNumber"

const DEFAULT_CONTRACT_DECIMAL = 18

export function fromContractToNative(
  contractValue: string | number | EthBN,
): BigNumber {
  return BigNumber.leftMoveDecimals(DEFAULT_CONTRACT_DECIMAL)(contractValue)
}

export function fromNativeToContract(
  contractValue: BigNumber,
  options?: {
    decimals?: number
    roundingMode?: BigNumber.RoundingMode
  },
): EthBN {
  return BigNumber.toEtherBigNumber({
    decimals: options?.decimals ?? DEFAULT_CONTRACT_DECIMAL,
    roundingMode: options?.roundingMode,
  })(contractValue)
}
