import { useMemo, useState } from "react"
import { usePersistFn } from "./usePersistFn"

export interface UseIsHoveringEventListeners {
  onHoverIn: () => void
  onHoverOut: () => void
}

export const useHoveringCallback = (
  hoveringStateChangeCallback: (isHovering: boolean) => void,
): UseIsHoveringEventListeners => {
  const callback = usePersistFn(hoveringStateChangeCallback)

  return useMemo(
    () => ({
      onHoverIn: () => callback(true),
      onHoverOut: () => callback(false),
    }),
    [callback],
  )
}

export const useIsHovering = (): [
  isHovering: boolean,
  eventListeners: UseIsHoveringEventListeners,
] => {
  const [isHovering, setIsHovering] = useState(false)

  const eventListeners = useHoveringCallback(setIsHovering)

  return [isHovering, eventListeners]
}
