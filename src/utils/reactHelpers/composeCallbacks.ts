import { keys } from "ramda"
import { AnyFunc, CompactType, StringOnly, UnionToIntersection } from "../types"

export type ComposedCallback<T extends any[]> = CompactType<
  UnionToIntersection<
    {
      [K in keyof T]: {
        [K1 in keyof T[K] as T[K][K1] extends AnyFunc ? K1 : never]: (
          ...args: Parameters<T[K][K1]>
        ) => void
      }
    }[number]
  >
>
// type B = ComposedCallback<
//   [{ a: (e: number) => number }, { b: (e: string) => void, c: number }]
// >

export const composeCallbacks = <T extends any[]>(
  ...callbacks: T
): ComposedCallback<T> => {
  const callbackNames: (keyof T[number])[] = Array.from(
    new Set(callbacks.flatMap(c => keys(c))),
  ).filter((k): k is StringOnly<keyof T> => typeof k === "string")

  return callbackNames.reduce((acc: any, callbackName: any) => {
    const fns = callbacks.flatMap(c =>
      typeof c[callbackName] === "function" ? [c[callbackName]] : [],
    )
    if (fns.length > 0) {
      acc[callbackName] = (...args: any[]) => {
        fns.forEach(fn => fn(...args))
      }
    }
    return acc
  }, {} as ComposedCallback<T>)
}
