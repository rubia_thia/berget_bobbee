import { Ref, useMemo, useRef } from "react"

export type ImmutableRefObject<T> = {
  readonly current: T
}

export function assignRef<V>(
  refValue: V | null,
  ref: Ref<V> | undefined,
): void {
  if (typeof ref === "function") {
    ref(refValue)
  }

  if (ref && "current" in ref) {
    ;(ref as any).current = refValue
  }
}

export function combineRefs<V>(...refs: (Ref<V> | undefined)[]): Ref<V> {
  return (instance: V | null): void => {
    refs.forEach(ref => assignRef(instance, ref))
  }
}

export function useCombinedRef<V>(...refs: (Ref<V> | undefined)[]): Ref<V> {
  return useMemo(
    () => combineRefs(...refs),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    refs,
  )
}

export function useAutoUpdatingRef<T>(
  value: T,
  isEqual: (a: T, b: T) => boolean = Object.is,
): ImmutableRefObject<T> {
  const ref = useRef(value)
  if (!isEqual(ref.current, value)) {
    ref.current = value
  }
  return ref
}
