import { FC, ReactNode } from "react"
import { StyleProp, ViewStyle } from "react-native"

/**
 * FunctionComponent with Children
 */
export type FCC<P = unknown> = FC<PropsWithChildren<P>>
export type FCS<P = unknown> = FC<P & { style?: StyleProp<ViewStyle> }>

export type PropsWithChildren<P = unknown, C = ReactNode | ReactNode[]> = P & {
  children?: C
}
