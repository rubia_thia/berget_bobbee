import { useRef } from "react"

export function usePersistFn<F extends (...args: any[]) => any>(fn: F): F {
  const fnRef = useRef<F>(fn)
  fnRef.current = fn

  const persistFn = useRef<F>()
  if (!persistFn.current) {
    persistFn.current = function (this: any, ...args): any {
      return fnRef.current!.apply(this, args)
    } as F
  }

  return persistFn.current!
}
