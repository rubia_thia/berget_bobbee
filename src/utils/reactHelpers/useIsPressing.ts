import { useMemo, useState } from "react"

export interface UseIsPressingEventListeners {
  onPressIn: () => void
  onPressOut: () => void
}

export const useIsPressing = (): [
  isHovered: boolean,
  eventListeners: UseIsPressingEventListeners,
] => {
  const [isPressing, setIsPressing] = useState(false)

  const eventListeners: UseIsPressingEventListeners = useMemo(
    () => ({
      onPressIn: () => setIsPressing(true),
      onPressOut: () => setIsPressing(false),
    }),
    [],
  )

  return [isPressing, eventListeners]
}
