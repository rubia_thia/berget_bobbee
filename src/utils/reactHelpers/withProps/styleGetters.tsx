/* eslint-disable react-hooks/rules-of-hooks */
import { StyleProp, TextStyle, ViewStyle } from "react-native"
import { ColorGetter, useColors } from "../../../components/Themed/color"
import { SpacingGetter, useSpacing } from "../../../components/Themed/spacing"
import { PropsFactory } from "./withProps"

export function styleGetters<S extends ViewStyle | TextStyle>(
  styleFactory: (styleValueGetters: {
    colors: ColorGetter
    spacing: SpacingGetter
  }) => StyleProp<S>,
): PropsFactory<{ style?: StyleProp<S> }> {
  return passInProps => {
    const colors = useColors()
    const spacing = useSpacing()

    const style = styleFactory({
      colors,
      spacing,
    })

    return {
      ...passInProps,
      style: [style, passInProps.style],
    }
  }
}
