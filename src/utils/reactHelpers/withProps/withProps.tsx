import { ComponentProps, ComponentType, useDebugValue } from "react"
import { StyleProp, TextStyle, ViewStyle } from "react-native"

export type PropsFactory<PropsCondition> = <P extends PropsCondition>(
  passInProps: P,
) => P

export function withProps<C extends ComponentType<any>>(
  props: ComponentProps<C> | PropsFactory<ComponentProps<C>>,
  Comp: C,
): ComponentType<ComponentProps<C>> {
  const fn: ComponentType<ComponentProps<C>> = _props => {
    const finalProps =
      typeof props === "function"
        ? (props as any)(_props)
        : { ...props, ..._props }

    // eslint-disable-next-line react-hooks/rules-of-hooks
    useDebugValue(props)

    return <Comp {...finalProps} />
  }

  fn.displayName = `withProps(${Comp.displayName ?? Comp.name ?? "Anonymous"})`

  return fn
}

export function mergeStyle<S extends ViewStyle | TextStyle>(
  style: StyleProp<S>,
): PropsFactory<{ style?: StyleProp<S> }> {
  return passInProps => ({
    ...passInProps,
    style: [style, passInProps.style],
  })
}
