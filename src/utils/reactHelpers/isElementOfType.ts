import { ComponentType, isValidElement, ReactElement } from "react"

export function isElementOfType<P>(
  el: any,
  type: ComponentType<P>,
): el is ReactElement<P> {
  if (!isValidElement(el)) return false
  return el.type === type
}
