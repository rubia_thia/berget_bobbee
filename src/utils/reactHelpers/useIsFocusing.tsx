import { useMemo, useState } from "react"

export interface UseIsFocusingEventListeners {
  onFocus: () => void
  onBlur: () => void
}

export const useIsFocusing = (): [
  isFocusing: boolean,
  eventListeners: UseIsFocusingEventListeners,
] => {
  const [isFocusing, setIsFocusing] = useState(false)

  const eventListeners: UseIsFocusingEventListeners = useMemo(
    () => ({
      onFocus() {
        setIsFocusing(true)
      },
      onBlur() {
        setIsFocusing(false)
      },
    }),
    [],
  )

  return [isFocusing, eventListeners]
}
