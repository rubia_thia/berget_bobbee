import { useCallback, useState } from "react"
import { LayoutChangeEvent } from "react-native"
import { matchSizeQuery, SizeQuery, SizeQueryValue } from "../matchSizeQuery"

export function useSizeQuery<Q extends SizeQuery<any>>(
  queries: Q,
): [
  value: undefined | SizeQueryValue<Q>,
  onSizeChange: (newSize: null | number | LayoutChangeEvent) => void,
] {
  const [size, setSize] = useState<null | number>(null)

  const onSizeChange = useCallback(
    (newSize: null | number | LayoutChangeEvent): void => {
      if (newSize == null || typeof newSize === "number") {
        setSize(newSize)
      } else {
        setSize(newSize.nativeEvent.layout.width)
      }
    },
    [],
  )

  const res = size == null ? undefined : matchSizeQuery(size, queries)

  return [res, onSizeChange]
}
