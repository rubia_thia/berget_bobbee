import {
  Children,
  ComponentType,
  isValidElement,
  ReactElement,
  ReactNode,
} from "react"
import { Text as RNText } from "react-native"
import { isNotNull } from "../typeHelpers"

export type RenderFn<P> = (renderProps: P) => ReactNode

// eslint-disable-next-line @typescript-eslint/ban-types
export function renderChildren<P extends {}>(
  children: ReactNode | RenderFn<P>,
  props: P,
): ReactNode {
  return renderPropComponent(
    typeof children === "function"
      ? { render: children }
      : { elements: <>{children}</> },
    props,
  )
}

export type PropComponent<P> =
  | RenderFn<P>
  | ReactElement<P>
  | (PropComponent.PropsLimited<P> & { elements?: ReactElement<P> })
  | {
      elements: ReactElement<P>
      render?: RenderFn<P>
      Component?: ComponentType<P>
    }
export namespace PropComponent {
  export type PL<P> = RenderFn<P> | PropComponent.PropsLimited<P>

  export type PropsLimited<P> =
    | {
        render?: RenderFn<P>
        Component?: ComponentType<P>
      }
    | {
        render: RenderFn<P>
        Component?: ComponentType<P>
      }
}

// eslint-disable-next-line @typescript-eslint/ban-types
export function renderPropComponent<P extends {}>(
  comps: PropComponent<P>,
  props: P,
): ReactNode {
  if (typeof comps === "function") {
    return comps(props)
  }
  if (isValidElement(comps)) {
    return comps
  }
  if (comps.Component != null) {
    return <comps.Component {...props} />
  }
  if (comps.render != null) {
    return comps.render(props)
  }
  if (comps.elements != null) {
    return comps.elements
  }
  return null
}

export const isChildrenEmpty = (children: ReactNode): boolean => {
  const meaningfulChildren =
    Children.map(children, child =>
      child == null || !child ? null : child,
    )?.filter(isNotNull) ?? []
  return meaningfulChildren.length <= 0
}

export const wrapText = (
  children: ReactNode,
  options: {
    Text?: ComponentType<{ children?: ReactNode }>
    oneLine?: boolean
  } = {},
): ReactNode => {
  const { Text = RNText } = options

  const childrenArray = Children.toArray(children)

  const isExpectedChild = (child: typeof childrenArray[number]): boolean =>
    typeof child === "string" || typeof child === "number"

  if (options.oneLine) {
    if (!childrenArray.every(isExpectedChild)) return children
    return <Text>{children}</Text>
  }

  return childrenArray.map((child, idx) => {
    if (isExpectedChild(child)) {
      return <Text key={`text-${idx}`}>{child}</Text>
    } else {
      return child
    }
  })
}
