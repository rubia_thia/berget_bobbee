import { createContext, useContext, useEffect, useState } from "react"
import { Dimensions as RNDimensions } from "react-native"
import { FCC } from "./types"

export interface Dimensions {
  width: number
  height: number
}

const DimensionsContext = createContext<null | Dimensions>(null)

export const DimensionsProvider: FCC = ({ children }) => {
  const [dimensions, setDimensions] = useState<Dimensions>(
    RNDimensions.get("window"),
  )

  useEffect(() => {
    const onChange = ({ window }: { window: Dimensions }): void => {
      setDimensions(window)
    }
    const subscription = RNDimensions.addEventListener("change", onChange)
    return () => subscription.remove()
  }, [])

  return (
    <DimensionsContext.Provider value={dimensions}>
      {children}
    </DimensionsContext.Provider>
  )
}

export const useDimensions = (): Dimensions => {
  const dim = useContext(DimensionsContext)

  if (dim == null) {
    throw new Error("useDimensions must be used within a DimensionsProvider")
  }

  return dim
}
