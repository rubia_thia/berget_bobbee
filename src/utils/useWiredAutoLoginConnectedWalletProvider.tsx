import { useEffect } from "react"
import { useMessage } from "../components/MessageProvider/MessageProvider"
import { SupportLoginType } from "../stores/AuthStore/AuthStore.service"
import { useAuthStore } from "../stores/AuthStore/useAuthStore"
import { waitFor } from "../stores/utils/waitFor"

export const useWiredAutoLoginConnectedWalletProvider = (): void => {
  const auth = useAuthStore()
  const message = useMessage()

  useEffect(() => {
    if (auth.connectedProvider == null) {
      return
    }
    if (auth.connectedProvider === SupportLoginType.MetaMask) {
      if (auth.isMetaMaskInstalled) {
        waitFor(() => auth.connectors$)
          .then(({ MetaMask }) => MetaMask.isAuthorized())
          .then(isAuthorized =>
            isAuthorized ? auth.login(SupportLoginType.MetaMask) : null,
          )
          .catch(e => {
            message.error({ message: e.message })
          })
      }
    } else {
      auth.login(auth.connectedProvider).catch(e => {
        message.error({ message: e.message })
      })
    }
  }, [message, auth])
}
