export const capitalize = (s: string): string =>
  s === "" ? "" : s[0]!.toUpperCase() + s.slice(1)

export function idFactory(prefix: string, separator = "/"): () => string {
  let id = 0
  return () => {
    id += 1
    return prefix + separator + id.toString()
  }
}
