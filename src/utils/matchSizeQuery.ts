import { findLast, sortBy, toPairs } from "ramda"

export type SizeQuery<V> = Readonly<{
  [k in number]: V
}>

export type SizeQueryValue<Q> = Q extends SizeQuery<infer V> ? V : never

export function matchSizeQuery<Q extends SizeQuery<any>>(
  size: number,
  queries: Q,
): undefined | SizeQueryValue<Q> {
  const queryPairs = toPairs(queries) as [`${number}`, any][]
  const sortedQueryPairs = sortBy(([size]) => parseInt(size, 10), queryPairs)
  const matchedQueryPair = findLast(
    ([sizeCondition]) => size > parseInt(sizeCondition, 10),
    sortedQueryPairs,
  )
  return matchedQueryPair?.[1]
}
