import { init as rInit, map as rMap } from "ramda"

export function arrayJoin<T>(
  separator: (info: { index: number }) => T,
  inputArray: T[],
): T[] {
  return inputArray.reduce((outputArray: T[], child, index) => {
    outputArray.push(child)

    if (index < inputArray.length - 1) {
      outputArray.push(separator({ index }))
    }

    return outputArray
  }, [])
}

export function hasAny<T>(ary: T[]): ary is [T, ...T[]]
export function hasAny<T>(ary: readonly T[]): ary is readonly [T, ...T[]]
export function hasAny<T>(ary: readonly T[]): ary is readonly [T, ...T[]] {
  return ary.length > 0
}

export function oneOf<T extends string[]>(
  ...coll: T
): (input: unknown) => input is T[number]
export function oneOf<T extends any[]>(
  ...coll: T
): (input: unknown) => input is T[number]
export function oneOf<T extends any[]>(
  ...coll: T
): (input: unknown) => input is T[number] {
  return ((input: unknown) => coll.includes(input)) as any
}

export function first<T>(ary: [T, ...T[]]): T
export function first<T>(ary: [...T[], T]): T
export function first<T>(ary: T[]): undefined | T
export function first<T>(ary: T[]): undefined | T {
  return ary[0]
}

export function last<T>(ary: [T, ...T[]]): T
export function last<T>(ary: [...T[], T]): T
export function last<T>(ary: T[]): undefined | T
export function last<T>(ary: T[]): undefined | T {
  return ary[ary.length - 1]
}

export function init<Inputs extends any[]>(
  ary: Inputs,
): Inputs extends [...init: infer R, last: any] ? R : Inputs {
  return rInit(ary) as any
}

export function map<T1, T2>(
  f: (i: T1) => T2,
  ary: readonly [T1, ...T1[]],
): [T2, ...T2[]]
export function map<T1, T2>(
  f: (i: T1) => T2,
  ary: readonly [...T1[], T1],
): [...T2[], T2]
export function map<T1, T2>(f: (i: T1) => T2, ary: readonly T1[]): T2[]
export function map<T1, T2>(f: (i: T1) => T2, ary: readonly T1[]): T2[] {
  return rMap(f, ary)
}
