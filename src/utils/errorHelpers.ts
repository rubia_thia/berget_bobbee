declare global {
  interface ErrorConstructor {
    new (message: string, options: ErrorConstructorOptions): Error
    new (message: string): Error
    readonly prototype: Error
  }
  interface ErrorConstructorOptions {
    cause: unknown
  }
  interface Error {
    name: string
    message: string
    stack?: string
    cause?: unknown
  }
}

export class CancelError extends Error {
  constructor(message?: string) {
    super(message)
    this.name = "CancelError"
  }
}

export class DisplayableError extends Error {
  constructor(message: string, options?: { cause: any }) {
    super(message, options)
    this.cause = options?.cause
  }
}

export interface ErrorAlerter {
  show(options: { message: string }): void
}
export const alertError = (
  prefix: string,
  alerter: ErrorAlerter,
): ((err: unknown) => void) => {
  return err => {
    if (err != null) {
      if (err instanceof DisplayableError || "message" in (err as any)) {
        alerter.show({ message: `${prefix}: ${(err as any).message}` })
        return
      }
    }

    alerter.show({ message: prefix })
  }
}

export const findError = <
  ErrEntities extends readonly { type: any }[],
  ErrTypes extends readonly ErrEntities[number]["type"][],
>(
  errors: undefined | ErrEntities,
  types: ErrTypes,
): undefined | Extract<ErrEntities[number], { type: ErrTypes[number] }> => {
  if (errors == null) return
  return errors.find(err => types.includes(err.type)) as any
}

export const extractCauseChain = (error: Error): string[] => {
  const cause: unknown = "cause" in error ? error.cause : undefined
  let formattedCause: string[]
  if (cause instanceof Error) {
    formattedCause = extractCauseChain(cause)
  } else if (typeof cause === "string") {
    formattedCause = [cause]
  } else {
    formattedCause = [JSON.stringify(cause)]
  }
  return [error.stack ?? "", ...formattedCause]
}

export const formatError = (error: Error): string => {
  return extractCauseChain(error).join("\n\nvvvvvvvvvvvvvv\n")
}
