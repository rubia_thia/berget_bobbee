module.exports = {
  extends: [
    "prettier",
    "plugin:react/recommended",
    "plugin:react-hooks/recommended",
    "plugin:storybook/recommended",
    "plugin:mobx/recommended",
  ],
  plugins: ["prettier", "formatjs"],
  globals: {
    React: "readonly",
    ReactDOM: "readonly",
  },
  settings: {
    react: {
      version: "detect",
    },
  },
  parserOptions: {
    ecmaVersion: 2020,
    ecmaFeatures: {},
    sourceType: "module",
  },
  rules: {
    // normal
    "max-classes-per-file": "off",
    curly: ["error", "multi-line"],
    "no-shadow": "off",
    "no-extra-semi": "off",
    "no-unused-vars": [
      "error",
      {
        args: "none",
        caughtErrors: "none",
        argsIgnorePattern: "^_",
      },
    ],
    "prettier/prettier": "error",
    // react
    "react/prop-types": "off",
    "react/react-in-jsx-scope": "off",
    "react/jsx-pascal-case": "off",
    "react/jsx-key": "off",
    "react/display-name": "off",
    "react-hooks/rules-of-hooks": "error",
    "react/self-closing-comp": "warn",
    "react-hooks/exhaustive-deps": [
      "error",
      {
        additionalHooks: "(useCreation)",
      },
    ],
    // formatjs
    "formatjs/enforce-description": ["error", "literal"],
    "formatjs/enforce-default-message": ["error", "literal"],
    "formatjs/enforce-placeholders": ["error"],
    "formatjs/enforce-plural-rules": ["error", { one: true, other: true }],
  },
  overrides: [
    {
      files: ["**/*.stories.*"],
      rules: {
        "import/no-anonymous-default-export": "off",
      },
    },
    {
      files: ["**/*.ts", "**/*.tsx"],
      extends: ["plugin:@typescript-eslint/recommended"],
      plugins: ["@typescript-eslint"],
      parser: "@typescript-eslint/parser",
      parserOptions: {
        project: "./tsconfig.json",
      },
      rules: {
        // typescript
        "@typescript-eslint/ban-ts-comment": "off",
        "@typescript-eslint/consistent-type-definitions": "off",
        "@typescript-eslint/consistent-type-assertions": "off",
        "@typescript-eslint/no-empty-interface": "off",
        "@typescript-eslint/no-namespace": "off",
        "@typescript-eslint/no-explicit-any": "off",
        "@typescript-eslint/no-non-null-assertion": "off",
        "@typescript-eslint/no-parameter-properties": "off",
        "@typescript-eslint/no-use-before-define": "off",
        "@typescript-eslint/no-extra-semi": "off",
        "@typescript-eslint/no-unused-vars": [
          "warn",
          { args: "after-used", caughtErrors: "none", argsIgnorePattern: "^_" },
        ],
        "@typescript-eslint/no-redeclare": [
          "off",
          { ignoreDeclarationMerge: true },
        ],
        "@typescript-eslint/explicit-member-accessibility": [
          "error",
          { accessibility: "no-public" },
        ],
        "@typescript-eslint/no-floating-promises": [
          "error",
          { ignoreVoid: true },
        ],
        "@typescript-eslint/explicit-function-return-type": [
          "error",
          { allowExpressions: true },
        ],
        "mobx/missing-observer": "off",
        "mobx/exhaustive-make-observable": "off", // we are using annotations
        "mobx/no-anonymous-observer": "off", // we are using babel plugin for this
      },
    },
  ],
}
