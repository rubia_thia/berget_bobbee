set +e

corepack prepare pnpm --activate
npx pnpm i --store=node_modules/.pnpm-store
npx tsc functions/config.ts --skipLibCheck --target esnext --module esnext --moduleResolution node
mv functions/config.js public/functions/
npx pnpm run build:web
npx sentry-cli releases new "$CF_PAGES_COMMIT_SHA"
npx sentry-cli releases files "$CF_PAGES_COMMIT_SHA" upload-sourcemaps ./dist/
npx sentry-cli releases finalize "$CF_PAGES_COMMIT_SHA"

# delete all sourcemaps.
rm -rf ./dist/**/*.map
