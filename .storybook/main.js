module.exports = {
  core: {
    builder: "webpack5",
  },
  stories: ["../src/**/*.stories.mdx", "../src/**/*.stories.@(js|jsx|ts|tsx)"],
  addons:
    process.env.STORYBOOK_PLATFORM === "react-native"
      ? [
          "@storybook/addon-ondevice-notes",
          "@storybook/addon-ondevice-controls",
          "@storybook/addon-ondevice-backgrounds",
          "@storybook/addon-ondevice-actions",
        ]
      : [
          "@storybook/addon-links",
          "@storybook/addon-actions",
          "@storybook/addon-essentials",
        ],
}
