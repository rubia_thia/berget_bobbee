import { StacksProvider } from "@mobily/stacks"
import { DecoratorFn } from "@storybook/react"
import React, { Suspense } from "react"
import { Platform, View } from "react-native"
import { SafeAreaInsetsContext } from "react-native-safe-area-context"
import { BlueButtonVariant } from "../src/components/Button/BlueButtonVariant"
import { DefaultButtonVariantProvider } from "../src/components/ButtonFramework/Button"
import { WiredIntlProvider } from "../src/components/IntlProvider/WiredIntlProvider"
import { PopoverPortalProvider } from "../src/components/Popover/Popover"
import { BreakpointsProvider } from "../src/components/Themed/breakpoints"
import { ColorsProvider } from "../src/components/Themed/color"
import { defaultThemeBreakpoints } from "../src/components/Themed/defaults/defaultThemeBreakpoints"
import { defaultThemePalette } from "../src/components/Themed/defaults/defaultThemePalette"
import { defaultThemeSpacing } from "../src/components/Themed/defaults/defaultThemeSpacing"
import { SpacingProvider } from "../src/components/Themed/spacing"
import { WiredFontLoader } from "../src/components/WiredFontLoader"
import { DimensionsProvider } from "../src/utils/reactHelpers/useDimensions"

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  layout: "fullscreen",
  docs: {
    // Opt-out of inline rendering
    inlineStories: false,
  },
}

export const decorators: DecoratorFn[] = [
  (Story, ctx) => (
    <WiredIntlProvider forceLanguageTag={"en-US"}>
      <DefaultButtonVariantProvider ButtonVariant={BlueButtonVariant}>
        <DimensionsProvider>
          <StacksProvider spacing={1} breakpoints={defaultThemeBreakpoints}>
            <BreakpointsProvider breakpoints={defaultThemeBreakpoints}>
              <ColorsProvider colors={defaultThemePalette}>
                <SpacingProvider spacings={defaultThemeSpacing}>
                  <SafeAreaInsetsContext.Provider
                    value={{ top: 0, right: 0, left: 0, bottom: 0 }}
                  >
                    <PopoverPortalProvider>
                      <Suspense fallback={null}>
                        <WiredFontLoader />
                        <View
                          style={
                            Platform.OS === "web"
                              ? { height: "fit-content", minHeight: "100vh" }
                              : { flex: 1 }
                          }
                        >
                          <Story {...ctx} />
                        </View>
                      </Suspense>
                    </PopoverPortalProvider>
                  </SafeAreaInsetsContext.Provider>
                </SpacingProvider>
              </ColorsProvider>
            </BreakpointsProvider>
          </StacksProvider>
        </DimensionsProvider>
      </DefaultButtonVariantProvider>
    </WiredIntlProvider>
  ),
]

// Only on web
if (Platform.OS === "web") {
  // Inject style
  const style = document.createElement("style")
  style.textContent = `textarea, select, input, button { outline: none!important; }`
  document.head.append(style)
}
