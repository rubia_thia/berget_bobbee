import { ComponentStory, Story } from "@storybook/react"
import { Draft, produce } from "immer"
import { ComponentType } from "react"
import { AnyFunc, CompactType } from "../src/utils/types"

function defaultUpdater(value: any): void {}

export type OptionalizeEventListeners<T> = CompactType<
  {
    [K in keyof T as K extends `on${infer R}`
      ? T[K] extends AnyFunc
        ? never
        : K
      : K]: T[K]
  } & {
    [K in keyof T as K extends `on${infer R}`
      ? T[K] extends AnyFunc
        ? K
        : never
      : never]?: T[K]
  }
>

export function withTemplate<P>(
  Component: ComponentType<P>,
  defaultValues: OptionalizeEventListeners<P>,
): (
  valueUpdater?: (value: Draft<P>) => P | void,
) => ComponentStory<typeof Component> {
  const Template: Story<P> = args => <Component {...(args as any)} />
  return function (updater = defaultUpdater) {
    const Result = Template.bind({})
    Result.args = produce(defaultValues, updater as any) as any
    return Result
  }
}
