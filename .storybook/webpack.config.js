const { withUnimodules } = require("@expo/webpack-config/addons")
const path = require("path")

const addSupportForSvgTransform = config => {
  config.module.rules.forEach(r => {
    if (isFileLoaderSvgRule(r)) {
      r.test = removeSvgPattern(r.test)
    }

    function isFileLoaderSvgRule(rule) {
      return rule.test.test("test.svg")
    }

    function removeSvgPattern(regex) {
      return new RegExp(regex.source.replace("svg|", ""))
    }
  })

  config.module.rules.unshift({
    test: /\.svg$/,
    exclude: /node_modules/,
    use: [
      {
        loader: "@svgr/webpack",
      },
    ],
  })
}

module.exports = ({ config, ...env }) => {
  addSupportForSvgTransform(config)

  config.module.rules.push({
    test: /\.mjs$/,
    include: /node_modules/,
    type: "javascript/auto",
  })

  return withUnimodules(config, {
    projectRoot: path.resolve(__dirname, "../"),
    babel: {
      dangerouslyAddModulePathsToTranspile: ["victory-native"],
    },
  })
}
