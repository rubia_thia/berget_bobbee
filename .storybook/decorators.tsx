import { DecoratorFn } from "@storybook/react"
import { StyleProp, View, ViewStyle } from "react-native"
import {
  CardBoxView,
  CardBoxViewProps,
} from "../src/components/CardBox/CardBox"

export const BackgroundColor: (color?: string) => DecoratorFn =
  (color = "#E9EFF1") =>
  Story => {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: color,
        }}
      >
        {Story()}
      </View>
    )
  }

export const CardContainer: (
  style?: StyleProp<ViewStyle>,
  otherProps?: Partial<CardBoxViewProps>,
) => DecoratorFn = (style, otherProps) => Story => {
  return (
    <CardBoxView style={[{ margin: 10 }, style]} {...otherProps}>
      <Story />
    </CardBoxView>
  )
}
