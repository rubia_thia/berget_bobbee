// organize-imports-ignore
import "./prepare"
import { registerRootComponent } from "expo"
import { App, getMessageController } from "./src/App"
import * as Sentry from "sentry-expo"
import { enableWorkboxInBrowser } from "./src/utils/enableWorkboxInBrowser"
import { extractCauseChain } from "./src/utils/errorHelpers"

Sentry.init({
  release: process.env["CF_PAGES_COMMIT_SHA"] || "Dev",
  environment: process.env["CF_PAGES_BRANCH"] || "Dev",
  dsn: "https://3031186487bc4ebea8c95b3ba226fe3f@o4504615882260480.ingest.sentry.io/4504616145256448",
  enableInExpoDevelopment: true,
  debug: __DEV__,
  ignoreErrors: [
    /Transaction reverted without a reason string/,
    /Failed to execute 'insertBefore' on 'Node'/,
    /Failed to execute 'removeChild' on 'Node'/,
    /No error message/,
    /user rejected transaction/,
    /timeout exceeded/,
    /timeout of .*ms exceeded/,
    /chrome-extension/,
  ],
  beforeSend(event, hint) {
    event.extra = {
      ...event.extra,
      causes:
        hint.originalException != null &&
        hint.originalException instanceof Error
          ? extractCauseChain(hint.originalException)
          : hint.originalException,
    }
    return event
  },
})

registerRootComponent(App)
enableWorkboxInBrowser(getMessageController)
