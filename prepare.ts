import "expo-dev-client"
import { polyfillWebCrypto } from "expo-standard-web-crypto"
import { shim } from "react-native-quick-base64"
import { enableFreeze } from "react-native-screens"
import "react-native-url-polyfill/auto"
import "./prepare.common"

enableFreeze(true)
polyfillWebCrypto()
shim()
