import { Buffer } from "@craftzdog/react-native-buffer"
import { NativeWindStyleSheet } from "nativewind"
import "react-native-gesture-handler"

// @ts-ignore
global.Buffer ??= Buffer

NativeWindStyleSheet.setOutput({
  default: "native",
})
