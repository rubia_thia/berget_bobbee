const fs = require("fs")
const posthtml = require("posthtml")
const esbuild = require("esbuild")

const commonEsbuildOptions = {
  outdir: "out",
  platform: "browser",
  sourcemap: false,
  bundle: true,
  write: false,
  treeShaking: true,
}

module.exports = function (api) {
  api.cache(true)

  return {
    presets: ["babel-preset-expo"],
    plugins: [
      [
        "transform-inline-environment-variables",
        {
          include: ["CF_PAGES_COMMIT_SHA", "CF_PAGES_BRANCH"],
        },
      ],
      "@babel/plugin-proposal-logical-assignment-operators",
      "@babel/plugin-proposal-numeric-separator",
      require("./scripts/babelPluginAddSpensorTag"),
      [
        require("./scripts/babelPluginRawTextImport"),
        {
          fetchers: {
            raw: targetFilePath => fs.readFileSync(targetFilePath, "utf8"),
            inlineScript: targetFilePath =>
              esbuild.buildSync({
                entryPoints: [targetFilePath],
                ...commonEsbuildOptions,
              }).outputFiles[0].text,
            inlineHtml: targetFilePath =>
              posthtml()
                .use(
                  require("./scripts/posthtmlPluginInlineLocalScript")({
                    filePath: targetFilePath,
                    readScript: filePath =>
                      esbuild.buildSync({
                        entryPoints: [filePath],
                        ...commonEsbuildOptions,
                      }).outputFiles[0].text,
                  }),
                )
                .process(fs.readFileSync(targetFilePath, "utf8"), {
                  sync: true,
                }).html,
          },
        },
      ],
      "nativewind/babel",
      [
        "module-resolver",
        {
          alias: {
            crypto: "react-native-quick-crypto",
            stream: "stream-browserify",
            buffer: "@craftzdog/react-native-buffer",
            url: "native-url",
          },
        },
      ],
      [
        require.resolve("babel-plugin-jsx-wrapper"),
        {
          displayName: true,
          decorator: "mobx-lite",
          exclude: [
            "*.stories.*",
            "*.storiesHelpers.*",
            ".storybook/*",
            "node_modules/*",
          ],
        },
      ],
      ["babel-plugin-formatjs", {}],
      "@babel/plugin-proposal-export-namespace-from",
      "react-native-reanimated/plugin",
    ],
  }
}
