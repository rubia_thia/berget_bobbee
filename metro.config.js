// Learn more https://docs.expo.io/guides/customizing-metro
const { getDefaultConfig } = require("expo/metro-config")

module.exports = (() => {
  const config = getDefaultConfig(__dirname)

  const { transformer, resolver } = config

  config.transformer = {
    ...transformer,
    babelTransformerPath: require.resolve("react-native-svg-transformer"),
  }

  config.resolver = {
    ...resolver,
    assetExts: resolver.assetExts.filter(ext => ext !== "svg"),
    sourceExts: [...resolver.sourceExts, "svg"],
  }

  const rewriteRequestUrl = config.server.rewriteRequestUrl
  config.server.rewriteRequestUrl = path => {
    if (process.env.STORYBOOK && path.includes("index.tsx.bundle")) {
      return rewriteRequestUrl(
        path.replace("index.tsx.bundle", "index.storybook.tsx.bundle"),
      )
    } else {
      return rewriteRequestUrl(path)
    }
  }

  return config
})()
