const template = (variables, { tpl }) => {
  return tpl`
import { unstable_createElement as createElement, Platform } from 'react-native';
${variables.imports};

${variables.interfaces};

let ${variables.componentName} = dontWrapByObserver((${variables.props}) => (
  ${variables.jsx}
));
 
if (Platform.OS === 'web') {
  const ForwardRefNativeSvgIcon = React.forwardRef(${variables.componentName});

  ${variables.componentName} = (props, ref) => {
    return createElement(ForwardRefNativeSvgIcon, { ...props, ref });
  }
}
${variables.exports};

function dontWrapByObserver(a) {
  return a;
}
`
}

module.exports = {
  ref: true,
  template,
  svgoConfig: {
    plugins: [
      {
        name: "preset-default",
        params: {
          overrides: {
            removeViewBox: false,
          },
        },
      },
      "prefixIds",
    ],
  },
}
