// organize-imports-ignore
import "./prepare"
import { registerRootComponent } from "expo"
import ReactNativeStorybook from "./.storybook/ReactNativeStorybook"

registerRootComponent(ReactNativeStorybook)
