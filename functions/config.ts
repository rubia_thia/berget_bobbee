import type {
  PagesFunction,
  Request,
  Response as CloudflareResponse,
} from "@cloudflare/workers-types"

export const onRequestOptions = async (): Promise<CloudflareResponse> => {
  return new Response(null, {
    status: 204,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Headers": "*",
      "Access-Control-Allow-Methods": "GET, OPTIONS",
      "Access-Control-Max-Age": "86400",
    },
  }) as any as CloudflareResponse
}

type Env = {
  AppConfig: {
    put(key: string, value: string): Promise<void>
    get(key: string): Promise<string | null>
    delete(key: string): Promise<void>
  }
}

export const onRequest: PagesFunction<Env> = async (
  context,
): Promise<CloudflareResponse> => {
  return handleRequest(context.request, context.env)
}

const envMaps = {
  "mainnet.uniwhale.tk": "Mainnet Staging",
  "uniwhale.tk": "Dev",
  "uniwhale-app.pages.dev": "Dev",
  "localhost:": "Local",
  "app.testnet.uniwhale.co": "PublicTest",
  "app.uniwhale.co": "BNB Mainnet",
}

async function handleRequest(request: Request, envs: Env): Promise<any> {
  const origin = new URL(request.url).searchParams.get("origin")
  const envKey =
    origin != null ? Object.keys(envMaps).find(a => origin.includes(a)) : null
  if (!envKey) {
    return new Response("Sorry, you have supplied an invalid host.", {
      status: 404,
      headers: {
        "Access-Control-Allow-Origin": "*",
      },
    })
  }
  // @ts-ignore
  const env = envMaps[envKey]
  const configs = await retrieveAndCacheConfigFromNotion(envs)
  const result = {}
  for (const config of configs) {
    // @ts-ignore
    result[config.Entry] = config[env]
  }
  return new Response(JSON.stringify(result), {
    headers: {
      "content-type": "application/json;charset=UTF-8",
      "Access-Control-Allow-Origin": "*",
      "Cache-Control": "max-age=60",
    },
  }) as any
}

async function retrieveAndCacheConfigFromNotion(envs: Env): Promise<any> {
  const notionURL =
    "https://still-wave-a807-production.reily.workers.dev/v1/table/3c1cf340324c4906bcbc9dd3f31f806d"
  const result = await envs.AppConfig.get(notionURL)
  if (result == null) {
    return readFromNotionAndSaveToKV()
  }
  try {
    const { config, cachedAt } = JSON.parse(result)
    if (Date.now() / 1000 - cachedAt / 1000 > 60 * 5) {
      return readFromNotionAndSaveToKV().catch(() => config)
    }
    return config
  } catch (e) {
    return readFromNotionAndSaveToKV()
  }
  async function readFromNotionAndSaveToKV(): Promise<any> {
    const config = await fetch(notionURL).then(a => a.json())
    await envs.AppConfig.put(
      notionURL,
      JSON.stringify({
        config,
        cachedAt: Date.now(),
      }),
    )
    return config
  }
}
