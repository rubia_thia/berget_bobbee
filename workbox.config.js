module.exports = {
  globDirectory: "dist/",
  globPatterns: ["**/*.{ttf,png,js,svg,html,json}"],
  swDest: "dist/sw.js",
  maximumFileSizeToCacheInBytes: 10 * 1024 * 1024,
  ignoreURLParametersMatching: [/^utm_/, /^fbclid$/],
}
