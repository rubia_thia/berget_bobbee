const colors = require("./src/components/Themed/defaults/tailwindColors.json")
const spacing = require("./src/components/Themed/defaults/tailwindSpacing.json")
const breakpoints = require("./src/components/Themed/defaults/tailwindBreakpoints.json")

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    colors,
    screens: breakpoints,
    spacing,
    extend: {},
  },
  plugins: [],
}
